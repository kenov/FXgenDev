/*
 *
 * SceneRender.h
 * 
 */


#ifndef __VX_SCENE_RENDER_H__
#define __VX_SCENE_RENDER_H__

#include "Render.h"

namespace VX {
	class Graphics;
	class PrimitiveRender;
	class SGRender;
	namespace SG{
		class Node;
	}
}

class SceneRender : public IRender
{
public:
	SceneRender(VX::Graphics* vg);
	~SceneRender();
	
	void Draw(const VX::SG::Node* node);
	void Resize(u32 w, u32 h);

	void SetViewMat(const VX::Math::matrix4x4& mat);
	void SetProjMat(const VX::Math::matrix4x4& mat);
	
	void NewScene();
	
	void ShowWire(b8 enable);
	b8   IsShowWire() const;
	
	void ShowDomainWire(b8 enable);
	b8   IsShowDomainWire() const;
	
	void ShowPolygon(b8 enable);
	b8   IsShowPolygon() const;

	void ShowMeshGrid(b8 enable);
	b8   IsShowMeshGrid() const;
	
	void SetIDColorTable(s32 i, const VX::Math::vec4& col);

	VX::Math::vec4 GetIDColorTable(s32 i);

private:
	void drawNode(const VX::SG::Node* node);
	
	u32 m_screenWidth;
	u32 m_screenHeight;
	b8  m_needupdate;
	
	VX::Graphics* g;
	VX::Math::matrix4x4 m_proj;
	VX::Math::matrix4x4 m_view;
	
	VX::SGRender* m_sgrender;
};

#endif // __VX_SCENE_RENDER_H__

