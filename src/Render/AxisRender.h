/*
 *
 * AxisRender.h
 * 
 */


#ifndef __VX_UI_AXISRENDER_H__
#define __VX_UI_AXISRENDER_H__

#include "../VX/Type.h"
#include "Render.h"
#include "../VX/Math.h"

namespace VX {
	class Graphics;
	class PrimitiveRender;
	namespace SG{
		class Node;
	}
}

class AxisRender : public IRender
{
public:
	AxisRender(VX::Graphics* vg);
	~AxisRender();
	
	void Draw(const VX::SG::Node* node);
	void Resize(u32 w, u32 h);

	void SetViewMat(const VX::Math::matrix4x4& mat);
	void SetProjMat(const VX::Math::matrix4x4& mat);
		
private:
	u32 m_screenWidth;
	u32 m_screenHeight;
	
	VX::Math::matrix4x4 m_view;
			
	VX::Graphics* g;
	VX::PrimitiveRender* m_pr;
};

#endif // __VX_UI_AXISRENDER_H__

