/*
 *
 * SceneRender.cpp
 * 
 */

#include "../VX/VX.h"
#include "../VX/Graphics.h"

#include "../VX/SG/SceneGraph.h"
#include "../VX/SGRender.h"

#include "SceneRender.h"

SceneRender::SceneRender(VX::Graphics* vg)
{
	g = vg;
	
	m_view = VX::Math::Identity();
	m_proj = VX::Math::Identity();
	
	m_sgrender = vxnew VX::SGRender(vg);
}

SceneRender::~SceneRender()
{
	vxdelete( m_sgrender );
}

void SceneRender::NewScene()
{
	m_sgrender->ClearCache();
}

void SceneRender::Draw(const VX::SG::Node* node)
{
	using namespace VX::Math;
	
	m_sgrender->SetWorldViewMat(m_view);
	m_sgrender->SetProjMat(m_proj);
	
	m_sgrender->Draw(node);
	m_sgrender->RenderAll();
}


void SceneRender::Resize(u32 w, u32 h)
{
	m_screenWidth  = w;
	m_screenHeight = h;
}

void SceneRender::SetViewMat(const VX::Math::matrix4x4& mat)
{
	m_view = mat;
}

void SceneRender::SetProjMat(const VX::Math::matrix4x4& mat)
{
	m_proj = mat;
}

void SceneRender::ShowWire(b8 enable)
{
	m_sgrender->ShowWire(enable);
}

b8 SceneRender::IsShowWire() const
{
	return m_sgrender->IsShowWire();
}

void SceneRender::ShowDomainWire(b8 enable)
{
	m_sgrender->ShowDomainWire(enable);
}

b8 SceneRender::IsShowDomainWire() const
{
	return m_sgrender->IsShowDomainWire();
}

void SceneRender::ShowMeshGrid(b8 enable)
{
	m_sgrender->ShowMeshGrid(enable);
}

b8 SceneRender::IsShowMeshGrid() const
{
	return m_sgrender->IsShowMeshGrid();
}

void SceneRender::ShowPolygon(b8 enable)
{
	m_sgrender->ShowPolygon(enable);
}

b8   SceneRender::IsShowPolygon() const
{
	return m_sgrender->IsShowPolygon();
}

void SceneRender::SetIDColorTable(s32 i, const VX::Math::vec4& col)
{
	m_sgrender->SetIDColorTable(i, col);
}

VX::Math::vec4 SceneRender::GetIDColorTable(s32 i)
{
	return m_sgrender->GetIDColorTable(i);
}


