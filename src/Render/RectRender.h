/*
 *
 * RectRender.h
 * 
 */


#ifndef __VX_UI_RECTRENDER_H__
#define __VX_UI_RECTRENDER_H__

#include "../VX/Type.h"
#include "Render.h"

namespace VX {
	class Graphics;
	class PrimitiveRender;
	namespace SG{
		class Node;
	}
}

class RectRender : public IRender
{
public:
	RectRender(VX::Graphics* vg);
	~RectRender();
	
	void Draw(const VX::SG::Node* node);
	void Resize(u32 w, u32 h);

	void SetViewMat(const VX::Math::matrix4x4& mat);
	void SetProjMat(const VX::Math::matrix4x4& mat);
	
	void Move(s32 dx, s32 dy);
	b8 OnClickUp();
	b8 OnClick(s32 x, s32 y);
	void StartRect(s32 sx, s32 sy, s32 ex, s32 ey);
	void EndRect(s32& sx, s32& sy, s32& ex, s32& ey);
	
	
	VX::SG::Node* GetActiveNode();
	
private:
	u32 m_screenWidth;
	u32 m_screenHeight;
			
	b8 m_selecting;
	
	s32 m_selectstart_x, m_selectstart_y;
	s32 m_selectend_x, m_selectend_y;
	
	VX::Graphics* g;
	VX::PrimitiveRender* m_pr;
};

#endif // __VX_UI_RECTRENDER_H__

