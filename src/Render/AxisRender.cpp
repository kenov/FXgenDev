#include "../VX/VX.h"
#include "AxisRender.h"
#include "../VX/PrimitiveRender.h"
#include "../VX/Graphics.h"

AxisRender::AxisRender(VX::Graphics* vg)
{
	g = vg;
	m_pr = vxnew VX::PrimitiveRender(g);
	m_view = VX::Math::Identity();
}

AxisRender::~AxisRender()
{
	vxdelete(m_pr);
}
	
void AxisRender::Draw(const VX::SG::Node* node)
{
	using namespace VX::Math;
	matrix orthoproj = Ortho(0.0f, static_cast<float>(m_screenWidth), static_cast<float>(m_screenHeight),0.0f, -100.0f, 100.0f);
	m_pr->SetProjMat(orthoproj);
	
	vec3 xr(m_view.m[0][0],m_view.m[0][1],m_view.m[0][2]); xr = normalize(xr);
	vec3 yr(m_view.m[1][0],m_view.m[1][1],m_view.m[1][2]); yr = normalize(yr);
	vec3 zr(m_view.m[2][0],m_view.m[2][1],m_view.m[2][2]); zr = normalize(zr);
	matrix rot;
	rot.m[0][0] = xr.x; rot.m[0][1] = xr.y; rot.m[0][2] = xr.z; rot.m[0][3] = 0.0f;
	rot.m[1][0] = yr.x; rot.m[1][1] = yr.y; rot.m[1][2] = yr.z; rot.m[1][3] = 0.0f;
	rot.m[2][0] = zr.x; rot.m[2][1] = zr.y; rot.m[2][2] = zr.z; rot.m[2][3] = 0.0f;
	rot.m[3][0] = 0.0f; rot.m[3][1] = 0.0f; rot.m[3][2] = 0.0f; rot.m[3][3] = 1.0f;
	matrix mv = Translation(static_cast<f32>(m_screenWidth - 60), static_cast<f32>(m_screenHeight - 150), 0.0f);
	m_pr->SetWorldViewMat(mv);
	
	g->Enable(VG_DEPTH_TEST);
	g->Enable(VG_BLEND);
	g->BlendFunc(VG_SRC_ALPHA, VG_ONE_MINUS_SRC_ALPHA);
	
	
	m_pr->Clear();
	m_pr->Begin(VX::PrimitiveRender::MODE_LINES);
	m_pr->Normal(vec3(0.0f,0.0f,-1.0f));
	const f32 size = 40.0f;
	const vec3 xpos = (rot * vec4(size,0.0f,0.0f,1.0f)).xyz();
	const vec3 ypos = (rot * vec4(0.0f,size,0.0f,1.0f)).xyz();
	const vec3 zpos = (rot * vec4(0.0f,0.0f,size,1.0f)).xyz();
	m_pr->Color(vec4(1.0f,0.0f,0.0f,1.0f));
	m_pr->Vertex(vec3(0.0f,0.0f,0.0f));
	m_pr->Vertex(xpos);
	m_pr->Vertex(xpos + vec3( 3,-4+8,0));  // X
	m_pr->Vertex(xpos + vec3(-3, 4+8,0));
	m_pr->Vertex(xpos + vec3(-3,-4+8,0));
	m_pr->Vertex(xpos + vec3( 3, 4+8,0));
	m_pr->Color(vec4(0.0f,1.0f,0.0f,1.0f));
	m_pr->Vertex(vec3(0.0f,0.0f,0.0f));
	m_pr->Vertex(ypos);
	m_pr->Vertex(ypos + vec3(-3, 4+8,0)); //Y
	m_pr->Vertex(ypos + vec3( 0, 0+8,0));
	m_pr->Vertex(ypos + vec3( 3, 4+8,0));
	m_pr->Vertex(ypos + vec3( 0, 0+8,0));
	m_pr->Vertex(ypos + vec3( 0, 0+8,0));
	m_pr->Vertex(ypos + vec3( 0,-4+8,0));
	m_pr->Color(vec4(0.0f,0.25f,1.0f,1.0f));
	m_pr->Vertex(vec3(0.0f,0.0f,0.0f));
	m_pr->Vertex(zpos);
	m_pr->Vertex(zpos + vec3( 3, 4+8,0));  // Z
	m_pr->Vertex(zpos + vec3(-3, 4+8,0));
	m_pr->Vertex(zpos + vec3( 3, 4+8,0));
	m_pr->Vertex(zpos + vec3(-3,-4+8,0));
	m_pr->Vertex(zpos + vec3(-3,-4+8,0));
	m_pr->Vertex(zpos + vec3( 3,-4+8,0));
	m_pr->End();
	m_pr->DrawAll();

	g->Disable(VG_BLEND);
}

void AxisRender::Resize(u32 w, u32 h)
{
	m_screenWidth  = w;
	m_screenHeight = h;
}

void AxisRender::SetViewMat(const VX::Math::matrix4x4& mat)
{
	m_view = mat;
}

void AxisRender::SetProjMat(const VX::Math::matrix4x4& mat)
{
	// to do nothing
}
		
