/*
 *
 * RectRender.cpp
 * 
 */


#include "../VX/VX.h"
#include "RectRender.h"

#include "../VX/PrimitiveRender.h"
#include "../VX/Graphics.h"
#include "../VX/Math.h"

namespace VX {
	class Graphics;
	class PrimitiveRender;
	namespace SG{
		class Node;
	}
}

RectRender::RectRender(VX::Graphics* vg)
{
	g = vg;
	m_pr = vxnew VX::PrimitiveRender(g);
	m_selecting = false;
}

RectRender::~RectRender()
{
	vxdelete(m_pr);
}
	
void RectRender::Draw(const VX::SG::Node* node)
{
	using namespace VX::Math;
	matrix orthoproj = Ortho(0.0f, static_cast<float>(m_screenWidth), 0.0f, static_cast<float>(m_screenHeight), -1.0f, 1.0f);
	m_pr->SetProjMat(orthoproj);
	
	g->Disable(VG_DEPTH_TEST);
	g->Enable(VG_BLEND);
	g->BlendFunc(VG_SRC_ALPHA, VG_ONE_MINUS_SRC_ALPHA);
	

	m_pr->Clear();
	if (m_selecting) {
		m_pr->Begin(VX::PrimitiveRender::MODE_LINE_STRIP);
		m_pr->Color(vec4(1.0f,1.0f,1.0f,1.0f));
		m_pr->Normal(vec3(0.0f,0.0f,-1.0f));
		m_pr->Vertex(vec3(static_cast<float>(m_selectstart_x), static_cast<float>(m_selectstart_y), 0.0f));
		m_pr->Vertex(vec3(static_cast<float>(m_selectend_x)  , static_cast<float>(m_selectstart_y), 0.0f));
		m_pr->Vertex(vec3(static_cast<float>(m_selectend_x)  , static_cast<float>(m_selectend_y)  , 0.0f));
		m_pr->Vertex(vec3(static_cast<float>(m_selectstart_x), static_cast<float>(m_selectend_y)  , 0.0f));
		m_pr->Vertex(vec3(static_cast<float>(m_selectstart_x), static_cast<float>(m_selectstart_y), 0.0f));
		m_pr->End();
		m_pr->DrawAll();
	}
	g->Disable(VG_BLEND);
	g->Enable(VG_DEPTH_TEST);

}

void RectRender::Resize(u32 w, u32 h)
{
	m_screenWidth  = w;
	m_screenHeight = h;
}

void RectRender::StartRect(s32 sx, s32 sy, s32 ex, s32 ey)
{
	m_selectstart_x = sx;
	m_selectstart_y = sy;
	m_selectend_x = ex;
	m_selectend_y = ey;
	m_selecting = true;
}

void RectRender::EndRect(s32& sx, s32& sy, s32& ex, s32& ey)
{
	sx = m_selectstart_x;
	sy = m_selectstart_y;
	ex = m_selectend_x;
	ey = m_selectend_y;
	m_selecting = false;
}

void RectRender::SetViewMat(const VX::Math::matrix4x4& mat)
{
	
}

void RectRender::SetProjMat(const VX::Math::matrix4x4& mat)
{
	
}
	
void RectRender::Move(s32 dx, s32 dy)
{
	if (m_selecting) {
		m_selectend_x = dx;
		m_selectend_y = dy;
	}
}

b8 RectRender::OnClickUp()
{
	m_selecting = false;
	return true;
}

b8 RectRender::OnClick(s32 x, s32 y)
{
	m_selectend_x = m_selectstart_x = x;
	m_selectend_y = m_selectstart_y = y;
	m_selecting   = true;
	return true;
}
