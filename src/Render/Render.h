/*
 *
 * Render.h
 *
 * pure virtual class
 * 
 */


#ifndef __VX_RENDER_H__
#define __VX_RENDER_H__

#include "../VX/Type.h"

namespace VX {
	namespace SG {
		class Node;
	}
	namespace Math {
		struct matrix4x4;
	}
}

class IRender
{
protected:
	IRender(){};
	
public:
	virtual ~IRender(){};
	
	virtual void Draw(const VX::SG::Node* node) = 0;
	virtual void Resize(u32 w, u32 h) = 0;

	virtual void SetViewMat(const VX::Math::matrix4x4& mat) = 0;
	virtual void SetProjMat(const VX::Math::matrix4x4& mat) = 0;
	
};

#endif // __VX_RENDER_H__

