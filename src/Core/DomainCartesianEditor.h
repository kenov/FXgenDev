/*
 *
 * DomainCartesianEditor.h
 *
 */

#ifndef __DOMAIN_CARTESIAN_EDITOR_H__
#define __DOMAIN_CARTESIAN_EDITOR_H__

#include "DomainEditor.h"
#include "../VX/Type.h"
#include "../VX/SG/Node.h"
#include <map>
#include <vector>
#include <string>

namespace VX {
	namespace SG {
		class Group;
		class DomainCartesian;
	} // namespace SG
} // namespace VX


class DomainCartesianEditor : public DomainEditor
{
public:
	DomainCartesianEditor(VX::SG::Group* root, VX::SG::DomainCartesian* domain);
	~DomainCartesianEditor();
	
	void SetVoxelFlag(b8 flag);
	void SetDivisionFlag(b8 Flag);
	b8 hasVoxel();
	b8 hasDivision();

	b8 SetDefault();
	b8 Centering(u8 axisFlag = 0); // bit 1:x 2:y z:3
	b8 SetBBoxMinMax(const VX::Math::vec3& min, const VX::Math::vec3& max);
	b8 SetVoxelCount(const VX::Math::idx3& vox);
	b8 SetVoxelPitch(const VX::Math::vec3& pitch);
	b8 SetDivCount(const u32 divCount);
	b8 SetDivCount(const VX::Math::idx3& div);
	b8 SetUnit(const std::string& unit);
	b8 SetExportPolicy(const u32 policy ); // policy : 0: pitch, 1: voxelCount
	b8 SetSubdomain(const u8* subdomain);

	s32 GetExportPolicy() const; // policy : 0: pitch, 1: voxelCount
	u8 GetBBoxMinMax(VX::Math::vec3& min, VX::Math::vec3& max) const;
	u8 GetBBoxMinMaxSize(VX::Math::vec3& min, VX::Math::vec3& max, VX::Math::vec3& size) const;
	u8 GetVoxelCount(VX::Math::idx3& vox) const;
	u8 GetVoxelPitch(VX::Math::vec3& pitch) const;
	u8 GetDivCount(VX::Math::idx3& div) const;
	const std::string& GetUnit() const;
	const u8* GetSubdomainPtr() const;

	void ResetClipping();
	
// member
private:
	VX::SG::NodeRefPtr<VX::SG::Group> m_sg;
};


#endif // __DOMAIN_CARTESIAN_EDITOR_H__
