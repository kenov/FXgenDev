/*
 *
 * DomainEditor.h
 *
 */

#ifndef __DOMAIN_EDITOR_H__
#define __DOMAIN_EDITOR_H__

#include "../VX/Type.h"
#include "../VX/SG/Node.h"
#include <string>
#include <vector>

namespace VX {
	namespace SG {
		class Domain;
		class OuterBC;
		class Medium;
	} // namespace SG
} // namespace VX

class DomainEditor
{
public:
	virtual ~DomainEditor();

	void AddList(const VX::SG::OuterBC* bcs);
	void AddList(const VX::SG::Medium* meds);
	void ClearLists();
	void GetOuterBCNames(std::vector<std::string>& outerbcs) const;
	void GetMediumNames(std::vector<std::string>& meds) const;
	
	u32 GetAxisNum() const { return 6; }
	b8 GetAxisBCMedium(u32 axis, std::string& flowbc, std::string& med) const;
	b8 SetAxisBCMedium(u32 axis, const std::string& flowbc, const std::string& med);
	
	const std::vector<std::string>& GetUnitList();
	
	VX::SG::Domain* GetActiveDomain();
	const VX::SG::Domain* GetActiveDomain() const;

	enum DOMAIN_EDITOR_TYPE {
		DOMAIN_EDITOR,
		DOMAIN_EDITOR_CARTESIAN,
		DOMAIN_EDITOR_BCM
	};
	DOMAIN_EDITOR_TYPE GetType();
	
	virtual void ResetClipping() = 0;

protected:
	DomainEditor(DOMAIN_EDITOR_TYPE type, VX::SG::Domain* domain);

private:
	DOMAIN_EDITOR_TYPE m_type;
	VX::SG::NodeRefPtr<VX::SG::Domain> m_activeDomain;
	
	std::vector<const VX::SG::OuterBC*> m_outerbc;
	std::vector<const VX::SG::Medium* > m_medium;
};

#endif // __DOMAIN_EDITOR_H__
