/*
 *
 * SliceController.cpp
 *
 *
 */

#include "SliceController.h"

#include "../VX/SG/MeshCartesian.h"

//////////////////////////////////////////// SliceController::MeshCartesianController ////////////////////////////////////////////
void SliceController::MeshCartesianController::SetPosition(const u32 axis, const f32 pos, const u32 index_coord, const b8 interactive)
{
	m_node->SetSlicePosition(axis, pos, index_coord, interactive);
}


VX::Math::vec3 SliceController::MeshCartesianController::GetBBoxMin() const
{
	return m_node->GetOrigin();
}

VX::Math::vec3 SliceController::MeshCartesianController::GetBBoxMax() const
{
	return m_node->GetOrigin() + m_node->GetRegion();
}


f32 SliceController::MeshCartesianController::GetPosition(const u32 axis) const
{
	return m_node->GetSlicePosition(axis);
}
f32 SliceController::MeshCartesianController::GetPitch(const u32 axis) const
{
	return m_node->GetPitch()[axis];
}

u32 SliceController::MeshCartesianController::GetIdxPosition(const u32 axis, const f32 coord_pos) const
{
	return m_node->GetIdxPosition(axis, coord_pos);
}

f32 SliceController::MeshCartesianController::GetCoordPosition(const u32 axis, const u32 idx_pos) const
{
	return m_node->GetCoordPosition(axis, idx_pos);
}

//////////////////////////////////////////// SliceController::VoxelCartesianG_Controller ////////////////////////////////////////////
void SliceController::VoxelCartesianG_Controller::SetPosition(const u32 axis, const f32 pos, const u32 index_coord, const b8 interactive)
{
	m_node->SetSlicePosition(axis, pos, index_coord, interactive);
}


VX::Math::vec3 SliceController::VoxelCartesianG_Controller::GetBBoxMin() const
{
	return m_node->GetOrigin();
}

VX::Math::vec3 SliceController::VoxelCartesianG_Controller::GetBBoxMax() const
{
	return m_node->GetOrigin() + m_node->GetRegion();
}


f32 SliceController::VoxelCartesianG_Controller::GetPosition(const u32 axis) const
{
	return m_node->GetSlicePosition(axis);
}
f32 SliceController::VoxelCartesianG_Controller::GetPitch(const u32 axis) const
{
	return m_node->GetPitch()[axis];
}

u32 SliceController::VoxelCartesianG_Controller::GetIdxPosition(const u32 axis, const f32 coord_pos) const
{
	return m_node->GetIdxPosition(axis, coord_pos);
}

f32 SliceController::VoxelCartesianG_Controller::GetCoordPosition(const u32 axis, const u32 idx_pos) const
{
	return m_node->GetCoordPosition(axis, idx_pos);
}

//////////////////////////////////////////// SliceController::VoxelCartesianL_Controller ////////////////////////////////////////////
void SliceController::VoxelCartesianL_Controller::SetPosition(const u32 axis, const f32 pos, const u32 index_coord, const b8 interactive)
{
	m_node->SetSlicePosition(axis, pos, index_coord, interactive);
}


VX::Math::vec3 SliceController::VoxelCartesianL_Controller::GetBBoxMin() const
{
	return m_node->GetOrigin();
}

VX::Math::vec3 SliceController::VoxelCartesianL_Controller::GetBBoxMax() const
{
	return m_node->GetOrigin() + m_node->GetRegion();
}


f32 SliceController::VoxelCartesianL_Controller::GetPosition(const u32 axis) const
{
	return m_node->GetSlicePosition(axis);
}
f32 SliceController::VoxelCartesianL_Controller::GetPitch(const u32 axis) const
{
	return m_node->GetPitch()[axis];
}

u32 SliceController::VoxelCartesianL_Controller::GetIdxPosition(const u32 axis, const f32 coord_pos) const
{
	return m_node->GetIdxPosition(axis, coord_pos);
}

f32 SliceController::VoxelCartesianL_Controller::GetCoordPosition(const u32 axis, const u32 idx_pos) const
{
	return m_node->GetCoordPosition(axis, idx_pos);
}

/**
*	@brief change active local node
*/
void SliceController::VoxelCartesianL_Controller::changeTargetNode(VX::SG::Node* node)
{
	VX::SG::VoxelCartesianL* d = dynamic_cast<VX::SG::VoxelCartesianL*>(node);
	if(d==NULL){
		assert(false);
	}
	m_node = d;
}

//////////////////////////////////////////// SliceController::VoxelCartesianC_Controller ////////////////////////////////////////////
void SliceController::VoxelCartesianC_Controller::SetPosition(const u32 axis, const f32 pos, const u32 index_coord, const b8 interactive)
{
	m_node->SetSlicePosition(axis, pos, index_coord, interactive);
}

VX::Math::vec3 SliceController::VoxelCartesianC_Controller::GetBBoxMin() const
{
	return m_node->GetOrigin();
}

VX::Math::vec3 SliceController::VoxelCartesianC_Controller::GetBBoxMax() const
{
	return m_node->GetOrigin() + m_node->GetRegion();
}

f32 SliceController::VoxelCartesianC_Controller::GetPosition(const u32 axis) const
{
	return m_node->GetSlicePosition(axis);
}

f32 SliceController::VoxelCartesianC_Controller::GetPitch(const u32 axis) const
{
	return m_node->GetPitch()[axis];
}

u32 SliceController::VoxelCartesianC_Controller::GetIdxPosition(const u32 axis, const f32 coord_pos) const
{
	return m_node->GetIdxPosition(axis, coord_pos);
}

f32 SliceController::VoxelCartesianC_Controller::GetCoordPosition(const u32 axis, const u32 idx_pos) const
{
	return m_node->GetCoordPosition(axis, idx_pos);
}


//////////////////////////////////////////// SliceController::DomainCartesianController //////////////////////////////////////////////////////
void SliceController::DomainCartesianController::SetPosition(const u32 axis, const f32 pos, const u32 index_coord, const b8 interactive)
{
	m_node->SetSlicePosition(axis, pos, index_coord, interactive);
}

VX::Math::vec3 SliceController::DomainCartesianController::GetBBoxMin() const
{
	return m_node->GetOrigin();
}

VX::Math::vec3 SliceController::DomainCartesianController::GetBBoxMax() const
{
	return m_node->GetOrigin() + m_node->GetRegion();
}

f32 SliceController::DomainCartesianController::GetPosition(const u32 axis) const
{
	return m_node->GetSlicePosition(axis);
}

f32 SliceController::DomainCartesianController::GetPitch(const u32 axis) const
{
	return m_node->GetPitch()[axis];
}

u32 SliceController::DomainCartesianController::GetIdxPosition(const u32 axis, const f32 coord_pos) const
{
	return m_node->GetIdxPosition(axis, coord_pos);
}

f32 SliceController::DomainCartesianController::GetCoordPosition(const u32 axis, const u32 idx_pos) const
{
	return m_node->GetCoordPosition(axis, idx_pos);
}

b8 SliceController::DomainCartesianController::CheckStatus() const
{
	return true;
}

//////////////////////////////////////////// SliceController::DomainBCMController //////////////////////////////////////////////////////
void SliceController::DomainBCMController::SetPosition(const u32 axis, const f32 pos, const u32 index_coord, const b8 interactive)
{
	if (m_node->GetOctree())
	{
		m_node->SetSlicePosition(axis, pos, index_coord, interactive);
	}
	else
	{
		m_node->UpdateDistanceFieldSlice(axis, pos, m_sg, interactive);
	}
}

VX::Math::vec3 SliceController::DomainBCMController::GetBBoxMin() const
{
	return m_node->GetOrigin();
}

VX::Math::vec3 SliceController::DomainBCMController::GetBBoxMax() const
{
	return m_node->GetOrigin() + m_node->GetRegion();
}


f32 SliceController::DomainBCMController::GetPosition(const u32 axis) const
{
	if (m_node->GetOctree())
	{
		return m_node->GetSlicePosition(axis);
	}
	else
	{
		return m_node->GetDistanceSlicePosition(axis);
	}
}
f32 SliceController::DomainBCMController::GetPitch(const u32 axis) const
{
	if (m_node->GetOctree())
	{
		return m_node->GetPitch()[axis];
	}
	else
	{
		return m_node->GetDistanceSlicePitch(axis);
	}
}

u32 SliceController::DomainBCMController::GetIdxPosition(const u32 axis, const f32 coord_pos) const
{
	return m_node->GetIdxPosition(axis, coord_pos);
}

f32 SliceController::DomainBCMController::GetCoordPosition(const u32 axis, const u32 idx_pos) const
{
	return m_node->GetCoordPosition(axis, idx_pos);
}

b8 SliceController::DomainBCMController::CheckStatus() const
{
	return true;
}

b8 SliceController::DomainBCMController::CheckSupportSlideUnit(u32 unit) const
{
	if (m_node->GetOctree())
	{
		return true;
	}
	else
	{
		return unit == SLIDE_COORD;
	}
}

//////////////////////////////////////////// SliceController::DomainBCMColorMapController //////////////////////////////////////////////////////
b8 SliceController::DomainBCMColorMapController::CheckStatus() const
{
	if (m_node->GetOctree() || !m_node->GetUseDistanceBasedMethod())
	{
		return false;
	}
	else
	{
		return true;
	}
}

b8 SliceController::DomainBCMColorMapController::SetColorMap(u32 width, void* data)
{
	VX::SG::DistanceFieldSlice* df = dynamic_cast<VX::SG::DistanceFieldSlice*>(m_node->GetDistanceSlice(0));
	df->GetColorMap()->WriteBufferRGB24(data, width * 3);
	return true;
}

f32 SliceController::DomainBCMColorMapController::GetRangeMin() const
{
	f32 min, max;
	m_node->GetDistanceSliceRange(&min, &max);
	return min;
}

f32 SliceController::DomainBCMColorMapController::GetRangeMax() const
{
	f32 min, max;
	m_node->GetDistanceSliceRange(&min, &max);
	return max;
}

void SliceController::DomainBCMColorMapController::GetRangeLevels(s32* min, s32* max, u32* selectMax) const
{
	m_node->GetColorMapRangeLevels(min, max, selectMax);
}

void SliceController::DomainBCMColorMapController::SetRangeLevels(s32 min, s32 max)
{
	m_node->SetColorMapRangeLevels(min, max);
}

//////////////////////////////////////////// SliceController::MeshBCMController //////////////////////////////////////////////////////
void SliceController::MeshBCMController::SetPosition(const u32 axis, const f32 pos, const u32 index_coord, const b8 interactive)
{
	m_node->SetSlicePosition(axis, pos, index_coord, interactive);
}

VX::Math::vec3 SliceController::MeshBCMController::GetBBoxMin() const
{
	return m_node->GetOrigin();
}

VX::Math::vec3 SliceController::MeshBCMController::GetBBoxMax() const
{
	return m_node->GetOrigin() + m_node->GetRegion();
}


f32 SliceController::MeshBCMController::GetPosition(const u32 axis) const
{
	return m_node->GetSlicePosition(axis);
}
f32 SliceController::MeshBCMController::GetPitch(const u32 axis) const
{
	return m_node->GetPitch()[axis];
}

u32 SliceController::MeshBCMController::GetIdxPosition(const u32 axis, const f32 coord_pos) const
{
	return m_node->GetIdxPosition(axis, coord_pos);
}

f32 SliceController::MeshBCMController::GetCoordPosition(const u32 axis, const u32 idx_pos) const
{
	return m_node->GetCoordPosition(axis, idx_pos);
}

