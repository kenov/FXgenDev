/*
 *
 * DomainBCMEditor.h
 *
 */

#ifndef __DOMAIN_BCM_EDITOR_H__
#define __DOMAIN_BCM_EDITOR_H__

#include "DomainEditor.h"
#include "../VX/Type.h"
#include "../VX/SG/Node.h"
#include "../VOX/Pedigree.h"

#include <map>
#include <vector>
#include <string>


class SliceController;

namespace VX {
	namespace SG {
		class Group;
		class Geometry;
		class DomainBCM;
	} // namespace SG
} // namespace VX


class DomainBCMEditor : public DomainEditor
{
public:
	DomainBCMEditor(VX::SG::Group* root, VX::SG::DomainBCM* domain, VX::SG::Group* bcmGroup);
	~DomainBCMEditor();

	//b8 ApplyParam();
	
	b8 SetDefault();
	b8 Centering(u8 axisFlag = 0); // bit 1:x 2:y z:3
	b8 SetBBoxMinMax(const VX::Math::vec3& min, const VX::Math::vec3& max);
	b8 SetRootLength(const VX::Math::vec3& rootLength);
	b8 SetRootDims(const VX::Math::idx3& rootDims);
	b8 SetUnit(const std::string& unit);
	b8 SetBaseLevel(const u32 level);
	b8 SetMinLevel(const u32 level);
	b8 SetBlockSize(const VX::Math::idx3& size);
	b8 SetLeafOrdering(const u32 ordering);

	b8 SetExportPolicy(const u32 policy);
	b8 SetDivCount(const u32 divCount);

	b8 AddGeometryScope(const std::string& name, const u32 level,
						const std::string type, const f32 ratio, const f32 cell);
	b8 DeleteGeometryScope(const u32 idx);
	b8 ClearGeometryScope();

	b8 AddRegionScope(const VX::Math::vec3& origin, const VX::Math::vec3& region, const u32 level,
					  const std::string& type, const f32& ratio, const f32& cell);
	b8 DeleteRegionScope(const u32 idx);
	b8 EditRegionScope(const u32 idx, const VX::Math::vec3& origin, const VX::Math::vec3& region, const u32 level);
	b8 ClearRegionScope();

	void AddRegionScopeGeometry(const VX::Math::vec3& origin, const VX::Math::vec3& region, const u32 level);
	void DeleteRegionScopeGeometry(const u32 idx);

	b8 HasOctree();
	b8 DeleteOctree();
	b8 CreateOctree();
	b8 CreateOctree(const std::vector<VOX::Pedigree>& pedigrees);
	
	b8 GetBBoxMinMax(VX::Math::vec3& min, VX::Math::vec3& max) const;
	b8 GetBBoxMinMaxSize(VX::Math::vec3& min, VX::Math::vec3& max, VX::Math::vec3& size) const;
	b8 GetRootDims(VX::Math::idx3& rootDims) const;
	b8 GetRootLength(VX::Math::vec3& rootLength) const;
	u32 GetBaseLevel() const;
	u32 GetMinLevel() const;
	const VX::Math::idx3& GetBlockSize() const;
	u32 GetLeafOrdering() const;

	u32 GetExportPolicy() const;
	u32 GetDivCount() const;

	u32 GetMaxLevel() const;

	const std::string& GetUnit() const;

	u32 GetNumGeometryScope() const;
	b8  GetGeometryScope(const u32 idx, std::string& name, u32& level,
						 std::string& type, f32& ratio, f32& cell) const;

	u32 GetNumRegionScope() const;
	b8  GetRegionScope(const u32 idx, VX::Math::vec3& origin, VX::Math::vec3& region, u32& level,
					   std::string& marginType, f32& marginRatio, f32& marginCell) const;

	b8 GetUseDistanceBasedMethod() const;
	void SetUseDistanceBasedMethod(b8 use);
	
	u32 GetNumSubdivisionMargin() const;
	b8 GetSubdivisionMargin(u32 idx, f32& margin, u32& level, b8& isMaxLevel) const;
	void AddSubdivisionMargin(f32 margin, u32 level, b8 isMaxLevel);
	void DeleteSubdivisionMargin(u32 idx);
	void EditSubdivisionMargin(u32 idx, f32 margin, u32 level, b8 isMaxLavel);
	void ClearSubdivisionMargin();

	std::string GetInformationText() const;

	b8 GetPedigreeList( std::vector<VOX::Pedigree>& pedigrees ) const;
	
	const VX::SG::Group* GetSceneGraph() const { return m_sg; }

	void ResetGroupLayout();
	void UpdateGlobalBoxGeometry();
	void UpdateDistanceFieldSlices(b8 reset = false);

	void ResetClipping() { return; }

private:
	VX::SG::NodeRefPtr<VX::SG::Group> m_sg;
	VX::SG::NodeRefPtr<VX::SG::Group> m_bcmGroup;   // top of DomainBCM Group

	VX::SG::NodeRefPtr<VX::SG::Group> m_levelGroup; // Level Blocks Group
	VX::SG::NodeRefPtr<VX::SG::Group> m_sliceGroup; // Slice Plane Group
	VX::SG::NodeRefPtr<VX::SG::Group> m_paramGroup; // Parameter Geometry Group (e.g. GlobalBBox, ScopeBBoxes)

	VX::SG::NodeRefPtr<VX::SG::Geometry> m_globalBox; // Global BBox | RootGrid
	std::vector< VX::SG::NodeRefPtr<VX::SG::Geometry> > m_scopeBoxes; // Region Scope BBoxes

	u32 *m_lvColorTbl;
};

#endif // __DOMAIN_BCM_EDITOR_H__

