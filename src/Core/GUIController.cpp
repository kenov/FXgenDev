#include "../VX/VX.h"

#include "GUIController.h"
#include "ViewController.h"
#include "fxUI/fxGUI.h"
#include "fxgenCore.h"

GUIController::GUIController(s32 w, s32 h, ViewController* vctrl, fxgenCore* core)
{
	m_core  = core;
	m_vctrl = vctrl;
	using namespace fxGUI;
	m_gui = vxnew fxGUIManager();
	m_gui->Resize(w, h);
	BaseWindow* root = m_gui->GetRoot();
	m_updatingButton = false;
	
	m_menufunc = 0;
	m_menufunc_ptr = 0;

	const float wlcol[] = {0.85f,0.85f,0.85f,1.0f};
	const float llcol[] = {0.42f,0.42f,0.42f,1.0f};
	const float ldcol[] = {0.28f,0.28f,0.28f,1.0f};
	const float ddcol[] = {0.17f,0.17f,0.17f,1.0f};
	const float blcol[] = {0.00f,0.00f,0.00f,1.0f};
	
	int topz = 80;

	// menu
	m_topmenu = new fxMenu(m_gui, 54,50,280,80,topz++);
	m_topmenu->SetMenuFunc(onMenu_, this);
	root->AddChild(m_topmenu);

	topz = 50;
	// status
	m_menu = new fxFrame(m_gui, 45, 47, w, 47, llcol, topz++);
	root->AddChild(m_menu);
	m_sep = new fxFrame(m_gui, 47, 69, w, 1, blcol, topz++);
	root->AddChild(m_sep);
	
	// waku
	m_wakuline[0] = new fxFrame(m_gui, 44, 46, w, 1, blcol, topz++);
	root->AddChild(m_wakuline[0]);
	m_wakuline[1] = new fxFrame(m_gui, 44, 46, 1, h, blcol, topz++);
	root->AddChild(m_wakuline[1]);
	m_wakuline[2] = new fxFrame(m_gui, 44, h, w, 1, blcol, topz++);
	root->AddChild(m_wakuline[2]);
	m_wakuline[3] = new fxFrame(m_gui, w, 46, 1, h, blcol, topz++);
	root->AddChild(m_wakuline[3]);
	// inner waku
	m_wakuline2[0] = new fxFrame(m_gui, 45, 94, 2, h, llcol, topz++);
	root->AddChild(m_wakuline2[0]);
	m_wakuline2[1] = new fxFrame(m_gui, 45, h-7, w, 25, llcol, topz++);
	root->AddChild(m_wakuline2[1]);
	m_wakuline2[2] = new fxFrame(m_gui, w-6, 94, 2, h, llcol, topz++);
	root->AddChild(m_wakuline2[2]);
	
	// frame
	m_iconbase = new fxFrame(m_gui, 0, 39, 44, h, ldcol, topz++);
	root->AddChild(m_iconbase);
	m_frametop = new fxFrame(m_gui, 0, 41, w, 5, ldcol, topz++);
	root->AddChild(m_frametop);
	m_framebottom = new fxFrame(m_gui, 0, h - 45, w, 18, ldcol, topz++);
	root->AddChild(m_framebottom);
	m_frameright = new fxFrame(m_gui, w - 46, 41, 6, h, ldcol, topz++);
	root->AddChild(m_frameright);
	m_topframe = new fxFrame(m_gui, 0, 0, w, 39, ddcol, topz++);
	root->AddChild(m_topframe);
	m_topline = new fxFrame(m_gui, 0, 39, w, 2, blcol, topz++);
	root->AddChild(m_topline);
	
	s32 offset_y = 1;
	static const fxGraphicButton::MODE mode[ICON_NUM] = {
		fxGraphicButton::MODE_OPTION,
		fxGraphicButton::MODE_OPTION,
		fxGraphicButton::MODE_OPTION,
		fxGraphicButton::MODE_OPTION,
		fxGraphicButton::MODE_OPTION,
		fxGraphicButton::MODE_OPTION,
		fxGraphicButton::MODE_TOGGLE,
		fxGraphicButton::MODE_NORMAL
		/* fxGraphicButton::MODE_NORMAL */
	};
	BlankWindow* modeicons = new BlankWindow();
	root->AddChild(modeicons);
	BlankWindow* viewicons = new BlankWindow();
	root->AddChild(viewicons);
	for (s32 i = 0; i < ICON_NUM; ++i){
		m_icon[i] = new fxGraphicButton(m_gui, 0, 58 + offset_y, 1, offset_y, 44, 44, topz, mode[i], GUIController::buttonCallback_, i, this);
		if (i < 6)
			modeicons->AddChild(m_icon[i]);
		else if (i < 7)
			viewicons->AddChild(m_icon[i]);
		else
			root->AddChild(m_icon[i]);
		offset_y += 48;
	}
	
	// Set
	/*ViewController::OPMode opmode = m_vctrl->GetMode();
	m_icon[opmode]->Push();
	m_icon[ICON_PERSPECTIVE]->Push();*/
	
	fxGraphic* logo = new fxGraphic(m_gui, 0, 0, 80, 280, 117, 38, topz++);
	root->AddChild(logo);
	m_logo2 = new fxGraphic(m_gui, w - 76, 0, 80, 340, 78, 38, topz++);
	root->AddChild(m_logo2);
	
	const int status_height = 75;
	const float font_size = 18.0f;
	m_txt_memory = new fxText(m_gui, 55,status_height,"Alloced Memory = 0 [MB]",font_size,wlcol, topz++);
	root->AddChild(m_txt_memory);
	m_txt_vram = new fxText(m_gui, 260,status_height,"Alloced VRAM = 0 [MB]",font_size,wlcol, topz++);
	root->AddChild(m_txt_vram);
	m_txt_fps = new fxText(m_gui, 450,status_height,"FPS = 0",font_size,wlcol, topz++);
	root->AddChild(m_txt_fps);
	m_txt_tri = new fxText(m_gui, 550,status_height,"Triangle = 0",font_size,wlcol, topz++);
	root->AddChild(m_txt_tri);
	
	const int msg_height = h - 20;
	m_msg = new fxText(m_gui, 55,msg_height,"Ready.",font_size,wlcol, topz++);
	root->AddChild(m_msg);
	
	m_splash = new fxSplash(m_gui, 0,0, 80, 400, 164, 194, topz++);
	root->AddChild(m_splash);
	m_splash->StartFade(3.0);
	
	// Tree
	const int treeZ = 0;
	m_tree = new fxTree(m_core,this,m_gui, 53, 100, treeZ);
	root->AddChild(m_tree);

	m_icon[ICON_PERSPECTIVE]->Push();
	
	setLayout(w, h);
}

void GUIController::AddMenuFunc(void (*func)(s32, void*), void* thisptr)
{
	m_menufunc = func;
	m_menufunc_ptr = thisptr;
}

void GUIController::AddMenu(const s8* menuname ,s32 menuid, s8 key, s32 optkey, b8 visible)
{
	fxGUI::fxText* txt = m_topmenu->AddMenu(menuname, visible);
	txt->SetID(menuid);
	if (key)
		m_shortcuts[(optkey << 16)|key] = txt;

}

void GUIController::AddSubCheckMenu(const s8* menuname, s32 menuid, const s8* submenuname, s8 key, s32 optkey)
{
	AddSubMenu(menuname, menuid, submenuname, key, optkey);
}

void GUIController::AddSubMenu(const s8* menuname, s32 menuid, const s8* submenuname, s8 key, s32 optkey)
{
	fxGUI::fxText* txt = m_topmenu->AddSubMenu(menuname, menuid, submenuname);
	if (key)
		m_shortcuts[(optkey << 16)|key] = txt;
}

void GUIController::AddSubMenuMenu(const s8* menuname, const s8* submenuname ,s32 menuid, s8 key, s32 optkey)
{
	fxGUI::fxText* txt = m_topmenu->AddSubMenuMenu(menuname, submenuname);
	txt->SetID(menuid);
	if (key)
		m_shortcuts[(optkey << 16)|key] = txt;

}

void GUIController::CheckMenu(const s8* menuname, s32 menuid, b8 check)
{
	m_topmenu->CheckMenu(menuname, menuid, check);
}

void GUIController::EnableMenu(const s8* menuname, s32 menuid, b8 enable)
{
	m_topmenu->EnableMenu(menuname, menuid, enable);
}

void GUIController::PressTopMenu(const s8* menuname)
{

	m_topmenu->onPressTopMenu(menuname);

}

void GUIController::AddSubMenuSeparator(const s8* menuname)
{
	m_topmenu->AddSubMenu(menuname, 0, "-");
}

VX::SG::Node* GUIController::GetTreeMenuNode(){
	return m_topmenu->GetNode();
}

GUIController::~GUIController()
{
	vxdelete(m_gui);
}

void GUIController::onMenu(s32 id)
{
	if (m_menufunc)
		(*m_menufunc)(id, m_menufunc_ptr);

}

void GUIController::Resize(s32 w, s32 h)
{
	m_gui->Resize(w, h);
	setLayout(w, h);
}

void GUIController::updateButtons()
{
	m_updatingButton = true;
	ViewController::OPMode mode = m_vctrl->GetMode();
	if (!m_icon[mode]->IsPush())
		m_icon[mode]->Push();
	ViewController::VIEWMode vmode = m_vctrl->GetViewMode();
	if (m_icon[ICON_PERSPECTIVE]->IsPush() && vmode == ViewController::VIEW_ORTHO)
		m_icon[ICON_PERSPECTIVE]->Push();
	if (!m_icon[ICON_PERSPECTIVE]->IsPush() && vmode == ViewController::VIEW_PERSPECTIVE)
		m_icon[ICON_PERSPECTIVE]->Push();
	m_updatingButton = false;
}

void GUIController::buttonCallback(int i)
{
	if (i <= ICON_ROT_AXIS) { // View/Pick
		m_vctrl->SetMode(static_cast<ViewController::OPMode>(i));
	} else if (i == ICON_PERSPECTIVE){ // Perpective/Ortho
		if (m_icon[i]->IsPush()) {
			m_vctrl->SetViewMode(ViewController::VIEW_PERSPECTIVE);
		} else {
			m_vctrl->SetViewMode(ViewController::VIEW_ORTHO);
		}
	} else if (i == ICON_FIT) {
		m_vctrl->Fit(m_core->GetRoot(), true);
		m_core->SetCross(m_vctrl->GetRotCenter(), m_vctrl->GetRadius());
	} else if (i >= ICON_X_NEGATIVE) {
		ViewController::RotDir rd = static_cast<ViewController::RotDir>(i - ICON_X_NEGATIVE);
		m_vctrl->SetRotateDir(rd);
	}
	if (!m_updatingButton) {
		static const char* msg[] = {
			"Pick Mode",
			"Rect Mode",
			"View Translation",
			"View Rotation",
			"View Zoom",
			"View Rotation Axis",
			"Perspective Mode",
			"Fit to selections",
			"X- Axis view",
			"X+ Axis view",
			"Y- Axis view",
			"Y+ Axis view",
			"Z- Axis view",
			"Z+ Axis view",
		};
		SetMessage(msg[i]);
	}
}

void GUIController::Draw()
{
	updateButtons();
	m_tree->Update();
	m_gui->Draw();
}

b8 GUIController::MouseDown(s32 x, s32 y)
{
	m_gui->MouseDown(0, x, y);
	skGUI::BaseWindow* win = m_gui->GetActiveWindow();
	if (win)
		return true;
	return false;
}

b8 GUIController::MouseUp  (s32 x, s32 y)
{
	m_gui->MouseUp(0, x, y);
	skGUI::BaseWindow* win = m_gui->GetActiveWindow();
	if (win)
		return true;
	return false;
}

b8 GUIController::MouseRightDown(s32 x, s32 y)
{
	m_gui->MouseDown(1, x, y);
	skGUI::BaseWindow* win = m_gui->GetActiveWindow();
	if (win)
		return true;
	return false;
}

b8 GUIController::MouseRightUp  (s32 x, s32 y)
{
	m_gui->MouseUp(1, x, y);
	skGUI::BaseWindow* win = m_gui->GetActiveWindow();
	if (win)
		return true;
	return false;
}


void GUIController::MouseMove(s32 x, s32 y)
{
	m_gui->MouseMove(x, y);
}

b8 GUIController::IsVisibleTree()
{
	return m_tree->IsShow();
}

void GUIController::SetVisibleTree(b8 visible)
{
	m_tree->SetShow(visible);
	m_tree->ForceUpdate();
}

#define N_TREE "TreeMenu"
void GUIController::ShowTreeMenu(b8 visible, int x, int y, VX::SG::Node* node)
{
	if (visible) {
		m_topmenu->SetMenuPos(N_TREE, x , y + 50);
		if (node != NULL) {
			m_topmenu->SetNode(node);
		}
		m_topmenu->onPressTopMenu(N_TREE);
	}
}

void GUIController::UpdateTree(b8 cleartree)
{
	m_tree->SetSceneGraph(m_core->GetRoot(),cleartree);
	m_tree->ForceUpdate();
}


void GUIController::setLayout(s32 w, s32 h)
{
	m_topframe->SetSize(w, 39);
	m_topline->SetSize(w,2);
	m_iconbase->SetSize(44, h);
	m_frametop->SetSize(w, 5);
	m_framebottom->SetSize(w, 6);
	m_framebottom->SetPos(44, h - 6);
	m_frameright->SetPos(w - 4, 46);
	m_frameright->SetSize(4, h - 52);
	m_wakuline[0]->SetSize(w-48, 1);
	m_wakuline[1]->SetSize(1, h - 52);
	m_wakuline[2]->SetPos(44, h - 7);
	m_wakuline[2]->SetSize(w-48, 1);
	m_wakuline[3]->SetPos(w-5, 46);
	m_wakuline[3]->SetSize(1, h - 52);
	
	m_menu->SetSize(w - 49, 47);
	m_sep->SetSize(w - 54, 1);
	m_logo2->SetPos(w - 76, 0);
	
	m_wakuline2[0]->SetSize(2, h - 101);
	m_wakuline2[1]->SetPos(45, h - 32);
	m_wakuline2[1]->SetSize(w-52, 25);
	m_wakuline2[2]->SetPos(w-7,94);
	m_wakuline2[2]->SetSize(2, h - 101);
	
	m_msg->SetPos(55, h - 28);
	
	m_tree->SetSize(250, h - 138);
	
	m_splash->SetPos((w - m_splash->GetWidth())/2, (h - m_splash->GetHeight())/2);
}

void GUIController::SetFPS(int fps)
{
	char buf[256] = {};
	sprintf(buf, "FPS = %d", fps);
	m_txt_fps->SetText(buf);
}

void GUIController::SetMemory(unsigned long int size)
{
	unsigned int msize = static_cast<unsigned int>(size>>20);
	char buf[64] = {};
	sprintf(buf, "Alloced Memory = %u [MB]", msize);
	m_txt_memory->SetText(buf);
}
void GUIController::SetVRAM(unsigned int size)
{
	unsigned int msize = size>>20;
	char buf[64] = {};
	sprintf(buf, "Alloced VRAM = %u [MB]", msize);
	m_txt_vram->SetText(buf);
}

void GUIController::SetTriNum(int size)
{
	char buf[64] = {};
	sprintf(buf, "Triangle = %d", size);
	m_txt_tri->SetText(buf);
}

void GUIController::SetMessage(const char* msg)
{
	m_msg->SetText(msg);
}

void GUIController::KeyDown(s32 key, s32 optkey)
{
    VXLogD("GUIController::KeyDown start (key[%d],optkey[%d],(optkey << 16)|key)=[%d])\n",key,optkey,(optkey << 16)|key);
	std::map<s32,fxGUI::fxText*>::iterator it = m_shortcuts.find((optkey << 16)|key);
    
    if (it != m_shortcuts.end() && m_menufunc && it->second && it->second->IsEnable()){
        VXLogD("GUIController::KeyDown in dic short cut (menuid[%d])\n",it->second->GetID());
        
        (*m_menufunc)(it->second->GetID(), m_menufunc_ptr);
    }else{
        VXLogD("GUIController::KeyDown none in dic short cut\n");
    }

	m_gui->KeyDown(key);

}

void GUIController::KeyUp(s32 key)
{
	m_gui->KeyUp(key);
}

b8 GUIController::IsAnimating()
{
	// if you need,
	return m_splash->IsAnimating();
}

