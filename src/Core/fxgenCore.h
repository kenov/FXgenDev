/*
 *
 * fxgenCore.h
 * 
 */

#ifndef __FXGEN_CORE_H__
#define __FXGEN_CORE_H__

#include "../VX/Type.h"
#include "../VX/Math.h"
#include "../VX/SG/Node.h"
#include "../VX/SG/Medium.h"
#include "GUIController.h"

#include <string>
#include <vector>
#include <wx/string.h>
#include <wx/dynarray.h>

class SceneRender;
class TreeRender;
class SceneEditor;
class RectRender;
class AxisRender;
class DomainEditor;
class DomainCartesianEditor;
class DomainBCMEditor;
class VoxelCartesianEditor;
class SliceController;
class VoxelCartesianParam;
namespace VX {
	class Graphics;
	class PrimitiveRender;
	class SGPickerElm;
	namespace SG {
		class Group;
		class Cross;
		class DomainCartesian;
		class DomainBCM;
		class VoxelCartesianG;
		class VoxelCartesianL;
		class GUISettings;
		
	}
}

class fxgenCore
{
public:
	fxgenCore();
	~fxgenCore();
	
	b8 Init(GUIController* gui);
	void Deinit();
	
	void Draw();
	void Resize(u32 w, u32 h);

	void NewScene();
	
	// Load / Save
	VX::SG::Node* LoadModel(const s8* filename);
	VX::SG::Node* LoadDirModel(const s8* dirname);


	b8 LoadFile(const s8* filename);
	b8 LoadFiles(const std::vector<std::string>& files);
	b8 LoadDir(const s8* dirname);
	b8 LoadBCMedium(const s8* filename);
	b8 LoadDomain(const s8* filename);
	b8 LoadVoxelCartesian(const VoxelCartesianParam& param , std::string& errMsg);
	b8 LoadCellIDMesh(const s8* filename);
	b8 LoadFaceBC(const s8* filename);
	b8 LoadGeomemtryList(const s8* filename);

	b8 SaveFile(const s8* filename, const s8* mode, f32 scale = 0, b8 visibleOnly = true);
	b8 SaveDir(const s8* dirname, const s8* mode, f32 scale = 0, b8 visibleOnly = true);
	b8 SaveAllParameter(const s8* filename);
	b8 SaveGeometryList(const s8* filename);

	b8 SavePolylibFile(const s8* filename);
	b8 SaveBCMedium(const s8* filename);
	b8 SaveDomain(const s8* filename);
	b8 SaveFaceBC(const s8* filename);

	void SetViewMatrix(const VX::Math::matrix4x4& mat);
	void SetProjMatrix(const VX::Math::matrix4x4& mat);
	
	b8 SyncMediumTree(const std::vector<VX::SG::NodeRefPtr<const VX::SG::Medium> > mediumVector);
	
	const VX::SG::Node* GetRoot() const;
	VX::SG::Node* GetRoot() ;

	VX::SG::Group* GetSettingNode() const;
	VX::SG::GUISettings* GetGuiSettingNode() const;

	// Selection
	void ClearSelection();
	void SelectionPick(s32 x, s32 y, b8 selection);
	void EndSelectionPick(s32 x, s32 y);

	void ShowWire(b8 enable);
	b8 IsShowWire() const;
	void ShowDomainWire(b8 enable);
	b8 IsShowDomainWire() const;
	void ShowPolygon(b8 enable);
	b8 IsShowPolygon() const;
	
	void ShowAxis(b8 enable);
	b8 IsShowAxis() const;
	void ShowCross(b8 enable);
	b8 IsShowCross() const;
	
	void ShowMeshGrid(b8 enable);
	b8 IsShowMeshGrid() const;

	void UpdateIDColorTable();
	void SetBackgroundColor(const VX::Math::vec4& color);
	VX::Math::vec4 GetBackgroundColor()const;
	// Edit
	//void SelectionDelete();
	//void SelectionNew();
	//void SelectionViewMove(s32 old_mouse_x, s32 old_mouse_y, s32 mouse_x, s32 mouse_y);
	
	b8 CreateNewCartesianDomain(VX::SG::DomainCartesian* domain = NULL);
	b8 CreateNewBCMDomain(VX::SG::DomainBCM* domain = NULL);
	b8 CreateNewVoxelCartesian(VX::SG::VoxelCartesianG* domain = NULL);
	VX::SG::VoxelCartesianG* GetVoxelCartesianG();
	const VX::SG::VoxelCartesianG* GetVoxelCartesianG()const;
	void changeSelectedLocalVoxel(VX::SG::VoxelCartesianL* local);

	b8 DeleteDomain();
	b8 DeleteVoxelCartesian();

	b8 DeleteCellIDMesh();

	DomainEditor* GetCurrentDomainEditor();
	VoxelCartesianEditor* GetCurrentVoxelCartesianEditor();

	VX::SG::Group* GetSafeUserPolyonRoot();

	//Generate Polygon
	void ReLoadPolygon_p(const wxString& name, const wxString& posX_p, const wxString& posY_p,const wxString& posZ_p,const wxString& size_p);
	void ReLoadPolygon_r(const wxString name, const wxString posX,const wxString posY,const wxString posZ,const wxString width,const wxString depth,
				const wxString height,const wxString nor_vecX,const wxString nor_vecY,const wxString nor_vecZ,
				const wxString dir_vecX,const wxString dir_vecY,const wxString dir_vecZ);
	void ReLoadPolygon_c(const wxString& name, const wxString& posX,const wxString& posY,const wxString& posZ,const wxString& depth,
					const wxString& fanRad,const wxString& bossRad,const wxString& nor_vecX,
					const wxString& nor_vecY,const wxString& nor_vecZ);

	void LoadPolygon_p(const wxString& posX_p, const wxString& posY_p,const wxString& posZ_p,const wxString& size_p,const wxString row);
	void LoadPolygon_r(const wxString posX,const wxString posY,const wxString posZ,const wxString width,const wxString depth,
				const wxString height,const wxString nor_vecX,const wxString nor_vecY,const wxString nor_vecZ,
				const wxString dir_vecX,const wxString dir_vecY,const wxString dir_vecZ,const wxString row_r);
	void LoadPolygon_c(const wxString posX,const wxString posY,const wxString posZ,const wxString depth,const wxString fanRad,
				const wxString bossRad,const wxString nor_vecX,const wxString nor_vecY,const wxString nor_vecZ,const wxString row_c);


	void DeletePolygon(const wxString& name );
	void DeleteNode(VX::SG::Node* node);

	SliceController* GetSliceController(u32 slideMode);
	SliceController* GetSliceController_G(u32 slideMode);
	SliceController* GetSliceController_L(u32 slideMode);
	SliceController* GetSliceController_C(u32 slideMode);

	void SetSlideMode(u32 slideMode);


	void StartSelectionRect(s32 startx, s32 starty, s32 endx, s32 endy);
	void EndSelectionRect(b8 selection = true);
	void SelectionSetID(u32 id);
	VX::Math::vec4 GetIDColorTable(s32 id);

	void SetCross(const VX::Math::vec3& pos, f32 size);
	
	enum SELECTIONMODE {
		MODE_GEOMETRY,
		MODE_DOMAIN
	};
	void SelectionMode(SELECTIONMODE mode);
	SELECTIONMODE GetSelectionMode() const;

	const VX::SGPickerElm& GetPickInformation() const;
	std::string GetPickInformationStr() const;
private:
	b8 hasBCMeduim();

private:

	VX::Graphics* g;
	
	u32 m_screenWidth;
	u32 m_screenHeight;

	VX::SG::NodeRefPtr<VX::SG::Group> m_root;
	VX::SG::NodeRefPtr<VX::SG::Group> m_setting;
	VX::SG::NodeRefPtr<VX::SG::Cross> m_cross;
	SceneRender* m_modelRender;

	DomainEditor*		m_domainEditor;
	VoxelCartesianEditor*	m_VoxelCartesianEditor;

	SliceController* m_sliceController;
	SliceController* m_sliceController_G;
	SliceController* m_sliceController_L;
	SliceController* m_sliceController_C;
	GUIController*   m_gui;

	RectRender* m_rectRender;
	AxisRender* m_axisRender;
	SceneEditor* m_editor;
	
	VX::Math::matrix m_view;
	VX::Math::matrix m_proj;
	b8 m_showaxis;
	
	VX::Math::vec4 m_defaultIDColor;
	// background color
	VX::Math::vec4 m_backColor;
};

#endif // __FXGEN_CORE_H__

