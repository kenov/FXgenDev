//
//  ViewController.cpp
//

#include "../VX/VX.h"
#include "ViewController.h"
#include "../UI/vxWindow.h"
#include "../UI/DialogUtil.h"

#include "../VX/SG/SceneGraph.h"

namespace  {

const f32 ZOOM_INIT = 100.0f;

void getAdjustNearFar(const VX::SG::Node* node, s32 screen_width, s32 screen_height, f32 degree, f32& nearval, f32& farval, f32& radius, b8 selectonly, f32 zoom = ZOOM_INIT)
{
	using namespace VX::Math;
	vec3 bb_max,bb_min;
	b8 r = VX::SG::GetBBox(node, bb_max, bb_min, selectonly);
	if (!r)
		VX::SG::GetBBox(node, bb_max, bb_min, false);
	const f32 radius2 = length(bb_max - bb_min);
	radius = radius2 * 0.5f;
	//vec3 center = (bb_max + bb_min) * 0.5;
	f32 center_off = radius / Sin(ToRadian(degree * 0.5f)) * zoom / ZOOM_INIT;
	nearval = radius2 * 0.1f;
	farval = radius * 10.0f + center_off;
}


VX::Math::matrix getAdjustViewTrans(const VX::SG::Node* node, VX::Math::vec3& rotcenter, f32& camera_off, b8 selectonly)
{
	using namespace VX::Math;
	vec3 bb_max,bb_min;
	b8 r = VX::SG::GetBBox(node, bb_max, bb_min, selectonly);
	if (!r)
		VX::SG::GetBBox(node, bb_max, bb_min, false);
	const f32 radius2 = length(bb_max - bb_min);
	const f32 radius = radius2 * 0.5f;
	vec3 center = (bb_max + bb_min) * 0.5f;
	const f32 fov_degree = 30.0;
	camera_off = radius / Sin(ToRadian(fov_degree * 0.5f));
	VXLogD("BBox(%f,%f,%f)-(%f,%f,%f)\n", bb_max.x, bb_max.y, bb_max.z, bb_min.x, bb_min.y, bb_min.z);
	rotcenter = center;
	return Translation(-center.x, -center.y, -center.z);
}
	
VX::Math::matrix4x4 lerp(const VX::Math::matrix4x4& a, const VX::Math::matrix4x4& b, f32 rate)
{
	return VX::Math::matrix4x4(
	   (b.f[0 ]-a.f[0 ])*rate+a.f[0 ], (b.f[1 ]-a.f[1 ])*rate+a.f[1 ], (b.f[2 ]-a.f[2 ])*rate+a.f[2 ], (b.f[3 ]-a.f[3 ])*rate+a.f[3 ],
	   (b.f[4 ]-a.f[4 ])*rate+a.f[4 ], (b.f[5 ]-a.f[5 ])*rate+a.f[5 ], (b.f[7 ]-a.f[7 ])*rate+a.f[7 ], (b.f[8 ]-a.f[8 ])*rate+a.f[8 ],
	   (b.f[8 ]-a.f[8 ])*rate+a.f[8 ], (b.f[9 ]-a.f[9 ])*rate+a.f[9 ], (b.f[10]-a.f[10])*rate+a.f[10], (b.f[11]-a.f[11])*rate+a.f[11],
	   (b.f[12]-a.f[12])*rate+a.f[12], (b.f[13]-a.f[13])*rate+a.f[13], (b.f[14]-a.f[14])*rate+a.f[14], (b.f[15]-a.f[15])*rate+a.f[15]
	);
}
	

}

using namespace VX;

ViewController::ViewController()
{
	init();
	m_screenWidth  = 1;
	m_screenHeight = 1;
	m_radius = 1.0f;
	m_mouse_rot   = false;
	m_mouse_rot_axis  = false;
	m_mouse_zoom  = false;
	m_mouse_trans = false;
	m_zoom_init = ZOOM_INIT;
	m_initview = VX::Math::Identity();
	m_opmode = VIEW_ROT;//SELECT_POINT;
	m_viewmode = VIEW_PERSPECTIVE;
	m_degreeRotAnimation = 0;
}

ViewController::~ViewController()
{
}

void ViewController::init()
{
	Reset();
}

void ViewController::Resize(s32 w, s32 h)
{
	m_screenWidth  = w;
	m_screenHeight = h;
}
	
const VX::Math::vec3& ViewController::GetRotCenter() const
{
	return m_rotcenter;
}

f32 ViewController::GetRadius() const
{
	return m_radius;
}

void ViewController::Reset(const VX::SG::Node* node)
{
	m_rotmat = Math::Identity();
	Fit(node, false);
}
void ViewController::Fit(const VX::SG::Node* node, b8 selectonly)
{
	if (!node) {
		m_trans = Math::vec2(0,0);
		m_rotcenter = Math::vec3(0,0,0);
		m_zoom = ZOOM_INIT;
	} else {
		m_initview = getAdjustViewTrans(node, m_rotcenter, m_cameraoff, selectonly);
		m_trans = Math::vec2(0,0);
		m_zoom = ZOOM_INIT;
		getAdjustNearFar(node, m_screenWidth, m_screenHeight, 30, m_nearval, m_farval, m_radius, selectonly);// get near/far
		StartAnimation();
	}
}

void ViewController::MouseRotDown(f32 x, f32 y)
{
	m_mouse_rot = true;
	m_mouse.x = x;
	m_mouse.y = y;
}
void ViewController::MouseRotUp(f32 x, f32 y)
{
	m_mouse_rot = false;
}
void ViewController::MouseRotAxisDown(f32 x, f32 y)
{
	m_mouse_rot_axis = true;
	m_mouse.x = x;
	m_mouse.y = y;
}
void ViewController::MouseRotAxisUp(f32 x, f32 y)
{
	m_mouse_rot_axis = false;
}
void ViewController::MouseZoomDown(f32 x, f32 y)
{
	m_mouse_zoom = true;
	m_mouse.x = x;
	m_mouse.y = y;
}
void ViewController::MouseZoomUp(f32 x, f32 y)
{
	m_mouse_zoom = false;
}
void ViewController::MouseTransDown(f32 x, f32 y)
{
	m_mouse_trans = true;
	m_mouse.x = x;
	m_mouse.y = y;
}
void ViewController::MouseTransUp(f32 x, f32 y)
{
	m_mouse_trans = false;
}

void ViewController::MouseMove(f32 x, f32 y)
{
	// rot
	if (m_mouse_rot) {
		Math::vec4 rd = Math::vec4(x - m_mouse.x, y - m_mouse.y, 0.0f, 0.0f) * .2f;
		m_rotmat = Math::RotationX(rd.y)* Math::RotationY(rd.x) * m_rotmat;
	}
	
	// rot Axis Z
	if (m_mouse_rot_axis) {
		Math::vec3 axisZ = Math::vec3(0.0f, 0.0f, 1.0f);
		f32 rad_bef = Math::Atan2(y - m_screenHeight / 2, x - m_screenWidth / 2);
		f32 rad_aft = Math::Atan2(m_mouse.y - m_screenHeight / 2, m_mouse.x - m_screenWidth / 2);
		f32 degree  = Math::ToDegree(rad_aft - rad_bef);
		m_rotmat = Math::RotationAxis(axisZ, degree) * m_rotmat;
	}

	// trans
	if (m_mouse_trans) {
		const Math::vec2 ncenter = Math::vec2(x,y);
		const Math::vec2 ocenter = m_mouse;
		Math::vec2 diff = ncenter - ocenter;
		diff.y = -diff.y;
		
		const f32 feelingParam = 0.00065f * m_zoom/ZOOM_INIT;
		diff *= m_cameraoff * feelingParam;
		m_trans += diff;
	}
	
	// zoom
	if (m_mouse_zoom) {
		const Math::vec2 dist = Math::vec2(x,y) - m_mouse;
		m_zoom += - (dist.x + dist.y) * 0.001f*m_zoom;

		//set zoom up limitation
		const float INIT_WEIGHT = 0.1f;//0.1f;
		const f32 zoom_min = m_zoom_init * INIT_WEIGHT;
		const f32 zoom_max = m_zoom_init * 100.0f;
		if (m_zoom < zoom_min)
			m_zoom = zoom_min;
		if (m_zoom > zoom_max)
			m_zoom = zoom_max;

		//calc auto far;
		const f32 zoom_offset = m_cameraoff * m_zoom / ZOOM_INIT;
		m_farval = zoom_offset * 2.0f;
		m_nearval = m_farval * 0.005f;
	}
	
	m_mouse.x = x;
	m_mouse.y = y;
}

void ViewController::OpenFreeRotateDlg()
{
	UI::vxWindow* win = UI::vxWindow::GetTopWindow();
	UI::FreeRotateDlg(win, true, this);
}

void ViewController::CloseFreeRotateDlg()
{
	UI::vxWindow* win = UI::vxWindow::GetTopWindow();
	UI::FreeRotateDlg(win, false, this);
}

void ViewController::RotateAxis(f32 degree)
{
	Math::vec3 axisZ = Math::vec3(0.0f, 0.0f, -1.0f);
	m_rotmat = Math::RotationAxis(axisZ, degree) * m_rotmat;
	m_old_rotmat = Math::RotationAxis(axisZ, m_degreeRotAnimation * m_animrate) * m_old_rotmat;
	m_degreeRotAnimation = m_degreeRotAnimation * (1.0f - m_animrate) + degree;
	StartAnimation();
}

void ViewController::ZoomUp(f32 scale, const VX::SG::Node* node, b8 selectonly)
{
	if (!node) {
		m_trans = Math::vec2(0,0);
		m_rotcenter = Math::vec3(0,0,0);
		m_zoom = ZOOM_INIT;
	} else {
		m_initview = getAdjustViewTrans(node, m_rotcenter, m_cameraoff, selectonly);
		if (scale == 0.f)
			return;
		m_old_scale = ((m_zoom / ZOOM_INIT) - m_old_scale) * m_animrate + m_old_scale;
		m_zoom /= scale;
		//set zoom up limitation
		const float INIT_WEIGHT = 0.1f;//0.1f;
		const f32 zoom_min = m_zoom_init * INIT_WEIGHT;
		const f32 zoom_max = m_zoom_init * 100.0f;
		if (m_zoom < zoom_min)
			m_zoom = zoom_min;
		if (m_zoom > zoom_max)
			m_zoom = zoom_max;

		//calc auto far;
		const f32 zoom_offset = m_cameraoff * m_zoom / ZOOM_INIT;
		m_farval = zoom_offset * 5.0f;
		m_nearval = m_farval * 0.005f;
		StartAnimation();
	}
}

Math::matrix ViewController::GetViewMatrix()
{
	using namespace Math;
	static const Math::matrix toZUP = Math::RotationX(-90.0f);
	
	const f32 scale = m_zoom / ZOOM_INIT;
	if (m_animrate > 0.0f) {
		const f64 animationTime = 0.8;
		m_animrate = static_cast<f32>((VX::GetTimeCount() - m_anim_starttime) / animationTime);
		m_animrate = smoothstep(0.0f,1.0f,m_animrate);
		//printf("anim=%f\n",m_animrate);
		if (m_animrate >= 1.0f) {
			m_animrate = 0.0f;
			m_degreeRotAnimation = 0.0f;
		} else if ( m_degreeRotAnimation != 0.0f) {
			Math::vec3 axisZ = Math::vec3(0.0f, 0.0f, -1.0f);
			const matrix4x4 rot = Math::RotationAxis(axisZ, m_degreeRotAnimation * m_animrate) * m_old_rotmat;
			matrix iv = m_initview;
			iv._43 -= m_cameraoff * scale;
			const matrix4x4 view = iv * Translation(m_trans) * Translation(m_rotcenter) * rot * toZUP * Translation(-m_rotcenter);
			return view;
		} else {
			const vec3 rotcenter = lerp(m_old_rotcenter, m_rotcenter, m_animrate);
			const vec2 trans     = lerp(m_old_trans,     m_trans,     m_animrate);
			const quaternion oq = QuatNormalize(ToQuaternion(m_old_rotmat));
			const quaternion nq = QuatNormalize(ToQuaternion(m_rotmat));
			const quaternion sq = QuatSlerp(oq, nq, m_animrate);
			const matrix4x4 rot = ToMatrix(QuatNormalize(sq));
			matrix iv = m_initview;
			iv._41 = (iv._41 - m_old_vcenter.x) * m_animrate + m_old_vcenter.x;
			iv._42 = (iv._42 - m_old_vcenter.y) * m_animrate + m_old_vcenter.y;
			iv._43 = (iv._43 - m_old_vcenter.z) * m_animrate + m_old_vcenter.z;
			
			const float cameraoff = (m_cameraoff - m_old_cameraoff) * m_animrate + m_old_cameraoff;
			const float nscale = (scale - m_old_scale) * m_animrate + m_old_scale;
			const f32 scaleoff = cameraoff * nscale;
			iv._43 -= scaleoff;
			
			//printf("VC(%.3f,%.3f,%.3f)\n",iv._41,iv._42,iv._43);
			const matrix4x4 view = iv * Translation(trans) * Translation(rotcenter) * rot * toZUP * Translation(-rotcenter);
			return view;
		}
	}
	m_old_rotcenter = m_rotcenter;
	m_old_rotmat    = m_rotmat;
	m_old_trans     = m_trans;
	m_old_cameraoff = m_cameraoff;
	m_old_scale     = scale;
	m_old_vcenter   = vec3(m_initview._41,m_initview._42,m_initview._43);
	
	matrix iv = m_initview;
	iv._43 -= m_cameraoff * scale;
	const matrix4x4 view = iv * Translation(m_trans) * Translation(m_rotcenter) * m_rotmat * toZUP * Translation(-m_rotcenter);
	return view;
}

Math::matrix ViewController::GetProjMatrix() const
{
	const f32 scale = m_zoom / ZOOM_INIT;
	const f32 aspect = m_screenWidth / static_cast<f32>(m_screenHeight);
	if (m_viewmode == VIEW_ORTHO)
		return VX::Math::Ortho(-m_radius * aspect * scale, m_radius * aspect * scale,
							   m_radius  * scale, -m_radius * scale, m_nearval, m_farval);
	else
		return VX::Math::PerspectiveFov(30.0, aspect, m_nearval, m_farval);
}

void ViewController::SetRotateDir(RotDir dir)
{
	switch (dir) {
		case DIR_X_NEGATIVE: m_rotmat = VX::Math::RotationY( 90.0f); break;
		case DIR_X_POSITIVE: m_rotmat = VX::Math::RotationY(-90.0f); break;
		case DIR_Y_NEGATIVE: m_rotmat = VX::Math::Identity(); break;
		case DIR_Y_POSITIVE: m_rotmat = VX::Math::RotationY(180.0f); break;
		case DIR_Z_NEGATIVE: m_rotmat = VX::Math::RotationY(-90.0f) *VX::Math::RotationZ(90.0f); break;
		case DIR_Z_POSITIVE: m_rotmat = VX::Math::RotationY( 90.0f) *VX::Math::RotationZ(90.0f); break;
		default:
			break;
	}
	StartAnimation();
}

void ViewController::StartAnimation()
{
	m_anim_starttime = VX::GetTimeCount() - 1.0E-3f;
	m_animrate = 1.0E-3f;
}

b8 ViewController::IsAnimating() const
{
	return (m_animrate > 0.0f);
}

b8 ViewController::GetBBox(const VX::SG::Node* node, VX::Math::vec3& box_max, VX::Math::vec3& box_min, b8 selectonly , b8 ignoreDomain )
{
	return VX::SG::GetBBox(node, box_max, box_min, selectonly,ignoreDomain);
}
	