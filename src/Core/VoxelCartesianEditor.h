/*
 *
 * VoxelCartesianEditor.h
 *
 */

#ifndef __VoxelCartesianEditor_H__
#define __VoxelCartesianEditor_H__

#include "../VX/Type.h"
#include "../VX/SG/Node.h"

#include <string>
#include <vector>

namespace VX {
	namespace SG {
		class VoxelCartesianG;
		class Group;
	} // namespace SG
} // namespace VX

class VoxelCartesianEditor
{
public:
	VoxelCartesianEditor(VX::SG::Group* root, VX::SG::VoxelCartesianG*  domain);
	virtual ~VoxelCartesianEditor();

	VX::SG::VoxelCartesianG* GetActiveDomain();
	const VX::SG::VoxelCartesianG* GetActiveDomain() const;
	b8 SetDefault();

private:
	VX::SG::NodeRefPtr<VX::SG::VoxelCartesianG> m_activeDomain;
	

};

#endif // __DOMAIN_EDITOR_H__
