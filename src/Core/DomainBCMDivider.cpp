#include "DomainBCMDivider.h"
#include "../VX/SG/DomainBCM.h"
#include "../VX/SG/Transform.h"
#include "../VX/SG/GeometryBVH.h"


using namespace std;
using namespace VX::Math;

namespace {

b8 split(const std::string& str, const char delimiter, std::vector<std::string>& list)
{
	list.clear();

	std::string istr = str;

	size_t p = 0;
	while( (p = istr.find(delimiter)) != std::string::npos )
	{
		if(p != 0){
			list.push_back( istr.substr(0, p) );
		}
		istr.erase(istr.begin(), istr.begin() + p + 1);
	}

	if( istr.length() != 0 ) list.push_back(istr);

	return true;
}

// TODO Transform
const VX::SG::Node* GetSGNode(const std::string& name, const VX::SG::Group* root)
{
	using namespace std;
	vector<string> nameList;
	split(name, '/', nameList);
	
	if( nameList[0] != root->GetName() )
	{
		return NULL;
	}

	const VX::SG::Group* grp  = root;
	for(int i = 1; i < nameList.size() -1; i++){
		const string& lname = nameList[i];
		
		bool find = false;
		for(int n = 0; n < grp->GetChildCount(); n++){
			const VX::SG::Node* nn = grp->GetChild(n);

			if( nn->GetType() != VX::SG::NODETYPE_GROUP && nn->GetType() != VX::SG::NODETYPE_TRANSFORM )
				continue;

			const string& nname = nn->GetName();

			if( lname == nname )
			{
				grp = static_cast<const VX::SG::Group*>(nn);
				find = true;
				break;
			}
		}

		if( !find )
			return NULL;
	}

	const string& lname = nameList[ nameList.size()-1 ];
	for(int n = 0; n < grp->GetChildCount(); n++)
	{
		const VX::SG::Node* nn = grp->GetChild(n);

		if( nn->GetType() != VX::SG::NODETYPE_GEOMETRY &&
			nn->GetType() != VX::SG::NODETYPE_GROUP    &&
			nn->GetType() != VX::SG::NODETYPE_TRANSFORM )
			continue;

		if( lname == nn->GetName() )
		{
			return nn;
		}
	}

	return NULL;
}

vector<f32> makeLevelMarginLookup(const vector<VX::SG::DomainBCM::SubdivisionMargin>& margins, u32 maxLevel)
{
	using std::min; using std::max;
	using namespace VX::SG;

	vector<f32> ret(maxLevel + 1, 0);
	for (vector<DomainBCM::SubdivisionMargin>::const_iterator i = margins.begin(); i != margins.end(); ++i)
	{
		u32 level = i->level;
		if (level == DomainBCM::SubdivisionMargin::MAX_LEVEL || maxLevel <= level)
		{
			level = maxLevel;
		}
		ret[level] = max(ret[level], i->margin);
	}
	for (u32 j = 0; j < ret.size(); ++j)
	{
		for (u32 i = 0; i < j; ++i)
		{
			ret[i] = max(ret[i], ret[j]);
		}
	}
	return ret;
}

inline void expand(BBox& bbox, f32 margin)
{
	bbox.min_ -= vec3(margin, margin, margin);
	bbox.max_ += vec3(margin, margin, margin);
}

inline b8 isOverlap(const BBox& a, const BBox& b)
{
	for (int i = 0; i < 3; ++i)
	{
		if (a.min_[i] > b.max_[i] || a.max_[i] < b.min_[i])
		{
			return false;
		}
	}
	return true;
}

void makeObb(const BBox& bbox, const matrix& m, vec3& center, vec3& halfextent, vec3* basis)
{
	center = (m * vec4((bbox.min_ + bbox.max_) / 2, 1)).xyz();
	const vec3 originalExtent = (bbox.max_ - bbox.min_) / 2;
	const vec3 e[] = { vec3(originalExtent.x, 0, 0), vec3(0, originalExtent.y, 0), vec3(0, 0, originalExtent.z) };
	for (int i = 0; i < 3; ++i)
	{
		vec3 f = (m * vec4(e[i], 0)).xyz();
		halfextent[i] = length(f);
		basis[i] = normalize(f);
	}
}


} // namespace

////////////////////////////////////////////////////////////////////////////////////

DomainBCMDividerBase::DomainBCMDividerBase(const VX::SG::DomainBCM* domain, const VX::SG::Group* sg)
	: m_sg(sg), m_rgnScopeList(domain->GetRegionScopeList())
{
	using namespace VX::SG;

	idx3 dims = domain->GetRootDims();
	vec3 rgn = domain->GetRegion();
	m_org = domain->GetOrigin();
	m_rootLength = vec3(rgn.x / dims.x, rgn.y / dims.y, rgn.z / dims.z);
	m_baseLevel = domain->GetBaseLevel();
	m_minLevel = domain->GetMinLevel();
	
	//m_marginRatio = 1.0 / domain->GetBlockSize().x;
	if (domain->getMarginType() == "Ratio") {
		m_marginRatio = domain->getMarginRatio();
	} else {
		m_marginRatio = domain->getMarginCell() / domain->GetBlockSize().x;
	}

	const vector<DomainBCM::GeometryScope*>& gscopes = domain->GetGeometryScopeList();
	m_geoScopeList.reserve(gscopes.size());
	for (vector<DomainBCM::GeometryScope*>::const_iterator i = gscopes.begin(); i != gscopes.end(); ++i)
	{
		const Node* node = GetSGNode((*i)->name, sg);
		if (node)
		{
			m_geoScopeList.push_back(vxnew GeoScope(node, (*i)->level));
		}
		else
		{
			VXLogW("[%s] does not exist in current scene\n", (*i)->name.c_str());
		}
	}

	m_rootGrid = new VOX::RootGrid(dims.x, dims.y, dims.z);
}

DomainBCMDividerBase::~DomainBCMDividerBase()
{
	delete m_rootGrid;
	for (std::vector<GeoScope*>::iterator i = m_geoScopeList.begin(); i != m_geoScopeList.end(); ++i)
	{
		vxdelete(*i);
	}
}

BBox DomainBCMDividerBase::DefineSearchRegion(const VOX::Pedigree& pedigree, int maxLevel)
{
#if 1
	int level = pedigree.getLevel();
	int px = pedigree.getX();
	int py = pedigree.getY();
	int pz = pedigree.getZ();

	int rootID = pedigree.getRootID();
	int ix = m_rootGrid->rootID2indexX(rootID);
	int iy = m_rootGrid->rootID2indexY(rootID);
	int iz = m_rootGrid->rootID2indexZ(rootID);

	int max0 = pedigree.getUpperBound() - 1; // 2^level - 1
	double d = 1.0 / (max0 + 1); // Block Size

	double x0 = ix + px * d;
	double y0 = iy + py * d;
	double z0 = iz + pz * d;
	double x1 = ix + (px + 1) * d;
	double y1 = iy + (py + 1) * d;
	double z1 = iz + (pz + 1) * d;

	// Set margin to be robust against ripple efficient
	if( level < maxLevel ){
		int n = 1 << (maxLevel - level - 1);
		double dd = d * (double)(n - 1) / n;
		if(!(px == 0    && m_rootGrid->isOuterBoundary( rootID, 0 /* VOX::M_X */ ))) x0 -= dd;
		if(!(py == 0    && m_rootGrid->isOuterBoundary( rootID, 2 /* VOX::M_Y */ ))) y0 -= dd;
		if(!(pz == 0    && m_rootGrid->isOuterBoundary( rootID, 4 /* VOX::M_Z */ ))) z0 -= dd;
		if(!(px == max0 && m_rootGrid->isOuterBoundary( rootID, 1 /* VOX::P_X */ ))) x1 += dd;
		if(!(py == max0 && m_rootGrid->isOuterBoundary( rootID, 3 /* VOX::P_Y */ ))) y1 += dd;
		if(!(pz == max0 && m_rootGrid->isOuterBoundary( rootID, 5 /* VOX::P_Z */ ))) z1 += dd;
	}

	// origin and scale fitting
	x0 = static_cast<double>(m_org.x) + x0 * static_cast<double>(m_rootLength.x);
	y0 = static_cast<double>(m_org.y) + y0 * static_cast<double>(m_rootLength.y);
	z0 = static_cast<double>(m_org.z) + z0 * static_cast<double>(m_rootLength.z);
	x1 = static_cast<double>(m_org.x) + x1 * static_cast<double>(m_rootLength.x);
	y1 = static_cast<double>(m_org.y) + y1 * static_cast<double>(m_rootLength.y);
	z1 = static_cast<double>(m_org.z) + z1 * static_cast<double>(m_rootLength.z);

	VX::Math::vec3 _min( static_cast<float>(x0), static_cast<float>(y0), static_cast<float>(z0) );
	VX::Math::vec3 _max( static_cast<float>(x1), static_cast<float>(y1), static_cast<float>(z1) );

#else
	// for Test (not consider 2 to 1)
	int lv = pedigree.getLevel();
	VX::Math::idx3 pidx(pedigree.getX(), pedigree.getY(), pedigree.getZ());
	VX::Math::idx3 ridx(m_rootGrid->rootID2indexX(pedigree.getRootID()),
		                m_rootGrid->rootID2indexY(pedigree.getRootID()),
						m_rootGrid->rootID2indexZ(pedigree.getRootID()));

	VX::Math::vec3 rgn( m_rootLength.x / (1 << lv),
		                m_rootLength.y / (1 << lv),
						m_rootLength.z / (1 << lv));
	VX::Math::vec3 org( m_org.x + (static_cast<f32>(pidx.x) * rgn.x) + (static_cast<f32>(ridx.x) * m_rootLength.x),
		                m_org.y + (static_cast<f32>(pidx.y) * rgn.y) + (static_cast<f32>(ridx.y) * m_rootLength.y),
		                m_org.z + (static_cast<f32>(pidx.z) * rgn.z) + (static_cast<f32>(ridx.z) * m_rootLength.z));

	VX::Math::vec3 _min( org );
	VX::Math::vec3 _max( org + rgn );
#endif

	return BBox(_min, _max);
}

////////////////////////////////////////////////////////////////////////////////////

DomainBCMDividerIntersection::DomainBCMDividerIntersection(const VX::SG::DomainBCM* domain, const VX::SG::Group* sg)
	: DomainBCMDividerBase(domain, sg)
{
}

DomainBCMDividerIntersection::~DomainBCMDividerIntersection()
{
}

b8 DomainBCMDividerIntersection::IntersectGeometry(const VX::SG::Node* node, const BBox& bbox)
{
	if( !node )
		return false;

	using namespace VX::SG;
	using namespace VX::Math;

	bool hit = false;
	const NODETYPE t = node->GetType();
	if( t == NODETYPE_GROUP )
	{
		const Group* grp = static_cast<const Group*>(node);
		s32 n = grp->GetChildCount();
		for(s32 i = 0; i < n; i++){
			hit |= IntersectGeometry(grp->GetChild(i), bbox);
		}
	}
	else if( t == NODETYPE_TRANSFORM)
	{
		const Transform* trs = static_cast<const Transform*>(node);
		const matrix& m = trs->GetMatrix();
		b8 push = false;
		if (m != Identity())
			push = true;
		if(push)
			m_stack.Push(m);
		s32 n = trs->GetChildCount();
		for( s32 i = 0; i < n; i++){
			hit |= IntersectGeometry(trs->GetChild(i), bbox);
		}
		if(push)
			m_stack.Pop();
	}
	else if( t == NODETYPE_GEOMETRY )
	{
		const GeometryBVH* geo = static_cast<const GeometryBVH*>(node);
		/* TODO Transform
		const matrix world = m_stack->GetMatrix();
		const matrix invWorld = Inverse(world);
		*/
		hit |= geo->IntersectBBox(&bbox.max_, &bbox.min_);
	}

	return hit;
}


VOX::Divider::NodeType DomainBCMDividerIntersection::operator() (const VOX::Pedigree& pedigree)
{
	using namespace VX::SG;

	u32 level = pedigree.getLevel();
	if (level < m_minLevel) return BRANCH;

	NodeType ret = LEAF_ACTIVE;

	if (level < m_baseLevel)
	{
		BBox box = DefineSearchRegion(pedigree, m_baseLevel);
		expand(box, m_marginRatio / (1 << m_baseLevel));
		if (IntersectGeometry(m_sg, box))
		{
			ret = BRANCH;
		}
	}

	for (vector<DomainBCM::RegionScope*>::const_iterator i = m_rgnScopeList.begin(); i != m_rgnScopeList.end(); ++i)
	{
		if (level < (*i)->level)
		{
			BBox scope((*i)->origin, (*i)->origin + (*i)->region);
			BBox box = DefineSearchRegion(pedigree, (*i)->level);
			expand(box, m_marginRatio / (1 << (*i)->level));
			if (isOverlap(box, scope))
			{
				ret = BRANCH;
			}
		}
	}

	for (vector<GeoScope*>::const_iterator i = m_geoScopeList.begin(); i != m_geoScopeList.end(); ++i)
	{
		if (level < (*i)->level)
		{
			BBox box = DefineSearchRegion(pedigree, (*i)->level);
			expand(box, m_marginRatio / (1 << (*i)->level));
			// TODO: m_stack.push(xform);
			if (IntersectGeometry((*i)->node, box))
			{
				ret = BRANCH;
			}
			// TODO: m_stack.pop();
		}
	}

	return ret;
}

////////////////////////////////////////////////////////////////////////////////////

DomainBCMDividerDistance::DomainBCMDividerDistance(const VX::SG::DomainBCM* domain, const VX::SG::Group* sg)
	: DomainBCMDividerBase(domain, sg)
{
	using namespace VX::SG;
	using std::max;

	u32 maxlevel = m_baseLevel;
	for (vector<DomainBCM::RegionScope*>::const_iterator i = m_rgnScopeList.begin(); i != m_rgnScopeList.end(); ++i)
	{
		maxlevel = max(maxlevel, (*i)->level);
	}
	for (vector<GeoScope*>::const_iterator i = m_geoScopeList.begin(); i != m_geoScopeList.end(); ++i)
	{
		maxlevel = max(maxlevel, (*i)->level);
	}

	const std::vector<DomainBCM::SubdivisionMargin>& margins = domain->GetSubdivisionMargins();
	m_margins = makeLevelMarginLookup(margins, maxlevel);
}

DomainBCMDividerDistance::~DomainBCMDividerDistance()
{
}

b8 DomainBCMDividerDistance::TestIntersection(const VX::SG::Node* node, const BBox& bbox, f32 margin)
{
	using namespace VX::SG;

	if (!node) return false;

	b8 hit = false;
	switch (node->GetType())
	{
	case NODETYPE_GROUP:
		{
			const Group* grp = static_cast<const Group*>(node);
			s32 cnt = grp->GetChildCount();
			for (s32 i = 0; i < cnt; ++i)
			{
				hit |= TestIntersection(grp->GetChild(i), bbox, margin);
				if (hit) break;
			}
		}
		break;

	case NODETYPE_TRANSFORM:
		{
			const Transform* xform = static_cast<const Transform*>(node);
			const matrix& m = xform->GetMatrix();
			b8 push = (m == Identity());
			if (push)
			{
				m_stack.Push(m);
			}
			s32 cnt = xform->GetChildCount();
			for (s32 i = 0; i < cnt; ++i)
			{
				hit |= TestIntersection(xform->GetChild(i), bbox, margin);
				if (hit) break;
			}
			if (push)
			{
				m_stack.Pop();
			}
		}
		break;

	case NODETYPE_GEOMETRY:
		{
			const GeometryBVH* geom = static_cast<const GeometryBVH*>(node);
			const matrix& m = m_stack.GetMatrix();
			if (m != Identity())
			{
				vec3 center;
				vec3 halfextent;
				vec3 basis[3];
				makeObb(bbox, Inverse(m), center, halfextent, basis);
				hit |= geom->IntersectOBB(center, halfextent + vec3(margin, margin, margin), basis);
			} else {
				hit |= geom->TestDistanceAABB(bbox.min_, bbox.max_, margin);
			}
		}
		break;

	default:
		;
	}
	
	return hit;
}

VOX::Divider::NodeType DomainBCMDividerDistance::operator() (const VOX::Pedigree& pedigree)
{
	using namespace VX::SG;

	u32 level = pedigree.getLevel();
	if (level < m_minLevel) return BRANCH;

	NodeType ret = LEAF_ACTIVE;

	if (level < m_baseLevel)
	{
		BBox box = DefineSearchRegion(pedigree, m_baseLevel);
		if (TestIntersection(m_sg, box, m_margins[level]))
		{
			ret = BRANCH;
		}
	}

	for (vector<DomainBCM::RegionScope*>::const_iterator i = m_rgnScopeList.begin(); i != m_rgnScopeList.end(); ++i)
	{
		if (level < (*i)->level)
		{
			BBox scope((*i)->origin, (*i)->origin + (*i)->region);
			BBox box = DefineSearchRegion(pedigree, (*i)->level);
			if (isOverlap(box, scope))
			{
				ret = BRANCH;
			}
		}
	}

	for (vector<GeoScope*>::const_iterator i = m_geoScopeList.begin(); i != m_geoScopeList.end(); ++i)
	{
		if (level < (*i)->level)
		{
			BBox box = DefineSearchRegion(pedigree, (*i)->level);
			// TODO: m_stack.push(xform);
			if (TestIntersection((*i)->node, box, m_margins[level]))
			{
				ret = BRANCH;
			}
			// TODO: m_stack.pop();
		}
	}

	return ret;
}
