/*
 *
 * VoxelCartesianEditor
 *
 */

#include "VoxelCartesianEditor.h"
#include "../VX/VX.h"
#include "../VX/SG/SceneGraph.h"


VoxelCartesianEditor::VoxelCartesianEditor(VX::SG::Group* root, VX::SG::VoxelCartesianG* domain)
{
	m_activeDomain = domain;
}

VoxelCartesianEditor::~VoxelCartesianEditor()
{
	
}

b8 VoxelCartesianEditor::SetDefault()
{
	//TODO:fuchi
	assert(false);
	VX::SG::VoxelCartesianG* domain = GetActiveDomain();
	if(!domain){
		assert(false);
		return false;
	}
	/*
	VX::Math::vec3 min, max;
	GetBBox(m_sg, max, min, false, true);
	domain->SetBBoxMinMax(min, max);
	domain->SetVoxelCount(VX::Math::idx3(1, 1, 1));
	domain->SetDivCount(VX::Math::idx3(1, 1, 1));
	domain->UpdateGeometry();	
	*/
	return true;
}

VX::SG::VoxelCartesianG* VoxelCartesianEditor::GetActiveDomain()
{
	return m_activeDomain;
}

const VX::SG::VoxelCartesianG* VoxelCartesianEditor::GetActiveDomain() const
{
	return m_activeDomain;
}
