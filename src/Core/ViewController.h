//
//  ViewController.h
//

#ifndef INCLUDE_VX_VIEWCONTROLLER_H
#define INCLUDE_VX_VIEWCONTROLLER_H

#include "../VX/Type.h"
#include "../VX/Math.h"
namespace VX{
	namespace SG {
		class Node;
	}
}

class ViewController
{
public:
	ViewController();
	~ViewController();
	
	void Resize(s32 w, s32 h);
	void Reset(const VX::SG::Node* node = 0);
	void Fit(const VX::SG::Node* node = 0, b8 selectonly = false);
	void MouseRotDown(f32 x, f32 y);
	void MouseRotUp(f32 x, f32 y);
	void MouseZoomDown(f32 x, f32 y);
	void MouseZoomUp(f32 x, f32 y);
	void MouseTransDown(f32 x, f32 y);
	void MouseTransUp(f32 x, f32 y);
	void MouseRotAxisDown(f32 x, f32 y);
	void MouseRotAxisUp(f32 x, f32 y);
	void MouseMove(f32 x, f32 y);

	void RotateAxis(f32 degree);
	void ZoomUp(f32 scale, const VX::SG::Node* node, b8 selectonly);

	const VX::Math::vec3& GetRotCenter() const;
	f32 GetRadius() const;
	
	VX::Math::matrix GetViewMatrix();
	VX::Math::matrix GetProjMatrix() const;
	b8 GetBBox(const VX::SG::Node* node, VX::Math::vec3& box_max, VX::Math::vec3& box_min, b8 selectonly = false, b8 ignoreDomain = false);
	
	enum OPMode{
		SELECT_POINT = 0,
		SELECT_RECT,
		VIEW_TRANS,
		VIEW_ROT,
		VIEW_ZOOM,
		VIEW_ROT_AXIS
	};
	OPMode GetMode() const   { return m_opmode; }
	void SetMode(OPMode mode) {
		m_opmode = mode;
		if (m_opmode == VIEW_ROT_AXIS) {
			OpenFreeRotateDlg();
		} else {
			CloseFreeRotateDlg();
		}
	};
	
	enum RotDir{
		DIR_X_NEGATIVE,
		DIR_X_POSITIVE,
		DIR_Y_NEGATIVE,
		DIR_Y_POSITIVE,
		DIR_Z_NEGATIVE,
		DIR_Z_POSITIVE
	};
	void SetRotateDir(RotDir mode);
	
	enum VIEWMode {
		VIEW_PERSPECTIVE,
		VIEW_ORTHO
	};
	void SetViewMode(VIEWMode view){ m_viewmode = view; }
	VIEWMode GetViewMode() const { return m_viewmode; }
		
	void StartAnimation();
	b8 IsAnimating() const;
	
private:
	void init();
	void OpenFreeRotateDlg();
	void CloseFreeRotateDlg();

	OPMode m_opmode;
	VIEWMode m_viewmode;
	
	VX::Math::vec2 m_mouse;
	b8 m_mouse_rot, m_mouse_rot_axis, m_mouse_zoom, m_mouse_trans;
	s32 m_screenWidth, m_screenHeight;
	f32 m_radius;
	
	VX::Math::vec3 m_rotcenter;
	VX::Math::vec2 m_trans;
	VX::Math::matrix m_rotmat;
	f32 m_zoom, m_zoom_init;
	f32 m_cameraoff;
	
	f32 m_nearval, m_farval;
	VX::Math::matrix4x4 m_initview;
	
	f64 m_anim_starttime;
	f32 m_animrate;
	f32 m_degreeRotAnimation;
	VX::Math::matrix m_old_rotmat;
	VX::Math::vec3   m_old_rotcenter;
	VX::Math::vec2   m_old_trans;
	VX::Math::vec3   m_old_vcenter;
	f32              m_old_cameraoff;
	f32              m_old_scale;
};

#endif // INCLUDE_VX_VIEWCONTROLLER_H
