/*
 * DomainBCMDivider.h
 *
 */
/// @file
/// 階層化格子のブロック分割判定クラス群
/// 

#ifndef __DOMAIN_BCM_DIVIDER_H__
#define __DOMAIN_BCM_DIVIDER_H__

#include "../VX/Type.h"
#include "../VX/Math.h"
#include "../VX/MatrixStack.h"
#include "../VOX/Divider.h"
#include "../VX/SG/DomainBCM.h"
#include <vector>
#include <utility>


///
/// 軸平行な境界ボックス(AABB).
/// Axis-Aligned Boundary/Bounding Box
///
struct BBox
{
	VX::Math::vec3 min_; ///< AABB の最小座標
	VX::Math::vec3 max_; ///< AABB の最大座標

	/// コンストラクタ
	BBox(const VX::Math::vec3& min, const VX::Math::vec3& max) : min_(min), max_(max) {}
};


///
/// スコープ設定をサポートする分割判定の基底クラス.
///
class DomainBCMDividerBase : public VOX::Divider
{
public:
	/// コンストラクタ
	///
	/// @param[in] domain 判定対象のドメイン
	/// @param[in] sg シーングラフ
	///
	DomainBCMDividerBase(const VX::SG::DomainBCM* domain, const VX::SG::Group* sg);

	/// デストラクタ
	~DomainBCMDividerBase();

	//NodeType operator() (const VOX::Pedigree& pedigree);

protected:
	/// ブロックの判定領域を計算する.
	/// 2to1 constraint を考慮して対象ブロックと隣接ブロックを統合した判定領域を計算する.
	///
	/// @param[in] pedigree ブロックの Pedigree
	/// @param[in] maxLevel 最大の細分化レベル. 
	///                     maxLevel 以上は 2to1 constraint を考慮する必要が無い.
	/// @return 判定領域
	///
	BBox DefineSearchRegion(const VOX::Pedigree& pedigree, int maxLevel);

	/// 計算時の形状スコープ保持クラス
	struct GeoScope
	{
		const VX::SG::Node* node;
		const u32 level;
		GeoScope(const VX::SG::Node* node, const u32 level) : node(node), level(level) {}
	};

	const VX::SG::Group* m_sg; ///< シーングラフ
	const std::vector<VX::SG::DomainBCM::RegionScope*> m_rgnScopeList; ///< 領域スコープ
	std::vector<GeoScope*> m_geoScopeList; ///< 形状スコープ
	u32 m_baseLevel;             ///< 基本の細分化レベル
	u32 m_minLevel;              ///< 最小細分化レベル. レベルがこれ以下の場合は必ず細分化対象とする.
	f32 m_marginRatio;           ///< 距離マージン(比率)
	VX::Math::vec3 m_org;        ///< ドメイン全体の基点座標
	VX::Math::vec3 m_rootLength; ///< ドメイン全体のサイズ
	VOX::RootGrid* m_rootGrid;   ///< マルチルート時のルートブロック配置
};


///
/// 交差に基づく分割判定クラス
///
class DomainBCMDividerIntersection : public DomainBCMDividerBase
{
public:
	/// コンストラクタ
	DomainBCMDividerIntersection(const VX::SG::DomainBCM* domain, const VX::SG::Group* sg);

	/// デストラクタ
	~DomainBCMDividerIntersection();

	NodeType operator() (const VOX::Pedigree& pedigree);

private:
	/// シーングラフのノードとの交差判定を行う
	///
	/// @param[in] node 対象シーン
	/// @param[in] bbox 判定の対象領域
	/// @return 判定結果. 交差している場合 true
	///
	b8 IntersectGeometry(const VX::SG::Node* node, const BBox& bbox);
		
	VX::MatrixStack m_stack; ///< シーングラフトラバース時の変換行列スタック
};


///
/// 距離に基づく交差判定クラス
///
class DomainBCMDividerDistance : public DomainBCMDividerBase
{
public:
	/// コンストラクタ
	DomainBCMDividerDistance(const VX::SG::DomainBCM* domain, const VX::SG::Group* sg);

	/// デストラクタ
	~DomainBCMDividerDistance();
	
	NodeType operator() (const VOX::Pedigree& pedigree);

private:
	/// シーングラフのノードとの距離判定を行う.
	///
	/// @param[in] node 対象シーン
	/// @param[in] box 判定の対象領域
	/// @param[in] margin 判定の距離マージン. 対象シーンとの距離が margin 以下なら分割対象とする
	/// @return 判定結果. 距離が距離マージン以下の場合は true
	///
	b8 TestIntersection(const VX::SG::Node* node, const BBox& box, f32 margin);

	VX::MatrixStack m_stack;    ///< シーングラフトラバース時の変換行列スタック
	std::vector<f32> m_margins; ///< レベル毎の距離マージン
};


#endif // __DOMAIN_BCM_DIVIDER_H__