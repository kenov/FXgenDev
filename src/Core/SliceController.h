/*
 *
 *  SliceController.h
 *
 */
/// @file SliceController.h
/// スライスダイアログのコントローラクラス
///

#ifndef __CORE_SLICE_CONTROLLER_H__
#define __CORE_SLICE_CONTROLLER_H__

#include "../VX/SG/Node.h"
#include "../VX/SG/MeshCartesian.h"
#include "../VX/SG/MeshBCM.h"
#include "../VX/SG/DomainCartesian.h"
#include "../VX/SG/DomainBCM.h"
#include "../VX/SG/VoxelCartesianG.h"
#include "../VX/SG/VoxelCartesianL.h"
#include "../VX/SG/VoxelCartesianC.h"
#include "../VX/Type.h"
#include "../VX/Math.h"

///
/// スライスダイアログのコントローラ
///
class SliceController
{
private:
	/// 対象となるスライス情報の同期コントローラの基底クラス
	class InnerController
	{
	public:
		InnerController(){ }
		virtual ~InnerController() { }
		void virtual SetPosition(const u32 axis, const f32 pos, const u32 index_coord, const b8 interactive ) = 0;
		f32 virtual GetPosition(const u32 axis) const = 0;
		f32 virtual GetPitch(const u32 axis) const = 0; 
		u32 virtual GetIdxPosition(const u32 axis, const f32 coord_pos) const = 0;
		f32 virtual GetCoordPosition(const u32 axis, const u32 idx_pos) const = 0;
		VX::Math::vec3 virtual GetBBoxMin()  const = 0;
		VX::Math::vec3 virtual GetBBoxMax()  const = 0;
		b8 virtual CheckStatus() const { return true; }
		b8 virtual CheckSupportSlideUnit(u32 unit) const { return true; }
		void virtual changeTargetNode(VX::SG::Node* node){}
		virtual void updateController() {}

	protected:
	};

	/// Cartesian Mesh の同期コントローラ
	class MeshCartesianController : public InnerController
	{
	public:
		MeshCartesianController( VX::SG::MeshCartesian* node ) : m_node(node) { }
		~MeshCartesianController() { }
		
		void SetPosition(const u32 axis, const f32 pos, const u32 index_coord, const b8 interactive);
		f32 GetPosition(const u32 axis) const;
		f32 GetPitch(const u32 axis) const;
		u32 GetIdxPosition(const u32 axis, const f32 coord_pos) const;
		f32 GetCoordPosition(const u32 axis, const u32 idx_pos) const;
		VX::Math::vec3 GetBBoxMin() const;
		VX::Math::vec3 GetBBoxMax() const;

	private:
		VX::SG::NodeRefPtr< VX::SG::MeshCartesian > m_node;
	};

	/// BCM Mesh の同期コントローラ
	class MeshBCMController : public InnerController
	{
	public:
		MeshBCMController( VX::SG::MeshBCM* node ) : m_node(node) { }
		~MeshBCMController() { }

		void SetPosition(const u32 axis, const f32 pos, const u32 index_coord, const b8 interactive);
		f32 GetPosition(const u32 axis) const;
		f32 GetPitch(const u32 axis) const;
		u32 GetIdxPosition(const u32 axis, const f32 coord_pos) const;
		f32 GetCoordPosition(const u32 axis, const u32 idx_pos) const;
		VX::Math::vec3 GetBBoxMin() const;
		VX::Math::vec3 GetBBoxMax() const;

	private:
		VX::SG::NodeRefPtr< VX::SG::MeshBCM > m_node;
	};

	/// Voxel Cartesian G の同期コントローラ
	class VoxelCartesianG_Controller : public InnerController
	{
	public:
		VoxelCartesianG_Controller( VX::SG::VoxelCartesianG* node ) : m_node(node) { }
		~VoxelCartesianG_Controller() { }
		
		void SetPosition(const u32 axis, const f32 pos, const u32 index_coord, const b8 interactive);
		f32 GetPosition(const u32 axis) const;
		f32 GetPitch(const u32 axis) const;
		u32 GetIdxPosition(const u32 axis, const f32 coord_pos) const;
		f32 GetCoordPosition(const u32 axis, const u32 idx_pos) const;
		VX::Math::vec3 GetBBoxMin() const;
		VX::Math::vec3 GetBBoxMax() const;

	private:
		VX::SG::NodeRefPtr< VX::SG::VoxelCartesianG > m_node;
	};
	/// Voxel Cartesian L の同期コントローラ
	class VoxelCartesianL_Controller : public InnerController
	{
	public:
		VoxelCartesianL_Controller( VX::SG::VoxelCartesianL* node ) : m_node(node) { }
		~VoxelCartesianL_Controller() { }
		
		void SetPosition(const u32 axis, const f32 pos, const u32 index_coord, const b8 interactive);
		f32 GetPosition(const u32 axis) const;
		f32 GetPitch(const u32 axis) const;
		u32 GetIdxPosition(const u32 axis, const f32 coord_pos) const;
		f32 GetCoordPosition(const u32 axis, const u32 idx_pos) const;
		VX::Math::vec3 GetBBoxMin() const;
		VX::Math::vec3 GetBBoxMax() const;
		void changeTargetNode(VX::SG::Node* node);
	private:
		VX::SG::NodeRefPtr< VX::SG::VoxelCartesianL > m_node;
	};

	/// Voxel Cartesian C の同期コントローラ
	class VoxelCartesianC_Controller : public InnerController
	{
	public:
		VoxelCartesianC_Controller( VX::SG::VoxelCartesianC* node ) : m_node(node) { }
		~VoxelCartesianC_Controller() { }
		
		void SetPosition(const u32 axis, const f32 pos, const u32 index_coord, const b8 interactive);
		f32 GetPosition(const u32 axis) const;
		f32 GetPitch(const u32 axis) const;
		u32 GetIdxPosition(const u32 axis, const f32 coord_pos) const;
		f32 GetCoordPosition(const u32 axis, const u32 idx_pos) const;
		VX::Math::vec3 GetBBoxMin() const;
		VX::Math::vec3 GetBBoxMax() const;

	private:
		VX::SG::NodeRefPtr< VX::SG::VoxelCartesianC > m_node;
	};

	/// Cartesian Domain の同期コントローラ
	class DomainCartesianController     : public InnerController
	{
	public:
		DomainCartesianController( VX::SG::DomainCartesian* node, const VX::SG::Group* sg ) : m_node(node), m_sg(sg) { }
		~DomainCartesianController() { }
		
		void SetPosition(const u32 axis, const f32 pos, const u32 index_coord, const b8 interactive);
		f32 GetPosition(const u32 axis) const;
		f32 GetPitch(const u32 axis) const;
		u32 GetIdxPosition(const u32 axis, const f32 coord_pos) const;
		f32 GetCoordPosition(const u32 axis, const u32 idx_pos) const;
		VX::Math::vec3 GetBBoxMin() const;
		VX::Math::vec3 GetBBoxMax() const;
		b8 CheckStatus() const;
	private:
		VX::SG::NodeRefPtr< VX::SG::DomainCartesian > m_node;
		VX::SG::NodeRefPtr<const VX::SG::Group> m_sg;
	};

	/// BCM Domain の同期コントローラ
	class DomainBCMController     : public InnerController
	{
	public:
		DomainBCMController( VX::SG::DomainBCM* node, const VX::SG::Group* sg ) : m_node(node), m_sg(sg) { }
		~DomainBCMController() { }
		
		void SetPosition(const u32 axis, const f32 pos, const u32 index_coord, const b8 interactive);
		f32 GetPosition(const u32 axis) const;
		f32 GetPitch(const u32 axis) const;
		u32 GetIdxPosition(const u32 axis, const f32 coord_pos) const;
		f32 GetCoordPosition(const u32 axis, const u32 idx_pos) const;
		VX::Math::vec3 GetBBoxMin() const;
		VX::Math::vec3 GetBBoxMax() const;
		b8 CheckStatus() const;
		b8 CheckSupportSlideUnit(u32 unit) const;
	private:
		VX::SG::NodeRefPtr< VX::SG::DomainBCM > m_node;
		VX::SG::NodeRefPtr<const VX::SG::Group> m_sg;
	};

	class ColorMapController
	{
	public:
		virtual ~ColorMapController() {}

		virtual b8 CheckStatus() const { return false; }
		virtual b8 SetColorMap(u32 width, void* data) = 0;
		virtual f32 GetRangeMin() const = 0;
		virtual f32 GetRangeMax() const = 0;
		virtual void GetRangeLevels(s32* min, s32* max, u32* selectMax) const = 0;
		virtual void SetRangeLevels(s32 min, s32 max) = 0;
	};

	class DomainBCMColorMapController : public ColorMapController
	{
	public:
		explicit DomainBCMColorMapController(VX::SG::DomainBCM* node) : m_node(node) {}
		~DomainBCMColorMapController() {}

		b8 CheckStatus() const;
		b8 SetColorMap(u32 width, void* data);
		f32 GetRangeMin() const;
		f32 GetRangeMax() const;
		void GetRangeLevels(s32* min, s32* max, u32* selectMax) const;
		void SetRangeLevels(s32 min, s32 max);
	private:
		VX::SG::NodeRefPtr< VX::SG::DomainBCM > m_node;
	};

public:
	/// スライダー数値の単位
	enum SlideUnit {
		SLIDE_IDX = 0,  ///< 分割ブロックのインデックス単位
		SLIDE_COORD = 1 ///< 座標値
	};

	/// スライダー操作時の更新モード
	enum SlideMode {
		MODE_FAST   = 0, ///< スライダー操作中は低負荷の簡易更新を行う. 操作完了後に詳細な更新を行う
		MODE_DETAIL = 1  ///< スライダー操作中も詳細な更新を行う
	};

	/// コンストラクタ
	SliceController(VX::SG::MeshCartesian* node)
	{
		m_controller = vxnew MeshCartesianController(node);
		m_colorMap = NULL;
		m_slideMode = 0;
		m_slideUnit = SLIDE_IDX;
		for (int i = 0; i < 3; i++) {
			m_spinPitch[i] = m_controller->GetPitch(i);
		}
	}

	/// コンストラクタ
	SliceController(VX::SG::MeshBCM* node)
	{
		m_controller = vxnew MeshBCMController(node);
		m_colorMap = NULL;
		m_slideMode = 0;
		m_slideUnit = SLIDE_IDX;
		for (int i = 0; i < 3; i++) {
			m_spinPitch[i] = m_controller->GetPitch(i);
		}
	}

	/// コンストラクタ
	SliceController(VX::SG::VoxelCartesianG* node)
	{
		m_controller = vxnew VoxelCartesianG_Controller(node);
		m_colorMap = NULL;
		m_slideMode = 0;
		m_slideUnit = SLIDE_IDX;
		for (int i = 0; i < 3; i++) {
			m_spinPitch[i] = m_controller->GetPitch(i);
		}
	}

	/// コンストラクタ
	SliceController(VX::SG::VoxelCartesianL* node)
	{
		m_controller = vxnew VoxelCartesianL_Controller(node);
		m_colorMap = NULL;
		m_slideMode = 0;
		m_slideUnit = SLIDE_IDX;
		for (int i = 0; i < 3; i++) {
			m_spinPitch[i] = m_controller->GetPitch(i);
		}
	}

	/// コンストラクタ
	SliceController(VX::SG::VoxelCartesianC* node)
	{
		m_controller = vxnew VoxelCartesianC_Controller(node);
		m_colorMap = NULL;
		m_slideMode = 0;
		m_slideUnit = SLIDE_IDX;
		for (int i = 0; i < 3; i++) {
			m_spinPitch[i] = m_controller->GetPitch(i);
		}
	}

	/// コンストラクタ
	SliceController(VX::SG::DomainCartesian* node, const VX::SG::Group* sg)
	{
		m_controller = vxnew DomainCartesianController(node, sg);
		m_colorMap = NULL;
		m_slideMode = MODE_FAST;
		m_slideUnit = SLIDE_COORD;
		for (u32 i = 0; i < 3; ++i)
		{
			m_spinPitch[i] = node->GetPitch()[i];
		}
	}

	/// コンストラクタ
	SliceController(VX::SG::DomainBCM* node, const VX::SG::Group* sg)
	{
		m_controller = vxnew DomainBCMController(node, sg);
		m_colorMap = vxnew DomainBCMColorMapController(node);
		m_slideMode = MODE_FAST;
		m_slideUnit = SLIDE_COORD;
		for (u32 i = 0; i < 3; ++i)
		{
			m_spinPitch[i] = m_controller->GetPitch(i);
		}
	}

	~SliceController()
	{
		vxdelete(m_colorMap);
		vxdelete(m_controller);
	}
	
	/// スライダー操作時の更新モードを設定する
	void SetSlideMode(u32 mode)
	{
		m_slideMode = mode;
	}

	/// スライダーのドラッグ操作によりスライス位置を設定する
	///
	/// @param[in] axis 操作軸(X/Y/Z)
	/// @param[in] pos 現在のスライダー単位によるスライス位置.
	///
	void SetPositionDrag(const u32 axis, const f32 pos)
	{
		m_controller->SetPosition(axis, pos, m_slideUnit, m_slideMode == 0 ? true : false);
	}

	/// スライス位置を設定する
	///
	/// @param[in] axis 操作軸(X/Y/Z)
	/// @param[in] pos 現在のスライダー単位によるスライス位置.
	///
	void SetPosition(const u32 axis, const f32 pos)
	{
		m_controller->SetPosition(axis, pos, m_slideUnit, false);
	}

	/// 座標値による現在のスライス位置
	f32 GetPosition(const u32 axis)
	{
		return m_controller->GetPosition(axis);
	}

	/// インデックス単位による現在のスライス位置
	u32 GetIdxPosition(const u32 axis, const f32 coord_pos) const
	{
		return m_controller->GetIdxPosition(axis, coord_pos);
	}
	
	/// インデックス位置から座標値を計算する
	///
	/// @param[in] axis 操作軸(X/Y/Z)
	/// @param[in] idx_pos インデックス単位での位置
	/// @return 座標値
	///
	f32 GetCoordPosition(const u32 axis, const u32 idx_pos) const
	{
		return m_controller->GetCoordPosition(axis, idx_pos);
	}

	/// 操作対象のスピンボタンによる移動量の標準値
	f32 GetPitch(const u32 axis) const 
	{
		return m_controller->GetPitch(axis);
	}

	/// スピンボタンによる移動量を設定する
	void SetSpinPitch(f32 pitch, const u32 axis)
	{
		m_spinPitch[axis] = pitch;
	}

	/// スピンボタンによる移動量
	f32 GetSpinPitch(const u32 axis) const
	{
		return m_controller->GetPitch(axis);
	}

	/// 操作対象の領域の最小値
	VX::Math::vec3 GetBBoxMin() const {
		return m_controller->GetBBoxMin();
	}

	/// 操作対象の領域の最小値
	VX::Math::vec3 GetBBoxMax() const {
		return m_controller->GetBBoxMax();
	}

	/// スライダーの数値単位
	///
	/// @return 現在の数値単位(SLIDE_IDX or SLIDE_COORD)
	///
	u32 GetSlideUnit() const {
		return m_slideUnit;
	}

	/// スライダーの操作単位を設定する
	///
	/// @param[in] unit 数値単位(SLIDE_IDX or SLIDE_COORD)
	///
	void SetSlideUnit(u32 unit)
	{
		if (m_controller->CheckSupportSlideUnit(unit))
		{
			m_slideUnit = unit;
		}
	}

	/// 指定する数値単位による設定が可能か
	b8 CheckSupportSlideUnit(u32 unit) const
	{
		return m_controller->CheckSupportSlideUnit(unit);
	}

	b8 CheckColorMapStatus() const
	{
		return m_colorMap && m_colorMap->CheckStatus();
	}

	b8 SetColorMap(void* data, u32 width)
	{
		if (!m_colorMap) return false;
		return m_colorMap->SetColorMap(width, data);
	}

	f32 GetColorMapRangeMin() const
	{
		if (!m_colorMap) return 0;
		return m_colorMap->GetRangeMin();
	}

	f32 GetColorMapRangeMax() const
	{
		if (!m_colorMap) return 0;
		return m_colorMap->GetRangeMax();
	}

	void GetColorMapRangeLevels(s32* min, s32* max, u32* selectMax) const
	{
		if (m_colorMap)
		{
			m_colorMap->GetRangeLevels(min, max, selectMax);
		}
		else
		{
			*min = *max = *selectMax = 0;
		}
	}

	void SetColorMapRangeLevels(s32 min, s32 max)
	{
		if (!m_colorMap) return;
		m_colorMap->SetRangeLevels(min, max);
	}

	/// スライス操作が可能か
	b8 CheckStatus() const
	{
		return m_controller->CheckStatus();
	}

	void changeTargetNode(VX::SG::Node* node)
	{
		assert(m_controller!=NULL);
		m_controller->changeTargetNode(node);
	}


private:
	InnerController* m_controller; ///< 操作スライスの同期コントローラ
	ColorMapController* m_colorMap;
	u32 m_slideMode;    ///< 0(MODE_FAST) interactive 1(MODE_DETAIL) detail
	u32 m_slideUnit;    ///< 0(SLIDE_IDX) セルのインデックス単位で移動 1(SLIDE_COORD) 座標値で移動
	f32 m_spinPitch[3]; ///< 各軸のスピンボタンでの移動量

};


#endif // __CORE_SLICE_CONTROLLER_H__
