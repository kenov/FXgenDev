/*
 *
 * GUIController.h
 * 
 */

#ifndef __GUI_CONTROLLER_H__
#define __GUI_CONTROLLER_H__

#include "../VX/Type.h"
#include <map>

namespace VX{
	namespace SG{
		class Node;
	}
}
namespace fxGUI {
	class fxGUIManager;
	class fxFrame;
	class fxGraphic;
	class fxGraphicButton;
	class fxText;
	class fxTree;
	class fxMenu;
	class fxTreeMenu;
	class fxSplash;
}
namespace skGUI{
	class BaseWindow;
	class BlankWindow;
}
class ViewController;
class fxgenCore;

typedef skGUI::BaseWindow BaseWindow;
typedef skGUI::BlankWindow BlankWindow;

class GUIController
{
public:
	GUIController(s32 w, s32 h, ViewController* vctrl, fxgenCore* core);
	~GUIController();
	
	void Resize(s32 w, s32 h);
	void Draw();
	b8 MouseDown(s32 x, s32 y);
	b8 MouseUp  (s32 x, s32 y);
	b8 MouseRightDown(s32 x, s32 y);
	b8 MouseRightUp  (s32 x, s32 y);
	void MouseMove(s32 x, s32 y);
	void KeyDown(s32 key, s32 optkey = 0);
	void KeyUp  (s32 key);
	
	b8 IsVisibleTree();
	void SetVisibleTree(b8 visible);
	void ShowTreeMenu(b8 visible, int x = 0, int y = 0, VX::SG::Node* node = NULL);
	
	void UpdateTree(b8 cleartree);
	
	b8 IsAnimating();
	

	void SetFPS(s32 fps);
	void SetMemory(unsigned long int size);
	void SetVRAM(u32 size);
	void SetTriNum(s32 size);
	void SetMessage(const s8* msg);
	
	void AddMenuFunc(void (*func)(s32, void*), void* thisptr);
	void AddMenu(const s8* menuname ,s32 menuid, s8 key = 0, s32 optkey = 0, b8 visible = true);
	void AddSubMenu(const s8* menuname, s32 menuid, const s8* submenuname, s8 key = 0, s32 optkey = 0);
	void AddSubMenuMenu(const s8* menuname, const s8* submenuname,s32 menuid, s8 key = 0, s32 optkey = 0);
	void AddSubCheckMenu(const s8* menuname, s32 menuid, const s8* submenuname, s8 key = 0, s32 optkey = 0);
	void CheckMenu(const s8* menuname, s32 menuid, b8 check);
	void EnableMenu(const s8* menuname, s32 menuid, b8 check);
	void PressTopMenu(const s8* menuname);

	void AddSubMenuSeparator(const s8* menuname);

	void AddTreeMenuFunc(void (*func)(s32, void*, VX::SG::Node*), void* thisptr);
	VX::SG::Node* GetTreeMenuNode();
	
private:
	static void buttonCallback_(s32 i, void* thisPtr)
	{
		GUIController* t = reinterpret_cast<GUIController*>(thisPtr);
		t->buttonCallback(i);
	}
	
	static void onMenu_(s32 id, void* thisptr){
		static_cast<GUIController*>(thisptr)->onMenu(id);
	}

	static void onTreeMenu_(s32 id, void* thisptr)
	{
		static_cast<GUIController*>(thisptr)->onTreeMenu(id);
	}

	void onMenu(s32 id);
	void onTreeMenu(s32 id);
	void updateButtons();
	void buttonCallback(s32 i);
	void setLayout(s32 w, s32 h);
	
	fxGUI::fxGUIManager* m_gui;
	
	fxGUI::fxText* m_txt_memory;
	fxGUI::fxText* m_txt_vram;
	fxGUI::fxText* m_txt_fps;
	fxGUI::fxText* m_txt_tri;
	
	fxGUI::fxFrame* m_topframe;
	fxGUI::fxFrame* m_topline;
	fxGUI::fxFrame* m_iconbase;
	fxGUI::fxFrame* m_frametop;
	fxGUI::fxFrame* m_framebottom;
	fxGUI::fxFrame* m_frameright;
	enum {
		ICON_PICK = 0,
		ICON_RECT,
		ICON_TRANS,
		ICON_ROT,
		ICON_ZOOM,
		ICON_ROT_AXIS,
		ICON_PERSPECTIVE,
		ICON_FIT,
		ICON_X_NEGATIVE,
		ICON_X_POSITIVE,
		ICON_Y_NEGATIVE,
		ICON_Y_POSITIVE,
		ICON_Z_NEGATIVE,
		ICON_Z_POSITIVE,
		ICON_NUM
	};
	fxGUI::fxGraphicButton* m_icon[ICON_NUM];
	fxGUI::fxFrame* m_wakuline[4];
	fxGUI::fxFrame* m_menu;
	fxGUI::fxFrame* m_sep;
	fxGUI::fxFrame* m_wakuline2[3];
	fxGUI::fxGraphic* m_logo2;

	fxGUI::fxText* m_msg;
	fxGUI::fxTree* m_tree;
	
	fxGUI::fxMenu* m_topmenu;
	
	fxGUI::fxSplash* m_splash;
	
	ViewController* m_vctrl;
	fxgenCore* m_core;
	
	void (*m_menufunc)(s32, void*);
	void* m_menufunc_ptr;
	
	b8 m_updatingButton;
	
	std::map<s32, fxGUI::fxText*> m_shortcuts;
};

#endif // __VXGEN_SCENE_EDITOR_H__

