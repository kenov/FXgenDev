/*
 *
 * DomainEditor
 *
 */

#include "DomainEditor.h"
#include "../VX/VX.h"
#include "../VX/SG/SceneGraph.h"

namespace  {
	class findOuterBCMedium
	{
	public:
		findOuterBCMedium(const std::string& name) : m_name(name) {}
		b8 operator()(const VX::SG::OuterBC*& bc) const
		{
			return bc->GetAbsoluteName() == m_name;
		}
		b8 operator()(const VX::SG::Medium*& med) const
		{
			return med->GetAbsoluteName() == m_name;
		}
		
	private:
		std::string m_name;
	};
}


DomainEditor::DomainEditor(DOMAIN_EDITOR_TYPE type, VX::SG::Domain* domain)
{
	m_type = type;
	m_activeDomain = domain;
}

DomainEditor::~DomainEditor()
{

}


DomainEditor::DOMAIN_EDITOR_TYPE DomainEditor::GetType()
{
	return m_type;
}

const std::vector<std::string>& DomainEditor::GetUnitList()
{
	return GetActiveDomain()->GetUnitList();
}

VX::SG::Domain* DomainEditor::GetActiveDomain()
{
	return m_activeDomain;
}

const VX::SG::Domain* DomainEditor::GetActiveDomain() const
{
	return m_activeDomain;
}

void DomainEditor::AddList(const VX::SG::OuterBC* bcs)
{
	m_outerbc.push_back(bcs);
}

void DomainEditor::AddList (const VX::SG::Medium* meds)
{
	m_medium.push_back(meds);
}

void DomainEditor::ClearLists()
{
	m_outerbc.clear();
	m_medium.clear();
}

void DomainEditor::GetOuterBCNames(std::vector<std::string>& outerbcs) const
{
	std::vector<const VX::SG::OuterBC*>::const_iterator it, eit = m_outerbc.end();
	for (it = m_outerbc.begin(); it != eit; ++it){
		outerbcs.push_back((*it)->GetAbsoluteName());
	}
}

void DomainEditor::GetMediumNames(std::vector<std::string>& meds) const
{
	std::vector<const VX::SG::Medium*>::const_iterator it, eit = m_medium.end();
	for (it = m_medium.begin(); it != eit; ++it){
		meds.push_back((*it)->GetAbsoluteName());
	}
}

b8 DomainEditor::GetAxisBCMedium(u32 axis, std::string& flowbc, std::string& med) const
{
	if (axis >= GetAxisNum())
		return false;
	
	const VX::SG::OuterBC* outerbc;
	outerbc = m_activeDomain->GetFlowBC(static_cast<VX::SG::Domain::BC_DIRECTION>(axis));
	if (outerbc)
		flowbc = outerbc->GetAbsoluteName();
	
	const VX::SG::Medium* medium;
	medium = m_activeDomain->GetGuideCellMedium(static_cast<VX::SG::Domain::BC_DIRECTION>(axis));
	if (medium)
		med = medium->GetAbsoluteName();
	
	return true;
}

b8 DomainEditor::SetAxisBCMedium(u32 axis, const std::string& flowbc, const std::string& med)
{
	if (axis >= GetAxisNum())
		return false;
	
	const VX::SG::OuterBC* outerbc_flow = 0;
	const VX::SG::Medium*  medium = 0;
	
	std::vector<const VX::SG::OuterBC*>::const_iterator bcit;
	bcit = std::find_if(m_outerbc.begin(), m_outerbc.end(), findOuterBCMedium(flowbc));
	if (bcit != m_outerbc.end())
		outerbc_flow = (*bcit);
	
	std::vector<const VX::SG::Medium*>::const_iterator medit;
	medit = std::find_if(m_medium.begin(), m_medium.end(), findOuterBCMedium(med));
	if (medit != m_medium.end())
		medium = (*medit);

	const_cast <VX::SG::OuterBC*>(outerbc_flow)->SetMedium(medium->GetLabel());

	m_activeDomain->SetFlowBC(static_cast<VX::SG::Domain::BC_DIRECTION>(axis), outerbc_flow);
	m_activeDomain->SetGuideCellMedium(static_cast<VX::SG::Domain::BC_DIRECTION>(axis), medium);
	return true;
}

