#include "../VX/VX.h"
#include "../VX/Math.h"
#include "../VX/SG/SceneGraph.h"
#include "../VX/SG/VoxGeometryUtil.h"
#include "../VX/MatrixStack.h"

#include "SliceController.h"
#include "DomainBCMEditor.h"
#include "DomainBCMDivider.h"

#include "../VOX/BCMOctree.h"
#include "../VOX/Pedigree.h"
#include "../VOX/Divider.h"
#include "../VOX/RootGrid.h"

#include <stdio.h>

////////////////////////////////////////////////////////////////////////////////////


DomainBCMEditor::DomainBCMEditor(VX::SG::Group* root, VX::SG::DomainBCM* domain, VX::SG::Group* bcmGroup) 
: DomainEditor(DOMAIN_EDITOR_BCM, domain), m_sg(root), m_bcmGroup(bcmGroup), m_lvColorTbl(NULL)
{
	m_levelGroup = NULL;
	m_sliceGroup = NULL;
	m_paramGroup = NULL;
	VX::SG::DomainBCM::InitLvColorTable(m_lvColorTbl, 16);
	
	UpdateGlobalBoxGeometry();
	using namespace VX::SG;
	std::vector<DomainBCM::RegionScope*>& list = domain->GetRegionScopeList();
	for(int i = 0; i < list.size(); i++){
		AddRegionScopeGeometry(list[i]->origin, list[i]->region, list[i]->level);
	}
}

DomainBCMEditor::~DomainBCMEditor()
{
	m_sg = NULL;
	vxdelete(m_lvColorTbl);
}


b8 DomainBCMEditor::SetDivCount( const u32 divCount )
{
	if( !m_sg ){
		return false;
	}

	VX::SG::DomainBCM* domain = dynamic_cast<VX::SG::DomainBCM*>(GetActiveDomain());
	if(!domain){
		return false;
	}

	return domain->SetNumDivs( divCount );
}

void DomainBCMEditor::ResetGroupLayout()
{
	// Delete Old SG Layout
	for(int i = m_bcmGroup->GetChildCount()-1; i >=0; --i)
	{
		VX::SG::Node* node = m_bcmGroup->GetChild(i);
		if( node->GetType() == VX::SG::NODETYPE_GROUP ){
			m_bcmGroup->RemoveChild(node);
		}
	}

	m_levelGroup = NULL;
	m_sliceGroup = NULL;
	m_paramGroup = NULL;
	
	VX::SG::DomainBCM* domain = dynamic_cast<VX::SG::DomainBCM*>(GetActiveDomain());
	if( domain->GetOctree() )
	{
		m_levelGroup = vxnew VX::SG::Group(VX::SG::NODETYPE_BBOX_DOMAIN_BCM_SUB_GROUP);
		char name[128];
		sprintf(name, "Level Blocks (total : %d blocks)", domain->GetNumBlocks());
		m_levelGroup->SetName(name);
		for(u32 i = 0; i <= domain->GetMaxLevel(); i++){
			m_levelGroup->AddChild(domain->GetLevelBox(i));
		}

		m_sliceGroup = vxnew VX::SG::Group(VX::SG::NODETYPE_BBOX_DOMAIN_SLICE_GROUP);
		m_sliceGroup->SetName("Slice Planes");
		for(u32 i = 0; i < 3; i++){
			m_sliceGroup->AddChild(domain->GetSlice(i));
		}

		m_levelGroup->SetVisible(false);
		m_sliceGroup->SetVisible(false);

		m_bcmGroup->AddChild(m_levelGroup);
		m_bcmGroup->AddChild(m_sliceGroup);
	}
	else
	{
		m_paramGroup = vxnew VX::SG::Group(VX::SG::NODETYPE_BBOX_DOMAIN_BCM_SUB_GROUP);
		m_paramGroup->SetName("Parameter");

		if( m_globalBox )
			m_paramGroup->AddChild(m_globalBox);

		for(size_t i = 0; i < m_scopeBoxes.size(); i++){
			m_paramGroup->AddChild(m_scopeBoxes[i]);
		}

		m_bcmGroup->AddChild(m_paramGroup);

		if (domain->GetUseDistanceBasedMethod())
		{
			m_sliceGroup = vxnew VX::SG::Group();
			m_sliceGroup->SetName("Distance Field Slices");
			for (u32 i = 0; i < 3; ++i)
			{
				m_sliceGroup->AddChild(domain->GetDistanceSlice(i));
			}
			m_sliceGroup->SetVisible(true);
			m_bcmGroup->AddChild(m_sliceGroup);
		}
	}
}


void DomainBCMEditor::UpdateGlobalBoxGeometry()
{
	VX::SG::DomainBCM* domain = dynamic_cast<VX::SG::DomainBCM*>(GetActiveDomain());
	if( !domain ) return;

	using namespace VX::SG;
	using namespace VX::Math;

	const VX::Math::idx3& rootDims = domain->GetRootDims();
	const VX::Math::vec3& org = domain->GetOrigin();
	const VX::Math::vec3& rgn = domain->GetRegion();

	if( !m_globalBox )
	{
		m_globalBox = vxnew Geometry(NODETYPE_BBOX_DOMAIN_BCM_GRID);
		m_globalBox->SetUseVertexLineColor(true);
		m_globalBox->SetDrawMode(Geometry::MODE_LINE);
		m_globalBox->SetName("Domain | RootGrid");
		m_globalBox->SetSelectable(true);
		m_globalBox->SetSelection(false);
	}

	idx2 GridCount[3] = { idx2( rootDims.y, rootDims.z ),
		                  idx2( rootDims.x, rootDims.z ),
		                  idx2( rootDims.x, rootDims.y ) };

	u32 GridStride[3];
	GridStride[0] = ((GridCount[0].x - 1) * 2 + (GridCount[0].y - 1) * 2);
	GridStride[1] = ((GridCount[1].x - 1) * 2 + (GridCount[1].y - 1) * 2);
	GridStride[2] = ((GridCount[2].x - 1) * 2 + (GridCount[2].y - 1) * 2);
	
	m_globalBox->Alloc( (8 + GridStride[0] + GridStride[1] + GridStride[2]), (24 + GridStride[0] + GridStride[1] + GridStride[2]) );

	Geometry::VertexFormat* vertex = m_globalBox->GetVertex();
	Index* index = m_globalBox->GetIndex();

	BoxSetter(vertex, index, org, rgn, m_lvColorTbl[0], 0);

	u32 vtxOffset = 8;
	u32 idxOffset = 24;
	for(u32 axis = 0; axis < 3; axis++){
		GridSetter(&vertex[vtxOffset], &index[idxOffset], axis, org, rgn, GridCount[axis], m_lvColorTbl[0], vtxOffset);
		vtxOffset += GridStride[axis];
		idxOffset += GridStride[axis];
	}

	m_globalBox->CalcBounds();
	m_globalBox->EnableNeedUpdate();
}

void DomainBCMEditor::UpdateDistanceFieldSlices(b8 reset)
{
	using namespace VX::SG;
	using std::max;
	typedef DomainBCM::SubdivisionMargin SubdivisionMargin;

	DomainBCM* domain = dynamic_cast<DomainBCM*>(GetActiveDomain());
	if (!domain) return;
	if (domain->GetOctree()) return;

	if (reset) {
		VX::Math::vec3 org = domain->GetOrigin();
		VX::Math::vec3 rgn = domain->GetRegion();
		for (s32 i = 0; i < 3; ++i) {
			domain->UpdateDistanceFieldSlice(i, org[i] + rgn[i] / 2, m_sg, false);
		}
	} else {
		for (s32 i = 0; i < 3; ++i) {
			domain->UpdateDistanceFieldSlice(i, domain->GetDistanceSlicePosition(i), m_sg, false);
		}
	}
}

void DomainBCMEditor::AddRegionScopeGeometry(const VX::Math::vec3& origin, const VX::Math::vec3& region, const u32 level)
{
	using namespace VX::SG;

	Geometry *geo = vxnew Geometry();
	geo->SetUseVertexLineColor(true);
	geo->SetDrawMode(Geometry::MODE_LINE);
	geo->SetName("Region Scope");
	geo->SetSelectable(false);
	geo->Alloc(8, 24);

	BoxSetter(geo->GetVertex(), geo->GetIndex(), origin, region, m_lvColorTbl[level]);
	geo->CalcBounds();
	geo->EnableNeedUpdate();

	m_scopeBoxes.push_back(geo);

	if( m_paramGroup ){
		m_paramGroup->AddChild(geo);
	}
}

void DomainBCMEditor::DeleteRegionScopeGeometry(const u32 idx)
{
	using namespace VX::SG;

	if( idx >= m_scopeBoxes.size() )
		return;

	Geometry* geo = m_scopeBoxes[idx];
	if( m_paramGroup ){
		m_paramGroup->RemoveChild(geo);
	}

	m_scopeBoxes.erase(m_scopeBoxes.begin() + idx);
}

b8 DomainBCMEditor::SetDefault()
{
	VX::SG::DomainBCM* domain = dynamic_cast<VX::SG::DomainBCM*>(GetActiveDomain());
	if( domain->GetOctree() )
		return true;

	VX::Math::vec3 min, max;
	GetBBox(m_sg, max, min, false, true);

	domain->SetBBoxMinMax(min, max);
	domain->SetRootDims(VX::Math::idx3(1, 1, 1));
	domain->SetBaseLevel(0);
	domain->SetMinLevel(0);
	domain->SetBlockSize(VX::Math::idx3(1, 1, 1));
	domain->SetExportPolicy(VX::SG::DomainBCM::EXP_PARAMETER);
	domain->SetLeafOrdering(VX::SG::DomainBCM::ORDER_Z);

	domain->GetGeometryScopeList().clear();
	domain->GetRegionScopeList().clear();

	m_scopeBoxes.clear();

	UpdateGlobalBoxGeometry();
	UpdateDistanceFieldSlices(true);
	ResetGroupLayout();
	return true;
}


b8 DomainBCMEditor::HasOctree()
{
	VX::SG::DomainBCM* domain = dynamic_cast<VX::SG::DomainBCM*>(GetActiveDomain());
	if( domain->GetOctree() )
	{
		return true;
	}
	else
	{
		return false;
	}
}

b8 DomainBCMEditor::DeleteOctree()
{
	VX::SG::DomainBCM* domain = dynamic_cast<VX::SG::DomainBCM*>(GetActiveDomain());
	domain->DeleteOctree();

	UpdateGlobalBoxGeometry();
	UpdateDistanceFieldSlices(true);
	ResetGroupLayout();
	return true;
}

b8 DomainBCMEditor::CreateOctree()
{
	VX::SG::DomainBCM* domain = dynamic_cast<VX::SG::DomainBCM*>(GetActiveDomain());
	if( !domain ){ return false; }

	domain->DeleteOctree();

	VOX::Divider* divider;
	if (domain->GetUseDistanceBasedMethod())
	{
		divider = new DomainBCMDividerDistance(domain, m_sg);
	}
	else
	{
		divider = new DomainBCMDividerIntersection(domain, m_sg);
	}

	f64 start = VX::GetTimeCount();

	domain->CreateOctree(divider);

	VXLogD("  Processing Time ...  %f sec.\n", VX::GetTimeCount() - start);

	delete divider;

	ResetGroupLayout();
	return true;
}

b8 DomainBCMEditor::CreateOctree(const std::vector<VOX::Pedigree>& pedigrees)
{
	VX::SG::DomainBCM* domain = dynamic_cast<VX::SG::DomainBCM*>(GetActiveDomain());
	if( !domain ){ return false; }

	domain->DeleteOctree();
	domain->CreateOctree(pedigrees);

	ResetGroupLayout();
	return true;
}

b8 DomainBCMEditor::Centering(u8 axisFlag)
{
	VX::SG::DomainBCM* domain = static_cast<VX::SG::DomainBCM*>(GetActiveDomain());
	if( !domain ){ return false; }

	VX::Math::vec3 curMin, curMax, curSize;
	GetBBoxMinMaxSize(curMin, curMax, curSize);

	VX::Math::vec3 min, max;
	GetBBox(m_sg, max, min, false, true);
	VX::Math::vec3 center = (max + min) * 0.5f;
	
	VX::Math::vec3 newMin, newMax;
	newMin = curMin;
	newMax = curMax;

	for(u32 i = 0; i < 3; i++){
		if( (axisFlag >> i) & 1 ){
			newMin[i] = center[i] - (curSize[i] * 0.5f);
			newMax[i] = center[i] + (curSize[i] * 0.5f);
		}
	}

	return SetBBoxMinMax(newMin, newMax);
}

b8 DomainBCMEditor::SetBBoxMinMax(const VX::Math::vec3& min, const VX::Math::vec3& max)
{
	VX::SG::DomainBCM* domain = static_cast<VX::SG::DomainBCM*>(GetActiveDomain());
	if( !domain ){ return false; }

	if( !domain->SetBBoxMinMax(min, max) )
		return false;
	
	UpdateGlobalBoxGeometry();
	return true;
}

b8 DomainBCMEditor::SetRootLength(const VX::Math::vec3& rootLength)
{
	VX::SG::DomainBCM* domain = static_cast<VX::SG::DomainBCM*>(GetActiveDomain());
	if( !domain ){ return false; }
	
	if( rootLength.x <= 0.0f || rootLength.y <= 0.0f || rootLength.z <= 0.0f )
		return false;
	
	VX::Math::vec3 curMin, curMax, curSize;
	GetBBoxMinMaxSize(curMin, curMax, curSize);

	VX::Math::idx3 rootDims;
	for(int i = 0; i < 3; i++){
		rootDims[i] = static_cast<s32>(ceil(curSize[i] / rootLength[i]));
		curSize[i]  = static_cast<f32>(rootDims[i]) * rootLength[i];
	}

	if( !SetBBoxMinMax(curMin, (curMin + curSize)) )
		return false;
	
	if( !SetRootDims(rootDims) )
		return false;
	
	UpdateGlobalBoxGeometry();
	return true;
}

b8 DomainBCMEditor::SetRootDims(const VX::Math::idx3& rootDims)
{
	VX::SG::DomainBCM* domain = static_cast<VX::SG::DomainBCM*>(GetActiveDomain());
	if( !domain ){ return false; }

	if( rootDims.x < 1 || rootDims.y < 1 || rootDims.y < 1) { return false; }

	if( !domain->SetRootDims(rootDims) )
		return false;
	
	UpdateGlobalBoxGeometry();
	return true;
}

b8 DomainBCMEditor::SetUnit(const std::string& unit )
{
	VX::SG::DomainBCM* domain = static_cast<VX::SG::DomainBCM*>(GetActiveDomain());
	if( !domain ){ return false; }

	return domain->SetUnit(unit);
}

b8 DomainBCMEditor::SetBaseLevel(const u32 level )
{
	VX::SG::DomainBCM* domain = static_cast<VX::SG::DomainBCM*>(GetActiveDomain());
	if( !domain ){ return false; }

	domain->SetBaseLevel(level);
	return true;
}

b8 DomainBCMEditor::SetMinLevel(const u32 level )
{
	VX::SG::DomainBCM* domain = static_cast<VX::SG::DomainBCM*>(GetActiveDomain());
	if( !domain ){ return false; }

	domain->SetMinLevel(level);
	return true;
}

b8 DomainBCMEditor::SetBlockSize(const VX::Math::idx3& size)
{
	VX::SG::DomainBCM* domain = static_cast<VX::SG::DomainBCM*>(GetActiveDomain());
	if( !domain ){ return false; }

	domain->SetBlockSize(size);
	return true;
}

b8 DomainBCMEditor::SetLeafOrdering(const u32 ordering)
{
	VX::SG::DomainBCM* domain = static_cast<VX::SG::DomainBCM*>(GetActiveDomain());
	if( !domain ){ return false; }

	VX::SG::DomainBCM::LeafOrdering _ordering;
	_ordering = ordering == 0 ? VX::SG::DomainBCM::ORDER_Z : VX::SG::DomainBCM::ORDER_HILBERT;

	domain->SetLeafOrdering(_ordering);

	return true;
}

b8 DomainBCMEditor::SetExportPolicy(const u32 policy)
{
	VX::SG::DomainBCM* domain = static_cast<VX::SG::DomainBCM*>(GetActiveDomain());
	if( !domain ) return false;

	VX::SG::DomainBCM::ExportPolicy _policy;
	_policy = policy == 0 ? VX::SG::DomainBCM::EXP_PARAMETER : VX::SG::DomainBCM::EXP_OCTREE;

	domain->SetExportPolicy(_policy);
	return true;
}


b8 DomainBCMEditor::GetBBoxMinMax(VX::Math::vec3& min, VX::Math::vec3& max) const
{
	const VX::SG::DomainBCM* domain = static_cast<const VX::SG::DomainBCM*>(GetActiveDomain());
	if( !domain ) return false;

	VX::Math::vec3 rgn = domain->GetRegion();
	min = domain->GetOrigin();

	max = min + rgn;
	return true;
}

b8 DomainBCMEditor::GetBBoxMinMaxSize(VX::Math::vec3& min, VX::Math::vec3& max, VX::Math::vec3& size) const
{
	if( !GetBBoxMinMax(min, max) )
		return false;
	
	size = max - min;
	return true;
}

b8 DomainBCMEditor::GetRootDims(VX::Math::idx3& rootDims) const
{
	const VX::SG::DomainBCM* domain = static_cast<const VX::SG::DomainBCM*>(GetActiveDomain());
	if( !domain ) return false;
	
	rootDims = domain->GetRootDims();
	return true;
}

b8 DomainBCMEditor::GetRootLength(VX::Math::vec3& rootLength) const
{
	VX::Math::idx3 rootDims;
	if( !GetRootDims(rootDims) )
		return false;
	
	VX::Math::vec3 bboxMin, bboxMax, bboxSize;
	if( !GetBBoxMinMaxSize(bboxMin, bboxMax, bboxSize) )
		return false;
	
	rootLength[0] = bboxSize[0] / static_cast<f32>(rootDims[0]);
	rootLength[1] = bboxSize[1] / static_cast<f32>(rootDims[1]);
	rootLength[2] = bboxSize[2] / static_cast<f32>(rootDims[2]);

	return true;
}

u32 DomainBCMEditor::GetBaseLevel() const
{
	const VX::SG::DomainBCM* domain = static_cast<const VX::SG::DomainBCM*>(GetActiveDomain());
	if( !domain ) return 0;

	return domain->GetBaseLevel();
}

u32 DomainBCMEditor::GetMinLevel() const
{
	const VX::SG::DomainBCM* domain = static_cast<const VX::SG::DomainBCM*>(GetActiveDomain());
	if( !domain ) return 0;
	
	return domain->GetMinLevel();
}

const VX::Math::idx3& DomainBCMEditor::GetBlockSize() const
{
	const VX::SG::DomainBCM* domain = static_cast<const VX::SG::DomainBCM*>(GetActiveDomain());
	return domain->GetBlockSize();
}

u32 DomainBCMEditor::GetLeafOrdering() const
{
	const VX::SG::DomainBCM* domain = static_cast<const VX::SG::DomainBCM*>(GetActiveDomain());
	if( !domain ) return 0;

	return domain->GetLeafOrdering() == VX::SG::DomainBCM::ORDER_Z ? 0 : 1;
}

u32 DomainBCMEditor::GetExportPolicy() const
{
	const VX::SG::DomainBCM* domain = static_cast<const VX::SG::DomainBCM*>(GetActiveDomain());
	if( !domain ) return false;

	return domain->GetExportPolicy() == VX::SG::DomainBCM::EXP_PARAMETER ? 0 : 1;
}

u32 DomainBCMEditor::GetDivCount() const
{
	const VX::SG::DomainBCM* domain = static_cast<const VX::SG::DomainBCM*>(GetActiveDomain());
	if( !domain ) return false;

	return domain->GetNumDivs();
}


const std::string& DomainBCMEditor::GetUnit() const
{
	const VX::SG::DomainBCM* domain = static_cast<const VX::SG::DomainBCM*>(GetActiveDomain());
	return domain->GetUnit();
}

u32 DomainBCMEditor::GetMaxLevel() const
{
	using namespace VX::SG;

	const DomainBCM* domain = static_cast<const DomainBCM*>(GetActiveDomain());
	if( !domain ) return 0;
	
	if( domain->GetOctree() )
	{
		return domain->GetMaxLevel();
	}
	else
	{
		return domain->ComputeMaxLevel();
	}
}


b8 DomainBCMEditor::AddGeometryScope(const std::string& name, const u32 level,
									 const std::string type, const f32 ratio, const f32 cell)
{
	using namespace VX::SG;

	DomainBCM* domain = static_cast<DomainBCM*>(GetActiveDomain());
	if( !domain ) return false;

	DomainBCM::GeometryScope *geoScp = vxnew DomainBCM::GeometryScope(name, level, type, ratio, cell);

	std::vector<DomainBCM::GeometryScope*>& list = domain->GetGeometryScopeList();
	list.push_back(geoScp);

	return true;
}

b8 DomainBCMEditor::DeleteGeometryScope(const u32 idx)
{
	using namespace VX::SG;

	DomainBCM* domain = static_cast<DomainBCM*>(GetActiveDomain());
	if( !domain ) return false;

	std::vector<DomainBCM::GeometryScope*>& list = domain->GetGeometryScopeList();

	if( idx >= list.size() )
		return false;
	
	vxdelete(list[idx]);
	list.erase(list.begin() + idx);
	
	return true;
}

b8 DomainBCMEditor::ClearGeometryScope()
{
	using namespace VX::SG;

	DomainBCM* domain = static_cast<DomainBCM*>(GetActiveDomain());
	if( !domain ) return false;
	
	domain->ClearGeometryScopeList();

	return true;
}

u32 DomainBCMEditor::GetNumGeometryScope() const
{
	using namespace VX::SG;

	const DomainBCM* domain = static_cast<const DomainBCM*>(GetActiveDomain());
	if( !domain ) return 0;

	const std::vector<DomainBCM::GeometryScope*>& list = domain->GetGeometryScopeList();
	return static_cast<u32>(list.size());
}

b8 DomainBCMEditor::GetGeometryScope(const u32 idx, std::string& name, u32& level,
									 std::string& type, f32& ratio, f32& cell) const
{
	using namespace VX::SG;

	const DomainBCM* domain = static_cast<const DomainBCM*>(GetActiveDomain());
	if( !domain ) return false;

	const std::vector<DomainBCM::GeometryScope*>& list = domain->GetGeometryScopeList();
	
	if( idx >= list.size() )
		return false;
	
	const DomainBCM::GeometryScope* scope = list[idx];

	name  = scope->name;
	level = scope->level;
	type = scope->type;
	ratio = scope->ratio;
	cell = scope->cell;
	return true;
}

b8 DomainBCMEditor::AddRegionScope(const VX::Math::vec3& origin, const VX::Math::vec3& region, const u32 level,
								   const std::string& type, const f32& ratio, const f32& cell)
{
	using namespace VX::SG;

	DomainBCM* domain = static_cast<DomainBCM*>(GetActiveDomain());
	if( !domain ) return false;

	DomainBCM::RegionScope* scope = vxnew DomainBCM::RegionScope(origin, region, level, type, ratio, cell);
	std::vector<DomainBCM::RegionScope*>& list = domain->GetRegionScopeList();
	list.push_back(scope);

	AddRegionScopeGeometry(origin, region, level);

	return true;
}

b8 DomainBCMEditor::DeleteRegionScope(const u32 idx)
{
	using namespace VX::SG;
	
	DomainBCM* domain = static_cast<DomainBCM*>(GetActiveDomain());
	if( !domain ) return false;

	std::vector<DomainBCM::RegionScope*>& list = domain->GetRegionScopeList();

	if( idx >= list.size() )
		return false;
	
	vxdelete(list[idx]);
	list.erase(list.begin() + idx);

	DeleteRegionScopeGeometry(idx);

	return true;
}

b8 DomainBCMEditor::EditRegionScope(const u32 idx, const VX::Math::vec3& origin, const VX::Math::vec3& region, const u32 level)
{
	using namespace VX::SG;

	DomainBCM* domain = static_cast<DomainBCM*>(GetActiveDomain());
	if( !domain ) return false;

	std::vector<DomainBCM::RegionScope*>& list = domain->GetRegionScopeList();
	if( idx >= list.size() )
		return false;

	if( idx >= m_scopeBoxes.size() )
		return false;

	list[idx]->origin = origin;
	list[idx]->region = region;
	list[idx]->level  = level;

	Geometry *geo = m_scopeBoxes[idx];
	BoxSetter(geo->GetVertex(), geo->GetIndex(), origin, region, m_lvColorTbl[level]);
	geo->CalcBounds();
	geo->EnableNeedUpdate();
	
	return true;
}

b8 DomainBCMEditor::ClearRegionScope()
{
	using namespace VX::SG;

	DomainBCM* domain = static_cast<DomainBCM*>(GetActiveDomain());
	if( !domain ) return false;

	if( m_paramGroup )
	{
		for(size_t i = m_scopeBoxes.size()-1; i <= 0; i--)
		{
			m_paramGroup->RemoveChild(m_scopeBoxes[i]);
		}
	}
	m_scopeBoxes.clear();
	
	domain->ClearRegionScopeList();

	return true;
}

u32 DomainBCMEditor::GetNumRegionScope() const
{
	using namespace VX::SG;
	const DomainBCM* domain = static_cast<const DomainBCM*>(GetActiveDomain());
	if( !domain ) return 0;
	
	return static_cast<u32>(domain->GetRegionScopeList().size());
}

b8 DomainBCMEditor::GetRegionScope(const u32 idx, VX::Math::vec3& origin, VX::Math::vec3& region, u32& level,
								   std::string& type, f32& ratio, f32& cell) const
{
	using namespace VX::SG;

	const DomainBCM* domain = static_cast<const DomainBCM*>(GetActiveDomain());
	if( !domain ) return false;

	const std::vector<DomainBCM::RegionScope*>& list = domain->GetRegionScopeList();

	if( idx >= list.size() )
		return false;
	
	const DomainBCM::RegionScope* scope = list[idx];

	origin = scope->origin;
	region = scope->region;
	level  = scope->level;
	type = scope->type;
	ratio = scope->ratio;
	cell = scope->cell;

	return true;
}

b8 DomainBCMEditor::GetPedigreeList( std::vector<VOX::Pedigree>& pedigrees ) const
{
	using namespace VX::SG;

	const DomainBCM* domain = static_cast<const DomainBCM*>(GetActiveDomain());
	if( !domain ) return false;

	return domain->GetPedigreeList( pedigrees );
}

b8 DomainBCMEditor::GetUseDistanceBasedMethod() const
{
	using namespace VX::SG;
	const DomainBCM* domain = static_cast<const DomainBCM*>(GetActiveDomain());
	if (!domain) return false;

	return domain->GetUseDistanceBasedMethod();
}

void DomainBCMEditor::SetUseDistanceBasedMethod(b8 use)
{
	using namespace VX::SG;
	DomainBCM* domain = static_cast<DomainBCM*>(GetActiveDomain());
	if (!domain) return;

	domain->SetUseDistanceMethod(use);

	if (!domain->GetOctree())
	{
		ResetGroupLayout();
	}

	if (m_sliceGroup)
	{
		m_sliceGroup->SetVisible(use);
	}
}

std::string DomainBCMEditor::GetInformationText() const
{
	using namespace VX::SG;
	using namespace VX::Math;

	const DomainBCM* domain = static_cast<const DomainBCM*>(GetActiveDomain());
	if (!domain) return std::string();

	std::string txt;
	txt.reserve(2048);
	// Min Cell Size
	{
		f64 cellSize[3];
		const u32 maxLevel = GetMaxLevel(); // don't call domain->GetMaxLevel(). Min Cell Size should be calculated always
		const idx3 blockSize = domain->GetBlockSize();
		vec3 rootLength;
		GetRootLength(rootLength);
		cellSize[0] = static_cast<f64>(rootLength[0]) / static_cast<f64>(1 << maxLevel) / static_cast<f64>(blockSize[0]);
		cellSize[1] = static_cast<f64>(rootLength[1]) / static_cast<f64>(1 << maxLevel) / static_cast<f64>(blockSize[1]);
		cellSize[2] = static_cast<f64>(rootLength[2]) / static_cast<f64>(1 << maxLevel) / static_cast<f64>(blockSize[2]);
		char buf[512] = { 0 };
		sprintf(buf, "Min Cell Size : [%13.5lf %13.5lf %13.5lf]\n", cellSize[0], cellSize[1], cellSize[2]);
		txt.append(buf);
	}
	// Octree Information
	if (domain->GetOctree())
	{
		txt.append("\nOctree :\n");
		const DomainBCM::BCMOctreeInformation& info = domain->GetOctreeInformation();
		const idx3 blockSize = domain->GetBlockSize();
		const u32 blockCount = blockSize.x * blockSize.y * blockSize.z;
		const u32 BYTES_PER_OCTREENODE = sizeof(VOX::Node);
		const u32 BITS_PER_CELL = 8;
		const u32 BITS_PER_BYTE = 8;
		const f64 MB = 1024.0 * 1024.0;
		char buf[512] = { 0 };
		txt.append("    Summary :\n");
		sprintf(buf, "        Max Level : %u\n", info.maxLevel);
		txt.append(buf);
		sprintf(buf, "        Total Node : %llu\n", info.totalNodeCount);
		txt.append(buf);
		sprintf(buf, "        Total Leaf Block : %llu\n", info.totalLeafCount);
		txt.append(buf);
		sprintf(buf, "        Total Cell : %llu\n", info.totalLeafCount * blockCount);
		txt.append(buf);
		sprintf(buf, "        Memory Usage : %.1lf MB\n", (info.totalNodeCount * BYTES_PER_OCTREENODE / MB));
		txt.append(buf);
		for (u32 i = 0; i < info.maxLevel + 1; ++i)
		{
			sprintf(buf, "    Level [%u] : %llu Leaf Blocks, %llu Cells\n", i, info.levels[i].leafCount, info.levels[i].leafCount * blockCount);
			txt.append(buf);
		}

		const u32 ndiv = domain->GetNumDivs();
		if (ndiv > 0)
		{
			txt.append("\nParallel Divide : \n");
			sprintf(buf, "    Number of Processes : %d\n", ndiv);
			txt.append(buf);
			txt.append("    Per Process : \n");
			sprintf(buf, "        Leaf Blocks : %lld\n", (info.totalLeafCount + (ndiv-1)) / ndiv); // round up
			txt.append(buf);
			sprintf(buf, "        Cells : %lld\n", (info.totalLeafCount * blockCount + (ndiv-1)) / ndiv); // round up
			txt.append(buf);
			sprintf(buf, "        Memory Usage : %.1lf MB\n",
				(info.totalNodeCount * BYTES_PER_OCTREENODE +
				 info.totalLeafCount * blockCount * (BITS_PER_CELL / static_cast<f64>(BITS_PER_BYTE)) / ndiv
				) / MB);
			txt.append(buf);
		}
	}
	else
	{
		txt.append("\nOctree : not created\n");
	}

	return txt;
}

u32 DomainBCMEditor::GetNumSubdivisionMargin() const
{
	using namespace VX::SG;
	const DomainBCM* domain = static_cast<const DomainBCM*>(GetActiveDomain());
	if (!domain) return 0;

	return static_cast<u32>(domain->GetSubdivisionMargins().size());
}

b8 DomainBCMEditor::GetSubdivisionMargin(u32 idx, f32& margin, u32& level, bool& isMaxLevel) const
{
	using namespace VX::SG;
	const DomainBCM* domain = static_cast<const DomainBCM*>(GetActiveDomain());
	if (!domain) return false;
	if (domain->GetSubdivisionMargins().size() <= idx) return false;

	const DomainBCM::SubdivisionMargin& s = domain->GetSubdivisionMargins()[idx];
	margin = s.margin;
	isMaxLevel = (s.level == DomainBCM::SubdivisionMargin::MAX_LEVEL);
	if (isMaxLevel)
	{
		level = domain->GetBaseLevel();
	}
	else
	{
		level = s.level;
	}
	return true;
}

void DomainBCMEditor::AddSubdivisionMargin(f32 margin, u32 level, b8 isMaxLevel)
{
	using namespace VX::SG;
	DomainBCM* domain = static_cast<DomainBCM*>(GetActiveDomain());
	if (!domain) return;
	
	domain->GetSubdivisionMargins().push_back(
		DomainBCM::SubdivisionMargin(margin, isMaxLevel ? DomainBCM::SubdivisionMargin::MAX_LEVEL : level));
	domain->UpdateColorMapRange();
}

void DomainBCMEditor::DeleteSubdivisionMargin(u32 idx)
{
	using namespace VX::SG;
	DomainBCM* domain = static_cast<DomainBCM*>(GetActiveDomain());
	if (!domain) return;
	if (domain->GetSubdivisionMargins().size() <= idx) return;

	domain->GetSubdivisionMargins().erase(domain->GetSubdivisionMargins().begin() + idx);
	domain->UpdateColorMapRange();
}

void DomainBCMEditor::EditSubdivisionMargin(u32 idx, f32 margin, u32 level, b8 isMaxLevel)
{
	using namespace VX::SG;
	DomainBCM* domain = static_cast<DomainBCM*>(GetActiveDomain());
	if (!domain) return;
	if (domain->GetSubdivisionMargins().size() <= idx) return;

	DomainBCM::SubdivisionMargin& s = domain->GetSubdivisionMargins()[idx];
	s.margin = margin;
	s.level = isMaxLevel ? DomainBCM::SubdivisionMargin::MAX_LEVEL : level;
	domain->UpdateColorMapRange();
}

void DomainBCMEditor::ClearSubdivisionMargin()
{
	using namespace VX::SG;
	DomainBCM* domain = static_cast<DomainBCM*>(GetActiveDomain());
	if (!domain) return;
	
	domain->ClearSubdivisionMargins();
	domain->UpdateColorMapRange();
}

