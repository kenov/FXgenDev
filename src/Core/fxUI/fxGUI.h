/*
 *  fxGUI.h
 *  fxGUI
 *
 *  Created by kioku on 2012/08/20.
 *
 */

#ifndef INCLUDE_FXGUI_H
#define INCLUDE_FXGUI_H

#include "skBaseGUI.h"
#include "SimpleGraphics.h"
#include "SimpleShader.h"
#include "SimpleTex.h"
#include "SimpleVB.h"

#include "icons.h"
#define STRINGIFY(A) #A

#include "mplus1cregular.h"

#define STB_TRUETYPE_IMPLEMENTATION 
#include "stb_truetype.h"


#include "../../VX/SG/SceneGraph.h"
#include "../../Core/fxgenCore.h"
#include <sstream>
#include "../../UI/DialogUtil.h"

class fxgenCore;
namespace fxGUI
{
		
bool TGALoader(const unsigned char* buffer, int& w, int& h, const unsigned char** buf)
{
#ifdef __GNUC__
#pragma pack(push, 1)
#define ALIGNMENT __attribute__((packed))
#else
#pragma pack(1)
#define ALIGNMENT 
#endif // __GNUC__	
	typedef struct
	{
		unsigned char identsize;          // size of ID field that follows 18 byte header (0 usually)
		unsigned char colourmaptype;      // type of colour map 0=none, 1=has palette
		unsigned char imagetype;          // type of image 0=none,1=indexed,2=rgb,3=grey,+8=rle packed
		short colourmapstart;     // first colour map entry in palette
		short colourmaplength;    // number of colours in palette
		unsigned char colourmapbits;      // number of bits per palette entry 15,16,24,32
		short xstart;             // image x origin
		short ystart;             // image y origin
		short width;              // image width in pixels
		short height;             // image height in pixels
		unsigned char bits;       // image bits per pixel 8,16,24,32
		unsigned char descriptor; // image descriptor bits (vh flip bits)
	} TGA_HEADER;
#ifdef __GNUC__
#pragma pack(pop)
#else
#pragma pack() 
#endif // __GNUC__

	const TGA_HEADER* header = reinterpret_cast<const TGA_HEADER*>(buffer);
	w = header->width;
	h = header->height;
	if (header->bits != 32 || header->colourmaptype != 0 || header->imagetype != 2)
		return false;
	*buf = buffer;
	*buf += 18;
	return true;
}
	
class fxGUIManager : public skGUI::GUIManager
{
public:
	fxGUIManager() : skGUI::GUIManager()
	{
		static const char* vshader = STRINGIFY(
			uniform mat4 proj;
			attribute vec3 pos;
			attribute vec4 col;
			attribute vec2 uv;
			varying vec4 color;
			varying vec2 tex;
			void main(void) {
				color = col;
				tex   = uv;
				gl_Position = proj * vec4(pos,1);
			}
		);
		static const char* fshader = STRINGIFY(
		uniform sampler2D texture;
		uniform float mono;
	    varying vec4 color;
		varying vec2 tex;
		void main(void) {
			vec4 t = texture2D(texture, tex);
			gl_FragColor = color * ((t.rrrr - t.rgba) * mono + t.rgba);
		}
		);

		g = new skGUI::SimpleGraphics();
		m_defshader = new skGUI::SimpleShader(g, vshader, fshader);
		m_defvb = new skGUI::SimpleVB(g, m_defshader);
		
		m_deftex = 0;
		int tw, th;
		const unsigned char* buf;
		if (TGALoader(icons, tw, th, &buf)) {
			m_deftex = new skGUI::SimpleTex(g, tw, th);
			unsigned char* data = m_deftex->Map();
			// upsidedown
			for (int y = 0; y < th; ++y)
				memcpy(data + tw * (th - y - 1) * 4, buf + tw * y * 4, tw * 4);
			m_deftex->Unmap();
		}
	}
	~fxGUIManager()
	{
		m_root = 0;
		delete m_deftex;
		delete m_defvb;
		delete m_defshader;
		delete g;
	}
	
	skGUI::SimpleGraphics* GetGraphics() const
	{
		return g;
	}
	
	skGUI::SimpleVB* GetDefaultVB() const
	{
		return m_defvb;
	}
	
	skGUI::SimpleShader* GetDefaultShader() const
	{
		return m_defshader;
	}
	
	const skGUI::SimpleTex* GetTex() const
	{
		return m_deftex;
	}
	
protected:
	void Ortho(float l, float r, float t, float b, float nearval, float farval, float mat[16])
	{
		const float x =  2.0f / (r - l);
		const float y =  2.0f / (t - b);
		const float z =  -2.0f / (farval - nearval);
		const float tx = - (r + l) / (r - l);
		const float ty = - (t + b) / (t - b);
		const float tz = - (farval + nearval) / (farval - nearval);
		for (int i = 0; i < 16; ++i)
			mat[i] = 0;
		mat[ 0] = x;
		mat[ 5] = y;
		mat[10] = z;
		mat[12] = tx;
		mat[13] = ty;
		mat[14] = tz;
		mat[15] = 1.0f;
	}

	void beginUI()
	{
		g->Clear(SG_DEPTH_BUFFER_BIT);
		m_defvb->Clear();
		g->Enable(SG_DEPTH_TEST);
		g->BlendFunc(SG_SRC_ALPHA, SG_ONE_MINUS_SRC_ALPHA);
		g->Enable(SG_BLEND);
		float proj[16];
		Ortho(0.0f, static_cast<float>(m_width), 0.0f, static_cast<float>(m_height), -100.0f, 200.0f, proj);
		m_defshader->Bind();
		m_defshader->SetUniformMatrix4x4("proj", 1, false, proj);
		m_defshader->SetUniform("texture", 0);
		m_defshader->SetUniform("mono",0.0f);
		m_defshader->Unbind();
		g->BindTexture(SG_TEXTURE_2D, m_deftex->GetID());
		m_drawsets.clear();
	}
	void endUI()
	{
		m_defvb->Update();
		m_defvb->Draw();

		g->BlendFunc(SG_ONE_MINUS_DST_COLOR, SG_ONE);//SG_SRC_COLOR, SG_ONE_MINUS_SRC_ALPHA);
		m_defshader->Bind();
		m_defshader->SetUniform("mono",1.0f);
		m_defshader->Unbind();
		const int n = static_cast<int>(m_drawsets.size());
		for (int i = 0; i < n; ++i) {
			g->BindTexture(SG_TEXTURE_2D, m_drawsets[i].tex->GetID());
			m_drawsets[i].vb->Draw();
		}
//		m_defshader->SetUniform("texture", 0);

		g->Disable(SG_BLEND);
	}
	
	friend class fxText;
	void addDrawSet(skGUI::SimpleVB* vb, skGUI::SimpleTex* tex)
	{
		m_drawsets.push_back(DrawSet(vb, tex));
	}
	/*void removeDrawSet(skGUI::SimpleVB* vb, skGUI::SimpleTex* tex)
	{
		size_t n = m_drawsets.size();
		for (size_t i = 0; i < n; ++i){
			if( m_drawsets[i].vb == vb && m_drawsets[i].tex == tex) {
				m_drawsets.erase(m_drawsets.begin() + i);
				return;
			}
		}
	}*/
	
private:
	skGUI::SimpleGraphics* g;
	skGUI::SimpleShader* m_defshader;
	skGUI::SimpleVB* m_defvb;
	skGUI::SimpleTex* m_deftex;
	class DrawSet{
	public:
		DrawSet(skGUI::SimpleVB* vb_, skGUI::SimpleTex* tex_):vb(vb_),tex(tex_){}
		skGUI::SimpleVB* vb;
		skGUI::SimpleTex* tex;
	};
	std::vector<DrawSet> m_drawsets;
	
};
	
enum FXUI_TYPE{
	UITYPE_FXFRAME,
	UITYPE_GRAPHIC,
	UITYPE_BUTTON,
	UITYPE_DYNAMICTEXT,
	UITYPE_TREE,
	UITYPE_TREE_ITEM,
	UITYPE_GRAPHICBUTTON_OPTION,
	UITYPE_MENU_ITEM,
	UITYPE_SLIDER,
};
	
class fxFrame : public skGUI::BaseWindow
{
public:
	fxFrame(fxGUIManager* mgr, int x, int y, int w, int h, const float color[4], int z = 0) : skGUI::BaseWindow(UITYPE_FXFRAME)
	{
		m_defvb = mgr->GetDefaultVB();
		m_x = x; m_y = y; m_width = w; m_height = h;
		m_color[0] = color[0];
		m_color[1] = color[1];
		m_color[2] = color[2];
		m_color[3] = color[3];
		m_z = z;
	}
	~fxFrame()
	{
	}
	void SetColor(const float color[4])
	{
		m_color[0] = color[0];
		m_color[1] = color[1];
		m_color[2] = color[2];
		m_color[3] = color[3];		
	}
protected:
	skGUI::BaseWindow* ownHit(int x, int y){
		if (x >= 0 && x < m_width
		&&  y >= 0 && y < m_height) return this;
		else return 0;
	}
	void ownDraw      (int parent_x, int parent_y)
	{
		const int sx = parent_x + m_x;
		const int sy = parent_y + m_y;
		m_defvb->Color4f(m_color[0], m_color[1], m_color[2], m_color[3]);
		m_defvb->Texcoord2f(1.0f, 1.0f);
		m_defvb->Rect2f(static_cast<float>(sx), static_cast<float>(sy),
			static_cast<float>(sx + m_width), static_cast<float>(sy + m_height), static_cast<float>(m_z));
	}
	bool ownMouseDown (int button, int x, int y){ return false; }
	bool ownMouseUp   (int button, int x, int y){ return false; }
	void ownMouseMove (int x, int y){}
private:
	skGUI::SimpleVB* m_defvb;
	float m_color[4];
	int m_z;
};


class fxGraphic : public skGUI::BaseWindow
{
public:
	fxGraphic(fxGUIManager* mgr, int x, int y, int tex_x, int tex_y, int tex_w, int tex_h, int z = 0) : skGUI::BaseWindow(UITYPE_GRAPHIC)
	{
		m_defvb = mgr->GetDefaultVB();
		m_tex_w = static_cast<float>(mgr->GetTex()->GetWidth());
		m_tex_h = static_cast<float>(mgr->GetTex()->GetHeight());
		m_x = x; m_y = y;
		m_tx = tex_x; m_ty = tex_y;
		m_width = tex_w; m_height = tex_h;
		m_z = z;
	}
	~fxGraphic()
	{
	}
protected:
	skGUI::BaseWindow* ownHit(int x, int y){
		if (x >= 0 && x < m_width
		&&  y >= 0 && y < m_height)
			return this;
		else
			return 0;
	}
	virtual void ownDraw      (int parent_x, int parent_y)
	{
		const int sx = parent_x + m_x;
		const int sy = parent_y + m_y;
		//color is white
		m_defvb->Color4f(1.0f,1.0f,1.0f,1.0f);
		m_defvb->RectUV2f(static_cast<float>(sx), static_cast<float>(sy),
						  static_cast<float>(sx + m_width), static_cast<float>(sy + m_height), static_cast<float>(m_z),
						  static_cast<float>(m_tx / m_tex_w), static_cast<float>(1.0 - m_ty / m_tex_h),
						  static_cast<float>((m_tx + m_width) / m_tex_w), static_cast<float>(1.0 - (m_ty + m_height) / m_tex_h));
	}
	bool ownMouseDown (int button, int x, int y) { return false; }
	bool ownMouseUp   (int button, int x, int y) { return false; }
	void ownMouseMove (int x, int y) {}

	skGUI::SimpleVB* m_defvb;
	int m_tx, m_ty;
	int m_z;
	float m_tex_w,m_tex_h;
};
	
/**
*	@brief For Eye color according to the medium val of node .
*/
class fxGraphicEye : public fxGraphic
{
public:
	fxGraphicEye(fxGUIManager* mgr, int x, int y, int tex_x, int tex_y, int tex_w, int tex_h, int z ,const VX::Math::vec4& nodeEye_color) 
		: fxGraphic(mgr,x,y,tex_x,tex_y,tex_w,tex_h,z)
		 ,m_color(nodeEye_color)
	{
	}
	~fxGraphicEye()
	{
	}
protected:

	virtual void ownDraw      (int parent_x, int parent_y)
	{
		const int sx = parent_x + m_x;
		const int sy = parent_y + m_y;

		//m_defvb->Color4f(1.0f,1.0f,1.0f,1.0f);
		m_defvb->Color4f( m_color.r, m_color.g, m_color.b , 1.0f);
		
		m_defvb->RectUV2f(static_cast<float>(sx), static_cast<float>(sy),
						  static_cast<float>(sx + m_width), static_cast<float>(sy + m_height), static_cast<float>(m_z),
						  static_cast<float>(m_tx / m_tex_w), static_cast<float>(1.0 - m_ty / m_tex_h),
						  static_cast<float>((m_tx + m_width) / m_tex_w), static_cast<float>(1.0 - (m_ty + m_height) / m_tex_h));
	}
	// eye icon color
	VX::Math::vec4 m_color;

};

class fxGraphicDiapha : public fxGraphic
{
public:
	fxGraphicDiapha(fxGUIManager* mgr, int x, int y, int tex_x, int tex_y, int tex_w, int tex_h, int z) 
		: fxGraphic(mgr,x,y,tex_x,tex_y,tex_w,tex_h,z)
	{
	}
	~fxGraphicDiapha()
	{
	}
protected:

	virtual void ownDraw      (int parent_x, int parent_y)
	{
		const int sx = parent_x + m_x;
		const int sy = parent_y + m_y;

		m_defvb->Color4f(1.0f,1.0f,1.0f,1.0f);
	
		m_defvb->RectUV2f(static_cast<float>(sx), static_cast<float>(sy),
						  static_cast<float>(sx + m_width), static_cast<float>(sy + m_height), static_cast<float>(m_z),
						  static_cast<float>(m_tx / m_tex_w), static_cast<float>(1.0 - m_ty / m_tex_h),
						  static_cast<float>((m_tx + m_width) / m_tex_w), static_cast<float>(1.0 - (m_ty + m_height) / m_tex_h));
	}
	// eye icon color
	VX::Math::vec4 m_color;

};

class fxGraphicButton : public fxGraphic
{
public:
	enum MODE{
		MODE_NORMAL = 0,
		MODE_TOGGLE,
		MODE_OPTION
	};
	fxGraphicButton(fxGUIManager* mgr, int x, int y, int tex_x, int tex_y, int tex_w, int tex_h, int z = 0, MODE mode = MODE_NORMAL, void (*func)(int,void*) = 0, int id = 0, void* thisptr = 0)
	: fxGraphic(mgr, x, y, tex_x, tex_y, tex_w, tex_h, z)
	{
		m_toggle = false;
		m_press = false;
		m_mode = mode;
		m_func = func;
		m_id = id;
		m_thisptr = thisptr;
		if (mode == MODE_OPTION)
			m_uiType = UITYPE_GRAPHICBUTTON_OPTION;
	}
	~fxGraphicButton()
	{
	}
	void Push()
	{
		ownMouseDown(0,m_width/2,m_height/2);
		ownMouseUp(0,m_width/2,m_height/2);
	}
	b8 IsPush() const
	{
		return m_toggle;
	}
	
protected:
	bool ownMouseDown (int button, int x, int y) {
		if (!ownHit(x,y))
			return false;
		m_press = true;
		return true;
	}

	bool ownMouseUp (int button, int x, int y) {
		if (!m_press)
			return false;
		m_press = false;
		
		if (!ownHit(x,y))
			return false;
		
		buttonDown();
		return true;
	}

	void otherToggleUp()
	{
		if (!m_parent)
			return;
			
		const size_t n = m_parent->GetNumChildren();
		for (size_t i = 0; i < n; ++i){
			const int uitype = m_parent->GetChild(i)->GetType();
			if (uitype == UITYPE_GRAPHICBUTTON_OPTION) {
				fxGraphicButton* btn = static_cast<fxGraphicButton*>(m_parent->GetChild(i));
				btn->m_toggle = false;
			}
		}
	}
	void buttonDown()
	{
		if (m_mode == MODE_TOGGLE){
			m_toggle = !m_toggle;
		}
		if (m_mode == MODE_OPTION){
			otherToggleUp();
			m_toggle = true;
		}
		if (m_func){
			(*m_func)(m_id, m_thisptr);
		}
	}
	void ownDraw      (int parent_x, int parent_y)
	{
		const int sx = parent_x + m_x;
		const int sy = parent_y + m_y;
		if (m_press || m_toggle)
			m_defvb->Color4f(1.0f,0.5f,0.0f,1.0f);
		else
			m_defvb->Color4f(1.0f,1.0f,1.0f,1.0f);
		m_defvb->RectUV2f(static_cast<float>(sx), static_cast<float>(sy),
						  static_cast<float>(sx + m_width), static_cast<float>(sy + m_height), static_cast<float>(m_z),
						  static_cast<float>(m_tx / m_tex_w), static_cast<float>(1.0 - m_ty / m_tex_h),
						  static_cast<float>((m_tx + m_width) / m_tex_w), static_cast<float>(1.0 - (m_ty + m_height) / m_tex_h));
	}
	
	b8 m_press;
	b8 m_toggle;
	MODE m_mode;
	void (*m_func)(int,void*);
	int m_id;
	void* m_thisptr;
};


class textRasterizer
{
private:
	textRasterizer()
	{
		const unsigned char* fontdata = mplus1cregular;
		stbtt_InitFont(&font, fontdata, 0);
	}
	~textRasterizer()
	{
	}
public:
	static textRasterizer& GetInstance()
	{
		static textRasterizer singleton;
		return singleton;
	}
	
	void GetTextSize(const char* text, float fontsize, int& w, int& h) const
	{
		int ascent,baseline,ch=0;
		float scale, xpos=0;
		scale = stbtt_ScaleForPixelHeight(&font, fontsize);
		stbtt_GetFontVMetrics(&font, &ascent,0,0);
		baseline = (int) (ascent * scale);
	
		while (text[ch]) {
			int chcode = static_cast<int>(*reinterpret_cast<const unsigned char*>(&text[ch]));
			int advance,lsb,x0,y0,x1,y1;
			float x_shift = xpos - (float) floor(xpos);
			stbtt_GetCodepointHMetrics(&font,chcode, &advance, &lsb);
			stbtt_GetCodepointBitmapBoxSubpixel(&font, chcode, scale,scale,x_shift,0, &x0,&y0,&x1,&y1);
			xpos += (advance * scale);
			if (chcode)
				xpos += scale * stbtt_GetCodepointKernAdvance(&font, chcode,chcode);
			ch++;
		}
		int ixpos = static_cast<int>(xpos);
		w = (((ixpos+3)>>2)<<2); // 4byte align
		h = static_cast<int>(fontsize);
		w = (w < 4 ? 4 : w);
		h = (h < 4 ? 4 : h);
		w += 4; // not just size?
	}
	
	void DrawText(const char* text, float fontsize, unsigned char* buf, int buf_w) const
	{
		int ascent,baseline,ch=0;
		float scale, xpos=0;
		scale = stbtt_ScaleForPixelHeight(&font, fontsize);
		stbtt_GetFontVMetrics(&font, &ascent,0,0);
		baseline = (int) (ascent * scale);
		
		while (text[ch]) {
			int chcode = static_cast<int>(*reinterpret_cast<const unsigned char*>(&text[ch]));
			int advance,lsb,x0,y0,x1,y1;
			float x_shift = xpos - (float) floor(xpos);
			stbtt_GetCodepointHMetrics(&font,chcode, &advance, &lsb);
			stbtt_GetCodepointBitmapBoxSubpixel(&font, chcode, scale,scale,x_shift,0, &x0,&y0,&x1,&y1);
			const float nextstep = scale*stbtt_GetCodepointKernAdvance(&font, chcode,chcode) + (advance * scale);
			if (xpos + nextstep > buf_w)
				break;
			unsigned char* bit = &buf[((baseline + y0) * buf_w + (int)xpos + x0)];
			stbtt_MakeCodepointBitmapSubpixel(&font, bit, x1-x0,y1-y0, buf_w, scale,scale,x_shift,0, chcode);
			xpos += nextstep;
			ch++;
		}
	}

private:
	stbtt_fontinfo font;
};
	
class fxText : public skGUI::BaseWindow
{
public:
	fxText(fxGUIManager* mgr, int x, int y, const char* text, float size, const float col[4], int z = 0, int maxwsize = 0) : skGUI::BaseWindow(UITYPE_DYNAMICTEXT)
	{
		m_mgr = mgr;
		m_vb = new skGUI::SimpleVB(mgr->GetGraphics(), mgr->GetDefaultShader());
		m_x = x; m_y = y;
		m_z = z;
		m_size = size;
		m_color[0] = col[0];
		m_color[1] = col[1];
		m_color[2] = col[2];
		m_color[3] = col[3];
		m_func = 0;
		m_funcid = 0;
		m_thisptr = 0;
		m_text = text;
		m_id = 0;
		
		textRasterizer& txt = textRasterizer::GetInstance();
		int w,h;
		txt.GetTextSize(text, size, w, h);
		m_width  = w;
		m_maxwsize = maxwsize = (((maxwsize + 3) >> 2) << 2);
		if (maxwsize != 0)
			m_width = (maxwsize < m_width ? maxwsize : m_width);
		m_height = h;
		m_tex = new skGUI::SimpleTex(mgr->GetGraphics(), m_width, m_height, skGUI::SimpleTex::COLOR_R8);
		unsigned char* ptr = m_tex->Map();
		memset(ptr, 0x00, w*h);
		txt.DrawText(text,size,ptr,m_width);
		m_tex->Unmap();
		
		ownDraw(0,0);
	}
	~fxText()
	{
		//m_mgr->removeDrawSet(m_vb,m_tex);
		delete m_tex;
		delete m_vb;
	}
	void SetColor(const float col[4])
	{
		m_color[0] = col[0];
		m_color[1] = col[1];
		m_color[2] = col[2];
		m_color[3] = col[3];
		m_cache_w = 0; // for update
		m_cache_h = 0;
	}
	void SetText(const char* text)
	{
		m_text = text;
		textRasterizer& txt = textRasterizer::GetInstance();
		int w,h;
		txt.GetTextSize(text, static_cast<float>(m_size), w, h);
		m_width = w;
		if (m_maxwsize != 0)
			m_width = (m_maxwsize < m_width ? m_maxwsize : m_width);
		m_height = h;
		m_tex->Resize(m_width,m_height);
		unsigned char* ptr = m_tex->Map();
		memset(ptr, 0x00, w*h);
		txt.DrawText(text,static_cast<float>(m_size),ptr,m_width);
		m_tex->Unmap();
	}
	void SetPressFunc(void (*func)(const s8*,void*), void* thisptr)
	{
		m_func = func;
		m_thisptr = thisptr;
	}
	void SetPressFunc(void (*func)(s32,void*), s32 id, void* thisptr)
	{
		m_funcid = func;
		m_thisptr = thisptr;
		m_id = id;
	}
	s32 GetID() const
	{
		return m_id;
	}
	void SetID(const s32& id) 
	{
		m_id = id;
	}

protected:
	BaseWindow* ownHit(int x, int y){
		if (x >= 0 && x < m_width &&  y >= 0 && y < m_height) return this;
		else return 0;
	}
	void ownDraw      (int parent_x, int parent_y)
	{
		const int sx = parent_x + m_x;
		const int sy = parent_y + m_y;
		if (m_cache_x != sx || m_cache_y != sy || m_cache_w != m_width || m_cache_h != m_height) {
			m_vb->Clear();
			m_vb->Color4f(m_color[0],m_color[1],m_color[2],m_color[3]);
			m_vb->RectUV2f(static_cast<float>(sx), static_cast<float>(sy),
						   static_cast<float>(sx + m_width), static_cast<float>(sy + m_height), static_cast<float>(m_z), 0.0f, 0.0f, 1.0f, 1.0f);
			m_vb->Update();
		}
		m_mgr->addDrawSet(m_vb, m_tex);
		m_cache_x = sx;
		m_cache_y = sy;
		m_cache_w = m_width;
		m_cache_h = m_height;
	}
	bool ownMouseUp (int button, int x, int y)
	{
		if (ownHit(x,y)) {
			if (m_func)
				(*m_func)(m_text.c_str(), m_thisptr);
			if (m_funcid)
				(*m_funcid)(m_id, m_thisptr);
			return true;
		}
		return false;
	}
	bool ownMouseDown   (int button, int x, int y){ return false; }
	void ownMouseMove (int x, int y) {}
private:
	fxGUIManager* m_mgr;
	skGUI::SimpleGraphics* g;
	skGUI::SimpleVB* m_vb;
	skGUI::SimpleTex* m_tex;
	std::string m_text;
	float m_color[4];
	int m_cache_x, m_cache_y, m_cache_w, m_cache_h;
	float m_size;
	int m_maxwsize;
	int m_z;
	int m_id;
	void (*m_func)(const s8* txt,void*);
	void (*m_funcid)(s32 id,void*);
	void* m_thisptr;

};

	
class fxTree : public skGUI::BaseWindow
{
private:
	class treeItem : public skGUI::BaseWindow
	{
	public:
		enum ExtMode{
			MODE_CHILD,
			MODE_GROUP_OPEN,
			MODE_GROUP_CLOSE,
			MODE_ROOT_OPEN,
			MODE_ROOT_CLOSE
		};

		treeItem(fxGUIManager* mgr, int x, int y, const char* name, ExtMode extmode, const VX::SG::Node* node, fxTree& tree , const VX::Math::vec4& nodeEye_color ) : skGUI::BaseWindow(UITYPE_TREE_ITEM), m_tree(tree), m_node(node)
		{
			const int barw = 300;
			const float fontsize = 16.0f;
			static const float barcol[] = {0.28f,0.28f,0.28f,0.70f};
			static const float fntcol[] = {1.00f,1.00f,1.00f,1.00f};
			m_x = x; m_y = y;
			
			static const int ext_child[]	= {140,20};
			static const int ext_close[]	= {140,40};
			static const int ext_open[]		= {140,60};
			static const int rot_close[]	= {140,80};
			static const int rot_open[]		= {140,100};
			static const int eye_open[]		= {80,100};
			static const int eye_close[]	= {80,120};
			static const int diapha_off[]	= {80,60};
			static const int diapha_on[]	= {80,80};
			int baseZ = tree.m_z;
			int itemZ = tree.m_z + 1;
			switch(extmode) {
				case MODE_CHILD:
					m_extract = new fxGraphic(mgr, -4, 0, ext_child[0], ext_child[1], 32, 18, itemZ);
					break;
				case MODE_GROUP_OPEN:
					m_extract = new fxGraphic(mgr, -4, 0, ext_open[0], ext_open[1], 32, 18, itemZ);
					break;
				case MODE_GROUP_CLOSE:
					m_extract = new fxGraphic(mgr, -4, 0, ext_close[0], ext_close[1], 32, 18, itemZ);
					break;
				case MODE_ROOT_CLOSE:
					m_extract = new fxGraphic(mgr, -4, 0, rot_close[0], rot_close[1], 32, 18, itemZ);
					break;
				case MODE_ROOT_OPEN:
					m_extract = new fxGraphic(mgr, -4, 0, rot_open[0], rot_open[1], 32, 18, itemZ);
					break;
				default:
					assert(0);
			};

			if (node->GetVisible()){
				m_eye = new fxGraphicEye(mgr,barw-x-32, 0, eye_open[0], eye_open[1], 30, 18, itemZ,nodeEye_color);
			}else{
				m_eye = new fxGraphicEye(mgr,barw-x-32, 0, eye_close[0], eye_close[1], 30, 18, itemZ,nodeEye_color);
			}

			if (node->GetType() == VX::SG::NODETYPE_GEOMETRY
				|| (node->GetType() == VX::SG::NODETYPE_GROUP 
				  && node->GetName() != "root" 
				  )) {
				if (node->GetDiapha()){
					m_diapha = new fxGraphicDiapha(mgr,barw-x-62, 0, diapha_on[0], diapha_on[1], 30, 18, itemZ);
				}else{
					m_diapha = new fxGraphicDiapha(mgr,barw-x-62, 0, diapha_off[0], diapha_off[1], 30, 18, itemZ);
				}
			} else {
				m_diapha = NULL;
			}

			m_bar     = new fxFrame(mgr, 0,0,barw - x,19,barcol, baseZ);
			m_txt     = new fxText(mgr, 30, 1, name, fontsize, fntcol, itemZ);//, barw - x - 60);
			AddChild(m_bar);
			AddChild(m_extract);
			AddChild(m_eye);
			if (m_diapha != NULL) {
				AddChild(m_diapha);
			}
			AddChild(m_txt);
			m_press = false;
			m_shift = m_ctrl = false;
			m_width = m_bar->GetWidth();
			m_height = m_bar->GetHeight();
		}
		void SetSelection  (b8 select){
			VX::SG::Node* nd = const_cast<VX::SG::Node*>(m_node);
			nd->SetSelection(select);
		}

		~treeItem()
		{
		}
	protected:
		skGUI::BaseWindow* ownHit(int x, int y){
			if (x >= 0 && x < m_width &&  y >= 0 && y < m_height) return this;
			else return 0;
		}
		void ownDraw      (int parent_x, int parent_y)
		{
			static const float barcol_select[] = {0.58f,0.38f,0.28f,0.70f};
			static const float barcol[] = {0.28f,0.28f,0.28f,0.70f};
			if (m_node->GetSelection())
				m_bar->SetColor(barcol_select);
			else
				m_bar->SetColor(barcol);
		}
		bool ownMouseDown (int button, int x, int y) {
			if (!ownHit(x,y))
				return false;

			m_press = true;
			return true;
		}

		/**
		*	@brief 
		*	@param[in] button	0:left click	(selection, if ctrl key press , it to be cancel selection) , 
		*						1:right click	(cancel selection)
		*/
		bool ownMouseUp   (int button, int x, int y) {
			if (!m_press)
				return false;
			m_press = false;
			
			if (!ownHit(x,y))
				return false;
			
			
			if (x < 30){
				//expand or collapse +,- button 
				m_tree.m_grpext[m_node] = !m_tree.m_grpext[m_node];
				m_tree.treeUpdate();
			} else if (m_diapha != NULL && x > m_diapha->GetX() && x < m_eye->GetX()){
				// diapha icon
				VX::SG::Node* nd = const_cast<VX::SG::Node*>(m_node);
				if(nd->GetType() == VX::SG::NODETYPE_GROUP) {
					VX::SG::Group* grp = static_cast<VX::SG::Group*>(nd);
					grp->SetDiapha(!nd->GetDiapha());
				} else {
					nd->SetDiapha(!nd->GetDiapha());
				}
				m_tree.treeUpdate();
			} else if (x > m_eye->GetX()){
				// eye icon

				
				VX::SG::Node* nd = const_cast<VX::SG::Node*>(m_node);
				if (nd->GetType() != VX::SG::NODETYPE_MEDIUM && !(nd->GetType() == VX::SG::NODETYPE_MEDIUM_GROUP)) {
					//mediumは目のアイコンは常に表示
					nd->SetVisible(!nd->GetVisible());
				}

				m_tree.treeUpdate();
				// notify children windows;
				UI::Notify_EyeStatus();
				
			} else if (
				m_node->GetType() == VX::SG::NODETYPE_GROUP                    	  || 
				m_node->GetType() == VX::SG::NODETYPE_TRANSFORM                	  || 
				m_node->GetType() == VX::SG::NODETYPE_GEOMETRY                    || 
				m_node->GetType() == VX::SG::NODETYPE_BBOX_DOMAIN_CARTESIAN_GROUP || 
				m_node->GetType() == VX::SG::NODETYPE_BBOX_DOMAIN_BCM_GROUP
				) {

				// name 
				VX::SG::Node* nd = const_cast<VX::SG::Node*>(m_node);
				if (button == 0){
					nd->SetSelection(!nd->GetSelection());
				}else if (button == 1){
					m_tree.SetSelectAllTreeItem(false);
					nd->SetSelection(true);
					m_tree.ShowTreeMenu(true, x + m_x, y + m_y, nd);
				}
				// show triangle count
				m_tree.showCountTriangles(m_node);

				// set clipboard for the selected node name
				UI::SetClipboard(m_node->GetName());
				VXLogD("clipboard=%s\n",UI::GetClipboard().c_str());

			}
			return true;
		}
		void ownMouseMove (int x, int y) {}
		enum {
			SHIFT_KEY = 306,
			ALT_KEY   = 307,
			CTRL_KEY  = 308
		};
		void ownKeyDown  (int key){
			if (key == SHIFT_KEY) m_shift = true;
			if (key == CTRL_KEY)  m_ctrl  = true;
		}
		void ownKeyUp  (int key){
			if (key == SHIFT_KEY) m_shift = false;
			if (key == CTRL_KEY)  m_ctrl  = false;
		}

		bool isChildEvent (int x, int y)   { return false; }; // igore child event

	private:
		fxGraphic* m_extract;
		fxGraphic* m_eye;
		fxGraphic* m_diapha;
		fxFrame*   m_bar;
		fxText*    m_txt;
		fxTree& m_tree;
		const VX::SG::Node* m_node;
		bool m_press;
		bool m_shift,m_ctrl;
	};
	class fxScrollTip : public fxGraphic
	{
	public:
		fxScrollTip(fxGUIManager* mgr, int x, int y, int tx, int ty, int tw, int th, int z, skGUI::BaseWindow* scrollbase, skGUI::BaseWindow* tray)
		: fxGraphic(mgr, x,y,tx,ty,tw,th,z)
		{
			m_press = false;
			m_base = scrollbase;
			m_tray = tray;
		}
		void UpdatePos()
		{
			float rate = - m_tray->GetY() / static_cast<float>(m_tray->GetHeight() - m_base->GetHeight());
			if (rate > 1.0f)
				rate = 1.0f;
			m_y = static_cast<int>((m_base->GetHeight() - m_height) * rate);
			changed(rate);
		}

	protected:
		bool ownMouseDown(int button, int x, int y)
		{
			if (!ownHit(x,y))
				return false;
			m_press = true;
			oy = y;
			return true;
		}
		bool ownMouseUp   (int button, int x, int y)
		{
			m_press = false;
			return false;
		}
		void ownMouseMove(int x, int y)
		{
			if (m_press) {
				m_y += y - oy;
				const int max_y = m_base->GetHeight() - m_height;
				if (m_y > max_y)
					m_y = max_y;
				if (m_y < 0)
					m_y = 0;
				const float rate = m_y / static_cast<float>(max_y);
				changed(rate);
			}
		}
		void changed(float rate)
		{
			m_tray->SetPos(m_tray->GetX(),static_cast<int>( - (m_tray->GetHeight() - m_base->GetHeight()) * rate));
		}

	private:
		skGUI::BaseWindow* m_base, *m_tray;
		bool m_press;
		int oy;
	};
public:
	fxTree(fxgenCore* core ,GUIController* gui, fxGUIManager* mgr, int x, int y, int z = 0) : skGUI::BaseWindow(UITYPE_TREE)
	{
		m_x = x;
		m_y = y;
		m_z = z;
		m_width  = 0;
		m_height = 0;
		m_mgr = mgr;
		m_core = core;
		m_gui = gui;
		m_base = new skGUI::BlankWindow();
		m_base->SetSize(0, 0);
		this->AddChild(m_base);
		static const float scrollcolor[] = {0.28f,0.28f,0.28f,0.70f};//{0.17f,0.17f,0.17f,0.7f};
		m_scroll = new fxFrame(mgr, 304, 0, 17, 1000, scrollcolor, z);
		m_scroll->SetSize(m_scroll->GetWidth(),0);
		m_tip = new fxScrollTip(mgr, 0, 0, 80, 140, 17, 46, z+1, m_scroll, m_base);
		this->AddChild(m_scroll);
		m_scroll->AddChild(m_tip);
		m_node = 0;
		m_needupdate = false;
	}
	~fxTree()
	{
	}
	void SetSceneGraph(const VX::SG::Node* node, bool cleartree = false)
	{
		if (cleartree)
			m_grpext.clear();
		m_node = node;
		refresh();
	}
	
	void Update()
	{
		if (m_needupdate) {
			m_needupdate = false;
			refresh();
		}
	}
	void ForceUpdate()
	{
		m_needupdate = true;
		Update();
	}
	
protected:	
	int refresh_rec(const VX::SG::Node* node, int px, int& py)
	{
		if (!node)
			return py;
		if (node->GetIntermediate())
			return py;
		
		// medium color id of node
		//white is default
		VX::Math::vec4 eyeColor(1,1,1,1);
		getNodeEyeColorByMedium(m_core , node , eyeColor);

		const VX::SG::NODETYPE ntype = node->GetType();
		int my_y = py;
		// SG::Group

		if(ntype == VX::SG::NODETYPE_MEDIUM){
			const VX::SG::Medium* medium = static_cast <const VX::SG::Medium*>(node);
			eyeColor = medium->GetColor();
		}

		b8 isGroup = 
			(
			ntype == VX::SG::NODETYPE_GROUP                       ||
		    ntype == VX::SG::NODETYPE_TRANSFORM                   ||
			ntype == VX::SG::NODETYPE_BBOX_DOMAIN_SLICE_GROUP     ||
			ntype == VX::SG::NODETYPE_BBOX_DOMAIN_CARTESIAN_GROUP ||
			ntype == VX::SG::NODETYPE_BBOX_DOMAIN_BCM_GROUP       ||
			ntype == VX::SG::NODETYPE_BBOX_DOMAIN_BCM_SUB_GROUP   ||
			ntype == VX::SG::NODETYPE_MEDIUM_GROUP                ||
			ntype == VX::SG::NODETYPE_MESH_CARTESIAN              ||
			ntype == VX::SG::NODETYPE_MESH_BCM                    ||
			ntype == VX::SG::NODETYPE_VOXEL_CARTESIAN_G           ||
			ntype == VX::SG::NODETYPE_VOXEL_CARTESIAN_L           ||
			ntype == VX::SG::NODETYPE_VOXEL_CARTESIAN_C
			);

		if(ntype == VX::SG::NODETYPE_VOXEL_CARTESIAN_G){
			//check is visible tree node or not
			const VX::SG::VoxelCartesianG* g = dynamic_cast<const VX::SG::VoxelCartesianG*>(node);
			assert(g!=NULL);
			isGroup = g->IsShowTreeNode();
		}

		if (isGroup)
		{
			treeItem::ExtMode emode;
			bool extopen = false;
			std::map<const VX::SG::Node*,bool>::const_iterator it = m_grpext.find(node);
			if (it == m_grpext.end())
				m_grpext[node] = extopen;
			else
				extopen = it->second;
			
			if (m_base->GetNumChildren()) {
				if (extopen) emode = treeItem::MODE_GROUP_OPEN;
				else         emode = treeItem::MODE_GROUP_CLOSE;
			} else {
				if (extopen) emode = treeItem::MODE_ROOT_OPEN;
				else         emode = treeItem::MODE_ROOT_CLOSE;
			}
			
			// add tree item
			treeItem* m_item = new treeItem(m_mgr, px, py, node->GetName().c_str(), emode, node, *this,eyeColor);
			m_base->AddChild(m_item);
		
			py += 20;
			if (!extopen)
				return my_y;
			
			// Childs
			const VX::SG::Group* grp = static_cast<const VX::SG::Group*>(node);
			const s32 cn = grp->GetChildCount();

			const int cpx = px + 18;
			int last_y = 0;
			for (s32 i = 0; i < cn; ++i) {
				last_y = refresh_rec(grp->GetChild(i), cpx, py);
			}
			static const float linecol[] = {0.87f,0.32f,0.05f,1.0f};
			fxFrame* line = new fxFrame(m_mgr, px + 13, my_y + 10, 1, last_y - my_y,linecol, m_z+2);
			m_base->AddChild(line);
		
		}
		// SG::Node other SG::Group
		else
		{
			
			treeItem* m_item = new treeItem(m_mgr, px, py, node->GetName().c_str(), treeItem::MODE_CHILD, node, *this,eyeColor);
			m_base->AddChild(m_item);
			
			py += 20;
		}
		return my_y;
	}
	

	/**
	*	@brief node eye color
	*	@param[in] node
	*	@param[out] color
	*
	*/
	b8 getNodeEyeColorByMedium(fxgenCore* core , const VX::SG::Node* nd, VX::Math::vec4& eyeColor )
	{
		// medium val
		VX::SG::Node* ndp = const_cast<VX::SG::Node*>(nd);
		
		VX::SG::NODETYPE t = ndp->GetType();
		if(t != VX::SG::NODETYPE_GEOMETRY){
			return false;
		}

		VX::SG::Geometry* g = static_cast<VX::SG::Geometry*>(ndp);
		if(g==NULL ){
			return false;
		}

		// the first triangle 's medium value become the color of Node icon
		u32* idb = g->GetIDBuffer();
		if(idb==NULL){
			return false;// texture etc
		}

		const u32 n		= g->GetIndexCount()/3;
		int color_id = 0;
		for (u32 i = 0; i < n; i++)
		{
			//get color id byte 
			color_id = (idb[i] >> 8) & 0xFF;
			if(color_id>0){
				// first hit tri color is node eye color
				//get tri's medium ID = colorIndex
				break;
			}
		}
		if(color_id==0){
			return false;
		}
		// get color by colorID()
		eyeColor = core->GetIDColorTable(color_id);
			
		return true;
	}

	void refresh_scrollbar()
	{
		m_scroll->SetSize(m_scroll->GetWidth(), m_height);
		const bool v = m_base->GetHeight() > m_scroll->GetHeight();
		m_scroll->SetShow(v);
		if (v)
			m_tip->UpdatePos();
		else {
			m_tip->SetPos(0,0);
			m_base->SetPos(m_base->GetX(), 0);
		}
	}
	
	void refresh()
	{
		m_base->ClearChild();
		
		if (!this->IsShow())
			return;
		
		int px = 0, py = 0;
		refresh_rec(m_node, px, py);
		
		m_base->SetSize(250, py);
		refresh_scrollbar();
	}

	void ownResized()
	{
		refresh_scrollbar();
	}
	
	void treeUpdate()
	{
		m_needupdate = true;
	}
	
	void showCountTriangles(const VX::SG::Node* node)
	{
		VX::SG::Node* nd = const_cast<VX::SG::Node*>(node);
		b8 recursible = true; 
		s32 total = 0;
		countTrisize( nd,recursible,total );

		std::ostringstream stream;
		//path from root
		std::string outPath;
		const VX::SG::Node* root = this->m_core->GetRoot();
		getPath( root , node ,  "" ,  outPath  );
		stream << outPath << " [polygon sum=" << total << "]";
		std::string result = stream.str();
	
		m_gui->SetMessage(result.c_str());
	}

	void getPath(const VX::SG::Node* self,const VX::SG::Node* target ,  const std::string& dir , std::string& outPath  )
	{

		using namespace VX::SG;
		using namespace VX::Math;
		const NODETYPE t = self->GetType();
		std::string selfdir = dir + "/" + self->GetName();
		if(self == target){
			outPath = selfdir;
			return ;
		}
	
		//call children
		if (t == NODETYPE_GROUP  || t == NODETYPE_TRANSFORM) {
	
			VX::SG::Node* s_self = const_cast<VX::SG::Node*>(self);
			Group* grp = static_cast<Group*>(s_self);
			s32 n = grp->GetChildCount();
			for (s32 i = 0; i < n; i++) {
				std::string parent_dir = selfdir;
				getPath( grp->GetChild(i),  target ,  parent_dir , outPath  );
				if(outPath.size()>0){
					//end
					return ;
				}
			}
		}
	
	}

	void countTrisize( VX::SG::Node* node ,const b8& recursible , s32& total)
	{
		if(!node->GetVisible()){
			return ;
		}

		using namespace VX::SG;
		NODETYPE t = node->GetType();
		if(t==NODETYPE_GEOMETRY){
			Geometry* g = static_cast<Geometry*>(node);
			VX::SG::Index n = g->GetIndexCount();
			total += (n/3);// total is tri size. n is point size.
		}

		if(!recursible){
			return;
		}

		if(t==NODETYPE_GROUP || t == NODETYPE_TRANSFORM){
			Group* g = static_cast<Group*>(node);
			const s32 n = g->GetChildCount();
			for (s32 i = 0; i < n; i++){
				countTrisize(g->GetChild(i),recursible,total);
			}
		}
	}

	skGUI::BaseWindow* ownHit(int x, int y){
		if (x >= 0 && x < m_width &&  y >= 0 && y < m_base->GetHeight()) return this;
		else return 0;
	}
	void ownDraw      (int parent_x, int parent_y)
	{
	}
	bool ownMouseDown (int button, int x, int y) { return false; }
	bool ownMouseUp   (int button, int x, int y) { return false; }
	void ownMouseMove (int x, int y) {}
	void ShowTreeMenu (b8 show, int x, int y, VX::SG::Node* node)
	{
		m_gui->ShowTreeMenu(show, x, y, node);
	}
	void SetSelectAllTreeItem (b8 select)
	{
		if(m_base->GetChild(0) != NULL) {
			treeItem* root = dynamic_cast<treeItem*>(m_base->GetChild(0));
			root->SetSelection(select);
		}
	}
private:
	fxgenCore*		m_core;
	GUIController*	m_gui;

	fxGUIManager* m_mgr;
	skGUI::BlankWindow* m_base;
	fxFrame* m_scroll;
	fxScrollTip* m_tip;
	bool m_needupdate;
	
	const VX::SG::Node* m_node;
	std::map<const VX::SG::Node*, bool> m_grpext;
	int m_z;
};

class fxSplash : public fxGraphic
{
public:
	fxSplash(fxGUIManager* mgr, int x, int y, int tex_x, int tex_y, int tex_w, int tex_h, int z = 0)
	: fxGraphic(mgr, x, y, tex_x, tex_y, tex_w, tex_h, z), m_alpha(1.0f)
	{
	}
	void StartFade(f64 tm_sec)
	{
		m_starttime = VX::GetTimeCount();
		m_endtime = m_starttime + tm_sec;
	}
	b8 IsAnimating()
	{
		if (!IsShow())
			return false;
	
		f64 tm = VX::GetTimeCount();
		using namespace VX::Math;
		m_alpha = static_cast<float>(1.0 - min(max(tm - m_endtime + 1.0, 0.0), 1.0));
		if (m_endtime < tm){
			SetShow(false);
		}
		return true;
	}
	
private:
	virtual void ownDraw      (int parent_x, int parent_y)
	{
		const int sx = parent_x + m_x;
		const int sy = parent_y + m_y;
		m_defvb->Color4f(m_alpha,m_alpha,m_alpha,m_alpha);
		m_defvb->RectUV2f(static_cast<float>(sx), static_cast<float>(sy),
						  static_cast<float>(sx + m_width), static_cast<float>(sy + m_height), static_cast<float>(m_z),
						  static_cast<float>(m_tx / m_tex_w), static_cast<float>(1.0 - m_ty / m_tex_h),
						  static_cast<float>((m_tx + m_width) / m_tex_w), static_cast<float>(1.0 - (m_ty + m_height) / m_tex_h));
	}

	f32 m_alpha;
	f64 m_starttime;
	f64 m_endtime;
};
	
class fxMenu : public BaseWindow
{
public:
	class fxMenuItem : public fxText
	{
	public:
		fxMenuItem(fxGUIManager* mgr, int x, int y, const char* text, float size, const float col[4], int z = 0, int maxwsize = 0)
		: fxText(mgr, x,y, text, size, col, z, maxwsize)
		{
			static const float red[] = {0.87f,0.32f,0.05f,1.0f};
			m_check = new fxFrame(mgr, -6, 2, 3, 14, red, z+1);
			m_check->SetShow(false);
			AddChild(m_check);
		}
		~fxMenuItem()
		{
		}
		
		void SetCheck(b8 check)
		{
			m_check->SetShow(check);
		}
	private:
		fxFrame* m_check;
	};
	
	class fxMenuFrame : public fxFrame
	{
	public:
		fxMenuFrame(fxGUIManager* mgr, int x, int y, int w, int h, const float color[4], int z = 0)
		: fxFrame(mgr, x, y, w, h, color, z)
		{
		}
	private:
		void ownMouseMove (int x, int y)
		{
			using namespace VX::Math;
			const int hspan = 24;
			const int wspan = 12;
			if (-hspan   < y && y < 0)                y = 0;
			if (m_height < y && y < m_height + hspan) y = m_height-1;
			if (-wspan   < x && x < 0)                x = 0;
			if (m_width < x && x < m_width + wspan)   x = m_width-1;
			
			if (!ownHit(x,y)) {
				SetShow(false);
			}
		}
	};
	
	fxMenu(fxGUIManager* mgr, int x, int y, int w, int h, int z = 0) : skGUI::BaseWindow(UITYPE_FXFRAME)
	{
		m_mgr = mgr;
		m_mx = 0;
		m_mz = z;
		m_x = x;
		m_y = y;
		m_width = w;
		m_height = h;
		m_func = 0;
		m_thisptr = 0;
	}
	virtual fxText* AddMenu(const s8* name , b8 visible = true)
	{
		const float fontsize = 18.0f;
		const float white[] = {1.0f,1.0f,1.0f,1.0f};
		const float gray[] = {0.50f,0.50f,0.50f,1.0f};
		fxText* txt = new fxText(m_mgr, m_mx, 0, name, fontsize, white, m_mz+1);
		m_submenu[name] = new fxMenuFrame(m_mgr, -4 + m_mx, 20, 0,0, gray, m_mz+1);
		m_submenu[name]->SetShow(false);
		this->AddChild(m_submenu[name]);
		txt->SetPressFunc(onPressTopMenu_, this);
		AddChild(txt);
		m_mx += txt->GetWidth() + 20;
		txt->SetShow(visible);
		//m_submenu[name]->SetPos(100, 100);

		return txt;
	}
	static void onPressTopMenu_(const s8* text, void* thisptr)
	{
		VXLogD("Menu:%s\n", text);
		fxMenu* ptr = static_cast<fxMenu*>(thisptr);
		ptr->onPressTopMenu(text);
	}
	void onPressTopMenu(const s8* text)
	{
		std::map<std::string, skGUI::SharedRefPtr<fxMenuFrame> >::iterator it,eit = m_submenu.end();
		// all invisible
		for (it = m_submenu.begin(); it != eit; ++it)
			it->second->SetShow(false);
		
		// visible selected menu
		it = m_submenu.find(std::string(text));
		if (it != m_submenu.end())
			it->second->SetShow(!it->second->IsShow());
	}
	static void onPressMenu_(s32 id, void* thisptr)
	{
		fxMenu* ptr = static_cast<fxMenu*>(thisptr);
		ptr->OnPressMenu(id);
	}
	virtual void OnPressMenu(s32 id)
	{
		if (m_func)
			(*m_func)(id, m_thisptr);

		// all invisible
		std::map<std::string, skGUI::SharedRefPtr<fxMenuFrame> >::iterator it,eit = m_submenu.end();
		for (it = m_submenu.begin(); it != eit; ++it)
			it->second->SetShow(false);
	}
	fxText* AddSubMenuMenu(const s8* topname, const s8* subname)
	{
		const float fontsize = 18.0f;
		const float white[] = {1.0f,1.0f,1.0f,1.0f};
		const float wgray[] = {0.66f,0.66f,0.66f,1.0f};
		const float gray[] = {0.44f,0.44f,0.44f,1.0f};
		
		fxText* txt = NULL;

		std::map<std::string, skGUI::SharedRefPtr<fxMenuFrame> >::iterator it = m_submenu.find(std::string(topname));
		if (it != m_submenu.end())
		{
			s32 n = static_cast<s32>(it->second->GetNumChildren());
			txt = new fxText(m_mgr, 12, n * 20, subname, fontsize, white, m_mz+2);
			txt->SetPressFunc(onPressTopMenu_, this);
			it->second->AddChild(txt); // submenu text

			// sub menu
			m_submenu[subname] = new fxMenuFrame(m_mgr, -12 + txt->GetX() + txt->GetParent()->GetX(), txt->GetY() + txt->GetParent()->GetY(), 0,0, gray, m_mz+1);
			m_submenu[subname]->SetShow(false);
			
			static const float red[] = {0.87f,0.32f,0.05f,1.0f};
			m_submenu[subname]->AddChild(new fxText(m_mgr, 12, 0, subname, fontsize, red, m_mz+2));
			this->AddChild(m_submenu[subname]);

			s32 maxw = 0;
			for (s32 i = 0; i < n+1; ++i){
				if (maxw < it->second->GetChild(i)->GetWidth())
					maxw = it->second->GetChild(i)->GetWidth() + 10;
			}
			it->second->SetSize(maxw + 8, 20 * (n+1));
		}
		return txt;
	}

	fxText* AddSubMenu(const s8* topname, s32 id, const s8* subname)
	{
		fxText* rettxt = 0;
		const float fontsize = 18.0f;
		const float white[] = {1.0f,1.0f,1.0f,1.0f};
		std::map<std::string, skGUI::SharedRefPtr<fxMenuFrame> >::iterator it = m_submenu.find(std::string(topname));
		if (it != m_submenu.end())
		{
			s32 n = static_cast<s32>(it->second->GetNumChildren());
			BaseWindow* item = 0;
			if (std::string(subname) == "-") {
				item = new fxFrame(m_mgr, 4, n * 20 + 10, 200,1, white, m_mz+2);
				rettxt = 0;
			} else {
				fxMenuItem* txt = new fxMenuItem(m_mgr, 12, n * 20, subname, fontsize, white, m_mz+2);
				item = static_cast<BaseWindow*>(txt);
				txt->SetPressFunc(onPressMenu_, id, this);
				rettxt = txt;
			}
			it->second->AddChild(item);
			
			s32 maxw = 0;
			for (s32 i = 0; i < n+1; ++i){
				if (maxw < it->second->GetChild(i)->GetWidth())
					maxw = it->second->GetChild(i)->GetWidth() + 10;
			}
			it->second->SetSize(maxw + 8, 20 * (n+1));
		}
		return rettxt;
	}
	
	virtual void SetMenuFunc(void (*func)(s32 menuid, void* thisptr), void* thisptr)
	{
		m_func = func;
		m_thisptr = thisptr;
	}
	
	
	void CheckMenu(const s8* menuname, s32 menuid, b8 check)
	{
		std::map<std::string, skGUI::SharedRefPtr<fxMenuFrame> >::iterator it = m_submenu.find(std::string(menuname));
		if (it == m_submenu.end())
			return;
		
		const s32 n = static_cast<s32>(it->second->GetNumChildren());
		for (s32 i = 0; i < n; ++i){
			BaseWindow* w = it->second->GetChild(i);
			if (w->GetType() != UITYPE_DYNAMICTEXT)
				continue;
			
			fxText* txt = static_cast<fxText*>(w);
			if (txt->GetID() != menuid)
				continue;
			
			fxMenuItem* item = static_cast<fxMenuItem*>(txt);
			item->SetCheck(check);
		}
	}
	void EnableMenu(const s8* menuname, s32 menuid, b8 enable)
	{
		std::map<std::string, skGUI::SharedRefPtr<fxMenuFrame> >::iterator it = m_submenu.find(std::string(menuname));
		if (it == m_submenu.end())
			return;
		
		const float white[] = {1.0f,1.0f,1.0f,1.0f};
		const float gray[] = {0.44f,0.44f,0.44f,1.0f};
		
		const s32 n = static_cast<s32>(it->second->GetNumChildren());
		for (s32 i = 0; i < n; ++i){
			BaseWindow* w = it->second->GetChild(i);
			if (w->GetType() != UITYPE_DYNAMICTEXT)
				continue;
			
			fxText* txt = static_cast<fxText*>(w);
			if (txt->GetID() != menuid)
				continue;
			
			if (enable)
				txt->SetColor(white);
			else
				txt->SetColor(gray);
			txt->SetEnable(enable);
		}
	}

	void SetMenuPos(const s8* menuname, int x, int y)
	{
		m_submenu[menuname]->SetPos(x, y);
	}

	void SetNode(VX::SG::Node* node) { m_node = node; }
	VX::SG::Node* GetNode()	{ return m_node; }

private:
	skGUI::BaseWindow* ownHit(int x, int y){
		if (x >= 0 && x < m_width &&  y >= 0 && y < m_height) return this;
		else return 0;
	}
	void ownDraw      (int parent_x, int parent_y)
	{
	}
	bool ownMouseDown (int button, int x, int y) { return false; }
	bool ownMouseUp   (int button, int x, int y) { return false; }
	void ownMouseMove (int x, int y) {}

	fxGUIManager* m_mgr;
	s32 m_mx,m_mz;
	std::map<std::string, skGUI::SharedRefPtr<fxMenuFrame> > m_submenu;
	void (*m_func)(s32 menuid, void* thisptr);
	void* m_thisptr;
	VX::SG::Node* m_node;
};

}// fxGUI

#endif // INCLUDE_FXGUI_H

