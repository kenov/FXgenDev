
#include "../VX/VX.h"
#include "../VX/Math.h"
#include "../VX/SG/SceneGraph.h"

#include <cmath>

#include "DomainCartesianEditor.h"

namespace {

inline
unsigned long long
CalcCommSize( unsigned long long iDiv
            , unsigned long long jDiv
            , unsigned long long kDiv
            , unsigned long long voxSize[3])
{
    if( (iDiv==0) || (jDiv==0) || (kDiv==0) ) return 0;
	if( !voxSize ) return 0;
	
	unsigned long long Len[3];
	Len[0] = voxSize[0] / iDiv; if( Len[0] == 0 ) return 0;
	Len[1] = voxSize[1] / jDiv; if( Len[1] == 0 ) return 0;
	Len[2] = voxSize[2] / kDiv; if( Len[2] == 0 ) return 0;
	
	unsigned long long commFace[3];
	if( iDiv != 1) commFace[0] = Len[1]*Len[2]*(iDiv-1);
	else commFace[0] = 0;
	if( jDiv != 1) commFace[1] = Len[2]*Len[0]*(jDiv-1);
	else commFace[1] = 0;
	if( kDiv != 1) commFace[2] = Len[0]*Len[1]*(kDiv-1);
	else commFace[2] = 0;
	
	return (commFace[0] + commFace[1] + commFace[2]);
}

bool DecideDivPattern( int divNum , unsigned int voxSize[3] , unsigned int divPttn[3])
{
	if( !voxSize || !divPttn )
	{
		return false;//CPM_ERROR_INVALID_PTR;
	}
	if( (voxSize[0]==0) || (voxSize[1]==0) || (voxSize[2]==0) )
	{
		return false;//CPM_ERROR_INVALID_VOXELSIZE;
	}
	if( divNum <= 1 ){
		divPttn[0] = divPttn[1] = divPttn[2] = 1;
		return true;//CPM_SUCCESS;
	}
	
	divPttn[0] = divPttn[1] = divPttn[2] = 0;
	
	unsigned long long minCommSize = 0;
	
	unsigned long long divNumll = divNum;
	unsigned long long voxSizell[3] = {0, 0, 0};
	unsigned long long divPttnll[3] = {0, 0, 0};
	voxSizell[0] = voxSize[0];
	voxSizell[1] = voxSize[1];
	voxSizell[2] = voxSize[2];
	
	bool flag = false;
	unsigned long long i, j, k;
	for(i=1; i<=divNumll; i++)
	{
		if( divNumll%i != 0 ) continue;
		if( (voxSizell[0] / i) < 1 ) break;
		unsigned long long jmax = divNumll/i;
		for(j=1; j<=jmax; j++)
		{
			if( jmax%j != 0 ) continue;
			if( (voxSizell[1] / j) < 1 ) break;
			
			k = jmax/j;
			if( (voxSizell[2] / k) < 1 ) break;
			
			unsigned long long commSize;
			if( (commSize=CalcCommSize(i, j, k, voxSizell)) == 0 ) break;
			
			if( !flag )
			{
				divPttnll[0] = i; divPttnll[1] = j; divPttnll[2] = k;
				minCommSize = commSize;
				flag = true;
			}
			else if( commSize < minCommSize )
			{
				divPttnll[0] = i; divPttnll[1] = j; divPttnll[2] = k;
				minCommSize = commSize;
			}
		}
	}
	
	divPttn[0] = static_cast<unsigned int>(divPttnll[0]);
	divPttn[1] = static_cast<unsigned int>(divPttnll[1]);
	divPttn[2] = static_cast<unsigned int>(divPttnll[2]);
	
	if( (divPttn[0]==0) || (divPttn[1]==0) || (divPttn[2]==0) )
	{
		return false;//CPM_ERROR_DECIDE_DIV_PATTERN;
	}
	
	return true;//CPM_SUCCESS;
}


} // namespace


DomainCartesianEditor::DomainCartesianEditor(VX::SG::Group* root, VX::SG::DomainCartesian* domain)
 : DomainEditor(DOMAIN_EDITOR_CARTESIAN, domain), m_sg(root)
{

}

DomainCartesianEditor::~DomainCartesianEditor() 
{ 
	m_sg = NULL;
}

b8 DomainCartesianEditor::SetDefault()
{
	VX::SG::DomainCartesian* domain = static_cast<VX::SG::DomainCartesian*>(GetActiveDomain());
	if(!domain) return false;

	VX::Math::vec3 min, max;
	GetBBox(m_sg, max, min, false, true);
	domain->SetBBoxMinMax(min, max);
	domain->SetVoxelCount(VX::Math::idx3(1, 1, 1));
	domain->SetDivCount(VX::Math::idx3(1, 1, 1));
	domain->UpdateGeometry();	

	return true;
}

b8 DomainCartesianEditor::Centering(u8 axisFlag)
{
	VX::SG::DomainCartesian* domain = static_cast<VX::SG::DomainCartesian*>(GetActiveDomain());
	if(!domain) return false;

	VX::Math::vec3 curMin, curMax, curSize;
	GetBBoxMinMaxSize(curMin, curMax, curSize);

	VX::Math::vec3 min, max;
	GetBBox(m_sg, max, min, false, true);
	VX::Math::vec3 center = (max + min) * 0.5f;
	
	VX::Math::vec3 newMin, newMax;
	newMin = curMin;
	newMax = curMax;

	for(u32 i = 0; i < 3; i++){
		if( (axisFlag >> i) & 1 ){
			newMin[i] = center[i] - (curSize[i] * 0.5f);
			newMax[i] = center[i] + (curSize[i] * 0.5f);
		}
	}

	return domain->SetBBoxMinMax(newMin, newMax, true);
}

b8 DomainCartesianEditor::SetBBoxMinMax(const VX::Math::vec3& min, const VX::Math::vec3& max)
{
	VX::SG::DomainCartesian* domain = static_cast<VX::SG::DomainCartesian*>(GetActiveDomain());
	if(!domain) return false;

	return domain->SetBBoxMinMax(min, max, true);
}


b8 DomainCartesianEditor::SetVoxelCount(const VX::Math::idx3& vox)
{
	VX::SG::DomainCartesian* domain = static_cast<VX::SG::DomainCartesian*>(GetActiveDomain());
	if(!domain) return false;

	if( vox.x < 1 || vox.y < 1 || vox.z < 1 )
		return false;

	return domain->SetVoxelCount(vox, true);
}

b8 DomainCartesianEditor::SetVoxelPitch(const VX::Math::vec3& pitch)
{
	VX::SG::DomainCartesian* domain = static_cast<VX::SG::DomainCartesian*>(GetActiveDomain());
	if(!domain) return false;

	if( pitch[0] <= 0.0f || pitch[1] <= 0.0f || pitch[2] <= 0.0f )
		return false;

	VX::Math::vec3 curMin, curMax, curSize;
	GetBBoxMinMaxSize(curMin, curMax, curSize);
	
	VX::Math::idx3 vox;
	for(u32 i = 0; i < 3; i++){
		vox[i]     = static_cast<s32>(ceil(curSize[i] / pitch[i]));
		curSize[i] = static_cast<f32>(vox[i]) * pitch[i];
	}
	
	if( !SetBBoxMinMax(curMin, (curMin + curSize)) )
		return false;
	
	return SetVoxelCount(vox);
}

b8 DomainCartesianEditor::SetDivCount( const VX::Math::idx3& div )
{		
	VX::SG::DomainCartesian* domain = static_cast<VX::SG::DomainCartesian*>(GetActiveDomain());
	if(!domain) return false;

	VX::Math::idx3 vox;
	if( !domain->GetVoxelCount(vox) )
		return false;
		
	if( vox[0] < div.x || vox[1] < div.y || vox[2] < div.z )
		return false;
	
	if( div.x < 1 || div.y < 1 || div.z < 1 )
		return false;

	return domain->SetDivCount(div, true);
}

b8 DomainCartesianEditor::SetDivCount( const u32 divCount ){

	VX::SG::DomainCartesian* domain = static_cast<VX::SG::DomainCartesian*>(GetActiveDomain());
	if(!domain) return false;

	if( divCount <= 0 )
		return false;
	
	VX::Math::idx3 vox;
	if( !domain->GetVoxelCount(vox) )
		return false;
	
	u32 _div[3];
	u32 _vox[3];
	_vox[0] = static_cast<u32>(vox[0]);
	_vox[1] = static_cast<u32>(vox[1]);
	_vox[2] = static_cast<u32>(vox[2]);
	if( !DecideDivPattern( divCount, _vox, _div) )
		return false;
	
	VX::Math::idx3 div(static_cast<s32>(_div[0]), static_cast<s32>(_div[1]), static_cast<s32>(_div[2]));
	return SetDivCount(div);
}



b8 DomainCartesianEditor::hasVoxel()
{
	VX::SG::DomainCartesian* domain = static_cast<VX::SG::DomainCartesian*>(GetActiveDomain());
	if(!domain) return false;

	VX::Math::idx3 vox;
	domain->GetVoxelCount(vox);

	if( vox[0] == 1 && vox[1] == 1 && vox[2] == 1 ){
		return false;
	}

	return true;
}

b8 DomainCartesianEditor::hasDivision()
{
	VX::SG::DomainCartesian* domain = static_cast<VX::SG::DomainCartesian*>(GetActiveDomain());
	if(!domain) return false;

	VX::Math::idx3 div;
	domain->GetDivCount(div);

	if( div[0] == 1 && div[1] == 1 && div[2] == 1 ){
		return false;
	}

	return true;
}

b8 DomainCartesianEditor::SetUnit(const std::string& unit )
{
	VX::SG::DomainCartesian* domain = static_cast<VX::SG::DomainCartesian*>(GetActiveDomain());
	if(!domain) return false;

	return domain->SetUnit(unit);
}

b8 DomainCartesianEditor::SetExportPolicy(const u32 policy )
{
	VX::SG::DomainCartesian* domain = static_cast<VX::SG::DomainCartesian*>(GetActiveDomain());
	if(!domain) return false;

	return domain->SetExportPolicy(policy == 0 ? VX::SG::DomainCartesian::EXP_VOXEL_PITCH : VX::SG::DomainCartesian::EXP_VOXEL_COUNT);
}

s32 DomainCartesianEditor::GetExportPolicy() const
{
	const VX::SG::DomainCartesian* domain = static_cast<const VX::SG::DomainCartesian*>(GetActiveDomain());
	if(!domain) return -1;
	
	return (domain->GetExportPolicy() == VX::SG::DomainCartesian::EXP_VOXEL_PITCH ? 0 : 1);
}

u8 DomainCartesianEditor::GetBBoxMinMax(VX::Math::vec3& min, VX::Math::vec3& max) const
{
	const VX::SG::DomainCartesian* domain = static_cast<const VX::SG::DomainCartesian*>(GetActiveDomain());
	if(!domain) return false;

	return domain->GetBBoxMinMax(min, max);
}

u8 DomainCartesianEditor::GetBBoxMinMaxSize(VX::Math::vec3& min, VX::Math::vec3& max, VX::Math::vec3& size) const
{
	if( !GetBBoxMinMax(min, max) )
		return false;
	
	size = max - min;

	return true;
}

u8 DomainCartesianEditor::GetVoxelCount(VX::Math::idx3& vox) const
{
	const VX::SG::DomainCartesian* domain = static_cast<const VX::SG::DomainCartesian*>(GetActiveDomain());
	if(!domain) return false;
	
	return domain->GetVoxelCount(vox);
}

u8 DomainCartesianEditor::GetVoxelPitch(VX::Math::vec3& pitch) const
{
	VX::Math::idx3 vox;
	if( !GetVoxelCount(vox) )
		return false;
	
	VX::Math::vec3 bboxMin, bboxMax, bboxSize;
	if( !GetBBoxMinMaxSize(bboxMin, bboxMax, bboxSize) )
		return false;
	
	pitch[0] = bboxSize[0] / static_cast<f32>(vox[0]);
	pitch[1] = bboxSize[1] / static_cast<f32>(vox[1]);
	pitch[2] = bboxSize[2] / static_cast<f32>(vox[2]);

	return true;
}

u8 DomainCartesianEditor::GetDivCount(VX::Math::idx3& div) const
{
	const VX::SG::DomainCartesian* domain = static_cast<const VX::SG::DomainCartesian*>(GetActiveDomain());
	if(!domain) return false;

	return domain->GetDivCount(div);
}

const std::string& DomainCartesianEditor::GetUnit() const
{
	const VX::SG::DomainCartesian* domain = static_cast<const VX::SG::DomainCartesian*>(GetActiveDomain());
	return domain->GetUnit();
}

const u8* DomainCartesianEditor::GetSubdomainPtr() const 
{
	const VX::SG::DomainCartesian* domain = dynamic_cast<const VX::SG::DomainCartesian*>(GetActiveDomain());
	if(!domain) return NULL;
	
	return domain->GetSubdomain();
}


b8 DomainCartesianEditor::SetSubdomain(const u8 *subdomain)
{
	VX::SG::DomainCartesian* domain = dynamic_cast<VX::SG::DomainCartesian*>(GetActiveDomain());
	return domain->SetSubdomain(subdomain, true);
}


void DomainCartesianEditor::ResetClipping()
{
	VX::SG::DomainCartesian* domain = dynamic_cast<VX::SG::DomainCartesian*>(GetActiveDomain());
	domain->ResetClipping();
}


