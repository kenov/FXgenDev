#include "../VX/VX.h"
#include "../VX/SG/SceneGraph.h"
#include "SceneEditor.h"

#include "../VX/SGPicker.h"

#include <set>

namespace  {
	class Rebuilder
	{
	public:
		Rebuilder(){}
		void operator()(VX::SG::Geometry* g) const
		{
			g->CalcBounds();
			VX::SG::GeometryBVH* gbvh = static_cast<VX::SG::GeometryBVH*>(g);
			gbvh->Build();
		}
		
	};
	
	class DisableUpdateFlagVisitor
	{
	public:
		DisableUpdateFlagVisitor(){}
		void operator()(VX::SG::Geometry* g) const
		{
			g->DisableNeedUpdate();
		}
		
	};
	/*
	class DeleteSelectionsVisitor
	{
	public:
		DeleteSelectionsVisitor(){}
		void operator()(VX::SG::Geometry* node) const
		{
			using namespace VX::SG;
			GeometryBVH* geo = static_cast<GeometryBVH*>(node);
			
			u32 inum = geo->GetIndexCount();
			VX::SG::Index* index = geo->GetIndex();
			const VX::SG::Geometry::VertexFormat* v =geo->GetVertex();
			
			// shrink index
			u32 indexCount = 0;
			for (u32 i = 0; i < inum; i+=3)
			{
				const b8 mark = 
				((v[index[i    ]].col & 0xFF) > 0)
				&&  ((v[index[i + 1]].col & 0xFF) > 0)
				&&  ((v[index[i + 2]].col & 0xFF) > 0);
				
				if (!mark)
				{
					if (i != indexCount)
					{
						index[indexCount    ] = index[i    ];
						index[indexCount + 1] = index[i + 1];
						index[indexCount + 2] = index[i + 2];
					}
					indexCount += 3;
				}
				
			}
			
			printf("oldIndexCount = %d newIndexCount = %d\n", inum/3, indexCount/3);
			if (indexCount != inum) // deleted
			{
				geo->MinimizeIndex(indexCount);
				
				geo->CalcBounds();
				geo->Build();
				geo->EnableNeedUpdate();
			}
		}
	};*/
	
	class ClearSelectionVistor
	{
	public:
		ClearSelectionVistor(){}
		void operator()(VX::SG::Node* node) const
		{
			node->SetSelection(false);
		}
	};
	/*
	class MoveSelectionVisitor
	{
	public:
		MoveSelectionVisitor(const VX::Math::matrix& mat)
		{
			mv = mat;
		}
		void operator()(VX::SG::Geometry* g) const
		{
			using namespace VX::Math;
			b8 needupdate = false;
			VX::SG::Geometry::VertexFormat* v = g->GetVertex();
			const u32 n = g->GetVertexCount();
			for (u32 i = 0; i < n; ++i)
			{
				bool mark = ((v[i].col & 0xFF) > 0 );
				if (mark) {
					if (!needupdate)
						needupdate = true;
				
					const vec4 p = mv * vec4(v[i].pos,1.0f);
					v[i].pos = p.xyz();
				}
			}
			if (needupdate)
				g->EnableNeedUpdate();
		}
	private:
		VX::Math::matrix mv;
	};*/

	/*
	class SelectionCollector
	{
	public:
		SelectionCollector()
		{
			m_step = 0;
			m_node = 0;
			m_indexCount = 0;
			m_vertexCount = 0;
		}
		~SelectionCollector()
		{
		}
		void CreateNewNode()
		{
			assert(m_step == 0);
			m_node = vxnew VX::SG::GeometryBVH();
			m_node->Alloc(m_indexCount, m_indexCount); // TODO(vertex)
			m_node->SetName("new_node");
			m_vertexCount = 0;
			m_indexCount  = 0;
			++m_step;
		}
		VX::SG::Node* BuildNode()
		{
			if (!m_node)
				return 0;
			
			m_node->CalcBounds();
			m_node->Build();
			return m_node;
		}
		 
		void operator()(const VX::SG::Geometry* geo)
		{
			using namespace VX::Math;
			if (!m_step) // count part
			{
				//std::vector<u32> temp;
				u32 inum = geo->GetIndexCount();
				const VX::SG::Index* index = geo->GetIndex();
				const VX::SG::Geometry::VertexFormat* v = geo->GetVertex();
				for (u32 i = 0; i < inum; i+=3)
				{
					const b8 mark = 
					    ((v[index[i    ]].col & 0xFF) > 0)
					&&  ((v[index[i + 1]].col & 0xFF) > 0)
					&&  ((v[index[i + 2]].col & 0xFF) > 0);
					
					if (mark) {
						//temp.push_back(index[i]);
						//temp.push_back(index[i+1]);
						//temp.push_back(index[i+2]);
						m_indexCount += 3;
					}
				}
				//m_vertexCount += temp.size();
			}
			else // copy part
			{
				assert(m_node != 0);
				VX::SG::Index* new_index = m_node->GetIndex();
				VX::SG::Geometry::VertexFormat* new_v = m_node->GetVertex();
				u32 inum = geo->GetIndexCount();
				const VX::SG::Index* index = geo->GetIndex();
				const VX::SG::Geometry::VertexFormat* v = geo->GetVertex();
				for (u32 i = 0; i < inum; i+=3)
				{
					const b8 mark = 
					((v[index[i    ]].col & 0xFF) > 0)
					&&  ((v[index[i + 1]].col & 0xFF) > 0)
					&&  ((v[index[i + 2]].col & 0xFF) > 0);
					
					if (mark) {
						new_v[m_vertexCount  ] = v[index[i  ]];
						new_v[m_vertexCount+1] = v[index[i+1]];
						new_v[m_vertexCount+2] = v[index[i+2]];
						new_v[m_vertexCount  ].col &= 0xFFFFFF00;
						new_v[m_vertexCount+1].col &= 0xFFFFFF00;
						new_v[m_vertexCount+2].col &= 0xFFFFFF00;
					    new_index[m_indexCount  ] = m_vertexCount    ;
						new_index[m_indexCount+1] = m_vertexCount + 1;
						new_index[m_indexCount+2] = m_vertexCount + 2;
						m_indexCount += 3;
						m_vertexCount += 3;
					}
				}
			}
		}

	private:

		u32 m_indexCount;
		u32 m_vertexCount;
		s32 m_step;
		VX::SG::GeometryBVH* m_node;
	};
	*/
	
	class IDSetSelectionVisitor
	{
	public:
		IDSetSelectionVisitor(u32 id)
		{
			m_id = id;
		}
		void operator()(VX::SG::Geometry* g) const
		{
			using namespace VX::Math;
			b8 needupdate = false;
			u32* idb = g->GetIDBuffer();
			const u32 n = g->GetIndexCount()/3;
			for (u32 i = 0; i < n; ++i)
			{
				const u32 idx = i;
				bool mark = ((idb[idx] & 0xFF) > 0 );
				if (mark) {
					if (!needupdate)
						needupdate = true;
					
					idb[idx] &= 0xFFFF00FF;
					idb[idx] |= (m_id<<8);
				}
			}
			if (needupdate)
				g->EnableNeedUpdate();
		}
	private:
		u32 m_id;
	};
} // namespace

SceneEditor::SceneEditor()
{
	m_view = VX::Math::Identity();
	m_proj = VX::Math::Identity();
	m_sg = 0;
	m_picker = vxnew VX::SGPicker();
	m_selmode = MODE_GEOMETRY;
}
 
SceneEditor::~SceneEditor()
{
	vxdelete(m_picker);
}

void SceneEditor::Resize(u32 w, u32 h)
{
	m_width  = w;
	m_height = h;
	m_picker->Resize(w, h);
}

void SceneEditor::SetViewMat(const VX::Math::matrix4x4& mat)
{
	m_view = mat;
}

void SceneEditor::SetProjMat(const VX::Math::matrix4x4& mat)
{
	m_proj = mat;
}

void SceneEditor::SetSceneGraph(VX::SG::Node* node)
{
	m_sg = node;
}

void SceneEditor::Pick(const s32 win_x, const s32 win_y, b8 selection)
{
	m_picker->SetProjMat(m_proj);
	m_picker->SetViewMat(m_view);
	VX::SGPicker::TYPE_TRAVERSAL type = (m_selmode == MODE_GEOMETRY ? VX::SGPicker::TYPE_GEOMETRY : VX::SGPicker::TYPE_DOMAIN);
	m_picker->Pick(m_sg, static_cast<f32>(win_x), static_cast<f32>(win_y), selection, type);
}

const VX::SGPickerElm& SceneEditor::GetPickInformation() const
{
	return m_picker->GetPickInformation();
}

void SceneEditor::EndPick(const s32 win_x, const s32 win_y)
{
	m_picker->EndPick(m_sg, static_cast<f32>(win_x), static_cast<f32>(win_y));
}

void SceneEditor::Pick(const s32 win_sx, const s32 win_sy, const s32 win_ex, const s32 win_ey, b8 selection)
{
	m_picker->SetProjMat(m_proj);
	m_picker->SetViewMat(m_view);
	VX::SGPicker::TYPE_TRAVERSAL type = (m_selmode == MODE_GEOMETRY ? VX::SGPicker::TYPE_GEOMETRY : VX::SGPicker::TYPE_DOMAIN);
	m_picker->Pick(m_sg, static_cast<f32>(win_sx), static_cast<f32>(win_sy), static_cast<f32>(win_ex), static_cast<f32>(win_ey), selection, type);
}

void SceneEditor::DeleteSelections()
{
	assert(0);
//	DeleteSelectionsVisitor deleter;
//	VX::SG::VisitAllGeometry(m_sg, deleter);
}

void SceneEditor::DisableAllGeometryUpdated()
{
	DisableUpdateFlagVisitor disable;
	VX::SG::VisitAllGeometry(m_sg, disable, false);
}

void SceneEditor::ClearSelections()
{
	ClearSelectionVistor clear;
	VX::SG::VisitAllNode(m_sg, clear);
}

void SceneEditor::SelectionMove(const VX::Math::matrix& mat)
{
	assert(0);
	//MoveSelectionVisitor mover(mat);
	//VX::SG::VisitAllGeometry(m_sg, mover);
}

void SceneEditor::SelectionSetID(u32 id)
{
	IDSetSelectionVisitor setid(id);
	VX::SG::VisitAllGeometry(m_sg, setid);
}

void SceneEditor::SelectionNew()
{
	if (!m_sg)
		return;
	
	assert(0);
/*	SelectionCollector collector;
	VX::SG::VisitAllGeometry(m_sg, collector); // step1
	collector.CreateNewNode();
	VX::SG::VisitAllGeometry(m_sg, collector); // step2
	VX::SG::Node* node = collector.BuildNode();
	VX::SG::Group* g = static_cast<VX::SG::Group*>(m_sg);
	g->AddChild(node);
*/	
}

void SceneEditor::SelectionMode(SELECTMODE mode)
{
	m_selmode = mode;
}


void SceneEditor::Rebuild()
{
	Rebuilder rebuilder;
	VX::SG::VisitAllGeometry(m_sg, rebuilder);
}


