/*
 *
 * SceneEditor.h
 * 
 */

#ifndef __VXGEN_SCENE_EDITOR_H__
#define __VXGEN_SCENE_EDITOR_H__

#include "../VX/Type.h"
#include "../VX/Math.h"

namespace VX {
	class SGPicker;
	class SGPickerElm;
	namespace SG {
		class Node;
	}
}

class SceneEditor
{
public:
	SceneEditor();
	~SceneEditor();
	
	void Resize(u32 w, u32 h);
	
	void SetViewMat(const VX::Math::matrix4x4& mat);
	void SetProjMat(const VX::Math::matrix4x4& mat);
	
	void SetSceneGraph(VX::SG::Node* node);
	
	void Pick(const s32 win_x, const s32 win_y, b8 selection);
	void Pick(const s32 win_sx, const s32 win_sy, const s32 win_ex, const s32 win_ey, b8 selection = true);
	void EndPick(const s32 win_x, const s32 win_y);

	const VX::SGPickerElm& GetPickInformation() const;


	void DisableAllGeometryUpdated();
	void DeleteSelections();
	void ClearSelections();
	void SelectionNew();
	void SelectionMove(const VX::Math::matrix& mat);
	void SelectionSetID(u32 id);

	enum SELECTMODE
	{
		MODE_GEOMETRY,
		MODE_DOMAIN
	};

	void Rebuild();
	
	void SelectionMode(SELECTMODE mode);
	SELECTMODE GetSelectionMode() const { return m_selmode; };

private:
	VX::SGPicker* m_picker;
	VX::SG::Node* m_sg;

	u32 m_width, m_height;
	
	VX::Math::matrix4x4 m_view;
	VX::Math::matrix4x4 m_proj;
	
	SELECTMODE m_selmode;

};

#endif // __VXGEN_SCENE_EDITOR_H__

