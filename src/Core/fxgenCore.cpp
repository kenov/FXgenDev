#include "../VX/VX.h"
#include "../VX/Graphics.h"
#include "../VX/Math.h"
#include "../VX/Time.h"
#include "../VX/Stream.h"
#include "../VX/Dir.h"
#include "../VX/SGPickerHelper.h"
#include "../VX/SGPicker.h"

#include "../FileIO/FileIOUtil.h"
#include "../FileIO/STLAsciiLoader.h"
#include "../FileIO/STLBinaryLoader.h"
#include "../FileIO/ObjLoader.h"
#include "../FileIO/STLAsciiSaver.h"
#include "../FileIO/STLBinarySaver.h"
#include "../FileIO/ObjSaver.h"
#include "../FileIO/AllParameterSaver.h"
#include "../FileIO/BCMediumListLoader.h"
#include "../FileIO/BCMediumListSaver.h"
#include "../FileIO/BCClassLoader.h"
#include "../FileIO/BCClassSaver.h"
#include "../FileIO/GuiSettingsLoader.h"
#include "../FileIO/DomainCartesianLoader.h"
#include "../FileIO/DomainCartesianSaver.h"
#include "../FileIO/DomainBCMLoader.h"
#include "../FileIO/DomainBCMSaver.h"
#include "../FileIO/GeometryListSaver.h"
#include "../FileIO/GeometryListLoader.h"



#include "../FileIO/FaceBCLoader.h"
#include "../FileIO/FaceBCSaver.h"
#include "../FileIO/MeshCartesianLoader.h"
#include "../FileIO/VoxelCartesianLoader.h"
#include "../FileIO/MeshBCMLoader.h"
#include "../FileIO/PolylibFileSaver.h"

#include "../Render/RectRender.h"
#include "../Render/AxisRender.h"
#include "../Render/SceneRender.h"
#include "../VX/SG/SceneGraph.h"
#include "DomainCartesianEditor.h"
#include "DomainBCMEditor.h"
#include "VoxelCartesianEditor.h"
#include "fxgenCore.h"
#include "SceneEditor.h"
#include "SliceController.h"

#include "../VX/SG/CoordinateViewer.h"

#ifdef _WIN32
#include <windows.h>
#include <stdlib.h>
#include <string.h>
#include <tchar.h>
#include <string>
#elif MACOS

#else
#include <stdlib.h>
#include <limits.h>
#include <unistd.h>
#endif

#include <stdio.h>

void debugStr(const char* str){
#ifdef _WIN32
	OutputDebugStringA(str);
	OutputDebugStringA("\n");
#else
	printf("%s\n", str);
#endif
}

#define BC_CLASS_FILENAME "BC_Class.tp"
#define GUI_SETTING_FILENAME "GUI_Setting.tp"

s32 VX::SG::Texture::MAX_SIZE = 8192;

namespace  {

	inline s32 skipSpace(const s8* a)
    {
        s32 i = 0;
        while(a[i] == 0x20 || a[i] == 0x09) ++i; // SPACE, TAB
        return i;
    }
	

	class ExistChecker {
	public:
		ExistChecker() : exist(false) { }
		void operator() (const VX::SG::Node* node)
		{
			VX::SG::NODETYPE t = node->GetType();
			if( t == VX::SG::NODETYPE_GEOMETRY ||
			    t == VX::SG::NODETYPE_BBOX_DOMAIN || 
				t == VX::SG::NODETYPE_BBOX_DOMAIN_CARTESIAN || 
			    t == VX::SG::NODETYPE_BBOX_DOMAIN_BCM ||
				t == VX::SG::NODETYPE_MESH_CARTESIAN ||
				t == VX::SG::NODETYPE_MESH_CARTESIAN_SLICE ||
				t == VX::SG::NODETYPE_VOXEL_CARTESIAN_G ||
				t == VX::SG::NODETYPE_VOXEL_CARTESIAN_SLICE_G ||
				t == VX::SG::NODETYPE_VOXEL_CARTESIAN_L ||
				t == VX::SG::NODETYPE_VOXEL_CARTESIAN_SLICE_L ||
				t == VX::SG::NODETYPE_VOXEL_CARTESIAN_C ||
				t == VX::SG::NODETYPE_VOXEL_CARTESIAN_SLICE_C ||
				t == VX::SG::NODETYPE_VOXEL_CARTESIAN_BCFLAG || 
				t == VX::SG::NODETYPE_MESH_BCM ||
				t == VX::SG::NODETYPE_MESH_BCM_SLICE
				 )
			{
				 exist = true;
	 		}
		}
		b8 exist;
	};

	class GeometryChecker {
	public:
		GeometryChecker() : exist(false) { }
		void operator() (const VX::SG::Node* node)
		{
			VX::SG::NODETYPE t = node->GetType();
			if( t == VX::SG::NODETYPE_GEOMETRY )
			{
				 exist = true;
	 		}
		}
		b8 exist;
	};
	

	b8 isSTLBinaryFile(const unsigned char* buf, int maxlen)
	{
		const char* stlascii_header = "solid";
		
		//space char escape
		const char* data = reinterpret_cast<const char*>(buf) ;
		int i = skipSpace(data);
		
		if (strncmp( &data[i], stlascii_header, strlen(stlascii_header)) == 0) {
			const s32 testlen = maxlen/4; // test 1/4 size.
			for (s32 i = 0; i < testlen; ++i) {
				if (buf[i] >= 0x7F || buf[i] == 0x00)
					return true;
			}
			return false; // Ascii
		}
		return true;
	}
	
	VX::SG::Node* loadModel(const s8* filename)
	{
		std::string fname = convertPath(filename);
		std::string ext = getExt(fname);
		
		using namespace VX::SG;
		Node* model = 0;
		VX::Stream* st = 0;
		if (ext == ".STL")
		{
			st = vxnew VX::Stream(fname.c_str(), VX::Stream::MODE_INPUT_BINARY_ONMEMORY);
			if (isSTLBinaryFile(static_cast<const unsigned char*>(st->GetData()), st->GetSize()))
			{
				VXLogD("STL_Binary\n");
				STLBinaryLoader loader;
				model = loader.Load(st);
			}
			else
			{
				VXLogD("STL_Ascii\n");
				STLAsciiLoader loader;
				model = loader.Load(st);
			}
		}
		else if (ext == ".SLB" || ext == ".STLB")
		{
			st = vxnew VX::Stream(fname.c_str(), VX::Stream::MODE_INPUT_BINARY_ONMEMORY);
			VXLogD("STL_Binary\n");
			STLBinaryLoader loader;
			model = loader.Load(st);
		}
		else if (ext == ".SLA" || ext == ".STLA")
		{
			st = vxnew VX::Stream(fname.c_str(), VX::Stream::MODE_INPUT_BINARY_ONMEMORY);
			VXLogD("STL_Ascii\n");
			STLAsciiLoader loader;
			model = loader.Load(st);
		}
		else if (ext == ".OBJ")
		{
			VXLogD("Obj\n");
			st = vxnew VX::Stream(fname.c_str(), VX::Stream::MODE_INPUT_BINARY_ONMEMORY);
			ObjLoader loader;
			model = loader.Load(st);
		}
		else
		{
			return 0;
		}
		if (model)
			static_cast<GeometryBVH*>(model)->Build();// Build BVH
		
		vxdelete(st);
		return model;
	}
	
	VX::SG::Node* loadDir(const s8* dirname)
	{
		VX::Dir dir(dirname);
		if (!dir.IsOpened())
			return 0;
		
		VXLogD("loadDir=%s\n",dirname);
		
		using namespace VX::SG;
		Transform* trs = vxnew Transform();
		trs->SetName(std::string(dir.GetName()));
		
		u32 n = dir.GetFileCount();
		for (u32 i = 0; i < n; ++i)
		{
			const std::string& fname = dir.GetFile(i);
			Node* model = loadModel(dir.GetFilePath(fname.c_str()).c_str());
			if (model)
				trs->AddChild(model);
		}
		n = dir.GetDirCount();
		for (u32 i = 0; i < n; ++i)
		{
			Node* model = loadDir(dir.GetFilePath(dir.GetDir(i).c_str()).c_str());
			if (model)
				trs->AddChild(model);
		}
		return trs;
	}
	
	b8 saveModel(const s8* filename, const s8* mode, const VX::SG::Node* node, f32 scale = 1, b8 visibleOnly = true)
	{
		VXLogD("saveModel=%s\n",filename);
		std::string fname = convertPath(filename);
		std::string ext = getExt(fname);
		if (std::string(mode) == "STLa" && ext != ".STL")
			fname += ".stl";
		else if (std::string(mode) == "STLb" && ext != ".STL")
			fname += ".stl";
		else if (std::string(mode) == "OBJ" && ext != ".OBJ")
			fname += ".obj";
		
		
		b8 ret = false;
		VX::Stream* st = vxnew VX::Stream(fname.c_str(), VX::Stream::MODE_OUTPUT_BINARY);
		if (std::string(mode) == "STLa")
		{
			STLAsciiSaver saver;
			ret = saver.Save(st, node, scale, visibleOnly);
		}
		else if (std::string(mode) == "STLb")
		{
			STLBinarySaver saver;
			ret = saver.Save(st, node, scale, visibleOnly);		
		}
		else if (std::string(mode) == "OBJ")
		{
			ObjSaver saver;
			ret = saver.Save(st, node);
		}
		vxdelete(st);
		return ret;
	}
	
	
	b8 saveDir(const s8* dirname, const s8* mode, const VX::SG::Node* node, f32 modelScale = 1, b8 visibleOnly = true)
	{
		if (!node)
			return false;
		if (node->GetIntermediate())
			return true;
		
		VXLogD("saveDir=%s\n",dirname);
		
		using namespace VX::SG;
		
		VX::Dir dir(dirname);
		
		const NODETYPE type = node->GetType();
		if (type == NODETYPE_GEOMETRY) {
			std::string modelname = node->GetName();
			if (modelname == "")
				modelname = "noname";
			std::string modelpath = dir.GetFilePath(modelname.c_str());
			if (!visibleOnly || node->GetVisible()) {
				if(!saveModel(modelpath.c_str(), mode, node, modelScale, visibleOnly))
					return false;
			}
		}
		else if (type == NODETYPE_GROUP || type == NODETYPE_TRANSFORM)
		{
			if (visibleOnly && !node->GetVisible())
				return false;
			std::string name = node->GetName();
			if (name == "")
				name = "noname";
			if (!dir.CreateDir(name.c_str()))
				return false;
			
			const Group* g = static_cast<const Group*>(node);
			const std::string& npath = dir.GetFilePath(name.c_str());
			u32 n = g->GetChildCount();
			for (u32 i = 0; i < n; ++i)
			{
				const Node* child = g->GetChild(i);
				if (!visibleOnly || child->GetVisible()) {
					if (!saveDir(npath.c_str(), mode, child, modelScale, visibleOnly))	
						return false;
				}
			}
		}
		return true;
	}

	void CreateDefaultBCClass(VX::SG::Group* setting)
	{
		using namespace VX::SG;
		VX::SG::Node* bcclass;
		// Create Outer BC Class
		bcclass = vxnew OuterBCClass("Wall"                        ); setting->AddChild(bcclass); 
		bcclass = vxnew OuterBCClass("Wall", "Adiabatic"           ); setting->AddChild(bcclass);
		bcclass = vxnew OuterBCClass("Wall", "HeatTransfer"        ); setting->AddChild(bcclass);
		bcclass = vxnew OuterBCClass("Wall", "HeatFlux"            ); setting->AddChild(bcclass);
		bcclass = vxnew OuterBCClass("Wall", "IsoThermal"          ); setting->AddChild(bcclass);
		bcclass = vxnew OuterBCClass("Wall", "ConstantTemperature" ); setting->AddChild(bcclass);
		bcclass = vxnew OuterBCClass("Outflow"                     ); setting->AddChild(bcclass);
		bcclass = vxnew OuterBCClass("SpecifiedVelocity"           ); setting->AddChild(bcclass);
		bcclass = vxnew OuterBCClass("Symmetric"                   ); setting->AddChild(bcclass);
		bcclass = vxnew OuterBCClass("Periodic"                    ); setting->AddChild(bcclass);
		bcclass = vxnew OuterBCClass("TractionFree"                ); setting->AddChild(bcclass);
		bcclass = vxnew OuterBCClass("FarField"                    ); setting->AddChild(bcclass);
		// Create Local BC Class
		bcclass = vxnew LocalBCClass("SpecifiedVelocity"           ); setting->AddChild(bcclass); 
		bcclass = vxnew LocalBCClass("Outflow"                     ); setting->AddChild(bcclass); 
		bcclass = vxnew LocalBCClass("Periodic"                    ); setting->AddChild(bcclass); 
		bcclass = vxnew LocalBCClass("Forcing"                     ); setting->AddChild(bcclass); 
		bcclass = vxnew LocalBCClass("PressureLoss"                ); setting->AddChild(bcclass); 
		bcclass = vxnew LocalBCClass("Fan"                         ); setting->AddChild(bcclass); 
		bcclass = vxnew LocalBCClass("Darcy"                       ); setting->AddChild(bcclass); 
		bcclass = vxnew LocalBCClass("CellMonitor"                 ); setting->AddChild(bcclass); 
		bcclass = vxnew LocalBCClass("Inactive"                    ); setting->AddChild(bcclass); 
		bcclass = vxnew LocalBCClass("Adiabatic"                   ); setting->AddChild(bcclass); 
		bcclass = vxnew LocalBCClass("DirectHeatFlux"              ); setting->AddChild(bcclass); 
		bcclass = vxnew LocalBCClass("HeatTransferB"               ); setting->AddChild(bcclass); 
		bcclass = vxnew LocalBCClass("HeatTransferN"               ); setting->AddChild(bcclass); 
		bcclass = vxnew LocalBCClass("HeatTransferS"               ); setting->AddChild(bcclass); 
		bcclass = vxnew LocalBCClass("HeatTransferSF"              ); setting->AddChild(bcclass); 
		bcclass = vxnew LocalBCClass("HeatTransferSN"              ); setting->AddChild(bcclass); 
		bcclass = vxnew LocalBCClass("IsoThermal"                  ); setting->AddChild(bcclass); 
		bcclass = vxnew LocalBCClass("Radiation"                   ); setting->AddChild(bcclass); 
		bcclass = vxnew LocalBCClass("HeatSource"                  ); setting->AddChild(bcclass); 
		bcclass = vxnew LocalBCClass("SpecifiedTemprature"         ); setting->AddChild(bcclass); 
		bcclass = vxnew LocalBCClass("Obstacle"					   ); setting->AddChild(bcclass); 
	}

	void CreateDefaultGuiSetting(VX::SG::Group* setting)
	{

		using namespace VX::SG;
		VX::SG::GUISettings* guisettings = vxnew GUISettings(); 
		setting->AddChild(guisettings);
		// default is only red color
		VX::Math::vec4 e;
		e.r=1.0f;
		e.g=0.0f;
		e.b=0.0f;
		e.a=1.0f;

		guisettings->Append(e);
	}


	std::string GetPath(const char* fileName)
	{
#if _WIN32 // Windows
		TCHAR szFull[_MAX_PATH];
		TCHAR szAppDir[_MAX_PATH];
		TCHAR szDrive[_MAX_PATH];
		TCHAR szDir[_MAX_DIR];

		::GetModuleFileName(NULL, szFull, sizeof(szFull)/sizeof(TCHAR));
		_tsplitpath_s(szFull, szDrive, _MAX_PATH, szDir, _MAX_DIR, NULL, 0, NULL, 0);
		_tmakepath_s(szAppDir, _MAX_PATH, szDrive, szDir, NULL, NULL);

		std::string path;
#ifdef UNICODE
		{
			std::wstring wpath(szAppDir);
			char *mbs = new char[wpath.length() * MB_CUR_MAX + 1];
			wcstombs(mbs, wpath.c_str(), wpath.length() * MB_CUR_MAX + 1);
			path = mbs;
			delete mbs;
		}
#else
		path = std::string(szAppDir);
#endif
		//return std::string(fileName);
		//return convertPath(path) + "/" + std::string(fileName);
		return convertPath(path) + std::string(fileName);
#elif MACOS // Mac OS X
		std::string path("FXgen.app/Contents/Resources/");
		return path + std::string(fileName);

#else // Linux
		char cBuf[PATH_MAX + 1];
		char cFull[PATH_MAX + 1];

		readlink("/proc/self/exe", cBuf, PATH_MAX);
		realpath(cBuf, cFull);

		std::string full(cFull);
		std::string::size_type index = full.rfind("/");
		if(index == std::string::npos){
			return std::string(fileName);
		}else{
			std::string path(full.substr(0, index));
			return path + "/" + std::string(fileName);
		}
#endif
	}

	std::string GetBCClassPath()
	{
		return GetPath(BC_CLASS_FILENAME);
	}

	std::string GetGuiSettingPath()
	{
		return GetPath(GUI_SETTING_FILENAME);
	}
	
} // namespace

fxgenCore::fxgenCore()
{
	g = vxnew VX::Graphics();
	// Get Max Texture Size
	g->GetIntegerv(VG_MAX_TEXTURE_SIZE, &(VX::SG::Texture::MAX_SIZE));
	VXLogD("MAX_TEXTURE_SIZE : %d\n", VX::SG::Texture::MAX_SIZE);

	m_modelRender = vxnew SceneRender(g);
	m_root = 0;
	
	m_view = VX::Math::Identity();
	m_proj = VX::Math::Identity();

	m_domainEditor		= NULL;
	m_VoxelCartesianEditor	= NULL;

	m_sliceController		= NULL;

	m_sliceController_G		= NULL;
	m_sliceController_L		= NULL;
	m_sliceController_C		= NULL;

	m_editor = vxnew SceneEditor();
	m_rectRender = vxnew RectRender(g);
	m_axisRender = vxnew AxisRender(g);
	m_showaxis = true;
	
	m_setting = vxnew VX::SG::Group();
	m_setting->SetName("setting");
	m_setting->SetIntermediate(true);
	m_cross = vxnew VX::SG::Cross();
	m_cross->SetVisible(false);
	m_setting->AddChild(m_cross);
	
	//デフォルトの０番目の色、グレー STL表示で使用
	const VX::Math::vec4 defColor(120/255.0f,120/255.0f,120/255.0f, 1.0f);

	m_defaultIDColor = defColor;
	m_modelRender->SetIDColorTable(0, m_defaultIDColor);

	m_backColor = VX::Math::vec4(0,0,0,1);

	std::string filepath;
	filepath = GetBCClassPath();

	FILE* fptest = fopen(filepath.c_str(), "r");
	if(fptest == NULL){
		VXLogW("BCClass file [%s] is not found, so create default BCClass.\n", filepath.c_str());
		CreateDefaultBCClass(m_setting);
	}else{
		fclose(fptest);
		BCClassLoader loader;
		if(!loader.Load(filepath.c_str(), m_setting)){
			VXLogE("load BCClass file error [%s].\n", filepath.c_str());
			for(int i = m_setting->GetChildCount()-1; i >= 0; --i){
				VX::SG::NODETYPE type;
				type = m_setting->GetChild(i)->GetType();
				if( type == VX::SG::NODETYPE_OUTERBCCLASS || type == VX::SG::NODETYPE_LOCALBCCLASS ){
					m_setting->RemoveChild(m_setting->GetChild(i));
				}
			}
			CreateDefaultBCClass(m_setting);
		}
	}

	//gui settings
	filepath = GetGuiSettingPath();
	fptest = fopen(filepath.c_str(), "r");
	if(fptest == NULL){
		VXLogW("GUISetting file [%s] is not found, so create default GUISetting.\n", filepath.c_str());
		CreateDefaultGuiSetting(m_setting);
	}else{
		fclose(fptest);
		GuiSettingsLoader loader;
		if(!loader.Load(filepath.c_str(), m_setting)){
			VXLogE("load GuiSetting file error [%s].\n", filepath.c_str());
			for(int i = m_setting->GetChildCount()-1; i >= 0; --i){
				VX::SG::NODETYPE type;
				type = m_setting->GetChild(i)->GetType();
				if( type == VX::SG::NODETYPE_GUI_SETTINGS ){
					m_setting->RemoveChild(m_setting->GetChild(i));
				}
			}
			CreateDefaultGuiSetting(m_setting);
		}
	}
	
}

fxgenCore::~fxgenCore()
{
	vxdelete(m_axisRender);
	vxdelete(m_rectRender);
	vxdelete(m_modelRender);
	vxdelete(m_editor);
	vxdelete(g);
	
	vxdelete(m_domainEditor);
	vxdelete(m_VoxelCartesianEditor);

	vxdelete(m_sliceController);
	vxdelete(m_sliceController_G);
	vxdelete(m_sliceController_L);
	vxdelete(m_sliceController_C);

	m_root = 0;
}

b8 fxgenCore::Init(GUIController* gui = NULL)
{
	m_gui = gui;
	return true;
}

void fxgenCore::Deinit()
{
	
}
	
void fxgenCore::Draw()
{
	g->ClearColor(m_backColor.r, m_backColor.g, m_backColor.b, m_backColor.a);
	g->Clear(VG_COLOR_BUFFER_BIT | VG_DEPTH_BUFFER_BIT);
	g->Enable(VG_DEPTH_TEST);
	/*
	//透明テスト  部品毎にデプスをOFFにしていく必要がある
	// Traverse Transparent Children
	glEnable(GL_BLEND);
	glDepthMask(GL_FALSE);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	*/
	if (!m_root)
		return;
	
	using namespace VX::Math;
	
	m_modelRender->SetViewMat(m_view);
	m_modelRender->SetProjMat(m_proj);
	m_modelRender->Draw(m_root);
	m_rectRender->Draw(m_root);
	if (m_showaxis) 
	{
		g->Clear(VG_DEPTH_BUFFER_BIT);
		m_axisRender->SetViewMat(m_view);
		m_axisRender->Draw(m_root);
	}
	/*
	//透明テスト終了
	glDepthMask(GL_TRUE);
	glDisable(GL_BLEND);
	*/
}
	
void fxgenCore::Resize(u32 w, u32 h)
{
	m_screenWidth  = w;
	m_screenHeight = h;
	g->Viewport(0, 0, w, h);
	m_editor->Resize(w, h);
	m_rectRender->Resize(w, h);
	m_axisRender->Resize(w, h);
}

void fxgenCore::NewScene()
{
	m_root = 0;
	m_editor->SetSceneGraph(m_root);
	//GPUのキャッシュクリア
	m_modelRender->NewScene();

	vxdelete(m_sliceController);
	vxdelete(m_sliceController_G);
	vxdelete(m_sliceController_L);
	vxdelete(m_sliceController_C);

	vxdelete(m_domainEditor);
	vxdelete(m_VoxelCartesianEditor);

	m_sliceController		= NULL;
	m_sliceController_G		= NULL;
	m_sliceController_L		= NULL;
	m_sliceController_C		= NULL;

	m_domainEditor		= NULL;
	m_VoxelCartesianEditor	= NULL;
}
	
VX::SG::Node* fxgenCore::LoadModel(const s8* filename)
{
	return loadModel(filename);
}

b8 fxgenCore::LoadGeomemtryList(const s8* tp_filename)
{

	if (m_root) {
		;//何もしない
	}else{
		NewScene();
		m_root = vxnew VX::SG::Group();
		m_root->SetName("root");
		m_root->AddChild(m_setting);
		//fuchi
	}

	// load geomerty list by argc parameter
	GeometryListLoader load;
	b8 ret = load.Load(tp_filename,m_root,this);
	if(!ret){
		VXLogE("LoadGeomemtryList# error\n");
	}

	m_editor->SetSceneGraph(m_root);
	m_modelRender->NewScene();

	return ret;
}

b8 fxgenCore::LoadFile(const s8* filename)
{	
	using namespace VX::SG;
	Node* model = loadModel(filename);
	
	if (!model)
		return false;
		
	if (m_root) {
		m_root->AddChild(model);
	} else {
		NewScene();
		m_root = vxnew VX::SG::Group();
		m_root->AddChild(m_setting);
		m_root->SetName(model->GetName());
		m_root->AddChild(model);
	}
	
	m_editor->SetSceneGraph(m_root);
	m_modelRender->NewScene();

	return true;
}

b8 fxgenCore::LoadFiles(const std::vector<std::string>& files)
{
	using namespace VX::SG;
	VX::SG::Group* stlTree;
		
	if (!m_root) {
		NewScene();
		m_root = vxnew VX::SG::Group();
		m_root->SetName("root");
		m_root->AddChild(m_setting);
	}
	s32 i;
	for (i = 0; i < m_root->GetChildCount(); i++)
	{
		if (m_root->GetChild(i)->GetName()=="Model") {
			stlTree = static_cast<VX::SG::Group*>(m_root->GetChild(i));
			break;
		}
	}
	if (i == m_root->GetChildCount()) {
		stlTree = vxnew VX::SG::Transform();
		stlTree->SetName("Model");
		m_root->AddChild(stlTree);
	}

	for(int i=0;i< files.size();i++)
	{
		const std::string& filename = files.at(i);

		Node* model = loadModel(filename.c_str());
		stlTree->AddChild(model);
	}

	m_editor->SetSceneGraph(m_root);
	m_modelRender->NewScene();

	return true;
}

VX::SG::Node* fxgenCore::LoadDirModel(const s8* dirname)
{
	return loadDir(dirname);
}

b8 fxgenCore::LoadDir(const s8* dirname)
{
	using namespace VX::SG;
	VX::SG::Group* nodeDir = vxnew VX::SG::Group();
	VX::SG::Group* stlTree;
	Node* model = loadDir(dirname);
	
	if (model)
	{
		model->Ref();
		
		if (!m_root) {
			NewScene();
			m_root = vxnew VX::SG::Group();
			m_root->AddChild(m_setting);
			m_root->SetName("root");
		}
		VX::Dir dir(dirname);
		nodeDir->SetName(dir.GetName().c_str());
		Group* gp = static_cast<Group*>(model);
		const s32 n = gp->GetChildCount();
		for (s32 i = 0; i < n; ++i){
			nodeDir->AddChild(gp->GetChild(i));
		}
		model->Unref();
	}

	s32 i;
	for (i = 0; i < m_root->GetChildCount(); i++)
	{
		if (m_root->GetChild(i)->GetName() == "Model") {
			stlTree = static_cast<VX::SG::Group*>(m_root->GetChild(i));
			break;
		}
	}
	if (i == m_root->GetChildCount()) {
		stlTree = vxnew VX::SG::Group();
		stlTree->SetName("Model");
		m_root->AddChild(stlTree);
	}
	stlTree->AddChild(nodeDir);
	
	m_editor->SetSceneGraph(m_root);
	m_modelRender->NewScene();
	
	return true;
}

b8 fxgenCore::LoadCellIDMesh(const s8* filename)
{
	using namespace VX::SG;

	Group* meshGroup = NULL;

	std::string ext = getExt(filename);
	if( ext == ".DFI" ){
		MeshCartesianLoader loader;
		meshGroup = loader.Load(filename);
	}else{
		MeshBCMLoader loader;
		meshGroup = loader.Load(filename);
	}

	if(!meshGroup){ return false; }
	
	DeleteDomain();
	DeleteCellIDMesh();

	if(!m_root){
		NewScene();
		m_root = vxnew VX::SG::Group();
		m_root->AddChild(m_setting);
		m_root->SetName("root");
		m_root->AddChild(meshGroup);

		m_editor->SetSceneGraph(m_root);
		m_modelRender->NewScene();
	}else{
		m_root->AddChild(meshGroup);
	}

	if( meshGroup->GetType() == VX::SG::NODETYPE_MESH_CARTESIAN )
	{
		m_sliceController = vxnew SliceController( dynamic_cast<VX::SG::MeshCartesian*>(meshGroup) );
		m_sliceController->SetSlideMode(0);
	}
	else if( meshGroup->GetType() == VX::SG::NODETYPE_MESH_BCM )
	{
		m_sliceController = vxnew SliceController( dynamic_cast<VX::SG::MeshBCM*>(meshGroup) );
		m_sliceController->SetSlideMode(0);
	}

	return true;
}

b8 fxgenCore::LoadVoxelCartesian( const VoxelCartesianParam& param, std::string& errMsg )
{
	// read dfi file
	//


	VoxelCartesianLoader loader;
	VX::SG::VoxelCartesianG* domain = dynamic_cast<VX::SG::VoxelCartesianG*>(loader.Load(param,this,errMsg));
	if( !domain ){
		return false;
	}

	//if there is no root node , crate top node.
	if(!m_root){
		NewScene();
		m_root = vxnew VX::SG::Group();
		m_root->AddChild(m_setting);
		m_root->SetName("root");

		m_editor->SetSceneGraph(m_root);
		m_modelRender->NewScene();
	}else{
		//既存GPUクリア
		m_modelRender->NewScene();
	}

	if(!CreateNewVoxelCartesian(domain))
	{
		assert(false);
		vxdelete(domain);
		return false;
	}
	return true;
}

b8 fxgenCore::LoadDomain(const s8* filename)
{
	if( !m_root ){ 
		VXLogE("LoadDomain# none m_root\n");
		return false; 
	}

	std::string fname = convertPath(filename);
	std::string ext = getExt(filename);

	if( ext == ".TP" && !isBCMFile(filename) )
	{
		DomainCartesianLoader loader;
		VX::SG::DomainCartesian* domain = dynamic_cast<VX::SG::DomainCartesian*>(loader.Load(filename));

		if( !domain ){
			return false;
		}

		if(!CreateNewCartesianDomain(domain))
		{
			vxdelete(domain);
			return false;
		}
		return true;

	}
	else
	{
		DomainBCMLoader loader;
		VX::SG::DomainBCM* domain = dynamic_cast<VX::SG::DomainBCM*>(loader.Load(filename));
		if( !domain ){
			return false;
		}
		if(!CreateNewBCMDomain(domain))
		{
			vxdelete(domain);
			return false;
		}
		return true;
	}

	return false;
}

b8 fxgenCore::LoadBCMedium(const s8* filename)
{
	using namespace VX::SG;
	BCMediumListLoader loader;

	if( !loader.Load(filename, m_setting) ){ 
		return false; 
	}

	UpdateIDColorTable();
	return true;
}

b8 fxgenCore::LoadFaceBC(const s8* filename)
{
	using namespace VX::SG;
	if( !m_root ){ return false; }
	if( !m_setting ){ return false; }
	
	DomainEditor* editor = GetCurrentDomainEditor();
	if( !editor ){
		VXLogI("no Domain=>(%s)\n", filename);
		return false;
	}
	if( !editor->GetActiveDomain() ){
		VXLogI("no active Domain=>(%s)\n", filename);
		return false;
	}

	if(!hasBCMeduim()){
		//try to load BCMeduim.
		if(!LoadBCMedium(filename)){
			VXLogI("fail to load LoadBCMedium=>(%s)\n", filename);
		}
	}

	FaceBCLoader loader;
	return loader.Load(filename, m_setting, editor->GetActiveDomain());
}

b8 fxgenCore::hasBCMeduim()
{
	VX::SG::Group* setting = GetSettingNode();
	const s32 n = setting->GetChildCount();
	b8 hasOuterbc = false ,hasMedium = false;
	for (s32 i = 0; i < n; ++i) {
		const VX::SG::NODETYPE nt = setting->GetChild(i)->GetType();
		if (nt == VX::SG::NODETYPE_OUTERBC){
			hasOuterbc = true;
		} else if (nt == VX::SG::NODETYPE_MEDIUM) {
			hasMedium = true;
		}
	}
	return hasOuterbc && hasMedium;
}

b8 fxgenCore::SavePolylibFile(const s8* filename)
{
	using namespace VX::SG;
	
	if( !m_root ){ return false; }
	if( !m_setting ){ return false; }
	
	PolylibFileSaver saver;
	const b8 visibleOnly = true;
	return saver.Save(filename, m_root , m_setting ,visibleOnly);
}

b8 fxgenCore::SaveGeometryList(const s8* filename)
{
	if( !m_root ){ 
		return false; 
	}
	GeometryListSaver saver;
	return saver.Save(filename,m_root);
}

b8 fxgenCore::SaveAllParameter(const s8* filename)
{
	using namespace VX::SG;
	VXLogI("fxgenCore::SaveAllParameter=>(%s)\n", filename);
	const Domain* domain = NULL;
	if( !m_root ){ return false; }
	if( !m_setting ){ return false; }

	DomainEditor* editor = GetCurrentDomainEditor();
	if( editor ){
		domain = editor->GetActiveDomain();
	}

	AllParameterSaver saver;
	return saver.Save(filename, m_setting, domain);
}

b8 fxgenCore::SaveBCMedium(const s8* filename)
{
	using namespace VX::SG;
	BCMediumListSaver saver;
	return saver.Save(filename, m_setting);
}

b8 fxgenCore::SaveDomain(const s8* filename)
{
	if(!m_root){
		return false;
	}
	
	const VX::SG::Domain* domain = NULL;

	DomainEditor* editor = GetCurrentDomainEditor();
	if( editor ){
		domain = editor->GetActiveDomain();
	}

	if( domain->GetType() == VX::SG::NODETYPE_BBOX_DOMAIN_CARTESIAN){
		DomainCartesianSaver saver;
		return saver.Save(filename, domain);
	}
	if( domain->GetType() == VX::SG::NODETYPE_BBOX_DOMAIN_BCM){
		DomainBCMSaver saver;
		return saver.Save(filename, domain);
	}
	
	return false;
}

b8 fxgenCore::SaveFaceBC(const s8 *filename)
{
	using namespace VX::SG;
	if( !m_root ){ return false; }
	if( !m_setting ){ return false; }

	DomainEditor* editor = GetCurrentDomainEditor();
	if( !editor ){
		return false;
	}

	FaceBCSaver saver;
	return saver.Save(filename, editor->GetActiveDomain());
}

const VX::SG::Node* fxgenCore::GetRoot() const
{
	return m_root;
}

VX::SG::Node* fxgenCore::GetRoot()
{
	return m_root;
}

VX::SG::Group* fxgenCore::GetSettingNode() const
{
	return m_setting;
}

VX::SG::GUISettings* fxgenCore::GetGuiSettingNode() const
{
	for(int i = m_setting->GetChildCount()-1; i >=0; i--){
		VX::SG::Node* n = m_setting->GetChild(i);
		VX::SG::NODETYPE type = n->GetType();
		if( type == VX::SG::NODETYPE_GUI_SETTINGS  )
		{
			return dynamic_cast<VX::SG::GUISettings*>(n);
		}
	}

	return NULL;
}

b8 fxgenCore::SaveFile(const s8* filename, const s8* mode, f32 scale, b8 visibleOnly)
{
	return saveModel(filename, mode, m_root, scale, visibleOnly);
}

b8 fxgenCore::SaveDir(const s8* dirname, const s8* mode, f32 modelScale, b8 visibleOnly)
{
	return saveDir(dirname, mode, m_root, modelScale, visibleOnly);
}
	
void fxgenCore::SetViewMatrix(const VX::Math::matrix4x4& mat)
{
	m_view = mat;
}

void fxgenCore::SetProjMatrix(const VX::Math::matrix4x4& mat)
{
	m_proj = mat;
}

b8 fxgenCore::CreateNewCartesianDomain(VX::SG::DomainCartesian* domain)
{
	if( !m_root ){
		return false;
	}

	GeometryChecker checker;
	VisitAllNode(m_root, checker);
	if( !checker.exist ){
		return false;
	}

	// Delete old Domain Node.
	if( !DeleteDomain() ){
		return false;
	}
	if( !DeleteCellIDMesh() ){
		return false;
	}
		
	if(!domain){
		// for Create New
		domain = vxnew VX::SG::DomainCartesian();
		m_domainEditor = vxnew DomainCartesianEditor(m_root, domain);
		dynamic_cast<DomainCartesianEditor*>(m_domainEditor)->SetDefault();
	}else{
		// for File Load
		m_domainEditor = vxnew DomainCartesianEditor(m_root, domain);
	}

	VX::SG::Group* group = vxnew VX::SG::DomainCartesianGroup();
	group->SetName("Cartesian Domain");
	group->AddChild(domain);
	m_root->AddChild(group);

	VX::SG::Group* sliceGroup = vxnew VX::SG::Group(VX::SG::NODETYPE_BBOX_DOMAIN_SLICE_GROUP);
	sliceGroup->SetName("Slice Planes");
	sliceGroup->SetVisible(false);
	for (u32 i = 0; i < 3; i++) {
		const VX::SG::Node* slice = domain->GetSlice(i);
		sliceGroup->AddChild(const_cast <VX::SG::Node*>(slice));
	}
	group->AddChild(sliceGroup);

	m_sliceController = vxnew SliceController( dynamic_cast<VX::SG::DomainCartesian*>(domain), m_root );
	m_sliceController->SetSlideMode(0);

	return true;
}

const VX::SG::VoxelCartesianG* fxgenCore::GetVoxelCartesianG() const
{
	s32 s = m_root->GetChildCount();
	for(int i=0;i<s;i++){
		if(m_root->GetChild(i)->GetType() == VX::SG::NODETYPE_VOXEL_CARTESIAN_G){
			const VX::SG::VoxelCartesianG* g = dynamic_cast<const VX::SG::VoxelCartesianG*>(m_root->GetChild(i));
			assert(g!=NULL);
			return g;
		}
	}
	return NULL;
}

VX::SG::VoxelCartesianG* fxgenCore::GetVoxelCartesianG()
{
	s32 s = m_root->GetChildCount();
	for(int i=0;i<s;i++){
		if(m_root->GetChild(i)->GetType() == VX::SG::NODETYPE_VOXEL_CARTESIAN_G){
			VX::SG::VoxelCartesianG* g = dynamic_cast<VX::SG::VoxelCartesianG*>(m_root->GetChild(i));
			assert(g!=NULL);
			return g;
		}
	}
	return NULL;
}

b8 fxgenCore::CreateNewVoxelCartesian(VX::SG::VoxelCartesianG* global)
{

	if( !m_root ){
		assert(false);
		return false;
	}

	DeleteVoxelCartesian();
	
	// for File Load
	m_root->AddChild(global);
	// this is instance controller to manage delete global voxel.
	m_VoxelCartesianEditor = vxnew VoxelCartesianEditor(m_root, global);

	// slice controller for global
	m_sliceController_G = vxnew SliceController( global );
	m_sliceController_G->SetSlideMode(0);

	// slice controller for local. just only setting by first child local node. later change according to user operation.
	const std::vector<VX::SG::NodeRefPtr<VX::SG::VoxelCartesianL> >& locals = global->GetLocals();
	assert(locals.size()>0);

	// create local
	VX::SG::VoxelCartesianL* local = locals.at(0);
	m_sliceController_L = vxnew SliceController( local );
	m_sliceController_L->SetSlideMode(0);


		// for clip
	VX::SG::VoxelCartesianC* clip = global->GetClipNode();

	m_sliceController_C = vxnew SliceController( clip );
	m_sliceController_C->SetSlideMode(0);

	return true;

}

/**
*	@brief change local voxel
*/
void fxgenCore::changeSelectedLocalVoxel(VX::SG::VoxelCartesianL* local)
{
	m_sliceController_L->changeTargetNode(local);
}

b8 fxgenCore::CreateNewBCMDomain(VX::SG::DomainBCM* domain)
{
	if( !m_root ){
		return false;
	}

	GeometryChecker checker;
	VisitAllNode(m_root, checker);
	if( !checker.exist ){
		return false;
	}

	if( !DeleteDomain() ){
		return false;
	}
	if( !DeleteCellIDMesh() ){
		return false;
	}

	VX::SG::Group* group = vxnew VX::SG::DomainBCMGroup();
	group->SetName("BCM Domain");

	if(!domain)
	{
		// for Create New
		domain = vxnew VX::SG::DomainBCM();
		group->AddChild(domain);
		
		m_domainEditor = vxnew DomainBCMEditor(m_root, domain, group);
		dynamic_cast<DomainBCMEditor*>(m_domainEditor)->SetDefault();
	}
	else
	{
		// for File Load
		group->AddChild(domain);
		m_domainEditor = vxnew DomainBCMEditor(m_root, domain, group);
		dynamic_cast<DomainBCMEditor*>(m_domainEditor)->ResetGroupLayout();
	}
	
	m_root->AddChild(group);
	
	m_sliceController = vxnew SliceController( dynamic_cast<VX::SG::DomainBCM*>(domain), m_root );
	m_sliceController->SetSlideMode(0);

	return true;
}

b8 fxgenCore::DeleteDomain()
{
	if( !m_root ){
		return true;
	}

	for(int i = m_root->GetChildCount()-1; i >=0 ; --i){
		VX::SG::NODETYPE type;
		type = m_root->GetChild(i)->GetType();
		if( type == VX::SG::NODETYPE_BBOX_DOMAIN ||
			type == VX::SG::NODETYPE_BBOX_DOMAIN_CARTESIAN_GROUP ||
			type == VX::SG::NODETYPE_BBOX_DOMAIN_BCM_GROUP )
		{
			m_root->RemoveChild(m_root->GetChild(i));
		}
	}
	
	vxdelete(m_domainEditor);
	m_domainEditor = NULL;

	ExistChecker checker;
	VisitAllNode(m_root, checker);
	if( !checker.exist ){
		NewScene();	
	}

	vxdelete(m_sliceController);
	m_sliceController = NULL;
	
	return true;
}

b8 fxgenCore::DeleteVoxelCartesian()
{
	if( !m_root ){
		//assert(false);
		return true;
	}

	for(int i = m_root->GetChildCount()-1; i >=0 ; --i){
		VX::SG::NODETYPE type;
		type = m_root->GetChild(i)->GetType();
		if( type == VX::SG::NODETYPE_VOXEL_CARTESIAN_G )
		{
			VX::SG::Node* child = m_root->GetChild(i);
			m_root->RemoveChild(child);
		}
	}
	
	vxdelete(m_VoxelCartesianEditor);
	m_VoxelCartesianEditor = NULL;

	vxdelete(m_sliceController_G);
	m_sliceController_G = NULL;
	
	vxdelete(m_sliceController_L);
	m_sliceController_L = NULL;
	
	vxdelete(m_sliceController_C);
	m_sliceController_C = NULL;

	return true;
}


b8 fxgenCore::DeleteCellIDMesh()
{
	if( !m_root ){
		return true;
	}
	
	for(int i = m_root->GetChildCount()-1; i >=0; --i){
		VX::SG::NODETYPE type;
		type = m_root->GetChild(i)->GetType();
		if( type == VX::SG::NODETYPE_MESH_CARTESIAN 
		   || type == VX::SG::NODETYPE_MESH_BCM )
		{
			m_root->RemoveChild(m_root->GetChild(i));
		}
	}

	ExistChecker checker;
	VisitAllNode(m_root, checker);
	if( !checker.exist ){
		NewScene();	
	}
	vxdelete(m_sliceController);
	m_sliceController = NULL;
	
	return true;
}

DomainEditor* fxgenCore::GetCurrentDomainEditor()
{
	if( !m_root ){ return NULL; }
	return m_domainEditor;
}

VoxelCartesianEditor* fxgenCore::GetCurrentVoxelCartesianEditor()
{
	if( !m_root ){ return NULL; }
	return m_VoxelCartesianEditor;
}

void fxgenCore::ReLoadPolygon_p(const wxString& name,const wxString& posX_p, const wxString& posY_p,const wxString& posZ_p,const wxString& size_p)
{
	using namespace VX::SG;
	VX::Stream* st = 0;
	Node* model = 0;
	
	VX::SG::Group* gr  = GetSafeUserPolyonRoot();

	for(int i=0;i<gr->GetChildCount();i++){
		VX::SG::Node* c = gr->GetChild(i);
		if(c->GetName()==name){
			
			VX::SG::GeometryBVH* geo = static_cast<VX::SG::GeometryBVH*>(c);

			//cの三角形描画情報を更新する(削除して作り直してる｡｡｡｡情報更新になってない)
			//std::string fname = c->GetName();			

			//gr->RemoveChild( c );

			PolygonGenerator PolygonGenerate;
			
			PolygonGenerate.Update(posX_p,posY_p,posZ_p,size_p,geo);
			//model = PolygonGenerate.Load(posX_p,posY_p,posZ_p,size_p);
		
			//model->SetName(fname);

			geo->Build();// Build BVH

			geo->EnableNeedUpdate();

			//gr->AddChild(model);

			//m_editor->SetSceneGraph(m_root);

			break;
		}
	}
}

void fxgenCore::ReLoadPolygon_r
	(const wxString name, wxString posX,const wxString posY,const wxString posZ,const wxString width,
	const wxString depth,const wxString height,const wxString nor_vecX,const wxString nor_vecY,
	const wxString nor_vecZ, const wxString dir_vecX ,const wxString dir_vecY, const wxString dir_vecZ)
{
	using namespace VX::SG;
	VX::Stream* st = 0;
	Node* model = 0;
	
	VX::SG::Group* gr  = GetSafeUserPolyonRoot();

	for(int i=0;i<gr->GetChildCount();i++){
		VX::SG::Node* c = gr->GetChild(i);
		if(c->GetName()==name){
			
			VX::SG::GeometryBVH* geo = static_cast<VX::SG::GeometryBVH*>(c);

			//cの三角形描画情報を更新する(削除して作り直してる｡｡｡｡情報更新になってない)
			//std::string fname = c->GetName();			

			//gr->RemoveChild( c );

			PolygonGenerator PolygonGenerate;
			
			PolygonGenerate.Update(posX,posY,posZ,width,
			depth,height,nor_vecX,nor_vecY,nor_vecZ,
			dir_vecX,dir_vecY,dir_vecZ,geo);
			//model = PolygonGenerate.Load(posX_p,posY_p,posZ_p,size_p);
		
			//model->SetName(fname);

			geo->Build();// Build BVH

			geo->EnableNeedUpdate();

			//gr->AddChild(model);

			//m_editor->SetSceneGraph(m_root);

			break;
		}
	}
}

void fxgenCore::ReLoadPolygon_c
	(const wxString& name, const wxString& posX,const wxString& posY,const wxString& posZ,const wxString& depth,
					const wxString& fanRad,const wxString& bossRad,const wxString& nor_vecX,
					const wxString& nor_vecY,const wxString& nor_vecZ)
{
	using namespace VX::SG;
	VX::Stream* st = 0;
	Node* model = 0;
	
	VX::SG::Group* gr  = GetSafeUserPolyonRoot();

	for(int i=0;i<gr->GetChildCount();i++){
		VX::SG::Node* c = gr->GetChild(i);
		if(c->GetName()==name){
			
			VX::SG::GeometryBVH* geo = static_cast<VX::SG::GeometryBVH*>(c);

			//cの三角形描画情報を更新する(削除して作り直してる｡｡｡｡情報更新になってない)
			//std::string fname = c->GetName();			

			//gr->RemoveChild( c );

			PolygonGenerator PolygonGenerate;
			
			PolygonGenerate.Update( posX, posY, posZ, depth, fanRad, bossRad, nor_vecX, nor_vecY, nor_vecZ,geo);
			//model = PolygonGenerate.Load(posX_p,posY_p,posZ_p,size_p);
		
			//model->SetName(fname);

			geo->Build();// Build BVH

			geo->EnableNeedUpdate();

			//gr->AddChild(model);

			//m_editor->SetSceneGraph(m_root);

			break;
		}
	}
}

void fxgenCore::LoadPolygon_p(const wxString& posX_p, const wxString& posY_p,const wxString& posZ_p,const wxString& size_p,const wxString row_p)
{
	using namespace VX::SG;
	VX::SG::Group* polygonTree  = GetSafeUserPolyonRoot();


	const std::string fname = std::string("Point_")+ row_p.ToStdString();
	VX::Stream* st = 0;
	Node* model = 0;

	PolygonGenerator PolygonGenerate;

	model = PolygonGenerate.Load(posX_p,posY_p,posZ_p,size_p);

	model->SetName(fname);

	static_cast<GeometryBVH*>(model)->Build();// Build BVH


	polygonTree->AddChild(model);

	m_editor->SetSceneGraph(m_root);
	m_modelRender->NewScene();

}

void fxgenCore::LoadPolygon_r
	(const wxString posX,const wxString posY,const wxString posZ,const wxString width,
	const wxString depth,const wxString height,const wxString nor_vecX,const wxString nor_vecY,
	const wxString nor_vecZ, const wxString dir_vecX ,const wxString dir_vecY, const wxString dir_vecZ, const wxString row_r)
{
	using namespace VX::SG;
	VX::SG::Group* polygonTree  = GetSafeUserPolyonRoot();


	const std::string fname = std::string("Rectangular_")+ row_r.ToStdString();
	VX::Stream* st = 0;
	Node* model = 0;

	PolygonGenerator PolygonGenerate;

	model = PolygonGenerate.Load(posX,posY,posZ,width,
			depth,height,nor_vecX,nor_vecY,nor_vecZ,
			dir_vecX,dir_vecY,dir_vecZ);

	model->SetName(fname);

	static_cast<GeometryBVH*>(model)->Build();// Build BVH

	polygonTree->AddChild(model);

	m_editor->SetSceneGraph(m_root);
	m_modelRender->NewScene();

}

void fxgenCore::LoadPolygon_c
	(const wxString posX,const wxString posY,const wxString posZ,const wxString depth,
	const wxString fanRad,const wxString bossRad,const wxString nor_vecX,
	const wxString nor_vecY,const wxString nor_vecZ,const wxString row_c)
{
	using namespace VX::SG;
	VX::SG::Group* polygonTree = GetSafeUserPolyonRoot();

	const std::string fname = std::string("Cylinder_")+ row_c.ToStdString();
	VX::Stream* st = 0;
	Node* model = 0;


	PolygonGenerator PolygonGenerate;


	model = PolygonGenerate.Load(posX,posY,posZ,depth,
			fanRad,bossRad,nor_vecX,nor_vecY,nor_vecZ);

	model->SetName(fname);

	static_cast<GeometryBVH*>(model)->Build();// Build BVH


	polygonTree->AddChild(model);

	m_editor->SetSceneGraph(m_root);
	m_modelRender->NewScene();

}

VX::SG::Group* fxgenCore::GetSafeUserPolyonRoot()
{
	using namespace VX::SG;
	VX::SG::Group* polygonTree;

	if (!m_root) {
		NewScene();
		m_root = vxnew VX::SG::Group();
		m_root->SetName("root");
		m_root->AddChild(m_setting);
	}
	s32 i;
	for (i = 0; i < m_root->GetChildCount(); i++)
	{
		if (m_root->GetChild(i)->GetName()=="Polygon") {
			polygonTree = static_cast<VX::SG::Group*>(m_root->GetChild(i));
			return polygonTree;
		}
	}
	
	//無いので新規作成
	polygonTree = vxnew VX::SG::Group();
	polygonTree->SetName("Polygon");
	m_root->AddChild(polygonTree);
	return polygonTree;

}

void fxgenCore::DeletePolygon(const wxString& name)
{
	VX::SG::Group* gr = GetSafeUserPolyonRoot();

	for(int i=0;i<gr->GetChildCount();i++){
		VX::SG::Node* c = gr->GetChild(i);
		if(c->GetName()==name){
			gr->RemoveChild( c );
			break;
		}
	}

	if(gr->GetChildCount() == 0 )
	{
		for(int i = m_root->GetChildCount()-1; i >=0 ; --i){
			std::string name;
			name = m_root->GetChild(i)->GetName();
			if( name == "Polygon" )
			{
				VX::SG::Node* child = m_root->GetChild(i);
				m_root->RemoveChild(child);
			}
		}
	}


	m_gui->UpdateTree(false);
	m_modelRender->NewScene();

}

void fxgenCore::DeleteNode(VX::SG::Node* node)
{
	using namespace VX::SG;

	if (node == NULL)
		return;

	NODETYPE type = node->GetType();
	if (!m_root || node == m_root) {
		NewScene();

	} else if (type == NODETYPE_BBOX_DOMAIN_CARTESIAN_GROUP || 
		       type == NODETYPE_BBOX_DOMAIN_BCM_GROUP) {
		
		DeleteDomain();

		// Enable Create Cartesian / BCM Menu.
		 m_gui->EnableMenu("Domain (D)", 58, true);
		 m_gui->EnableMenu("Domain (D)", 59, true);
		
		// Disable Edit Cartesian Menu.
		m_gui->EnableMenu("Domain (D)", 60,  false);
		m_gui->EnableMenu("Domain (D)", 62, false);
	

	} else {
		Node* parentNode = node->GetParent();

		if (parentNode == NULL)
			return;

		if (parentNode->GetType() == NODETYPE_GROUP || 
			parentNode->GetType() == NODETYPE_TRANSFORM) {
			Group* parentGroup = dynamic_cast<Group*>(parentNode);
			parentGroup->RemoveChild(node);
			if(parentGroup->GetChildCount() == 0  
				|| (parentGroup == m_root && parentGroup->GetChildCount() <= 1)
				) {
				DeleteNode(parentGroup);
			}
		}

	}
	m_gui->UpdateTree(false);
	m_modelRender->NewScene();

}

SliceController* fxgenCore::GetSliceController(u32 slideMode)
{
	if( !m_root ){ return NULL; }
	if( !m_sliceController ){ return NULL; }
	if( !m_sliceController->CheckStatus() ){ return NULL; }

	m_sliceController->SetSlideMode(slideMode);
	return m_sliceController;
}
SliceController* fxgenCore::GetSliceController_G(u32 slideMode)
{
	if( !m_root ){ return NULL; }
	if( !m_sliceController_G ){ return NULL; }
	if( !m_sliceController_G->CheckStatus() ){ return NULL; }

	m_sliceController_G->SetSlideMode(slideMode);
	return m_sliceController_G;
}



SliceController* fxgenCore::GetSliceController_L(u32 slideMode)
{
	if( !m_root ){ return NULL; }
	if( !m_sliceController_L ){ return NULL; }
	if( !m_sliceController_L->CheckStatus() ){ return NULL; }

	m_sliceController_L->SetSlideMode(slideMode);
	return m_sliceController_L;
}

SliceController* fxgenCore::GetSliceController_C(u32 slideMode)
{
	if( !m_root ){ return NULL; }
	if( !m_sliceController_C ){ return NULL; }
	if( !m_sliceController_C->CheckStatus() ){ return NULL; }

	m_sliceController_C->SetSlideMode(slideMode);
	return m_sliceController_C;
}


void fxgenCore::SetSlideMode(u32 slideMode)
{
	if (m_sliceController != NULL && m_sliceController->CheckStatus() ) {
		m_sliceController->SetSlideMode(slideMode);
	}

	if (m_sliceController_G != NULL && m_sliceController_G->CheckStatus() ) {
		m_sliceController_G->SetSlideMode(slideMode);
	}

	if (m_sliceController_L != NULL && m_sliceController_L->CheckStatus() ) {
		m_sliceController_L->SetSlideMode(slideMode);
	}

	if (m_sliceController_C != NULL && m_sliceController_C->CheckStatus() ) {
		m_sliceController_C->SetSlideMode(slideMode);
	}
}

void fxgenCore::ClearSelection()
{
	m_editor->ClearSelections();
}

void fxgenCore::SelectionPick(s32 x, s32 y, b8 selection)
{
	m_editor->SetProjMat(m_proj);
	m_editor->SetViewMat(m_view);
	m_editor->Pick(x,y, selection);
}

const VX::SGPickerElm& fxgenCore::GetPickInformation() const
{
	return m_editor->GetPickInformation();
}

std::string fxgenCore::GetPickInformationStr() const
{
	std::string msg;
	const VX::SGPickerElm& pick = GetPickInformation();
	bool isGeo = false;
	VX::SG::HitGeometry geo = pick.GetGeometry(isGeo);
	if(isGeo){
		const VX::SG::Node* node = m_root;
		const VX::SG::HitGeometry* hg = &geo;
		const VX::SG::VoxelCartesianG* g = GetVoxelCartesianG();
		VX::SGPickerHelper helper;
		if(g != NULL){
			//ボクセルモードの場合はGridのインデックスを表示する
			msg = helper.getVoxelPickInformation(g,hg);
		}else{
			//通常のstlポリゴンピック
			msg = helper.getTriPickInformation(node,hg);
		}
	}
	return msg;
}

void fxgenCore::EndSelectionPick(s32 x, s32 y)
{
	m_editor->EndPick(x,y);
}

/*
void fxgenCore::SelectionRectPick(s32 startx, s32 starty, s32 endx, s32 endy)
{
#ifdef PERF_PICK
	f64 st = VX::GetTimeCount();
#endif
		m_editor->Pick(startx, starty, endx, endy);
#ifdef PERF_PICK
	f64 et = VX::GetTimeCount();
	printf("picktime = %3.6f\n", et - st);
#endif
}*/

void fxgenCore::StartSelectionRect(s32 startx, s32 starty, s32 endx, s32 endy)
{
	m_rectRender->StartRect(startx, starty, endx, endy);
}

void fxgenCore::EndSelectionRect(b8 selection)
{
	s32 sx,sy,ex,ey;
	m_rectRender->EndRect(sx, sy, ex, ey);
	m_editor->SetProjMat(m_proj);
	m_editor->SetViewMat(m_view);
	m_editor->Pick(sx, sy, ex, ey, selection);
}

void fxgenCore::SelectionSetID(u32 id)
{
	VXLogI("SetID = %d\n", id);
	m_editor->SelectionSetID(id);
}

void fxgenCore::ShowWire(b8 enable)
{
	m_modelRender->ShowWire(enable);
}

b8 fxgenCore::IsShowWire() const
{
	return m_modelRender->IsShowWire();
}

void fxgenCore::ShowDomainWire(b8 enable)
{
	m_modelRender->ShowDomainWire(enable);
}

b8 fxgenCore::IsShowDomainWire() const
{
	return m_modelRender->IsShowDomainWire();
}

void fxgenCore::ShowMeshGrid(b8 enable)
{
	m_modelRender->ShowMeshGrid(enable);
}

b8 fxgenCore::IsShowMeshGrid() const
{
	return m_modelRender->IsShowMeshGrid();
}

void fxgenCore::ShowPolygon(b8 enable)
{
	m_modelRender->ShowPolygon(enable);
}

b8 fxgenCore::IsShowPolygon() const
{
	return m_modelRender->IsShowPolygon();
}

void fxgenCore::ShowAxis(b8 enable)
{
	m_showaxis = enable;
}

b8 fxgenCore::IsShowAxis() const
{
	return m_showaxis;
}

void fxgenCore::ShowCross(b8 enable)
{
	m_cross->SetVisible(enable);
}

b8 fxgenCore::IsShowCross() const
{
	return m_cross->GetVisible();
}



void fxgenCore::UpdateIDColorTable()
{
	std::vector<VX::SG::NodeRefPtr<const VX::SG::Medium> > mediumVector;
	m_modelRender->SetIDColorTable(0, m_defaultIDColor);
	
	const s32 n = m_setting->GetChildCount();
	for (s32 i = 0; i < n; ++i)
	{
		const VX::SG::Node* node = m_setting->GetChild(i);
		if (node->GetType() == VX::SG::NODETYPE_LOCALBC)
		{
			const VX::SG::LocalBC* bc = static_cast<const VX::SG::LocalBC*>(node);
			const s32 id = bc->GetID();
			m_modelRender->SetIDColorTable(id, bc->GetColor());
		}
		else if (node->GetType() == VX::SG::NODETYPE_MEDIUM)
		{
			const VX::SG::Medium* med = static_cast<const VX::SG::Medium*>(node);
			mediumVector.push_back(med);
			const s32 id = med->GetID();
			m_modelRender->SetIDColorTable(id, med->GetColor());
		}
	}
	if (mediumVector.size() > 0) {
		SyncMediumTree(mediumVector);
	}
}

VX::Math::vec4 fxgenCore::GetIDColorTable(s32 id)
{
	return m_modelRender->GetIDColorTable(id);
}

void fxgenCore::SetCross(const VX::Math::vec3& pos, f32 size)
{
	m_cross->Set(pos.x, pos.y, pos.z, size);
}

void fxgenCore::SelectionMode(fxgenCore::SELECTIONMODE mode)
{
	m_editor->SelectionMode(static_cast<SceneEditor::SELECTMODE>(mode));
}

fxgenCore::SELECTIONMODE fxgenCore::GetSelectionMode() const
{
	return static_cast<SELECTIONMODE>(m_editor->GetSelectionMode());
}

void fxgenCore::SetBackgroundColor(const VX::Math::vec4& color)
{
	m_backColor = color;
}
VX::Math::vec4 fxgenCore::GetBackgroundColor()const
{
	return m_backColor;
}

b8  fxgenCore::SyncMediumTree(const std::vector<VX::SG::NodeRefPtr<const VX::SG::Medium> > mediumVector)
{
	using namespace VX::SG;

	Group* mediumTree = vxnew Group(NODETYPE_MEDIUM_GROUP);
	mediumTree->SetName("Medium");
		

	for(int i=0;i< mediumVector.size(); i++)
	{
		Node* mediumNode = const_cast<Node*>(static_cast<const Node*>(mediumVector[i]));
		mediumNode->SetName(mediumVector[i]->GetLabel());
		mediumTree->AddChild(mediumNode);
	}


	if (m_root)
	{
		for (s32 i = 0; i < m_root->GetChildCount(); i++)
		{
			if(m_root->GetChild(i)->GetType() == NODETYPE_MEDIUM_GROUP)
			{
				m_root->RemoveChild(m_root->GetChild(i));
			}
		}
		m_root->AddChild(mediumTree);
	} else {
		NewScene();
		m_root = vxnew VX::SG::Group();
		m_root->SetName("root");
		m_root->AddChild(m_setting);
		m_root->AddChild(mediumTree);
	}
	m_editor->SetSceneGraph(m_root);
	if (m_gui)
	{
		m_gui->UpdateTree(false);
	}
	m_modelRender->NewScene();

	return true;
}
