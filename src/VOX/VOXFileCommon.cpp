/// 

#include "VOXFileCommon.h"
#include "../VX/VX.h"
#include "../VX/Log.h"
#include "../VX/RLE.h"

#include <stdio.h>

namespace VOX
{
/////////////////// Endian Swapper //////////////////////////
/// for 2bytes
static inline void BSwap16(void* a){
	u16* x = (u16*)a;
	*x = (u16)( ((((*x) & 0xff00) >> 8 ) | (((*x) & 0x00ff) << 8)) );
}

/// for 4bytes
static inline void BSwap32(void* a){
	u32* x = (u32*)a;
	*x = ( (((*x) & 0xff000000) >> 24) | (((*x) & 0x00ff0000) >> 8 ) |
	       (((*x) & 0x0000ff00) <<  8) | (((*x) & 0x000000ff) << 24) );
}

/// for 8bytes
static inline void BSwap64(void* a){
	u64* x = (u64*)a;
	*x=( (((*x) & 0xff00000000000000ull) >> 56) | (((*x) & 0x00ff000000000000ull) >> 40) |
	     (((*x) & 0x0000ff0000000000ull) >> 24) | (((*x) & 0x000000ff00000000ull) >>  8) |
	     (((*x) & 0x00000000ff000000ull) <<  8) | (((*x) & 0x0000000000ff0000ull) << 24) |
	     (((*x) & 0x000000000000ff00ull) << 40) | (((*x) & 0x00000000000000ffull) << 56) );
}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	size_t GetBitVoxelSize( const LBHeader& hdr, size_t numBlocks ){
		size_t blockSize = (hdr.size[0] + hdr.vc * 2) * (hdr.size[1] + hdr.vc * 2) * (hdr.size[2] + hdr.vc * 2);
		return GetBitVoxelSize(blockSize * numBlocks, static_cast<unsigned char>(hdr.bitWidth));
	}
	
	bool Load_LeafBlock_Header( FILE *fp, LBHeader& hdr, bool& isNeedSwap)
	{
		fread(&hdr, sizeof(LBHeader), 1, fp);
		isNeedSwap = false;
		
		if( hdr.identifier != LEAFBLOCK_FILE_IDENTIFIER ){
			BSwap32(&hdr.identifier);
	
			if( hdr.identifier != LEAFBLOCK_FILE_IDENTIFIER ){
				return false;
			}
	
			isNeedSwap = true;
	
			BSwap16(&hdr.bitWidth);
			BSwap32(&hdr.vc);
			BSwap32(&hdr.size[0]);
			BSwap32(&hdr.size[1]);
			BSwap32(&hdr.size[2]);
			BSwap64(&hdr.numBlock);
		}
		
		return true;
	}
	
	bool Load_LeafBlock_CellIDHeader( FILE *fp, LBCellIDHeader& chdr, const bool isNeedSwap )
	{
		fread(&chdr, sizeof(LBCellIDHeader), 1, fp);
		if( isNeedSwap ){
			BSwap64(&chdr.numBlock);
			BSwap64(&chdr.compSize);
		}
		if(chdr.compSize != 0 && (chdr.compSize % sizeof(GridRleCode)) != 0){
			VXLogE("compress size is invalid\n");
			return false;
		}
		return true;
	}
	
	bool Load_LeafBlock_CellIDData( FILE *fp, unsigned char** data, 
	                                       const LBHeader& hdr, const LBCellIDHeader& chdr, const bool isNeedSwap)
	{
		using namespace VOX;
		size_t sz = 0;
		if( chdr.compSize == 0 ){
			sz = GetBitVoxelSize(hdr, chdr.numBlock) * sizeof(bitVoxelCell);
		}else{
			sz = chdr.compSize;
		}

		//*data = vxnew unsigned char[sz];
		*data = new unsigned char[sz]; // !! Do not use vxnew !!
	
		size_t result = fread(*data, sizeof(unsigned char), sz, fp);
		
#ifdef _DEBUG
		if (result != sz){
			assert(false);
		}
#endif

		if( isNeedSwap ){
			if( chdr.compSize == 0 ){
				size_t bitVoxelSize = GetBitVoxelSize(hdr, chdr.numBlock);
				bitVoxelCell* bitVoxel = reinterpret_cast<bitVoxelCell*>(*data);
				for(int i = 0; i < bitVoxelSize; i++){
					BSwap32(&bitVoxel[i]);
				}
			}else{
				GridRleCode* p = reinterpret_cast<GridRleCode*>(*data);
				for(int i = 0; i < sz / sizeof(GridRleCode); i++){
					BSwap32(&(p[i].c));
				}
			}
		}
		return true;
	}
	
	unsigned char* DecompCellIDData( const LBHeader &header,  const CellIDCapsule& cc)
	{
		using namespace VOX;
		using namespace VX;
	
		unsigned char* ret = NULL;
	
		size_t blockSize = (header.size[0] + header.vc*2) * (header.size[1] + header.vc*2) * (header.size[2] + header.vc*2);
		size_t dataSize  = blockSize * cc.header.numBlock;
	
		//VXLogI("cc.header.numBlock : %ld, cc.header.compSize : %ld\n", cc.header.numBlock, cc.header.compSize);
		//for(int i = 0; i < cc.header.compSize; i++){
		//	VXLogI("cc.data[%d] : %d\n", i, cc.data[i]);
		//}
	
		bitVoxelCell* bitVoxel = NULL;
		if( cc.header.compSize != 0){
			size_t bitVoxelSize = GetBitVoxelSize(dataSize, static_cast<u8>(header.bitWidth));
			size_t dsize = bitVoxelSize * sizeof(bitVoxelCell);
			bitVoxel = rleDecode<bitVoxelCell, unsigned char>(cc.data, cc.header.compSize, dsize);
			//vxdeleteArray(cc.data);
			delete [] cc.data; // !! Do not use vxnew !!
		}else{
			bitVoxel = reinterpret_cast<bitVoxelCell*>(cc.data);
		}
	
		ret = DecompressBitVoxel(dataSize, bitVoxel, static_cast<u8>(header.bitWidth));
	
		//vxdeleteArray(bitVoxel);
		delete [] (bitVoxel); // !! Do not use vxnew !!
	
		return ret;
	}
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	bool LoadOctreeHeader(FILE *fp, OctHeader& header, bool& isNeedSwap)
	{
		isNeedSwap = false;
		fread(&header, sizeof(header), 1, fp);
	
		if( header.identifier != OCTREE_FILE_IDENTIFIER ){
			BSwap32(&header.identifier);
	
			if( header.identifier != OCTREE_FILE_IDENTIFIER ){
				return false;
			}
	
			isNeedSwap = true;
			for(int i = 0; i < 3; i++){
				BSwap64(&header.org[i]);
				BSwap64(&header.rgn[i]);
				BSwap32(&header.rootDims[i]);
			}
			BSwap32(&header.maxLevel);
			BSwap64(&header.numLeaf);
		}
	
		return true;
	}
	
	bool LoadOctreeFile(const std::string& filename, OctHeader& header, std::vector<Pedigree>& pedigrees)
	{
		using namespace std;
		FILE *fp = NULL;
		if( (fp = fopen(filename.c_str(), "rb")) == NULL ){
			VXLogE("open file error(%s) [%s:%d].\n", filename.c_str(), __FILE__, __LINE__);
			return false;
		}
		
		bool isNeedSwap = false;
		if( !LoadOctreeHeader(fp, header, isNeedSwap) ){
			VXLogE("Load Header error(%s) [%s:%d].\n", filename.c_str(), __FILE__, __LINE__);
			fclose(fp);
			return false;
		}
	
		pedigrees.resize(header.numLeaf);
		fread(&pedigrees[0], sizeof(Pedigree), header.numLeaf, fp);
	
		fclose(fp);
	
		if( isNeedSwap ){
			for(vector<Pedigree>::iterator it = pedigrees.begin(); it != pedigrees.end(); ++it){
				BSwap64(&(*it));
			}
		}
		return true;
	}

} // namespace VOX

