#ifndef __VOX_UTIL_H__
#define __VOX_UTIL_H__

#include "Node.h"

namespace VOX
{
	template<class T>
	inline void VisitAllBCMNode(const VOX::Node* node, T& func)
	{
		if (!node)
		{
			return;
		}

		func(node);

		if( !node->isLeafNode() )
		{
			for(int i = 0; i < 8; i++){
				VisitAllBCMNode(node->getChild(i), func);
			}
		}
	}

	class BCMNodeCounter
	{
	public:
		BCMNodeCounter() : m_numNodes(0) { }
		~BCMNodeCounter() { }
		void operator() (const VOX::Node* node)
		{
			m_numNodes++;
		}

		u64 GetNumNodes() const { return m_numNodes; }

	private:
		u64 m_numNodes;
	};

} // namespace VOX

#endif // __VOX_UTIL_H__
