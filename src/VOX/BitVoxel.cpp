///
/// @file  BitVoxel.cpp
/// @brief ビットボクセル圧縮/展開ライブラリ
///

#include "BitVoxel.h"
#include <cstring>

#include "../VX/Memory.h"

namespace VOX
{
	size_t GetBitVoxelSize(const size_t voxelSize, const unsigned char bitWidth)
	{
		const unsigned char vox_per_cell = (sizeof(bitVoxelCell) * 8) / bitWidth;
		return (voxelSize / vox_per_cell + (voxelSize % vox_per_cell == 0 ? 0 : 1 ));
	}


	bitVoxelCell* CompressBitVoxel( size_t* bitVoxelSize, const size_t voxelSize, const unsigned char* voxel, const unsigned char  bitWidth)
	{
		const unsigned char vox_per_cell = (sizeof(bitVoxelCell) * 8) / bitWidth;
		size_t bsz  = voxelSize / vox_per_cell + (voxelSize % vox_per_cell == 0 ? 0 : 1);

		//bitVoxelCell* bitVoxel = vxnew bitVoxelCell[bsz];
		bitVoxelCell* bitVoxel = new bitVoxelCell[bsz]; // !! Do not use vxnew !!
		memset(bitVoxel, 0, sizeof(bitVoxelCell) * bsz);
		
		unsigned char mask = 0;
		for(int i = 0; i < bitWidth; i++) mask += (1 << i);

		for(size_t i = 0; i < voxelSize; i++){
			size_t       cellIdx =  i / vox_per_cell;
			unsigned int bitIdx  = (i % vox_per_cell) * bitWidth;

			unsigned char c = voxel[i];

			bitVoxel[cellIdx] += (c & mask) << bitIdx;
		}

		*bitVoxelSize = bsz;
		return bitVoxel;

	}
	

	unsigned char* DecompressBitVoxel( const size_t voxelSize, const bitVoxelCell* bitVoxel, const unsigned char  bitWidth)
	{
		const unsigned char vox_per_cell = (sizeof(bitVoxelCell) * 8) / bitWidth;

		//unsigned char* voxel = vxnew unsigned char[voxelSize];
		unsigned char* voxel = new unsigned char[voxelSize]; // !! Do not use vxnew !!
		memset(voxel, 0, sizeof(unsigned char) * voxelSize);

		unsigned char mask = 0;
		for(int i = 0; i < bitWidth; i++) mask += (1 << i);

		for(size_t i = 0; i < voxelSize; i++){
			size_t       cellIdx =  i / vox_per_cell;
			unsigned int bitIdx  = (i % vox_per_cell) * bitWidth;

			voxel[i] = (bitVoxel[cellIdx] >> bitIdx) & mask;
		}

		return voxel;
	}


} // namespace VOX

