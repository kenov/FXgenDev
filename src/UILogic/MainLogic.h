/*
 * Copyright 2011 SGI Japan, Ltd.
 * ALL RIGHTS RESERVED
 *
 * MainLogic.h
 * 
 */

#ifndef _VX_MAIN_LOGIC_H_
#define _VX_MAIN_LOGIC_H_

#include "../VX/Type.h"
#include "BaseLogic.h"
#include <string>
#include <vector>

class fxgenCore;
class ViewController;
class GUIController;
namespace VX {
	namespace SG {
		class Node;
	} // namespace SG
} // namespace VX

namespace UI{
	class DomainCartDlg;
} // namespace UI

class MainLogic : public BaseLogic
{
private:
	MainLogic();
public:
	MainLogic(const std::vector<std::string>& argv);
	~MainLogic();

	void Init();
	void Deinit();
	
	void Draw();
	void OnResize(u32 w, u32 h);
	void OnMouseMove(s32 x, s32 y);
	void OnMouseLeftDown  (s32 x, s32 y);
	void OnMouseRightDown (s32 x, s32 y);
	void OnMouseMiddleDown(s32 x, s32 y);
	void OnMouseLeftUp    (s32 x, s32 y);
	void OnMouseRightUp   (s32 x, s32 y);
	void OnMouseMiddleUp  (s32 x, s32 y);
	bool OnIdle();
	void OnKeyDown(s32 key);
	void OnKeyUp(s32 key);
	void OnMenu(s32 menu_id);
	
	static void OnMenu_(s32 menu_id, void* thisptr){ static_cast<MainLogic*>(thisptr)->OnMenu(menu_id); }

	void importGeometryList(const std::string& filename);

private:
	bool hasArgv()const;
	void actionArgv();

private:
	fxgenCore* m_core;
	ViewController* m_vctrl;
	GUIController* m_gui;

	std::vector<std::string> m_argv;

	s32 m_width;
	s32 m_height;
	b8 m_exit;
	b8 m_treeDrag;

	s32 m_oldMouseX;
	s32 m_oldMouseY;
	s32 m_mouseButton;

	s32 m_key;
	b8 m_key_ctrl;
	b8 m_key_alt;
	b8 m_key_shift;

	f32 m_spinx;
	f32 m_spiny;
	f32 m_transx;
	f32 m_transy;
	f32 m_zoom;
	
	f64 m_oldleftclickedTime;
	
	b8 m_dragging, m_rightdragging;
	s32 m_selecting_sx,	m_selecting_sy;
	s32 m_selecting_ex,	m_selecting_ey;

	u32 m_slideMode;

	void fileOpenMenuFunc(void);
	void exitMenuFunc(void);
	void aboutMenuFunc(void);
	void updateMenu(void);
	void initMenu(void);
	void NotifySelectionPick();


};

#endif // _VX_MAIN_LOGIC_H_

