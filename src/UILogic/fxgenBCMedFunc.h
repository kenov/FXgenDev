/*
 *
 * fxgenBCMedFunc.h
 *
 * This is fxgenBCMedFunc class
 * 
 */

#ifndef __fxgenBCMedFunc_H__
#define __fxgenBCMedFunc_H__

#include "../VX/Type.h"
#include "../UI/BCMedFunc.h"
#include "../VX/SG/Node.h"
#include <string>
#include <vector>

//class fxgenCore;
//class ViewController;
//class GUIController;



class fxgenBCMedFunc : public UI::BCMedFunc
{
public:
	fxgenBCMedFunc();
	virtual ~fxgenBCMedFunc();

	//copy constructor
    fxgenBCMedFunc(const fxgenBCMedFunc& obj);
    fxgenBCMedFunc& operator=(const fxgenBCMedFunc& obj);

	int GetLocalBCClassNum() const;
	int GetOuterBCClassNum() const;
	std::string GetLocalBCClass(int i) const;
	std::string GetOuterBCClass(int i) const;
	int GetLocalBCNum() const;
	int GetOuterBCNum() const;
	int GetMediumNum() const;
	const VX::SG::LocalBC* GetLocalBC(int i) const;
	const VX::SG::OuterBC* GetOuterBC(int i) const;
	const VX::SG::Medium* GetMedium(int i) const;
	void ClearLocalBC();
	void ClearOuterBC();
	void ClearMedium();
	void AddLocalBC(const VX::SG::LocalBC* localbc);
	void AddOuterBC(const VX::SG::OuterBC* outerbc);
	void AddMedium(const VX::SG::Medium* medium);
	void AddLocalBCClass(const VX::SG::LocalBCClass* localbcclass);
	void AddOuterBCClass(const VX::SG::OuterBCClass* outerbcclass);
	const VX::SG::LocalBCClass* findLocalBCClass(const std::string& name) const;
	const VX::SG::OuterBCClass* findOuterBCClass(const std::string& name) const;
	virtual const VX::SG::LocalBC* CreateLocalBC(const std::string& name, const std::string& classname, float col[4], unsigned int id, const std::string& mediumname) const;
	virtual const VX::SG::OuterBC* CreateOuterBC(const std::string& name, const std::string& classname) const;
	virtual const VX::SG::Medium* CreateMedium(const std::string& label, float col[4], unsigned int id, const std::string& medType) const;
	
private:
	void assignCopy(const fxgenBCMedFunc& obj);

private:
	std::vector<std::string> m_outerBCClassListStr;
	std::vector<std::string> m_localBCClassListStr;
	std::vector<VX::SG::NodeRefPtr<const VX::SG::LocalBC> >      m_localbc;
	std::vector<VX::SG::NodeRefPtr<const VX::SG::OuterBC> >      m_outerbc;
	std::vector<VX::SG::NodeRefPtr<const VX::SG::Medium> >       m_medium;
	std::vector<VX::SG::NodeRefPtr<const VX::SG::LocalBCClass> > m_localbcclass;
	std::vector<VX::SG::NodeRefPtr<const VX::SG::OuterBCClass> > m_outerbcclass;
};


#endif // __VX_BASELOGIC_H__
