﻿/*
 * MainLogic.cpp
 * 
 * Logic is UI bridge code.
 * UI must be separated from other code.
 *
 */

//#include "../VX/SG/CoordinateViewer.h"

#include "../VX/VX.h"
#include "../VX/Time.h"
#include "MainLogic.h"
#include "fxgenBCMedFunc.h"
#include "../Core/fxgenCore.h"
#include "../VX/SG/SceneGraph.h"
#include "../Core/ViewController.h"
#include "../Core/GUIController.h"
#include "../Core/DomainEditor.h"
#include "../Core/DomainCartesianEditor.h"
#include "../Core/DomainBCMEditor.h"
#include "../Core/SliceController.h"
#include "../VX/Graphics.h"
#include "../VX/Graphics/GraphicsProfiler.h"

#include <wx/string.h>
#include <wx/file.h>
#include <wx/dir.h>

#include <string>

// -- UI code --
#include "../UI/vxWindow.h"
#include "../UI/DialogUtil.h"
#include "../FileIO/VoxelCartesianParam.h"

// --

using namespace UI;

namespace {
	
	class fxgenSubdomainFunc : public UI::SubdomainFunc
	{
	public:
		fxgenSubdomainFunc(VX::SG::Domain* dom) : m_dom(dom){}
		
		void Inactive()
		{
			if (!m_dom)
				return;
			m_dom->SelectedSubdomainStatus(false);
		}
		void Active()
		{
			if (!m_dom)
				return;
			m_dom->SelectedSubdomainStatus(true);
		}
	private:
		VX::SG::Domain* m_dom;
	};
	
	

	
	void clearLocalBCOuterBCMedium(VX::SG::Group* setting)
	{
		setting->clearLocalBCOuterBCMedium();
		/*
		const int n = setting->GetChildCount();
		for (int i = n - 1; i >= 0; --i) {
			VX::SG::Node* node = setting->GetChild(i);
			if (node->GetType() == VX::SG::NODETYPE_LOCALBC
			||  node->GetType() == VX::SG::NODETYPE_OUTERBC
			||  node->GetType() == VX::SG::NODETYPE_MEDIUM)
				setting->RemoveChild(node);
		}
		*/
	}
}

enum MENU_ID {
	//base menu name
	MENUID_FILE	   = 1,
	MENUID_F_IMPORT,
	MENUID_F_EXPORT,
	MENUID_VIEW,
	MENUID_SELECTION,
	MENUID_DOMAIN,
	MENUID_MESH,
	MENUID_SETTING,
	MENUID_VOXELTOOLS,
	MENUID_HELP,
	MENUID_INVISIBLE,
	MENUID_TREE,

	//menu
	MENUID_NEWSCENE,
	MENUID_OPENSCENE,
	MENUID_SAVESCENE,
	MENUID_IMPORT_SHAPEFILE,
	MENUID_IMPORT_SHAPEDIR,
	MENUID_IMPORT_DOMAIN,
	MENUID_IMPORT_GEOMETRY_LIST,
	MENUID_IMPORT_BCMEDIUMLIST,
	MENUID_IMPORT_FACEBC,
	MENUID_IMPORT_CELLIDMESH,

	MENUID_SAVEFILE_VMG,
	MENUID_SAVEFILE_OBJ,
	MENUID_SAVEFILE_STL,
	MENUID_SAVEFILE_GEOMETRY_LIST,
	MENUID_SAVEFILE_ALLPARAMETER,
	MENUID_SAVEFILE_POLYLIB,
	//MENUID_SAVEFILE_BCMEDIUMLIST,
	//MENUID_SAVEFILE_DOMAIN,
	//MENUID_SAVEFILE_FACEBC,
	MENUID_SAVEDIR_OBJ,
	MENUID_SAVEDIR_STL,
	
	// View
	MENUID_VIEW_VIEWROT,
	MENUID_VIEW_VIEWTRANS,
	MENUID_VIEW_VIEWZOOM,
	MENUID_VIEW_VIEWROT_AXIS,
	MENUID_VIEW_PERSPECTIVE,
	MENUID_VIEW_ORTHO,
	MENUID_VIEW_COORDINATEAXIS,
	MENUID_VIEW_CENTERCROSS,  
	MENUID_VIEW_TREEVIEW,     
	MENUID_VIEW_MODE_WIRE,
	MENUID_VIEW_MODE_IDWIRE,
	MENUID_VIEW_MODE_ID,
	MENUID_VIEW_MODE_DOMAIN_BOX,
	MENUID_VIEW_MODE_DOMAIN_BOXWIRE,
	MENUID_VIEW_SLICECONTROL,
	MENUID_VIEW_MODE_SLICECONTROL_FAST,
	MENUID_VIEW_MODE_SLICECONTROL_DETAIL,
	MENUID_VIEW_MODE_MESH_GRID,
	MENUID_VIEW_VIEWALL,
	
	// Selection
	MENUID_SELECTION_PICK,
	MENUID_SELECTION_RECT,
	MENUID_SELECTION_POLY,
	MENUID_SELECTION_SUBD,
	MENUID_SELECTION_SETTING_BCMEDIUM,
	MENUID_SELECTION_SETTING_BCMEDIUM2,
	MENUID_SELECTION_SETTING_SUBDOMAIN,
	MENUID_SELECTION_FOCUS,
	MENUID_SELECTION_NONE,
	MENUID_SELECTION_RESETDOMAINCLIP,

	// Domain
	MENUID_DOMAIN_CREATE_CART_DOMAIN,
	MENUID_DOMAIN_CREATE_BCM_DOMAIN,
	MENUID_DOMAIN_DOMAIN_SETTING,
	MENUID_DOMAIN_DELETE_DOMAIN,
	MENUID_DOMAIN_OUTERBC_SETTING,

	// Mesh
	MENUID_MESH_DELETE,

	// Setting
	MENUID_SETTING_BCMEDIUM_LIST,
	MENUID_SETTING_BCMEDIUM_LIST2,	
	MENUID_SETTING_COORDINATE_LIST,
	MENUID_SETTING_BACKCOLOR,	
	MENUID_EXIT,
	MENUID_VERSION,

	// Tools
	MENUID_TOOLS_VOXEL_CARTESIAN_SETTING,
	MENUID_TOOLS_VOXEL_CARTESIAN_DELETE,
	MENUID_TOOLS_VOXEL_CARTESIAN_IMPORT,// for SD

	// Invisible
	MENUID_INVISIBLE_ZOOMIN2,
	MENUID_INVISIBLE_ZOOMIN5,
	MENUID_INVISIBLE_ZOOMOUT2,
	MENUID_INVISIBLE_ZOOMOUT5,

	// Tree Menu
	MENUID_TREE_DELETE,

};

#define BIT_CTRL_KEY		(0x0001)
#define BIT_ALT_KEY			(0x0002)
#define BIT_SHIFT_KEY       (0x0004)

namespace  {
	enum APP_KEY {
        BACKSPACE_KEY = 8,
		DELETE_KEY = 127,
		SHIFT_KEY = 306,
		ALT_KEY   = 307,
		CTRL_KEY  = 308,
#if _WIN32
		WIN_KEY   = 394
#else
		WIN_KEY   = 396
#endif
	};
}

MainLogic::MainLogic(const std::vector<std::string>& argv) : BaseLogic()
{
	m_argv.insert(m_argv.end(),argv.begin(),argv.end());
	m_core  = vxnew fxgenCore();
	m_exit  = false;
	m_vctrl = vxnew ViewController();
	m_gui   = vxnew GUIController(100,100, m_vctrl, m_core);
	m_core->Init(m_gui);
	
	m_dragging = m_rightdragging = false;
	m_key_ctrl = m_key_alt = m_key_shift = false;

	m_slideMode	= SliceController::MODE_FAST;
}

MainLogic::~MainLogic()
{
	Deinit();
}

#ifdef __APPLE__
#define CTRLKEYSTR "Cmd"
#else
#define CTRLKEYSTR "Ctrl"
#endif

#define ALT_KEYSTR "Alt"
#define SHIFT_KEYSTR "Shift"

#define N_FILE "File (F)"
#define N_VIEW "View (V)"
#define N_SELECTION "Selection (L)"
#define N_DOMAIN "Domain (D)"
#define N_MESH "Mesh (M)"
#define N_SETTING "Setting (S)"
#define N_VOXELTOOLS "VoxelTools (T)"
#define N_HELP "Help (H)"
#define N_F_IMPORT "Import [I]"
#define N_F_EXPORT "Export [E]"
#define N_INVISIBLE "Invisible"
#define N_TREE "TreeMenu"

void MainLogic::Init()
{
	// File
	m_gui->AddMenu(N_FILE , MENUID_FILE, 'F', BIT_ALT_KEY);
	m_gui->AddSubMenu(N_FILE, MENUID_NEWSCENE,              "New Scene        [" CTRLKEYSTR "+N]", 'N', BIT_CTRL_KEY);
	m_gui->AddSubMenu(N_FILE, MENUID_OPENSCENE,             "Open Scene(.fxg) [" CTRLKEYSTR "+O]", 'O', BIT_CTRL_KEY);
	m_gui->AddSubMenu(N_FILE, MENUID_SAVESCENE,             "Save Scene(.fxg) [" CTRLKEYSTR "+S]", 'S', BIT_CTRL_KEY);
	m_gui->EnableMenu(N_FILE, MENUID_OPENSCENE, false);
	m_gui->EnableMenu(N_FILE, MENUID_SAVESCENE, false);
	m_gui->AddSubMenuSeparator(N_FILE);
	m_gui->AddSubMenuMenu(N_FILE, N_F_IMPORT,MENUID_F_IMPORT, 'I');
	m_gui->AddSubMenu(N_F_IMPORT, MENUID_IMPORT_SHAPEFILE,    "Import Shape(STL,OBJ)       [H]", 'H');
	m_gui->AddSubMenu(N_F_IMPORT, MENUID_IMPORT_SHAPEDIR,     "Import Shape Dir(STL,OBJ)   [J]", 'J');
	m_gui->AddSubMenu(N_F_IMPORT, MENUID_IMPORT_BCMEDIUMLIST, "Import BC/Medium List       [B]", 'B');
	m_gui->AddSubMenu(N_F_IMPORT, MENUID_IMPORT_DOMAIN,       "Import Domain(Cartesian,BCM)[U]", 'U');
	m_gui->AddSubMenu(N_F_IMPORT, MENUID_IMPORT_FACEBC,       "Import FaceBC");
	m_gui->AddSubMenu(N_F_IMPORT, MENUID_IMPORT_CELLIDMESH,   "Import CellID Mesh (Cartesian,BCM)");
	m_gui->AddSubMenu(N_F_IMPORT, MENUID_IMPORT_GEOMETRY_LIST, "Import Geometry List");

	m_gui->AddSubMenuSeparator(N_FILE);
	m_gui->AddSubMenuMenu(N_FILE, N_F_EXPORT,MENUID_F_EXPORT, 'E');
	m_gui->AddSubMenu(N_F_EXPORT, MENUID_SAVEFILE_OBJ,          "Export File Obj");
	m_gui->AddSubMenu(N_F_EXPORT, MENUID_SAVEFILE_STL,    "Export File STL[V]", 'V');

	m_gui->AddSubMenu(N_F_EXPORT, MENUID_SAVEFILE_ALLPARAMETER, "Export File All Parameter[X]", 'X');
	m_gui->AddSubMenu(N_F_EXPORT, MENUID_SAVEFILE_POLYLIB,		 "Export File Polylib");
	m_gui->AddSubMenu(N_F_EXPORT, MENUID_SAVEFILE_GEOMETRY_LIST, "Export Geometry List");
	//m_gui->AddSubMenu(N_F_EXPORT, MENUID_SAVEFILE_BCMEDIUMLIST, "Export File BC/Medium List");
	//m_gui->AddSubMenu(N_F_EXPORT, MENUID_SAVEFILE_DOMAIN,       "Export File Domain(Cartesian, BCM)");
	//m_gui->AddSubMenu(N_F_EXPORT, MENUID_SAVEFILE_FACEBC,       "Export File FaceBC");
	m_gui->AddSubMenuSeparator(N_F_EXPORT);
	m_gui->AddSubMenu(N_F_EXPORT, MENUID_SAVEDIR_OBJ,           "Export Dir Obj");
	m_gui->AddSubMenu(N_F_EXPORT, MENUID_SAVEDIR_STL,     "Export Dir STL[Y]", 'Y');
	m_gui->AddSubMenuSeparator(N_FILE);
	m_gui->AddSubMenu(N_FILE, MENUID_EXIT,                  "Exit  [" CTRLKEYSTR "+Q]", 'Q', BIT_CTRL_KEY);
	
	// View
	m_gui->AddMenu(N_VIEW,MENUID_VIEW, 'V', BIT_ALT_KEY);
	m_gui->AddSubCheckMenu(N_VIEW, MENUID_VIEW_VIEWTRANS,      "View Translation  [T]", 'T');
	m_gui->AddSubCheckMenu(N_VIEW, MENUID_VIEW_VIEWROT,        "View Rotation     [R]", 'R');
	m_gui->AddSubCheckMenu(N_VIEW, MENUID_VIEW_VIEWZOOM,       "View Zoom         [Z]", 'Z');
	m_gui->AddSubCheckMenu(N_VIEW, MENUID_VIEW_VIEWROT_AXIS,   "View Rotation Axis[" CTRLKEYSTR " + R]", 'R', BIT_CTRL_KEY);
	m_gui->AddSubMenuSeparator(N_VIEW);
	m_gui->AddSubCheckMenu(N_VIEW, MENUID_VIEW_PERSPECTIVE,    "Perspective View  [P]", 'P');
	m_gui->AddSubMenuSeparator(N_VIEW);
	m_gui->AddSubCheckMenu(N_VIEW, MENUID_VIEW_COORDINATEAXIS, "Coordinate Axis");
	m_gui->AddSubCheckMenu(N_VIEW, MENUID_VIEW_CENTERCROSS,    "Center Cross      [C]", 'C');
	m_gui->AddSubCheckMenu(N_VIEW, MENUID_VIEW_TREEVIEW,       "Tree View");
	m_gui->AddSubMenuSeparator(N_VIEW);
	m_gui->AddSubCheckMenu(N_VIEW, MENUID_VIEW_MODE_ID,        "Polygon ID Mode  [1]", '1');
	m_gui->CheckMenu(N_VIEW, MENUID_VIEW_MODE_ID, !m_core->IsShowWire() && m_core->IsShowPolygon());
	m_gui->AddSubCheckMenu(N_VIEW, MENUID_VIEW_MODE_IDWIRE,    "Polygon ID+Wire Mode [2]", '2');
	m_gui->CheckMenu(N_VIEW, MENUID_VIEW_MODE_IDWIRE, m_core->IsShowWire() && m_core->IsShowPolygon());
	m_gui->AddSubCheckMenu(N_VIEW, MENUID_VIEW_MODE_WIRE,      "Polygon Wire Mode  [3]", '3');
	m_gui->CheckMenu(N_VIEW, MENUID_VIEW_MODE_WIRE, m_core->IsShowWire() && !m_core->IsShowPolygon());
	m_gui->AddSubMenuSeparator(N_VIEW);
	m_gui->AddSubCheckMenu(N_VIEW, MENUID_VIEW_MODE_DOMAIN_BOX ,"Domain Box Mode  [4]", '4');
	m_gui->CheckMenu(N_VIEW, MENUID_VIEW_MODE_DOMAIN_BOX, !m_core->IsShowDomainWire());
	m_gui->AddSubCheckMenu(N_VIEW, MENUID_VIEW_MODE_DOMAIN_BOXWIRE,"Domain Box+Wire Mode  [5]", '5');
	m_gui->CheckMenu(N_VIEW, MENUID_VIEW_MODE_DOMAIN_BOXWIRE, m_core->IsShowDomainWire());
	m_gui->AddSubMenuSeparator(N_VIEW);

	m_gui->AddSubCheckMenu(N_VIEW, MENUID_VIEW_SLICECONTROL, "Show Slice Control [" CTRLKEYSTR "+C]", 'C', BIT_CTRL_KEY);
	m_gui->AddSubMenuSeparator(N_VIEW);

	m_gui->AddSubCheckMenu(N_VIEW, MENUID_VIEW_MODE_SLICECONTROL_FAST,   "Slice Control Mode : Fast");
	m_gui->AddSubCheckMenu(N_VIEW, MENUID_VIEW_MODE_SLICECONTROL_DETAIL, "Slice Control Mode : Detail");
	m_gui->CheckMenu(N_VIEW,   MENUID_VIEW_MODE_SLICECONTROL_FAST,   true);
	m_gui->CheckMenu(N_VIEW, MENUID_VIEW_MODE_SLICECONTROL_DETAIL, false);
	m_gui->AddSubMenuSeparator(N_VIEW);

	m_gui->AddSubCheckMenu(N_VIEW, MENUID_VIEW_MODE_MESH_GRID, "Show Mesh Grid [G]", 'G');
	m_gui->CheckMenu(N_VIEW, MENUID_VIEW_MODE_MESH_GRID, false);

	m_gui->AddSubMenuSeparator(N_VIEW);
	m_gui->AddSubCheckMenu(N_VIEW, MENUID_VIEW_VIEWALL, "View All [A]", 'A');
	
	m_gui->AddMenu(N_SELECTION,MENUID_SELECTION, 'L', BIT_ALT_KEY);
	m_gui->AddSubCheckMenu(N_SELECTION, MENUID_SELECTION_PICK, "Pick Mode [S]", 'S');
	m_gui->AddSubCheckMenu(N_SELECTION, MENUID_SELECTION_RECT, "Rect Mode [W]", 'W');

	m_gui->AddSubMenuSeparator(N_SELECTION);
	m_gui->AddSubCheckMenu(N_SELECTION, MENUID_SELECTION_POLY, "Polygon Mode    [O]", 'O');
	m_gui->CheckMenu      (N_SELECTION, MENUID_SELECTION_POLY, true);
	m_gui->AddSubCheckMenu(N_SELECTION, MENUID_SELECTION_SUBD, "Sub Domain Mode [D]", 'D');
	m_gui->CheckMenu      (N_SELECTION, MENUID_SELECTION_SUBD, false);
	m_gui->AddSubMenuSeparator(N_SELECTION);
	
	m_gui->AddSubMenu(N_SELECTION, MENUID_SELECTION_SETTING_BCMEDIUM2, "Setting Polygon BC/Medium          [M]", 'M');
	m_gui->AddSubMenu(N_SELECTION, MENUID_SELECTION_SETTING_SUBDOMAIN, "Setting Sub Domain Active/Inactive [N]", 'N');
	m_gui->EnableMenu(N_SELECTION, MENUID_SELECTION_SETTING_SUBDOMAIN, false);
	m_gui->AddSubMenuSeparator(N_SELECTION);
	m_gui->AddSubMenu(N_SELECTION, MENUID_SELECTION_FOCUS, "Focus To Selection [F]", 'F');
	m_gui->AddSubMenuSeparator(N_SELECTION);
	m_gui->AddSubMenu(N_SELECTION, MENUID_SELECTION_NONE, "None [Q]", 'Q');
	m_gui->AddSubMenuSeparator(N_SELECTION);
	m_gui->AddSubMenu(N_SELECTION, MENUID_SELECTION_RESETDOMAINCLIP, "Reset Domain Clipping [K]", 'K');
	m_gui->EnableMenu(N_SELECTION, MENUID_SELECTION_RESETDOMAINCLIP, false);
	
	m_gui->AddMenu(N_DOMAIN,MENUID_DOMAIN, 'D', BIT_ALT_KEY);
	m_gui->AddSubMenu(N_DOMAIN, MENUID_DOMAIN_CREATE_CART_DOMAIN,  "Create New Cartesian domain");
	m_gui->AddSubMenu(N_DOMAIN, MENUID_DOMAIN_CREATE_BCM_DOMAIN,   "Create New BCM domain");
	//m_gui->EnableMenu(N_DOMAIN, MENUID_DOMAIN_CREATE_BCM_DOMAIN,   false);
	m_gui->AddSubMenuSeparator(N_DOMAIN);
	m_gui->AddSubMenu(N_DOMAIN, MENUID_DOMAIN_DOMAIN_SETTING,      "Edit Domain");
	m_gui->EnableMenu(N_DOMAIN, MENUID_DOMAIN_DOMAIN_SETTING,      false);
	m_gui->AddSubMenu(N_DOMAIN, MENUID_DOMAIN_OUTERBC_SETTING,     "Edit FaceBC");
	m_gui->EnableMenu(N_DOMAIN, MENUID_DOMAIN_OUTERBC_SETTING,     false);
	m_gui->AddSubMenuSeparator(N_DOMAIN);
	m_gui->AddSubMenu(N_DOMAIN, MENUID_DOMAIN_DELETE_DOMAIN,       "Delete Domain");

	m_gui->AddMenu(N_MESH,MENUID_MESH, 'M', BIT_ALT_KEY);
	m_gui->AddSubMenu(N_MESH, MENUID_MESH_DELETE, "Delete Mesh [" CTRLKEYSTR " + W]", 'W', BIT_CTRL_KEY);
	m_gui->EnableMenu(N_MESH, MENUID_MESH_DELETE, false);

	m_gui->AddMenu(N_SETTING,MENUID_SETTING, 'S', BIT_ALT_KEY);

	m_gui->AddSubMenu(N_SETTING, MENUID_SETTING_BCMEDIUM_LIST2,       "BC/Medium List Manager [L]", 'L');
	m_gui->AddSubMenu(N_SETTING, MENUID_SETTING_COORDINATE_LIST,		"Coordinate Viewer");
	m_gui->EnableMenu(N_SETTING, MENUID_SETTING_COORDINATE_LIST, false);
//	m_gui->AddSubMenu(N_SETTING, MENUID_SETTING_BACKCOLOR,       "Background Color [" CTRLKEYSTR " + " ALT_KEYSTR " +B]", 'B', BIT_CTRL_KEY|BIT_ALT_KEY);
    m_gui->AddSubMenu(N_SETTING, MENUID_SETTING_BACKCOLOR,       "Background Color");

	m_gui->AddMenu(N_VOXELTOOLS,MENUID_VOXELTOOLS, 'T', BIT_ALT_KEY);
	m_gui->AddSubMenu(N_VOXELTOOLS, MENUID_TOOLS_VOXEL_CARTESIAN_IMPORT,	"Import Dfi File [" CTRLKEYSTR " + X]", 'X', BIT_CTRL_KEY);
	m_gui->AddSubMenu(N_VOXELTOOLS, MENUID_TOOLS_VOXEL_CARTESIAN_SETTING,	"Edit                   [" CTRLKEYSTR " + Y]", 'Y', BIT_CTRL_KEY);
	m_gui->AddSubMenu(N_VOXELTOOLS, MENUID_TOOLS_VOXEL_CARTESIAN_DELETE,	"Delete");

	// Help
	m_gui->AddMenu(N_HELP,MENUID_HELP, 'H', BIT_ALT_KEY);
	m_gui->AddSubMenu(N_HELP, MENUID_VERSION, "About FXgen" );
	
	// Invisible
	m_gui->AddMenu(N_INVISIBLE, MENUID_INVISIBLE, 0, 0, false);
	m_gui->AddSubMenu(N_INVISIBLE, MENUID_INVISIBLE_ZOOMIN2,  "Zoom in  x2",  ',', 0);
    m_gui->AddSubMenu(N_INVISIBLE, MENUID_INVISIBLE_ZOOMOUT2, "Zoom out x2",  '.', 0);
    m_gui->AddSubMenu(N_INVISIBLE, MENUID_INVISIBLE_ZOOMIN5,  "Zoom in  x5",  ',', BIT_SHIFT_KEY);
    m_gui->AddSubMenu(N_INVISIBLE, MENUID_INVISIBLE_ZOOMOUT5, "Zoom out x5",  '.', BIT_SHIFT_KEY);
	// For Mac JIS keyboard
    m_gui->AddSubMenu(N_INVISIBLE, MENUID_INVISIBLE_ZOOMIN5,  "Zoom in  x5",  '<', BIT_SHIFT_KEY);
    m_gui->AddSubMenu(N_INVISIBLE, MENUID_INVISIBLE_ZOOMOUT5, "Zoom out x5",  '>', BIT_SHIFT_KEY);
	
	// Tree Menu
	m_gui->AddMenu(N_TREE, MENUID_TREE, 0, 0, false);
	m_gui->AddSubMenu(N_TREE, MENUID_TREE_DELETE, "Delete" );
	m_gui->AddMenuFunc(MainLogic::OnMenu_, this);
	
	updateMenu();
	
	// for Splash
	UI::vxWindow* mainwin = UI::vxWindow::GetTopWindow();
	mainwin->StartIdleloop();


	// arg action
	if(hasArgv()){
		actionArgv();
	}
}

bool MainLogic::hasArgv()const
{
	return m_argv.size()>1;
}

void MainLogic::actionArgv()
{
	std::string geo_tp_path = m_argv[1];
	if(!wxFile::Exists(geo_tp_path)){
		VXLogE("GeometryList file no exist.%s\n",geo_tp_path.c_str());
		return;
	}
	importGeometryList(geo_tp_path);

}


void MainLogic::Deinit()
{
	if (m_core)
	{
		m_core->Deinit();
		vxdelete(m_core);
		m_core = 0;
		vxdelete(m_vctrl);
		m_vctrl = 0;
		vxdelete(m_gui);
		m_gui = 0;
	}
}

void MainLogic::OnResize(u32 w, u32 h)
{
	if (m_core)
		m_core->Resize(w, h);
	if (m_vctrl)
		m_vctrl->Resize(w,h);
	if (m_gui)
		m_gui->Resize(w, h);

	m_width  = w;
	m_height = h;
}

bool MainLogic::OnIdle()
{
	if (m_vctrl->IsAnimating() || m_gui->IsAnimating()) {
		Draw();
		return true;
	}
	return false;
}

void MainLogic::Draw()
{
	if (!m_core || !m_gui)
		return;

	const f64 tm = VX::GetTimeCount();
	static f64 stm = tm;
	static s32 cnt = 0;
	cnt++;
	VX::GraphicsProfiler& p = VX::GraphicsProfiler::GetInstance();
	if (tm - stm > 1.0) {
		m_gui->SetFPS(cnt);
		cnt = 0;
		stm = tm;
		m_gui->SetMemory(static_cast<unsigned long int>(VX::Memory::AllocedSize() + VX::Memory::AllocedSize_EXT()));
		m_gui->SetVRAM(p.GetVramSize());
		m_gui->SetTriNum(p.GetDrawTriCount());
	}
	p.ClearDrawInfo();
	
	m_core->SetViewMatrix(m_vctrl->GetViewMatrix());
	m_core->SetProjMatrix(m_vctrl->GetProjMatrix());
	m_core->Draw();
	m_gui->Draw();
}

void MainLogic::OnKeyDown(s32 key)
{

	s32 optkey = (m_key_ctrl?BIT_CTRL_KEY:0)|(m_key_alt?BIT_ALT_KEY:0)|(m_key_shift?BIT_SHIFT_KEY:0);

    VXLogD("MainLogic::OnKeyDown key[%d] ,optkey[%d]\n",key,optkey);
    VXLogD("MainLogic::OnKeyDown before. m_key_shift[%d] ,m_key_alt[%d],m_key_ctrl[%d]\n",m_key_shift,m_key_alt,m_key_ctrl);
    
	m_gui->KeyDown(key,optkey);

	if (key == SHIFT_KEY)
		m_key_shift = true;
	else if (key == ALT_KEY)
		m_key_alt = true;
	else if (key == CTRL_KEY)
		m_key_ctrl = true;
	else {
		m_key = key;
		m_key_ctrl = false;
	}
    
    VXLogD("MainLogic::OnKeyDown after. m_key_shift[%d] ,m_key_alt[%d],m_key_ctrl[%d]\n",m_key_shift,m_key_alt,m_key_ctrl);
    
}

void MainLogic::OnKeyUp(s32 key)
{
	m_gui->KeyUp(key);
	if (key == SHIFT_KEY) 
		m_key_shift = false;
	else if (key == ALT_KEY)
		m_key_alt = false;
	else if (key == CTRL_KEY) 
		m_key_ctrl = false;
	else
		m_key = 0;
}
 
void MainLogic::OnMouseLeftDown  (s32 x, s32 y)
{
	if (m_gui && m_gui->MouseDown(x, y))
		return; // Hit to GUI

	const ViewController::OPMode mode =  m_vctrl->GetMode();
	
	if (m_key_alt) {
		if (m_key_ctrl)       m_vctrl->MouseZoomDown((f32)(x), (f32)(y));
		else if (m_key_shift) m_vctrl->MouseTransDown((f32)(x), (f32)(y));
		else                  m_vctrl->MouseRotDown((f32)(x), (f32)(y));
	} else {
		if (mode == ViewController::SELECT_POINT) {
			m_dragging = true;
			m_core->SelectionPick(x, y, !m_key_ctrl);

			NotifySelectionPick();

		} else if (mode == ViewController::SELECT_RECT) {
			m_dragging = true;
			m_selecting_sx = x;
			m_selecting_ex = x;
			m_selecting_sy = y;
			m_selecting_ey = y;
			m_core->StartSelectionRect(m_selecting_sx, m_selecting_sy, m_selecting_ex, m_selecting_ey);
		} else if (mode == ViewController::VIEW_ROT) { // not good
			m_vctrl->MouseRotDown((f32)(x), (f32)(y));
		} else if (mode == ViewController::VIEW_TRANS) {
			m_vctrl->MouseTransDown((f32)(x), (f32)(y));
		} else if (mode == ViewController::VIEW_ZOOM) {
			m_vctrl->MouseZoomDown((f32)(x), (f32)(y));
		} else if (mode == ViewController::VIEW_ROT_AXIS) {
			m_vctrl->MouseRotAxisDown((f32)(x), (f32)(y));
		}
	}
}
void MainLogic::OnMouseRightDown (s32 x, s32 y){
	if (m_gui && m_gui->MouseRightDown(x, y))
		return; // Hit to GUI
	
	if (m_key_alt) {
		m_vctrl->MouseZoomDown((f32)(x), (f32)(y));
	} else {
		const ViewController::OPMode mode =  m_vctrl->GetMode();
		if (mode == ViewController::SELECT_POINT) {
			m_dragging = m_rightdragging = true;
			m_core->SelectionPick(x, y, false);

			// show pick info on GUI
			m_gui->SetMessage(m_core->GetPickInformationStr().c_str());

		} else if (mode == ViewController::SELECT_RECT) {
			m_dragging = m_rightdragging = true;
			m_selecting_sx = x;
			m_selecting_ex = x;
			m_selecting_sy = y;
			m_selecting_ey = y;
			m_core->StartSelectionRect(m_selecting_sx, m_selecting_sy, m_selecting_ex, m_selecting_ey);
		}
	}
}
void MainLogic::OnMouseMiddleDown(s32 x, s32 y)
{
	if (m_gui && m_gui->MouseDown(x, y))
		return; // Hit to GUI
	
	if (m_key_alt) {
		m_vctrl->MouseTransDown((f32)(x), (f32)(y));
	}
}
void MainLogic::OnMouseLeftUp    (s32 x, s32 y)
{
	if (m_gui && m_gui->MouseUp(x, y)){
		updateMenu();
		UI::vxWindow* win = UI::vxWindow::GetTopWindow();
		win->StartIdleloop();
		return;
	}
	m_vctrl->MouseRotUp((f32)(x), (f32)(y));
	m_vctrl->MouseRotAxisUp((f32)(x), (f32)(y));
	m_vctrl->MouseTransUp((f32)(x), (f32)(y));
	m_vctrl->MouseZoomUp((f32)(x), (f32)(y));

	m_core->EndSelectionPick(x,y);
	if (m_vctrl->GetMode() == ViewController::SELECT_RECT && m_dragging)
		m_core->EndSelectionRect( !m_key_ctrl );

	m_dragging = m_rightdragging = false;
}
void MainLogic::OnMouseRightUp   (s32 x, s32 y)
{
	if (m_gui && m_gui->MouseRightUp(x, y))
	{
		updateMenu();
		UI::vxWindow* win = UI::vxWindow::GetTopWindow();
		win->StartIdleloop();
		return;
	}
	m_vctrl->MouseZoomUp((f32)(x), (f32)(y));
	
	if (m_vctrl->GetMode() == ViewController::SELECT_RECT && m_dragging)
		m_core->EndSelectionRect(false);
	
	m_dragging = m_rightdragging = false;
}

void MainLogic::OnMouseMiddleUp  (s32 x, s32 y)
{
	if (m_gui && m_gui->MouseUp(x, y))
	{
		updateMenu();
		UI::vxWindow* win = UI::vxWindow::GetTopWindow();
		win->StartIdleloop();
		return;
	}
	m_vctrl->MouseTransUp((f32)(x), (f32)(y));
}

void MainLogic::OnMouseMove(s32 x, s32 y)
{
	const ViewController::OPMode mode =  m_vctrl->GetMode();
	if (!m_dragging && (m_key_alt || mode > 1)) {
		m_vctrl->MouseMove((f32)(x), (f32)(y));
	} else if (m_dragging && !m_key_alt){
		if (mode == ViewController::SELECT_POINT) {
			m_core->SelectionPick(x, y, m_rightdragging ? false : !m_key_ctrl );
		} else if (mode == ViewController::SELECT_RECT) {
			m_selecting_ex = x;
			m_selecting_ey = y;
			m_core->StartSelectionRect(m_selecting_sx, m_selecting_sy, m_selecting_ex, m_selecting_ey);
		}
	}// else {
		m_gui->MouseMove(x, y);
	//}
}

void MainLogic::updateMenu()
{
	//UI::vxWindow* win = UI::vxWindow::GetTopWindow();
	GUIController* win = m_gui;
	if (!win)
		return;
	
	m_gui->CheckMenu(N_VIEW, MENUID_VIEW_TREEVIEW, 	m_gui->IsVisibleTree());
	m_gui->CheckMenu(N_VIEW, MENUID_VIEW_COORDINATEAXIS, m_core->IsShowAxis());
	m_gui->CheckMenu(N_VIEW, MENUID_VIEW_CENTERCROSS, m_core->IsShowCross());

	m_gui->CheckMenu(N_VIEW,      MENUID_VIEW_VIEWTRANS,    false);
	m_gui->CheckMenu(N_VIEW,      MENUID_VIEW_VIEWROT,      false);
	m_gui->CheckMenu(N_VIEW,      MENUID_VIEW_VIEWZOOM,     false);
	m_gui->CheckMenu(N_VIEW,      MENUID_VIEW_VIEWROT_AXIS, false);
	m_gui->CheckMenu(N_SELECTION, MENUID_SELECTION_PICK,    false);
	m_gui->CheckMenu(N_SELECTION, MENUID_SELECTION_RECT,    false);
	switch (m_vctrl->GetMode()) {
		case ViewController::VIEW_TRANS:    m_gui->CheckMenu(N_VIEW,      MENUID_VIEW_VIEWTRANS,    true); break;
		case ViewController::VIEW_ROT:      m_gui->CheckMenu(N_VIEW,      MENUID_VIEW_VIEWROT,      true); break;
		case ViewController::VIEW_ZOOM:     m_gui->CheckMenu(N_VIEW,      MENUID_VIEW_VIEWZOOM,     true); break;
		case ViewController::VIEW_ROT_AXIS: m_gui->CheckMenu(N_VIEW,      MENUID_VIEW_VIEWROT_AXIS, true); break;
		case ViewController::SELECT_POINT:  m_gui->CheckMenu(N_SELECTION, MENUID_SELECTION_PICK,    true); break;
		case ViewController::SELECT_RECT:   m_gui->CheckMenu(N_SELECTION, MENUID_SELECTION_RECT,    true); break;
		default:
			break;
	}
	
	if (m_vctrl->GetViewMode() == ViewController::VIEW_PERSPECTIVE) {
		m_gui->CheckMenu(N_VIEW, MENUID_VIEW_PERSPECTIVE, true);
		m_gui->CheckMenu(N_VIEW, MENUID_VIEW_ORTHO, false);
	} else {
		m_gui->CheckMenu(N_VIEW, MENUID_VIEW_PERSPECTIVE, false);
		m_gui->CheckMenu(N_VIEW, MENUID_VIEW_ORTHO, true);
	}
}

void MainLogic::initMenu()
{
	UI::vxWindow* mainwin = UI::vxWindow::GetTopWindow();
	// unshow domain dialog
	UI::DomainCartesianDlg(mainwin, NULL, false);
	UI::Domain_BCM_Dlg(mainwin, NULL, NULL, false);
	UI::VoxelCartesian_Dlg(mainwin, NULL, NULL, NULL,false);

	// Enable Create Cartesian / BCM Menu.
	m_gui->EnableMenu(N_DOMAIN, MENUID_DOMAIN_CREATE_CART_DOMAIN, true);
	m_gui->EnableMenu(N_DOMAIN, MENUID_DOMAIN_CREATE_BCM_DOMAIN, true);
	
	// Disable Edit Cartesian Menu.
	m_gui->EnableMenu(N_DOMAIN, MENUID_DOMAIN_DOMAIN_SETTING, false);

	// Disable Edit OuterBC Menu.
	m_gui->EnableMenu(N_DOMAIN, MENUID_DOMAIN_OUTERBC_SETTING, false);

}
void MainLogic::OnMenu(s32 menu_id)
{
    VXLogD("MainLogic::OnMenu[%d]\n",menu_id);
    
	//alt �L�[������Ȃ���̘A�����삪����̂ŏ�Ԃ�L�����Z�����Ȃ�
    //m_key_ctrl = m_key_alt = m_key_shift = false;
	UI::vxWindow* win = UI::vxWindow::GetTopWindow();
    if (!win){
        VXLogD("GetTopWindow is Null\n");
        return;
    }
    VXLogD("MainLogic::OnMenu start\n");
    
	if( menu_id == MENUID_FILE){
		m_gui->PressTopMenu(N_FILE);
	}else if( menu_id == MENUID_F_IMPORT){
		m_gui->PressTopMenu(N_F_IMPORT);
	}else if( menu_id == MENUID_F_EXPORT){
		m_gui->PressTopMenu(N_F_EXPORT);
	}else if( menu_id == MENUID_VIEW){
		m_gui->PressTopMenu(N_VIEW);
	}else if( menu_id == MENUID_SELECTION){
		m_gui->PressTopMenu(N_SELECTION);
	}else if( menu_id == MENUID_DOMAIN){
		m_gui->PressTopMenu(N_DOMAIN);
	}else if( menu_id == MENUID_MESH){
		m_gui->PressTopMenu(N_MESH);
	}else if( menu_id == MENUID_SETTING){
		m_gui->PressTopMenu(N_SETTING);
	}else if( menu_id == MENUID_VOXELTOOLS){
		m_gui->PressTopMenu(N_VOXELTOOLS);
	}else if( menu_id == MENUID_HELP){
		m_gui->PressTopMenu(N_HELP);

	}else if (menu_id == MENUID_NEWSCENE)
	{
		if (!UI::ConfirmDlg("New scene?"))
			return;
		
		m_core->NewScene();

		m_gui->UpdateTree(true);
		std::string msg = "New scene";
		m_gui->SetMessage(msg.c_str());
		initMenu();
		
	}
	else if (menu_id == MENUID_IMPORT_SHAPEFILE)
	{
		std::vector<std::string> files;
		std::string filename;
		if (!UI::MultipleFileOpenDlg(filename, 
			"Supported Format|*.obj;*.stl;*.sla;*.slb;*.stla;*.stlb;|Alias Wavefront OBJ(obj)|*.obj|STL Ascii/Binary(stl,sla,slb,stla,stlb)|*.stl;*.sla;*.slb;*.stla;*.stlb",
			files))
			return;

		m_core->LoadFiles(files);
		m_vctrl->Reset(m_core->GetRoot());
		m_core->SetCross(m_vctrl->GetRotCenter(), m_vctrl->GetRadius());
		win->StartIdleloop();
		m_gui->UpdateTree(true);
		std::string msg = "Loaded: " + filename;
		m_gui->SetMessage(msg.c_str());
		initMenu();
		
		//Enable coordinate viewer setting.
		m_gui->EnableMenu(N_SETTING, MENUID_SETTING_COORDINATE_LIST, true);
	}
 	else if (menu_id == MENUID_IMPORT_SHAPEDIR)
	{
		std::string dirname;
		std::string defaultpath;
		if (!UI::DirOpenDlg(dirname,defaultpath))
			return;
		
		m_core->LoadDir(dirname.c_str());
		m_vctrl->Reset(m_core->GetRoot());
		m_core->SetCross(m_vctrl->GetRotCenter(),m_vctrl->GetRadius());
		win->StartIdleloop();
		m_gui->UpdateTree(true);
		std::string msg = "Loaded Dir: " + dirname;
		m_gui->SetMessage(msg.c_str());
		initMenu();

		//Enable coordinate viewer setting.
		m_gui->EnableMenu(N_SETTING, MENUID_SETTING_COORDINATE_LIST, true);
	}

	else if (menu_id == MENUID_IMPORT_BCMEDIUMLIST)
	{
		std::string filename;
		if( !UI::FileOpenDlg(filename, "BC/Medium Table(*.tp)|*.tp") )
			return;
			
		if (!m_core->LoadBCMedium(filename.c_str())) {
			UI::MessageDlg("Failed to Load BC/Medium Table.");
			return;
		}

		std::string msg = "Loaded: " + filename;
		m_gui->SetMessage(msg.c_str());
	}

	else if (menu_id == MENUID_IMPORT_DOMAIN)
	{
		if (!m_core->GetRoot()) {
			UI::MessageDlg("It is necessary to import a shape data, before importing domain.");
			m_gui->SetMessage("Failed to Load Domain.");
			return;
		}
		std::string filename;
		if( UI::FileOpenDlg(filename, "Domain(*.tp, *.bcm, *.oct)|*.tp;*.bcm;*.oct")){
			std::string msg = "Load: " + filename + " (now loading...)";
			m_gui->SetMessage(msg.c_str());
			UI::vxWindow::GetTopWindow()->Draw();

			if(!m_core->LoadDomain(filename.c_str())) {
				UI::MessageDlg("Failed to Load Domain.");
				m_gui->SetMessage("Failed to Load Domain.");
				return;
			}
		}else{
			return;
		}

		std::string msg = "Loaded: " + filename;
		m_gui->SetMessage(msg.c_str());

		m_gui->UpdateTree(false);
		m_vctrl->Reset(m_core->GetRoot());
		m_core->SetCross(m_vctrl->GetRotCenter(), m_vctrl->GetRadius());

		// unshow domain dialog
		UI::DomainCartesianDlg(win, NULL, false);
		UI::Domain_BCM_Dlg(win, NULL, NULL, false);
		UI::SliceControllerDlg(win, NULL, false);

		// Disable Create Cartesian / BCM Menu.
		m_gui->EnableMenu(N_DOMAIN, MENUID_DOMAIN_CREATE_CART_DOMAIN, false);
		m_gui->EnableMenu(N_DOMAIN, MENUID_DOMAIN_CREATE_BCM_DOMAIN, false);
		m_gui->EnableMenu(N_DOMAIN, MENUID_DOMAIN_DELETE_DOMAIN, true);
		
		// Enable Edit Cartesian Menu.
		m_gui->EnableMenu(N_DOMAIN, MENUID_DOMAIN_DOMAIN_SETTING, true);
		m_gui->EnableMenu(N_DOMAIN, MENUID_DOMAIN_OUTERBC_SETTING, true);

		m_gui->EnableMenu(N_MESH, MENUID_MESH_DELETE, false);
	}
		
	else if (menu_id == MENUID_IMPORT_FACEBC)
	{
		std::string filename;
		if( UI::FileOpenDlg(filename, "FaceBC(*.tp)|*.tp")){
			if(!m_core->LoadFaceBC(filename.c_str())){
				UI::MessageDlg("Failed to Load FaceBC.");
				m_gui->SetMessage("Failed to Load FaceBC.");
				return;
			}
		}else{
			return;
		}

		std::string msg = "Loaded: " + filename;
		m_gui->SetMessage(msg.c_str());
	}

 	else if (menu_id == MENUID_IMPORT_GEOMETRY_LIST)
	{

		std::string filename;
		if( !UI::FileOpenDlg(filename, "Geometry List(*.tp)|*.tp")) {
			return;
		}

		importGeometryList(filename);
		
	}
	
	else if (menu_id == MENUID_IMPORT_CELLIDMESH)
	{
		std::string filename;
		if( UI::FileOpenDlg(filename, "CellID Mesh|*.dfi;*.bcm")) {
			std::string msg = "Load: " + filename + " (now loading...)";
			m_gui->SetMessage(msg.c_str());
			UI::vxWindow::GetTopWindow()->Draw();

			if(!m_core->LoadCellIDMesh(filename.c_str())){
				UI::MessageDlg("Failed to Load CellID Mesh.");
				msg = "Failed to Load File: " + filename;
				m_gui->SetMessage(msg.c_str());
				return;
			}
			m_vctrl->Reset(m_core->GetRoot());
			m_core->SetCross(m_vctrl->GetRotCenter(), m_vctrl->GetRadius());
			win->StartIdleloop();
			m_gui->UpdateTree(false);
			msg = "Loaded: " + filename;
			m_gui->SetMessage(msg.c_str());

		}else{
			return;
		}

		initMenu();
		
		std::string msg = "Loaded: " + filename;
		m_gui->SetMessage(msg.c_str());

		UI::DomainCartesianDlg(win, NULL, false);
		UI::Domain_BCM_Dlg(win, NULL, NULL, false);
		UI::SliceControllerDlg(win, NULL, false);
		m_gui->EnableMenu(N_MESH, MENUID_MESH_DELETE, true);
		m_gui->EnableMenu(N_DOMAIN, MENUID_DOMAIN_CREATE_CART_DOMAIN, false);
		m_gui->EnableMenu(N_DOMAIN, MENUID_DOMAIN_CREATE_BCM_DOMAIN, false);
		m_gui->EnableMenu(N_DOMAIN, MENUID_DOMAIN_DELETE_DOMAIN, false);
	}

	else if (menu_id == MENUID_SAVEFILE_OBJ)
	{
		std::string filename;
		if (UI::FileSaveDlg(filename, "Alias Wavefront OBJ(*.obj)|*.obj"))
		{
			if (!m_core->SaveFile(filename.c_str(), "OBJ")){
				UI::MessageDlg("Failed to save file.");
			}else{
				std::string msg = "Saved OBJ File: " + filename;
				m_gui->SetMessage(msg.c_str());
			}
		}
	}
	else if (menu_id == MENUID_SAVEFILE_STL)
	{
		UI::ExportSTLDlg(win, true, false, m_core, m_gui);
	}

	else if (menu_id == MENUID_SAVEFILE_POLYLIB)
	{
		std::string filename;
		if( UI::FileSaveDlg(filename, "Polylib (*.tp)|*.tp"))
		{
			if(!m_core->SavePolylibFile(filename.c_str())){
				UI::MessageDlg("Failed to save Polylib.");
				return;
			}
		}
		std::string msg = "Save Polylib File: " + filename;
		m_gui->SetMessage(msg.c_str());
	}

	else if (menu_id == MENUID_SAVEFILE_ALLPARAMETER)
	{
		std::string filename;
		if( UI::FileSaveDlg(filename, "All Parameter(*.tp)|*.tp"))
		{
			VXLogI("select=>(%s)\n", filename.c_str());

			if(!m_core->SaveAllParameter(filename.c_str())){
				UI::MessageDlg("Failed to save All Parameter.");
				return;
			}
		}
		std::string msg = "Save All Parameter File: " + filename;
		m_gui->SetMessage(msg.c_str());
	}

	else if (menu_id == MENUID_SAVEFILE_GEOMETRY_LIST)
	{
		std::string filename;
		if( UI::FileSaveDlg(filename, "Export Geometry List(*.tp)|*.tp"))
		{
			VXLogI("select=>(%s)\n", filename.c_str());

			if(!m_core->SaveGeometryList(filename.c_str())){
				UI::MessageDlg("Failed to save Geometry List.");
				return;
			}
		}
		std::string msg = "Save Geometry List File: " + filename;
		m_gui->SetMessage(msg.c_str());
	}

	/*
	else if (menu_id == MENUID_SAVEFILE_BCMEDIUMLIST)
	{
		std::string filename;
		if (UI::FileSaveDlg(filename, "BC/Medium List(*.tp)|*.tp"))
		{
			if (!m_core->SaveBCMedium(filename.c_str())) {
				UI::MessageDlg("Failed to save BC/Medium List.");
				return;
			}
		}
		std::string msg = "Saved BC/Medium File: " + filename;
		m_gui->SetMessage(msg.c_str());
	}
	else if (menu_id == MENUID_SAVEFILE_DOMAIN)
	{
		std::string filename;
		if (UI::FileSaveDlg(filename, "Domain(*.tp)|*.tp"))
		{
			if (!m_core->SaveDomain(filename.c_str())) {
				UI::MessageDlg("Failed to save Domain");
				return;
			}
		}
		std::string msg = "Saved Domain File: " + filename;
		m_gui->SetMessage(msg.c_str());
	}

	else if (menu_id == MENUID_SAVEFILE_FACEBC)
	{
		std::string filename;
		if( UI::FileSaveDlg(filename, "FaceBC(*.tp)|*.tp")){
			if(!m_core->SaveFaceBC(filename.c_str())){
				UI::MessageDlg("Failed to save FaceBC.");
				return;
			}
		}
		std::string msg = "Saved FaceBC File: " + filename;
		m_gui->SetMessage(msg.c_str());
	}
	*/
	else if (menu_id == MENUID_SAVEDIR_OBJ)
	{
		std::string dirname;
		if (UI::DirSaveDlg(dirname,""))
		{
			if (!m_core->SaveDir(dirname.c_str(), "OBJ")) {
				UI::MessageDlg("Failed to save file.");
			}else{
				std::string msg = "Saved Dir[OBJ]: " + dirname;
				m_gui->SetMessage(msg.c_str());
			}
		}
	}
	else if (menu_id == MENUID_SAVEDIR_STL)
	{
		UI::ExportSTLDlg(win, true, true, m_core, m_gui);
	}


	else if (menu_id == MENUID_DOMAIN_CREATE_CART_DOMAIN)
	{
		// Create New Cartesian Domain
		if( !m_core->CreateNewCartesianDomain() ){
			UI::MessageDlg("Failed to create new cartesian domain.\n");
			return;
		}

		DomainEditor* editor = m_core->GetCurrentDomainEditor();
		if(editor->GetType() == DomainEditor::DOMAIN_EDITOR_CARTESIAN){
		 	UI::DomainCartesianDlg(win, dynamic_cast<DomainCartesianEditor*>(editor));
		}else{
			UI::MessageDlg("Failed to create new cartesian domain.\n");
			return;
		}

		if (m_gui)
			m_gui->UpdateTree(false);

		// Disable Create Cartesian Menu.
		m_gui->EnableMenu(N_DOMAIN, MENUID_DOMAIN_CREATE_CART_DOMAIN, false);
		m_gui->EnableMenu(N_DOMAIN, MENUID_DOMAIN_CREATE_BCM_DOMAIN, false);
		m_gui->EnableMenu(N_DOMAIN, MENUID_DOMAIN_DELETE_DOMAIN, true);
		
		// Enable Edit Cartesian Menu.
		m_gui->EnableMenu(N_DOMAIN, MENUID_DOMAIN_DOMAIN_SETTING,  true);
		m_gui->EnableMenu(N_DOMAIN, MENUID_DOMAIN_OUTERBC_SETTING, true);

		m_gui->EnableMenu(N_MESH, MENUID_MESH_DELETE, false);
	}

	else if (menu_id == MENUID_DOMAIN_CREATE_BCM_DOMAIN)
	{
		if( !m_core->CreateNewBCMDomain() ){
			UI::MessageDlg("Failed to create new BCM domain.\n");
			return;
		}

		DomainEditor* editor = m_core->GetCurrentDomainEditor();
		if(editor->GetType() == DomainEditor::DOMAIN_EDITOR_BCM){
			UI::Domain_BCM_Dlg(win, dynamic_cast<DomainBCMEditor*>(editor), m_gui);
		}else{
			UI::MessageDlg("Failed to create new BCM domain.\n");
			return;
		}
		
		if (m_gui)
			m_gui->UpdateTree(false);

		// Disable Create Cartesian Menu.
		m_gui->EnableMenu(N_DOMAIN, MENUID_DOMAIN_CREATE_CART_DOMAIN, false);
		m_gui->EnableMenu(N_DOMAIN, MENUID_DOMAIN_CREATE_BCM_DOMAIN, false);
		m_gui->EnableMenu(N_DOMAIN, MENUID_DOMAIN_DELETE_DOMAIN, true);
		
		// Enable Edit Cartesian Menu.
		m_gui->EnableMenu(N_DOMAIN, MENUID_DOMAIN_DOMAIN_SETTING,  true);
		m_gui->EnableMenu(N_DOMAIN, MENUID_DOMAIN_OUTERBC_SETTING, true);

		m_gui->EnableMenu(N_MESH, MENUID_MESH_DELETE, false);
	}

	else if (menu_id == MENUID_DOMAIN_DOMAIN_SETTING)
	{
		DomainEditor* editor = m_core->GetCurrentDomainEditor();
		if( !editor ){
			UI::MessageDlg("Domain is nothing");
			return;
		}

		if( editor->GetType() == DomainEditor::DOMAIN_EDITOR_CARTESIAN){
			DomainCartesianEditor* cartEditor = dynamic_cast<DomainCartesianEditor*>(editor);
			UI::DomainCartesianDlg(win, cartEditor);
			return;
		}
		if( editor->GetType() == DomainEditor::DOMAIN_EDITOR_BCM){
			DomainBCMEditor* bcmEditor = dynamic_cast<DomainBCMEditor*>(editor);
			UI::Domain_BCM_Dlg(win, bcmEditor, m_gui);
			return;
		}
	
	} 

	else if (menu_id == MENUID_DOMAIN_OUTERBC_SETTING)
	{
		DomainEditor* editor = m_core->GetCurrentDomainEditor();
		if( !editor ){
			UI::MessageDlg("Domain is nothing");
			return;
		}

		VX::SG::Group* setting = m_core->GetSettingNode();
		const s32 n = setting->GetChildCount();
		b8 hasOuterbc = false ,hasMedium = false;
		for (s32 i = 0; i < n; ++i) {
			const VX::SG::NODETYPE nt = setting->GetChild(i)->GetType();
			if (nt == VX::SG::NODETYPE_OUTERBC){
				const VX::SG::OuterBC* bc = static_cast<const VX::SG::OuterBC*>(setting->GetChild(i));
				editor->AddList(bc);
				hasOuterbc = true;
			} else if (nt == VX::SG::NODETYPE_MEDIUM) {
				const VX::SG::Medium* med = static_cast<const VX::SG::Medium*>(setting->GetChild(i));
				editor->AddList(med);
				hasMedium = true;
			}
		}
		if (hasOuterbc && hasMedium)
			UI::OuterBCSettingDlgModal(win->GetTopWindow(), editor);
		else
			UI::MessageDlg("No OuterBC/Medium");
		editor->ClearLists();
		return;
	}

	else if (menu_id == MENUID_DOMAIN_DELETE_DOMAIN)
	{
		if( !m_core->DeleteDomain() ){
			UI::MessageDlg("Failed to delete domain.");
			return;
		}
		
		if (m_gui)
			m_gui->UpdateTree(false);

		UI::DomainCartesianDlg(win, NULL, false);
		UI::Domain_BCM_Dlg(win, NULL, NULL, false);

		UI::SliceControllerDlg(win, NULL, false);

		// Enable Create Cartesian / BCM Menu.
		m_gui->EnableMenu(N_DOMAIN, MENUID_DOMAIN_CREATE_CART_DOMAIN, true);
		m_gui->EnableMenu(N_DOMAIN, MENUID_DOMAIN_CREATE_BCM_DOMAIN, true);
		
		// Disable Edit Cartesian Menu.
		m_gui->EnableMenu(N_DOMAIN, MENUID_DOMAIN_DOMAIN_SETTING,  false);
		m_gui->EnableMenu(N_DOMAIN, MENUID_DOMAIN_OUTERBC_SETTING, false);
	
	}
	// to dfi importer
	else if (menu_id == MENUID_TOOLS_VOXEL_CARTESIAN_IMPORT)
	{
		// modal
		// select by GUI
		VoxelCartesianParam sdv_info;

		int ret = UI::SDImportDlgModal(win,sdv_info);
		if (ret == 0){
			return;
		}

		std::string msg = "Load: " + sdv_info.Get_Dfi_file() + " (now loading...)";
		m_gui->SetMessage(msg.c_str());
		std::string errMsg;
		if(!m_core->LoadVoxelCartesian(sdv_info ,errMsg ))
		{
			if(errMsg.size()==0){
				errMsg = "Error to read.\n" + sdv_info.Get_Dfi_file();
			}
			UI::ErrMessageDlg(errMsg.c_str());
			m_gui->SetMessage(errMsg.c_str());
			return;
		}

		//�x��������Ε\������
		if(errMsg.size()>0){
			UI::ErrMessageDlg(errMsg.c_str());
		}
	
		msg = "Loaded: " + sdv_info.Get_Dfi_file();
		m_gui->SetMessage(msg.c_str());

		m_gui->UpdateTree(false);
		m_vctrl->Reset(m_core->GetRoot());
		m_core->SetCross(m_vctrl->GetRotCenter(), m_vctrl->GetRadius());

		// unshow relating domain sdv's dialog
		VoxelCartesianEditor* editor = m_core->GetCurrentVoxelCartesianEditor();
		UI::VoxelCartesian_Dlg(win, editor, m_gui,m_core);

		// Enablee Menu.
		m_gui->EnableMenu(N_DOMAIN, MENUID_TOOLS_VOXEL_CARTESIAN_SETTING, true);
		m_gui->EnableMenu(N_DOMAIN, MENUID_TOOLS_VOXEL_CARTESIAN_DELETE,  true);
	}
	else if (menu_id == MENUID_TOOLS_VOXEL_CARTESIAN_SETTING) {

		//open modeless dialog
		VoxelCartesianEditor* editor = m_core->GetCurrentVoxelCartesianEditor();
		
		if( !editor ){
			UI::MessageDlg("VoxelTool is not loaded yet.");
			return;
		}
		
		//modeless
		UI::VoxelCartesian_Dlg(win, editor, m_gui,m_core);

	}
	else if (menu_id == MENUID_TOOLS_VOXEL_CARTESIAN_DELETE)
	{
		if( !m_core->DeleteVoxelCartesian() ){
			UI::MessageDlg("Failed to delete VoxelCartesian.");
			return;
		}
		
		if (m_gui){
			m_gui->UpdateTree(false);
		}

		// close modeless windows
		UI::VoxelCartesian_Dlg(win, NULL, NULL, NULL,false);

		// Disable Menu.
		m_gui->EnableMenu(N_DOMAIN, MENUID_TOOLS_VOXEL_CARTESIAN_SETTING, false);
		m_gui->EnableMenu(N_DOMAIN, MENUID_TOOLS_VOXEL_CARTESIAN_DELETE,  false);

	}

	else if (menu_id == MENUID_MESH_DELETE)
	{
		m_gui->SetMessage("Delete Mesh (now deleting...) ");
		UI::vxWindow::GetTopWindow()->Draw();
		if( !m_core->DeleteCellIDMesh() ){
			m_gui->SetMessage("Failed to delete Mesh");
			UI::MessageDlg("Failed to delete Mesh.");
			return;
		}
		if (m_gui)
			m_gui->UpdateTree(false);

		UI::SliceControllerDlg(win, NULL, false);
		m_gui->SetMessage("Delete Mesh");

		m_gui->EnableMenu(N_DOMAIN, MENUID_DOMAIN_CREATE_CART_DOMAIN, true);
		m_gui->EnableMenu(N_DOMAIN, MENUID_DOMAIN_CREATE_BCM_DOMAIN, true);
		m_gui->EnableMenu(N_DOMAIN, MENUID_DOMAIN_DELETE_DOMAIN, true);
		m_gui->EnableMenu(N_MESH,   MENUID_MESH_DELETE, false);
	}

	else if (menu_id == MENUID_SETTING_BCMEDIUM_LIST
	||	menu_id == MENUID_SETTING_BCMEDIUM_LIST2
	||   menu_id == MENUID_SELECTION_SETTING_BCMEDIUM
	||   menu_id == MENUID_SELECTION_SETTING_BCMEDIUM2)
	{
		fxgenBCMedFunc bcmedfunc;
		VX::SG::Group* setting = m_core->GetSettingNode();
		const s32 n = setting->GetChildCount();
		for (s32 i = 0; i < n; ++i) {
			const VX::SG::NODETYPE nt = setting->GetChild(i)->GetType();
			if (nt == VX::SG::NODETYPE_LOCALBCCLASS){
				const VX::SG::LocalBCClass* cls = static_cast<const VX::SG::LocalBCClass*>(setting->GetChild(i));
				bcmedfunc.AddLocalBCClass(cls);
			}
			else if (nt == VX::SG::NODETYPE_OUTERBCCLASS){
				const VX::SG::OuterBCClass* cls = static_cast<const VX::SG::OuterBCClass*>(setting->GetChild(i));
				bcmedfunc.AddOuterBCClass(cls);
			}
			else if (nt == VX::SG::NODETYPE_LOCALBC){
				const VX::SG::LocalBC* bc = static_cast<const VX::SG::LocalBC*>(setting->GetChild(i));
				bcmedfunc.AddLocalBC(bc);
			}
			else if (nt == VX::SG::NODETYPE_OUTERBC){
				const VX::SG::OuterBC* bc = static_cast<const VX::SG::OuterBC*>(setting->GetChild(i));
				bcmedfunc.AddOuterBC(bc);
			}
			else if (nt == VX::SG::NODETYPE_MEDIUM){
				const VX::SG::Medium* m = static_cast<const VX::SG::Medium*>(setting->GetChild(i));
				bcmedfunc.AddMedium(m);
			}
		}
		
		if (menu_id == MENUID_SELECTION_SETTING_BCMEDIUM)
		{
			int ret = UI::LocalBCSettingDlgModal(win, bcmedfunc);
			if (!ret)
				return;
			const int lbc = bcmedfunc.GetLocalBCNum();
			const int med = bcmedfunc.GetMediumNum();
			if (lbc) {
				m_core->SelectionSetID(bcmedfunc.GetLocalBC(0)->GetID());
			} else if (med) {
				m_core->SelectionSetID(bcmedfunc.GetMedium(0)->GetID());
			}
		}
		//change to modeless
		if (menu_id == MENUID_SELECTION_SETTING_BCMEDIUM2)
		{
			b8 ret = UI::LocalBCSettingDlgModal2(win, bcmedfunc,m_core,m_gui);
			if (!ret)
				return;
		}
		
		else if (menu_id == MENUID_SETTING_BCMEDIUM_LIST)
		{
			int ret = UI::BCMedDlgModal(win, bcmedfunc);
			if (!ret)
				return;

			clearLocalBCOuterBCMedium(setting);
			const int lbc = bcmedfunc.GetLocalBCNum();
			for (int i = 0; i < lbc; ++i) {
				setting->AddChild(const_cast<VX::SG::LocalBC*>(bcmedfunc.GetLocalBC(i)));
			}
			const int obc = bcmedfunc.GetOuterBCNum();
			for (int i = 0; i < obc; ++i) {
				setting->AddChild(const_cast<VX::SG::OuterBC*>(bcmedfunc.GetOuterBC(i)));
			}
			const int med = bcmedfunc.GetMediumNum();
			for (int i = 0; i < med; ++i) {
				setting->AddChild(const_cast<VX::SG::Medium*>(bcmedfunc.GetMedium(i)));
			}
			m_core->UpdateIDColorTable();
		}
		//change to modeless
		else if (menu_id == MENUID_SETTING_BCMEDIUM_LIST2)
		{
			b8 ret = UI::BCMedDlgModal2(win, bcmedfunc,m_core);
			if (!ret)
				return;
		}

	} else if( menu_id == MENUID_SETTING_BACKCOLOR){

		VX::Math::vec4 color = m_core->GetBackgroundColor();
		if( UI::ColorDlg(win, color )){
			m_core->SetBackgroundColor(color);
		}
	} 
	else if( menu_id == MENUID_SETTING_COORDINATE_LIST){

		b8 ret = UI::CoordinateViewer_DlgModal(win, m_core, m_vctrl,m_gui);	
		if (!ret)
		return;

		}
		
	else if (menu_id == MENUID_SELECTION_SETTING_SUBDOMAIN){
		DomainEditor* de = m_core->GetCurrentDomainEditor();
		if( !de ){
			UI::MessageDlg("Domain is nothing");
			return;
		}
		fxgenSubdomainFunc subdomFunc(de->GetActiveDomain());
		int ret = UI::SubdomainSettingDlgModal(win, subdomFunc);
		if (!ret)
			return;
	} else if (menu_id == MENUID_SELECTION_RESETDOMAINCLIP){
		DomainEditor* de = m_core->GetCurrentDomainEditor();
		if( !de ){
			UI::MessageDlg("Domain is nothing");
			return;
		}
		de->ResetClipping();
	} else if (menu_id == MENUID_VIEW_MODE_WIRE) {
		m_gui->CheckMenu(N_VIEW, MENUID_VIEW_MODE_IDWIRE, false);
		m_gui->CheckMenu(N_VIEW, MENUID_VIEW_MODE_ID,     false);
		m_gui->CheckMenu(N_VIEW, MENUID_VIEW_MODE_WIRE,   true);
		m_core->ShowWire(true);
		m_core->ShowPolygon(false);
		m_gui->SetMessage("Polygon Wire mode");
	} else if (menu_id == MENUID_VIEW_MODE_IDWIRE) {
		m_gui->CheckMenu(N_VIEW, MENUID_VIEW_MODE_IDWIRE, true);
		m_gui->CheckMenu(N_VIEW, MENUID_VIEW_MODE_ID,     false);
		m_gui->CheckMenu(N_VIEW, MENUID_VIEW_MODE_WIRE,   false);
		m_core->ShowWire(true);
		m_core->ShowPolygon(true);
		m_gui->SetMessage("Polygon ID+Wire mode");
	} else if (menu_id == MENUID_VIEW_MODE_ID) {
		m_gui->CheckMenu(N_VIEW, MENUID_VIEW_MODE_IDWIRE, false);
		m_gui->CheckMenu(N_VIEW, MENUID_VIEW_MODE_ID,     true);
		m_gui->CheckMenu(N_VIEW, MENUID_VIEW_MODE_WIRE,   false);
		m_core->ShowWire(false);
		m_core->ShowPolygon(true);
		m_gui->SetMessage("Polygon ID mode");
	} else if (menu_id == MENUID_VIEW_MODE_DOMAIN_BOX) {
		m_gui->CheckMenu(N_VIEW, MENUID_VIEW_MODE_DOMAIN_BOX, true);
		m_gui->CheckMenu(N_VIEW, MENUID_VIEW_MODE_DOMAIN_BOXWIRE, false);
		m_core->ShowDomainWire(false);
		m_gui->SetMessage("Domain Box mode");
	} else if (menu_id == MENUID_VIEW_MODE_DOMAIN_BOXWIRE) {
		m_gui->CheckMenu(N_VIEW, MENUID_VIEW_MODE_DOMAIN_BOX, false);
		m_gui->CheckMenu(N_VIEW, MENUID_VIEW_MODE_DOMAIN_BOXWIRE, true);
		m_core->ShowDomainWire(true);
		m_gui->SetMessage("Domain Box+Wire mode");
	} else if (menu_id == MENUID_SELECTION_PICK){
		m_vctrl->SetMode(ViewController::SELECT_POINT);
		m_gui->CheckMenu(N_SELECTION, MENUID_SELECTION_RECT, false);
		m_gui->CheckMenu(N_SELECTION, MENUID_SELECTION_PICK, true);
		m_gui->CheckMenu(N_VIEW, MENUID_VIEW_VIEWROT,        false);
		m_gui->CheckMenu(N_VIEW, MENUID_VIEW_VIEWTRANS,      false);
		m_gui->CheckMenu(N_VIEW, MENUID_VIEW_VIEWZOOM,       false);
		m_gui->CheckMenu(N_VIEW, MENUID_VIEW_VIEWROT_AXIS,   false);
		m_gui->SetMessage("Pick mode");
	} else if (menu_id == MENUID_SELECTION_RECT){
		m_vctrl->SetMode(ViewController::SELECT_RECT);
		m_gui->CheckMenu(N_SELECTION, MENUID_SELECTION_RECT, true);
		m_gui->CheckMenu(N_SELECTION, MENUID_SELECTION_PICK, false);
		m_gui->CheckMenu(N_VIEW, MENUID_VIEW_VIEWROT,        false);
		m_gui->CheckMenu(N_VIEW, MENUID_VIEW_VIEWTRANS,      false);
		m_gui->CheckMenu(N_VIEW, MENUID_VIEW_VIEWZOOM,       false);
		m_gui->CheckMenu(N_VIEW, MENUID_VIEW_VIEWROT_AXIS,   false);
		m_gui->SetMessage("Rect mode");
	} else if (menu_id == MENUID_VIEW_VIEWROT){
		m_vctrl->SetMode(ViewController::VIEW_ROT);
		m_gui->CheckMenu(N_SELECTION, MENUID_SELECTION_RECT, false);
		m_gui->CheckMenu(N_SELECTION, MENUID_SELECTION_PICK, false);
		m_gui->CheckMenu(N_VIEW, MENUID_VIEW_VIEWROT,        true);
		m_gui->CheckMenu(N_VIEW, MENUID_VIEW_VIEWTRANS,      false);
		m_gui->CheckMenu(N_VIEW, MENUID_VIEW_VIEWZOOM,       false);
		m_gui->CheckMenu(N_VIEW, MENUID_VIEW_VIEWROT_AXIS,   false);
		m_gui->SetMessage("View Rotation Mode");
	} else if (menu_id == MENUID_VIEW_VIEWTRANS){
		m_vctrl->SetMode(ViewController::VIEW_TRANS);
		m_gui->CheckMenu(N_SELECTION, MENUID_SELECTION_RECT, false);
		m_gui->CheckMenu(N_SELECTION, MENUID_SELECTION_PICK, false);
		m_gui->CheckMenu(N_VIEW, MENUID_VIEW_VIEWROT,        false);
		m_gui->CheckMenu(N_VIEW, MENUID_VIEW_VIEWTRANS,      true);
		m_gui->CheckMenu(N_VIEW, MENUID_VIEW_VIEWZOOM,       false);
		m_gui->CheckMenu(N_VIEW, MENUID_VIEW_VIEWROT_AXIS,   false);
		m_gui->SetMessage("View Translation Mode");
	} else if (menu_id == MENUID_VIEW_VIEWZOOM){
		m_vctrl->SetMode(ViewController::VIEW_ZOOM);
		m_gui->CheckMenu(N_SELECTION, MENUID_SELECTION_RECT, false);
		m_gui->CheckMenu(N_SELECTION, MENUID_SELECTION_PICK, false);
		m_gui->CheckMenu(N_VIEW, MENUID_VIEW_VIEWROT,        false);
		m_gui->CheckMenu(N_VIEW, MENUID_VIEW_VIEWTRANS,      false);
		m_gui->CheckMenu(N_VIEW, MENUID_VIEW_VIEWZOOM,       true);
		m_gui->CheckMenu(N_VIEW, MENUID_VIEW_VIEWROT_AXIS,   false);
		m_gui->SetMessage("View Zoom Mode");
	} else if (menu_id == MENUID_VIEW_VIEWROT_AXIS){
		m_vctrl->SetMode(ViewController::VIEW_ROT_AXIS);
		m_gui->CheckMenu(N_SELECTION, MENUID_SELECTION_RECT, false);
		m_gui->CheckMenu(N_SELECTION, MENUID_SELECTION_PICK, false);
		m_gui->CheckMenu(N_VIEW, MENUID_VIEW_VIEWROT,        false);
		m_gui->CheckMenu(N_VIEW, MENUID_VIEW_VIEWTRANS,      false);
		m_gui->CheckMenu(N_VIEW, MENUID_VIEW_VIEWZOOM,       false);
		m_gui->CheckMenu(N_VIEW, MENUID_VIEW_VIEWROT_AXIS,   true);
		m_gui->SetMessage("View Axis Rotation Mode");
	} else if (menu_id == MENUID_SELECTION_POLY){
		m_gui->CheckMenu(N_SELECTION, MENUID_SELECTION_POLY, true);
		m_gui->CheckMenu(N_SELECTION, MENUID_SELECTION_SUBD, false);
		m_gui->EnableMenu(N_SELECTION, MENUID_SELECTION_SETTING_BCMEDIUM, true);
		m_gui->EnableMenu(N_SELECTION, MENUID_SELECTION_SETTING_BCMEDIUM2, true);
		m_gui->EnableMenu(N_SELECTION, MENUID_SELECTION_SETTING_SUBDOMAIN,false);
		m_gui->EnableMenu(N_SELECTION, MENUID_SELECTION_RESETDOMAINCLIP, false);
		m_core->SelectionMode(fxgenCore::MODE_GEOMETRY);
		m_gui->SetMessage("Polygon Selection Mode");
	} else if (menu_id == MENUID_SELECTION_SUBD){
		m_gui->CheckMenu(N_SELECTION, MENUID_SELECTION_POLY, false);
		m_gui->CheckMenu(N_SELECTION, MENUID_SELECTION_SUBD, true);
		m_gui->EnableMenu(N_SELECTION, MENUID_SELECTION_SETTING_BCMEDIUM, false);
		m_gui->EnableMenu(N_SELECTION, MENUID_SELECTION_SETTING_BCMEDIUM2, false);
		m_gui->EnableMenu(N_SELECTION, MENUID_SELECTION_SETTING_SUBDOMAIN,true);
		m_gui->EnableMenu(N_SELECTION, MENUID_SELECTION_RESETDOMAINCLIP, true);
		m_core->SelectionMode(fxgenCore::MODE_DOMAIN);
		m_gui->SetMessage("SubDomain Selection Mode");
	} else if (menu_id == MENUID_VIEW_PERSPECTIVE) {
		if (m_vctrl->GetViewMode() != ViewController::VIEW_PERSPECTIVE) {
			m_gui->CheckMenu(N_VIEW, MENUID_VIEW_PERSPECTIVE, true);
			m_vctrl->SetViewMode(ViewController::VIEW_PERSPECTIVE);
			m_gui->SetMessage("Perspective Mode");
		} else {
			m_gui->CheckMenu(N_VIEW, MENUID_VIEW_PERSPECTIVE, false);
			m_vctrl->SetViewMode(ViewController::VIEW_ORTHO);
			m_gui->SetMessage("Ortho Mode");
		}
	} else if (menu_id == MENUID_VIEW_COORDINATEAXIS) {
		m_core->ShowAxis(!m_core->IsShowAxis());
	} else if (menu_id == MENUID_VIEW_CENTERCROSS) {
		m_core->ShowCross(!m_core->IsShowCross());
		m_gui->CheckMenu(N_VIEW, MENUID_VIEW_CENTERCROSS, m_core->IsShowCross());
	} else if (menu_id == MENUID_VIEW_TREEVIEW) {
		m_gui->SetVisibleTree(!m_gui->IsVisibleTree());
	} else if (menu_id == MENUID_VIEW_VIEWALL) {
		m_vctrl->Reset(m_core->GetRoot());
		m_core->SetCross(m_vctrl->GetRotCenter(), m_vctrl->GetRadius());
		win->StartIdleloop();
		m_gui->SetMessage("View all");
	} 
	else if (menu_id == MENUID_VIEW_SLICECONTROL)
	{
		SliceController* controller = m_core->GetSliceController(m_slideMode);
		if( controller )
		{
			UI::SliceControllerDlg(win, controller);
		}
		else
		{
			UI::MessageDlg("No such Slice Plane in current scene.");
		}
		return;
	} 
	else if (menu_id == MENUID_VIEW_MODE_SLICECONTROL_FAST) {
		m_slideMode = SliceController::MODE_FAST;
		m_core->SetSlideMode(m_slideMode);
		m_gui->CheckMenu(N_VIEW,   MENUID_VIEW_MODE_SLICECONTROL_FAST, true );
		m_gui->CheckMenu(N_VIEW, MENUID_VIEW_MODE_SLICECONTROL_DETAIL, false);
	} else if (menu_id == MENUID_VIEW_MODE_SLICECONTROL_DETAIL) {
		m_slideMode = SliceController::MODE_DETAIL;
		m_core->SetSlideMode(m_slideMode);
		m_gui->CheckMenu(N_VIEW,   MENUID_VIEW_MODE_SLICECONTROL_FAST, false);
		m_gui->CheckMenu(N_VIEW, MENUID_VIEW_MODE_SLICECONTROL_DETAIL, true );
	} else if (menu_id == MENUID_VIEW_MODE_MESH_GRID) {
		m_core->ShowMeshGrid(!m_core->IsShowMeshGrid());
		m_gui->CheckMenu(N_VIEW, MENUID_VIEW_MODE_MESH_GRID, m_core->IsShowMeshGrid());
	} else if (menu_id == MENUID_SELECTION_FOCUS) {
		m_vctrl->Fit(m_core->GetRoot(), true);
		m_core->SetCross(m_vctrl->GetRotCenter(), m_vctrl->GetRadius());
		win->StartIdleloop();
		m_gui->SetMessage("Focus to selections");
	} else if (menu_id == MENUID_SELECTION_NONE) {
		m_core->ClearSelection();
		m_gui->SetMessage("Clear selections");

	} else if (menu_id == MENUID_VERSION) {
		UI::ShowInfoDlg();
	} else if (menu_id == MENUID_EXIT) {
		
		bool isBBoxOK = UI::ConfirmDlg("Are you sure to quit FXgen?");
		if(isBBoxOK){
			UI::Exit();
		}
	} else if (menu_id == MENUID_INVISIBLE_ZOOMIN2) {
		m_vctrl->ZoomUp(2.f, m_core->GetRoot(), true);
		win->StartIdleloop();
	} else if (menu_id == MENUID_INVISIBLE_ZOOMIN5) {
		m_vctrl->ZoomUp(5.f, m_core->GetRoot(), true);
		win->StartIdleloop();
	} else if (menu_id == MENUID_INVISIBLE_ZOOMOUT2) {
		m_vctrl->ZoomUp(0.5f, m_core->GetRoot(), true);
		win->StartIdleloop();
	} else if (menu_id == MENUID_INVISIBLE_ZOOMOUT5) {
		m_vctrl->ZoomUp(0.2f, m_core->GetRoot(), true);
		win->StartIdleloop();
	} else if (menu_id == MENUID_TREE_DELETE) {
		VX::SG::Node* node = m_gui->GetTreeMenuNode();
		if (node == NULL) {
			return;
		} else if (node->GetType() == VX::SG::NODETYPE_BBOX_DOMAIN_CARTESIAN_GROUP || 
			       node->GetType() == VX::SG::NODETYPE_BBOX_DOMAIN_BCM_GROUP) {
			OnMenu(MENUID_DOMAIN_DELETE_DOMAIN);
		} else {
			m_core->DeleteNode(node);
		}
	}
}

void MainLogic::importGeometryList(const std::string& filename)
{
	bool ret = m_core->LoadGeomemtryList(filename.c_str());
	
	if(!ret){
		VXLogE("importGeometryList error :%s\n", filename.c_str());
		return;
	}
	
	m_vctrl->Reset(m_core->GetRoot());
	m_core->SetCross(m_vctrl->GetRotCenter(), m_vctrl->GetRadius());
	
	UI::vxWindow* win = UI::vxWindow::GetTopWindow();
	win->StartIdleloop();
	m_gui->UpdateTree(true);
	std::string msg = "Loaded: " + filename;
	m_gui->SetMessage(msg.c_str());
	initMenu();

}

void MainLogic::NotifySelectionPick()
{
	// show pick info on GUI
	m_gui->SetMessage(m_core->GetPickInformationStr().c_str());

	// notify to window
	UI::NotifySelectionPick();
}

