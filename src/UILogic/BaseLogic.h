/*
 *
 * BaseLogic.h
 *
 * This is interface between UI and Core
 * 
 */

#ifndef __VX_BASELOGIC_H__
#define __VX_BASELOGIC_H__

// Logic interface
class BaseLogic
{
private:
	bool m_exit;
	
protected:
	BaseLogic() : m_exit(false) {};
	void Exit()
	{
		m_exit = true;
	}
	
public:
	
	virtual ~BaseLogic(void){};
	
	// init event
	virtual void Init(){};
	virtual void Deinit(){};

	// mouse event
	enum {
		MOUSE_LEFT   = 1,
		MOUSE_RIGHT  = 2,
		MOUSE_MIDDLE = 4
	};

	virtual void OnMouse              (int x, int y, int button){};
	virtual void OnMouseMove          (int x, int y){};
	virtual void OnMouseLeftDown      (int x, int y){};
	virtual void OnMouseRightDown     (int x, int y){};
	virtual void OnMouseMiddleDown    (int x, int y){};
	virtual void OnMouseLeftUp        (int x, int y){};
	virtual void OnMouseRightUp       (int x, int y){};
	virtual void OnMouseMiddleUp      (int x, int y){};
	virtual void OnMouseWheel         (int x, int y, int wheel){};
	virtual void OnMouseLeftDblClick  (int x, int y){};
	virtual void OnMouseRightDblClick (int x, int y){};
	virtual void OnMouseMiddleDblClick(int x, int y){};

	// Keyboard event
	virtual void OnKeyDown(int code){};
	virtual void OnKeyUp  (int code){};

	// Idle
	virtual bool OnIdle(){ return false; };

	// Window Event
	virtual void OnResize(unsigned int w, unsigned int h){};
	virtual void Draw(){};
	
	virtual void OnMenu(int menu_id){};

	
	bool IsExit()
	{
		return m_exit;
	}
};


#endif // __VX_BASELOGIC_H__
