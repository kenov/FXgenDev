/*
 * fxgenBCMedFunc.cpp
 * 
 *
 */

//#include "../VX/VX.h"
//#include "../VX/Time.h"
#include "fxgenBCMedFunc.h"
#include "../VX/SG/LocalBCClass.h"
#include "../VX/SG/OuterBCClass.h"
#include "../VX/Math.h"
#include "../VX/SG/LocalBC.h"
#include "../VX/SG/OuterBC.h"
#include "../VX/SG/Medium.h"

//#include "../Core/fxgenCore.h"

fxgenBCMedFunc::fxgenBCMedFunc() : UI::BCMedFunc()
{
		
}

fxgenBCMedFunc::~fxgenBCMedFunc()
{

}

//	コピーコンストラクタ
fxgenBCMedFunc::fxgenBCMedFunc( const fxgenBCMedFunc& obj )
{
	assignCopy(obj);
}

fxgenBCMedFunc& fxgenBCMedFunc::operator=(const fxgenBCMedFunc& obj)
{
	if(this == &obj){
		return *this;   //自己代入
	}
	assignCopy(obj);
	return(*this);
}
void fxgenBCMedFunc::assignCopy(const fxgenBCMedFunc& obj)
{

	m_outerBCClassListStr.clear();
	m_outerBCClassListStr.insert(		
						m_outerBCClassListStr.end(),
						obj.m_outerBCClassListStr.begin(),
						obj.m_outerBCClassListStr.end());

	m_localBCClassListStr.clear();
	m_localBCClassListStr.insert(		
						m_localBCClassListStr.end(),
						obj.m_localBCClassListStr.begin(),
						obj.m_localBCClassListStr.end());

	m_localbc.clear();
	m_localbc.insert(		
						m_localbc.end(),
						obj.m_localbc.begin(),
						obj.m_localbc.end());

	m_outerbc.clear();
	m_outerbc.insert(		
						m_outerbc.end(),
						obj.m_outerbc.begin(),
						obj.m_outerbc.end());

	m_medium.clear();
	m_medium.insert(		
						m_medium.end(),
						obj.m_medium.begin(),
						obj.m_medium.end());

	m_localbcclass.clear();
	m_localbcclass.insert(		
						m_localbcclass.end(),
						obj.m_localbcclass.begin(),
						obj.m_localbcclass.end());

	m_outerbcclass.clear();
	m_outerbcclass.insert(		
						m_outerbcclass.end(),
						obj.m_outerbcclass.begin(),
						obj.m_outerbcclass.end());

}

int fxgenBCMedFunc::GetLocalBCClassNum() const
{
	return static_cast<int>(m_localBCClassListStr.size());
}
int fxgenBCMedFunc::GetOuterBCClassNum() const
{
	return static_cast<int>(m_outerBCClassListStr.size());
}
std::string fxgenBCMedFunc::GetLocalBCClass(int i) const
{
	if (i >= m_localBCClassListStr.size())
		return std::string();

	return m_localBCClassListStr[i];
}
std::string fxgenBCMedFunc::GetOuterBCClass(int i) const
{
	if (i >= m_outerBCClassListStr.size())
		return std::string();

	return m_outerBCClassListStr[i];
}

int fxgenBCMedFunc::GetLocalBCNum() const
{
	return static_cast<int>(m_localbc.size());
}
int fxgenBCMedFunc::GetOuterBCNum() const
{
	return static_cast<int>(m_outerbc.size());
}
int fxgenBCMedFunc::GetMediumNum() const
{
	return static_cast<int>(m_medium.size());
}
const VX::SG::LocalBC* fxgenBCMedFunc::GetLocalBC(int i) const
{
	if (i >= m_localbc.size())
		return 0;
	return m_localbc[i];
}
const VX::SG::OuterBC* fxgenBCMedFunc::GetOuterBC(int i) const
{
	if (i >= m_outerbc.size())
		return 0;
	return m_outerbc[i];
}
const VX::SG::Medium* fxgenBCMedFunc::GetMedium(int i) const
{
	if (i >= m_medium.size())
		return 0;
	return m_medium[i];
}

void fxgenBCMedFunc::ClearLocalBC()
{
	m_localbc.clear();
}
void fxgenBCMedFunc::ClearOuterBC()
{
	m_outerbc.clear();
}
void fxgenBCMedFunc::ClearMedium()
{
	m_medium.clear();
}

void fxgenBCMedFunc::AddLocalBC(const VX::SG::LocalBC* localbc)
{
	m_localbc.push_back(localbc);
}
void fxgenBCMedFunc::AddOuterBC(const VX::SG::OuterBC* outerbc)
{
	m_outerbc.push_back(outerbc);
}
void fxgenBCMedFunc::AddMedium(const VX::SG::Medium* medium)
{
	m_medium.push_back(medium);
}
void fxgenBCMedFunc::AddLocalBCClass(const VX::SG::LocalBCClass* localbcclass)
{
	m_localBCClassListStr.push_back(localbcclass->GetName().c_str());
	m_localbcclass.push_back(localbcclass);
}
void fxgenBCMedFunc::AddOuterBCClass(const VX::SG::OuterBCClass* outerbcclass)
{
	m_outerBCClassListStr.push_back(outerbcclass->GetName().c_str());
	m_outerbcclass.push_back(outerbcclass);
}
const VX::SG::LocalBCClass* fxgenBCMedFunc::findLocalBCClass(const std::string& name) const
{
	const int n = static_cast<int>(m_localbcclass.size());
	for (int i = 0; i < n; ++i) {
		if (m_localbcclass[i]->GetName() == name)
			return m_localbcclass[i];
	}
	return 0;
}

const VX::SG::OuterBCClass* fxgenBCMedFunc::findOuterBCClass(const std::string& name) const
{
	const int n = static_cast<int>(m_outerbcclass.size());
	for (int i = 0; i < n; ++i) {
		if (m_outerbcclass[i]->GetName() == name)
			return m_outerbcclass[i];
	}
	return 0;
}

const VX::SG::LocalBC* fxgenBCMedFunc::CreateLocalBC(
	const std::string& name, const std::string& classname, float col[4], unsigned int id,const std::string& mediumname) const
{
	return vxnew VX::SG::LocalBC(name, findLocalBCClass(classname), id, VX::Math::vec4(col[0], col[1], col[2], col[3]),mediumname);
}
const VX::SG::OuterBC* fxgenBCMedFunc::CreateOuterBC(const std::string& name, const std::string& classname) const
{
	return vxnew VX::SG::OuterBC(name, findOuterBCClass(classname));
}
const VX::SG::Medium* fxgenBCMedFunc::CreateMedium(const std::string& label, float col[4], unsigned int id, const std::string& medType) const
{
	return vxnew VX::SG::Medium(label, id, VX::Math::vec4(col[0], col[1], col[2], col[3]), medType);
}

		

