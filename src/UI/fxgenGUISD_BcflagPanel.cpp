#include "fxgenGUISD_BcflagPanel.h"

#include "DialogUtil.h"
#include <wx/colordlg.h>
#include "../VX/SG/LocalBCClass.h"
#include "../VX/SG/LocalBC.h"
#include "../VX/SG/Group.h"
#include "../VX/SG/OuterBCClass.h"
#include "../VX/SG/OuterBC.h"
#include "../VX/SG/Medium.h"
#include "../VX/SG/VoxelCartesianL_Scan.h"

#include "../VX/SG/VoxelCartesianL.h"
#include "../VX/SG/VoxelCartesianL_Scan.h"

#include "../VX/SG/VoxelCartesianG.h"
#include "../Core/fxgenCore.h"

#include "../Core/GUIController.h"
#include "../UI/vxWindow.h"

#include "fxgenGUISD_Frame.h"


#include "../FileIO/BCMediumIDNumbering.h"
#include <algorithm>
#include <map>
#include <string>

fxgenGUISD_BcflagPanel::fxgenGUISD_BcflagPanel(
	wxWindow* parent,  GUIController *gui,fxgenCore* core,
	fxgenGUISD_Frame* dlg
	)
:SD_BcflagPanel( parent ), m_core(core), m_gui(gui),m_dlg(dlg)
{
	
}

void fxgenGUISD_BcflagPanel::Show_inner()
{
	//bcflagのテーブルをクリアする
	ClearMedBcflgGridTable();
}
void fxgenGUISD_BcflagPanel::apply()
{
	wxBusyCursor wait;

	//選択されたBCflag値
	std::vector<u8> BCflags;
	getSelectedBCFlags( BCflags);
	
	VX::SG::VoxelCartesianG* g = m_core->GetVoxelCartesianG();
	//グローバルに電波する
	g->ChangeShowBCFlag(BCflags);

	// Local Planeの目のアイコンを強制的に見せる。BCFlagはlocalに属しているため可視化したほうが便利
	changeEyeStatus();

	//ツリー更新
	updateMainWnd();
}

void fxgenGUISD_BcflagPanel::changeEyeStatus()
{
	VX::SG::VoxelCartesianG* g = m_core->GetVoxelCartesianG();
	//g->GetClipNode()->SetVisible(b_clip_eye_open);
	//g->GetGlobalGroup()->SetVisible(b_global_eye_open);
	g->GetLocalGroup()->SetVisible(true);

	m_dlg->Notify_EyeStatus();

}


/**
*	@brief BCflagを複数選択して適用ボタン
*/
void fxgenGUISD_BcflagPanel::m_button_applyOnButtonClick( wxCommandEvent& event )
{
	apply();
}


void fxgenGUISD_BcflagPanel::m_checkBox_hyde_globalPlaneOnCheckBox( wxCommandEvent& event )
{

	bool visible = !(event.IsChecked());

	VX::SG::VoxelCartesianG* g = m_core->GetVoxelCartesianG();
	g->GetGlobalGroup()->SetVisible(visible);

	// repaint 
	updateMainWnd();

}

/**
*	@brief planeを非表示するフラグ
*/
void fxgenGUISD_BcflagPanel::m_checkBox_hyde_LocalPlanesOnCheckBox( wxCommandEvent& event )
{

	//stagingされていて　可視化されているサブドメインのXYZ断面を表示しない
	bool XYZshow = !(event.IsChecked());

	const std::vector<VX::SG::VoxelCartesianL*>& staginglocals = m_dlg->Get_staging_locals();
	for(int i=0;i<staginglocals.size();i++){

		VX::SG::VoxelCartesianL* local = staginglocals[i];

		s32 c = local->GetChildCount();
		for( int j=0;j<c;j++){
			VX::SG::Node* plane = local->GetChild(j);
			if (plane->GetType() == VX::SG::NODETYPE_VOXEL_CARTESIAN_SLICE_L) {
				//XYZ断面の可視化を変える
				plane->SetVisible(XYZshow);
			}
		}
	}


	//ツリーノードを更新
	updateMainWnd();

}

void fxgenGUISD_BcflagPanel::getSelectedBCFlags( std::vector<u8>& BCflags)
{

	//commit cell editing status.
    //m_pLocalBCGrid->SelectAll(); //ClearSelection(); //->AcceptsFocus();
    m_pLocalBCGrid->SaveEditControlValue();
    
	int rowsum = m_pLocalBCGrid->GetRows();
	for (int i = 0; i < rowsum; i++) {
		int COL_CHECK = 0;
		int COL_ID = 4;
		//AttrEditorを参照すると解放がgridの管轄になるのでセルの値だけを取得する
		wxString CheckStauts = m_pLocalBCGrid->GetCellValue(i, COL_CHECK);
        VXLogI("CheckStauts[row=%d][%s]\n", i ,CheckStauts.ToStdString().c_str() );
		
        if( wxGridCellBoolEditor::IsTrueValue(CheckStauts)){
			//check
			wxString ID = m_pLocalBCGrid->GetCellValue(i, COL_ID);
			s32 val = atoi(ID.c_str());
			
			u8 bc = 0;

			if (val > 32) {// invalid ID
				assert(false);
				bc = 0;
			}else{
				bc = static_cast<u8>(val);
				
			}

			BCflags.push_back(bc);

		}
		
	}
    VXLogI("selected bcflag sum(%d/%d)\n", BCflags.size(),rowsum);
}


void fxgenGUISD_BcflagPanel::updateMainWnd()
{
	m_dlg->updateMainWnd();

	// tree icon update
	//m_gui->UpdateTree(false);
	//UI::vxWindow::GetTopWindow()->Draw();
}


void fxgenGUISD_BcflagPanel::AddLocalBC(const VX::SG::LocalBC* local)
{
	wxString alias		= local->GetAlias();
	wxString classname	= local->GetClass()->GetLabel();
	VX::Math::vec4 col  = local->GetColor();
	u32 id				= local->GetID();
	wxString medium		= local->GetMedium();

	//
	// Add New row
	m_pLocalBCGrid->AppendRows();

	int row = m_pLocalBCGrid->GetRows()-1;

	wxGridCellAttr* attr = new wxGridCellAttr();
	attr->SetEditor(new wxGridCellBoolEditor() );
	attr->SetRenderer( new wxGridCellBoolRenderer() );
	m_pLocalBCGrid->SetAttr(row ,0, attr);



	m_pLocalBCGrid->SetCellValue(row, 1, alias);
	m_pLocalBCGrid->SetCellValue(row, 2, classname);
	// column of color setting
	m_pLocalBCGrid->SetCellBackgroundColour(row, 3, wxColour(col[0]*0xFF, col[1]*0xFF, col[2]*0xFF));
	m_pLocalBCGrid->SetReadOnly(row, 3, true);

	m_pLocalBCGrid->SetCellValue(row, 4, wxString::Format("%d",id));
	m_pLocalBCGrid->SetCellValue(row ,5, medium);


}




void fxgenGUISD_BcflagPanel::NotifySelectionPick()
{
	//特になし
}
void fxgenGUISD_BcflagPanel::AddMedium(const VX::SG::Medium* med)
{
	wxString label			= med->GetLabel();
	VX::Math::vec4 col		= med->GetColor();

	u32 id					= med->GetID();
	wxString medType		= med->GetMediumType();

	//
	// Add New row
	m_pMedGrid->AppendRows();

	int row = m_pMedGrid->GetRows()-1;
	m_pMedGrid->SetCellValue(row , 0, label);

	// column of color setting
	m_pMedGrid->SetCellBackgroundColour(row, 1, wxColour(col[0]*0xFF, col[1]*0xFF, col[2]*0xFF));
	m_pMedGrid->SetReadOnly(row, 1, true);

	// ID
	// readonly cel
	m_pMedGrid->SetCellValue(row, 2, wxString::Format("%d",id));

	m_pMedGrid->SetCellValue(row, 3, medType);

}

fxgenGUISD_BcflagPanel::~fxgenGUISD_BcflagPanel()
{
	
}

void fxgenGUISD_BcflagPanel::CreateMedBcflgGridTable()
{
	setGuiData();		
}
void fxgenGUISD_BcflagPanel::ClearMedBcflgGridTable()
{
	clear_LocalBCGrid();
	clear_MedGrid();
}

int fxgenGUISD_BcflagPanel::getMediumNodeSizeInRoot()const
{
	int cnt = 0;
	VX::SG::Group* setting = m_core->GetSettingNode();
	const s32 n = setting->GetChildCount();
	for (s32 i = 0; i < n; ++i) {
		const VX::SG::NODETYPE nt = setting->GetChild(i)->GetType();
		if (nt == VX::SG::NODETYPE_MEDIUM){
			cnt++;
		}
	}
	return cnt;
}

int fxgenGUISD_BcflagPanel::getBCLocalNodeSizeInRoot()const
{

	int cnt = 0;
	VX::SG::Group* setting = m_core->GetSettingNode();
	const s32 n = setting->GetChildCount();
	for (s32 i = 0; i < n; ++i) {
		const VX::SG::NODETYPE nt = setting->GetChild(i)->GetType();
		if (nt == VX::SG::NODETYPE_LOCALBC){
			cnt++;
		}
	}
	return cnt;
}

bool fxgenGUISD_BcflagPanel::Show(bool show )
{

	return wxWindow::Show(show);
}

void fxgenGUISD_BcflagPanel::clear_LocalBCGrid()
{
	m_pLocalBCGrid->SetSelectionMode(wxGrid::wxGridSelectCells);
	m_pLocalBCGrid->SetSelectionForeground(wxColour(204,102,0));
	m_pLocalBCGrid->SetSelectionBackground(wxColour(72,31,0));


	int b_row = m_pLocalBCGrid->GetRows();
	if(b_row>0){
		m_pLocalBCGrid->DeleteRows(0,b_row);
	}
	m_pLocalBCGrid->ClearGrid();
}

void fxgenGUISD_BcflagPanel::clear_MedGrid()
{
	
	int m_row = m_pMedGrid->GetRows();
	if(m_row>0){
		m_pMedGrid->DeleteRows(0,m_row);
	}

	m_pMedGrid->SetSelectionMode(wxGrid::wxGridSelectCells);
	m_pMedGrid->ClearGrid();
}

/**
*	breif ステージインされているLocalBCの持つ BCFlag群を取得
*/
void fxgenGUISD_BcflagPanel::get_stageinLocalBCFlag( VX::SG::VoxelCartesianL_Scan& scan) const
{
	
	scan.Clear();

	const std::vector<VX::SG::VoxelCartesianL*>& staginglocals = m_dlg->Get_staging_locals();
	for(int i=0;i<staginglocals.size();i++){
		VX::SG::VoxelCartesianL* local = staginglocals[i];
		const VX::SG::VoxelCartesianL_Scan& result = local->GetScanResult_bcf();
		for(int j=0;j<scan.GetSize();j++){
			if(result.IsEnable(j)){
				scan.SetEnable(j);
			}
		}
	}

}

/**
*	@brief ロードされているCellID / BCFlag の情報を元にGridを作成
*/
void fxgenGUISD_BcflagPanel::setGuiData()
{
	// previous grid data clear

	clear_LocalBCGrid();

	clear_MedGrid();

	m_pMedGrid->EnableEditing(false);
	m_pLocalBCGrid->EnableEditing(false);

	//BCFlagを取得
	VX::SG::VoxelCartesianL_Scan bcf_scanresult;
	get_stageinLocalBCFlag(bcf_scanresult);

	VX::SG::Group* setting = m_core->GetSettingNode();

	const s32 n = setting->GetChildCount();
	for (s32 i = 0; i < n; ++i) {
		const VX::SG::NODETYPE nt = setting->GetChild(i)->GetType();
		//medium
		if (nt == VX::SG::NODETYPE_MEDIUM){
			const VX::SG::Medium* m = static_cast<const VX::SG::Medium*>(setting->GetChild(i));
			AddMedium(m);
			continue;
		}
		//localBC は stagingされているものだけを画面に表示する
		if (nt == VX::SG::NODETYPE_LOCALBC){
			const VX::SG::LocalBC* bc = static_cast<const VX::SG::LocalBC*>(setting->GetChild(i));
			s32 id = bc->GetID();
			if(bcf_scanresult.IsEnable(id)){
				//あれば行追加
				AddLocalBC(bc);
			}
		}
	}

	m_pMedGrid->EnableEditing(true);
	m_pLocalBCGrid->EnableEditing(true);


	
}



