/*
 * vxWindow.h
 * 
 */

#ifndef __VX_MAIN_WINDOW_H__
#define __VX_MAIN_WINDOW_H__

//
// !!! MUST NOT INCLUDE wx headers HERE !!!
//
#include <iostream>
#include <string.h>
#include <vector>	

class BaseLogic;
class MainFrame;

class wxWindow;

namespace UI {
	
class vxWindow
{
public:
	vxWindow(const std::string& title, int sx = -1, int sy = -1, int w = -1, int h = -1);
	~vxWindow();

	void SetLogic(BaseLogic* logic);

	
	void AddMenu(const std::string& menu_name);
	void AddSubMenu(const std::string& menu_name, int sub_menu_id, const std::string& sub_menu_name);
	void AddSubCheckMenu(const std::string& menu_name, int sub_menu_id, const std::string& sub_menu_name);
	void AddSubMenuSeparator(const std::string& menu_name);
	void CheckMenu(const std::string& menu_name, int sub_menu_id, bool check);
	void EnableMenu(const std::string& menu_name, int sub_menu_id, bool enable);

	void StartIdleloop();
	
	void Draw();

	void SetTopWindow();


	static vxWindow* GetTopWindow();

	wxWindow* GetWX();
private:
	MainFrame* m_mainframe;
	static vxWindow* m_top;
};
	
} // namespace UI


#endif // __VX_MAIN_WINDOW_H__
