#include <wx/wx.h>
#include <string>
#include <vector>
#include "vxWindow.h"
#include "../UILogic/MainLogic.h"

class vxApp : public wxApp
{
public:
	bool OnInit();
	int  OnExit();
	bool Initialize(int& argc, wxChar **argv);

private:
	UI::vxWindow*  m_mainwindow;
	BaseLogic* m_mainlogic;
	std::vector<std::string> m_argv;
};

IMPLEMENT_APP(vxApp)

#include "../VX/VX.h"

bool vxApp::OnInit()
{
	const int default_screen_width  = 1024;
	const int default_screen_height = 768;

	m_mainwindow = vxnew UI::vxWindow("FXgen", -1,-1, default_screen_width, default_screen_height);

	m_mainwindow->SetTopWindow();
#ifdef MACOS
	m_mainlogic  = vxnew MainLogic(m_argv);
#else
	m_mainlogic  = vxnew MainLogic(m_argv);
#endif

	m_mainlogic->Init();
	m_mainwindow->SetLogic(m_mainlogic);


	return true;
}

int vxApp::OnExit()
{
	m_mainlogic->Deinit();
	vxdelete(m_mainwindow);
	vxdelete(m_mainlogic);


#if defined(_DEBUG) || defined(DEBUG)
#ifndef NOTUSE_VXMEMORY
	VX::Memory::MemoryLeakReport();
#endif // NOTUSE_VXMEMORY
#endif // _DEBUG

	return 0;
}

bool vxApp::Initialize(int& argc, wxChar **argv)
{
	//call super
	for(int i=0;i<argc;i++){
		wxString mystring(argv[i]);
		std::string stlstring = std::string(mystring.mb_str());
		m_argv.push_back(stlstring);
	}
	bool ret = wxApp::Initialize(argc,argv);
	return ret;
}

