#include "fxgenGUISDImportDlgBase.h"
#include "DialogUtil.h"
#include "TextParser.h"
#include "../FileIO/FileIOUtil.h"
#include "../VX/VX.h"

VoxelCartesianParam fxgenGUISDImportDlgBase::m_param;

fxgenGUISDImportDlgBase::fxgenGUISDImportDlgBase( wxWindow* parent )
:
SDImportDlgBase( parent )
{

}

void fxgenGUISDImportDlgBase::SDImportDlgBaseOnActivate( wxActivateEvent& event )
{
// TODO: Implement SDImportDlgBaseOnActivate
	VXLogI("SDImportDlgBaseOnActivate\n");
}

void fxgenGUISDImportDlgBase::SDImportDlgBaseOnActivateApp( wxActivateEvent& event )
{
// TODO: Implement SDImportDlgBaseOnActivateApp
}

void fxgenGUISDImportDlgBase::SDImportDlgBaseOnClose( wxCloseEvent& event )
{
// TODO: Implement SDImportDlgBaseOnClose
	EndModal(0);

}

void fxgenGUISDImportDlgBase::SDImportDlgBaseOnInitDialog( wxInitDialogEvent& event )
{


	if(m_param.Has_Dfi_file()){
		m_textCtrl_dfi->SetValue(m_param.Get_Dfi_file());
	}

	if(m_param.Has_CellID_dir()){
		m_textCtrl_cellid->SetValue(m_param.Get_CellID_dir());
	}else if(m_param.Has_CellID_file()){
		m_textCtrl_cellid->SetValue(m_param.Get_CellID_file());
	}

	if(m_param.Has_BCflg_dir()){
		m_textCtrl_bcflg->SetValue(m_param.Get_BCflg_dir());
	}else if(m_param.Has_BCflg_file()){
		m_textCtrl_bcflg->SetValue(m_param.Get_BCflg_file());
	}
	

	
#ifdef _WIN32
	this->SetSize(820,300);
#elif MACOS
	// leave this size
#else
	this->SetSize(820,300);
#endif

}

void fxgenGUISDImportDlgBase::m_button_file_dfiOnButtonClick( wxCommandEvent& event )
{
	std::string filename ;
	if( !UI::FileOpenDlg(filename,"Index dfi(*.dfi)|*.dfi", getDfiDirectory())){
		return;
	}

	m_textCtrl_dfi->SetValue(filename);

	//read content dfi
	readDfiContent(filename);
}

std::string fxgenGUISDImportDlgBase::getDfiDirectory() const
{
	std::string dfi = m_textCtrl_dfi->GetValue().ToStdString();
	
	//ディレクトリを取得
	std::string dir;
	std::replace( dfi.begin(), dfi.end(), '\\', '/' );
	int pos = dfi.find_last_of('/');
	if(pos != std::string::npos){
		dir = dfi.substr(0,pos) ;
	}
	return dir;
}


/**
*	@brief read cid,bcf directory
*/
void fxgenGUISDImportDlgBase::readDfiContent( const std::string& filename )
{
	VXLogI("read file=>(%s)\n", filename.c_str());

	TextParser tp;

	if( tp.read(filename) != TP_NO_ERROR ){

		VXLogI("read file=>error\n", filename.c_str());

		return ;
	}

	tp.changeNode("/FilePath");

	std::string cid_dir,bcf_dir;

	std::vector<std::string> lbls;
	tp.getLabels(lbls);
	for(std::vector<std::string>::iterator it = lbls.begin(); it != lbls.end(); ++it){
		std::string valStr;
		tp.getValue(*it, valStr);

		if( CompStr(*it, "cid") == 0 ){
			cid_dir = valStr;
			
		}else if( CompStr(*it, "bcf") == 0 ){
			bcf_dir = valStr;
		}
	}
	// setting
	std::string abs_dir = filename;
	std::replace( abs_dir.begin(), abs_dir.end(), '\\', '/' );

	unsigned int loc = abs_dir.find_last_of ( '/');
    if( loc == std::string::npos ){
		return;
	}
	abs_dir = abs_dir.substr(0, loc);
	
	if(cid_dir.size()>0){
		cid_dir = getAbsPath( abs_dir ,cid_dir);
		// set textfield
		m_textCtrl_cellid->SetValue(cid_dir);	
	}
	if(bcf_dir.size()>0){
		bcf_dir = getAbsPath( abs_dir ,bcf_dir);
		m_textCtrl_bcflg->SetValue(bcf_dir);
	}
}

std::string fxgenGUISDImportDlgBase::getAbsPath( const std::string& abs_dir , const std::string& relpath)
{
	VXLogI("in getAbsPath=>(%s)\n", abs_dir.c_str());

	std::string wk = relpath;
	unsigned int loc = wk.find( "./", 0 );
    if( loc == 0 ){
		//相対パスの場合
		wk = wk.substr(2,wk.size()-2);
		std::string abspath = abs_dir + "/" +  wk;

		VXLogI("out abspath=>(%s)\n", abspath.c_str());

		return abspath;
	}else{
		//絶対パスの場合はabs_dirを見ない

		VXLogI("out relpath=>(%s)\n", relpath.c_str());

		return relpath;
	}
}

void fxgenGUISDImportDlgBase::m_button_file_cellidOnButtonClick( wxCommandEvent& event )
{
	std::string filename ;
	if( !UI::FileOpenDlg(filename,"CellID(*.bvx)|*.bvx" ,getDfiDirectory())){
		return;
	}
	m_textCtrl_cellid->SetValue(filename);

	//ダイレクトファイルモードなので、排他関係にあるbcfファイル を消す
	m_textCtrl_bcflg->SetValue("");

}

void fxgenGUISDImportDlgBase::m_button_dir_cellidOnButtonClick( wxCommandEvent& event )
{
	
	std::string path ;
	//入力フィールドが空なら、dfiフィアルのあるパスをデフォルトで開く。
	if( !UI::DirOpenDlg(path, getDfiDirectory())){
		return;
	}

	m_textCtrl_cellid->SetValue(path);

}

void fxgenGUISDImportDlgBase::m_button_cid_clearOnButtonClick( wxCommandEvent& event )
{
	m_textCtrl_cellid->SetValue("");	
}

void fxgenGUISDImportDlgBase::m_button_file_bcflgOnButtonClick( wxCommandEvent& event )
{
	std::string filename ;
	if( !UI::FileOpenDlg(filename,"BCFlg(*.bvx)|*.bvx",getDfiDirectory() )){
		return;
	}
	m_textCtrl_bcflg->SetValue(filename);

	//ダイレクトファイルモードなので、排他関係にあるcellidファイル を消す
	m_textCtrl_cellid->SetValue("");
}

void fxgenGUISDImportDlgBase::m_button_dir_bcflgOnButtonClick( wxCommandEvent& event )
{
	std::string path ;
	if( !UI::DirOpenDlg(path,getDfiDirectory())){
		return;
	}
	m_textCtrl_bcflg->SetValue(path);	
}

void fxgenGUISDImportDlgBase::m_button_bcf_clearOnButtonClick( wxCommandEvent& event )
{
	m_textCtrl_bcflg->SetValue("");
}

void fxgenGUISDImportDlgBase::m_pOKBtnOnButtonClick( wxCommandEvent& event )
{
	setFromGUIToParam();
	std::string msg;
	if(!m_param.isInputValid(msg)){
		UI::MessageDlg(msg.c_str());
		return;
	}
	EndModal(1);
}

void fxgenGUISDImportDlgBase::setFromGUIToParam()
{
	m_param.Clear();

	std::string dfi = m_textCtrl_dfi->GetValue().ToStdString();
	std::string cell = m_textCtrl_cellid->GetValue().ToStdString();
	std::string bcf = m_textCtrl_bcflg->GetValue().ToStdString();

	if( m_param.existFile(dfi)){
		m_param.Set_Dfi_file(dfi);
	}
	
	if( m_param.existDir(cell)){
		m_param.Set_CellID_dir(cell);
	}else if(m_param.existFile(cell)){
		m_param.Set_CellID_file(cell);
	}

	if( m_param.existDir(bcf)){
		m_param.Set_BCflg_dir(bcf);
	}else if(m_param.existFile(bcf)){
		m_param.Set_BCflg_file(bcf);
	}
}
void fxgenGUISDImportDlgBase::m_pCancelBtnOnButtonClick( wxCommandEvent& event )
{
// TODO: Implement m_pCancelBtnOnButtonClick
	EndModal(0);
}


VoxelCartesianParam fxgenGUISDImportDlgBase::GetResult()const
{
	return m_param;
}

