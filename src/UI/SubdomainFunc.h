/*
 *
 * SubdomainFunc.h
 * 
 */

#ifndef _VX_SubdomainFunc_H_
#define _VX_SubdomainFunc_H_


namespace UI
{
class SubdomainFunc
{
protected:
	SubdomainFunc(){};
public:
	virtual ~SubdomainFunc(){}
	virtual void Inactive() = 0;
	virtual void Active() = 0;
};
}

#endif // _VX_DIALOG_UTIL_H_
