#include "DomainBCMDlg.h"
#include "../Core/DomainBCMEditor.h"
#include "../Core/GUIController.h"
#include "../VX/Math.h"
#include "DialogUtil.h"

#include "../VOX/Pedigree.h"

#include "../VX/SG/NodeType.h"
#include "../VX/SG/Node.h"
#include "../VX/SG/Group.h"

#include "../VX/Log.h"

#include <map>
#include <string>
#include <algorithm>

#include "vxWindow.h"

namespace UI
{
	const char *orderingTxt[2] = { "Z", "Hilbert" };

	/////////////////////////////////////////////////////////////////////////////
	class GeometryScopeDlg : public GeometryScopeDlgBase
	{
	public:
		GeometryScopeDlg(wxWindow* parent, DomainBCMEditor* editor)
		  : GeometryScopeDlgBase(parent), m_editor(editor), m_level(0), m_isOK(false)
		  , m_type("Cell"), m_ratio(0), m_cell(1)
		{
			const VX::SG::Group* root = m_editor->GetSceneGraph();

			wxTreeItemId rootId = m_pTreeCtrl->AddRoot(root->GetName());
			std::string dir  = root->GetName() + "/";
			m_list.insert(std::pair<wxTreeItemId, std::string>(rootId, root->GetName()));

			for(int i = 0; i < root->GetChildCount(); i++){
				CreateTree(root->GetChild(i), dir, rootId);
			}
			
			m_pTreeCtrl->ExpandAll();
			char value[128];
			sprintf(value, "%d", m_level);
			m_pLevelTxt->SetValue(value);
		}

		~GeometryScopeDlg()
		{

		}

		void OnOKBtn( wxCommandEvent& event )
		{
			wxTreeItemId id = m_pTreeCtrl->GetSelection();
			if( !id ){
				UI::MessageDlg("Nothing Geometry is selected");
				return;
			}
			
			if( !ReadDlg() )
				return;

			m_editor->AddGeometryScope(m_list[id], m_level, m_type, m_ratio, m_cell);
			m_isOK = true;

			EndModal(wxID_OK);
		}

		void OnLevelTxt( wxCommandEvent& event )
		{
			int level = atoi(m_pLevelTxt->GetValue().c_str());
			if( level < 0 || level >= 16 )
			{
				UI::MessageDlg("Level is must be range in 0 - 15");
				char value[128];
				sprintf(value, "%d", m_level);
				m_pLevelTxt->SetValue(value);
				return;
			}
			m_level = level;
		}

		void OnCancelBtn( wxCommandEvent& event )
		{
			EndModal(wxID_OK);
		}

		b8 isOK() const { return m_isOK; }

		void OnMarginRadio( wxCommandEvent& event )
		{
			SetGeoMargin();
		}
		
	private:
		void CreateTree(const VX::SG::Node* node, const std::string& dir, const wxTreeItemId treeId)
		{
			VX::SG::NODETYPE type = node->GetType();
			std::string name = dir + node->GetName();

			if( type == VX::SG::NODETYPE_GROUP || type == VX::SG::NODETYPE_TRANSFORM )
			{
				if(node->GetIntermediate())
				{
					return;
				}

				wxTreeItemId curId = m_pTreeCtrl->AppendItem(treeId, node->GetName());
				m_list.insert(std::pair<wxTreeItemId, std::string>(curId, name));

				std::string curDir = name + "/";
				const VX::SG::Group* grp = static_cast<const VX::SG::Group*>(node);
				for(int i = 0; i < grp->GetChildCount(); i++)
				{
					CreateTree(grp->GetChild(i), curDir, curId);
				}
			}
			
			if( type == VX::SG::NODETYPE_GEOMETRY )
			{
				wxTreeItemId curId = m_pTreeCtrl->AppendItem(treeId, node->GetName());
				m_list.insert(std::pair<wxTreeItemId, std::string>(curId, name));
				return;
			}

			return;
		}
		
		void UpdateDlg()
		{
			char txt[64];
			sprintf(txt, "%d", m_level); m_pLevelTxt->SetValue(txt);
			
			if( m_pGeoMarginCellRBtn->GetValue() ) {
				sprintf(txt, "%f", m_cell);	  m_pGeoMarginTxt->SetValue(txt);
			} else {
				sprintf(txt, "%f", m_ratio);  m_pGeoMarginTxt->SetValue(txt);
			}
		}
		
		b8 ReadDlg()
		{
			int level = atoi(m_pLevelTxt->GetValue().c_str());
			if( level < 0 || level >= 16 )
			{
				UI::MessageDlg("Level is must be range in 0 - 15");
				return false;
			}
			m_level = level;
			
			if (m_pGeoMarginCellRBtn->GetValue()) {
				m_type = "Cell";
				m_cell = atof(m_pGeoMarginTxt->GetValue().c_str());
			} else {
				m_type = "Ratio";
				m_ratio = atof(m_pGeoMarginTxt->GetValue().c_str());
			}
			
			UpdateDlg();
			return true;
		}
		
		void SetGeoMargin()
		{
			char txt[64];
			wxString vstr;
			
			if( m_pGeoMarginCellRBtn->GetValue() ) {
				m_type = "Cell";
				vstr = m_pGeoMarginTxt->GetValue();
				m_ratio = atof(vstr.c_str());
				
				sprintf(txt, "%f", m_cell);
				m_pGeoMarginTxt->SetValue(txt);
			}
			
			if( m_pGeoMarginRatioRBtn->GetValue() ) {
				m_type = "Ratio";
				vstr = m_pGeoMarginTxt->GetValue();
				m_cell = atof(vstr.c_str());
				
				sprintf(txt, "%f", m_ratio);
				m_pGeoMarginTxt->SetValue(txt);
			}
		}
		
		std::map<wxTreeItemId, std::string> m_list;
		DomainBCMEditor* m_editor;
		u32 m_level;
		b8  m_isOK;
		
		f32 m_ratio, m_cell;
		std::string m_type;
	};

	/////////////////////////////////////////////////////////////////////////////
	class RegionScopeDlg : public RegionScopeDlgBase
	{
	public:
		RegionScopeDlg(wxWindow* parent, DomainBCMEditor* editor)
		  : RegionScopeDlgBase(parent), m_editor(editor), m_level(0), m_isOK(false)
		  , m_type("Cell"), m_ratio(0), m_cell(1)
		{
			m_editor->GetBBoxMinMax(m_min, m_max);

			m_pPolicySizeRBtn->SetValue(TRUE);
			m_pPolicyMaxRBtn->SetValue(FALSE);

			m_pMarginCellRBtn->SetValue(TRUE);
			m_pMarginRatioRBtn->SetValue(FALSE);

			SetBBoxPolicy();
			UpdateDlg();
		}

		~RegionScopeDlg()
		{

		}

		void OnPolicyRadio( wxCommandEvent& event )
		{
			SetBBoxPolicy();
		}
		
		void OnMarginRadio( wxCommandEvent& event )
		{
			SetBBoxMargin();
		}
		
		void OnOKBtn( wxCommandEvent& event )
		{
			if( !ReadDlg() )
				return;
			
			VX::Math::vec3 size = m_max - m_min;
			m_editor->AddRegionScope(m_min, size, m_level, m_type, m_ratio, m_cell);
			m_isOK = true;

			EndModal(wxID_OK);
		}

		void OnCancelBtn( wxCommandEvent& event )
		{
			EndModal(wxID_OK);
		}

		void OnLevelTxt( wxCommandEvent& event )
		{
			ReadDlg();
			UpdateDlg();
			UI::vxWindow::GetTopWindow()->Draw();
		}

		void OnBBoxTxt( wxCommandEvent& event )
		{
			ReadDlg();
			UpdateDlg();
			UI::vxWindow::GetTopWindow()->Draw();
		}

		b8 isOK() const { return m_isOK; }

	private:
		void SetBBoxPolicy()
		{
			m_pMinXTxt->Enable();
			m_pMinYTxt->Enable();
			m_pMinZTxt->Enable();

			if( m_pPolicySizeRBtn->GetValue() ){
				m_pPolicyMaxRBtn->SetValue(FALSE);
				m_pMaxXTxt->Disable();
				m_pMaxYTxt->Disable();
				m_pMaxZTxt->Disable();
				m_pSizeXTxt->Enable();
				m_pSizeYTxt->Enable();
				m_pSizeZTxt->Enable();
			}else{
				m_pPolicySizeRBtn->SetValue(FALSE);
				m_pMaxXTxt->Enable();
				m_pMaxYTxt->Enable();
				m_pMaxZTxt->Enable();
				m_pSizeXTxt->Disable();
				m_pSizeYTxt->Disable();
				m_pSizeZTxt->Disable();
			}
		}

		void SetBBoxMargin()
		{
			char txt[64];
			wxString vstr;
			
			if( m_pMarginCellRBtn->GetValue() ) {
				m_type = "Cell";
				vstr = m_pMarginTxt->GetValue();
				m_ratio = atof(vstr.c_str());
				
				sprintf(txt, "%f", m_cell);
				m_pMarginTxt->SetValue(txt);
			}
			
			if( m_pMarginRatioRBtn->GetValue() ) {
				m_type = "Ratio";
				vstr = m_pMarginTxt->GetValue();
				m_cell = atof(vstr.c_str());
				
				sprintf(txt, "%f", m_ratio);
				m_pMarginTxt->SetValue(txt);
			}
		}

		void UpdateDlg()
		{
			char txt[64];
			VX::Math::vec3 size = m_max - m_min;
			sprintf(txt, "%f", m_min[0]); m_pMinXTxt->SetValue(txt); 
			sprintf(txt, "%f", m_min[1]); m_pMinYTxt->SetValue(txt); 
			sprintf(txt, "%f", m_min[2]); m_pMinZTxt->SetValue(txt); 
			sprintf(txt, "%f", m_max[0]); m_pMaxXTxt->SetValue(txt); 
			sprintf(txt, "%f", m_max[1]); m_pMaxYTxt->SetValue(txt); 
			sprintf(txt, "%f", m_max[2]); m_pMaxZTxt->SetValue(txt); 
			sprintf(txt, "%f", size[0]);  m_pSizeXTxt->SetValue(txt); 
			sprintf(txt, "%f", size[1]);  m_pSizeYTxt->SetValue(txt); 
			sprintf(txt, "%f", size[2]);  m_pSizeZTxt->SetValue(txt); 
			
			sprintf(txt, "%d", m_level); m_pLevelTxt->SetValue(txt);
			if( m_pMarginCellRBtn->GetValue() ) {
				sprintf(txt, "%f", m_cell);	  m_pMarginTxt->SetValue(txt);
			} else {
				sprintf(txt, "%f", m_ratio);  m_pMarginTxt->SetValue(txt);
			}
		}
		
		b8 ReadDlg()
		{
			int level = atoi(m_pLevelTxt->GetValue().c_str());
			if( level < 0 || level >= 16 )
			{
				UI::MessageDlg("Level is must be range in 0 - 15");
				return false;
			}

			wxString vstr;

			VX::Math::vec3 bmin, bmax, bsize;
			vstr = m_pMinXTxt->GetValue(); bmin[0] = atof(vstr.c_str());
			vstr = m_pMinYTxt->GetValue(); bmin[1] = atof(vstr.c_str());
			vstr = m_pMinZTxt->GetValue(); bmin[2] = atof(vstr.c_str());

			if(m_pPolicyMaxRBtn->GetValue()){
				vstr = m_pMaxXTxt->GetValue(); bmax[0] = atof(vstr.c_str());
				vstr = m_pMaxYTxt->GetValue(); bmax[1] = atof(vstr.c_str());
				vstr = m_pMaxZTxt->GetValue(); bmax[2] = atof(vstr.c_str());

			}else{
				vstr = m_pSizeXTxt->GetValue(); bsize[0] = atof(vstr.c_str());
				vstr = m_pSizeYTxt->GetValue(); bsize[1] = atof(vstr.c_str());
				vstr = m_pSizeZTxt->GetValue(); bsize[2] = atof(vstr.c_str());

				bmax = bmin + bsize;
			}
			
			if( bmax[0] <= bmin[0] || bmax[1] <= bmin[1] || bmax[2] <= bmin[2] )
			{
				UpdateDlg();
				return false;
			}
			
			m_min = bmin;
			m_max = bmax;
			m_level = level;

			if (m_pMarginCellRBtn->GetValue()) {
				m_type = "Cell";
				m_cell = atof(m_pMarginTxt->GetValue().c_str());
			} else {
				m_type = "Ratio";
				m_ratio = atof(m_pMarginTxt->GetValue().c_str());
			}
			
			UpdateDlg();

			return true;
		}

		DomainBCMEditor* m_editor;

		VX::Math::vec3 m_min, m_max;
		u32 m_level;
		b8  m_isOK;
		
		f32 m_ratio, m_cell;
		std::string m_type;
	};

	/////////////////////////////////////////////////////////////////////////////

	DomainBCMDlg::DomainBCMDlg(wxWindow* parent, GUIController *gui)
	: DomainBCMDlgBase(parent),
	  m_editor(NULL),
	  m_gui(gui)
	{
		wxListItem itemCol;
		itemCol.m_image = -1;
		itemCol.m_mask  = wxLIST_MASK_TEXT;

		// GeometryScope ListCtrl
		itemCol.m_text = _T("Name");  m_pGeoScopeListCtrl->InsertColumn(0, itemCol);
		itemCol.m_text = _T("Level"); m_pGeoScopeListCtrl->InsertColumn(1, itemCol);
		itemCol.m_text = _T("Type");     m_pGeoScopeListCtrl->InsertColumn(2, itemCol);
		itemCol.m_text = _T("Margin");   m_pGeoScopeListCtrl->InsertColumn(3, itemCol);

		m_pGeoScopeListCtrl->SetColumnWidth(0, 300);
		m_pGeoScopeListCtrl->SetColumnWidth(1, 60);
		m_pGeoScopeListCtrl->SetColumnWidth(2, 60);
		m_pGeoScopeListCtrl->SetColumnWidth(3, 60);

		// Region Scope ListCtrl
		itemCol.m_text = _T("BBOX MIN");  m_pRgnScopeListCtrl->InsertColumn(0, itemCol);
		itemCol.m_text = _T("BBOX MAX");  m_pRgnScopeListCtrl->InsertColumn(1, itemCol);
		itemCol.m_text = _T("Level");     m_pRgnScopeListCtrl->InsertColumn(2, itemCol);
		itemCol.m_text = _T("Type");     m_pRgnScopeListCtrl->InsertColumn(3, itemCol);
		itemCol.m_text = _T("Margin");     m_pRgnScopeListCtrl->InsertColumn(4, itemCol);

		m_pRgnScopeListCtrl->SetColumnWidth(0, 150);
		m_pRgnScopeListCtrl->SetColumnWidth(1, 150);
		m_pRgnScopeListCtrl->SetColumnWidth(2, 60);
		m_pRgnScopeListCtrl->SetColumnWidth(3, 60);
		m_pRgnScopeListCtrl->SetColumnWidth(4, 70);
	}

	DomainBCMDlg::~DomainBCMDlg()
	{
		m_editor = NULL;
	}

	void DomainBCMDlg::SetEditor(DomainBCMEditor *editor)
	{
		m_editor = editor;
	}

	void DomainBCMDlg::BackupParameters()
	{
		m_editor->GetBBoxMinMax(m_old_bboxMin, m_old_bboxMax);
		m_editor->GetRootDims(m_old_rootDims);
		m_old_blockSize = m_editor->GetBlockSize();
		m_old_baseLevel = m_editor->GetBaseLevel();
		m_old_minLevel  = m_editor->GetMinLevel();
		m_old_leafOrdering = m_editor->GetLeafOrdering();
		m_old_exportPolicy = m_editor->GetExportPolicy();

		u32 num = 0;
		num = m_editor->GetNumGeometryScope();
		m_old_geoScopeList.clear();
		m_old_geoScopeList.reserve(num);
		for(u32 i = 0; i < num; i++){
			std::string name;
			u32 lv;
			std::string type;
			f32 ratio;
			f32 cell;
			m_editor->GetGeometryScope(i, name, lv, type, ratio, cell);
			m_old_geoScopeList.push_back(GeoScope(name, lv, type, ratio, cell));
		}

		num = m_editor->GetNumRegionScope();
		m_old_rgnScopeList.clear();
		m_old_rgnScopeList.reserve(num);
		for(u32 i = 0; i < num; i++){
			VX::Math::vec3 org, rgn;
			u32 lv;
			std::string type;
			f32 ratio;
			f32 cell;
			m_editor->GetRegionScope(i, org, rgn, lv, type, ratio, cell);
			m_old_rgnScopeList.push_back(RgnScope(org, rgn, lv, type, ratio, cell));
		}

		m_old_useDistanceBasedMethod = m_editor->GetUseDistanceBasedMethod();

		num = m_editor->GetNumSubdivisionMargin();
		m_old_subdivisionMargins.clear();
		m_old_subdivisionMargins.reserve(num);
		for (u32 i = 0; i < num; ++i)
		{
			f32 margin;
			u32 level;
			b8 isMaxLevel;
			m_editor->GetSubdivisionMargin(i, margin, level, isMaxLevel);
			m_old_subdivisionMargins.push_back(SubdivisionMargin(margin, level, isMaxLevel));
		}

		m_pedigrees.clear();
		if(m_editor->HasOctree())
		{
			m_editor->GetPedigreeList(m_pedigrees);
		}
	}

	bool DomainBCMDlg::Show(bool show)
	{
		if(show)
		{
			// Backup old parameters for cancel
			BackupParameters();

			// Init user define bbox
			m_userDefBBoxMin = m_old_bboxMin;
			m_userDefBBoxMax = m_old_bboxMax;

			Init();
		}
		else
		{
			m_editor = NULL;
		}
		return DomainBCMDlgBase::Show(show);
	}

	void DomainBCMDlg::Init()
	{
		m_pPolicySizeRBtn->SetValue(TRUE);
		m_pPolicyMaxRBtn->SetValue(FALSE);

		const std::vector<std::string> &unitList = m_editor->GetUnitList();
		m_pUnitChoice->Clear();
		for(int i = 0; i < unitList.size(); i++){
			m_pUnitChoice->Append(unitList[i]);
		}
		
		m_pLeafOrderingChoice->Clear();
		for(int i = 0; i < 2; i++){
			m_pLeafOrderingChoice->Append(orderingTxt[i]);
		}

		UpdateDlg();
		FixParameterEditable();
	}

	void DomainBCMDlg::FixParameterEditable()
	{
		if( m_editor->HasOctree() )
		{
			m_pMinXTxt->Disable();
			m_pMinYTxt->Disable();
			m_pMinZTxt->Disable();
			m_pMaxXTxt->Disable();
			m_pMaxYTxt->Disable();
			m_pMaxZTxt->Disable();
			m_pSizeXTxt->Disable();
			m_pSizeYTxt->Disable();
			m_pSizeZTxt->Disable();

			m_pDefaultBtn->Disable();

			m_pRootLengthXTxt->Disable();
			m_pRootLengthYTxt->Disable();
			m_pRootLengthZTxt->Disable();
			m_pRootDimsXTxt->Disable();
			m_pRootDimsYTxt->Disable();
			m_pRootDimsZTxt->Disable();

			m_pBaseLevelTxt->Disable();
			m_pMinLevelTxt->Disable();

			m_pBlockSizeXTxt->Disable();
			m_pBlockSizeYTxt->Disable();
			m_pBlockSizeZTxt->Disable();
			m_pLeafOrderingChoice->Disable();

			//m_pRgnScopeListCtrl->Disable();
			m_pAddRgnBtn->Disable();
			m_pDeleteRgnBtn->Disable();

			//m_pGeoScopeListCtrl->Disable();
			m_pAddGeoBtn->Disable();
			m_pDeleteGeoBtn->Disable();

			EnableDistanceBasedCtrls(false);

			m_pParallelDivTxt->Enable();
		}
		else
		{
			m_pMinXTxt->Enable();
			m_pMinYTxt->Enable();
			m_pMinZTxt->Enable();
			m_pMaxXTxt->Enable();
			m_pMaxYTxt->Enable();
			m_pMaxZTxt->Enable();

			m_pDefaultBtn->Enable();

			m_pSizeXTxt->Enable();
			m_pSizeYTxt->Enable();
			m_pSizeZTxt->Enable();
			m_pRootLengthXTxt->Enable();
			m_pRootLengthYTxt->Enable();
			m_pRootLengthZTxt->Enable();
			m_pRootDimsXTxt->Enable();
			m_pRootDimsYTxt->Enable();
			m_pRootDimsZTxt->Enable();

			m_pBaseLevelTxt->Enable();
			m_pMinLevelTxt->Enable();

			m_pBlockSizeXTxt->Enable();
			m_pBlockSizeYTxt->Enable();
			m_pBlockSizeZTxt->Enable();
			m_pLeafOrderingChoice->Enable();

			//m_pRgnScopeListCtrl->Enable();
			m_pAddRgnBtn->Enable();
			m_pDeleteRgnBtn->Enable();

			//m_pGeoScopeListCtrl->Enable();
			m_pAddGeoBtn->Enable();
			m_pDeleteGeoBtn->Enable();

			EnableDistanceBasedCtrls(true);

			m_pParallelDivTxt->Disable();

			SetBBoxPolicy();
		}
	}

	void DomainBCMDlg::UpdateGeometryScopeDlg()
	{
		m_pGeoScopeListCtrl->DeleteAllItems();
		
		wxListItem item;
		char wkstr[128];
		for(u32 i = 0; i < m_editor->GetNumGeometryScope(); i++){
			std::string name;
			u32 lv;
			std::string type;
			f32 ratio, cell;
			m_editor->GetGeometryScope(i, name, lv, type, ratio, cell);

			m_pGeoScopeListCtrl->InsertItem(i, wkstr);
			m_pGeoScopeListCtrl->SetItem(i, 0, name.c_str());
			sprintf(wkstr, "%d", lv);
			m_pGeoScopeListCtrl->SetItem(i, 1, wkstr);

			// Margin Type
			m_pGeoScopeListCtrl->SetItem(i, 2, type.c_str());
			
			// Margin Value
			if (type == "Ratio") {
				sprintf(wkstr, "%f", ratio);
			} else {
				sprintf(wkstr, "%f", cell);
			}
			m_pGeoScopeListCtrl->SetItem(i, 3, wkstr);

			item.m_itemId = i;
			item.SetTextColour(wxColour(255, 255, 255));
			m_pGeoScopeListCtrl->SetItem(item);
		}
	}

	void DomainBCMDlg::UpdateRegionScopeDlg()
	{
		m_pRgnScopeListCtrl->DeleteAllItems();
		wxListItem item;
		char wkstr[128];
		for(u32 i = 0; i < m_editor->GetNumRegionScope(); i++){
			VX::Math::vec3 min, size, max;
			u32 lv;
			std::string type;
			f32 ratio, cell;
			m_editor->GetRegionScope(i, min, size, lv, type, ratio, cell);
			
			max = min + size;

			m_pRgnScopeListCtrl->InsertItem(i, wkstr);
			m_pRgnScopeListCtrl->SetItemTextColour(i, wxColour(1, 0, 0));

			sprintf(wkstr, "%.3f, %.3f, %.3f", min.x, min.y, min.z);
			m_pRgnScopeListCtrl->SetItem(i, 0, wkstr);

			sprintf(wkstr, "%.3f, %.3f, %.3f", max.x, max.y, max.z);
			m_pRgnScopeListCtrl->SetItem(i, 1, wkstr);

			sprintf(wkstr, "%d", lv);
			m_pRgnScopeListCtrl->SetItem(i, 2, wkstr);
			
			// Margin Type
			m_pRgnScopeListCtrl->SetItem(i, 3, type.c_str());
			
			// Margin Value
			if (type == "Ratio") {
				sprintf(wkstr, "%f", ratio);
			} else {
				sprintf(wkstr, "%f", cell);
			}
			m_pRgnScopeListCtrl->SetItem(i, 4, wkstr);
			
			item.m_itemId = i;
			item.SetTextColour(wxColour(255, 255, 255));
			m_pRgnScopeListCtrl->SetItem(item);
		}
	}

	void DomainBCMDlg::UpdateMarginGrid()
	{
		m_pMarginGrid->BeginBatch();
		m_pMarginGrid->SetEditable(false);
		m_pMarginGrid->SetSelectionMode(wxGrid::wxGridSelectRows);

		// margin column
		wxGridCellAttr* marginAttr = new wxGridCellAttr();
		marginAttr->SetEditor(new wxGridCellFloatEditor());
		m_pMarginGrid->SetColAttr(0, marginAttr);

		// level choice column
		wxGridCellAttr* levelAttr = new wxGridCellAttr();
		u32 maxLevel = m_editor->GetMaxLevel();
		wxArrayString levels;
		levels.reserve(maxLevel + 1);
		levels.Add("Max");
		for (s32 i = maxLevel; 0 <= i; --i)
		{
			levels.Add(wxString::Format("%i", i));
		}
		levelAttr->SetEditor(new wxGridCellChoiceEditor(levels, true));
		m_pMarginGrid->SetColAttr(1, levelAttr);

		s32 crows = m_pMarginGrid->GetRows();
		s32 rows = m_editor->GetNumSubdivisionMargin();
		if (crows > rows)
		{
			m_pMarginGrid->DeleteRows(0, crows - rows);
		}
		else
		{
			m_pMarginGrid->AppendRows(rows - crows);
		}

		for (s32 i = 0; i < rows; ++i)
		{
			f32 margin;
			u32 level;
			b8 isMaxLevel;
			m_editor->GetSubdivisionMargin(i, margin, level, isMaxLevel);
			m_pMarginGrid->SetCellValue(i, 0, wxString::Format("%f", margin));
			if (isMaxLevel)
			{
				m_pMarginGrid->SetCellValue(i, 1, "Max");
			}
			else
			{
				m_pMarginGrid->SetCellValue(i, 1, wxString::Format("%i", level));
			}
		}

		m_pMarginGrid->SetEditable(true);
		m_pMarginGrid->EndBatch();
	}

	void DomainBCMDlg::UpdateDlg()
	{
		char txt[64];
		// BBox
		VX::Math::vec3 bboxMin, bboxMax, bboxSize;
		m_editor->GetBBoxMinMaxSize(bboxMin, bboxMax, bboxSize);
		sprintf(txt, "%f", bboxMin[0]);  m_pMinXTxt->SetValue(txt);
		sprintf(txt, "%f", bboxMin[1]);  m_pMinYTxt->SetValue(txt);
		sprintf(txt, "%f", bboxMin[2]);  m_pMinZTxt->SetValue(txt);
		sprintf(txt, "%f", bboxMax[0]);  m_pMaxXTxt->SetValue(txt);
		sprintf(txt, "%f", bboxMax[1]);  m_pMaxYTxt->SetValue(txt);
		sprintf(txt, "%f", bboxMax[2]);  m_pMaxZTxt->SetValue(txt);
		sprintf(txt, "%f", bboxSize[0]); m_pSizeXTxt->SetValue(txt);
		sprintf(txt, "%f", bboxSize[1]); m_pSizeYTxt->SetValue(txt);
		sprintf(txt, "%f", bboxSize[2]); m_pSizeZTxt->SetValue(txt);
		
		// RootDims
		VX::Math::idx3 rootDims;
		VX::Math::vec3 rootLength;
		m_editor->GetRootDims(rootDims);
		m_editor->GetRootLength(rootLength);
		sprintf(txt, "%d", rootDims[0]); m_pRootDimsXTxt->SetValue(txt);
		sprintf(txt, "%d", rootDims[1]); m_pRootDimsYTxt->SetValue(txt);
		sprintf(txt, "%d", rootDims[2]); m_pRootDimsZTxt->SetValue(txt);

		sprintf(txt, "%f", rootLength[0]); m_pRootLengthXTxt->SetValue(txt);
		sprintf(txt, "%f", rootLength[1]); m_pRootLengthYTxt->SetValue(txt);
		sprintf(txt, "%f", rootLength[2]); m_pRootLengthZTxt->SetValue(txt);
		
		// Base / Min Level
		sprintf(txt, "%d", m_editor->GetBaseLevel()); m_pBaseLevelTxt->SetValue(txt);
		sprintf(txt, "%d", m_editor->GetMinLevel());  m_pMinLevelTxt->SetValue(txt);

		// Div Count
		sprintf(txt, "%d", m_editor->GetDivCount()); m_pParallelDivTxt->SetValue(txt);
		
		// Unit
		const std::string &currentUnit = m_editor->GetUnit();
		int index = m_pUnitChoice->FindString(currentUnit);
		if( index != -1 ){
			m_pUnitChoice->Select(index);
		}else{
			m_pUnitChoice->Select(0);
		}

		// File Export Policy
		if(m_editor->GetExportPolicy() == 0){
			m_pExpPolicyParamBtn->SetValue(TRUE);
			m_pExpPolicyOctBtn->SetValue(FALSE);
		}else{
			m_pExpPolicyParamBtn->SetValue(FALSE);
			m_pExpPolicyOctBtn->SetValue(TRUE);
		}

		// Leaf Block Size
		const VX::Math::idx3& blockSize = m_editor->GetBlockSize();
		sprintf(txt, "%d", blockSize.x); m_pBlockSizeXTxt->SetValue(txt);
		sprintf(txt, "%d", blockSize.y); m_pBlockSizeYTxt->SetValue(txt);
		sprintf(txt, "%d", blockSize.z); m_pBlockSizeZTxt->SetValue(txt);

		// Leaf Block Ordering
		const std::string odrTxt = orderingTxt[m_editor->GetLeafOrdering()];
		int odrIdx = m_pLeafOrderingChoice->FindString(odrTxt);
		if( odrIdx != -1 ){
			m_pLeafOrderingChoice->Select(odrIdx);
		}else{
			m_pLeafOrderingChoice->Select(0);
		}
		
		// Region Scope
		UpdateRegionScopeDlg();

		// Geometry Scope
		UpdateGeometryScopeDlg();

		// Voxelization
		m_pUseDistanceChk->SetValue(m_editor->GetUseDistanceBasedMethod());
		//sprintf(txt, "%f", m_editor->GetDistanceMargin());
		//m_pDistanceMarginTxt->SetValue(txt);
		UpdateMarginGrid();

		// Information
		m_pInfoText->SetValue(m_editor->GetInformationText());
		
		
		// TODO: show me!!!
		/*
		 // Information
		 double cellSize[3];
		 u32 maxLevel = m_editor->GetMaxLevel();
		 cellSize[0] = static_cast<double>(rootLength[0]) / static_cast<double>(1 << maxLevel) / static_cast<double>(blockSize[0]);
		 cellSize[1] = static_cast<double>(rootLength[1]) / static_cast<double>(1 << maxLevel) / static_cast<double>(blockSize[1]);
		 cellSize[2] = static_cast<double>(rootLength[2]) / static_cast<double>(1 << maxLevel) / static_cast<double>(blockSize[2]);
		 
		 char info[512] = {0};
		 sprintf(info, "Min Cell Size : [%13.5lf %13.5lf %13.5lf]", cellSize[0], cellSize[1], cellSize[2]);
		 
		 m_pInfoStaticTxt->SetLabel(info);
		 */
	}

	void DomainBCMDlg::OnCancelBtn( wxCommandEvent& event )
	{
		Reset();
		this->Show(false);
	}
	
	void DomainBCMDlg::Reset()
	{
		m_editor->SetBBoxMinMax(m_old_bboxMin, m_old_bboxMax);
		m_editor->SetRootDims(m_old_rootDims);
		m_editor->SetBlockSize(m_old_blockSize);
		m_editor->SetBaseLevel(m_old_baseLevel);
		m_editor->SetMinLevel(m_old_baseLevel);
		m_editor->SetLeafOrdering(m_old_leafOrdering);
		m_editor->SetExportPolicy(m_old_exportPolicy);

		m_editor->ClearGeometryScope();
		m_editor->ClearRegionScope();
		m_editor->ClearSubdivisionMargin();

		for(std::vector<GeoScope>::iterator it = m_old_geoScopeList.begin(); it != m_old_geoScopeList.end(); ++it){
			m_editor->AddGeometryScope(it->name, it->lv, it->type, it->ratio, it->cell);
		}

		for(std::vector<RgnScope>::iterator it = m_old_rgnScopeList.begin(); it != m_old_rgnScopeList.end(); ++it){
			m_editor->AddRegionScope(it->org, it->rgn, it->lv, it->type, it->ratio, it->cell);
		}

		m_editor->SetUseDistanceBasedMethod(m_old_useDistanceBasedMethod);
		for (std::vector<SubdivisionMargin>::const_iterator i = m_old_subdivisionMargins.begin(); i != m_old_subdivisionMargins.end(); ++i)
		{
			m_editor->AddSubdivisionMargin(i->margin, i->level, i->isMaxLevel);
		}
		

		if( m_pedigrees.size() == 0 )
		{
			m_editor->DeleteOctree();
		}
		else
		{
			m_editor->CreateOctree(m_pedigrees);
		}

		UpdateDlg();
		m_editor->UpdateDistanceFieldSlices();
		UpdateSliceControllerDlg();
		m_gui->UpdateTree(false);
		UI::vxWindow::GetTopWindow()->Draw();
	}

	b8 DomainBCMDlg::isAutoChangeBBox()
	{
		VX::Math::vec3 curBBoxMin, curBBoxMax;
		m_editor->GetBBoxMinMax(curBBoxMin, curBBoxMax);

		for(int i = 0; i < 3; i++){
			if( (curBBoxMin[i] - m_userDefBBoxMin[i]) > 1.0e-6 ){ // current pitch is not equal old pitch
				return true;
			}
			if( (curBBoxMax[i] - m_userDefBBoxMax[i]) > 1.0e-6 ){ // current pitch is not equal old pitch
				return true;
			}
		}
		return false;
	}

	b8 DomainBCMDlg::isChangedRegion()
	{
		// Region Seeting

		VX::Math::vec3 min, max;
		VX::Math::vec3 bbox_min, bbox_max, bbox_max2;
		
		m_editor->GetBBoxMinMax(min, max);
		
		wxString vstr;
		vstr = m_pMinXTxt->GetValue(); bbox_min[0] = atof(vstr.c_str());
		vstr = m_pMinYTxt->GetValue(); bbox_min[1] = atof(vstr.c_str());
		vstr = m_pMinZTxt->GetValue(); bbox_min[2] = atof(vstr.c_str());

		vstr = m_pMaxXTxt->GetValue(); bbox_max[0] = atof(vstr.c_str());
		vstr = m_pMaxYTxt->GetValue(); bbox_max[1] = atof(vstr.c_str());
		vstr = m_pMaxZTxt->GetValue(); bbox_max[2] = atof(vstr.c_str());

		VX::Math::vec3 bbox_size;
		vstr = m_pSizeXTxt->GetValue(); bbox_size[0] = atof(vstr.c_str());
		vstr = m_pSizeYTxt->GetValue(); bbox_size[1] = atof(vstr.c_str());
		vstr = m_pSizeZTxt->GetValue(); bbox_size[2] = atof(vstr.c_str());

		bbox_max2 = bbox_min + bbox_size;

		for(int i = 0; i < 3; i++){
			if( (bbox_min[i] - min[i]) > 1.0e-6 ){ // current pitch is not equal old pitch
				return true;
			}
			if( (bbox_max[i] - max[i]) > 1.0e-6 ){ // current pitch is not equal old pitch
				return true;
			}
			if( (bbox_max2[i] - max[i]) > 1.0e-6 ){ // current pitch is not equal old pitch
				return true;
			}
		}

		return false;
	}

	b8 DomainBCMDlg::isChangedRootGrid()
	{
		VX::Math::idx3 rootDims, dims;
		VX::Math::vec3 rootLen, len;
		m_editor->GetRootDims(dims);
		m_editor->GetRootLength(len);

		wxString vstr;
		vstr = m_pRootDimsXTxt->GetValue(); rootDims[0] = atoi(vstr.c_str());
		vstr = m_pRootDimsYTxt->GetValue(); rootDims[1] = atoi(vstr.c_str());
		vstr = m_pRootDimsZTxt->GetValue(); rootDims[2] = atoi(vstr.c_str());
		vstr = m_pRootLengthXTxt->GetValue(); rootLen[0] = atof(vstr.c_str());
		vstr = m_pRootLengthYTxt->GetValue(); rootLen[1] = atof(vstr.c_str());
		vstr = m_pRootLengthZTxt->GetValue(); rootLen[2] = atof(vstr.c_str());

		for(int i = 0; i < 3; i++){
			if( (rootDims[i] - dims[i]) > 0 ){ 
				return true;
			}
			if( (rootLen[i] - len[i]) > 1.0e-6 ){
				return true;
			}
		}

		return false;
	}

	b8 DomainBCMDlg::isChangedOctreeLevel()
	{
		u32 baseLevel, minLevel;
		wxString vstr;
		vstr = m_pBaseLevelTxt->GetValue(); baseLevel = atoi(vstr.c_str());
		vstr = m_pMinLevelTxt->GetValue();  minLevel  = atoi(vstr.c_str());


		u32 base = m_editor->GetBaseLevel();
		u32 min = m_editor->GetMinLevel();
		if( (baseLevel - base) > 0 ){
			return true;
		}
		if( (minLevel - min) > 0 ){
			return true;
		}

		return false;
	}

	b8 DomainBCMDlg::isChangedLeafBlock()
	{
		VX::Math::idx3 blockSize;
		const VX::Math::idx3 &size = m_editor->GetBlockSize();
		wxString vstr;
		vstr = m_pBlockSizeXTxt->GetValue(); blockSize.x = atoi(vstr.c_str());
		vstr = m_pBlockSizeYTxt->GetValue(); blockSize.y = atoi(vstr.c_str());
		vstr = m_pBlockSizeZTxt->GetValue(); blockSize.z = atoi(vstr.c_str());
		
		for(int i = 0; i < 3; i++){
			if( (blockSize[i] - size[i]) > 0 ){ 
				return true;
			}
		}

		return false;
	}

	void DomainBCMDlg::OnOKBtn( wxCommandEvent& event )
	{
		bool isBBoxOK = true;
		bool isIgnoredChange = true;
		std::vector< std::string > changevalues;

		if(isAutoChangeBBox())
		{
			isBBoxOK = UI::ConfirmDlg("Region Parameters are changed automatically.\n"
				                      "Do you confirm these parameters?\n");
		}

		if( isChangedRegion() ) {
			changevalues.push_back(std::string("Region Setting\n"));
		}
		if( isChangedRootGrid() ) {
			changevalues.push_back(std::string("Root Grid Setting\n"));
		}
		if( isChangedOctreeLevel() ) {
			changevalues.push_back(std::string("Octree Level Setting\n"));
		}
		if( isChangedLeafBlock() ) {
			changevalues.push_back(std::string("Leaf Block Size\n"));
		}

		if (changevalues.size() > 0) {
			std::string message = "Parameters below are changed but not confirmed.\n"
					  			  "Do you ignore this change:\n";
			for(int i = 0; i < (int)changevalues.size(); i++) {
				message += "  - ";
				message += changevalues[i];
			}
			isIgnoredChange = UI::ConfirmDlg(message.c_str());
		}
		
		if( isBBoxOK && isIgnoredChange )		
		{
			DomainBCMDlgBase::Show(false);
		}
	}
	
	void DomainBCMDlg::OnParallelDivTxt( wxCommandEvent& event )
	{
		_OnParallelDivTxt();
	}

	void DomainBCMDlg::_OnParallelDivTxt()
	{
		wxString vstr;
		s32 numDiv = 0;
		vstr = m_pParallelDivTxt->GetValue(); numDiv = atoi(vstr.c_str());

		if( numDiv > 0 ){
			m_editor->SetDivCount(numDiv);
		}
		UpdateDlg();
		UI::vxWindow::GetTopWindow()->Draw();
	}

	void DomainBCMDlg::OnCreateOctreeBtn( wxCommandEvent& event )
	{
		bool isBBoxOK = true;
		bool isIgnoredChange = true;
		std::vector< std::string > changevalues;

		if( isAutoChangeBBox() )
		{
			isBBoxOK = UI::ConfirmDlg("Region Parameters are changed automatically.\n"
			                          "Do you confirm these parameters?\n");
		}

		if( isChangedRegion() ) {
			changevalues.push_back(std::string("Region Setting\n"));
		}
		if( isChangedRootGrid() ) {
			changevalues.push_back(std::string("Root Grid Setting\n"));
		}
		if( isChangedOctreeLevel() ) {
			changevalues.push_back(std::string("Octree Level Setting\n"));
		}
		if( isChangedLeafBlock() ) {
			changevalues.push_back(std::string("Leaf Block Size\n"));
		}

		if (changevalues.size() > 0) {
			std::string message = "Parameters below are changed but not confirmed.\n"
					  			  "Do you ignore this change:\n";
			for(int i = 0; i < (int)changevalues.size(); i++) {
				message += "  - ";
				message += changevalues[i];
			}
			isIgnoredChange = UI::ConfirmDlg(message.c_str());
		}
		
		if( isBBoxOK && isIgnoredChange )
		{
			m_editor->GetBBoxMinMax(m_userDefBBoxMin, m_userDefBBoxMax);
			
			if( m_editor->CreateOctree() )
			{
				FixParameterEditable();

				m_pExpPolicyParamBtn->SetValue(FALSE);
				m_pExpPolicyOctBtn->SetValue(TRUE);
				m_editor->SetExportPolicy(1);

				UI::SliceControllerDlg(NULL, NULL, false);
			}
			
			UpdateDlg();
			m_gui->UpdateTree(false);
			UI::vxWindow::GetTopWindow()->Draw();
		}
	}

	void DomainBCMDlg::OnDeleteOctreeBtn( wxCommandEvent& event )
	{
		if( m_editor->DeleteOctree() )
		{
			FixParameterEditable();

			m_pExpPolicyParamBtn->SetValue(TRUE);
			m_pExpPolicyOctBtn->SetValue(FALSE);
			m_editor->SetExportPolicy(0);

			UI::SliceControllerDlg(NULL, NULL, false);
		}
		
		UpdateDlg();
		m_gui->UpdateTree(false);
		UI::vxWindow::GetTopWindow()->Draw();
	}

	void DomainBCMDlg::SetBBoxPolicy()
	{
		m_pMinXTxt->Enable();
		m_pMinYTxt->Enable();
		m_pMinZTxt->Enable();

		if( m_pPolicySizeRBtn->GetValue() ){
			m_pPolicyMaxRBtn->SetValue(FALSE);
			m_pMaxXTxt->Disable();
			m_pMaxYTxt->Disable();
			m_pMaxZTxt->Disable();
			m_pSizeXTxt->Enable();
			m_pSizeYTxt->Enable();
			m_pSizeZTxt->Enable();
		}else{
			m_pPolicySizeRBtn->SetValue(FALSE);
			m_pMaxXTxt->Enable();
			m_pMaxYTxt->Enable();
			m_pMaxZTxt->Enable();
			m_pSizeXTxt->Disable();
			m_pSizeYTxt->Disable();
			m_pSizeZTxt->Disable();
		}

		u8 axisFlag = GetCenteringFlag();
		if( ((axisFlag >> 0) & 1 ) == 1){ 
			m_pMinXTxt->Disable(); 
			m_pMaxXTxt->Disable();
			m_pSizeXTxt->Enable();
		}
		if( ((axisFlag >> 1) & 1 ) == 1){ 
			m_pMinYTxt->Disable(); 
			m_pMaxYTxt->Disable(); 
			m_pSizeYTxt->Enable();
		}
		if( ((axisFlag >> 2) & 1 ) == 1){ 
			m_pMinZTxt->Disable(); 
			m_pMaxZTxt->Disable(); 
			m_pSizeZTxt->Enable();
		}
	}

	void DomainBCMDlg::OnPolicyRadio( wxCommandEvent& event )
	{
		SetBBoxPolicy();
	}

	void DomainBCMDlg::OnBBoxTxt( wxCommandEvent& event )
	{
		_OnBBoxTxt();
	}
	void DomainBCMDlg::_OnBBoxTxt()
	{
		VX::Math::vec3 bbox_min;
		VX::Math::vec3 bbox_max;
		
		wxString vstr;
		vstr = m_pMinXTxt->GetValue(); bbox_min[0] = atof(vstr.c_str());
		vstr = m_pMinYTxt->GetValue(); bbox_min[1] = atof(vstr.c_str());
		vstr = m_pMinZTxt->GetValue(); bbox_min[2] = atof(vstr.c_str());

		if(m_pPolicyMaxRBtn->GetValue()){
			vstr = m_pMaxXTxt->GetValue(); bbox_max[0] = atof(vstr.c_str());
			vstr = m_pMaxYTxt->GetValue(); bbox_max[1] = atof(vstr.c_str());
			vstr = m_pMaxZTxt->GetValue(); bbox_max[2] = atof(vstr.c_str());

		}else{
			VX::Math::vec3 bbox_size;
			vstr = m_pSizeXTxt->GetValue(); bbox_size[0] = atof(vstr.c_str());
			vstr = m_pSizeYTxt->GetValue(); bbox_size[1] = atof(vstr.c_str());
			vstr = m_pSizeZTxt->GetValue(); bbox_size[2] = atof(vstr.c_str());

			bbox_max = bbox_min + bbox_size;
		}

		if( bbox_max[0] <= bbox_min[0] || bbox_max[1] <= bbox_min[1] ||
			bbox_max[2] <= bbox_min[2] )
		{
			UpdateDlg();
			UI::vxWindow::GetTopWindow()->Draw();
			return;
		}

		m_editor->SetBBoxMinMax(bbox_min, bbox_max);
		m_editor->Centering(GetCenteringFlag());

		// Set User Define BBox
		m_editor->GetBBoxMinMax(m_userDefBBoxMin, m_userDefBBoxMax);

		UpdateDlg();

		m_editor->UpdateDistanceFieldSlices();
		UpdateSliceControllerDlg();
		UI::vxWindow::GetTopWindow()->Draw();
	}

	void DomainBCMDlg::OnCenterChk( wxCommandEvent& event )
	{
		SetBBoxPolicy();
		m_editor->Centering(GetCenteringFlag());
		UpdateDlg();
		UI::vxWindow::GetTopWindow()->Draw();
	}

	void DomainBCMDlg::OnUnitChoice( wxCommandEvent& event )
	{
		wxString unit_str = m_pUnitChoice->GetString(m_pUnitChoice->GetSelection());

		m_editor->SetUnit(unit_str.ToStdString());
	}

	void DomainBCMDlg::OnDefaultBtn( wxCommandEvent& event )
	{
		m_editor->SetDefault();
		UpdateDlg();
		m_gui->UpdateTree(false);
		UI::vxWindow::GetTopWindow()->Draw();
	}

	void DomainBCMDlg::OnForceUnifChk( wxCommandEvent& event )
	{
		// nothing
	}
	void DomainBCMDlg::OnRootLengthTxt( wxCommandEvent& event )
	{
		_OnRootLengthTxt();
	}

	void DomainBCMDlg::_OnRootLengthTxt( )
	{
		// revert BBox to user define bbox
		m_editor->SetBBoxMinMax(m_userDefBBoxMin, m_userDefBBoxMax);

		VX::Math::vec3 oldlen;
		m_editor->GetRootLength(oldlen);

		VX::Math::vec3 len;

		wxString vstr;
		vstr = m_pRootLengthXTxt->GetValue(); len[0] = atof(vstr.c_str());
		vstr = m_pRootLengthYTxt->GetValue(); len[1] = atof(vstr.c_str());
		vstr = m_pRootLengthZTxt->GetValue(); len[2] = atof(vstr.c_str());

		if( m_pUnifChk->GetValue() ){
			int idx = 0;
			for(int i = 0; i < 3; i++){
				if( fabs(oldlen[i] - len[i]) > 1.0e-6 ){
					idx = i;
					break;
				}
			}
			for(int i = 0; i < 3; i++){
				len[i] = len[idx];
			}
		}

		m_editor->SetRootLength(len);
		m_editor->Centering(GetCenteringFlag());

		UpdateDlg();
		UI::vxWindow::GetTopWindow()->Draw();
	}
	void DomainBCMDlg::OnRootDimsTxt( wxCommandEvent& event )
	{
		_OnRootDimsTxt();
	}

	void DomainBCMDlg::_OnRootDimsTxt()
	{
		// revert BBox to user define bbox
		m_editor->SetBBoxMinMax(m_userDefBBoxMin, m_userDefBBoxMax);

		VX::Math::vec3 oldlen;
		VX::Math::vec3 oldBBoxMin, oldBBoxMax, oldBBoxSize;
		m_editor->GetRootLength(oldlen);
		m_editor->GetBBoxMinMaxSize(oldBBoxMin, oldBBoxMax, oldBBoxSize);

		VX::Math::idx3 rootDims;

		wxString vstr;
		vstr = m_pRootDimsXTxt->GetValue(); rootDims[0] = atoi(vstr.c_str());
		vstr = m_pRootDimsYTxt->GetValue(); rootDims[1] = atoi(vstr.c_str());
		vstr = m_pRootDimsZTxt->GetValue(); rootDims[2] = atoi(vstr.c_str());

		if( m_pUnifChk->GetValue() ){
			VX::Math::vec3 curlen = VX::Math::vec3(oldBBoxSize.x / static_cast<f32>(rootDims[0]),
			                                       oldBBoxSize.y / static_cast<f32>(rootDims[1]),
											       oldBBoxSize.z / static_cast<f32>(rootDims[2]));
			int idx = 0;
			for(int i = 0; i < 3; i++){
				if( fabs(oldlen[i] - curlen[i]) > 1.0e-6 ){ // current pitch is not equal old pitch
					idx = i;
					break;
				}
			}
			for(int i = 0; i < 3; i++){
				curlen[i] = curlen[idx];
			}
			m_editor->SetRootLength(curlen);
			m_editor->Centering(GetCenteringFlag());
		}else{
			m_editor->SetRootDims(rootDims);
			m_editor->Centering(GetCenteringFlag());
		}

		UpdateDlg();
		UI::vxWindow::GetTopWindow()->Draw();
	}

	void DomainBCMDlg::OnLevelTxt( wxCommandEvent& event )
	{
		_OnLevelTxt();
	}

	void DomainBCMDlg::_OnLevelTxt()
	{
		u32 baseLevel, minLevel;
		wxString vstr;
		vstr = m_pBaseLevelTxt->GetValue(); baseLevel = atoi(vstr.c_str());
		vstr = m_pMinLevelTxt->GetValue();  minLevel  = atoi(vstr.c_str());

		if( baseLevel >= 16 || minLevel >= 16 )
		{
			UpdateDlg();
			return;
		}

		if( minLevel > baseLevel )
		{
			UpdateDlg();
			return;
		}

		m_editor->SetBaseLevel(baseLevel);
		m_editor->SetMinLevel(minLevel);
		
		UpdateDlg();

		m_editor->UpdateDistanceFieldSlices();
		UpdateSliceControllerDlg();
	}

	void DomainBCMDlg::OnExportPolicyRadio( wxCommandEvent& event )
	{
		if( m_pExpPolicyParamBtn->GetValue() )
		{
			m_pExpPolicyOctBtn->SetValue(FALSE);
			m_editor->SetExportPolicy(0);
		}
		else
		{
			m_pExpPolicyParamBtn->SetValue(FALSE);
			m_editor->SetExportPolicy(1);
		}
		UpdateDlg();
	}

	void DomainBCMDlg::OnBlockSizeTxt( wxCommandEvent& event )
	{
		VX::Math::idx3 size;
		wxString vstr;
		vstr = m_pBlockSizeXTxt->GetValue(); size.x = atoi(vstr.c_str());
		vstr = m_pBlockSizeYTxt->GetValue(); size.y = atoi(vstr.c_str());
		vstr = m_pBlockSizeZTxt->GetValue(); size.z = atoi(vstr.c_str());
		
		m_editor->SetBlockSize(size);

		UpdateDlg();
	}

	void DomainBCMDlg::OnLeafOrderingChoice( wxCommandEvent& event )
	{
		wxString wxstr = m_pLeafOrderingChoice->GetString(m_pLeafOrderingChoice->GetSelection());
		
		std::string odrStr = wxstr.ToStdString();
		if(odrStr == std::string(orderingTxt[0])){
			m_editor->SetLeafOrdering(0);
		}else{
			m_editor->SetLeafOrdering(1);
		}

		UpdateDlg();
	}

	void DomainBCMDlg::OnAddGeoBtn( wxCommandEvent& event )
	{
		GeometryScopeDlg geoScopeDlg(this, m_editor);
		geoScopeDlg.ShowModal();

		UpdateDlg();
	}

	void DomainBCMDlg::OnDeleteGeoBtn( wxCommandEvent& event )
	{
		long item = -1;
		for(; ;){
			item = m_pGeoScopeListCtrl->GetNextItem(item, wxLIST_NEXT_ALL, wxLIST_STATE_SELECTED);
			if( item < 0 ) break;

			m_editor->DeleteGeometryScope(static_cast<u32>(item));
		}

		UpdateDlg();
	}
	
	void DomainBCMDlg::OnAddRgnBtn( wxCommandEvent& event )
	{
		// Create New Region BBox;
		RegionScopeDlg rgnScopeDlg(this, m_editor);
		rgnScopeDlg.ShowModal();

		UpdateDlg();
		m_gui->UpdateTree(false);
		UI::vxWindow::GetTopWindow()->Draw();
	}

	void DomainBCMDlg::OnDeleteRgnBtn( wxCommandEvent& event )
	{
		long item = -1;
		for(; ;){
			item = m_pRgnScopeListCtrl->GetNextItem(item, wxLIST_NEXT_ALL, wxLIST_STATE_SELECTED);
			if( item < 0 ) break;

			m_editor->DeleteRegionScope(static_cast<u32>(item));
		}

		UpdateDlg();
		m_gui->UpdateTree(false);
		UI::vxWindow::GetTopWindow()->Draw();
	}
	
	u8 DomainBCMDlg::GetCenteringFlag()
	{
		u8 flag = 0;
		if( m_pCenterXChk->GetValue() ){ flag += (1 << 0); }
		if( m_pCenterYChk->GetValue() ){ flag += (1 << 1); }
		if( m_pCenterZChk->GetValue() ){ flag += (1 << 2); }

		return flag;
	}

	void DomainBCMDlg::OnUseDistanceChk( wxCommandEvent& event )
	{
		m_editor->SetUseDistanceBasedMethod(event.IsChecked());
		EnableDistanceBasedCtrls(true);
		UpdateDlg();
		UpdateSliceControllerDlg();
		m_gui->UpdateTree(false);
		UI::vxWindow::GetTopWindow()->Draw();
	}

	void DomainBCMDlg::OnAddMarginBtn(wxCommandEvent& event)
	{
		m_editor->AddSubdivisionMargin(0, 0, true);
		UpdateDlg();
		UI::vxWindow::GetTopWindow()->Draw();
		UpdateSliceControllerDlg();
	}

	void DomainBCMDlg::OnDeleteMarginBtn(wxCommandEvent& event)
	{
		wxArrayInt arr = m_pMarginGrid->GetSelectedRows();
		std::vector<s32> s32arr(arr.begin(), arr.end());
		std::sort(s32arr.begin(), s32arr.end());
		for (std::vector<s32>::const_reverse_iterator i = s32arr.rbegin(); i != s32arr.rend(); ++i)
		{
			m_editor->DeleteSubdivisionMargin(*i);
		}
		UpdateDlg();
		UI::vxWindow::GetTopWindow()->Draw();
		UpdateSliceControllerDlg();
	}

	void DomainBCMDlg::OnChangeMargin(wxGridEvent& event)
	{
		s32 row = event.GetRow();
		double margin = 0;
		if (!m_pMarginGrid->GetCellValue(row, 0).ToCDouble(&margin))
		{
			event.Veto();
			return;
		}
		wxString levelStr = m_pMarginGrid->GetCellValue(row, 1);
		if (levelStr == "Max")
		{
			m_editor->EditSubdivisionMargin(row, margin, 0, true);
		}
		else
		{
			long level;
			if (!levelStr.ToLong(&level))
			{
				event.Veto();
				return;
			}
			m_editor->EditSubdivisionMargin(row, margin, level, false);
		}

		//UpdateDlg();
		m_editor->UpdateDistanceFieldSlices();
		m_gui->UpdateTree(false);
		UI::vxWindow::GetTopWindow()->Draw();
		UpdateSliceControllerDlg();
	}

	void DomainBCMDlg::EnableDistanceBasedCtrls(b8 enable)
	{
		m_pUseDistanceChk->Enable(enable);
		if (m_editor->GetUseDistanceBasedMethod())
		{
			m_pMarginGrid->Enable(enable);
			m_pAddMarginBtn->Enable(enable);
			m_pDeleteMarginBtn->Enable(enable);
		}
		else
		{
			m_pMarginGrid->Disable();
			m_pAddMarginBtn->Disable();
			m_pDeleteMarginBtn->Disable();
		}
	}

	void DomainBCMDlg::OnBBoxTxtOnKillFocus( wxFocusEvent& event )
	{
		_OnBBoxTxt();
		event.Skip();
	}
	void DomainBCMDlg::OnRootLengthTxtOnKillFocus( wxFocusEvent& event )
	{
		_OnRootLengthTxt();
		event.Skip();
	}

	void DomainBCMDlg::OnRootDimsTxtOnKillFocus( wxFocusEvent& event )
	{
		_OnRootDimsTxt();
		event.Skip();
	}
	void DomainBCMDlg::OnLevelTxtOnKillFocus( wxFocusEvent& event )
	{
		_OnLevelTxt();
		event.Skip();
	}

	void DomainBCMDlg::OnParallelDivTxtOnKillFocus( wxFocusEvent& event )
	{
		_OnParallelDivTxt();
		event.Skip();
	}



} // namespace UI
