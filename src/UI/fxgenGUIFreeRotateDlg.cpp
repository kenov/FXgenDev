#include "fxgenGUIFreeRotateDlg.h"

#include "vxWindow.h"

#include "../VX/VX.h"
#include <time.h>

namespace UI
{
	fxgenGUIFreeRotateDlg::fxgenGUIFreeRotateDlg(wxWindow* parent, ViewController* vctrl)
				: FreeRotateDlgBase(parent)
	{
		m_vctrl  = vctrl;
	}

	void fxgenGUIFreeRotateDlg::FreerotateDlgBaseOnClose( wxCloseEvent& event )
	{
		this->Show(false);
	}

	void fxgenGUIFreeRotateDlg::OnFreeRotateBtn( wxCommandEvent& event )
	{
		wxString vstr;
		vstr = m_rotateDegreeTxt->GetValue();
		f32 degree = atof(vstr.c_str());
		m_vctrl->RotateAxis(degree);
		StartAnimation();
	}

	void fxgenGUIFreeRotateDlg::OnPlus90RotateBCBtn( wxCommandEvent& event )
	{
		m_vctrl->RotateAxis(90.0);
		StartAnimation();
	}

	void fxgenGUIFreeRotateDlg::OnMinus90RotateBtn( wxCommandEvent& event )
	{
		m_vctrl->RotateAxis(-90.0);
		StartAnimation();
	}

	void fxgenGUIFreeRotateDlg::StartAnimation()
	{
		UI::vxWindow* win = UI::vxWindow::GetTopWindow();
		win->StartIdleloop();
	}

	void fxgenGUIFreeRotateDlg::OnCloseBtn( wxCommandEvent& event )
	{
		this->Show(false);
	}

} // namespace UI

