
#include "vxWindow.h"
#include "DialogUtil.h"

#include "../VX/VX.h"
#include "fxgenGUIExportSTLDlg.h"
#include "../FileIO/FileIOUtil.h"
//#include <string>


namespace UI
{
	fxgenGUIExportSTLDlg::fxgenGUIExportSTLDlg(wxWindow* parent, fxgenCore* core, GUIController* gui)
		: ExportSTLDlgBase(parent)
	{
		m_core = core;
		m_gui = gui;
		m_modelScale = 1.f;
		m_modeDir = false;
		m_modeAscii = true;
		m_visibleOnly = false;
	}

	void fxgenGUIExportSTLDlg::OnExportModelScaleRadio( wxCommandEvent& event )	{}

	void fxgenGUIExportSTLDlg::SetModeDir(bool modeDir)
	{
		m_modeDir = modeDir;
		if (m_modeDir) {
			SetTitle(_T("Export Dir STL"));
		} else {
			SetTitle(_T("Export File STL"));
		}
	}
	
	void fxgenGUIExportSTLDlg::OnNextBtn( wxCommandEvent& event )
	{
		fxgenGUIExportSTLDlg::SetMode();

		if (!m_modeDir) {
			if (m_modeAscii) {
				fxgenGUIExportSTLDlg::SaveFileAscii();
			} else {
				fxgenGUIExportSTLDlg::SaveFileBinary();
			}
		} else {
			if (m_modeAscii) {
				fxgenGUIExportSTLDlg::SaveDirAscii();
			} else {
				fxgenGUIExportSTLDlg::SaveDirBinary();
			}
		}
	}

	void fxgenGUIExportSTLDlg::SaveFileAscii() {
		std::string filename;
		if (UI::FileSaveDlg(filename, "STL Ascii(*.stl)|*.stl")) {
			if (!m_core->SaveFile(filename.c_str(), "STLa", m_modelScale, m_visibleOnly)) {
				UI::MessageDlg("Failed to save file.");
			} else {
				std::string msg = "Saved STL Ascii File: " + filename;
				m_gui->SetMessage(msg.c_str());
				this->Show(false);
			}
		}
	}

	void fxgenGUIExportSTLDlg::SaveFileBinary() {
		std::string filename;
		if (UI::FileSaveDlg(filename, "STL Binary(*.stl)|*.stl")) {
			if (!m_core->SaveFile(filename.c_str(), "STLb", m_modelScale, m_visibleOnly)) {
				UI::MessageDlg("Failed to save file.");
			} else {
				std::string msg = "Saved STL Binary File: " + filename;
				m_gui->SetMessage(msg.c_str());
				this->Show(false);
			}
		}
	}

	void fxgenGUIExportSTLDlg::SaveDirAscii() {
		std::string dirname;
		if (UI::DirSaveDlg(dirname,""))	{
			if (!m_core->SaveDir(dirname.c_str(), "STLa", m_modelScale, m_visibleOnly)) {
				UI::MessageDlg("Failed to save file.");
			} else {
				std::string msg = "Saved Dir[STL Ascii]: " + dirname;
				m_gui->SetMessage(msg.c_str());
				this->Show(false);
			}
		}
	}

	void fxgenGUIExportSTLDlg::SaveDirBinary() {
		std::string dirname;
		if (UI::DirSaveDlg(dirname,""))	{
			if (!m_core->SaveDir(dirname.c_str(), "STLb", m_modelScale, m_visibleOnly)) {
				UI::MessageDlg("Failed to save file.");
			} else {
				std::string msg = "Saved Dir[STL Binary]: " + dirname;
				m_gui->SetMessage(msg.c_str());
				this->Show(false);
			}
		}
	}

	void fxgenGUIExportSTLDlg::SetMode()
	{
		if (m_exportSTLTypeAsciiRadioBtn->GetValue()) {
			m_modeAscii = true;
		} else if (m_exportSTLTypeBinaryRadioBtn->GetValue()) {
			m_modeAscii = false;
		}

		if (m_exportTypeAllRadioBtn->GetValue()) {
			m_visibleOnly = false;
		} else if (m_exportTypeOnlyRadioBtn->GetValue()) {
			m_visibleOnly = true;
		}

		if (m_modelScaleRadioBtn1->GetValue()) {
			m_modelScale = 1.f;
		} else if (m_modelScaleRadioBtn2->GetValue()) {
			wxString vstr;
			vstr = m_modelScaleTextCtrl->GetValue();
			m_modelScale = atof(vstr.c_str());
		} else if (m_modelScaleRadioBtn3->GetValue()) {
			m_modelScale = 10.f;
		} else if (m_modelScaleRadioBtn4->GetValue()) {
			m_modelScale = 100.f;
		} else if (m_modelScaleRadioBtn5->GetValue()) {
			m_modelScale = 1000.f;
		} else if (m_modelScaleRadioBtn6->GetValue()) {
			m_modelScale = 0.1f;
		} else if (m_modelScaleRadioBtn7->GetValue()) {
			m_modelScale = 0.01f;
		} else if (m_modelScaleRadioBtn8->GetValue()) {
			m_modelScale = 0.001f;
		}
	}

	void fxgenGUIExportSTLDlg::ExportSTLDlgBaseOnClose( wxCloseEvent& event )
	{
		this->Show(false);
	}

	void fxgenGUIExportSTLDlg::OnCancelBtn( wxCommandEvent& event )
	{
		this->Show(false);
	}

} // namespace UI

