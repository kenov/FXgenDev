/*
 *
 * DomainCartDilg.h
 *
 */

#ifndef _VX_DOMAIN_CART_DLG_H_
#define _VX_DOMAIN_CART_DLG_H_

#include "vxWindow.h"

#include "DialogBase.h"
#include "../VX/Type.h"
#include "../VX/Math.h"

class DomainCartesianEditor;

namespace UI
{
	class DomainCartDlg : public DomainCartDlgBase
	{
	public:
		DomainCartDlg(wxWindow *parent);
		~DomainCartDlg();
		void SetEditor(DomainCartesianEditor* editor);
		bool Show(bool show = true);
	
	private:

		void Init();
		void UpdateDlg();

		// Overload Functions
		void OnPolicyRadio( wxCommandEvent& event );
		void OnExportPolicyRadio( wxCommandEvent& event );
		void OnBBoxTxt( wxCommandEvent& event );
		void OnDefaultBtn( wxCommandEvent& event );
		void OnCenterChk( wxCommandEvent& event );
		void OnEnableVoxelBtn( wxCommandEvent& event );
		void OnForceUnifChk( wxCommandEvent& event );
		void OnPitchTxt( wxCommandEvent& event );
		void OnVoxTxt( wxCommandEvent& event );
		void OnEnableDvdBtn( wxCommandEvent& event );
		void OnDvdManuTxt( wxCommandEvent& event );
		void OnDvdAutoTxt( wxCommandEvent& event );
		void OnOKBtn( wxCommandEvent& event );
		void OnCancelBtn( wxCommandEvent& event );
		void OnUnitChoice( wxCommandEvent& event );
		void OnClose( wxCloseEvent& event );

		void OnBBoxTxtOnKillFocus( wxFocusEvent& event );
		void OnPitchTxtOnKillFocus( wxFocusEvent& event );
		void OnVoxTxtOnKillFocus( wxFocusEvent& event );
		void OnDvdManuTxtOnKillFocus( wxFocusEvent& event );
		void OnDvdAutoTxtOnKillFocus( wxFocusEvent& event );

		void Reset();
		void SetBBoxPolicy();
		void InitVoxDivAppear();
		u8 GetCenteringFlag();

	private:
		void _OnBBoxTxt(  );
		void _OnPitchTxt();
		void _OnVoxTxt( );
		void _OnDvdManuTxt(  );
		void _OnDvdAutoTxt(  );

	// Member
	private:

		DomainCartesianEditor *m_editor;

		VX::Math::vec3 m_userDefBBoxMin;
		VX::Math::vec3 m_userDefBBoxMax;

		// for Cancel
		VX::Math::vec3 m_old_bboxMin;
		VX::Math::vec3 m_old_bboxMax;
		VX::Math::idx3 m_old_vox;
		VX::Math::idx3 m_old_div;
		s32            m_old_expPolicy;
		std::string    m_old_unit;
		u8*            m_oldSubdomain;

		// for dialog state
		b8			   m_voxExpand;
		b8			   m_dvdExpand;
		wxBitmap m_conBmp;
		wxBitmap m_expBmp;
	};


} // namespace UI

namespace fxGUI
{
	bool TGALoader(const unsigned char* buffer, int& w, int& h, const unsigned char** buf);
}

#endif // _VX_VOX_CART_DLG_H_
