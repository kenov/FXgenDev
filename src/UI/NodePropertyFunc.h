/*
 *
 * NodePropertyFunc.h
 * 
 */

#ifndef _VX_NodePropertyFunc_H_
#define _VX_NodePropertyFunc_H_

#include <string>
namespace UI
{
class NodePropertyFunc
{
protected:
	NodePropertyFunc(){};
public:
	virtual ~NodePropertyFunc(){}
	virtual const std::string& GetNodeType() = 0;
	virtual const std::string& GetNodeName() = 0;
	virtual int GetNodeID() = 0;
	virtual bool GetVisible() = 0;
	virtual void SetVisible(bool visible) = 0;
	virtual void SetName(const std::string& name) = 0;
	virtual void SetID(int id) = 0;
};
}

#endif // _VX_NodePropertyFunc_H_
