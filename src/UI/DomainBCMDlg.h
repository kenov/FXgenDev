/*
 *
 * DomainBCMDilg.h
 *
 */

#ifndef _VX_DOMAIN_BCM_DLG_H_
#define _VX_DOMAIN_BCM_DLG_H_

#include "DialogBase.h"
#include "../VX/Type.h"
#include "../VX/Math.h"

#include "../VOX/Pedigree.h"

#include <vector>
#include <string>

class GUIController;
class DomainBCMEditor;

namespace UI
{
	class DomainBCMDlg : public DomainBCMDlgBase
	{
	public:
		DomainBCMDlg(wxWindow *parent, GUIController *gui);
		~DomainBCMDlg();
		void SetEditor(DomainBCMEditor* editor);
		bool Show(bool show = true);

	private:

		void Init();
		void Apply();
		void BackupParameters();
		void Reset();
		void SetBBoxPolicy();
		void UpdateDlg();
		void UpdateGeometryScopeDlg();
		void UpdateRegionScopeDlg();
		void UpdateMarginGrid();
		void EnableDistanceBasedCtrls(b8 enable = true);
		void FixParameterEditable();
		u8 GetCenteringFlag();
		b8 isAutoChangeBBox();
		b8 isChangedRegion();
		b8 isChangedRootGrid();
		b8 isChangedOctreeLevel();
		b8 isChangedLeafBlock();

		void OnPolicyRadio( wxCommandEvent& event );
		void OnBBoxTxt( wxCommandEvent& event );
		void OnCenterChk( wxCommandEvent& event );
		void OnUnitChoice( wxCommandEvent& event );
		void OnDefaultBtn( wxCommandEvent& event );

		void OnForceUnifChk( wxCommandEvent& event );
		void OnRootLengthTxt( wxCommandEvent& event );
		void OnRootDimsTxt( wxCommandEvent& event );

		void OnLevelTxt( wxCommandEvent& event );

		void OnExportPolicyRadio( wxCommandEvent& event );

		void OnBlockSizeTxt( wxCommandEvent& event );
		void OnLeafOrderingChoice( wxCommandEvent& event );

		void OnAddGeoBtn( wxCommandEvent& event );
		void OnDeleteGeoBtn( wxCommandEvent& event );
		void OnAddRgnBtn( wxCommandEvent& event );
		void OnDeleteRgnBtn( wxCommandEvent& event );

		void OnUseDistanceChk( wxCommandEvent& event );
		void OnAddMarginBtn( wxCommandEvent& event );
		void OnDeleteMarginBtn( wxCommandEvent& event );
		void OnChangeMargin( wxGridEvent& event );

		void OnCreateOctreeBtn( wxCommandEvent& event );
		void OnDeleteOctreeBtn( wxCommandEvent& event );

		void OnParallelDivTxt( wxCommandEvent& event );

		void OnOKBtn( wxCommandEvent& event );
		void OnCancelBtn( wxCommandEvent& event );

		void OnBBoxTxtOnKillFocus( wxFocusEvent& event );
		void OnRootLengthTxtOnKillFocus( wxFocusEvent& event );
		void OnRootDimsTxtOnKillFocus( wxFocusEvent& event );
		void OnLevelTxtOnKillFocus( wxFocusEvent& event );
		void OnParallelDivTxtOnKillFocus( wxFocusEvent& event );

	private:
		void _OnBBoxTxt( );
		void _OnRootLengthTxt( );
		void _OnRootDimsTxt(  );
		void _OnLevelTxt( );
		void _OnParallelDivTxt( );

	// Member
	private:
		GUIController   *m_gui;
		DomainBCMEditor *m_editor;

		// for Cancel
		struct GeoScope
		{
			std::string name;
			u32 lv;
			std::string type;
			f32 ratio, cell;
			GeoScope(const std::string& _name, const u32 _lv, const std::string& _type, f32 _ratio, f32 _cell)
			: name(_name), lv(_lv), type(_type), ratio(_ratio), cell(_cell)
			{

			}
		};
		struct RgnScope
		{
			VX::Math::vec3 org, rgn;
			u32 lv;
			std::string type;
			f32 ratio, cell;
			RgnScope(const VX::Math::vec3& _org, const VX::Math::vec3& _rgn, const u32 _lv,
					 const std::string& _type, f32 _ratio, f32 _cell)
			: org(_org), rgn(_rgn), lv(_lv), type(_type), ratio(_ratio), cell(_cell)
			{

			}
		};
		struct SubdivisionMargin
		{
			f32 margin;
			u32 level;
			b8 isMaxLevel;
			SubdivisionMargin(f32 margin, u32 level, b8 isMaxLevel) : margin(margin), level(level), isMaxLevel(isMaxLevel) {}
		};

		// for Cancel
		VX::Math::vec3 m_old_bboxMin;
		VX::Math::vec3 m_old_bboxMax;
		VX::Math::idx3 m_old_rootDims;
		VX::Math::idx3 m_old_blockSize;
		u32 m_old_baseLevel;
		u32 m_old_minLevel;
		u32 m_old_leafOrdering;
		u32 m_old_exportPolicy;
		std::vector<GeoScope> m_old_geoScopeList;
		std::vector<RgnScope> m_old_rgnScopeList;
		std::vector<SubdivisionMargin> m_old_subdivisionMargins;
		b8 m_old_useDistanceBasedMethod;

		std::vector<VOX::Pedigree> m_pedigrees;

		// for check user define
		VX::Math::vec3 m_userDefBBoxMin;
		VX::Math::vec3 m_userDefBBoxMax;
	};


} // namespace UI

#endif // _VX_DOMAIN_BCM_DLG_H_
