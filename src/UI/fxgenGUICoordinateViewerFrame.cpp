#include "fxgenGUICoordinateViewerFrame.h"

#include "vxWindow.h"


#include "../Core/fxgenCore.h"
#include "../Core/ViewController.h"
#include "../VX/SG/Group.h"



using namespace VX::Math;
using namespace VX::SG;
fxgenGUICoordinateViewerFrame::fxgenGUICoordinateViewerFrame(wxWindow* parent,fxgenCore* core,ViewController* vctrl, GUIController* gui)
:CoordinateViewerFrame( parent ),m_core(core),m_vctrl(vctrl),m_gui(gui) ,m_slidePitch(0,0,0),
		m_position(0,0,0)
{

	//スライダーの初期設定
	SetupSlider();
	
	//その他パラメータ項目の初期設定
	SetupPara();

	//スライダーのアップ/ダウンボタン
	unsigned char *limg = new unsigned char[20 * 20 * 3];
	unsigned char *rimg = new unsigned char[20 * 20 * 3];
	memcpy(limg, leftArrow, (20 * 20 * 3));
	memcpy(rimg, rightArrow, (20 * 20 * 3));
		
	wxImage laImg(20, 20, limg, false);
	wxImage raImg(20, 20, rimg, false);

	wxBitmap labImg(laImg);
	wxBitmap rabImg(raImg);

	m_pXPosUpBtn_p->SetBitmapLabel(rabImg);
	m_pYPosUpBtn_p->SetBitmapLabel(rabImg);
	m_pZPosUpBtn_p->SetBitmapLabel(rabImg);

	m_pXPosDownBtn_p->SetBitmapLabel(labImg);
	m_pYPosDownBtn_p->SetBitmapLabel(labImg);
	m_pZPosDownBtn_p->SetBitmapLabel(labImg);

	m_pXPosUpBtn_r->SetBitmapLabel(rabImg);
	m_pYPosUpBtn_r->SetBitmapLabel(rabImg);
	m_pZPosUpBtn_r->SetBitmapLabel(rabImg);

	m_pXPosDownBtn_r->SetBitmapLabel(labImg);
	m_pYPosDownBtn_r->SetBitmapLabel(labImg);
	m_pZPosDownBtn_r->SetBitmapLabel(labImg);

	m_pXPosUpBtn_c->SetBitmapLabel(rabImg);
	m_pYPosUpBtn_c->SetBitmapLabel(rabImg);
	m_pZPosUpBtn_c->SetBitmapLabel(rabImg);

	m_pXPosDownBtn_c->SetBitmapLabel(labImg);
	m_pYPosDownBtn_c->SetBitmapLabel(labImg);
	m_pZPosDownBtn_c->SetBitmapLabel(labImg);
	


	Fit();
	FitInside();

}

void fxgenGUICoordinateViewerFrame:: SetupSlider()
{

	VX::Math::vec3 bb_max, bb_min;
	m_vctrl->GetBBox(m_core->GetRoot(), bb_max, bb_min, false);

	m_bbox[0] = bb_min;
	m_bbox[1] = bb_max;

	for(int j=0; j < 3; j++){
		m_slidePitch[j] = (m_bbox[1][j] - m_bbox[0][j]) / 100.0f;
//		m_position[j] = (m_bbox[1][j] + m_bbox[0][j]) / 2.0f;
		m_position[j] = m_bbox[0][j];

	}
	for(int i = 0; i < 9; i++){
		SetSlidePos(i);
		SetPosTxt(i);
		SetPitchTxt(i);
	}
}

void fxgenGUICoordinateViewerFrame:: SetupPara()
{
	VX::Math::vec3 bb_max, bb_min, bb_d_maxmin;
	m_vctrl->GetBBox(m_core->GetRoot(), bb_max, bb_min, false);
	float m_size_100, m_size_10;

	m_bbox[0] = bb_min;
	m_bbox[1] = bb_max;

	wxTextCtrl *text_ctrl = 0; // 設定するテキストコントロール

	bb_d_maxmin = m_bbox[1] - m_bbox[0];

	if(bb_d_maxmin.x < bb_d_maxmin.y && bb_d_maxmin.x < bb_d_maxmin.z)
	{
		m_size_100 = bb_d_maxmin.x / 100.0f;
		m_size_10  = bb_d_maxmin.x / 10.0f;

	}
	else if(bb_d_maxmin.y < bb_d_maxmin.x && bb_d_maxmin.y < bb_d_maxmin.z)
	{
		m_size_100 = bb_d_maxmin.y / 100.0f;
		m_size_10  = bb_d_maxmin.x / 10.0f;
	}
	else
	{
		m_size_100 = bb_d_maxmin.z / 100.0f;
		m_size_10  = bb_d_maxmin.x / 10.0f;
	}

	//point
	text_ctrl = m_pSizeTxt_p;
	text_ctrl->SetValue(toStr(m_size_100));

}

void fxgenGUICoordinateViewerFrame::ReLoad_p()
{
	wxArrayInt ar = m_pPointGrid_p->GetSelectedRows();
	wxASSERT(ar.GetCount()==1 || ar.GetCount()==0);

	int n = static_cast<int>(ar.GetCount());
	if(n==0){
		return;
	}

	wxString posX_p = m_pXPosTxt_p->GetValue();
	wxString posY_p = m_pYPosTxt_p->GetValue();
	wxString posZ_p = m_pZPosTxt_p->GetValue();
	wxString size_p = m_pSizeTxt_p->GetValue();

	int sel = ar[0];//先頭
	wxString name  = m_pPointGrid_p->GetCellValue(sel, 0);

	//ToDoポリゴンデータの再読み込み
	m_core->ReLoadPolygon_p(name,posX_p,posY_p,posZ_p,size_p);

		
		
	//表の書換え
	wxString loadcoord = "(" + posX_p + "," + posY_p + "," + posZ_p + ")";

	int row = m_pPointGrid_p->GetCursorRow();
	m_pPointGrid_p->SetCellValue(row,1,loadcoord);

	//ツリーや画面更新
	m_gui->UpdateTree(false);
	UI::vxWindow::GetTopWindow()->Draw();
}

void fxgenGUICoordinateViewerFrame::ReLoad_r()
{
		wxArrayInt ar = m_pRectangularGrid_r->GetSelectedRows();
		wxASSERT(ar.GetCount()==1 || ar.GetCount()==0);

		int n = static_cast<int>(ar.GetCount());
		if(n==0){
			return;
		}

		wxString posX_r = m_pXPosTxt_r->GetValue();
		wxString posY_r = m_pYPosTxt_r->GetValue();
		wxString posZ_r = m_pZPosTxt_r->GetValue();
		wxString width_r = m_pWidthTxt_r->GetValue();
		wxString depth_r = m_pDepthTxt_r->GetValue();
		wxString height_r = m_pHeightTxt_r->GetValue();
		wxString nor_vecX_r = m_pOrientationVecXTxt_r->GetValue();
		wxString nor_vecY_r = m_pOrientationVecYTxt_r->GetValue();
		wxString nor_vecZ_r = m_pOrientationVecZTxt_r->GetValue();
		wxString dir_vecX_r = m_pDirVecXTxt_r->GetValue();
		wxString dir_vecY_r = m_pDirVecYTxt_r->GetValue();
		wxString dir_vecZ_r = m_pDirVecZTxt_r->GetValue();

		int sel = ar[0];//先頭
		wxString name  = m_pRectangularGrid_r->GetCellValue(sel, 0);

		//ToDoポリゴンデータの再読み込み
		m_core->ReLoadPolygon_r(name,posX_r,posY_r,posZ_r,width_r,
						depth_r,height_r,nor_vecX_r,nor_vecY_r,nor_vecZ_r,
						dir_vecX_r,dir_vecY_r,dir_vecZ_r);

		
		
		//表の書換え
		wxString loadcoord = "(" + posX_r + "," + posY_r + "," + posZ_r + ")";

		int row = m_pRectangularGrid_r->GetCursorRow();
		m_pRectangularGrid_r->SetCellValue(row,1,loadcoord);
}

void fxgenGUICoordinateViewerFrame::ReLoad_c()
{
		wxArrayInt ar = m_pCylinderGrid_c->GetSelectedRows();
		wxASSERT(ar.GetCount()==1 || ar.GetCount()==0);

		int n = static_cast<int>(ar.GetCount());
		if(n==0){
			return;
		}

		wxString posX_c = m_pXPosTxt_c->GetValue();
		wxString posY_c = m_pYPosTxt_c->GetValue();
		wxString posZ_c = m_pZPosTxt_c->GetValue();
		wxString bossRad_c = m_pBossRadTxt_c->GetValue();
		wxString depth_c = m_pDepthTxt_c->GetValue();
		wxString fanRad_c = m_pFanRadiusTxt_c->GetValue();
		wxString nor_vecX_c = m_pOrientationVecXTxt_c->GetValue();
		wxString nor_vecY_c = m_pOrientationVecYTxt_c->GetValue();
		wxString nor_vecZ_c = m_pOrientationVecZTxt_c->GetValue();

		int sel = ar[0];//先頭
		wxString name  = m_pCylinderGrid_c->GetCellValue(sel, 0);

		//ToDoポリゴンデータの再読み込み
		m_core->ReLoadPolygon_c(name,posX_c,posY_c,posZ_c,bossRad_c,depth_c,fanRad_c,nor_vecX_c,nor_vecY_c,nor_vecZ_c);
		
		//表の書換え
		wxString loadcoord = "(" + posX_c + "," + posY_c + "," + posZ_c + ")";

		int row = m_pCylinderGrid_c->GetCursorRow();
		m_pCylinderGrid_c->SetCellValue(row,1,loadcoord);
}

void fxgenGUICoordinateViewerFrame::OnActivate( wxActivateEvent& event )
{

	VXLogI("fxgenGUICoordinateViewerFrame::OnActivate\n");
	event.Skip();
}

void fxgenGUICoordinateViewerFrame::OnActivateApp( wxActivateEvent& event )
{

	VXLogI("fxgenGUICoordinateViewerFrame::OnActivateApp\n");
	event.Skip();
}

void fxgenGUICoordinateViewerFrame::OnClose( wxCloseEvent& event )
{
	VXLogI("fxgenGUISD_Frame::OnClose\n");
	//隠すだけ
	this->Show(false);
}

void fxgenGUICoordinateViewerFrame::m_pCloseBtnOnButtonClick( wxCommandEvent& event )
{
	VXLogI("fxgenGUISD_Frame::m_pCloseBtnOnButtonClick\n");
	//TODO:fuchi skipが悪影響がないか後でチェック

	this->Show(false);

}

void fxgenGUICoordinateViewerFrame::m_notebookOnNotebookPageChanged( wxNotebookEvent& event )
{
// TODO: Implement m_notebookOnNotebookPageChanged
	int selectindex = event.GetSelection();
    wxWindow* w = m_notebook4->GetPage(selectindex);
	//macosではページチェンジ時にページが表示されないバグがあるため明示的にshowする
    w->Show(true);
}

void fxgenGUICoordinateViewerFrame::m_notebookOnNotebookPageChanging( wxNotebookEvent& event )
{
// TODO: Implement m_notebookOnNotebookPageChanging
	//何もしない
}

void fxgenGUICoordinateViewerFrame::OnXPosDown_p( wxCommandEvent& event )
{
// TODO: Implement OnXPosDown_p

	u32 axis = 0;
	// 新しいPositionの計算
	m_position[axis] = GetLimitedPos(m_position[axis] - m_slidePitch[axis], axis);

	// スライダー、Positionの位置を変更
	SetPosTxt(axis);
	SetSlidePos(axis);


	// 再描画
	UI::vxWindow::GetTopWindow()->Draw();
}

void fxgenGUICoordinateViewerFrame::OnXSlideScroll_p( wxScrollEvent& event )
{
// TODO: Implement OnXSlideScroll_p

	f32 slidePos = static_cast<f32>(m_pXSlider_p->GetValue());
	const int axis = 0;

	// Positionテキストのアップデート
	SetPosTxt(axis);
	UI::vxWindow::GetTopWindow()->Draw();

}

void fxgenGUICoordinateViewerFrame::OnXSlideRelease_p( wxScrollEvent& event )
{
// TODO: Implement OnXSlideRelease_p
}

void fxgenGUICoordinateViewerFrame::OnXSlideDrag_p( wxScrollEvent& event )
{
// TODO: Implement OnXSlideDrag_p

	f32 slidePos = static_cast<f32>(m_pXSlider_p->GetValue());
	const int axis = 0;
	// Positionのアップデート
	m_position[axis] = m_bbox[0][axis] + event.GetPosition() * m_slidePitch[axis];

	// Positionテキストのアップデート
	SetPosTxt(axis);
	UI::vxWindow::GetTopWindow()->Draw();

//	もし、行が選択されていたら、geometryを抽出し、表および、パラメータを書き換える
	if(m_pPointGrid_p->GetSelectedRows().Count() != 0)
	{
		ReLoad_p();
	}

}

void fxgenGUICoordinateViewerFrame::OnXPosUp_p( wxCommandEvent& event )
{
// TODO: Implement OnXPosUp_p
//	if(!m_ctrl) return;

	u32 axis = 0;
	// 新しいPositionの計算
	m_position[axis] = GetLimitedPos(m_position[axis] + m_slidePitch[axis], axis);

	// スライダー、Positionの位置を変更
	SetPosTxt(axis);
	SetSlidePos(axis);

	// 再描画
	UI::vxWindow::GetTopWindow()->Draw();
}

void fxgenGUICoordinateViewerFrame::m_pXPosTxt_pOnKillFocus(wxFocusEvent& event)
{
	updatePosTxt(event.GetId());
	event.Skip();
}

void fxgenGUICoordinateViewerFrame::m_pXPitchTxt_pOnKillFocus(wxFocusEvent& event)
{
	updatePitchTxt(event.GetId());
	event.Skip();
}

void fxgenGUICoordinateViewerFrame::m_pYPosTxt_pOnKillFocus(wxFocusEvent& event)
{
	updatePosTxt(event.GetId());
	event.Skip();
}

void fxgenGUICoordinateViewerFrame::m_pYPitchTxt_pOnKillFocus(wxFocusEvent& event)
{
	updatePitchTxt(event.GetId());
	event.Skip();
}
void fxgenGUICoordinateViewerFrame::m_pZPosTxt_pOnKillFocus(wxFocusEvent& event)
{
	updatePosTxt(event.GetId());
	event.Skip();
}

void fxgenGUICoordinateViewerFrame::m_pZPitchTxt_pOnKillFocus(wxFocusEvent& event)
{
	updatePitchTxt(event.GetId());
	event.Skip();
}

void fxgenGUICoordinateViewerFrame::m_pSizeTxt_pOnKillFocus(wxFocusEvent& event)
{
	updateParaTxt(event.GetId());
	event.Skip();
}

void fxgenGUICoordinateViewerFrame::OnPosTxt_p( wxCommandEvent& event )
{
// TODO: Implement OnPosTxt_p
	updatePosTxt(event.GetId());
}

void fxgenGUICoordinateViewerFrame::OnPitchTxt_p( wxCommandEvent& event )
{
// TODO: Implement OnPitchTxt_p
	updatePitchTxt(event.GetId());
}

void fxgenGUICoordinateViewerFrame::OnYPosDown_p( wxCommandEvent& event )
{
// TODO: Implement OnYPosDown_p

	u32 axis = 1;
	// 新しいPositionの計算
	m_position[axis] = GetLimitedPos(m_position[axis] - m_slidePitch[axis], axis);

	// スライダー、Positionの位置を変更
	SetPosTxt(axis);
	SetSlidePos(axis);


	// 再描画
	UI::vxWindow::GetTopWindow()->Draw();
}

void fxgenGUICoordinateViewerFrame::OnYSlideScroll_p( wxScrollEvent& event )
{
// TODO: Implement OnYSlideScroll_p
//	if(!m_ctrl) return;

	f32 slidePos = static_cast<f32>(m_pYSlider_p->GetValue());
	const int axis = 1;

	// Positionテキストのアップデート
	SetPosTxt(axis);
	UI::vxWindow::GetTopWindow()->Draw();
}

void fxgenGUICoordinateViewerFrame::OnYSlideRelease_p( wxScrollEvent& event )
{
// TODO: Implement OnYSlideRelease_p
}

void fxgenGUICoordinateViewerFrame::OnYSlideDrag_p( wxScrollEvent& event )
{
// TODO: Implement OnYSlideDrag_p
//	if(!m_ctrl) return;

	f32 slidePos = static_cast<f32>(m_pYSlider_p->GetValue());
	const int axis = 1;

	m_position[axis] = m_bbox[0][axis] + event.GetPosition() * m_slidePitch[axis];

	// Positionテキストのアップデート
	SetPosTxt(axis);
	UI::vxWindow::GetTopWindow()->Draw();

	//	もし、行が選択されていたら、geometryを抽出し、表および、パラメータを書き換える
	if(m_pPointGrid_p->GetSelectedRows().Count() != 0)
	{
		//更新処理
		ReLoad_p();

	}
}

void fxgenGUICoordinateViewerFrame::OnYPosUp_p( wxCommandEvent& event )
{
// TODO: Implement OnYPosUp_p

	u32 axis = 1;
	// 新しいPositionの計算
	m_position[axis] = GetLimitedPos(m_position[axis] + m_slidePitch[axis], axis);

	// スライダー、Positionの位置を変更
	SetPosTxt(axis);
	SetSlidePos(axis);


	// 再描画
	UI::vxWindow::GetTopWindow()->Draw();
}

void fxgenGUICoordinateViewerFrame::OnZPosDown_p( wxCommandEvent& event )
{
// TODO: Implement OnZPosDown_p


	u32 axis = 2;
	// 新しいPositionの計算
	m_position[axis] = GetLimitedPos(m_position[axis] - m_slidePitch[axis], axis);

	// スライダー、Positionの位置を変更
	SetPosTxt(axis);
	SetSlidePos(axis);


	// 再描画
	UI::vxWindow::GetTopWindow()->Draw();
}

void fxgenGUICoordinateViewerFrame::OnZSlideScroll_p( wxScrollEvent& event )
{
// TODO: Implement OnZSlideScroll_p

	f32 slidePos = static_cast<f32>(m_pZSlider_p->GetValue());
	const int axis = 2;
	
	// Positionテキストのアップデート
	SetPosTxt(axis);
	UI::vxWindow::GetTopWindow()->Draw();
}

void fxgenGUICoordinateViewerFrame::OnZSlideRelease_p( wxScrollEvent& event )
{
// TODO: Implement OnZSlideRelease_p
}

void fxgenGUICoordinateViewerFrame::OnZSlideDrag_p( wxScrollEvent& event )
{
// TODO: Implement OnZSlideDrag_p

	f32 slidePos = static_cast<f32>(m_pZSlider_p->GetValue());
	const int axis = 2;
	// Positionのアップデート
	m_position[axis] = m_bbox[0][axis] + event.GetPosition() * m_slidePitch[axis];

	// Positionテキストのアップデート
	SetPosTxt(axis);
	UI::vxWindow::GetTopWindow()->Draw();

	//	もし、行が選択されていたら、geometryを抽出し、表および、パラメータを書き換える
	if(m_pPointGrid_p->GetSelectedRows().Count() != 0)
	{
		ReLoad_p();
	}
}

void fxgenGUICoordinateViewerFrame::OnZPosUp_p( wxCommandEvent& event )
{
// TODO: Implement OnZPosUp_p

	u32 axis = 2;
	// 新しいPositionの計算
	m_position[axis] = GetLimitedPos(m_position[axis] + m_slidePitch[axis], axis);

	// スライダー、Positionの位置を変更
	SetPosTxt(axis);
	SetSlidePos(axis);


	// 再描画
	UI::vxWindow::GetTopWindow()->Draw();

}

void fxgenGUICoordinateViewerFrame::OnSizeTxt_p( wxCommandEvent& event )
{
// TODO: Implement OnSizeTxt_p
	updateParaTxt(event.GetId());

}

void fxgenGUICoordinateViewerFrame::m_button_list_AddButtonClick_p( wxCommandEvent& event )
{
// TODO: Implement m_button_list_AddButtonClick_p

	UI::vxWindow* win = UI::vxWindow::GetTopWindow();

	wxBusyCursor wait;

	//ポリゴンデータ(座標値,サイズ)を取得する
	wxString posX_p = m_pXPosTxt_p->GetValue();
	wxString posY_p = m_pYPosTxt_p->GetValue();
	wxString posZ_p = m_pZPosTxt_p->GetValue();
	wxString size_p = m_pSizeTxt_p->GetValue();

	const int rowID = GetFreeId_p();
	wxString row_p = toStr_int(rowID);

	//ポリゴン生成 
	m_core->LoadPolygon_p(posX_p,posY_p,posZ_p,size_p,row_p);
	
	m_gui->UpdateTree(false);
	UI::vxWindow::GetTopWindow()->Draw();
	
	// GUIテーブル再構築
	CreateGuiByPolygon_p();

}

void fxgenGUICoordinateViewerFrame::CreateGuiByPolygon_p()
{
	setGuiData_p();
}

/**
*	@brief データをテーブルに設定する
*/
void fxgenGUICoordinateViewerFrame::setGuiData_p()
{
	//select cell
	// 行選択モード
	wxGrid::wxGridSelectionModes mode = wxGrid::wxGridSelectRows;//wxGrid::wxGridSelectCells;
	m_pPointGrid_p->SetSelectionMode( mode );

	//選択時の行の色と背景色
	m_pPointGrid_p->SetSelectionForeground(wxColour(204,102,0));
	m_pPointGrid_p->SetSelectionBackground(wxColour(72,31,0));

	//add対象(座標値,サイズ)を取得する
	wxString posX_p = m_pXPosTxt_p->GetValue();
	wxString posY_p = m_pYPosTxt_p->GetValue();
	wxString posZ_p = m_pZPosTxt_p->GetValue();
	wxString size_p = m_pSizeTxt_p->GetValue();

	// previous grid data clear
	const int rowID = GetFreeId_p();
	const int row = m_pPointGrid_p->GetRows();

	m_pPointGrid_p->EnableEditing(false);
	b_creating_grid = true;
	m_pPointGrid_p->AppendRows(1);

	//grid　に行追加
	AddRow_p(rowID,row,posX_p,posY_p,posZ_p,size_p);

	b_creating_grid = false;
	m_pPointGrid_p->EnableEditing(true);
}

void fxgenGUICoordinateViewerFrame::AddRow_p(const int& rowID,const int& row,wxString posX, wxString posY, wxString posZ, wxString size)
{

	wxGridCellAttr* attr_load = new wxGridCellAttr();
	attr_load->SetEditor(	new wxGridCellBoolEditor());
	attr_load->SetRenderer( new wxGridCellBoolRenderer());

	wxString loadcoord = "(" + posX + "," + posY + "," + posZ + ")";


	m_pPointGrid_p->SetCellValue(row,0,"Point_"+toStr_int(rowID));
	m_pPointGrid_p->SetCellValue(row,1,loadcoord);
	m_pPointGrid_p->SetCellValue(row,2,size);

	//Read Only
	m_pPointGrid_p->SetReadOnly(row,0,true);
	m_pPointGrid_p->SetReadOnly(row,1,true);
	m_pPointGrid_p->SetReadOnly(row,2,true);

}

void fxgenGUICoordinateViewerFrame::m_button_list_ClearButtonClick_p( wxCommandEvent& event )
{
// TODO: Implement m_button_list_ClearButtonClick_p
	//現在のステージングをデータクリア
	wxArrayInt ar = m_pPointGrid_p->GetSelectedRows();
	wxASSERT(ar.GetCount()==1 || ar.GetCount()==0);

	int n = static_cast<int>(ar.GetCount());
	if(n==0){
		return;
	}

	int sel = ar[0];//先頭
	wxString name  = m_pPointGrid_p->GetCellValue(sel, 0);
	m_pPointGrid_p->DeleteRows(sel);

	//scean graphから削除
	m_core->DeletePolygon(name);

	UI::vxWindow::GetTopWindow()->Draw();

	
}

void fxgenGUICoordinateViewerFrame::m_button_list_AllClearButtonClick_p( wxCommandEvent& event )
{
// TODO: Implement m_button_list_AllClearButtonClick_p
	int row = m_pPointGrid_p->GetRows();

	//ポリゴンをクリア
	const int lbc = m_pPointGrid_p->GetRows();
	bool find = false;
	for (int i = 0; i < lbc; ++i){
		wxString name = m_pPointGrid_p->GetCellValue(i, 0);

		//scean graphから削除
		m_core->DeletePolygon(name);
		UI::vxWindow::GetTopWindow()->Draw();
	}

	//gridデータをクリア
	if(row>0){
		m_pPointGrid_p->DeleteRows(0,row);
	}
}

void fxgenGUICoordinateViewerFrame::m_pPointGrid_pOnGridSelectCell( wxGridEvent& event )
{
// TODO: Implement m_pPointGrid_pOnGridSelectCell
	wxArrayInt ar = m_pPointGrid_p->GetSelectedRows();
	wxASSERT(ar.GetCount()==1 || ar.GetCount()==0);

	int n = static_cast<int>(ar.GetCount());
	if(n==0){
		return;
	}

	int sel = ar[0];//先頭
	wxString coord = m_pPointGrid_p->GetCellValue(sel,1);
//	wxString coord = "10.1111,30.000,50.111";
	wxString size  = m_pPointGrid_p->GetCellValue(sel,2);

	wxTextCtrl *text_ctrl = 0; // 設定するテキストコントロール
	
	//textboxに設定

	char buffer[256];
	//size_t n = sizeof( buffer );
	memset( buffer, '\0', sizeof( buffer ) );
	strcpy( buffer, coord);
	
	char cpos_X[100];
	char cpos_Y[100];
	char cpos_Z[100];

	sscanf( buffer, "(%s", cpos_X );

	char *p = strchr( cpos_X, ',' );
	if( p == NULL ) {
		return;
	}
	else {
		*p ='\0';
		strcpy( cpos_Y, p + 1 );
		p = strchr( cpos_Y, ',' );
		if( p == NULL ) {
			return;
		}
		else {
			*p ='\0';
			strcpy( cpos_Z, p + 1 );
			p = strchr( cpos_Z, ')');
			*p = '\0';
		}
	}

	wxString pos_X = wxString(cpos_X);
	wxString pos_Y = wxString(cpos_Y);
	wxString pos_Z = wxString(cpos_Z);


	text_ctrl = m_pXPosTxt_p;
	text_ctrl->SetValue(pos_X);
	if(!toFloat(pos_X,m_position[0])){
		return;
	}
	SetSlidePos(0);

	text_ctrl = m_pYPosTxt_p;
	text_ctrl->SetValue(pos_Y);
	if(!toFloat(pos_Y,m_position[1])){
		return;
	}
	SetSlidePos(1);

	text_ctrl = m_pZPosTxt_p;
	text_ctrl->SetValue(pos_Z);
	if(!toFloat(pos_Z,m_position[2])){
		return;
	}
	SetSlidePos(2);

	updatePosTxt(event.GetId());

	text_ctrl = m_pSizeTxt_p;
	text_ctrl->SetValue(size);


}

bool fxgenGUICoordinateViewerFrame::toFloat( const wxString& number , float& retVal)
{
	double d=0;
	if(!number.ToDouble(&d)){
		return false;
	}
	
	retVal = static_cast<float>(d);
	return true;
}

void fxgenGUICoordinateViewerFrame::OnXPosDown_r( wxCommandEvent& event )
{
// TODO: Implement OnXPosDown_r

	u32 axis = 0;
	// 新しいPositionの計算
	m_position[axis] = GetLimitedPos(m_position[axis] - m_slidePitch[axis], axis);

	u32 i= 3;
	// スライダー、Positionの位置を変更
	SetPosTxt(i);
	SetSlidePos(i);

	// 再描画
	UI::vxWindow::GetTopWindow()->Draw();

}

void fxgenGUICoordinateViewerFrame::OnXSlideScroll_r( wxScrollEvent& event )
{
// TODO: Implement OnXSlideScroll_r

	f32 slidePos = static_cast<f32>(m_pXSlider_r->GetValue());
	const int axis = 3;

	// Positionテキストのアップデート
	SetPosTxt(axis);
	UI::vxWindow::GetTopWindow()->Draw();

}

void fxgenGUICoordinateViewerFrame::OnXSlideRelease_r( wxScrollEvent& event )
{
// TODO: Implement OnXSlideRelease_r
}

void fxgenGUICoordinateViewerFrame::OnXSlideDrag_r( wxScrollEvent& event )
{
// TODO: Implement OnXSlideDrag_r

	f32 slidePos = static_cast<f32>(m_pXSlider_r->GetValue());
	const int axis = 0;
	// Positionのアップデート

	m_position[axis] = m_bbox[0][axis] + event.GetPosition() * m_slidePitch[axis];

	u32 i= 3;
	// Positionテキストのアップデート
	SetPosTxt(i);
	UI::vxWindow::GetTopWindow()->Draw();

	//	もし、行が選択されていたら、geometryを抽出し、表および、パラメータを書き換える
	if(m_pRectangularGrid_r->GetSelectedRows().Count() != 0)
	{
		ReLoad_r();
	}

}

void fxgenGUICoordinateViewerFrame::OnXPosUp_r( wxCommandEvent& event )
{
// TODO: Implement OnXPosUp_r

	u32 axis = 0;
	// 新しいPositionの計算
	m_position[axis] = GetLimitedPos(m_position[axis] + m_slidePitch[axis], axis);

	u32 i= 3;
	// スライダー、Positionの位置を変更
	SetPosTxt(i);
	SetSlidePos(i);

	// 再描画
	UI::vxWindow::GetTopWindow()->Draw();
}

void fxgenGUICoordinateViewerFrame::m_pXPosTxt_rOnKillFocus(wxFocusEvent& event)
{
	updatePosTxt(event.GetId());
	event.Skip();
}

void fxgenGUICoordinateViewerFrame::m_pXPitchTxt_rOnKillFocus(wxFocusEvent& event)
{
	updatePitchTxt(event.GetId());
	event.Skip();
}

void fxgenGUICoordinateViewerFrame::m_pYPosTxt_rOnKillFocus(wxFocusEvent& event)
{
	updatePosTxt(event.GetId());
	event.Skip();
}

void fxgenGUICoordinateViewerFrame::m_pYPitchTxt_rOnKillFocus(wxFocusEvent& event)
{
	updatePitchTxt(event.GetId());
	event.Skip();
}
void fxgenGUICoordinateViewerFrame::m_pZPosTxt_rOnKillFocus(wxFocusEvent& event)
{
	updatePosTxt(event.GetId());
	event.Skip();
}

void fxgenGUICoordinateViewerFrame::m_pZPitchTxt_rOnKillFocus(wxFocusEvent& event)
{
	updatePitchTxt(event.GetId());
	event.Skip();
}

void fxgenGUICoordinateViewerFrame::m_pWidthTxt_rOnKillFocus(wxFocusEvent& event)
{
	updateParaTxt(event.GetId());
	event.Skip();
}
void fxgenGUICoordinateViewerFrame::m_pDepthTxt_rOnKillFocus(wxFocusEvent& event)
{
	updateParaTxt(event.GetId());
	event.Skip();
}
void fxgenGUICoordinateViewerFrame::m_pHeightTxt_rOnKillFocus(wxFocusEvent& event)
{
	updateParaTxt(event.GetId());
	event.Skip();
}
void fxgenGUICoordinateViewerFrame::m_pOrientationVecXTxt_rOnKillFocus(wxFocusEvent& event)
{
	updateParaTxt(event.GetId());
	event.Skip();
}
void fxgenGUICoordinateViewerFrame::m_pOrientationVecYTxt_rOnKillFocus(wxFocusEvent& event)
{
	updateParaTxt(event.GetId());
	event.Skip();
}
void fxgenGUICoordinateViewerFrame::m_pOrientationVecZTxt_rOnKillFocus(wxFocusEvent& event)
{
	updateParaTxt(event.GetId());
	event.Skip();
}

void fxgenGUICoordinateViewerFrame::OnPosTxt_r( wxCommandEvent& event )
{
// TODO: Implement OnPosTxt_r

	updatePosTxt(event.GetId());

}

void fxgenGUICoordinateViewerFrame::OnPitchTxt_r( wxCommandEvent& event )
{
// TODO: Implement OnPitchTxt_r
	updatePitchTxt(event.GetId());
}

void fxgenGUICoordinateViewerFrame::OnYPosDown_r( wxCommandEvent& event )
{
// TODO: Implement OnYPosDown_r
	u32 axis = 1;
	// 新しいPositionの計算
	m_position[axis] = GetLimitedPos(m_position[axis] - m_slidePitch[axis], axis);

	u32 i= 4;
	// スライダー、Positionの位置を変更
	SetPosTxt(i);
	SetSlidePos(i);

	// 再描画
	UI::vxWindow::GetTopWindow()->Draw();

}

void fxgenGUICoordinateViewerFrame::OnYSlideScroll_r( wxScrollEvent& event )
{
// TODO: Implement OnYSlideScroll_r

	f32 slidePos = static_cast<f32>(m_pYSlider_r->GetValue());
	const int axis = 1;

	u32 i= 4;
	// Positionテキストのアップデート
	SetPosTxt(i);
	UI::vxWindow::GetTopWindow()->Draw();
}

void fxgenGUICoordinateViewerFrame::OnYSlideRelease_r( wxScrollEvent& event )
{
// TODO: Implement OnYSlideRelease_r

}

void fxgenGUICoordinateViewerFrame::OnYSlideDrag_r( wxScrollEvent& event )
{
// TODO: Implement OnYSlideDrag_r
//	if(!m_ctrl) return;

	f32 slidePos = static_cast<f32>(m_pYSlider_r->GetValue());
	const int axis = 1;
	// Positionのアップデート

	m_position[axis] = m_bbox[0][axis] + event.GetPosition() * m_slidePitch[axis];

	u32 i= 4;
	// Positionテキストのアップデート
	SetPosTxt(i);
	UI::vxWindow::GetTopWindow()->Draw();

	//	もし、行が選択されていたら、geometryを抽出し、表および、パラメータを書き換える
	if(m_pRectangularGrid_r->GetSelectedRows().Count() != 0)
	{
		ReLoad_r();
	}

}

void fxgenGUICoordinateViewerFrame::OnYPosUp_r( wxCommandEvent& event )
{
// TODO: Implement OnYPosUp_r
	//	if(!m_ctrl) return;

	u32 axis = 1;
	// 新しいPositionの計算
	m_position[axis] = GetLimitedPos(m_position[axis] + m_slidePitch[axis], axis);

	u32 i= 4;
	// スライダー、Positionの位置を変更
	SetPosTxt(i);
	SetSlidePos(i);

	// 再描画
	UI::vxWindow::GetTopWindow()->Draw();

}

void fxgenGUICoordinateViewerFrame::OnZPosDown_r( wxCommandEvent& event )
{
// TODO: Implement OnZPosDown_r

	u32 axis = 2;
	// 新しいPositionの計算
	m_position[axis] = GetLimitedPos(m_position[axis] - m_slidePitch[axis], axis);

	u32 i= 5;
	// スライダー、Positionの位置を変更
	SetPosTxt(i);
	SetSlidePos(i);

	// 再描画
	UI::vxWindow::GetTopWindow()->Draw();

}

void fxgenGUICoordinateViewerFrame::OnZSlideScroll_r( wxScrollEvent& event )
{
// TODO: Implement OnZSlideScroll_r

	f32 slidePos = static_cast<f32>(m_pZSlider_r->GetValue());
	const int axis = 2;

	u32 i= 5;
	SetPosTxt(i);
	UI::vxWindow::GetTopWindow()->Draw();
}

void fxgenGUICoordinateViewerFrame::OnZSlideRelease_r( wxScrollEvent& event )
{
// TODO: Implement OnZSlideRelease_r
}

void fxgenGUICoordinateViewerFrame::OnZSlideDrag_r( wxScrollEvent& event )
{
// TODO: Implement OnZSlideDrag_r

	f32 slidePos = static_cast<f32>(m_pZSlider_r->GetValue());
	const int axis = 2;

	m_position[axis] = m_bbox[0][axis] + event.GetPosition() * m_slidePitch[axis];

	u32 i= 5;
	// Positionテキストのアップデート
	SetPosTxt(i);
	UI::vxWindow::GetTopWindow()->Draw();

	//	もし、行が選択されていたら、geometryを抽出し、表および、パラメータを書き換える
	if(m_pRectangularGrid_r->GetSelectedRows().Count() != 0)
	{
		ReLoad_r();
	}
}

void fxgenGUICoordinateViewerFrame::OnZPosUp_r( wxCommandEvent& event )
{
// TODO: Implement OnZPosUp_r

	u32 axis = 2;
	// 新しいPositionの計算
	m_position[axis] = GetLimitedPos(m_position[axis] + m_slidePitch[axis], axis);

	u32 i= 5;
	// スライダー、Positionの位置を変更
	SetPosTxt(i);
	SetSlidePos(i);

	// 再描画
	UI::vxWindow::GetTopWindow()->Draw();

}

void fxgenGUICoordinateViewerFrame::OnWidthTxt_r( wxCommandEvent& event )
{
// TODO: Implement OnWidthTxt_r
	updateParaTxt(event.GetId());
}

void fxgenGUICoordinateViewerFrame::OnDepthTxt_r( wxCommandEvent& event )
{
// TODO: Implement OnDepthTxt_r
	updateParaTxt(event.GetId());
}

void fxgenGUICoordinateViewerFrame::OnHeightTxt_r( wxCommandEvent& event )
{
// TODO: Implement OnHeightTxt_r
	updateParaTxt(event.GetId());
}

void fxgenGUICoordinateViewerFrame::OnOrientationVectorXTxt_r( wxCommandEvent& event )
{
// TODO: Implement OnOrientationVectorXTxt_r
	updateParaTxt(event.GetId());
}

void fxgenGUICoordinateViewerFrame::OnOrientationVectorYTxt_r( wxCommandEvent& event )
{
// TODO: Implement OnOrientationVectorYTxt_r
	updateParaTxt(event.GetId());
}

void fxgenGUICoordinateViewerFrame::OnOrientationVectorZTxt_r( wxCommandEvent& event )
{
// TODO: Implement OnOrientationVectorZTxt_r
	updateParaTxt(event.GetId());
}
void fxgenGUICoordinateViewerFrame::m_pDirVecXTxt_rOnKillFocus( wxFocusEvent& event )
{
// TODO: Implement m_pDirVecXTxt_rOnKillFocus
	updateParaTxt(event.GetId());
	event.Skip();
}

void fxgenGUICoordinateViewerFrame::OnDirVectorXTxt_r( wxCommandEvent& event )
{
// TODO: Implement OnDirVectorXTxt_r
	updateParaTxt(event.GetId());
}

void fxgenGUICoordinateViewerFrame::m_pDirVecYTxt_rOnKillFocus( wxFocusEvent& event )
{
// TODO: Implement m_pDirVecYTxt_rOnKillFocus
	updateParaTxt(event.GetId());
	event.Skip();
}

void fxgenGUICoordinateViewerFrame::OnDirVectorYTxt_r( wxCommandEvent& event )
{
// TODO: Implement OnDirVectorYTxt_r
	updateParaTxt(event.GetId());
}

void fxgenGUICoordinateViewerFrame::m_pDirVecZTxt_rOnKillFocus( wxFocusEvent& event )
{
// TODO: Implement m_pDirVecZTxt_rOnKillFocus
	updateParaTxt(event.GetId());
	event.Skip();
}

void fxgenGUICoordinateViewerFrame::OnDirVectorZTxt_r( wxCommandEvent& event )
{
// TODO: Implement OnDirVectorZTxt_r
	updateParaTxt(event.GetId());
}

void fxgenGUICoordinateViewerFrame::m_button_list_AddButtonClick_r( wxCommandEvent& event )
{
// TODO: Implement m_button_list_AddButtonClick_r
		UI::vxWindow* win = UI::vxWindow::GetTopWindow();

	wxBusyCursor wait;

	//ポリゴンデータ(座標値,サイズ)を取得する
	wxString posX_r = m_pXPosTxt_r->GetValue();
	wxString posY_r = m_pYPosTxt_r->GetValue();
	wxString posZ_r = m_pZPosTxt_r->GetValue();
	wxString width_r = m_pWidthTxt_r->GetValue();
	wxString depth_r = m_pDepthTxt_r->GetValue();
	wxString height_r = m_pHeightTxt_r->GetValue();
	wxString nor_vecX_r = m_pOrientationVecXTxt_r->GetValue();
	wxString nor_vecY_r = m_pOrientationVecYTxt_r->GetValue();
	wxString nor_vecZ_r = m_pOrientationVecZTxt_r->GetValue();
	wxString dir_vecX_r = m_pDirVecXTxt_r->GetValue();
	wxString dir_vecY_r = m_pDirVecYTxt_r->GetValue();
	wxString dir_vecZ_r = m_pDirVecZTxt_r->GetValue();

	const int rowID = GetFreeId_r();
	wxString row_r = toStr_int(rowID);

	//ポリゴン生成 
	m_core->LoadPolygon_r(posX_r,posY_r,posZ_r,width_r,
						depth_r,height_r,nor_vecX_r,nor_vecY_r,nor_vecZ_r,
						dir_vecX_r,dir_vecY_r,dir_vecZ_r,row_r);
	
	m_gui->UpdateTree(false);
	UI::vxWindow::GetTopWindow()->Draw();
	
	// GUIテーブル再構築
	CreateGuiByPolygon_r();
}

void fxgenGUICoordinateViewerFrame::CreateGuiByPolygon_r()
{
	setGuiData_r();
}

/**
*	@brief データをテーブルに設定する
*/
void fxgenGUICoordinateViewerFrame::setGuiData_r()
{
	//select cell
	// 行選択モード
	wxGrid::wxGridSelectionModes mode = wxGrid::wxGridSelectRows;//wxGrid::wxGridSelectCells;
	m_pRectangularGrid_r->SetSelectionMode( mode );

	//選択時の行の色と背景色
	m_pRectangularGrid_r->SetSelectionForeground(wxColour(204,102,0));
	m_pRectangularGrid_r->SetSelectionBackground(wxColour(72,31,0));

	//add対象(座標値,サイズ)を取得する
	wxString posX_r = m_pXPosTxt_r->GetValue();
	wxString posY_r = m_pYPosTxt_r->GetValue();
	wxString posZ_r = m_pZPosTxt_r->GetValue();
	wxString width_r = m_pWidthTxt_r->GetValue();
	wxString depth_r = m_pDepthTxt_r->GetValue();
	wxString height_r = m_pHeightTxt_r->GetValue();
	wxString nor_vecX_r = m_pOrientationVecXTxt_r->GetValue();
	wxString nor_vecY_r = m_pOrientationVecYTxt_r->GetValue();
	wxString nor_vecZ_r = m_pOrientationVecZTxt_r->GetValue();
	wxString dir_vecX_r = m_pDirVecXTxt_r->GetValue();
	wxString dir_vecY_r = m_pDirVecYTxt_r->GetValue();
	wxString dir_vecZ_r = m_pDirVecZTxt_r->GetValue();


	// previous grid data clear
	const int rowID = GetFreeId_r();
	const int row = m_pRectangularGrid_r->GetRows();
	
	//m_pRectangularGrid_r->EnableEditing(false);
	b_creating_grid = true;
	m_pRectangularGrid_r->AppendRows(1);

	//grid　に行追加
	AddRow_r(rowID,row,posX_r,posY_r,posZ_r,width_r,
			depth_r,height_r,nor_vecX_r,nor_vecY_r,nor_vecZ_r,
			dir_vecX_r,dir_vecY_r,dir_vecZ_r);

	b_creating_grid = false;
	m_pPointGrid_p->EnableEditing(true);
}

void fxgenGUICoordinateViewerFrame::AddRow_r
	(const int& rowID,const int& row,wxString posX,wxString posY,wxString posZ,wxString width,
	wxString depth,wxString height,wxString nor_vecX,wxString nor_vecY,wxString nor_vecZ,
	wxString dir_vecX,wxString dir_vecY,wxString dir_vecZ)
{

	// def attr
	// colum editor
	//col0, Load Flag
	wxGridCellAttr* attr_load = new wxGridCellAttr();
	attr_load->SetEditor(	new wxGridCellBoolEditor());
	attr_load->SetRenderer( new wxGridCellBoolRenderer());


	wxString loadcoord = "(" + posX + "," + posY + "," + posZ + ")";
	wxString norcoord =  "(" + nor_vecX + "," + nor_vecY+ "," + nor_vecZ + ")";
	wxString dircoord =  "(" + dir_vecX + "," + dir_vecY+ "," + dir_vecZ + ")";

	m_pRectangularGrid_r->SetCellValue(row,0,"Rectangular_"+toStr_int(rowID));
	m_pRectangularGrid_r->SetCellValue(row,1,loadcoord);
	m_pRectangularGrid_r->SetCellValue(row,2,width);
	m_pRectangularGrid_r->SetCellValue(row,3,depth);
	m_pRectangularGrid_r->SetCellValue(row,4,height);
	m_pRectangularGrid_r->SetCellValue(row,5,norcoord);
	m_pRectangularGrid_r->SetCellValue(row,6,dircoord);

	//Read Only
	m_pRectangularGrid_r->SetReadOnly(row,0,true);
	m_pRectangularGrid_r->SetReadOnly(row,1,true);
	m_pRectangularGrid_r->SetReadOnly(row,2,true);
	m_pRectangularGrid_r->SetReadOnly(row,3,true);
	m_pRectangularGrid_r->SetReadOnly(row,4,true);
	m_pRectangularGrid_r->SetReadOnly(row,5,true);
	m_pRectangularGrid_r->SetReadOnly(row,6,true);


}



void fxgenGUICoordinateViewerFrame::m_button_list_ClearButtonClick_r( wxCommandEvent& event )
{
// TODO: Implement m_button_list_ClearButtonClick_r
		//現在のステージングをデータクリア
	wxArrayInt ar = m_pRectangularGrid_r->GetSelectedRows();
	wxASSERT(ar.GetCount()==1 || ar.GetCount()==0);

	int n = static_cast<int>(ar.GetCount());
	if(n==0){
		return;
	}

	int sel = ar[0];//先頭
	wxString name  = m_pRectangularGrid_r->GetCellValue(sel, 0);
	m_pRectangularGrid_r->DeleteRows(sel);

	//scean graphから削除
	m_core->DeletePolygon(name);

	UI::vxWindow::GetTopWindow()->Draw();

}

void fxgenGUICoordinateViewerFrame::m_button_list_AllClearButtonClick_r( wxCommandEvent& event )
{
// TODO: Implement m_button_list_AllClearButtonClick_r
	int row = m_pRectangularGrid_r->GetRows();

	//ポリゴンをクリア
	const int lbc = m_pRectangularGrid_r->GetRows();
	bool find = false;
	for (int i = 0; i < lbc; ++i){
		wxString name = m_pRectangularGrid_r->GetCellValue(i, 0);

		//scean graphから削除
		m_core->DeletePolygon(name);
		UI::vxWindow::GetTopWindow()->Draw();
	}

	//gridデータをクリア
	if(row>0){
		m_pRectangularGrid_r->DeleteRows(0,row);
	}
}



void fxgenGUICoordinateViewerFrame::m_pRectangularGrid_rOnGridSelectCell( wxGridEvent& event )
{
// TODO: Implement m_pRectangularGrid_rOnGridSelectCell
	wxArrayInt ar = m_pRectangularGrid_r->GetSelectedRows();
	wxASSERT(ar.GetCount()==1 || ar.GetCount()==0);

	int n = static_cast<int>(ar.GetCount());
	if(n==0){
		return;
	}

	int sel = ar[0];//先頭
	wxString coord = m_pRectangularGrid_r->GetCellValue(sel,1);
	wxString width  = m_pRectangularGrid_r->GetCellValue(sel,2);
	wxString depth  = m_pRectangularGrid_r->GetCellValue(sel,3);
	wxString height  = m_pRectangularGrid_r->GetCellValue(sel,4);
	wxString oriVec  = m_pRectangularGrid_r->GetCellValue(sel,5);
	wxString dirVec  = m_pRectangularGrid_r->GetCellValue(sel,6);

	wxTextCtrl *text_ctrl = 0; // 設定するテキストコントロール
	
	//textboxに設定

	//coordの展開
	char buffer[256];

	memset( buffer, '\0', sizeof( buffer ) );
	strcpy( buffer, coord);
	
	char cpos_X[100];
	char cpos_Y[100];
	char cpos_Z[100];

	sscanf( buffer, "(%s", cpos_X );

	char *p = strchr( cpos_X, ',' );
	if( p == NULL ) {
		return;
	}
	else {
		*p ='\0';
		strcpy( cpos_Y, p + 1 );
		p = strchr( cpos_Y, ',' );
		if( p == NULL ) {
			return;
		}
		else {
			*p ='\0';
			strcpy( cpos_Z, p + 1 );
			p = strchr( cpos_Z, ')');
			*p = '\0';
		}
	}

	wxString pos_X = wxString(cpos_X);
	wxString pos_Y = wxString(cpos_Y);
	wxString pos_Z = wxString(cpos_Z);

	//oriVecの展開
	char buffer_ori[256];

	memset( buffer_ori, '\0', sizeof( buffer_ori ) );
	strcpy( buffer_ori, oriVec);
	
	char coriVec_X[100];
	char coriVec_Y[100];
	char coriVec_Z[100];

	sscanf( buffer_ori, "(%s", coriVec_X );

	char *p2 = strchr( coriVec_X, ',' );
	if( p2 == NULL ) {
		return;
	}
	else {
		*p2 ='\0';
		strcpy( coriVec_Y, p2 + 1 );
		p2 = strchr( coriVec_Y, ',' );
		if( p2 == NULL ) {
			return;
		}
		else {
			*p2 ='\0';
			strcpy( coriVec_Z, p2 + 1 );
			p2 = strchr( coriVec_Z, ')');
			*p2 = '\0';
		}
	}

	wxString oriVec_X = wxString(coriVec_X);
	wxString oriVec_Y = wxString(coriVec_Y);
	wxString oriVec_Z = wxString(coriVec_Z);

	//dirVecの展開
	char buffer_dir[256];

	memset( buffer_dir, '\0', sizeof( buffer_dir ) );
	strcpy( buffer_dir, dirVec);
	
	char cdirVec_X[100];
	char cdirVec_Y[100];
	char cdirVec_Z[100];

	sscanf( buffer_dir, "(%s", cdirVec_X );

	char *p3 = strchr( cdirVec_X, ',' );
	if( p3 == NULL ) {
		return;
	}
	else {
		*p3 ='\0';
		strcpy( cdirVec_Y, p3 + 1 );
		p3 = strchr( cdirVec_Y, ',' );
		if( p3 == NULL ) {
			return;
		}
		else {
			*p3 ='\0';
			strcpy( cdirVec_Z, p3 + 1 );
			p3 = strchr( cdirVec_Z, ')');
			*p3 = '\0';
		}
	}

	wxString dirVec_X = wxString(cdirVec_X);
	wxString dirVec_Y = wxString(cdirVec_Y);
	wxString dirVec_Z = wxString(cdirVec_Z);


	//更新
	text_ctrl = m_pXPosTxt_r;
	text_ctrl->SetValue(pos_X);
	if(!toFloat(pos_X,m_position[0])){
		return;
	}
	SetSlidePos(3);

	text_ctrl = m_pYPosTxt_r;
	text_ctrl->SetValue(pos_Y);
	if(!toFloat(pos_Y,m_position[1])){
		return;
	}
	SetSlidePos(4);

	text_ctrl = m_pZPosTxt_r;
	text_ctrl->SetValue(pos_Z);
	if(!toFloat(pos_Z,m_position[2])){
		return;
	}
	SetSlidePos(5);

	updatePosTxt(event.GetId());

	text_ctrl = m_pWidthTxt_r;
	text_ctrl->SetValue(width);

	text_ctrl = m_pDepthTxt_r;
	text_ctrl->SetValue(depth);

	text_ctrl = m_pHeightTxt_r;
	text_ctrl->SetValue(height);

	text_ctrl = m_pOrientationVecXTxt_r;
	text_ctrl->SetValue(oriVec_X);

	text_ctrl = m_pOrientationVecYTxt_r;
	text_ctrl->SetValue(oriVec_Y);

	text_ctrl = m_pOrientationVecZTxt_r;
	text_ctrl->SetValue(oriVec_Z);

	text_ctrl = m_pDirVecXTxt_r;
	text_ctrl->SetValue(dirVec_X);

	text_ctrl = m_pDirVecYTxt_r;
	text_ctrl->SetValue(dirVec_Y);

	text_ctrl = m_pDirVecZTxt_r;
	text_ctrl->SetValue(dirVec_Z);
}

void fxgenGUICoordinateViewerFrame::OnXPosDown_c( wxCommandEvent& event )
{
// TODO: Implement OnXPosDown_c
	u32 axis = 0;
	// 新しいPositionの計算
	m_position[axis] = GetLimitedPos(m_position[axis] - m_slidePitch[axis], axis);

	u32 i= 6;
	// スライダー、Positionの位置を変更
	SetPosTxt(i);
	SetSlidePos(i);

	// 再描画
	UI::vxWindow::GetTopWindow()->Draw();

}

void fxgenGUICoordinateViewerFrame::OnXSlideScroll_c( wxScrollEvent& event )
{
// TODO: Implement OnXSlideScroll_c
	f32 slidePos = static_cast<f32>(m_pXSlider_c->GetValue());
	const int axis = 6;

	SetPosTxt(axis);
	UI::vxWindow::GetTopWindow()->Draw();

}

void fxgenGUICoordinateViewerFrame::OnXSlideRelease_c( wxScrollEvent& event )
{
// TODO: Implement OnXSlideRelease_c
}

void fxgenGUICoordinateViewerFrame::OnXSlideDrag_c( wxScrollEvent& event )
{
// TODO: Implement OnXSlideDrag_c
	//	if(!m_ctrl) return;

	f32 slidePos = static_cast<f32>(m_pXSlider_c->GetValue());
	const int axis = 0;

	m_position[axis] = m_bbox[0][axis] + event.GetPosition() * m_slidePitch[axis];

	u32 i= 6;
	// Positionテキストのアップデート
	SetPosTxt(i);
	UI::vxWindow::GetTopWindow()->Draw();

	//	もし、行が選択されていたら、geometryを抽出し、表および、パラメータを書き換える
	if(m_pCylinderGrid_c->GetSelectedRows().Count() != 0)
	{
		ReLoad_c();
	}

}

void fxgenGUICoordinateViewerFrame::OnXPosUp_c( wxCommandEvent& event )
{
// TODO: Implement OnXPosUp_c

	u32 axis = 0;
	// 新しいPositionの計算
	m_position[axis] = GetLimitedPos(m_position[axis] + m_slidePitch[axis], axis);

	u32 i= 6;
	// スライダー、Positionの位置を変更
	SetPosTxt(i);
	SetSlidePos(i);

	// 再描画
	UI::vxWindow::GetTopWindow()->Draw();
}

void fxgenGUICoordinateViewerFrame::OnPosTxt_c( wxCommandEvent& event )
{
// TODO: Implement OnPosTxt_c
	updatePosTxt(event.GetId());
}

void fxgenGUICoordinateViewerFrame::OnPitchTxt_c( wxCommandEvent& event )
{
// TODO: Implement OnPitchTxt_c
	updatePitchTxt(event.GetId());
}

void fxgenGUICoordinateViewerFrame::OnYPosDown_c( wxCommandEvent& event )
{
// TODO: Implement OnYPosDown_c
	u32 axis = 1;
	// 新しいPositionの計算
	m_position[axis] = GetLimitedPos(m_position[axis] - m_slidePitch[axis], axis);

	u32 i= 7;
	// スライダー、Positionの位置を変更
	SetPosTxt(i);
	SetSlidePos(i);


	// 再描画
	UI::vxWindow::GetTopWindow()->Draw();
}

void fxgenGUICoordinateViewerFrame::OnYSlideScroll_c( wxScrollEvent& event )
{
// TODO: Implement OnYSlideScroll_c
//	if(!m_ctrl) return;

	f32 slidePos = static_cast<f32>(m_pYSlider_c->GetValue());
	const int axis = 1;

	u32 i= 7;
	// Positionテキストのアップデート
	SetPosTxt(i);
	UI::vxWindow::GetTopWindow()->Draw();
}

void fxgenGUICoordinateViewerFrame::OnYSlideRelease_c( wxScrollEvent& event )
{
// TODO: Implement OnYSlideRelease_c
}

void fxgenGUICoordinateViewerFrame::OnYSlideDrag_c( wxScrollEvent& event )
{
// TODO: Implement OnYSlideDrag_c

	f32 slidePos = static_cast<f32>(m_pYSlider_c->GetValue());
	const int axis = 1;

	m_position[axis] = m_bbox[0][axis] + event.GetPosition() * m_slidePitch[axis];

	u32 i= 7;
	// Positionテキストのアップデート
	SetPosTxt(i);
	UI::vxWindow::GetTopWindow()->Draw();

	if(m_pCylinderGrid_c->GetSelectedRows().Count() != 0)
	{
		ReLoad_c();
	}
}

void fxgenGUICoordinateViewerFrame::OnYPosUp_c( wxCommandEvent& event )
{
// TODO: Implement OnYPosUp_c

	u32 axis = 1;
	// 新しいPositionの計算
	m_position[axis] = GetLimitedPos(m_position[axis] + m_slidePitch[axis], axis);

	u32 i= 7;
	// スライダー、Positionの位置を変更
	SetPosTxt(i);
	SetSlidePos(i);

	// 再描画
	UI::vxWindow::GetTopWindow()->Draw();
}

void fxgenGUICoordinateViewerFrame::OnZPosDown_c( wxCommandEvent& event )
{
// TODO: Implement OnZPosDown_c

	u32 axis = 2;
	// 新しいPositionの計算
	m_position[axis] = GetLimitedPos(m_position[axis] - m_slidePitch[axis], axis);

	u32 i= 8;
	// スライダー、Positionの位置を変更
	SetPosTxt(i);
	SetSlidePos(i);

	// 再描画
	UI::vxWindow::GetTopWindow()->Draw();

}

void fxgenGUICoordinateViewerFrame::OnZSlideScroll_c( wxScrollEvent& event )
{
// TODO: Implement OnZSlideScroll_c

	f32 slidePos = static_cast<f32>(m_pZSlider_c->GetValue());
	const int axis = 2;

	// Positionテキストのアップデート
	u32 i= 8;
	SetPosTxt(i);
	UI::vxWindow::GetTopWindow()->Draw();
}

void fxgenGUICoordinateViewerFrame::OnZSlideRelease_c( wxScrollEvent& event )
{
// TODO: Implement OnZSlideRelease_c
}

void fxgenGUICoordinateViewerFrame::OnZSlideDrag_c( wxScrollEvent& event )
{
// TODO: Implement OnZSlideDrag_c

	f32 slidePos = static_cast<f32>(m_pZSlider_c->GetValue());
	const int axis = 2;

	m_position[axis] = m_bbox[0][axis] + event.GetPosition() * m_slidePitch[axis];

	u32 i= 8;
	// Positionテキストのアップデート
	SetPosTxt(i);
	UI::vxWindow::GetTopWindow()->Draw();

	if(m_pCylinderGrid_c->GetSelectedRows().Count() != 0)
	{
		ReLoad_c();
	}
}

void fxgenGUICoordinateViewerFrame::OnZPosUp_c( wxCommandEvent& event )
{
// TODO: Implement OnZPosUp_c


		u32 axis = 2;
		// 新しいPositionの計算
		m_position[axis] = GetLimitedPos(m_position[axis] + m_slidePitch[axis], axis);

		u32 i= 8;
		// スライダー、Positionの位置を変更
		SetPosTxt(i);
		SetSlidePos(i);

		// 再描画
		UI::vxWindow::GetTopWindow()->Draw();
}

void fxgenGUICoordinateViewerFrame::OnFanRadiusTxt_c( wxCommandEvent& event )
{
// TODO: Implement OnFanTxt_c
	updateParaTxt(event.GetId());
}

void fxgenGUICoordinateViewerFrame::OnDepthTxt_c( wxCommandEvent& event )
{
// TODO: Implement OnDepthTxt_c
	updateParaTxt(event.GetId());
}

void fxgenGUICoordinateViewerFrame::OnBossRadiusTxt_c( wxCommandEvent& event )
{
// TODO: Implement OnBossRadiusTxt_c
	updateParaTxt(event.GetId());
}

void fxgenGUICoordinateViewerFrame::OnOrientationVectorXTxt_c( wxCommandEvent& event )
{
// TODO: Implement OnOrientationVectorXTxt_c
	updateParaTxt(event.GetId());
}
void fxgenGUICoordinateViewerFrame::OnOrientationVectorYTxt_c( wxCommandEvent& event )
{
// TODO: Implement OnOrientationVectorYTxt_c
	updateParaTxt(event.GetId());
}
void fxgenGUICoordinateViewerFrame::OnOrientationVectorZTxt_c( wxCommandEvent& event )
{
// TODO: Implement OnOrientationVectorZTxt_c
	updateParaTxt(event.GetId());
}

void fxgenGUICoordinateViewerFrame::m_button_list_AddButtonClick_c( wxCommandEvent& event )
{
// TODO: Implement m_button_list_AddButtonClick_c
		UI::vxWindow* win = UI::vxWindow::GetTopWindow();

	wxBusyCursor wait;

	//ポリゴンデータ(座標値,サイズ)を取得する
	wxString posX_c = m_pXPosTxt_c->GetValue();
	wxString posY_c = m_pYPosTxt_c->GetValue();
	wxString posZ_c = m_pZPosTxt_c->GetValue();
	wxString bossRad_c = m_pBossRadTxt_c->GetValue();
	wxString depth_c = m_pDepthTxt_c->GetValue();
	wxString fanRad_c = m_pFanRadiusTxt_c->GetValue();
	wxString nor_vecX_c = m_pOrientationVecXTxt_c->GetValue();
	wxString nor_vecY_c = m_pOrientationVecYTxt_c->GetValue();
	wxString nor_vecZ_c = m_pOrientationVecZTxt_c->GetValue();

	const int rowID = GetFreeId_c();
	wxString row_c = toStr_int(rowID);
	
	//ポリゴン生成 
	m_core->LoadPolygon_c(posX_c,posY_c,posZ_c,depth_c,fanRad_c,bossRad_c,
		                  nor_vecX_c,nor_vecY_c,nor_vecZ_c,row_c);
	
	m_gui->UpdateTree(false);
	UI::vxWindow::GetTopWindow()->Draw();
	
	// GUIテーブル再構築
	CreateGuiByPolygon_c();
}

void fxgenGUICoordinateViewerFrame::CreateGuiByPolygon_c()
{
	setGuiData_c();
}

/**
*	@brief データをテーブルに設定する
*/
void fxgenGUICoordinateViewerFrame::setGuiData_c()
{
	//select cell
	// 行選択モード
	wxGrid::wxGridSelectionModes mode = wxGrid::wxGridSelectRows;//wxGrid::wxGridSelectCells;
	m_pCylinderGrid_c->SetSelectionMode( mode );

	//選択時の行の色と背景色
	m_pCylinderGrid_c->SetSelectionForeground(wxColour(204,102,0));
	m_pCylinderGrid_c->SetSelectionBackground(wxColour(72,31,0));

	//add対象(座標値,サイズ)を取得する
	wxString posX_c = m_pXPosTxt_c->GetValue();
	wxString posY_c = m_pYPosTxt_c->GetValue();
	wxString posZ_c = m_pZPosTxt_c->GetValue();
	wxString bossRad_c = m_pBossRadTxt_c->GetValue();
	wxString depth_c = m_pDepthTxt_c->GetValue();
	wxString fanRad_c = m_pFanRadiusTxt_c->GetValue();
	wxString nor_vecX_c = m_pOrientationVecXTxt_c->GetValue();
	wxString nor_vecY_c = m_pOrientationVecYTxt_c->GetValue();
	wxString nor_vecZ_c = m_pOrientationVecZTxt_c->GetValue();


	// previous grid data clear
	const int rowID = GetFreeId_c();
	const int row = m_pCylinderGrid_c->GetRows();
	
	m_pCylinderGrid_c->EnableEditing(false);
	b_creating_grid = true;
	m_pCylinderGrid_c->AppendRows(1);

	//grid　に行追加
	AddRow_c(rowID,row,posX_c,posY_c,posZ_c,bossRad_c,
			depth_c,fanRad_c,nor_vecX_c,nor_vecY_c,nor_vecZ_c);

	b_creating_grid = false;
	m_pCylinderGrid_c->EnableEditing(true);
}

void fxgenGUICoordinateViewerFrame::AddRow_c
	(const int& rowID,const int& row,wxString posX,wxString posY,wxString posZ,wxString fanRad,
	wxString depth,wxString bossRad,wxString nor_vecX,wxString nor_vecY,wxString nor_vecZ)
{

	// def attr
	// colum editor
	//col0, Load Flag
	wxGridCellAttr* attr_load = new wxGridCellAttr();
	attr_load->SetEditor(	new wxGridCellBoolEditor());
	attr_load->SetRenderer( new wxGridCellBoolRenderer());


	wxString loadcoord = "(" + posX + "," + posY + "," + posZ + ")";
	wxString norcoord =  "(" + nor_vecX + "," + nor_vecY+ "," + nor_vecZ + ")";

	m_pCylinderGrid_c->SetCellValue(row,0,"Cylinder_"+toStr_int(rowID));
	m_pCylinderGrid_c->SetCellValue(row,1,loadcoord);
	m_pCylinderGrid_c->SetCellValue(row,2,depth);
	m_pCylinderGrid_c->SetCellValue(row,3,fanRad);
	m_pCylinderGrid_c->SetCellValue(row,4,bossRad);
	m_pCylinderGrid_c->SetCellValue(row,5,norcoord);

	//Read Only
	m_pCylinderGrid_c->SetReadOnly(row,0,true);
	m_pCylinderGrid_c->SetReadOnly(row,1,true);
	m_pCylinderGrid_c->SetReadOnly(row,2,true);
	m_pCylinderGrid_c->SetReadOnly(row,3,true);
	m_pCylinderGrid_c->SetReadOnly(row,4,true);
	m_pCylinderGrid_c->SetReadOnly(row,5,true);

}

void fxgenGUICoordinateViewerFrame::m_button_list_ClearButtonClick_c( wxCommandEvent& event )
{
// TODO: Implement m_button_list_ClearButtonClick_c
	//現在のステージングをデータクリア
	wxArrayInt ar = m_pCylinderGrid_c->GetSelectedRows();
	wxASSERT(ar.GetCount()==1 || ar.GetCount()==0);

	int n = static_cast<int>(ar.GetCount());
	if(n==0){
		return;
	}

	int sel = ar[0];//先頭
	wxString name  = m_pCylinderGrid_c->GetCellValue(sel, 0);
	m_pCylinderGrid_c->DeleteRows(sel);

	//scean graphから削除
	m_core->DeletePolygon(name);
	
	UI::vxWindow::GetTopWindow()->Draw();
}

void fxgenGUICoordinateViewerFrame::m_button_list_AllClearButtonClick_c( wxCommandEvent& event )
{
// TODO: Implement m_button_list_AllClearButtonClick_c
	int row = m_pCylinderGrid_c->GetRows();

	//ポリゴンをクリア
	const int lbc = m_pCylinderGrid_c->GetRows();
	bool find = false;
	for (int i = 0; i < lbc; ++i){
		wxString name = m_pCylinderGrid_c->GetCellValue(i, 0);

		//scean graphから削除
		m_core->DeletePolygon(name);
		UI::vxWindow::GetTopWindow()->Draw();
	}

	//gridデータをクリア
	if(row>0){
		m_pCylinderGrid_c->DeleteRows(0,row);
	}
}

void fxgenGUICoordinateViewerFrame::m_pXPosTxt_cOnKillFocus(wxFocusEvent& event)
{
	updatePosTxt(event.GetId());
	event.Skip();
};
void fxgenGUICoordinateViewerFrame::m_pXPitchTxt_cOnKillFocus(wxFocusEvent& event)
{
	updatePitchTxt(event.GetId());
	event.Skip();
};
void fxgenGUICoordinateViewerFrame::m_pYPosTxt_cOnKillFocus(wxFocusEvent& event)
{
	updatePosTxt(event.GetId());
	event.Skip();
};
void fxgenGUICoordinateViewerFrame::m_pYPitchTxt_cOnKillFocus(wxFocusEvent& event)
{
	updatePitchTxt(event.GetId());
	event.Skip();
};
void fxgenGUICoordinateViewerFrame::m_pZPosTxt_cOnKillFocus(wxFocusEvent& event)
{
	updatePosTxt(event.GetId());
	event.Skip();
};
void fxgenGUICoordinateViewerFrame::m_pZPitchTxt_cOnKillFocus(wxFocusEvent& event)
{
	updatePitchTxt(event.GetId());
	event.Skip();
};
void fxgenGUICoordinateViewerFrame::m_pFanRadiusTxt_cOnKillFocus(wxFocusEvent& event)
{
	updateParaTxt(event.GetId());
	event.Skip(); 
};
void fxgenGUICoordinateViewerFrame::m_pDepthTxt_cOnKillFocus(wxFocusEvent& event)
{
	updateParaTxt(event.GetId());
	event.Skip();
};
void fxgenGUICoordinateViewerFrame::m_pBossRadTxt_cOnKillFocus(wxFocusEvent& event)
{
	updateParaTxt(event.GetId());
	event.Skip();
};
void fxgenGUICoordinateViewerFrame::m_pOrientationVecXTxt_cOnKillFocus(wxFocusEvent& event)
{
	updateParaTxt(event.GetId());
	event.Skip();
};
void fxgenGUICoordinateViewerFrame::m_pOrientationVecYTxt_cOnKillFocus(wxFocusEvent& event)
{
	updateParaTxt(event.GetId());
	event.Skip();
};
void fxgenGUICoordinateViewerFrame::m_pOrientationVecZTxt_cOnKillFocus(wxFocusEvent& event)
{
	updateParaTxt(event.GetId());
	event.Skip();
};

void fxgenGUICoordinateViewerFrame::m_pCylinderGrid_cOnGridSelectCell( wxGridEvent& event )
{
// TODO: Implement m_pCylinderGrid_cOnGridSelectCell
	wxArrayInt ar = m_pCylinderGrid_c->GetSelectedRows();
	wxASSERT(ar.GetCount()==1 || ar.GetCount()==0);

	int n = static_cast<int>(ar.GetCount());
	if(n==0){
		return;
	}
	
	int sel = ar[0];//先頭
	wxString coord = m_pCylinderGrid_c->GetCellValue(sel,1);
	wxString depth  = m_pCylinderGrid_c->GetCellValue(sel,2);
	wxString fanRadius  = m_pCylinderGrid_c->GetCellValue(sel,3);
	wxString bossRadius  = m_pCylinderGrid_c->GetCellValue(sel,4);
	wxString oriVec  = m_pCylinderGrid_c->GetCellValue(sel,5);

	wxTextCtrl *text_ctrl = 0; // 設定するテキストコントロール
	
	//textboxに設定

	//coordの展開
	char buffer[256];

	memset( buffer, '\0', sizeof( buffer ) );
	strcpy( buffer, coord);
	
	char cpos_X[100];
	char cpos_Y[100];
	char cpos_Z[100];

	sscanf( buffer, "(%s", cpos_X );

	char *p = strchr( cpos_X, ',' );
	if( p == NULL ) {
		return;
	}
	else {
		*p ='\0';
		strcpy( cpos_Y, p + 1 );
		p = strchr( cpos_Y, ',' );
		if( p == NULL ) {
			return;
		}
		else {
			*p ='\0';
			strcpy( cpos_Z, p + 1 );
			p = strchr( cpos_Z, ')');
			*p = '\0';
		}
	}

	wxString pos_X = wxString(cpos_X);
	wxString pos_Y = wxString(cpos_Y);
	wxString pos_Z = wxString(cpos_Z);

	//oriVecの展開
	char buffer_ori[256];

	memset( buffer_ori, '\0', sizeof( buffer_ori ) );
	strcpy( buffer_ori, oriVec);
	
	char coriVec_X[100];
	char coriVec_Y[100];
	char coriVec_Z[100];

	sscanf( buffer_ori, "(%s", coriVec_X );

	char *p2 = strchr( coriVec_X, ',' );
	if( p2 == NULL ) {
		return;
	}
	else {
		*p2 ='\0';
		strcpy( coriVec_Y, p2 + 1 );
		p2 = strchr( coriVec_Y, ',' );
		if( p2 == NULL ) {
			return;
		}
		else {
			*p2 ='\0';
			strcpy( coriVec_Z, p2 + 1 );
			p2 = strchr( coriVec_Z, ')');
			*p2 = '\0';
		}
	}

	wxString oriVec_X = wxString(coriVec_X);
	wxString oriVec_Y = wxString(coriVec_Y);
	wxString oriVec_Z = wxString(coriVec_Z);


	//更新
	text_ctrl = m_pXPosTxt_c;
	text_ctrl->SetValue(pos_X);
	if(!toFloat(pos_X,m_position[0])){
		return;
	}
	SetSlidePos(6);

	text_ctrl = m_pYPosTxt_c;
	text_ctrl->SetValue(pos_Y);
	if(!toFloat(pos_Y,m_position[1])){
		return;
	}
	SetSlidePos(7);

	text_ctrl = m_pZPosTxt_c;
	text_ctrl->SetValue(pos_Z);
	if(!toFloat(pos_Z,m_position[2])){
		return;
	}
	SetSlidePos(8);

	updatePosTxt(event.GetId());

	text_ctrl = m_pDepthTxt_c;
	text_ctrl->SetValue(depth);

	text_ctrl = m_pFanRadiusTxt_c;
	text_ctrl->SetValue(fanRadius);

	text_ctrl = m_pBossRadTxt_c;
	text_ctrl->SetValue(bossRadius);

	text_ctrl = m_pOrientationVecXTxt_c;
	text_ctrl->SetValue(oriVec_X);

	text_ctrl = m_pOrientationVecYTxt_c;
	text_ctrl->SetValue(oriVec_Y);

	text_ctrl = m_pOrientationVecZTxt_c;
	text_ctrl->SetValue(oriVec_Z);

}

// 入力されたposition値を範囲内に収めた値に切り詰めて返す
f32 fxgenGUICoordinateViewerFrame::GetLimitedPos(f32 pos, u32 axis)
{
	f32 newpos = pos;
	if (pos < m_bbox[0][axis]) newpos = m_bbox[0][axis];
	if (pos > m_bbox[1][axis]) newpos = m_bbox[1][axis];
	return newpos;
}

// m_positionの値からスライドバーの位置を設定
void fxgenGUICoordinateViewerFrame::SetSlidePos(u32 axis)
{
	wxSlider *slider = 0; // 設定するテキストコントロール
	switch (axis) {
	default:
		// 指定した軸ではない場合は終了
		break;
	case 0: // pointタブ X軸
		slider = m_pXSlider_p;
		axis = 0;
		break;
	case 1: // pointタブ Y軸
		slider = m_pYSlider_p;
		axis = 1;
		break;
	case 2: // pointタブ Z軸
		slider = m_pZSlider_p;
		axis = 2;
		break;
	case 3: // recタブ X軸
		slider = m_pXSlider_r;
		axis = 0;
		break;
	case 4: // recタブ Y軸
		slider = m_pYSlider_r;
		axis = 1;
		break;
	case 5: // recタブ Z軸
		slider = m_pZSlider_r;
		axis = 2;
		break;
	case 6: // cylタブ X軸
		slider = m_pXSlider_c;
		axis = 0;
		break;
	case 7: // cylタブ Y軸
		slider = m_pYSlider_c;
		axis = 1;
		break;
	case 8: // cylタブ Z軸
		slider = m_pZSlider_c;
		axis = 2;
		break;
	}
	int slidePos = static_cast<int>((m_position[axis] - m_bbox[0][axis]) / m_slidePitch[axis]);
	slider->SetValue(slidePos);
}

// m_positionの値からポジションテキストを設定
void fxgenGUICoordinateViewerFrame::SetPosTxt(u32 axis)
{
	wxTextCtrl *text_ctrl = 0; // 設定するテキストコントロール
	switch (axis) {
	default:
		// 指定した軸ではない場合は終了
		break;
	case 0: // pointタブ X軸
		text_ctrl = m_pXPosTxt_p;
		axis = 0;
		break;
	case 1: // pointタブ Y軸
		text_ctrl = m_pYPosTxt_p;
		axis = 1;
		break;
	case 2: // pointタブ Z軸
		text_ctrl = m_pZPosTxt_p;
		axis = 2;
		break;
	case 3: // recタブ X軸
		text_ctrl = m_pXPosTxt_r;
		axis = 0;
		break;
	case 4: // recタブ Y軸
		text_ctrl = m_pYPosTxt_r;
		axis = 1;
		break;
	case 5: // recタブ Z軸
		text_ctrl = m_pZPosTxt_r;
		axis = 2;
		break;
	case 6: // cylタブ X軸
		text_ctrl = m_pXPosTxt_c;
		axis = 0;
		break;
	case 7: // cylタブ Y軸
		text_ctrl = m_pYPosTxt_c;
		axis = 1;
		break;
	case 8: // cylタブ Z軸
		text_ctrl = m_pZPosTxt_c;
		axis = 2;
		break;
	}

	// 座標値で管理する場合はそのまま書く
	text_ctrl->SetValue(toStr(m_position[axis]));


}


wxString fxgenGUICoordinateViewerFrame::toStr(const float& val)const
{
	return fxgenGUICoordinateViewerFrame::FloatFormat(val);
}

wxString fxgenGUICoordinateViewerFrame::FloatFormat(const float& value)
{
	//計算したfloat値を
	//GUIの直接入力の６ケタ（暫定）に合わせて記載
	return wxString::Format("%.6f", value);
	//return wxString::Format("%.2f", value);

}

wxString fxgenGUICoordinateViewerFrame::toStr_int(const int& val)const
{
	return fxgenGUICoordinateViewerFrame::IntFormat(val);
}

wxString fxgenGUICoordinateViewerFrame::IntFormat(const int& value)
{
	//計算したfloat値を
	//GUIの直接入力の６ケタ（暫定）に合わせて記載
	return wxString::Format("%d", value);
	//return wxString::Format("%.2f", value);

}

void fxgenGUICoordinateViewerFrame::updatePosTxt( const int& guiID )
{

	u32 axis = 0;
	switch (guiID) {
	default:
		// 指定したIDではない場合は終了
		return;
	case SLICE_CTRL_XPOS_P_TXT:
		axis = 0;
		break;
	case SLICE_CTRL_YPOS_P_TXT:
		axis = 1;
		break;
	case SLICE_CTRL_ZPOS_P_TXT:
		axis = 2;
		break;
	case SLICE_CTRL_XPOS_R_TXT:
		axis = 3;
		break;
	case SLICE_CTRL_YPOS_R_TXT:
		axis = 4;
		break;
	case SLICE_CTRL_ZPOS_R_TXT:
		axis = 5;
		break;
	case SLICE_CTRL_XPOS_C_TXT:
		axis = 6;
		break;
	case SLICE_CTRL_YPOS_C_TXT:
		axis = 7;
		break;
	case SLICE_CTRL_ZPOS_C_TXT:
		axis = 8;
		break;
	}
	// 値の保存
	SetPosValue(axis, m_slideUnit);
		
	// スライダーバーの位置をPositionに合わせる
	SetSlidePos(axis);

	// 位置の設定
//	m_ctrl->SetPosition(axis, m_position[axis]);
	// 再描画
	UI::vxWindow::GetTopWindow()->Draw();
}

void fxgenGUICoordinateViewerFrame::updateParaTxt( const int& guiID )
{
	// 再描画
	UI::vxWindow::GetTopWindow()->Draw();
}


void fxgenGUICoordinateViewerFrame::SetPosValue(u32 axis, u32 unit)
{
	

	wxTextCtrl *text_ctrl = 0; // 設定するテキストコントロール
	switch (axis) {
	default:
		// 指定した軸ではない場合は終了
		break;
	case 0: // pointタブ X軸
		text_ctrl = m_pXPosTxt_p;
		axis = 0;
		break;
	case 1: // pointタブ Y軸
		text_ctrl = m_pYPosTxt_p;
		axis = 1;
		break;
	case 2: // pointタブ Z軸
		text_ctrl = m_pZPosTxt_p;
		axis = 2;
		break;
	case 3: // recタブ X軸
		text_ctrl = m_pXPosTxt_r;
		axis = 0;
		break;
	case 4: // recタブ Y軸
		text_ctrl = m_pYPosTxt_r;
		axis = 1;
		break;
	case 5: // recタブ Z軸
		text_ctrl = m_pZPosTxt_r;
		axis = 2;
		break;
	case 6: // cylタブ X軸
		text_ctrl = m_pXPosTxt_c;
		axis = 0;
		break;
	case 7: // cylタブ Y軸
		text_ctrl = m_pYPosTxt_c;
		axis = 1;
		break;
	case 8: // cylタブ Z軸
		text_ctrl = m_pZPosTxt_c;
		axis = 2;
		break;
	}

		double val;
		if (!text_ctrl->GetValue().ToDouble(&val)) {
			// 値がおかしい場合は元の値に戻す
			text_ctrl->SetValue(toStr( m_position[axis]));
		}
		f32 coord_pos = static_cast<f32>(val);
		// 範囲制限
		if (coord_pos < m_bbox[0][axis]) coord_pos = m_bbox[0][axis];
		if (coord_pos > m_bbox[1][axis]) coord_pos = m_bbox[1][axis];
		// 値を入れなおす
		text_ctrl->SetValue(toStr( coord_pos));
		m_position[axis] = coord_pos;
}

void fxgenGUICoordinateViewerFrame::updatePitchTxt( const int& guiID )
{
// TODO: Implement OnPitchTxt



	switch (guiID) {
	default:
		// 指定したIDではない場合は終了
		return;
	case SLICE_CTRL_XPITCH_P_TXT:
		SetPitchValue(0);
		break;
	case SLICE_CTRL_YPITCH_P_TXT:
		SetPitchValue(1);
		break;
	case SLICE_CTRL_ZPITCH_P_TXT:
		SetPitchValue(2);
		break;
	case SLICE_CTRL_XPITCH_R_TXT:
		SetPitchValue(3);
		break;
	case SLICE_CTRL_YPITCH_R_TXT:
		SetPitchValue(4);
		break;
	case SLICE_CTRL_ZPITCH_R_TXT:
		SetPitchValue(5);
		break;
	case SLICE_CTRL_XPITCH_C_TXT:
		SetPitchValue(6);
		break;
	case SLICE_CTRL_YPITCH_C_TXT:
		SetPitchValue(7);
		break;
	case SLICE_CTRL_ZPITCH_C_TXT:
		SetPitchValue(8);
		break;
	}

}

// Pitchテキストからm_slidePitchのを設定
void fxgenGUICoordinateViewerFrame::SetPitchValue(u32 axis)
{

	wxTextCtrl *text_ctrl = 0; // 設定するテキストコントロール
	switch (axis) {
	default:
		// 指定した軸ではない場合は終了
		break;
	case 0: // pointタブ X軸
		text_ctrl = m_pXPitchTxt_p;
		axis = 0;
		break;
	case 1: // pointタブ Y軸
		text_ctrl = m_pYPitchTxt_p;
		axis = 1;
		break;
	case 2: // pointタブ Z軸
		text_ctrl = m_pZPitchTxt_p;
		axis = 2;
		break;
	case 3: // recタブ X軸
		text_ctrl = m_pXPitchTxt_r;
		axis = 0;
		break;
	case 4: // recタブ Y軸
		text_ctrl = m_pYPitchTxt_r;
		axis = 1;
		break;
	case 5: // recタブ Z軸
		text_ctrl = m_pZPitchTxt_r;
		axis = 2;
		break;
	case 6: // cylタブ X軸
		text_ctrl = m_pXPitchTxt_c;
		axis = 0;
		break;
	case 7: // cylタブ Y軸
		text_ctrl = m_pYPitchTxt_c;
		axis = 1;
		break;
	case 8: // cylタブ Z軸
		text_ctrl = m_pZPitchTxt_c;
		axis = 2;
		break;
	}

		double val;
		if (!text_ctrl->GetValue().ToDouble(&val)) {
			// 値がおかしい場合は元の値に戻す
			text_ctrl->SetValue(toStr( m_slidePitch[axis]));
		}
		f32 coord_pitch = static_cast<f32>(val);
		// 範囲制限
		if (coord_pitch < 0) coord_pitch = 0;
		if (coord_pitch > (m_bbox[1][axis] - m_bbox[0][axis])) coord_pitch = m_bbox[1][axis] - m_bbox[0][axis];
		// 値を入れなおす
		text_ctrl->SetValue(toStr( coord_pitch));
		m_slidePitch[axis] = coord_pitch;
//	}

}

// Pitchの表示を切り替える
void fxgenGUICoordinateViewerFrame::SetPitchTxt(u32 axis)
{

	wxTextCtrl *text_ctrl = 0; // 設定するテキストコントロール
	switch (axis) {
	default:
		// 指定した軸ではない場合はOnXPosDown_p終了
		break;
	case 0: // pointタブ X軸
		text_ctrl = m_pXPitchTxt_p;
		axis = 0;
		break;
	case 1: // pointタブ Y軸
		text_ctrl = m_pYPitchTxt_p;
		axis = 1;
		break;
	case 2: // pointタブ Z軸
		text_ctrl = m_pZPitchTxt_p;
		axis = 2;
		break;
	case 3: // recタブ X軸
		text_ctrl = m_pXPitchTxt_r;
		axis = 0;
		break;
	case 4: // recタブ Y軸
		text_ctrl = m_pYPitchTxt_r;
		axis = 1;
		break;
	case 5: // recタブ Z軸
		text_ctrl = m_pZPitchTxt_r;
		axis = 2;
		break;
	case 6: // cylタブ X軸
		text_ctrl = m_pXPitchTxt_c;
		axis = 0;
		break;
	case 7: // cylタブ Y軸
		text_ctrl = m_pYPitchTxt_c;
		axis = 1;
		break;
	case 8: // cylタブ Z軸
		text_ctrl = m_pZPitchTxt_c;
		axis = 2;
		break;
	}

	// 座標値で管理する場合はそのまま書く
	text_ctrl->SetValue(toStr(m_slidePitch[axis]));




//	if(!m_ctrl) return;
		// 座標値で管理する場合はそのまま書く
//		for (int i = 0; i < 3; i++) {
//			m_spinPitch[i] = m_ctrl->GetSpinPitch(i);
//		}

	switch (axis) {
	case 0:
		m_pXPitchTxt_p->SetValue(toStr( m_slidePitch[0]));
		break;
	case 1:
		m_pYPitchTxt_p->SetValue(toStr( m_slidePitch[1]));
		break;
	case 2:
		m_pZPitchTxt_p->SetValue(toStr( m_slidePitch[2]));
		break;
	}
}

int fxgenGUICoordinateViewerFrame::GetFreeId_p() const
{
	for (int freeid = 1; freeid < 100; ++freeid) {
		const int lbc = m_pPointGrid_p->GetRows();
		bool find = false;
		for (int i = 0; i < lbc; ++i){
			wxString id = m_pPointGrid_p->GetCellValue(i, 0);
			wxString id2 = "Point_"+toStr_int(freeid);
			if (id == id2) {
				find = true;
				continue;
			}
		}

		if (find)
			continue;
		else
			return freeid;
	}
	return 0; // Fail
}

int fxgenGUICoordinateViewerFrame::GetFreeId_r() const
{
	for (int freeid = 1; freeid < 100; ++freeid) {
		const int lbc = m_pRectangularGrid_r->GetRows();
		bool find = false;
		for (int i = 0; i < lbc; ++i){
			wxString id = m_pRectangularGrid_r->GetCellValue(i, 0);
			wxString id2 = "Rectangular_"+toStr_int(freeid);
			if (id == id2) {
				find = true;
				continue;
			}
		}

		if (find)
			continue;
		else
			return freeid;
	}
	return 0; // Fail
}

int fxgenGUICoordinateViewerFrame::GetFreeId_c() const
{
	for (int freeid = 1; freeid < 100; ++freeid) {
		const int lbc = m_pCylinderGrid_c->GetRows();
		bool find = false;
		for (int i = 0; i < lbc; ++i){
			wxString id = m_pCylinderGrid_c->GetCellValue(i, 0);
			wxString id2 = "Cylinder_"+toStr_int(freeid);
			if (id == id2) {
				find = true;
				continue;
			}
		}

		if (find)
			continue;
		else
			return freeid;
	}
	return 0; // Fail
}