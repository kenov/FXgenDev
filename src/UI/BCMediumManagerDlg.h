/*
 *
 * BCMediumManagerDlg.h
 *
 */

#ifndef _VX_BC_MEDIUM_MANAGER_DLG_H_
#define _VX_BC_MEDIUM_MANAGER_DLG_H_

#include "DialogBase.h"

#include <vector>
#include <string>


namespace UI
{
	class BCMediumManagerDlg : public BCMediumManagerDlgBase
	{
	public:
		BCMediumManagerDlg(wxWindow *parent);

		void OnNewLocalBCBtn( wxCommandEvent& event );
		void OnDelLocalBCBtn( wxCommandEvent& event );
		void OnClickLocalBCValue( wxGridEvent& event );

	private:
		std::vector<wxString> m_outerBCClassListStr;
		std::vector<wxString> m_localBCClassListStr;

		std::vector<wxColour> m_colList;
		
	};

} // namespace UI

#endif // _VX_BC_MEDIUM_MANAGER_DLG_H_
