#include "fxgenGUISubdomainSettingDlgBase.h"

fxgenGUISubdomainSettingDlgBase::fxgenGUISubdomainSettingDlgBase( wxWindow* parent, UI::SubdomainFunc& func )
:
SubdomainSettingDlgBase( parent ), m_func(func)
{
	m_pStatusList->Select(0);
	m_pOKBtn->SetDefault();
}

void fxgenGUISubdomainSettingDlgBase::OnOKBtn( wxCommandEvent& event )
{
	int sel = m_pStatusList->GetSelection();
	if (sel == 0)
		m_func.Inactive();
	else if (sel == 1)
		m_func.Active();
	
	EndDialog(1);
}

void fxgenGUISubdomainSettingDlgBase::OnCancelBtn( wxCommandEvent& event )
{
	EndDialog(0);
}
