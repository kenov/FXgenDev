
#include "BCMediumManagerDlg.h"
#include <wx/colordlg.h>


namespace UI
{
	BCMediumManagerDlg::BCMediumManagerDlg(wxWindow *parent)
	 : BCMediumManagerDlgBase(parent)
	{
		// TODO Tekitou
		m_outerBCClassListStr.push_back("Default...");

		m_outerBCClassListStr.push_back("Outflow");
		m_outerBCClassListStr.push_back("Periodic");
		m_outerBCClassListStr.push_back("Specified_Velocity");
		m_outerBCClassListStr.push_back("Symmetric");
		m_outerBCClassListStr.push_back("Traction_Free");
		m_outerBCClassListStr.push_back("Far_Field");
		m_outerBCClassListStr.push_back("Wall");
		m_outerBCClassListStr.push_back("Temperature");
		m_outerBCClassListStr.push_back("Ambient_Temperature");
		m_outerBCClassListStr.push_back("Adiabatic");
		m_outerBCClassListStr.push_back("HeatFlux");
		m_outerBCClassListStr.push_back("HeatTransfer_Type_S");
		m_outerBCClassListStr.push_back("HeatTransfer_Type_SF");
		m_outerBCClassListStr.push_back("HeatTransfer_Type_SN");
		m_outerBCClassListStr.push_back("HeatTransfer_Type_B");
		m_outerBCClassListStr.push_back("IsoThermal");


		m_localBCClassListStr.push_back("Default...");

		m_localBCClassListStr.push_back("Specified_Velocity");
		m_localBCClassListStr.push_back("Outflow");
		m_localBCClassListStr.push_back("Periodic");
		m_localBCClassListStr.push_back("Inactive");
		m_localBCClassListStr.push_back("Cell_Monitor");
		m_localBCClassListStr.push_back("Adabatic");
		m_localBCClassListStr.push_back("Direct_Heat_Flux");
		m_localBCClassListStr.push_back("HeatTransfer_B");
		m_localBCClassListStr.push_back("HeatTransfer_S");
		m_localBCClassListStr.push_back("HeatTransfer_SF");
		m_localBCClassListStr.push_back("HeatTransfer_SN");
		m_localBCClassListStr.push_back("IsoThermal");
		m_localBCClassListStr.push_back("Heat_Source");
		m_localBCClassListStr.push_back("Specified_Temperature");
		m_localBCClassListStr.push_back("Obstacle");
	}

	void BCMediumManagerDlg::OnNewLocalBCBtn( wxCommandEvent& event )
	{
		// Add New row
		m_pLocalBCGrid->AppendRows();

		// column order -> 0 : alias, 1 : class, 2 : ID
		wxGridCellEditor *celeditor[3];
		wxGridCellAttr   *attr;

		// column of alias
		// set column attribute to Text Editor
		celeditor[0] = new wxGridCellTextEditor();

		// column of class
		// set column attribute to Choice box
		celeditor[1] = new wxGridCellChoiceEditor(m_localBCClassListStr.size(), &m_localBCClassListStr[0]);

		// Set editor for each column
		for(int i = 0; i < 2; i++){
			attr = new wxGridCellAttr();
			attr->SetEditor(celeditor[i]);
			m_pLocalBCGrid->SetColAttr(i, attr);
		}

		// column of color setting
		int rowIdx = m_pLocalBCGrid->GetNumberRows() - 1;
		m_colList.push_back(wxColour(0x00, 0xFF, 0xFF));
		m_pLocalBCGrid->SetCellBackgroundColour(rowIdx, 2, m_colList[rowIdx]);
		m_pLocalBCGrid->SetReadOnly(rowIdx, 2, true);
		
	}

	void BCMediumManagerDlg::OnDelLocalBCBtn( wxCommandEvent& event )
	{

	}

	void BCMediumManagerDlg::OnClickLocalBCValue( wxGridEvent& event )
	{
		if(event.GetRow() == -1 ) return BCMediumManagerDlgBase::OnClickLocalBCValue( event );
		if(event.GetCol() != 2  ) return BCMediumManagerDlgBase::OnClickLocalBCValue( event );
		
		wxColourDialog coldlg(this);
		coldlg.SetTitle(_T("Choose color.."));
		if( coldlg.ShowModal() != wxID_OK ) return;

		wxColourData cdata = coldlg.GetColourData();
		wxColour rcol = cdata.GetColour();

		m_pLocalBCGrid->SetCellBackgroundColour(event.GetRow(), event.GetCol(), rcol);

		return;
	}




} // namespace UI

