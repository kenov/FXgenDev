#include "fxgenGUISD_LocalPanel.h"



#include "../Core/fxgenCore.h"
#include "../Core/SliceController.h"
#include "vxWindow.h"
#include "fxgenGUISDSettingUtil.h"
#include "fxgenGUISD_Frame.h"

fxgenGUISD_LocalPanel::fxgenGUISD_LocalPanel
	(
	wxWindow* parent , 
	GUIController *gui,
	fxgenCore* core ,
	fxgenGUISD_Frame* dlg
	)
:
SD_LocalPanel( parent ), m_core(core), m_gui(gui),m_ctrl(NULL),m_dlg(dlg)
{


	unsigned char *limg = new unsigned char[20 * 20 * 3];
	unsigned char *rimg = new unsigned char[20 * 20 * 3];
	memcpy(limg, leftArrow, (20 * 20 * 3));
	memcpy(rimg, rightArrow, (20 * 20 * 3));
		
	wxImage laImg(20, 20, limg, false);
	wxImage raImg(20, 20, rimg, false);

	wxBitmap labImg(laImg);
	wxBitmap rabImg(raImg);

	m_pXPosUpBtn->SetBitmapLabel(rabImg);
	m_pYPosUpBtn->SetBitmapLabel(rabImg);
	m_pZPosUpBtn->SetBitmapLabel(rabImg);

	m_pXPosDownBtn->SetBitmapLabel(labImg);
	m_pYPosDownBtn->SetBitmapLabel(labImg);
	m_pZPosDownBtn->SetBitmapLabel(labImg);

	Fit();
	FitInside();

	enum {
		TIME_X=1000,
		TIME_Y,
		TIME_Z
	};
	//タイマー
	this->m_pTimerX = new wxTimer(this,TIME_X);
	this->m_pTimerY = new wxTimer(this,TIME_Y);
	this->m_pTimerZ = new wxTimer(this,TIME_Z);

	m_TimerX_Way = true;//上り
	m_TimerY_Way = true;//上り
	m_TimerZ_Way = true;//上り
    
	this->Connect(TIME_X,wxEVT_TIMER, wxTimerEventHandler(fxgenGUISD_LocalPanel::OnTimerX));
	this->Connect(TIME_Y,wxEVT_TIMER, wxTimerEventHandler(fxgenGUISD_LocalPanel::OnTimerY));
	this->Connect(TIME_Z,wxEVT_TIMER, wxTimerEventHandler(fxgenGUISD_LocalPanel::OnTimerZ));

}

void fxgenGUISD_LocalPanel::updateGuiPartsStatusByData()
{
	//チェックボックスを最新にする
	bool isCheck = m_core->GetVoxelCartesianG()->GetLocalGroup()->GetVisible();
	m_checkBox_local_slice_visiable->SetValue(isCheck);

}
bool fxgenGUISD_LocalPanel::Show(bool 	show )
{
	if(show){
		updateGuiPartsStatusByData();
		UpdateControllerView();
	}

	return wxWindow::Show(show);
}

fxgenGUISD_LocalPanel::~fxgenGUISD_LocalPanel()
{
	int i=0;
}

void fxgenGUISD_LocalPanel::SetController(SliceController* controller)
{
	if(m_ctrl!=controller){
		m_ctrl = controller;
	}

	if(!m_ctrl) return;

	m_bbox[0] = m_ctrl->GetBBoxMin();
	m_bbox[1] = m_ctrl->GetBBoxMax();
		
	for(int i = 0; i < 3; i++){
		m_slidePitch[i] = (m_bbox[1][i] - m_bbox[0][i]) / 100.0f;
		m_position[i] = m_ctrl->GetPosition(i);
		m_spinPitch[i] = m_ctrl->GetSpinPitch(i);
		SetSlidePos(i);
		SetPosTxt(i, m_ctrl->GetSlideUnit());
	}
	SetPitchTxt(m_ctrl->GetSlideUnit());

	if (!m_ctrl->CheckSupportSlideUnit(SliceController::SLIDE_IDX))
	{
		SetPitchTxt(SliceController::SLIDE_COORD);
		m_ctrl->SetSlideUnit(SliceController::SLIDE_COORD);
	}

	if (m_ctrl->GetSlideUnit() == SliceController::SLIDE_IDX) {
		m_pPolicyIdxBtn->SetValue(true);
		m_pPolicyCoordBtn->SetValue(false);
	} else {
		m_pPolicyIdxBtn->SetValue(false);
		m_pPolicyCoordBtn->SetValue(true);			
	}

	m_pPolicyIdxBtn->Enable(m_ctrl->CheckSupportSlideUnit(SliceController::SLIDE_IDX));
	m_pPolicyIdxLabel->Enable(m_ctrl->CheckSupportSlideUnit(SliceController::SLIDE_IDX));
	m_pPolicyCoordBtn->Enable(m_ctrl->CheckSupportSlideUnit(SliceController::SLIDE_COORD));
	m_pPolicyCoordLabel->Enable(m_ctrl->CheckSupportSlideUnit(SliceController::SLIDE_COORD));



	this->Fit();
}

void fxgenGUISD_LocalPanel::UpdateControllerView()
{
	if (m_ctrl)
	{
		SetController(m_ctrl);
	}
}

void fxgenGUISD_LocalPanel::Notify_EyeStatus()
{
	updateGuiPartsStatusByData();
}


void fxgenGUISD_LocalPanel::NotifySelectionPick()
{
	//ページがアクティブで表示されていれば、アクティブなlocalが変更されている可能性があるのでＧＵＩを変更する
	if(this->IsShown()){
		UpdateControllerView();
	}
}

void fxgenGUISD_LocalPanel::Show_inner()
{
	updateGuiPartsStatusByData();
	
	//
	SliceController* g = m_core->GetSliceController_L(SliceController::MODE_DETAIL);

	SetController(g);

}


// m_positionの値からスライドバーの位置を設定
void fxgenGUISD_LocalPanel::SetSlidePos(u32 axis)
{
	if(!m_ctrl) return;

	wxSlider *slider = 0; // 設定するテキストコントロール
	switch (axis) {
	default:
		// 指定した軸ではない場合は終了
		break;
	case 0: // X軸
		slider = m_pXSlider;
		break;
	case 1: // Y軸
		slider = m_pYSlider;
		break;
	case 2: // Z軸
		slider = m_pZSlider;
		break;
	}
	int slidePos = static_cast<int>((m_position[axis] - m_bbox[0][axis]) / m_slidePitch[axis]);
	slider->SetValue(slidePos);
}

void fxgenGUISD_LocalPanel::m_checkBox_local_slice_visiableOnCheckBox( wxCommandEvent& event )
{
	bool visible = event.IsChecked();

	VX::SG::VoxelCartesianG* g = m_core->GetVoxelCartesianG();
	g->GetLocalGroup()->SetVisible(visible);

	// repaint 
	m_dlg->updateMainWnd();
}

void fxgenGUISD_LocalPanel::OnRadioButton( wxCommandEvent& event )
{
// TODO: Implement OnRadioButton
	if(!m_ctrl) return;

	switch (event.GetId()) {
	default:
		// 指定したIDではない場合は終了
		return;
	// 格子単位で移動
	case SLICE_CTRL_IDX_RADIO:
		if (m_ctrl->CheckSupportSlideUnit(SliceController::SLIDE_IDX)) {

			if (m_ctrl->GetSlideUnit() == SliceController::SLIDE_COORD) {
				// pitchの表示を切り替える
				SetPitchTxt(SliceController::SLIDE_IDX);
			}
			// 移動の単位を格子単位に変更
			m_ctrl->SetSlideUnit(SliceController::SLIDE_IDX);
		}
		break;
	// 座標位置で移動
	case SLICE_CTRL_COORD_RADIO:
		if (m_ctrl->CheckSupportSlideUnit(SliceController::SLIDE_COORD)) {
			if (m_ctrl->GetSlideUnit() == SliceController::SLIDE_IDX) {
				// pitchの表示を切り替える
				SetPitchTxt(SliceController::SLIDE_COORD);
			}
			// 移動の単位を座標単位に変更
			m_ctrl->SetSlideUnit(SliceController::SLIDE_COORD);
		}
		break;
	}

	// Positionの値を変更する
	for (int i = 0; i < 3; i++) {
		m_ctrl->SetPosition(i, m_position[i]);
		m_position[i] = m_ctrl->GetPosition(i);
		SetPosTxt(i, m_ctrl->GetSlideUnit());
	}
	// 再描画
	UI::vxWindow::GetTopWindow()->Draw();

}

void fxgenGUISD_LocalPanel::OnXPosDown( wxCommandEvent& event )
{
// TODO: Implement OnXPosDown
	if(!m_ctrl) return;

	u32 axis = 0;
	// 新しいPositionの計算
	m_position[axis] = GetLimitedPos(m_position[axis] - m_spinPitch[axis], axis);

	// スライダー、Positionの位置を変更
	SetPosTxt(axis, m_ctrl->GetSlideUnit());
	SetSlidePos(axis);

	// 位置の設定
	m_ctrl->SetPosition(axis, m_position[axis]);
	m_position[axis] = m_ctrl->GetPosition(axis);
	// 再描画
	UI::vxWindow::GetTopWindow()->Draw();

}

void fxgenGUISD_LocalPanel::OnXSlideScroll( wxScrollEvent& event )
{
// TODO: Implement OnXSlideScroll
	if(!m_ctrl) return;

	f32 slidePos = static_cast<f32>(m_pXSlider->GetValue());
	const int axis = 0;
	// Positionのアップデート
	m_ctrl->SetPosition(axis, m_bbox[0][axis] + slidePos * m_slidePitch[axis]);
	m_position[axis] = m_ctrl->GetPosition(axis);
	// Positionテキストのアップデート
	SetPosTxt(axis, m_ctrl->GetSlideUnit());
	UI::vxWindow::GetTopWindow()->Draw();
}

// Positionの値からいろいろ設定
void fxgenGUISD_LocalPanel::SetPosValue(u32 axis, u32 unit)
{
	if(!m_ctrl) return;

	wxTextCtrl *text_ctrl = 0; // 設定するテキストコントロール
	switch (axis) {
	default:
		// 指定した軸ではない場合は終了
		break;
	case 0: // X軸
		text_ctrl = m_pXPosTxt;
		break;
	case 1: // Y軸
		text_ctrl = m_pYPosTxt;
		break;
	case 2: // Z軸
		text_ctrl = m_pZPosTxt;
		break;
	}
	if (unit == SliceController::SLIDE_IDX) {
		// idxを座標値に変更
		int val = 1;
		bool ret = m_dlg->GetTextCtrlIndex( text_ctrl , val ,true);
		if (!ret) {
			// 値がおかしい場合は元の値に戻す
			int idx_pos = ConvertPosCoord2Idx(m_position[axis], axis);
			m_dlg->SetTextCtrlIndex(text_ctrl,idx_pos,true);
		}
		s32 idx_pos = static_cast<int>(val);
		// 範囲制限
		s32 range_max = static_cast<s32>((m_bbox[1][axis] - m_bbox[0][axis]) / m_ctrl->GetPitch(axis));
		if (idx_pos <= 0) idx_pos = 0;
		if (idx_pos >= range_max) idx_pos = range_max - 1;
		// 値を入れなおす
		m_dlg->SetTextCtrlIndex( text_ctrl ,idx_pos, true);

		m_position[axis] = ConvertPosIdx2Coord(idx_pos, axis);
	} else {
		// そのまま代入する
		double val;
		if (!text_ctrl->GetValue().ToDouble(&val)) {
			// 値がおかしい場合は元の値に戻す
			text_ctrl->SetValue(toStr( m_position[axis]));
		}
		f32 coord_pos = static_cast<f32>(val);
		// 範囲制限
		if (coord_pos < m_bbox[0][axis]) coord_pos = m_bbox[0][axis];
		if (coord_pos > m_bbox[1][axis]) coord_pos = m_bbox[1][axis];
		// 値を入れなおす
		text_ctrl->SetValue(toStr( coord_pos));
		m_position[axis] = coord_pos;
	}		
}

// Index値で持っているPositionの値を座標値に変更
f32 fxgenGUISD_LocalPanel::ConvertPosIdx2Coord(u32 pos, u32 axis)
{
	return m_ctrl->GetCoordPosition(axis, pos) + m_ctrl->GetPitch(axis) * 0.5f;
}


// セル幅(座標値)を取得
f32 fxgenGUISD_LocalPanel::GetCoordPitch(const u32 axis) const 
{
	return m_ctrl->GetPitch(axis);
}

// m_positionの値からポジションテキストを設定
void fxgenGUISD_LocalPanel::SetPosTxt(u32 axis, u32 unit)
{
	if(!m_ctrl) return;

	wxTextCtrl *text_ctrl = 0; // 設定するテキストコントロール
	switch (axis) {
	default:
		// 指定した軸ではない場合は終了
		break;
	case 0: // X軸
		text_ctrl = m_pXPosTxt;
		break;
	case 1: // Y軸
		text_ctrl = m_pYPosTxt;
		break;
	case 2: // Z軸
		text_ctrl = m_pZPosTxt;
		break;
	}

	if (unit == SliceController::SLIDE_IDX) {
		// インデックス番号で管理する場合は座標をインデックス番号に変換する
		int idx_pos = ConvertPosCoord2Idx(m_position[axis], axis);
		// 1スタート
		m_dlg->SetTextCtrlIndex(text_ctrl,idx_pos,true);
	} else {
		// 座標値で管理する場合はそのまま書く
		text_ctrl->SetValue(toStr( m_position[axis]));
	}

}
wxString fxgenGUISD_LocalPanel::toStr(const float& val)const
{
	return fxgenGUISD_Frame::FloatFormat(val);
}

void fxgenGUISD_LocalPanel::OnXSlideDrag( wxScrollEvent& event )
{
	if(!m_ctrl) return;

	f32 slidePos = static_cast<f32>(m_pXSlider->GetValue());
	const int axis = 0;
	// Positionのアップデート
	m_ctrl->SetPositionDrag(axis, m_bbox[0][axis] + slidePos * m_slidePitch[axis]);
	m_position[axis] = m_ctrl->GetPosition(axis);
	// Positionテキストのアップデート
	SetPosTxt(axis, m_ctrl->GetSlideUnit());
	UI::vxWindow::GetTopWindow()->Draw();

}

void fxgenGUISD_LocalPanel::OnXPosUp( wxCommandEvent& event )
{
// TODO: Implement OnXPosUp
	if(!m_ctrl) return;

	u32 axis = 0;
	// 新しいPositionの計算
	m_position[axis] = GetLimitedPos(m_position[axis] + m_spinPitch[axis], axis);

	// スライダー、Positionの位置を変更
	SetPosTxt(axis, m_ctrl->GetSlideUnit());
	SetSlidePos(axis);

	// 位置の設定
	m_ctrl->SetPosition(axis, m_position[axis]);
	m_position[axis] = m_ctrl->GetPosition(axis);
	// 再描画
	UI::vxWindow::GetTopWindow()->Draw();
}




void fxgenGUISD_LocalPanel::updatePosTxt( const int& guiID )
{
// TODO: Implement OnPosTxt
	if(!m_ctrl) return;

	u32 axis = 0;
	switch (guiID) {
	default:
		// 指定したIDではない場合は終了
		return;
	case SLICE_CTRL_XPOS_TXT:
		axis = 0;
		break;
	case SLICE_CTRL_YPOS_TXT:
		axis = 1;
		break;
	case SLICE_CTRL_ZPOS_TXT:
		axis = 2;
		break;
	}
	// 値の保存
	SetPosValue(axis, m_ctrl->GetSlideUnit());
		
	// スライダーバーの位置をPositionに合わせる
	SetSlidePos(axis);

	// 位置の設定
	m_ctrl->SetPosition(axis, m_position[axis]);
	// 再描画
	UI::vxWindow::GetTopWindow()->Draw();
}

// 入力されたposition値を範囲内に収めた値に切り詰めて返す
f32 fxgenGUISD_LocalPanel::GetLimitedPos(f32 pos, u32 axis)
{
	f32 newpos = pos;
	if (pos < m_bbox[0][axis]) newpos = m_bbox[0][axis];
	if (pos > m_bbox[1][axis]) newpos = m_bbox[1][axis];
	return newpos;
}

void fxgenGUISD_LocalPanel::updatePitchTxt( const int& guiID )
{
// TODO: Implement OnPitchTxt

	if(!m_ctrl) return;

	switch (guiID) {
	default:
		// 指定したIDではない場合は終了
		return;
	case SLICE_CTRL_XPITCH_TXT:
		SetPitchValue(0, m_ctrl->GetSlideUnit());
		break;
	case SLICE_CTRL_YPITCH_TXT:
		SetPitchValue(1, m_ctrl->GetSlideUnit());
		break;
	case SLICE_CTRL_ZPITCH_TXT:
		SetPitchValue(2, m_ctrl->GetSlideUnit());
		break;
	}
}


// Pitchテキストからm_spinPitchのを設定
void fxgenGUISD_LocalPanel::SetPitchValue(u32 axis, u32 unit)
{
	if(!m_ctrl) return;

	wxTextCtrl *text_ctrl = 0; // 設定するテキストコントロール
	switch (axis) {
	default:
		// 指定した軸ではない場合は終了
		break;
	case 0: // X軸
		text_ctrl = m_pXPitchTxt;
		break;
	case 1: // Y軸
		text_ctrl = m_pYPitchTxt;
		break;
	case 2: // Z軸
		text_ctrl = m_pZPitchTxt;
		break;
	}
	if (unit == SliceController::SLIDE_IDX) {
		// idxを座標値に変更
		long val = 1;
		if (!text_ctrl->GetValue().ToLong(&val)) {
			// 値がおかしい場合は元の値に戻す
			text_ctrl->SetValue(wxString::Format("%d", ConvertPitchCoord2Idx(m_spinPitch[axis], axis)));
		}
		s32 idx_pitch = static_cast<s32>(val);
		// 範囲制限
		s32 range_max = static_cast<s32>((m_bbox[1][axis] - m_bbox[0][axis]) / m_ctrl->GetPitch(axis));
		if (idx_pitch <= 0) idx_pitch = 1;
		if (idx_pitch > range_max) idx_pitch = range_max;
		// 値を入れなおす
		text_ctrl->SetValue(wxString::Format("%d", idx_pitch));

		m_spinPitch[axis] = ConvertPitchIdx2Coord(idx_pitch, axis);
	} else {
		// そのまま代入する
		double val;
		if (!text_ctrl->GetValue().ToDouble(&val)) {
			// 値がおかしい場合は元の値に戻す
			text_ctrl->SetValue(toStr(m_spinPitch[axis]));
		}
		f32 coord_pitch = static_cast<f32>(val);
		// 範囲制限
		if (coord_pitch < 0) coord_pitch = 0;
		if (coord_pitch > (m_bbox[1][axis] - m_bbox[0][axis])) coord_pitch = m_bbox[1][axis] - m_bbox[0][axis];
		// 値を入れなおす
		text_ctrl->SetValue(toStr( coord_pitch));
		m_spinPitch[axis] = coord_pitch;
	}
	m_ctrl->SetSpinPitch(m_spinPitch[axis], axis);
}


// Index値で持っているpitchの値を座標値に変更
f32 fxgenGUISD_LocalPanel::ConvertPitchIdx2Coord(u32 pitch, u32 axis)
{
	f32 coord_pitch = static_cast<f32>(pitch * m_ctrl->GetPitch(axis));
	// 範囲制限
	if (coord_pitch < 0) coord_pitch = 0;
	if (coord_pitch > (m_bbox[1][axis] - m_bbox[0][axis])) coord_pitch = m_bbox[1][axis] - m_bbox[0][axis];
	return coord_pitch;
}

// 座標値で持っているPositionの値をIndex値に変更
u32 fxgenGUISD_LocalPanel::ConvertPosCoord2Idx(f32 pos, u32 axis)
{
	return m_ctrl->GetIdxPosition(axis, pos);
}


// Pitchの表示を切り替える
void fxgenGUISD_LocalPanel::SetPitchTxt(u32 newunit)
{
	if(!m_ctrl) return;

	if (newunit == SliceController::SLIDE_IDX) {
		// 座標値をidx値に変更
		u32 newp[3];
		for (int i = 0; i < 3; i++) {
			newp[i] = ConvertPitchCoord2Idx(m_spinPitch[i], i);
			m_spinPitch[i] = ConvertPitchIdx2Coord(newp[i], i);
		}
		m_pXPitchTxt->SetValue(wxString::Format("%d", newp[0]));
		m_pYPitchTxt->SetValue(wxString::Format("%d", newp[1]));
		m_pZPitchTxt->SetValue(wxString::Format("%d", newp[2]));
	} else {
		// 座標値で管理する場合はそのまま書く
		for (int i = 0; i < 3; i++) {
			m_spinPitch[i] = m_ctrl->GetSpinPitch(i);
		}
		m_pXPitchTxt->SetValue(toStr( m_spinPitch[0]));
		m_pYPitchTxt->SetValue(toStr( m_spinPitch[1]));
		m_pZPitchTxt->SetValue(toStr( m_spinPitch[2]));
	}
}

// 座標値で持っているpitchの値をIndex値に変更
u32 fxgenGUISD_LocalPanel::ConvertPitchCoord2Idx(f32 pitch, u32 axis)
{
	s32 idx_pitch = static_cast<s32>(pitch / m_ctrl->GetPitch(axis));
	// 範囲制限
	s32 range_max = static_cast<s32>((m_bbox[1][axis] - m_bbox[0][axis]) / m_ctrl->GetPitch(axis));
	if (idx_pitch <= 0) idx_pitch = 1;
	if (idx_pitch > range_max) idx_pitch = range_max;
	return idx_pitch;
}


void fxgenGUISD_LocalPanel::OnYPosDown( wxCommandEvent& event )
{
// TODO: Implement OnYPosDown
	if(!m_ctrl) return;

	u32 axis = 1;
	// 新しいPositionの計算
	m_position[axis] = GetLimitedPos(m_position[axis] - m_spinPitch[axis], axis);

	// スライダー、Positionの位置を変更
	SetPosTxt(axis, m_ctrl->GetSlideUnit());
	SetSlidePos(axis);

	// 位置の設定
	m_ctrl->SetPosition(axis, m_position[axis]);
	m_position[axis] = m_ctrl->GetPosition(axis);
	// 再描画
	UI::vxWindow::GetTopWindow()->Draw();
}

void fxgenGUISD_LocalPanel::OnYSlideScroll( wxScrollEvent& event )
{
// TODO: Implement OnYSlideScroll
	if(!m_ctrl) return;

	f32 slidePos = static_cast<f32>(m_pYSlider->GetValue());
	const int axis = 1;
	// Positionのアップデート
	m_ctrl->SetPosition(axis, m_bbox[0][axis] + slidePos * m_slidePitch[axis]);
	m_position[axis] = m_ctrl->GetPosition(axis);
	// Positionテキストのアップデート
	SetPosTxt(axis, m_ctrl->GetSlideUnit());
	UI::vxWindow::GetTopWindow()->Draw();
}

void fxgenGUISD_LocalPanel::OnYSlideRelease( wxScrollEvent& event )
{
// TODO: Implement OnYSlideRelease
}

void fxgenGUISD_LocalPanel::OnYSlideDrag( wxScrollEvent& event )
{
// TODO: Implement OnYSlideDrag
	if(!m_ctrl) return;

	f32 slidePos = static_cast<f32>(m_pYSlider->GetValue());
	const int axis = 1;
	// Positionのアップデート
	m_ctrl->SetPositionDrag(axis, m_bbox[0][axis] + slidePos * m_slidePitch[axis]);
	m_position[axis] = m_ctrl->GetPosition(axis);
	// Positionテキストのアップデート
	SetPosTxt(axis, m_ctrl->GetSlideUnit());
	UI::vxWindow::GetTopWindow()->Draw();
}

void fxgenGUISD_LocalPanel::OnYPosUp( wxCommandEvent& event )
{
// TODO: Implement OnYPosUp
	if(!m_ctrl) return;

	u32 axis = 1;
	// 新しいPositionの計算
	m_position[axis] = GetLimitedPos(m_position[axis] + m_spinPitch[axis], axis);

	// スライダー、Positionの位置を変更
	SetPosTxt(axis, m_ctrl->GetSlideUnit());
	SetSlidePos(axis);

	// 位置の設定
	m_ctrl->SetPosition(axis, m_position[axis]);
	m_position[axis] = m_ctrl->GetPosition(axis);
	// 再描画
	UI::vxWindow::GetTopWindow()->Draw();
}

void fxgenGUISD_LocalPanel::OnZPosDown( wxCommandEvent& event )
{
	if(!m_ctrl) return;

	u32 axis = 2;
	// 新しいPositionの計算
	m_position[axis] = GetLimitedPos(m_position[axis] - m_spinPitch[axis], axis);

	// スライダー、Positionの位置を変更
	SetPosTxt(axis, m_ctrl->GetSlideUnit());
	SetSlidePos(axis);

	// 位置の設定
	m_ctrl->SetPosition(axis, m_position[axis]);
	m_position[axis] = m_ctrl->GetPosition(axis);
	// 再描画
	UI::vxWindow::GetTopWindow()->Draw();
}

void fxgenGUISD_LocalPanel::OnZSlideScroll( wxScrollEvent& event )
{
// TODO: Implement OnZSlideScroll
	if(!m_ctrl) return;

	f32 slidePos = static_cast<f32>(m_pZSlider->GetValue());
	const int axis = 2;
	// Positionのアップデート
	m_ctrl->SetPosition(axis, m_bbox[0][axis] + slidePos * m_slidePitch[axis]);
	m_position[axis] = m_ctrl->GetPosition(axis);
	// Positionテキストのアップデート
	SetPosTxt(axis, m_ctrl->GetSlideUnit());
	UI::vxWindow::GetTopWindow()->Draw();
}

void fxgenGUISD_LocalPanel::OnZSlideRelease( wxScrollEvent& event )
{
// TODO: Implement OnZSlideRelease
}

void fxgenGUISD_LocalPanel::OnZSlideDrag( wxScrollEvent& event )
{
// TODO: Implement OnZSlideDrag
	if(!m_ctrl) return;

	f32 slidePos = static_cast<f32>(m_pZSlider->GetValue());
	const int axis = 2;
	// Positionのアップデート
	m_ctrl->SetPositionDrag(axis, m_bbox[0][axis] + slidePos * m_slidePitch[axis]);
	m_position[axis] = m_ctrl->GetPosition(axis);
	// Positionテキストのアップデート
	SetPosTxt(axis, m_ctrl->GetSlideUnit());
	UI::vxWindow::GetTopWindow()->Draw();
}

void fxgenGUISD_LocalPanel::OnZPosUp( wxCommandEvent& event )
{
// TODO: Implement OnZPosUp
	if(!m_ctrl) return;

	u32 axis = 2;
	// 新しいPositionの計算
	m_position[axis] = GetLimitedPos(m_position[axis] + m_spinPitch[axis], axis);

	// スライダー、Positionの位置を変更
	SetPosTxt(axis, m_ctrl->GetSlideUnit());
	SetSlidePos(axis);

	// 位置の設定
	m_ctrl->SetPosition(axis, m_position[axis]);
	m_position[axis] = m_ctrl->GetPosition(axis);
	// 再描画
	UI::vxWindow::GetTopWindow()->Draw();
}



void fxgenGUISD_LocalPanel::OnPosTxt( wxCommandEvent& event )
{
	updatePosTxt(event.GetId());
}

void fxgenGUISD_LocalPanel::OnPosTxtKillFocus( wxFocusEvent& event )
{
	updatePosTxt(event.GetId());
	event.Skip();
}
void fxgenGUISD_LocalPanel::OnPitchTxtKillFocus( wxFocusEvent& event )
{
	updatePitchTxt(event.GetId());
	event.Skip();
}
void fxgenGUISD_LocalPanel::OnPitchTxt( wxCommandEvent& event )
{
	updatePitchTxt(event.GetId());
}


void fxgenGUISD_LocalPanel::OnTimerX(wxTimerEvent& WXUNUSED(event))
{
	wxCommandEvent e;

	int v = m_pXSlider->GetValue();
	if(m_TimerX_Way){
		//まず上に上げる
		OnXPosUp(e);
		if (v == m_pXSlider->GetMax()){
			//次回から下りに方向を変える
			m_TimerX_Way=false;
		}
	}else{
		//まず下に下げる
		//最小値なら上り方向にする
		OnXPosDown(e);
		if(v == m_pXSlider->GetMin()){
			//次回から下りに方向を変える
			m_TimerX_Way=true;
		}
	}
}
void fxgenGUISD_LocalPanel::OnTimerY(wxTimerEvent& WXUNUSED(event))
{
	wxCommandEvent e;

	int v = m_pYSlider->GetValue();
	if(m_TimerY_Way){
		//まず上に上げる
		OnYPosUp(e);
		if (v == m_pYSlider->GetMax()){
			//次回から下りに方向を変える
			m_TimerY_Way=false;
		}
	}else{
		//まず下に下げる
		//最小値なら上り方向にする
		OnYPosDown(e);
		if(v == m_pYSlider->GetMin()){
			//次回から下りに方向を変える
			m_TimerY_Way=true;
		}
	}
}
void fxgenGUISD_LocalPanel::OnTimerZ(wxTimerEvent& WXUNUSED(event))
{
	wxCommandEvent e;

	int v = m_pZSlider->GetValue();
	if(m_TimerZ_Way){
		//まず上に上げる
		OnZPosUp(e);
		if (v == m_pZSlider->GetMax()){
			//次回から下りに方向を変える
			m_TimerZ_Way=false;
		}
	}else{
		//まず下に下げる
		//最小値なら上り方向にする
		OnZPosDown(e);
		if(v == m_pZSlider->GetMin()){
			//次回から下りに方向を変える
			m_TimerZ_Way=true;
		}
	}
}

void fxgenGUISD_LocalPanel::m_play_check_xOnCheckBox( wxCommandEvent& event )
{
	//start timer
	if(event.IsChecked()){
		int frameRate = m_dlg->GetFrameRate();
		this->m_pTimerX->Start(frameRate);
	}else{
		this->m_pTimerX->Stop();
	}
}
void fxgenGUISD_LocalPanel::m_play_check_yOnCheckBox( wxCommandEvent& event )
{
	//start timer
	if(event.IsChecked()){
		int frameRate = m_dlg->GetFrameRate();
		this->m_pTimerY->Start(frameRate);
	}else{
		this->m_pTimerY->Stop();
	}
}
void fxgenGUISD_LocalPanel::m_play_check_zOnCheckBox( wxCommandEvent& event )
{
	//start timer
	if(event.IsChecked()){
		int frameRate = m_dlg->GetFrameRate();
		this->m_pTimerZ->Start(frameRate);
	}else{
		this->m_pTimerZ->Stop();
	}
}

void fxgenGUISD_LocalPanel::stopTimers()
{
	this->m_pTimerX->Stop();
	this->m_pTimerY->Stop();
	this->m_pTimerZ->Stop();
	//GUIのチェックをoffにする
	this->m_play_check_x->SetValue(false);
	this->m_play_check_y->SetValue(false);
	this->m_play_check_z->SetValue(false);
}
