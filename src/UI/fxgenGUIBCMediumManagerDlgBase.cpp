#include "fxgenGUIBCMediumManagerDlgBase.h"
#include "DialogUtil.h"
#include <wx/colordlg.h>
#include "../VX/SG/LocalBCClass.h"
#include "../VX/SG/LocalBC.h"
#include "../VX/SG/OuterBCClass.h"
#include "../VX/SG/OuterBC.h"
#include "../VX/SG/Medium.h"
#include <algorithm>

const int MAXID_NUM = 30;

fxgenGUIBCMediumManagerDlgBase::fxgenGUIBCMediumManagerDlgBase( wxWindow* parent, UI::BCMedFunc& func )
:
BCMediumManagerDlgBase( parent ), m_func(func)
{
	m_pOuterBCGrid->SetSelectionMode(wxGrid::wxGridSelectRows);
	m_pLocalBCGrid->SetSelectionMode(wxGrid::wxGridSelectRows);
	m_pMedGrid->SetSelectionMode(wxGrid::wxGridSelectRows);
	
	m_pLocalBCGrid->SetSelectionForeground(wxColour(204,102,0));
	m_pLocalBCGrid->SetSelectionBackground(wxColour(72,31,0));
	m_pOuterBCGrid->SetSelectionForeground(wxColour(204,102,0));
	m_pOuterBCGrid->SetSelectionBackground(wxColour(72,31,0));
	m_pMedGrid->SetSelectionForeground(wxColour(204,102,0));
	m_pMedGrid->SetSelectionBackground(wxColour(72,31,0));
	
	const int lbcnum = func.GetLocalBCClassNum();
	for(int i = 0; i < lbcnum; ++i) {
		m_localBCClassListStr.push_back(func.GetLocalBCClass(i).c_str());
	}
	const int obcnum = func.GetOuterBCClassNum();
	for(int i = 0; i < obcnum; ++i) {
		m_outerBCClassListStr.push_back(func.GetOuterBCClass(i).c_str());
	}
	
	const int lbc = func.GetLocalBCNum();
	for (int i = 0; i < lbc; ++i) {
		const VX::SG::LocalBC* bc = func.GetLocalBC(i);
		const std::string alsname = bc->GetAlias();
		std::string clsname;
		if (bc->GetClass())
			clsname = bc->GetClass()->GetName();
		
		float col[4] = {bc->GetColor().r, bc->GetColor().g, bc->GetColor().b, bc->GetColor().a};
		AddLocalBC(alsname.c_str(), clsname.c_str(), col, bc->GetID());
	}
	const int obc = func.GetOuterBCNum();
	for (int i = 0; i < obc; ++i) {
		const VX::SG::OuterBC* bc = func.GetOuterBC(i);
		const std::string alsname = bc->GetAlias();
		std::string clsname;
		if (bc->GetClass())
			clsname = bc->GetClass()->GetName();
		AddOuterBC(alsname.c_str(), clsname.c_str());
	}
	const int med = func.GetMediumNum();
	for (int i = 0; i < med; ++i) {
		const VX::SG::Medium* m = func.GetMedium(i);
		const std::string lblname = m->GetLabel();
		float col[4] = {m->GetColor().r, m->GetColor().g, m->GetColor().b, m->GetColor().a};
		AddMedium(lblname.c_str(), col, m->GetID(), m->GetMediumType());
	}


#if defined(WIN32) || defined(WIN64)
	wxFont font = m_pOKBtn->GetFont();
	font.SetPointSize(10);
	m_pOKBtn->SetFont(font);
	m_pCancel->SetFont(font);

	m_pNewMedBtn->SetFont(font);
	m_pDelMedBtn->SetFont(font);
	m_pNewOuterBCBtn->SetFont(font);
	m_pDelOuterBCBtn->SetFont(font);
	m_pNewLocalBCBtn->SetFont(font);
	m_pDelLocalBCBtn->SetFont(font);
#endif
}

void fxgenGUIBCMediumManagerDlgBase::AddOuterBC(const wxString& alias, const wxString& classname)
{
	m_pOuterBCGrid->EnableEditing(false);
	// Add New row
	m_pOuterBCGrid->AppendRows();
	
	// column order -> 0 : alias, 1 : class, 2 : ID
	wxGridCellEditor *celeditor[3];
	wxGridCellAttr   *attr;
	
	// column of alias
	// set column attribute to Text Editor
	celeditor[0] = new wxGridCellTextEditor();
	m_pOuterBCGrid->SetCellValue(m_pOuterBCGrid->GetRows()-1, 0, alias);
	
	// column of class
	// set column attribute to Choice box
	celeditor[1] = new wxGridCellChoiceEditor(m_outerBCClassListStr.size(), &m_outerBCClassListStr[0]);
	m_pOuterBCGrid->SetCellValue(m_pOuterBCGrid->GetRows()-1, 1, classname);
	
	// Set editor for each column
	for(int i = 0; i < 2; i++){
		attr = new wxGridCellAttr();
		attr->SetEditor(celeditor[i]);
		m_pOuterBCGrid->SetColAttr(i, attr);
	}
	m_pOuterBCGrid->EnableEditing(true);
}

void fxgenGUIBCMediumManagerDlgBase::AddLocalBC(const wxString& alias, const wxString& classname, float col[4], unsigned int id)
{
	m_pLocalBCGrid->EnableEditing(false);
	// Add New row
	m_pLocalBCGrid->AppendRows();
	
	// column order -> 0 : alias, 1 : class, 2 : Color, 3 : ID
	wxGridCellEditor *celeditor[4];
	wxGridCellAttr   *attr;
	
	// column of alias
	// set column attribute to Text Editor
	celeditor[0] = new wxGridCellTextEditor();
	m_pLocalBCGrid->SetCellValue(m_pLocalBCGrid->GetRows()-1, 0, alias);

	// column of class
	// set column attribute to Choice box
	celeditor[1] = new wxGridCellChoiceEditor(m_localBCClassListStr.size(), &m_localBCClassListStr[0]);
	m_pLocalBCGrid->SetCellValue(m_pLocalBCGrid->GetRows()-1, 1, classname);

	for(int i = 0; i < 2; i++){
		attr = new wxGridCellAttr();
		attr->SetEditor(celeditor[i]);
		m_pLocalBCGrid->SetColAttr(i, attr);
	}
	
	// column of color setting
	int rowIdx = m_pLocalBCGrid->GetNumberRows() - 1;
	m_pLocalBCGrid->SetCellBackgroundColour(rowIdx, 2, wxColour(col[0]*0xFF, col[1]*0xFF, col[2]*0xFF));
	m_pLocalBCGrid->SetReadOnly(rowIdx, 2, true);
	
	celeditor[3] = new wxGridCellNumberEditor();
	m_pLocalBCGrid->SetCellValue(m_pLocalBCGrid->GetRows()-1, 3, wxString::Format("%d",id));
	attr = new wxGridCellAttr();
	attr->SetEditor(celeditor[3]);
	m_pLocalBCGrid->SetColAttr(3, attr);

	m_pLocalBCGrid->EnableEditing(true);
}

void fxgenGUIBCMediumManagerDlgBase::AddMedium(const wxString& label, float col[4], unsigned int id, const wxString& medType)
{
	m_pMedGrid->EnableEditing(false);
	// Add New row
	m_pMedGrid->AppendRows();
	
	// column order -> 0 : label, 1 : Color, 2 : ID, 3 : Type
	wxGridCellEditor *celeditor[4];
	wxGridCellAttr   *attr;
	
	// column of alias
	// set column attribute to Text Editor
	celeditor[0] = new wxGridCellTextEditor();
	m_pMedGrid->SetCellValue(m_pMedGrid->GetRows()-1, 0, label);
	attr = new wxGridCellAttr();
	attr->SetEditor(celeditor[0]);
	m_pMedGrid->SetColAttr(0, attr);
	
	// column of color setting
	const int rowIdx = m_pMedGrid->GetNumberRows() - 1;
	const int colIdx = 1;
	m_pMedGrid->SetCellBackgroundColour(rowIdx, colIdx, wxColour(col[0]*0xFF, col[1]*0xFF, col[2]*0xFF));
	m_pMedGrid->SetReadOnly(rowIdx, colIdx, true);

	// ID
	celeditor[2] = new wxGridCellNumberEditor();
	m_pMedGrid->SetCellValue(m_pMedGrid->GetRows()-1, 2, wxString::Format("%d",id));
	attr = new wxGridCellAttr();
	attr->SetEditor(celeditor[2]);
	m_pMedGrid->SetColAttr(2, attr);
	
	wxArrayString typeArray;
	typeArray.Add(wxString("Solid"));
	typeArray.Add(wxString("Fluid"));
	celeditor[3] = new wxGridCellChoiceEditor(typeArray.size(), &typeArray[0]);
	attr = new wxGridCellAttr();
	attr->SetEditor(celeditor[3]);
	m_pMedGrid->SetColAttr(3, attr);
	m_pMedGrid->SetCellValue(m_pMedGrid->GetRows()-1, 3, medType);

	

	m_pMedGrid->EnableEditing(true);
}

int fxgenGUIBCMediumManagerDlgBase::GetFreeId() const
{
	for (int freeid = 1; freeid < 100; ++freeid) {
		const int lbc = m_pLocalBCGrid->GetRows();
		bool find = false;
		for (int i = 0; i < lbc; ++i){
			wxString id = m_pLocalBCGrid->GetCellValue(i, 3);
			long val;
			if (id.ToLong(&val) && val == freeid) {
				find = true;
				continue;
			}
		}
		const int med = m_pMedGrid->GetRows();
		for (int i = 0; i < med; ++i){
			wxString id = m_pMedGrid->GetCellValue(i, 2);
			long val;
			if (id.ToLong(&val) && val == freeid) {
				find = true;
				continue;
			}
		}
		if (find)
			continue;
		else
			return freeid;
	}
	return 0; // Fail
}


void fxgenGUIBCMediumManagerDlgBase::OnChangeMedValue( wxGridEvent& event )
{



	checkValues();


}

void fxgenGUIBCMediumManagerDlgBase::OnClickMedValue( wxGridEvent& event )
{
	if(event.GetRow() == -1 ) return BCMediumManagerDlgBase::OnClickLocalBCValue( event );
	if(event.GetCol() != 1  ) return BCMediumManagerDlgBase::OnClickLocalBCValue( event );
	
	int rowidx = event.GetRow();
	int colIdx = event.GetCol();
	wxColourDialog coldlg(this);
	wxColour col = m_pMedGrid->GetCellBackgroundColour(rowidx, colIdx);
	coldlg.GetColourData().SetChooseFull(true);
	coldlg.GetColourData().GetColour() = col;
	coldlg.SetTitle(_T("Choose color.."));
	if( coldlg.ShowModal() != wxID_OK ) return;
	
	wxColourData cdata = coldlg.GetColourData();
	wxColour rcol = cdata.GetColour();
	
	m_pMedGrid->SetCellBackgroundColour(event.GetRow(), event.GetCol(), rcol);
	m_pMedGrid->Refresh();
	return;
}

void fxgenGUIBCMediumManagerDlgBase::OnNewMedBtn( wxCommandEvent& event )
{
	wxString defbcclass;
	float col[4] = {1.0, 0,0,0};
	const int nid = GetFreeId();
	if (nid > MAXID_NUM){
		UI::MessageDlg("IDs are less than 31");
		return;
	}
	
	char newname[64] = {};
	sprintf(newname, "medium%d", nid);
	AddMedium(newname, col, nid, "Solid");
}

void fxgenGUIBCMediumManagerDlgBase::OnDelMedBtn( wxCommandEvent& event )
{
	wxArrayInt ar = m_pMedGrid->GetSelectedRows();
	int n = static_cast<int>(ar.GetCount());
	for (int i = n - 1; i >= 0; --i){
		int sel = ar[i];
		m_pMedGrid->DeleteRows(sel);
	}
}

void fxgenGUIBCMediumManagerDlgBase::OnChangeOuterBCValue( wxGridEvent& event )
{
	checkValues();
}

void fxgenGUIBCMediumManagerDlgBase::OnClickOuterBCValue( wxGridEvent& event )
{
	if(event.GetRow() == -1 ) return BCMediumManagerDlgBase::OnClickOuterBCValue( event );
	if(event.GetCol() != 2  ) return BCMediumManagerDlgBase::OnClickOuterBCValue( event );
	return;
}

void fxgenGUIBCMediumManagerDlgBase::OnNewOuterBCBtn( wxCommandEvent& event )
{
	int eid = 0;
	char newname[64];
	const int obc = m_pOuterBCGrid->GetRows();
	bool found = true;
	while (found) {
		found = false;
		sprintf(newname, "outerBC%d",eid++);
		for (int i = 0; i < obc; ++i) {
			wxString bcaliasname = m_pOuterBCGrid->GetCellValue(i, 0);
			if (bcaliasname == wxString(newname)) {
				found = true;
				break;
			}
		}
	}
	
	wxString defbcclass;
	if (m_outerBCClassListStr.size())
		defbcclass = m_outerBCClassListStr[0].c_str();
	AddOuterBC(newname, defbcclass);
}

void fxgenGUIBCMediumManagerDlgBase::OnDelOuterBCBtn( wxCommandEvent& event )
{
	wxArrayInt ar = m_pOuterBCGrid->GetSelectedRows();
	int n = static_cast<int>(ar.GetCount());
	for (int i = n - 1; i >= 0; --i){
		int sel = ar[i];
		m_pOuterBCGrid->DeleteRows(sel);
	}
}

void fxgenGUIBCMediumManagerDlgBase::OnChangeLocalBCValue( wxGridEvent& event )
{
	checkValues();
}

void fxgenGUIBCMediumManagerDlgBase::OnClickLocalBCValue( wxGridEvent& event )
{
	if(event.GetRow() == -1 ) return BCMediumManagerDlgBase::OnClickLocalBCValue( event );
	if(event.GetCol() != 2  ) return BCMediumManagerDlgBase::OnClickLocalBCValue( event );
	
	int rowidx = event.GetRow();
	int colIdx = event.GetCol();
	wxColourDialog coldlg(this);
	wxColour col = m_pMedGrid->GetCellBackgroundColour(rowidx, colIdx);
	coldlg.GetColourData().SetChooseFull(true);
	coldlg.GetColourData().GetColour() = col;
	coldlg.SetTitle(_T("Choose color.."));
	if( coldlg.ShowModal() != wxID_OK ) return;
	
	wxColourData cdata = coldlg.GetColourData();
	wxColour rcol = cdata.GetColour();
	
	m_pLocalBCGrid->SetCellBackgroundColour(event.GetRow(), event.GetCol(), rcol);
	m_pLocalBCGrid->Refresh();
	return;
}

void fxgenGUIBCMediumManagerDlgBase::OnNewLocalBCBtn( wxCommandEvent& event )
{
	wxString defbcclass;
	if (m_localBCClassListStr.size())
		defbcclass = m_localBCClassListStr[0].c_str();
	
	float col[4] = {1.0, 0,0,0};
	const int nid = GetFreeId();
	if (nid > MAXID_NUM){
		UI::MessageDlg("IDs are less than 31");
		return;
	}

	char newname[64] = {};
	sprintf(newname, "localBC%d", nid);
	AddLocalBC(newname, defbcclass, col, nid);
}

void fxgenGUIBCMediumManagerDlgBase::OnDelLocalBCBtn( wxCommandEvent& event )
{
	wxArrayInt ar = m_pLocalBCGrid->GetSelectedRows();
	int n = static_cast<int>(ar.GetCount());
	for (int i = n - 1; i >= 0; --i){
		int sel = ar[i];
		m_pLocalBCGrid->DeleteRows(sel);
	}
}

bool fxgenGUIBCMediumManagerDlgBase::checkValues()
{
	const int maxIDNum = 31;
	int useid[maxIDNum] = {1};
	
	// LocalBC
	std::vector<wxString> lbcmed_aliasTable;
	const int lbc = m_pLocalBCGrid->GetRows();
	for (int i = 0; i < lbc; ++i) {
		wxString bcaliasname = m_pLocalBCGrid->GetCellValue(i, 0);
		wxString bcclassname = m_pLocalBCGrid->GetCellValue(i, 1);
		if (bcaliasname == "")
		{
			UI::MessageDlg("LocalBC: Empty Alias name");
			return false;
		}
		if (std::find(lbcmed_aliasTable.begin(), lbcmed_aliasTable.end(), bcaliasname) != lbcmed_aliasTable.end()) {
			char buf[128] = {};
			sprintf(buf, "LocalBC/Medium: double Alias/Label>%s", bcaliasname.ToStdString().c_str());
			UI::MessageDlg(buf);
			return false;
		}
		lbcmed_aliasTable.push_back(bcaliasname);
		
		wxColour wc = m_pLocalBCGrid->GetCellBackgroundColour(i, 2);
		unsigned long val;
		unsigned int id = 0;
		if (m_pLocalBCGrid->GetCellValue(i, 3).ToCULong(&val))
			id = static_cast<unsigned int>(val);
		if (val < 1 || val >= maxIDNum) {
			UI::MessageDlg("LocalBC: ID must be 1 to 30.");
			return false;
		}
		if (useid[id])
		{
			char buf[64] = {};
			sprintf(buf, "LocalBC/Medium: double IDs>%d", id);
			UI::MessageDlg(buf);
			return false;
		}
		useid[id] = 1;
	}
	
	// Medium
	const int med = m_pMedGrid->GetRows();
	for (int i = 0; i < med; ++i) {
		wxString labelname = m_pMedGrid->GetCellValue(i, 0);
		if (labelname == "")
		{
			UI::MessageDlg("Medium: Empty Label");
			return false;
		}

		if (std::find(lbcmed_aliasTable.begin(), lbcmed_aliasTable.end(), labelname) != lbcmed_aliasTable.end()) {
			char buf[128] = {};
			sprintf(buf, "LocalBC/Medium: double Alias/Label>%s", labelname.ToStdString().c_str());
			UI::MessageDlg(buf);
			return false;
		}
		lbcmed_aliasTable.push_back(labelname);
		
		wxColour wc = m_pMedGrid->GetCellBackgroundColour(i, 1);
		unsigned long val;
		unsigned int id = 0;
		if (m_pMedGrid->GetCellValue(i, 2).ToCULong(&val))
			id = static_cast<unsigned int>(val);
		if (val < 1 || val >= maxIDNum) {
			UI::MessageDlg("Medium: ID must be 1 to 30.");
			return false;
		}
		if (useid[id])
		{
			char buf[64] = {};
			sprintf(buf, "LocalBC/Medium: double IDs>%d", id);
			UI::MessageDlg(buf);
			return false;
		}
		std::string typeName = m_pMedGrid->GetCellValue(i, 3).ToStdString();
		useid[id] = 1;
	}
	
	// OuterBC
	std::vector<wxString> obc_aliasTable;
	const int obc = m_pOuterBCGrid->GetRows();
	for (int i = 0; i < obc; ++i) {
		wxString bcaliasname = m_pOuterBCGrid->GetCellValue(i, 0);
		wxString bcclassname = m_pOuterBCGrid->GetCellValue(i, 1);
		if (std::find(obc_aliasTable.begin(), obc_aliasTable.end(), bcaliasname) != obc_aliasTable.end()) {
			char buf[128] = {};
			sprintf(buf, "OuterBC: There are same Alias>%s", bcaliasname.ToStdString().c_str());
			UI::MessageDlg(buf);
			return false;
		}
		obc_aliasTable.push_back(bcaliasname);
	}
	
	return true;
}

void fxgenGUIBCMediumManagerDlgBase::OnOKBtn( wxCommandEvent& event )
{
	m_pOuterBCGrid->EnableEditing(false);
	m_pLocalBCGrid->EnableEditing(false);
	m_pMedGrid->EnableEditing(false);
	
	if (!checkValues())
		return ;
	
	m_func.ClearLocalBC();
	int lbc = m_pLocalBCGrid->GetRows();
	for (int i = 0; i < lbc; ++i) {
		wxString bcaliasname = m_pLocalBCGrid->GetCellValue(i, 0);
		wxString bcclassname = m_pLocalBCGrid->GetCellValue(i, 1);
		wxColour wc = m_pLocalBCGrid->GetCellBackgroundColour(i, 2);
		unsigned long val;
		unsigned int id = 0;
		if (m_pLocalBCGrid->GetCellValue(i, 3).ToCULong(&val))
			id = static_cast<unsigned int>(val);
	
		float col[4] = {wc.Red()/255.0f,wc.Green()/255.0f,wc.Blue()/255.0f,1};
		wxString medium = "";
		m_func.AddLocalBC(m_func.CreateLocalBC(std::string(bcaliasname.c_str().AsChar()), std::string(bcclassname.c_str().AsChar()),
						  col, id , std::string(medium.c_str().AsChar())));
	}
	
	m_func.ClearOuterBC();
	int obc = m_pOuterBCGrid->GetRows();
	for (int i = 0; i < obc; ++i) {
		wxString bcaliasname = m_pOuterBCGrid->GetCellValue(i, 0);
		wxString bcclassname = m_pOuterBCGrid->GetCellValue(i, 1);
		
		m_func.AddOuterBC(m_func.CreateOuterBC(std::string(bcaliasname.c_str().AsChar()), std::string(bcclassname.c_str().AsChar())));
	}
	
	m_func.ClearMedium();
	int med = m_pMedGrid->GetRows();
	for (int i = 0; i < med; ++i) {
		wxString labelname = m_pMedGrid->GetCellValue(i, 0);
		wxColour wc = m_pMedGrid->GetCellBackgroundColour(i, 1);
		unsigned long val;
		unsigned int id = 0;
		if (m_pMedGrid->GetCellValue(i, 2).ToCULong(&val))
			id = static_cast<unsigned int>(val);
		
		std::string typeName = m_pMedGrid->GetCellValue(i, 3).ToStdString();
		float col[4] = {wc.Red()/255.0f,wc.Green()/255.0f,wc.Blue()/255.0f,1};
		m_func.AddMedium(m_func.CreateMedium(std::string(labelname.c_str().AsChar()), col, id, typeName));
	}


	EndModal(1);
}

void fxgenGUIBCMediumManagerDlgBase::OnCancelBtn( wxCommandEvent& event )
{
	EndModal(0);
}
