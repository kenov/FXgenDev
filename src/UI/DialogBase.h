///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Sep  8 2010)
// http://www.wxformbuilder.org/
//
// PLEASE DO "NOT" EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#ifndef __DialogBase__
#define __DialogBase__

#include <wx/string.h>
#include <wx/stattext.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/statline.h>
#include <wx/radiobut.h>
#include <wx/sizer.h>
#include <wx/textctrl.h>
#include <wx/checkbox.h>
#include <wx/choice.h>
#include <wx/button.h>
#include <wx/panel.h>
#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/icon.h>
#include <wx/bmpbuttn.h>
#include <wx/dialog.h>
#include <wx/listctrl.h>
#include <wx/grid.h>
#include <wx/choicebk.h>
#include <wx/treectrl.h>
#include <wx/slider.h>
#include <wx/statbmp.h>
#include <wx/listbox.h>
#include <wx/gauge.h>
#include <wx/notebook.h>
#include <wx/spinctrl.h>
#include <wx/frame.h>

///////////////////////////////////////////////////////////////////////////

namespace UI
{
	#define SLICE_CTRL_IDX_RADIO 1000
	#define SLICE_CTRL_COORD_RADIO 1001
	#define SLICE_CTRL_XPOS_TXT 1002
	#define SLICE_CTRL_XPITCH_TXT 1003
	#define SLICE_CTRL_YPOS_TXT 1004
	#define SLICE_CTRL_YPITCH_TXT 1005
	#define SLICE_CTRL_ZPOS_TXT 1006
	#define SLICE_CTRL_ZPITCH_TXT 1007
	#define SLICE_CTRL_XPOS_P_TXT 1008
	#define SLICE_CTRL_XPITCH_P_TXT 1009
	#define SLICE_CTRL_YPOS_P_TXT 1010
	#define SLICE_CTRL_YPITCH_P_TXT 1011
	#define SLICE_CTRL_ZPOS_P_TXT 1012
	#define SLICE_CTRL_ZPITCH_P_TXT 1013
	#define SIZE_P_TXT 1014
	#define SLICE_CTRL_XPOS_R_TXT 1015
	#define SLICE_CTRL_XPITCH_R_TXT 1016
	#define SLICE_CTRL_YPOS_R_TXT 1017
	#define SLICE_CTRL_YPITCH_R_TXT 1018
	#define SLICE_CTRL_ZPOS_R_TXT 1019
	#define SLICE_CTRL_ZPITCH_R_TXT 1020
	#define WIDTH_R_TXT 1021
	#define DEPTH_R_TXT 1022
	#define HEIGHT_R_TXT 1023
	#define ORIENTATION_VEC_X_R_TXT 1024
	#define DIR_VEC_X_R_TXT 1025
	#define DIR_VEC_Y_R_TXT 1026
	#define DIR_VEC_Z_R_TXT 1027
	#define SLICE_CTRL_XPOS_C_TXT 1028
	#define SLICE_CTRL_XPITCH_C_TXT 1029
	#define SLICE_CTRL_YPOS_C_TXT 1030
	#define SLICE_CTRL_YPITCH_C_TXT 1031
	#define SLICE_CTRL_ZPOS_C_TXT 1032
	#define SLICE_CTRL_ZPITCH_C_TXT 1033
	#define DEPTH_C_TXT 1034
	#define FAN_RADIUS_C_TXT 1035
	#define BOSS_RADIUS_C_TXT 1036
	#define ORIENTATION_VEC_X_C_TXT 1037
	#define ORIENTATION_VEC_Y_C_TXT 1038
	#define ORIENTATION_VEC_Z_C_TXT 1039
	
	///////////////////////////////////////////////////////////////////////////////
	/// Class DomainCartDlgBase
	///////////////////////////////////////////////////////////////////////////////
	class DomainCartDlgBase : public wxDialog 
	{
		private:
		
		protected:
			wxBoxSizer* m_pBaseSizer;
			wxBoxSizer* m_pTopSizer;
			wxStaticText* m_staticText31;
			wxPanel* m_panel2;
			wxStaticText* m_staticText2;
			wxStaticLine* m_staticline132;
			wxRadioButton* m_pPolicySizeRBtn;
			wxStaticText* m_staticText21;
			wxRadioButton* m_pPolicyMaxRBtn;
			wxStaticText* m_staticText211;
			
			wxStaticText* m_staticText3;
			wxStaticText* m_staticText4;
			wxStaticText* m_staticText41;
			wxStaticText* m_staticText42;
			wxTextCtrl* m_pMinXTxt;
			wxTextCtrl* m_pMinYTxt;
			wxTextCtrl* m_pMinZTxt;
			wxStaticText* m_staticText7;
			wxTextCtrl* m_pMaxXTxt;
			wxTextCtrl* m_pMaxYTxt;
			wxTextCtrl* m_pMaxZTxt;
			wxStaticText* m_staticText11;
			wxTextCtrl* m_pSizeXTxt;
			wxTextCtrl* m_pSizeYTxt;
			wxTextCtrl* m_pSizeZTxt;
			wxStaticText* m_staticText421;
			wxCheckBox* m_pCenterXChk;
			wxCheckBox* m_pCenterYChk;
			wxCheckBox* m_pCenterZChk;
			wxStaticText* m_staticText251;
			wxChoice* m_pUnitChoice;
			wxButton* m_pDefaultBtn;
			wxStaticText* m_staticText311;
			wxBoxSizer* m_pDivisionSettingBoxSizer;
			wxPanel* m_pVoxPanel;
			wxBitmapButton* m_pVoxBtn;
			wxStaticText* m_staticText3111;
			wxBoxSizer* m_pVoxBoxSizer;
			wxCheckBox* m_pUnifChk;
			wxStaticText* m_staticText24;
			
			wxStaticText* m_staticText261;
			wxStaticText* m_staticText2612;
			wxStaticText* m_staticText2613;
			wxStaticText* m_staticText22;
			wxTextCtrl* m_pPitchXTxt;
			wxTextCtrl* m_pPitchYTxt;
			wxTextCtrl* m_pPitchZTxt;
			wxStaticText* m_staticText271;
			wxTextCtrl* m_pVoxXTxt;
			wxTextCtrl* m_pVoxYTxt;
			wxTextCtrl* m_pVoxZTxt;
			wxStaticText* m_staticText23;
			wxStaticLine* m_staticline91;
			wxRadioButton* m_pExpPolicyPitchBtn;
			wxStaticText* m_staticText212;
			wxRadioButton* m_pExpPolicyVoxBtn;
			wxStaticText* m_staticText2111;
			wxPanel* m_pDvdPanel;
			wxBitmapButton* m_pDvdBtn;
			wxStaticText* m_staticText31111;
			wxBoxSizer* m_pDvdBoxSizer;
			
			wxStaticText* m_staticText28113;
			wxStaticText* m_staticText281111;
			wxStaticText* m_staticText281121;
			wxStaticText* m_staticText2812;
			wxTextCtrl* m_pDvdXTxt;
			wxTextCtrl* m_pDvdYTxt;
			wxTextCtrl* m_pDvdZTxt;
			wxStaticText* m_staticText2911;
			wxTextCtrl* m_pDvdAutoTxt;
			wxStaticLine* m_staticline1;
			wxButton* m_pOKBtn;
			wxButton* m_pCancelBtn;
			
			// Virtual event handlers, overide them in your derived class
			virtual void OnClose( wxCloseEvent& event ) { event.Skip(); }
			virtual void OnPolicyRadio( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnBBoxTxtOnKillFocus( wxFocusEvent& event ) { event.Skip(); }
			virtual void OnBBoxTxt( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnCenterChk( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnUnitChoice( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnDefaultBtn( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnEnableVoxelBtn( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnForceUnifChk( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnPitchTxtOnKillFocus( wxFocusEvent& event ) { event.Skip(); }
			virtual void OnPitchTxt( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnVoxTxtOnKillFocus( wxFocusEvent& event ) { event.Skip(); }
			virtual void OnVoxTxt( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnExportPolicyRadio( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnEnableDvdBtn( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnDvdManuTxtOnKillFocus( wxFocusEvent& event ) { event.Skip(); }
			virtual void OnDvdManuTxt( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnDvdAutoTxtOnKillFocus( wxFocusEvent& event ) { event.Skip(); }
			virtual void OnDvdAutoTxt( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnOKBtn( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnCancelBtn( wxCommandEvent& event ) { event.Skip(); }
			
		
		public:
			
			DomainCartDlgBase( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("Cartesian Domain Property"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( -1,-1 ), long style = wxCAPTION );
			~DomainCartDlgBase();
		
	};
	
	///////////////////////////////////////////////////////////////////////////////
	/// Class DomainBCMDlgBase
	///////////////////////////////////////////////////////////////////////////////
	class DomainBCMDlgBase : public wxDialog 
	{
		private:
		
		protected:
			wxBoxSizer* m_pBaseSizer;
			wxBoxSizer* m_pTopSizer;
			wxStaticText* m_staticText31;
			wxPanel* m_panel2;
			wxStaticText* m_staticText2;
			wxStaticLine* m_staticline132;
			wxRadioButton* m_pPolicySizeRBtn;
			wxStaticText* m_staticText21;
			wxRadioButton* m_pPolicyMaxRBtn;
			wxStaticText* m_staticText211;
			
			wxStaticText* m_staticText3;
			wxStaticText* m_staticText4;
			wxStaticText* m_staticText41;
			wxStaticText* m_staticText42;
			wxTextCtrl* m_pMinXTxt;
			wxTextCtrl* m_pMinYTxt;
			wxTextCtrl* m_pMinZTxt;
			wxStaticText* m_staticText7;
			wxTextCtrl* m_pMaxXTxt;
			wxTextCtrl* m_pMaxYTxt;
			wxTextCtrl* m_pMaxZTxt;
			wxStaticText* m_staticText11;
			wxTextCtrl* m_pSizeXTxt;
			wxTextCtrl* m_pSizeYTxt;
			wxTextCtrl* m_pSizeZTxt;
			wxStaticText* m_staticText421;
			wxCheckBox* m_pCenterXChk;
			wxCheckBox* m_pCenterYChk;
			wxCheckBox* m_pCenterZChk;
			wxStaticText* m_staticText251;
			wxChoice* m_pUnitChoice;
			wxButton* m_pDefaultBtn;
			wxStaticText* m_staticText311;
			wxBoxSizer* m_pVoxDvdBoxSizer;
			wxChoicebook* m_choicebook1;
			wxPanel* m_pVoxPanel;
			wxStaticText* m_staticText23212;
			wxCheckBox* m_pUnifChk;
			wxStaticText* m_staticText24;
			
			wxStaticText* m_staticText261;
			wxStaticText* m_staticText2612;
			wxStaticText* m_staticText2613;
			wxStaticText* m_staticText22;
			wxTextCtrl* m_pRootLengthXTxt;
			wxTextCtrl* m_pRootLengthYTxt;
			wxTextCtrl* m_pRootLengthZTxt;
			wxStaticText* m_staticText271;
			wxTextCtrl* m_pRootDimsXTxt;
			wxTextCtrl* m_pRootDimsYTxt;
			wxTextCtrl* m_pRootDimsZTxt;
			wxStaticLine* m_staticline40;
			wxStaticText* m_staticText232122;
			wxStaticText* m_staticText23;
			wxTextCtrl* m_pBaseLevelTxt;
			wxStaticText* m_staticText231;
			wxStaticText* m_staticText233;
			wxTextCtrl* m_pMinLevelTxt;
			wxStaticText* m_staticText2311;
			wxStaticLine* m_staticline401;
			wxStaticText* m_staticText2322;
			wxStaticLine* m_staticline911;
			wxRadioButton* m_pExpPolicyParamBtn;
			wxStaticText* m_staticText2121;
			wxRadioButton* m_pExpPolicyOctBtn;
			wxStaticText* m_staticText21111;
			wxPanel* m_pVoxPanel2;
			wxStaticText* m_staticText2321211;
			wxStaticText* m_staticText26111;
			wxTextCtrl* m_pBlockSizeXTxt;
			wxStaticText* m_staticText261211;
			wxTextCtrl* m_pBlockSizeYTxt;
			wxStaticText* m_staticText261311;
			wxTextCtrl* m_pBlockSizeZTxt;
			wxStaticLine* m_staticline4021;
			wxStaticText* m_staticText23221;
			wxChoice* m_pLeafOrderingChoice;
			wxPanel* m_pVoxPanel1;
			wxStaticText* m_staticText2321;
			wxListCtrl* m_pRgnScopeListCtrl;
			wxButton* m_pAddRgnBtn;
			wxButton* m_pDeleteRgnBtn;
			wxPanel* m_pVoxPanel11;
			wxStaticText* m_staticText232111;
			wxListCtrl* m_pGeoScopeListCtrl;
			wxButton* m_pAddGeoBtn;
			wxButton* m_pDeleteGeoBtn;
			wxPanel* m_pVoxPanel3;
			wxStaticText* m_staticText100;
			wxCheckBox* m_pUseDistanceChk;
			wxStaticText* m_staticText109;
			wxGrid* m_pMarginGrid;
			wxButton* m_pAddMarginBtn;
			wxButton* m_pDeleteMarginBtn;
			wxPanel* m_pVoxPanel12;
			wxStaticText* m_staticText23211;
			wxTextCtrl* m_pInfoText;
			wxButton* m_pCreateOctreeBtn;
			wxButton* m_pDeleteOctreeBtn;
			wxStaticText* m_staticText312;
			wxPanel* m_panel21;
			wxStaticText* m_staticText2212;
			wxTextCtrl* m_pParallelDivTxt;
			wxStaticLine* m_staticline1;
			wxButton* m_pOKBtn;
			wxButton* m_pCancelBtn;
			
			// Virtual event handlers, overide them in your derived class
			virtual void OnClose( wxCloseEvent& event ) { event.Skip(); }
			virtual void OnPolicyRadio( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnBBoxTxtOnKillFocus( wxFocusEvent& event ) { event.Skip(); }
			virtual void OnBBoxTxt( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnCenterChk( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnUnitChoice( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnDefaultBtn( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnForceUnifChk( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnRootLengthTxtOnKillFocus( wxFocusEvent& event ) { event.Skip(); }
			virtual void OnRootLengthTxt( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnRootDimsTxtOnKillFocus( wxFocusEvent& event ) { event.Skip(); }
			virtual void OnRootDimsTxt( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnLevelTxtOnKillFocus( wxFocusEvent& event ) { event.Skip(); }
			virtual void OnLevelTxt( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnExportPolicyRadio( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnBlockSizeTxt( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnLeafOrderingChoice( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnAddRgnBtn( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnDeleteRgnBtn( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnAddGeoBtn( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnDeleteGeoBtn( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnUseDistanceChk( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnChangeMargin( wxGridEvent& event ) { event.Skip(); }
			virtual void OnAddMarginBtn( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnDeleteMarginBtn( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnCreateOctreeBtn( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnDeleteOctreeBtn( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnParallelDivTxtOnKillFocus( wxFocusEvent& event ) { event.Skip(); }
			virtual void OnParallelDivTxt( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnOKBtn( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnCancelBtn( wxCommandEvent& event ) { event.Skip(); }
			
		
		public:
			
			DomainBCMDlgBase( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("BCM Domain Property"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( -1,-1 ), long style = wxCAPTION );
			~DomainBCMDlgBase();
		
	};
	
	///////////////////////////////////////////////////////////////////////////////
	/// Class RegionScopeDlgBase
	///////////////////////////////////////////////////////////////////////////////
	class RegionScopeDlgBase : public wxDialog 
	{
		private:
		
		protected:
			wxBoxSizer* m_pBaseSizer;
			wxBoxSizer* m_pTopSizer;
			wxStaticText* m_staticText31;
			wxPanel* m_panel2;
			wxStaticText* m_staticText2;
			wxStaticLine* m_staticline132;
			wxRadioButton* m_pPolicySizeRBtn;
			wxStaticText* m_staticText21;
			wxRadioButton* m_pPolicyMaxRBtn;
			wxStaticText* m_staticText211;
			
			wxStaticText* m_staticText3;
			wxStaticText* m_staticText4;
			wxStaticText* m_staticText41;
			wxStaticText* m_staticText42;
			wxTextCtrl* m_pMinXTxt;
			wxTextCtrl* m_pMinYTxt;
			wxTextCtrl* m_pMinZTxt;
			wxStaticText* m_staticText7;
			wxTextCtrl* m_pMaxXTxt;
			wxTextCtrl* m_pMaxYTxt;
			wxTextCtrl* m_pMaxZTxt;
			wxStaticText* m_staticText11;
			wxTextCtrl* m_pSizeXTxt;
			wxTextCtrl* m_pSizeYTxt;
			wxTextCtrl* m_pSizeZTxt;
			wxStaticText* m_staticText22;
			wxTextCtrl* m_pLevelTxt;
			wxStaticText* m_staticText184;
			wxStaticText* m_staticText23;
			wxStaticLine* m_staticline1321;
			wxRadioButton* m_pMarginCellRBtn;
			wxStaticText* m_staticText212;
			wxRadioButton* m_pMarginRatioRBtn;
			wxStaticText* m_staticText2111;
			wxStaticText* m_staticText221;
			wxTextCtrl* m_pMarginTxt;
			wxStaticLine* m_staticline33;
			wxButton* m_pOKBtn;
			wxButton* m_pCancelBtn;
			
			// Virtual event handlers, overide them in your derived class
			virtual void OnClose( wxCloseEvent& event ) { event.Skip(); }
			virtual void OnPolicyRadio( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnBBoxTxt( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnLevelTxt( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnMarginRadio( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnMarginTxt( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnOKBtn( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnCancelBtn( wxCommandEvent& event ) { event.Skip(); }
			
		
		public:
			
			RegionScopeDlgBase( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("Region Scope Setting"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( -1,-1 ), long style = wxCAPTION );
			~RegionScopeDlgBase();
		
	};
	
	///////////////////////////////////////////////////////////////////////////////
	/// Class GeometryScopeDlgBase
	///////////////////////////////////////////////////////////////////////////////
	class GeometryScopeDlgBase : public wxDialog 
	{
		private:
		
		protected:
			wxBoxSizer* m_pBaseSizer;
			wxBoxSizer* m_pTopSizer;
			wxStaticText* m_staticText31;
			wxPanel* m_panel2;
			wxTreeCtrl* m_pTreeCtrl;
			wxStaticText* m_staticText22;
			wxTextCtrl* m_pLevelTxt;
			wxStaticText* m_staticText183;
			wxStaticText* m_staticText23;
			wxStaticLine* m_staticline1321;
			wxRadioButton* m_pGeoMarginCellRBtn;
			wxStaticText* m_staticText212;
			wxRadioButton* m_pGeoMarginRatioRBtn;
			wxStaticText* m_staticText2111;
			wxStaticText* m_staticText221;
			wxTextCtrl* m_pGeoMarginTxt;
			wxStaticLine* m_staticline33;
			wxButton* m_pOKBtn;
			wxButton* m_pCancelBtn;
			
			// Virtual event handlers, overide them in your derived class
			virtual void OnClose( wxCloseEvent& event ) { event.Skip(); }
			virtual void OnLevelTxt( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnMarginRadio( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnMarginTxt( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnOKBtn( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnCancelBtn( wxCommandEvent& event ) { event.Skip(); }
			
		
		public:
			
			GeometryScopeDlgBase( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("Geometry Scope Setting"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( -1,-1 ), long style = wxCAPTION );
			~GeometryScopeDlgBase();
		
	};
	
	///////////////////////////////////////////////////////////////////////////////
	/// Class SliceCtrlDlgBase
	///////////////////////////////////////////////////////////////////////////////
	class SliceCtrlDlgBase : public wxDialog 
	{
		private:
		
		protected:
			wxBoxSizer* m_pBaseSizer;
			wxBoxSizer* m_pTopSizer;
			wxStaticText* m_staticText31;
			wxPanel* m_panel2;
			wxStaticText* m_staticText77;
			wxRadioButton* m_pPolicyIdxBtn;
			wxStaticText* m_pPolicyIdxLabel;
			wxRadioButton* m_pPolicyCoordBtn;
			wxStaticText* m_pPolicyCoordLabel;
			wxPanel* m_panel17;
			wxStaticText* m_staticText107;
			wxStaticText* m_staticText108;
			wxStaticText* m_staticText109;
			wxStaticText* m_staticText110;
			wxStaticText* m_staticText931;
			wxBitmapButton* m_pXPosDownBtn;
			wxSlider* m_pXSlider;
			wxBitmapButton* m_pXPosUpBtn;
			wxTextCtrl* m_pXPosTxt;
			wxTextCtrl* m_pXPitchTxt;
			wxStaticText* m_staticText9311;
			wxBitmapButton* m_pYPosDownBtn;
			wxSlider* m_pYSlider;
			wxBitmapButton* m_pYPosUpBtn;
			wxTextCtrl* m_pYPosTxt;
			wxTextCtrl* m_pYPitchTxt;
			wxStaticText* m_staticText9312;
			wxBitmapButton* m_pZPosDownBtn;
			wxSlider* m_pZSlider;
			wxBitmapButton* m_pZPosUpBtn;
			wxTextCtrl* m_pZPosTxt;
			wxTextCtrl* m_pZPitchTxt;
			wxBoxSizer* m_pColorMapSizer;
			wxPanel* m_panel24;
			wxStaticText* m_staticText1071;
			wxStaticBitmap* m_pColorMapImage;
			wxStaticText* m_staticText1072;
			wxStaticText* m_staticText103;
			wxStaticText* m_staticText104;
			wxStaticText* m_staticText1081;
			wxStaticText* m_staticText106;
			wxChoice* m_pColorMapRangeMinLevel;
			wxChoice* m_pColorMapRangeMaxLevel;
			wxStaticText* m_pColorMapRangeTxt;
			wxStaticLine* m_staticline1;
			
			wxButton* m_pCloseBtn;
			
			// Virtual event handlers, overide them in your derived class
			virtual void OnRadioButton( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnXPosDown( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnXSlideScroll( wxScrollEvent& event ) { event.Skip(); }
			virtual void OnXSlideRelease( wxScrollEvent& event ) { event.Skip(); }
			virtual void OnXSlideDrag( wxScrollEvent& event ) { event.Skip(); }
			virtual void OnXPosUp( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnPosTxt( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnPitchTxt( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnYPosDown( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnYSlideScroll( wxScrollEvent& event ) { event.Skip(); }
			virtual void OnYSlideRelease( wxScrollEvent& event ) { event.Skip(); }
			virtual void OnYSlideDrag( wxScrollEvent& event ) { event.Skip(); }
			virtual void OnYPosUp( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnZPosDown( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnZSlideScroll( wxScrollEvent& event ) { event.Skip(); }
			virtual void OnZSlideRelease( wxScrollEvent& event ) { event.Skip(); }
			virtual void OnZSlideDrag( wxScrollEvent& event ) { event.Skip(); }
			virtual void OnZPosUp( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnColorMapMinLevelChoice( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnColorMapMaxLevelChoice( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnCloseBtn( wxCommandEvent& event ) { event.Skip(); }
			
		
		public:
			
			SliceCtrlDlgBase( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("Slice Control"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 533,349 ), long style = wxCAPTION );
			~SliceCtrlDlgBase();
		
	};
	
	///////////////////////////////////////////////////////////////////////////////
	/// Class BCMediumManagerDlgBase
	///////////////////////////////////////////////////////////////////////////////
	class BCMediumManagerDlgBase : public wxDialog 
	{
		private:
		
		protected:
			wxStaticText* m_staticText30;
			wxPanel* m_panel3;
			wxGrid* m_pMedGrid;
			wxButton* m_pNewMedBtn;
			wxButton* m_pDelMedBtn;
			wxStaticText* m_staticText301;
			wxPanel* m_panel4;
			wxGrid* m_pOuterBCGrid;
			wxButton* m_pNewOuterBCBtn;
			wxButton* m_pDelOuterBCBtn;
			wxStaticText* m_staticText3011;
			wxPanel* m_panel5;
			wxGrid* m_pLocalBCGrid;
			wxButton* m_pNewLocalBCBtn;
			wxButton* m_pDelLocalBCBtn;
			wxButton* m_pOKBtn;
			wxButton* m_pCancel;
			
			// Virtual event handlers, overide them in your derived class
			virtual void OnChangeMedValue( wxGridEvent& event ) { event.Skip(); }
			virtual void OnClickMedValue( wxGridEvent& event ) { event.Skip(); }
			virtual void OnNewMedBtn( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnDelMedBtn( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnChangeOuterBCValue( wxGridEvent& event ) { event.Skip(); }
			virtual void OnClickOuterBCValue( wxGridEvent& event ) { event.Skip(); }
			virtual void OnNewOuterBCBtn( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnDelOuterBCBtn( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnChangeLocalBCValue( wxGridEvent& event ) { event.Skip(); }
			virtual void OnClickLocalBCValue( wxGridEvent& event ) { event.Skip(); }
			virtual void OnNewLocalBCBtn( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnDelLocalBCBtn( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnOKBtn( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnCancelBtn( wxCommandEvent& event ) { event.Skip(); }
			
		
		public:
			
			BCMediumManagerDlgBase( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("BC/Medium Manager"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxCAPTION );
			~BCMediumManagerDlgBase();
		
	};
	
	///////////////////////////////////////////////////////////////////////////////
	/// Class BCMediumManagerDlgBase2
	///////////////////////////////////////////////////////////////////////////////
	class BCMediumManagerDlgBase2 : public wxDialog 
	{
		private:
		
		protected:
			wxStaticText* m_staticText30;
			wxPanel* m_panel3;
			wxGrid* m_pMedGrid;
			wxButton* m_pNewMedBtn;
			wxButton* m_pDelMedBtn;
			wxStaticText* m_staticText301;
			wxPanel* m_panel4;
			wxGrid* m_pOuterBCGrid;
			wxButton* m_pNewOuterBCBtn;
			wxButton* m_pDelOuterBCBtn;
			wxStaticText* m_staticText3011;
			wxPanel* m_panel5;
			wxGrid* m_pLocalBCGrid;
			wxButton* m_pNewLocalBCBtn;
			wxButton* m_pDelLocalBCBtn;
			wxButton* m_pOKBtn;
			wxButton* m_pCancel;
			
			// Virtual event handlers, overide them in your derived class
			virtual void OnChangeMedValue( wxGridEvent& event ) { event.Skip(); }
			virtual void OnClickMedValue( wxGridEvent& event ) { event.Skip(); }
			virtual void OnNewMedBtn( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnDelMedBtn( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnChangeOuterBCValue( wxGridEvent& event ) { event.Skip(); }
			virtual void OnClickOuterBCValue( wxGridEvent& event ) { event.Skip(); }
			virtual void OnNewOuterBCBtn( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnDelOuterBCBtn( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnChangeLocalBCValue( wxGridEvent& event ) { event.Skip(); }
			virtual void OnClickLocalBCValue( wxGridEvent& event ) { event.Skip(); }
			virtual void OnNewLocalBCBtn( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnDelLocalBCBtn( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnOKBtn( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnCancelBtn( wxCommandEvent& event ) { event.Skip(); }
			
		
		public:
			
			BCMediumManagerDlgBase2( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("BC/Medium Manager"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 591,620 ), long style = wxCAPTION );
			~BCMediumManagerDlgBase2();
		
	};
	
	///////////////////////////////////////////////////////////////////////////////
	/// Class LocalBCSettingDlgBase
	///////////////////////////////////////////////////////////////////////////////
	class LocalBCSettingDlgBase : public wxDialog 
	{
		private:
		
		protected:
			wxGrid* m_pBCMedGrid;
			wxButton* m_pOKBtn;
			wxButton* m_pCancelBtn;
			
			// Virtual event handlers, overide them in your derived class
			virtual void OnBCMListRangeSelect( wxGridRangeSelectEvent& event ) { event.Skip(); }
			virtual void OnOKBtn( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnCancelBtn( wxCommandEvent& event ) { event.Skip(); }
			
		
		public:
			
			LocalBCSettingDlgBase( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("Local Boundary Setting"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 487,-1 ), long style = wxCAPTION );
			~LocalBCSettingDlgBase();
		
	};
	
	///////////////////////////////////////////////////////////////////////////////
	/// Class LocalBCSettingDlgBase2
	///////////////////////////////////////////////////////////////////////////////
	class LocalBCSettingDlgBase2 : public wxDialog 
	{
		private:
		
		protected:
			wxGrid* m_pBCMedGrid;
			wxButton* m_pOKBtn;
			wxButton* m_pCancelBtn;
			
			// Virtual event handlers, overide them in your derived class
			virtual void OnBCMListRangeSelect( wxGridRangeSelectEvent& event ) { event.Skip(); }
			virtual void OnOKBtn( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnCancelBtn( wxCommandEvent& event ) { event.Skip(); }
			
		
		public:
			
			LocalBCSettingDlgBase2( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("Local Boundary Setting"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 487,-1 ), long style = wxCAPTION );
			~LocalBCSettingDlgBase2();
		
	};
	
	///////////////////////////////////////////////////////////////////////////////
	/// Class SubdomainSettingDlgBase
	///////////////////////////////////////////////////////////////////////////////
	class SubdomainSettingDlgBase : public wxDialog 
	{
		private:
		
		protected:
			wxListBox* m_pStatusList;
			wxButton* m_pOKBtn;
			wxButton* m_pCancelBtn;
			
			// Virtual event handlers, overide them in your derived class
			virtual void OnOKBtn( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnCancelBtn( wxCommandEvent& event ) { event.Skip(); }
			
		
		public:
			
			SubdomainSettingDlgBase( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("Subdomain Setting"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( -1,-1 ), long style = wxCAPTION );
			~SubdomainSettingDlgBase();
		
	};
	
	///////////////////////////////////////////////////////////////////////////////
	/// Class OuterBCSettingDlgBase
	///////////////////////////////////////////////////////////////////////////////
	class OuterBCSettingDlgBase : public wxDialog 
	{
		private:
		
		protected:
			wxPanel* m_panel6;
			wxGrid* m_pBCMedGrid;
			wxButton* m_pOKBtn;
			wxButton* m_pCancelBtn;
			
			// Virtual event handlers, overide them in your derived class
			virtual void OnBCMedGridChange( wxGridEvent& event ) { event.Skip(); }
			virtual void OnOKBtn( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnCancelBtn( wxCommandEvent& event ) { event.Skip(); }
			
		
		public:
			
			OuterBCSettingDlgBase( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("FaceBC Setting"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( -1,-1 ), long style = wxCAPTION );
			~OuterBCSettingDlgBase();
		
	};
	
	///////////////////////////////////////////////////////////////////////////////
	/// Class ProgressBarDlgBase
	///////////////////////////////////////////////////////////////////////////////
	class ProgressBarDlgBase : public wxDialog 
	{
		private:
		
		protected:
			wxPanel* m_panel6;
			wxGauge* m_pProgress;
		
		public:
			
			ProgressBarDlgBase( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("Progree"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = 0 );
			~ProgressBarDlgBase();
		
	};
	
	///////////////////////////////////////////////////////////////////////////////
	/// Class SDImportDlgBase
	///////////////////////////////////////////////////////////////////////////////
	class SDImportDlgBase : public wxDialog 
	{
		private:
		
		protected:
			wxStaticText* m_staticText302;
			wxTextCtrl* m_textCtrl_dfi;
			wxButton* m_button_file_dfi;
			
			wxStaticText* m_staticText301;
			wxTextCtrl* m_textCtrl_cellid;
			wxButton* m_button_file_cellid;
			wxButton* m_button_dir_cellid;
			wxButton* m_button_cid_clear;
			wxStaticText* m_staticText3011;
			wxTextCtrl* m_textCtrl_bcflg;
			wxButton* m_button_file_bcflg;
			wxButton* m_button_dir_bcflg;
			
			wxButton* m_button_bcf_clear;
			
			wxButton* m_pOKBtn;
			wxButton* m_pCancelBtn;
			
			// Virtual event handlers, overide them in your derived class
			virtual void SDImportDlgBaseOnActivate( wxActivateEvent& event ) { event.Skip(); }
			virtual void SDImportDlgBaseOnActivateApp( wxActivateEvent& event ) { event.Skip(); }
			virtual void SDImportDlgBaseOnClose( wxCloseEvent& event ) { event.Skip(); }
			virtual void SDImportDlgBaseOnInitDialog( wxInitDialogEvent& event ) { event.Skip(); }
			virtual void m_button_file_dfiOnButtonClick( wxCommandEvent& event ) { event.Skip(); }
			virtual void m_button_file_cellidOnButtonClick( wxCommandEvent& event ) { event.Skip(); }
			virtual void m_button_dir_cellidOnButtonClick( wxCommandEvent& event ) { event.Skip(); }
			virtual void m_button_cid_clearOnButtonClick( wxCommandEvent& event ) { event.Skip(); }
			virtual void m_button_file_bcflgOnButtonClick( wxCommandEvent& event ) { event.Skip(); }
			virtual void m_button_dir_bcflgOnButtonClick( wxCommandEvent& event ) { event.Skip(); }
			virtual void m_button_bcf_clearOnButtonClick( wxCommandEvent& event ) { event.Skip(); }
			virtual void m_pOKBtnOnButtonClick( wxCommandEvent& event ) { event.Skip(); }
			virtual void m_pCancelBtnOnButtonClick( wxCommandEvent& event ) { event.Skip(); }
			
		
		public:
			
			SDImportDlgBase( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("Import dfi file"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 814,254 ), long style = wxCAPTION|wxCLOSE_BOX|wxDEFAULT_DIALOG_STYLE|wxMAXIMIZE_BOX|wxMINIMIZE_BOX|wxRESIZE_BORDER );
			~SDImportDlgBase();
		
	};
	
	///////////////////////////////////////////////////////////////////////////////
	/// Class SD_Frame
	///////////////////////////////////////////////////////////////////////////////
	class SD_Frame : public wxFrame 
	{
		private:
		
		protected:
			wxBoxSizer* m_pBaseSizer;
			wxBoxSizer* m_pTopSizer;
			wxPanel* m_topdumyPanel;
			wxButton* m_button75;
			wxButton* m_button76;
			wxButton* m_button77;
			wxNotebook* m_notebook;
			wxPanel* m_panel_stagein;
			wxPanel* m_panel_globallocal;
			wxPanel* m_panel58;
			wxButton* m_button94;
			wxPanel* m_panel59;
			wxButton* m_button95;
			wxPanel* m_panel_bcflag;
			wxStaticText* m_staticText191;
			wxSpinCtrl* m_playSpeedspinCtrl;
			
			wxButton* m_pCloseBtn;
			
			// Virtual event handlers, overide them in your derived class
			virtual void OnActivate( wxActivateEvent& event ) { event.Skip(); }
			virtual void OnActivateApp( wxActivateEvent& event ) { event.Skip(); }
			virtual void OnClose( wxCloseEvent& event ) { event.Skip(); }
			virtual void m_notebookOnNotebookPageChanged( wxNotebookEvent& event ) { event.Skip(); }
			virtual void m_notebookOnNotebookPageChanging( wxNotebookEvent& event ) { event.Skip(); }
			virtual void m_pCloseBtnOnButtonClick( wxCommandEvent& event ) { event.Skip(); }
			
		
		public:
			
			SD_Frame( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("SubDomain Viewer"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 292,431 ), long style = wxDEFAULT_FRAME_STYLE|wxFRAME_FLOAT_ON_PARENT|wxTAB_TRAVERSAL );
			~SD_Frame();
		
	};
	
	///////////////////////////////////////////////////////////////////////////////
	/// Class SD_SelectPanel
	///////////////////////////////////////////////////////////////////////////////
	class SD_SelectPanel : public wxPanel 
	{
		private:
		
		protected:
			wxBoxSizer* m_pBaseSizer;
			wxStaticText* m_staticText197;
			
			wxGrid* m_pSubDomainGrid;
			wxButton* m_button_reselection;
			wxButton* m_button_staging;
			wxButton* m_button_subdomainload;
			wxButton* m_button_clear;
			
			
			// Virtual event handlers, overide them in your derived class
			virtual void m_pSubDomainGridOnGridSelectCell( wxGridEvent& event ) { event.Skip(); }
			virtual void m_button_reselectionOnButtonClick( wxCommandEvent& event ) { event.Skip(); }
			virtual void m_button_stagingOnButtonClick( wxCommandEvent& event ) { event.Skip(); }
			virtual void m_button_subdomainloadOnButtonClick( wxCommandEvent& event ) { event.Skip(); }
			virtual void m_button_clearOnButtonClick( wxCommandEvent& event ) { event.Skip(); }
			
		
		public:
			
			SD_SelectPanel( wxWindow* parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 357,284 ), long style = wxTAB_TRAVERSAL );
			~SD_SelectPanel();
		
	};
	
	///////////////////////////////////////////////////////////////////////////////
	/// Class SD_ClippingPanel
	///////////////////////////////////////////////////////////////////////////////
	class SD_ClippingPanel : public wxPanel 
	{
		private:
		
		protected:
			wxStaticText* m_staticText191;
			wxCheckBox* m_checkBox_clipslice_visiable;
			wxPanel* m_panel2;
			wxStaticText* m_staticText77;
			wxRadioButton* m_pPolicyIdxBtn;
			wxStaticText* m_pPolicyIdxLabel;
			wxRadioButton* m_pPolicyCoordBtn;
			wxStaticText* m_pPolicyCoordLabel;
			wxPanel* m_panel17;
			wxStaticText* m_staticText107;
			wxStaticText* m_staticText108;
			wxStaticText* m_staticText109;
			wxStaticText* m_staticText110;
			wxStaticText* m_staticText931;
			wxBitmapButton* m_pXPosDownBtn;
			wxSlider* m_pXSlider;
			wxBitmapButton* m_pXPosUpBtn;
			wxCheckBox* m_play_check_x;
			wxTextCtrl* m_pXPosTxt;
			wxTextCtrl* m_pXPitchTxt;
			wxStaticText* m_staticText9311;
			wxBitmapButton* m_pYPosDownBtn;
			wxSlider* m_pYSlider;
			wxBitmapButton* m_pYPosUpBtn;
			wxCheckBox* m_play_check_y;
			wxTextCtrl* m_pYPosTxt;
			wxTextCtrl* m_pYPitchTxt;
			wxStaticText* m_staticText9312;
			wxBitmapButton* m_pZPosDownBtn;
			wxSlider* m_pZSlider;
			wxBitmapButton* m_pZPosUpBtn;
			wxCheckBox* m_play_check_z;
			wxTextCtrl* m_pZPosTxt;
			wxTextCtrl* m_pZPitchTxt;
			
			// Virtual event handlers, overide them in your derived class
			virtual void m_checkBox_clipslice_visiableOnCheckBox( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnRadioButton( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnXPosDown( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnXSlideScroll( wxScrollEvent& event ) { event.Skip(); }
			virtual void OnXSlideRelease( wxScrollEvent& event ) { event.Skip(); }
			virtual void OnXSlideDrag( wxScrollEvent& event ) { event.Skip(); }
			virtual void OnXPosUp( wxCommandEvent& event ) { event.Skip(); }
			virtual void m_play_check_xOnCheckBox( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnPosTxtKillFocus( wxFocusEvent& event ) { event.Skip(); }
			virtual void OnPosTxt( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnPitchTxtKillFocus( wxFocusEvent& event ) { event.Skip(); }
			virtual void OnPitchTxt( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnYPosDown( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnYSlideScroll( wxScrollEvent& event ) { event.Skip(); }
			virtual void OnYSlideRelease( wxScrollEvent& event ) { event.Skip(); }
			virtual void OnYSlideDrag( wxScrollEvent& event ) { event.Skip(); }
			virtual void OnYPosUp( wxCommandEvent& event ) { event.Skip(); }
			virtual void m_play_check_yOnCheckBox( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnZPosDown( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnZSlideScroll( wxScrollEvent& event ) { event.Skip(); }
			virtual void OnZSlideRelease( wxScrollEvent& event ) { event.Skip(); }
			virtual void OnZSlideDrag( wxScrollEvent& event ) { event.Skip(); }
			virtual void OnZPosUp( wxCommandEvent& event ) { event.Skip(); }
			virtual void m_play_check_zOnCheckBox( wxCommandEvent& event ) { event.Skip(); }
			
		
		public:
			
			SD_ClippingPanel( wxWindow* parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 449,183 ), long style = wxTAB_TRAVERSAL );
			~SD_ClippingPanel();
		
	};
	
	///////////////////////////////////////////////////////////////////////////////
	/// Class SD_GlobalPanel
	///////////////////////////////////////////////////////////////////////////////
	class SD_GlobalPanel : public wxPanel 
	{
		private:
		
		protected:
			wxBoxSizer* m_pTopSizer;
			wxStaticText* m_staticText192;
			wxCheckBox* m_checkBox_global_slice_visiable;
			wxPanel* m_panel2;
			wxStaticText* m_staticText77;
			wxRadioButton* m_pPolicyIdxBtn;
			wxStaticText* m_pPolicyIdxLabel;
			wxRadioButton* m_pPolicyCoordBtn;
			wxStaticText* m_pPolicyCoordLabel;
			wxPanel* m_panel17;
			wxStaticText* m_staticText107;
			wxStaticText* m_staticText108;
			wxStaticText* m_staticText109;
			wxStaticText* m_staticText110;
			wxStaticText* m_staticText931;
			wxBitmapButton* m_pXPosDownBtn;
			wxSlider* m_pXSlider;
			wxBitmapButton* m_pXPosUpBtn;
			wxCheckBox* m_play_check_x;
			wxTextCtrl* m_pXPosTxt;
			wxTextCtrl* m_pXPitchTxt;
			wxStaticText* m_staticText9311;
			wxBitmapButton* m_pYPosDownBtn;
			wxSlider* m_pYSlider;
			wxBitmapButton* m_pYPosUpBtn;
			wxCheckBox* m_play_check_y;
			wxTextCtrl* m_pYPosTxt;
			wxTextCtrl* m_pYPitchTxt;
			wxStaticText* m_staticText9312;
			wxBitmapButton* m_pZPosDownBtn;
			wxSlider* m_pZSlider;
			wxBitmapButton* m_pZPosUpBtn;
			wxCheckBox* m_play_check_z;
			wxTextCtrl* m_pZPosTxt;
			wxTextCtrl* m_pZPitchTxt;
			
			// Virtual event handlers, overide them in your derived class
			virtual void m_checkBox_global_slice_visiableOnCheckBox( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnRadioButton( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnXPosDown( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnXSlideScroll( wxScrollEvent& event ) { event.Skip(); }
			virtual void OnXSlideRelease( wxScrollEvent& event ) { event.Skip(); }
			virtual void OnXSlideDrag( wxScrollEvent& event ) { event.Skip(); }
			virtual void OnXPosUp( wxCommandEvent& event ) { event.Skip(); }
			virtual void m_play_check_xOnCheckBox( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnPosTxtKillFocus( wxFocusEvent& event ) { event.Skip(); }
			virtual void OnPosTxt( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnPitchTxtKillFocus( wxFocusEvent& event ) { event.Skip(); }
			virtual void OnPitchTxt( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnYPosDown( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnYSlideScroll( wxScrollEvent& event ) { event.Skip(); }
			virtual void OnYSlideRelease( wxScrollEvent& event ) { event.Skip(); }
			virtual void OnYSlideDrag( wxScrollEvent& event ) { event.Skip(); }
			virtual void OnYPosUp( wxCommandEvent& event ) { event.Skip(); }
			virtual void m_play_check_yOnCheckBox( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnZPosDown( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnZSlideScroll( wxScrollEvent& event ) { event.Skip(); }
			virtual void OnZSlideRelease( wxScrollEvent& event ) { event.Skip(); }
			virtual void OnZSlideDrag( wxScrollEvent& event ) { event.Skip(); }
			virtual void OnZPosUp( wxCommandEvent& event ) { event.Skip(); }
			virtual void m_play_check_zOnCheckBox( wxCommandEvent& event ) { event.Skip(); }
			
		
		public:
			
			SD_GlobalPanel( wxWindow* parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 464,191 ), long style = wxTAB_TRAVERSAL );
			~SD_GlobalPanel();
		
	};
	
	///////////////////////////////////////////////////////////////////////////////
	/// Class SD_LocalPanel
	///////////////////////////////////////////////////////////////////////////////
	class SD_LocalPanel : public wxPanel 
	{
		private:
		
		protected:
			wxBoxSizer* m_pTopSizer;
			wxStaticText* m_staticText193;
			wxCheckBox* m_checkBox_local_slice_visiable;
			wxPanel* m_panel2;
			wxStaticText* m_staticText77;
			wxRadioButton* m_pPolicyIdxBtn;
			wxStaticText* m_pPolicyIdxLabel;
			wxRadioButton* m_pPolicyCoordBtn;
			wxStaticText* m_pPolicyCoordLabel;
			wxPanel* m_panel17;
			wxStaticText* m_staticText107;
			wxStaticText* m_staticText108;
			wxStaticText* m_staticText109;
			wxStaticText* m_staticText110;
			wxStaticText* m_staticText931;
			wxBitmapButton* m_pXPosDownBtn;
			wxSlider* m_pXSlider;
			wxBitmapButton* m_pXPosUpBtn;
			wxCheckBox* m_play_check_x;
			wxTextCtrl* m_pXPosTxt;
			wxTextCtrl* m_pXPitchTxt;
			wxStaticText* m_staticText9311;
			wxBitmapButton* m_pYPosDownBtn;
			wxSlider* m_pYSlider;
			wxBitmapButton* m_pYPosUpBtn;
			wxCheckBox* m_play_check_y;
			wxTextCtrl* m_pYPosTxt;
			wxTextCtrl* m_pYPitchTxt;
			wxStaticText* m_staticText9312;
			wxBitmapButton* m_pZPosDownBtn;
			wxSlider* m_pZSlider;
			wxBitmapButton* m_pZPosUpBtn;
			wxCheckBox* m_play_check_z;
			wxTextCtrl* m_pZPosTxt;
			wxTextCtrl* m_pZPitchTxt;
			
			// Virtual event handlers, overide them in your derived class
			virtual void m_checkBox_local_slice_visiableOnCheckBox( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnRadioButton( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnXPosDown( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnXSlideScroll( wxScrollEvent& event ) { event.Skip(); }
			virtual void OnXSlideRelease( wxScrollEvent& event ) { event.Skip(); }
			virtual void OnXSlideDrag( wxScrollEvent& event ) { event.Skip(); }
			virtual void OnXPosUp( wxCommandEvent& event ) { event.Skip(); }
			virtual void m_play_check_xOnCheckBox( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnPosTxtKillFocus( wxFocusEvent& event ) { event.Skip(); }
			virtual void OnPosTxt( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnPitchTxtKillFocus( wxFocusEvent& event ) { event.Skip(); }
			virtual void OnPitchTxt( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnYPosDown( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnYSlideScroll( wxScrollEvent& event ) { event.Skip(); }
			virtual void OnYSlideRelease( wxScrollEvent& event ) { event.Skip(); }
			virtual void OnYSlideDrag( wxScrollEvent& event ) { event.Skip(); }
			virtual void OnYPosUp( wxCommandEvent& event ) { event.Skip(); }
			virtual void m_play_check_yOnCheckBox( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnZPosDown( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnZSlideScroll( wxScrollEvent& event ) { event.Skip(); }
			virtual void OnZSlideRelease( wxScrollEvent& event ) { event.Skip(); }
			virtual void OnZSlideDrag( wxScrollEvent& event ) { event.Skip(); }
			virtual void OnZPosUp( wxCommandEvent& event ) { event.Skip(); }
			virtual void m_play_check_zOnCheckBox( wxCommandEvent& event ) { event.Skip(); }
			
		
		public:
			
			SD_LocalPanel( wxWindow* parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 452,183 ), long style = wxTAB_TRAVERSAL );
			~SD_LocalPanel();
		
	};
	
	///////////////////////////////////////////////////////////////////////////////
	/// Class SD_BcflagPanel
	///////////////////////////////////////////////////////////////////////////////
	class SD_BcflagPanel : public wxPanel 
	{
		private:
		
		protected:
			wxStaticText* m_staticText194;
			wxGrid* m_pMedGrid;
			wxStaticText* m_staticText195;
			wxGrid* m_pLocalBCGrid;
			
			wxCheckBox* m_checkBox_hyde_globalPlane;
			wxCheckBox* m_checkBox_hyde_LocalPlanes;
			wxButton* m_button_apply;
			
			// Virtual event handlers, overide them in your derived class
			virtual void m_checkBox_hyde_globalPlaneOnCheckBox( wxCommandEvent& event ) { event.Skip(); }
			virtual void m_checkBox_hyde_LocalPlanesOnCheckBox( wxCommandEvent& event ) { event.Skip(); }
			virtual void m_button_applyOnButtonClick( wxCommandEvent& event ) { event.Skip(); }
			
		
		public:
			
			SD_BcflagPanel( wxWindow* parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 378,356 ), long style = wxTAB_TRAVERSAL );
			~SD_BcflagPanel();
		
	};
	
	///////////////////////////////////////////////////////////////////////////////
	/// Class dumy
	///////////////////////////////////////////////////////////////////////////////
	class dumy : public wxPanel 
	{
		private:
		
		protected:
		
		public:
			
			dumy( wxWindow* parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 500,300 ), long style = wxTAB_TRAVERSAL );
			~dumy();
		
	};
	
	///////////////////////////////////////////////////////////////////////////////
	/// Class CoordinateViewerFrame
	///////////////////////////////////////////////////////////////////////////////
	class CoordinateViewerFrame : public wxFrame 
	{
		private:
		
		protected:
			wxNotebook* m_notebook4;
			wxPanel* m_panel41;
			wxPanel* m_panel44;
			wxStaticText* m_staticText186;
			wxStaticText* m_staticText187;
			wxStaticText* m_staticText202;
			wxStaticText* m_staticText188;
			wxStaticText* m_staticText201;
			wxStaticText* m_staticText189;
			wxStaticText* m_staticText190;
			wxStaticText* m_staticText161;
			wxBitmapButton* m_pXPosDownBtn_p;
			wxSlider* m_pXSlider_p;
			wxBitmapButton* m_pXPosUpBtn_p;
			wxTextCtrl* m_pXPosTxt_p;
			wxTextCtrl* m_pXPitchTxt_p;
			wxStaticText* m_staticText162;
			wxBitmapButton* m_pYPosDownBtn_p;
			wxSlider* m_pYSlider_p;
			wxBitmapButton* m_pYPosUpBtn_p;
			wxTextCtrl* m_pYPosTxt_p;
			wxTextCtrl* m_pYPitchTxt_p;
			wxStaticText* m_staticText163;
			wxBitmapButton* m_pZPosDownBtn_p;
			wxSlider* m_pZSlider_p;
			wxBitmapButton* m_pZPosUpBtn_p;
			wxTextCtrl* m_pZPosTxt_p;
			wxTextCtrl* m_pZPitchTxt_p;
			wxPanel* m_panel421;
			wxStaticText* m_staticText166;
			wxStaticText* m_staticText167;
			wxTextCtrl* m_pSizeTxt_p;
			wxStaticLine* m_staticline16;
			wxButton* m_button_Add_p;
			wxButton* m_button_Clear_p;
			wxButton* m_button_AllClear_p;
			wxStaticLine* m_staticline17;
			wxPanel* m_panel62;
			wxStaticText* m_staticText1861;
			wxGrid* m_pPointGrid_p;
			wxPanel* m_panel413;
			wxPanel* m_panel441;
			wxStaticText* m_staticText1862;
			wxStaticText* m_staticText1872;
			wxStaticText* m_staticText2021;
			wxStaticText* m_staticText1882;
			wxStaticText* m_staticText2011;
			wxStaticText* m_staticText1892;
			wxStaticText* m_staticText1902;
			wxStaticText* m_staticText1612;
			wxBitmapButton* m_pXPosDownBtn_r;
			wxSlider* m_pXSlider_r;
			wxBitmapButton* m_pXPosUpBtn_r;
			wxTextCtrl* m_pXPosTxt_r;
			wxTextCtrl* m_pXPitchTxt_r;
			wxStaticText* m_staticText1622;
			wxBitmapButton* m_pYPosDownBtn_r;
			wxSlider* m_pYSlider_r;
			wxBitmapButton* m_pYPosUpBtn_r;
			wxTextCtrl* m_pYPosTxt_r;
			wxTextCtrl* m_pYPitchTxt_r;
			wxStaticText* m_staticText1632;
			wxBitmapButton* m_pZPosDownBtn_r;
			wxSlider* m_pZSlider_r;
			wxBitmapButton* m_pZPosUpBtn_r;
			wxTextCtrl* m_pZPosTxt_r;
			wxTextCtrl* m_pZPitchTxt_r;
			wxPanel* m_panel4213;
			wxStaticText* m_staticText1663;
			wxStaticText* m_staticText1673;
			wxTextCtrl* m_pWidthTxt_r;
			wxStaticText* m_staticText207;
			wxStaticText* m_staticText208;
			wxStaticText* m_staticText210;
			wxStaticText* m_staticText211;
			wxStaticText* m_staticText260;
			wxStaticText* m_staticText261;
			wxTextCtrl* m_pDepthTxt_r;
			wxStaticText* m_staticText212;
			wxStaticText* m_staticText213;
			wxStaticText* m_staticText214;
			wxStaticText* m_staticText215;
			wxStaticText* m_staticText262;
			wxStaticText* m_staticText263;
			wxTextCtrl* m_pHeightTxt_r;
			wxStaticText* m_staticText216;
			wxStaticText* m_staticText217;
			wxStaticText* m_staticText218;
			wxStaticText* m_staticText219;
			wxStaticText* m_staticText2621;
			wxStaticText* m_staticText267;
			wxTextCtrl* m_pOrientationVecXTxt_r;
			wxStaticText* m_staticText220;
			wxTextCtrl* m_pOrientationVecYTxt_r;
			wxStaticText* m_staticText221;
			wxTextCtrl* m_pOrientationVecZTxt_r;
			wxStaticText* m_staticText236;
			wxStaticText* m_staticText237;
			wxTextCtrl* m_pDirVecXTxt_r;
			wxStaticText* m_staticText238;
			wxTextCtrl* m_pDirVecYTxt_r;
			wxStaticText* m_staticText239;
			wxTextCtrl* m_pDirVecZTxt_r;
			wxStaticLine* m_staticline163;
			wxButton* m_button_Add_r;
			wxButton* m_button_Clear_r;
			wxButton* m_button_AllClear_r;
			wxStaticLine* m_staticline173;
			wxPanel* m_panel621;
			wxStaticText* m_staticText18613;
			wxGrid* m_pRectangularGrid_r;
			wxPanel* m_panel4131;
			wxPanel* m_panel4411;
			wxStaticText* m_staticText18621;
			wxStaticText* m_staticText18721;
			wxStaticText* m_staticText20211;
			wxStaticText* m_staticText18821;
			wxStaticText* m_staticText20111;
			wxStaticText* m_staticText18921;
			wxStaticText* m_staticText19021;
			wxStaticText* m_staticText16121;
			wxBitmapButton* m_pXPosDownBtn_c;
			wxSlider* m_pXSlider_c;
			wxBitmapButton* m_pXPosUpBtn_c;
			wxTextCtrl* m_pXPosTxt_c;
			wxTextCtrl* m_pXPitchTxt_c;
			wxStaticText* m_staticText16221;
			wxBitmapButton* m_pYPosDownBtn_c;
			wxSlider* m_pYSlider_c;
			wxBitmapButton* m_pYPosUpBtn_c;
			wxTextCtrl* m_pYPosTxt_c;
			wxTextCtrl* m_pYPitchTxt_c;
			wxStaticText* m_staticText16321;
			wxBitmapButton* m_pZPosDownBtn_c;
			wxSlider* m_pZSlider_c;
			wxBitmapButton* m_pZPosUpBtn_c;
			wxTextCtrl* m_pZPosTxt_c;
			wxTextCtrl* m_pZPitchTxt_c;
			wxPanel* m_panel42132;
			wxStaticText* m_staticText16632;
			wxStaticText* m_staticText16732;
			wxTextCtrl* m_pDepthTxt_c;
			wxStaticText* m_staticText222;
			wxStaticText* m_staticText223;
			wxStaticText* m_staticText224;
			wxStaticText* m_staticText225;
			wxStaticText* m_staticText2601;
			wxStaticText* m_staticText2611;
			wxTextCtrl* m_pFanRadiusTxt_c;
			wxStaticText* m_staticText226;
			wxStaticText* m_staticText227;
			wxStaticText* m_staticText228;
			wxStaticText* m_staticText229;
			wxStaticText* m_staticText2622;
			wxStaticText* m_staticText2631;
			wxTextCtrl* m_pBossRadTxt_c;
			wxStaticText* m_staticText230;
			wxStaticText* m_staticText231;
			wxStaticText* m_staticText232;
			wxStaticText* m_staticText233;
			wxStaticText* m_staticText26211;
			wxStaticText* m_staticText2671;
			wxTextCtrl* m_pOrientationVecXTxt_c;
			wxStaticText* m_staticText234;
			wxTextCtrl* m_pOrientationVecYTxt_c;
			wxStaticText* m_staticText235;
			wxTextCtrl* m_pOrientationVecZTxt_c;
			wxStaticLine* m_staticline1631;
			wxButton* m_button_Add_c;
			wxButton* m_button_Clear_c;
			wxButton* m_button_AllClear_c;
			wxStaticLine* m_staticline1731;
			wxPanel* m_panel6211;
			wxStaticText* m_staticText186131;
			wxGrid* m_pCylinderGrid_c;
			wxPanel* m_panel49;
			wxButton* m_pCloseBtn;
			
			// Virtual event handlers, overide them in your derived class
			virtual void OnActivate( wxActivateEvent& event ) { event.Skip(); }
			virtual void OnActivateApp( wxActivateEvent& event ) { event.Skip(); }
			virtual void OnClose( wxCloseEvent& event ) { event.Skip(); }
			virtual void m_notebookOnNotebookPageChanged( wxNotebookEvent& event ) { event.Skip(); }
			virtual void m_notebookOnNotebookPageChanging( wxNotebookEvent& event ) { event.Skip(); }
			virtual void OnXPosDown_p( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnXSlideScroll_p( wxScrollEvent& event ) { event.Skip(); }
			virtual void OnXSlideRelease_p( wxScrollEvent& event ) { event.Skip(); }
			virtual void OnXSlideDrag_p( wxScrollEvent& event ) { event.Skip(); }
			virtual void OnXPosUp_p( wxCommandEvent& event ) { event.Skip(); }
			virtual void m_pXPosTxt_pOnKillFocus( wxFocusEvent& event ) { event.Skip(); }
			virtual void OnPosTxt_p( wxCommandEvent& event ) { event.Skip(); }
			virtual void m_pXPitchTxt_pOnKillFocus( wxFocusEvent& event ) { event.Skip(); }
			virtual void OnPitchTxt_p( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnYPosDown_p( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnYSlideScroll_p( wxScrollEvent& event ) { event.Skip(); }
			virtual void OnYSlideRelease_p( wxScrollEvent& event ) { event.Skip(); }
			virtual void OnYSlideDrag_p( wxScrollEvent& event ) { event.Skip(); }
			virtual void OnYPosUp_p( wxCommandEvent& event ) { event.Skip(); }
			virtual void m_pYPosTxt_pOnKillFocus( wxFocusEvent& event ) { event.Skip(); }
			virtual void m_pYPitchTxt_pOnKillFocus( wxFocusEvent& event ) { event.Skip(); }
			virtual void OnZPosDown_p( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnZSlideScroll_p( wxScrollEvent& event ) { event.Skip(); }
			virtual void OnZSlideRelease_p( wxScrollEvent& event ) { event.Skip(); }
			virtual void OnZSlideDrag_p( wxScrollEvent& event ) { event.Skip(); }
			virtual void OnZPosUp_p( wxCommandEvent& event ) { event.Skip(); }
			virtual void m_pZPosTxt_pOnKillFocus( wxFocusEvent& event ) { event.Skip(); }
			virtual void m_pZPitchTxt_pOnKillFocus( wxFocusEvent& event ) { event.Skip(); }
			virtual void m_pSizeTxt_pOnKillFocus( wxFocusEvent& event ) { event.Skip(); }
			virtual void OnSizeTxt_p( wxCommandEvent& event ) { event.Skip(); }
			virtual void m_button_list_AddButtonClick_p( wxCommandEvent& event ) { event.Skip(); }
			virtual void m_button_list_ClearButtonClick_p( wxCommandEvent& event ) { event.Skip(); }
			virtual void m_button_list_AllClearButtonClick_p( wxCommandEvent& event ) { event.Skip(); }
			virtual void m_pPointGrid_pOnGridSelectCell( wxGridEvent& event ) { event.Skip(); }
			virtual void OnXPosDown_r( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnXSlideScroll_r( wxScrollEvent& event ) { event.Skip(); }
			virtual void OnXSlideRelease_r( wxScrollEvent& event ) { event.Skip(); }
			virtual void OnXSlideDrag_r( wxScrollEvent& event ) { event.Skip(); }
			virtual void OnXPosUp_r( wxCommandEvent& event ) { event.Skip(); }
			virtual void m_pXPosTxt_rOnKillFocus( wxFocusEvent& event ) { event.Skip(); }
			virtual void OnPosTxt_r( wxCommandEvent& event ) { event.Skip(); }
			virtual void m_pXPitchTxt_rOnKillFocus( wxFocusEvent& event ) { event.Skip(); }
			virtual void OnPitchTxt_r( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnYPosDown_r( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnYSlideScroll_r( wxScrollEvent& event ) { event.Skip(); }
			virtual void OnYSlideRelease_r( wxScrollEvent& event ) { event.Skip(); }
			virtual void OnYSlideDrag_r( wxScrollEvent& event ) { event.Skip(); }
			virtual void OnYPosUp_r( wxCommandEvent& event ) { event.Skip(); }
			virtual void m_pYPosTxt_rOnKillFocus( wxFocusEvent& event ) { event.Skip(); }
			virtual void m_pYPitchTxt_rOnKillFocus( wxFocusEvent& event ) { event.Skip(); }
			virtual void OnZPosDown_r( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnZSlideScroll_r( wxScrollEvent& event ) { event.Skip(); }
			virtual void OnZSlideRelease_r( wxScrollEvent& event ) { event.Skip(); }
			virtual void OnZSlideDrag_r( wxScrollEvent& event ) { event.Skip(); }
			virtual void OnZPosUp_r( wxCommandEvent& event ) { event.Skip(); }
			virtual void m_pZPosTxt_rOnKillFocus( wxFocusEvent& event ) { event.Skip(); }
			virtual void m_pZPitchTxt_rOnKillFocus( wxFocusEvent& event ) { event.Skip(); }
			virtual void m_pWidthTxt_rOnKillFocus( wxFocusEvent& event ) { event.Skip(); }
			virtual void OnWidthTxt_r( wxCommandEvent& event ) { event.Skip(); }
			virtual void m_pDepthTxt_rOnKillFocus( wxFocusEvent& event ) { event.Skip(); }
			virtual void OnDepthTxt_r( wxCommandEvent& event ) { event.Skip(); }
			virtual void m_pHeightTxt_rOnKillFocus( wxFocusEvent& event ) { event.Skip(); }
			virtual void OnHeightTxt_r( wxCommandEvent& event ) { event.Skip(); }
			virtual void m_pOrientationVecXTxt_rOnKillFocus( wxFocusEvent& event ) { event.Skip(); }
			virtual void OnOrientationVectorXTxt_r( wxCommandEvent& event ) { event.Skip(); }
			virtual void m_pOrientationVecYTxt_rOnKillFocus( wxFocusEvent& event ) { event.Skip(); }
			virtual void OnOrientationVectorYTxt_r( wxCommandEvent& event ) { event.Skip(); }
			virtual void m_pOrientationVecZTxt_rOnKillFocus( wxFocusEvent& event ) { event.Skip(); }
			virtual void OnOrientationVectorZTxt_r( wxCommandEvent& event ) { event.Skip(); }
			virtual void m_pDirVecXTxt_rOnKillFocus( wxFocusEvent& event ) { event.Skip(); }
			virtual void OnDirVectorXTxt_r( wxCommandEvent& event ) { event.Skip(); }
			virtual void m_pDirVecYTxt_rOnKillFocus( wxFocusEvent& event ) { event.Skip(); }
			virtual void OnDirVectorYTxt_r( wxCommandEvent& event ) { event.Skip(); }
			virtual void m_pDirVecZTxt_rOnKillFocus( wxFocusEvent& event ) { event.Skip(); }
			virtual void OnDirVectorZTxt_r( wxCommandEvent& event ) { event.Skip(); }
			virtual void m_button_list_AddButtonClick_r( wxCommandEvent& event ) { event.Skip(); }
			virtual void m_button_list_ClearButtonClick_r( wxCommandEvent& event ) { event.Skip(); }
			virtual void m_button_list_AllClearButtonClick_r( wxCommandEvent& event ) { event.Skip(); }
			virtual void m_pRectangularGrid_rOnGridSelectCell( wxGridEvent& event ) { event.Skip(); }
			virtual void OnXPosDown_c( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnXSlideScroll_c( wxScrollEvent& event ) { event.Skip(); }
			virtual void OnXSlideRelease_c( wxScrollEvent& event ) { event.Skip(); }
			virtual void OnXSlideDrag_c( wxScrollEvent& event ) { event.Skip(); }
			virtual void OnXPosUp_c( wxCommandEvent& event ) { event.Skip(); }
			virtual void m_pXPosTxt_cOnKillFocus( wxFocusEvent& event ) { event.Skip(); }
			virtual void OnPosTxt_c( wxCommandEvent& event ) { event.Skip(); }
			virtual void m_pXPitchTxt_cOnKillFocus( wxFocusEvent& event ) { event.Skip(); }
			virtual void OnPitchTxt_c( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnYPosDown_c( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnYSlideScroll_c( wxScrollEvent& event ) { event.Skip(); }
			virtual void OnYSlideRelease_c( wxScrollEvent& event ) { event.Skip(); }
			virtual void OnYSlideDrag_c( wxScrollEvent& event ) { event.Skip(); }
			virtual void OnYPosUp_c( wxCommandEvent& event ) { event.Skip(); }
			virtual void m_pYPosTxt_cOnKillFocus( wxFocusEvent& event ) { event.Skip(); }
			virtual void m_pYPitchTxt_cOnKillFocus( wxFocusEvent& event ) { event.Skip(); }
			virtual void OnZPosDown_c( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnZSlideScroll_c( wxScrollEvent& event ) { event.Skip(); }
			virtual void OnZSlideRelease_c( wxScrollEvent& event ) { event.Skip(); }
			virtual void OnZSlideDrag_c( wxScrollEvent& event ) { event.Skip(); }
			virtual void OnZPosUp_c( wxCommandEvent& event ) { event.Skip(); }
			virtual void m_pZPosTxt_cOnKillFocus( wxFocusEvent& event ) { event.Skip(); }
			virtual void m_pZPitchTxt_cOnKillFocus( wxFocusEvent& event ) { event.Skip(); }
			virtual void m_pDepthTxt_cOnKillFocus( wxFocusEvent& event ) { event.Skip(); }
			virtual void OnDepthTxt_c( wxCommandEvent& event ) { event.Skip(); }
			virtual void m_pFanRadiusTxt_cOnKillFocus( wxFocusEvent& event ) { event.Skip(); }
			virtual void OnFanRadiusTxt_c( wxCommandEvent& event ) { event.Skip(); }
			virtual void m_pBossRadTxt_cOnKillFocus( wxFocusEvent& event ) { event.Skip(); }
			virtual void OnBossRadiusTxt_c( wxCommandEvent& event ) { event.Skip(); }
			virtual void m_pOrientationVecXTxt_cOnKillFocus( wxFocusEvent& event ) { event.Skip(); }
			virtual void OnOrientationVectorXTxt_c( wxCommandEvent& event ) { event.Skip(); }
			virtual void m_pOrientationVecYTxt_cOnKillFocus( wxFocusEvent& event ) { event.Skip(); }
			virtual void OnOrientationVectorYTxt_c( wxCommandEvent& event ) { event.Skip(); }
			virtual void m_pOrientationVecZTxt_cOnKillFocus( wxFocusEvent& event ) { event.Skip(); }
			virtual void OnOrientationVectorZTxt_c( wxCommandEvent& event ) { event.Skip(); }
			virtual void m_button_list_AddButtonClick_c( wxCommandEvent& event ) { event.Skip(); }
			virtual void m_button_list_ClearButtonClick_c( wxCommandEvent& event ) { event.Skip(); }
			virtual void m_button_list_AllClearButtonClick_c( wxCommandEvent& event ) { event.Skip(); }
			virtual void m_pCylinderGrid_cOnGridSelectCell( wxGridEvent& event ) { event.Skip(); }
			virtual void m_pCloseBtnOnButtonClick( wxCommandEvent& event ) { event.Skip(); }
			
		
		public:
			
			CoordinateViewerFrame( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxEmptyString, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 445,575 ), long style = wxDEFAULT_FRAME_STYLE|wxFRAME_FLOAT_ON_PARENT|wxTAB_TRAVERSAL );
			~CoordinateViewerFrame();
		
	};
	
	///////////////////////////////////////////////////////////////////////////////
	/// Class FreeRotateDlgBase
	///////////////////////////////////////////////////////////////////////////////
	class FreeRotateDlgBase : public wxDialog 
	{
		private:
		
		protected:
			wxStaticText* m_staticText302;
			wxPanel* m_panel41;
			wxStaticText* m_staticText252;
			wxTextCtrl* m_rotateDegreeTxt;
			wxButton* m_pFreeRotateBtn;
			wxStaticText* m_staticText301;
			wxPanel* m_panel411;
			
			wxButton* m_pPlus90RotateBtn;
			wxButton* m_pMinus90RotateBtn;
			
			wxButton* m_pCloseBtn1;
			
			// Virtual event handlers, overide them in your derived class
			virtual void FreerotateDlgBaseOnClose( wxCloseEvent& event ) { event.Skip(); }
			virtual void OnDvdManuTxtOnKillFocus( wxFocusEvent& event ) { event.Skip(); }
			virtual void OnDvdManuTxt( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnFreeRotateBtn( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnPlus90RotateBCBtn( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnMinus90RotateBtn( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnCloseBtn( wxCommandEvent& event ) { event.Skip(); }
			
		
		public:
			
			FreeRotateDlgBase( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("Rotate"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( -1,-1 ), long style = wxCAPTION|wxCLOSE_BOX|wxSTAY_ON_TOP );
			~FreeRotateDlgBase();
		
	};
	
	///////////////////////////////////////////////////////////////////////////////
	/// Class ExportSTLDlgBase
	///////////////////////////////////////////////////////////////////////////////
	class ExportSTLDlgBase : public wxDialog 
	{
		private:
		
		protected:
			wxStaticText* m_staticText302;
			wxPanel* m_panel411;
			wxRadioButton* m_exportSTLTypeAsciiRadioBtn;
			wxStaticText* m_staticText1;
			wxRadioButton* m_exportSTLTypeBinaryRadioBtn;
			wxStaticText* m_staticText2;
			wxStaticText* m_staticText3021;
			wxPanel* m_panel41;
			wxRadioButton* m_exportTypeAllRadioBtn;
			wxStaticText* m_staticText3;
			wxRadioButton* m_exportTypeOnlyRadioBtn;
			wxStaticText* m_staticText4;
			wxStaticText* m_staticText3022;
			wxPanel* m_panel412;
			wxRadioButton* m_modelScaleRadioBtn1;
			wxStaticText* m_staticText5;
			wxRadioButton* m_modelScaleRadioBtn2;
			wxStaticText* m_staticText6;
			wxTextCtrl* m_modelScaleTextCtrl;
			wxRadioButton* m_modelScaleRadioBtn3;
			wxStaticText* m_staticText7;
			wxRadioButton* m_modelScaleRadioBtn4;
			wxStaticText* m_staticText8;
			wxRadioButton* m_modelScaleRadioBtn5;
			wxStaticText* m_staticText9;
			wxRadioButton* m_modelScaleRadioBtn6;
			wxStaticText* m_staticText10;
			wxRadioButton* m_modelScaleRadioBtn7;
			wxStaticText* m_staticText11;
			wxRadioButton* m_modelScaleRadioBtn8;
			wxStaticText* m_staticText12;
			
			wxButton* m_pNextBtn;
			wxButton* m_pCloseBtn;
			
			// Virtual event handlers, overide them in your derived class
			virtual void ExportSTLDlgBaseOnClose( wxCloseEvent& event ) { event.Skip(); }
			virtual void OnExportModelScaleRadio( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnDvdManuTxtOnKillFocus( wxFocusEvent& event ) { event.Skip(); }
			virtual void OnDvdManuTxt( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnNextBtn( wxCommandEvent& event ) { event.Skip(); }
			virtual void OnCancelBtn( wxCommandEvent& event ) { event.Skip(); }
			
		
		public:
			
			ExportSTLDlgBase( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("Export STL"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( -1,-1 ), long style = wxCAPTION|wxCLOSE_BOX );
			~ExportSTLDlgBase();
		
	};
	
} // namespace UI

#endif //__DialogBase__
