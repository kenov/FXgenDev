#ifndef __fxgenGUIBCMediumManagerDlgBase__
#define __fxgenGUIBCMediumManagerDlgBase__

/**
@file
Subclass of BCMediumManagerDlgBase, which is generated by wxFormBuilder.
*/

#include "DialogBase.h"

//// end generated include
#include <vector>
#include <string>
#include "DialogUtil.h"
#include "BCMedFunc.h"

/** Implementing BCMediumManagerDlgBase */
class fxgenGUIBCMediumManagerDlgBase : public UI::BCMediumManagerDlgBase
{
	protected:
		// Handlers for BCMediumManagerDlgBase events.
		void OnChangeMedValue( wxGridEvent& event );
		void OnClickMedValue( wxGridEvent& event );
		void OnNewMedBtn( wxCommandEvent& event );
		void OnDelMedBtn( wxCommandEvent& event );
		void OnChangeOuterBCValue( wxGridEvent& event );
		void OnClickOuterBCValue( wxGridEvent& event );
		void OnNewOuterBCBtn( wxCommandEvent& event );
		void OnDelOuterBCBtn( wxCommandEvent& event );
		void OnChangeLocalBCValue( wxGridEvent& event );
		void OnClickLocalBCValue( wxGridEvent& event );
		void OnNewLocalBCBtn( wxCommandEvent& event );
		void OnDelLocalBCBtn( wxCommandEvent& event );
		void OnOKBtn( wxCommandEvent& event );
		void OnCancelBtn( wxCommandEvent& event );
	
	
		bool checkValues();
	public:
		/** Constructor */
	fxgenGUIBCMediumManagerDlgBase( wxWindow* parent, UI::BCMedFunc& func);
	//// end generated class members
	
	protected:
		std::vector<wxString> m_outerBCClassListStr;
		std::vector<wxString> m_localBCClassListStr;
		UI::BCMedFunc& m_func;
	
		void AddOuterBC(const wxString& alias, const wxString& classname);
		void AddLocalBC(const wxString& alias, const wxString& classname, float col[4], unsigned int id);
		void AddMedium (const wxString& label, float col[4], unsigned int id, const wxString& medType);
		int GetFreeId() const;
};

#endif // __fxgenGUIBCMediumManagerDlgBase__
