/*
 *
 *
 *
 */

#ifndef _VX_SLICE_CONTROLLER_DLG_H_
#define _VX_SLICE_CONTROLLER_DLG_H_

#include "DialogBase.h"
#include "../VX/Log.h"
#include "../VX/Type.h"
#include "../VX/Math.h"

class SliceController;
namespace UI
{
	class SliceCtrlDlg : public SliceCtrlDlgBase
	{
	public:
		SliceCtrlDlg(wxWindow *parent);

		void SetController(SliceController* controller);
		void OnXSlideDrag( wxScrollEvent& event );
		void OnXSlideScroll( wxScrollEvent& event );
		void OnYSlideDrag( wxScrollEvent& event );
		void OnYSlideScroll( wxScrollEvent& event );
		void OnZSlideDrag( wxScrollEvent& event );
		void OnZSlideScroll( wxScrollEvent& event );
		void OnPosTxt( wxCommandEvent& event );
		void OnPitchTxt( wxCommandEvent& event );
		void OnXPosUp( wxCommandEvent& event );
		void OnXPosDown( wxCommandEvent& event );
		void OnYPosUp( wxCommandEvent& event );
		void OnYPosDown( wxCommandEvent& event );
		void OnZPosUp( wxCommandEvent& event );
		void OnZPosDown( wxCommandEvent& event );
		void OnRadioButton( wxCommandEvent& event );
		void OnColorMapMinLevelChoice( wxCommandEvent& event );
		void OnColorMapMaxLevelChoice( wxCommandEvent& event );

		void OnCloseBtn( wxCommandEvent& event )
		{
			Show(false);
		}

		/// 現在のコントローラで情報を再更新する
		void UpdateControllerView();

	private:
		// m_positionの値からスライドバーの位置を設定
		void SetSlidePos(u32 axis);
		// m_positionの値からポジションテキストを設定
		void SetPosTxt(u32 axis, u32 unit);
		// Posテキストからm_positionのを設定
		void SetPosValue(u32 axis, u32 unit);
		// Pitchテキストからm_pitchのを設定
		void SetPitchValue(u32 axis, u32 unit);
		// Pitchのテキストを設定
		void SetPitchTxt(u32 newunit);
		// 入力されたposition値を範囲内に収めた値に切り詰めて返す
		f32 GetLimitedPos(f32 pos, u32 axis);

		// セル幅(座標値)を取得
		f32 GetCoordPitch(const u32 axis) const;

		// 座標値で持っているpitchの値をIndex値に変更
		u32 ConvertPitchCoord2Idx(f32 pitch, u32 axis);
		// Index値で持っているpitchの値を座標値に変更
		f32 ConvertPitchIdx2Coord(u32 pitch, u32 axis);
		// 座標値で持っているPositionの値をIndex値に変更
		u32 ConvertPosCoord2Idx(f32 pos, u32 axis);
		// Index値で持っているPositionの値を座標値に変更
		f32 ConvertPosIdx2Coord(u32 pos, u32 axis);

	private:
		SliceController* m_ctrl;
		VX::Math::vec3 m_bbox[2];
		VX::Math::vec3 m_slidePitch;

		VX::Math::vec3 m_position;  // スライスコントロールの位置(座標値)
		VX::Math::vec3 m_spinPitch; // スピンボタンを押したときのスライスコントロールの増減値(座標値) 
	};

} // namespace UI
#endif // _VX_SLICE_CONTROLLER_DLG_H_
