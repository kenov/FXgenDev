
#include "DialogUtil.h"
#include "DomainCartDlg.h"

#include "../VX/Math.h"
#include "../Core/DomainCartesianEditor.h"
#include "../Core/fxUI/icons.h"

#include "vxWindow.h"

#define ICON_CONTACT_EXPAND_WIDTH  16
#define ICON_CONTACT_EXPAND_HEIGHT 16

namespace UI
{
	DomainCartDlg::DomainCartDlg(wxWindow *parent)
	: DomainCartDlgBase(parent),
	  m_editor(NULL),
	  m_oldSubdomain(NULL)
	{
        m_voxExpand = false;
        m_dvdExpand = false;
		//Set contact-expand image for BMP button
		const int conTgaX = 284, conTgaY =  0;
		const int expTgaX = 284, expTgaY = 16;
		const int iconW = ICON_CONTACT_EXPAND_WIDTH;
		const int iconH = ICON_CONTACT_EXPAND_HEIGHT;
		int tgaW, tgaH;

		unsigned char conBGRA[ICON_CONTACT_EXPAND_WIDTH * ICON_CONTACT_EXPAND_HEIGHT * 4];
		unsigned char expBGRA[ICON_CONTACT_EXPAND_WIDTH * ICON_CONTACT_EXPAND_HEIGHT * 4];
		const unsigned char* buf;
		if (fxGUI::TGALoader(icons, tgaW, tgaH, &buf)) {
			for (int y = 0; y < iconH; y++) {
				memcpy(conBGRA + iconW * y * 4, buf + (tgaW * (y + conTgaY) + conTgaX) * 4, iconW * 4);
				memcpy(expBGRA + iconW * y * 4, buf + (tgaW * (y + expTgaY) + expTgaX) * 4, iconW * 4);
			}
		}

		unsigned char* conRGB = new unsigned char[ICON_CONTACT_EXPAND_WIDTH * ICON_CONTACT_EXPAND_HEIGHT * 3];
		unsigned char* expRGB = new unsigned char[ICON_CONTACT_EXPAND_WIDTH * ICON_CONTACT_EXPAND_HEIGHT * 3];
		for (int i = 0; i < iconW; i++) { 
			for (int j = 0; j < iconH; j++) { 
				conRGB[(i + iconW * j) * 3    ] = conBGRA[(i + iconW * j) * 4 + 2];
				conRGB[(i + iconW * j) * 3 + 1] = conBGRA[(i + iconW * j) * 4 + 1];
				conRGB[(i + iconW * j) * 3 + 2] = conBGRA[(i + iconW * j) * 4    ];

				expRGB[(i + iconW * j) * 3    ] = expBGRA[(i + iconW * j) * 4 + 2];
				expRGB[(i + iconW * j) * 3 + 1] = expBGRA[(i + iconW * j) * 4 + 1];
				expRGB[(i + iconW * j) * 3 + 2] = expBGRA[(i + iconW * j) * 4    ];
			}
		}
		wxImage conImg(iconW, iconH, conRGB, false);
		wxImage expImg(iconW, iconH, expRGB, false);

		m_conBmp = wxBitmap(conImg);
		m_expBmp = wxBitmap(expImg);

	}	

	DomainCartDlg::~DomainCartDlg()
	{
		if(m_oldSubdomain){
			delete [] m_oldSubdomain;
		}
		m_oldSubdomain = NULL;
	}

	void DomainCartDlg::SetEditor(DomainCartesianEditor *editor){
		m_editor = editor;
	}

	bool DomainCartDlg::Show(bool show)
	{
		if(show){

			// Backup Old Parameters for Cancel
			m_editor->GetBBoxMinMax(m_old_bboxMin, m_old_bboxMax);
			m_editor->GetVoxelCount(m_old_vox);
			m_editor->GetDivCount(m_old_div);
			m_old_expPolicy = m_editor->GetExportPolicy();
			if(m_oldSubdomain){ 
				delete [] m_oldSubdomain;
			}
			m_oldSubdomain = NULL;
			if(m_editor->GetSubdomainPtr()){
				size_t sz = m_old_div[0] * m_old_div[1] * m_old_div[2];
				m_oldSubdomain = new u8[sz];
				memcpy(m_oldSubdomain, m_editor->GetSubdomainPtr(), sizeof(u8) * sz);
			}

			// Init UserDefine BBox
			m_userDefBBoxMin = m_old_bboxMin;
			m_userDefBBoxMax = m_old_bboxMax;

			Init();
		}
		else{
			m_editor = NULL;
			delete [] m_oldSubdomain;
			m_oldSubdomain = NULL;
		}

		return DomainCartDlgBase::Show(show);
	}


	void DomainCartDlg::InitVoxDivAppear()
	{
		if(m_voxExpand){
			m_pVoxBtn->SetBitmapLabel(m_expBmp);
			m_pVoxBoxSizer->Show(true);
			m_pUnifChk->Enable();
			m_pPitchXTxt->Enable();
			m_pPitchYTxt->Enable();
			m_pPitchZTxt->Enable();
			m_pVoxXTxt->Enable();
			m_pVoxYTxt->Enable();
			m_pVoxZTxt->Enable();
		}else{
			m_pVoxBtn->SetBitmapLabel(m_conBmp);
			m_pVoxBoxSizer->Show(false);
			m_pUnifChk->Disable();
			m_pPitchXTxt->Disable();
			m_pPitchYTxt->Disable();
			m_pPitchZTxt->Disable();
			m_pVoxXTxt->Disable();
			m_pVoxYTxt->Disable();
			m_pVoxZTxt->Disable();
		}

		if(m_dvdExpand){
			m_pDvdBtn->SetBitmapLabel(m_expBmp);
			m_pDvdBoxSizer->Show(true);
			m_pDvdXTxt->Enable();
			m_pDvdYTxt->Enable();
			m_pDvdZTxt->Enable();
			m_pDvdAutoTxt->Enable();
		}else{
			m_pDvdBtn->SetBitmapLabel(m_conBmp);
			m_pDvdBoxSizer->Show(false);
			m_pDvdXTxt->Disable();
			m_pDvdYTxt->Disable();
			m_pDvdZTxt->Disable();
			m_pDvdAutoTxt->Disable();
		}

		this->Fit();
	}

	void DomainCartDlg::Init(){

		m_pPolicySizeRBtn->SetValue(TRUE);
		m_pPolicyMaxRBtn->SetValue(FALSE);
		SetBBoxPolicy();

		m_pExpPolicyPitchBtn->SetValue(TRUE);
		m_pExpPolicyVoxBtn->SetValue(FALSE);

		m_pUnifChk->SetValue(FALSE);
		m_pVoxXTxt->Enable();
		m_pVoxYTxt->Enable();
		m_pVoxZTxt->Enable();
	
		if(m_editor->GetExportPolicy() == 0){
			m_pExpPolicyPitchBtn->SetValue(TRUE);
			m_pExpPolicyVoxBtn->SetValue(FALSE);
		}else{
			m_pExpPolicyPitchBtn->SetValue(FALSE);
			m_pExpPolicyVoxBtn->SetValue(TRUE);
		}

		InitVoxDivAppear();

		const std::vector<std::string> &unitList = m_editor->GetUnitList();
		m_pUnitChoice->Clear();
		for(int i = 0; i < unitList.size(); i++){
			m_pUnitChoice->Append(unitList[i]);
		}

		UpdateDlg();
		UI::vxWindow::GetTopWindow()->Draw();
	}

	void DomainCartDlg::UpdateDlg(){

		char txt[64];
		// BBox
		VX::Math::vec3 bboxMin, bboxMax, bboxSize;
		m_editor->GetBBoxMinMaxSize(bboxMin, bboxMax, bboxSize);

		sprintf(txt, "%f", bboxMin[0]);  m_pMinXTxt->SetValue(txt);
		sprintf(txt, "%f", bboxMin[1]);  m_pMinYTxt->SetValue(txt);
		sprintf(txt, "%f", bboxMin[2]);  m_pMinZTxt->SetValue(txt);
		sprintf(txt, "%f", bboxMax[0]);  m_pMaxXTxt->SetValue(txt);
		sprintf(txt, "%f", bboxMax[1]);  m_pMaxYTxt->SetValue(txt);
		sprintf(txt, "%f", bboxMax[2]);  m_pMaxZTxt->SetValue(txt);
		sprintf(txt, "%f", bboxSize[0]); m_pSizeXTxt->SetValue(txt);
		sprintf(txt, "%f", bboxSize[1]); m_pSizeYTxt->SetValue(txt);
		sprintf(txt, "%f", bboxSize[2]); m_pSizeZTxt->SetValue(txt);
		
		// Pitch / Voxel Count
		VX::Math::vec3 pitch;
		VX::Math::idx3 vox;
		m_editor->GetVoxelPitch(pitch);
		m_editor->GetVoxelCount(vox);

		sprintf(txt, "%f", pitch[0]); m_pPitchXTxt->SetValue(txt);
		sprintf(txt, "%f", pitch[1]); m_pPitchYTxt->SetValue(txt);
		sprintf(txt, "%f", pitch[2]); m_pPitchZTxt->SetValue(txt);

		sprintf(txt, "%d", vox[0]); m_pVoxXTxt->SetValue(txt);
		sprintf(txt, "%d", vox[1]); m_pVoxYTxt->SetValue(txt);
		sprintf(txt, "%d", vox[2]); m_pVoxZTxt->SetValue(txt);

		// Parallel Divide
		VX::Math::idx3 div;
		m_editor->GetDivCount(div);
		sprintf(txt, "%d", div[0]); m_pDvdXTxt->SetValue(txt);
		sprintf(txt, "%d", div[1]); m_pDvdYTxt->SetValue(txt);
		sprintf(txt, "%d", div[2]); m_pDvdZTxt->SetValue(txt);

		u64 numDivs = static_cast<u64>(div[0]) * static_cast<u64>(div[1]) * static_cast<u64>(div[2]);
		sprintf(txt, "%llu", numDivs); m_pDvdAutoTxt->SetValue(txt);

		const std::string &currentUnit = m_editor->GetUnit();
		int index = m_pUnitChoice->FindString(currentUnit);
		if( index != -1 ){
			m_pUnitChoice->Select(index);
		}else{
			m_pUnitChoice->Select(0);
		}
	}

	u8 DomainCartDlg::GetCenteringFlag()
	{
		u8 flag = 0;
		if( m_pCenterXChk->GetValue() ){ flag += (1 << 0); }
		if( m_pCenterYChk->GetValue() ){ flag += (1 << 1); }
		if( m_pCenterZChk->GetValue() ){ flag += (1 << 2); }

		return flag;
	}

	void DomainCartDlg::SetBBoxPolicy()
	{

		m_pMinXTxt->Enable();
		m_pMinYTxt->Enable();
		m_pMinZTxt->Enable();

		if( m_pPolicySizeRBtn->GetValue() ){
			m_pPolicyMaxRBtn->SetValue(FALSE);
			m_pMaxXTxt->Disable();
			m_pMaxYTxt->Disable();
			m_pMaxZTxt->Disable();
			m_pSizeXTxt->Enable();
			m_pSizeYTxt->Enable();
			m_pSizeZTxt->Enable();
		}else{
			m_pPolicySizeRBtn->SetValue(FALSE);
			m_pMaxXTxt->Enable();
			m_pMaxYTxt->Enable();
			m_pMaxZTxt->Enable();
			m_pSizeXTxt->Disable();
			m_pSizeYTxt->Disable();
			m_pSizeZTxt->Disable();
		}

		u8 axisFlag = GetCenteringFlag();
		if( ((axisFlag >> 0) & 1 ) == 1){ 
			m_pMinXTxt->Disable(); 
			m_pMaxXTxt->Disable();
			m_pSizeXTxt->Enable();
		}
		if( ((axisFlag >> 1) & 1 ) == 1){ 
			m_pMinYTxt->Disable(); 
			m_pMaxYTxt->Disable(); 
			m_pSizeYTxt->Enable();
		}
		if( ((axisFlag >> 2) & 1 ) == 1){ 
			m_pMinZTxt->Disable(); 
			m_pMaxZTxt->Disable(); 
			m_pSizeZTxt->Enable();
		}
	}

	void DomainCartDlg::OnExportPolicyRadio( wxCommandEvent& event )
	{
		if( m_pExpPolicyPitchBtn->GetValue() ){
			m_pExpPolicyVoxBtn->SetValue(FALSE);
			m_editor->SetExportPolicy(0);
		}else{
			m_pExpPolicyPitchBtn->SetValue(FALSE);
			m_editor->SetExportPolicy(1);
		}
	}

	void DomainCartDlg::OnCenterChk( wxCommandEvent& event )
	{
		SetBBoxPolicy();
		m_editor->Centering(GetCenteringFlag());
		UpdateDlg();
		UI::vxWindow::GetTopWindow()->Draw();
	}

	void DomainCartDlg::OnPolicyRadio( wxCommandEvent& event )
	{
		SetBBoxPolicy();
	}
	void DomainCartDlg::OnBBoxTxt( wxCommandEvent& event )
	{
		_OnBBoxTxt();
	}

	void DomainCartDlg::_OnBBoxTxt()
	{
		VX::Math::vec3 bbox_min;
		VX::Math::vec3 bbox_max;
		
		wxString vstr;
		vstr = m_pMinXTxt->GetValue(); bbox_min[0] = atof(vstr.c_str());
		vstr = m_pMinYTxt->GetValue(); bbox_min[1] = atof(vstr.c_str());
		vstr = m_pMinZTxt->GetValue(); bbox_min[2] = atof(vstr.c_str());

		if(m_pPolicyMaxRBtn->GetValue()){
			vstr = m_pMaxXTxt->GetValue(); bbox_max[0] = atof(vstr.c_str());
			vstr = m_pMaxYTxt->GetValue(); bbox_max[1] = atof(vstr.c_str());
			vstr = m_pMaxZTxt->GetValue(); bbox_max[2] = atof(vstr.c_str());

		}else{
			VX::Math::vec3 bbox_size;
			vstr = m_pSizeXTxt->GetValue(); bbox_size[0] = atof(vstr.c_str());
			vstr = m_pSizeYTxt->GetValue(); bbox_size[1] = atof(vstr.c_str());
			vstr = m_pSizeZTxt->GetValue(); bbox_size[2] = atof(vstr.c_str());

			bbox_max = bbox_min + bbox_size;
		}

		if( bbox_max[0] <= bbox_min[0] || bbox_max[1] <= bbox_min[1] ||
			bbox_max[2] <= bbox_min[2] )
		{
			UpdateDlg();
			UI::vxWindow::GetTopWindow()->Draw();
			return;
		}

		m_editor->SetBBoxMinMax(bbox_min, bbox_max);
		m_editor->Centering(GetCenteringFlag());

		// Set User Define BBox
		m_editor->GetBBoxMinMax(m_userDefBBoxMin, m_userDefBBoxMax);

		UpdateDlg();
		UI::vxWindow::GetTopWindow()->Draw();
	}
	
	void DomainCartDlg::OnForceUnifChk( wxCommandEvent& event )
	{
	}

	void DomainCartDlg::OnUnitChoice( wxCommandEvent& event )
	{
		assert(m_editor);

		wxString unit_str = m_pUnitChoice->GetString(m_pUnitChoice->GetSelection());

		m_editor->SetUnit(unit_str.ToStdString());
		UI::vxWindow::GetTopWindow()->Draw();
	}

	void DomainCartDlg::OnDefaultBtn( wxCommandEvent& event )
	{
		assert(m_editor);

		m_editor->SetDefault();
		UpdateDlg();
		UI::vxWindow::GetTopWindow()->Draw();
	}

	void DomainCartDlg::OnPitchTxt( wxCommandEvent& event )
	{
		_OnPitchTxt();
	}

	void DomainCartDlg::_OnPitchTxt()
	{
		// revert BBox to user define BBox
		m_editor->SetBBoxMinMax(m_userDefBBoxMin, m_userDefBBoxMax);

		VX::Math::vec3 oldPitch;
		m_editor->GetVoxelPitch(oldPitch);

		VX::Math::vec3 pitch;
		
		wxString vstr;
		vstr = m_pPitchXTxt->GetValue(); pitch[0] = atof(vstr.c_str());
		vstr = m_pPitchYTxt->GetValue(); pitch[1] = atof(vstr.c_str());
		vstr = m_pPitchZTxt->GetValue(); pitch[2] = atof(vstr.c_str());

		if( m_pUnifChk->GetValue() ){
			int idx = 0;
			for(int i = 0; i < 3; i++){
				if( fabs(oldPitch[i] - pitch[i]) > 1.0e-6 ){ // current pitch is not equal old pitch
					idx = i;
					break;
				}
			}
			for(int i = 0; i < 3; i++){
				pitch[i] = pitch[idx];
			}
		}

		m_editor->SetVoxelPitch(pitch);
		m_editor->Centering(GetCenteringFlag());

		UpdateDlg();
		UI::vxWindow::GetTopWindow()->Draw();
	}

	void DomainCartDlg::OnVoxTxt( wxCommandEvent& event )
	{
		_OnVoxTxt();
	}

	void DomainCartDlg::_OnVoxTxt(  )
	{
		// revert BBox to user define BBox
		m_editor->SetBBoxMinMax(m_userDefBBoxMin, m_userDefBBoxMax);

		VX::Math::vec3 oldPitch;
		VX::Math::vec3 oldBBoxMin, oldBBoxMax, oldBBoxSize;
		m_editor->GetVoxelPitch(oldPitch);
		m_editor->GetBBoxMinMaxSize(oldBBoxMin, oldBBoxMax, oldBBoxSize);
		
		VX::Math::idx3 vox;

		wxString vstr;
		vstr = m_pVoxXTxt->GetValue(); vox[0] = atoi(vstr.c_str());
		vstr = m_pVoxYTxt->GetValue(); vox[1] = atoi(vstr.c_str());
		vstr = m_pVoxZTxt->GetValue(); vox[2] = atoi(vstr.c_str());

		if( m_pUnifChk->GetValue() )
		{
			VX::Math::vec3 curPitch = VX::Math::vec3(oldBBoxSize.x / static_cast<f32>(vox[0]),
			                                         oldBBoxSize.y / static_cast<f32>(vox[1]),
													 oldBBoxSize.z / static_cast<f32>(vox[2]));
			int idx = 0;
			for(int i = 0; i < 3; i++){
				if( fabs(oldPitch[i] - curPitch[i]) > 1.0e-6 ){ // current Pitch is not equal to old pitch
					idx = i;
					break;
				}
			}
			for(int i = 0; i < 3; i++){
				curPitch[i] = curPitch[idx];
			}
			m_editor->SetVoxelPitch(curPitch);
			m_editor->Centering(GetCenteringFlag());
		}
		else
		{
			m_editor->SetVoxelCount(vox);
			m_editor->Centering(GetCenteringFlag());
		}

		UpdateDlg();
		UI::vxWindow::GetTopWindow()->Draw();
	}
	void DomainCartDlg::OnDvdManuTxt( wxCommandEvent& event )
	{
		_OnDvdManuTxt();
	}

	void DomainCartDlg::_OnDvdManuTxt( )
	{
		VX::Math::idx3 div;
		wxString vstr;
		vstr = m_pDvdXTxt->GetValue(); div[0] = atoi(vstr.c_str());
		vstr = m_pDvdYTxt->GetValue(); div[1] = atoi(vstr.c_str());
		vstr = m_pDvdZTxt->GetValue(); div[2] = atoi(vstr.c_str());

		m_editor->SetDivCount(div);

		UpdateDlg();
		UI::vxWindow::GetTopWindow()->Draw();
	}

	void DomainCartDlg::OnDvdAutoTxt( wxCommandEvent& event )
	{
		_OnDvdAutoTxt();
	}

	void DomainCartDlg::_OnDvdAutoTxt()
	{
		u32 div;
		wxString vstr;
		vstr = m_pDvdAutoTxt->GetValue(); div = atoi(vstr.c_str());

		m_editor->SetDivCount(div);

		UpdateDlg();
		UI::vxWindow::GetTopWindow()->Draw();
	}

	void DomainCartDlg::OnEnableVoxelBtn( wxCommandEvent& event )
	{
		if(m_voxExpand){
			m_editor->SetVoxelCount(VX::Math::idx3(1, 1, 1));
		}
		m_voxExpand = !m_voxExpand;
		InitVoxDivAppear();
		UpdateDlg();
		UI::vxWindow::GetTopWindow()->Draw();
	}

	void DomainCartDlg::OnEnableDvdBtn( wxCommandEvent& event )
	{
		if(m_dvdExpand){
			m_editor->SetDivCount(VX::Math::idx3(1, 1, 1));
		}
		m_dvdExpand = !m_dvdExpand;
		InitVoxDivAppear();
		UpdateDlg();
		UI::vxWindow::GetTopWindow()->Draw();
	}

	void DomainCartDlg::OnOKBtn( wxCommandEvent& event )
	{
		bool isAutoSettingBBox = false;
		bool isBBoxOK = true;

		VX::Math::vec3 curBBoxMin, curBBoxMax;
		m_editor->GetBBoxMinMax(curBBoxMin, curBBoxMax);

		for(int i = 0; i < 3; i++){
			if( (curBBoxMin[i] - m_userDefBBoxMin[i]) > 1.0e-6 ){ // current pitch is not equal old pitch
				isAutoSettingBBox = true;
				isBBoxOK          = false;
				break;
			}
			if( (curBBoxMax[i] - m_userDefBBoxMax[i]) > 1.0e-6 ){ // current pitch is not equal old pitch
				isAutoSettingBBox = true;
				isBBoxOK          = false;
				break;
			}
		}

		if(isAutoSettingBBox)
		{
			isBBoxOK = UI::ConfirmDlg("Region Parameters are changed automatically.\n"
				                      "Do you confirm these parameters?\n");
		}
		
		if(isBBoxOK)
		{
			DomainCartDlgBase::Show(false);
		}
	}

	void DomainCartDlg::OnCancelBtn( wxCommandEvent& event )
	{
		Reset();
		this->Show(false);
	}

	void DomainCartDlg::OnClose( wxCloseEvent& event )
	{
		Reset();
		this->Show(false);
	}

	void DomainCartDlg::Reset()
	{
		assert(m_editor);

		m_editor->SetUnit(m_old_unit);
		m_editor->SetBBoxMinMax(m_old_bboxMin, m_old_bboxMax);
		m_editor->SetVoxelCount(m_old_vox);
		m_editor->SetDivCount(m_old_div);

		UpdateDlg();

		if(m_oldSubdomain){
			m_editor->SetSubdomain(m_oldSubdomain);
		}
	}

	void DomainCartDlg::OnBBoxTxtOnKillFocus( wxFocusEvent& event )
	{
		_OnBBoxTxt();
		event.Skip();
	}

	void DomainCartDlg::OnPitchTxtOnKillFocus( wxFocusEvent& event )
	{
		_OnPitchTxt();
		event.Skip();
	}

	void DomainCartDlg::OnVoxTxtOnKillFocus( wxFocusEvent& event )
	{
		_OnVoxTxt();
		event.Skip();
	}

	void DomainCartDlg::OnDvdManuTxtOnKillFocus( wxFocusEvent& event )
	{
		_OnDvdManuTxt();
		event.Skip();
	}

	void DomainCartDlg::OnDvdAutoTxtOnKillFocus( wxFocusEvent& event )
	{
		_OnDvdAutoTxt();
		event.Skip();
	}
	
} // namespace UI

