///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Sep  8 2010)
// http://www.wxformbuilder.org/
//
// PLEASE DO "NOT" EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#include "DialogBase.h"

///////////////////////////////////////////////////////////////////////////
using namespace UI;

DomainCartDlgBase::DomainCartDlgBase( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );
	this->SetBackgroundColour( wxColour( 44, 44, 44 ) );
	
	m_pBaseSizer = new wxBoxSizer( wxVERTICAL );
	
	m_pTopSizer = new wxBoxSizer( wxVERTICAL );
	
	m_staticText31 = new wxStaticText( this, wxID_ANY, wxT("Region Setting"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText31->Wrap( -1 );
	m_staticText31->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), 70, 90, 92, false, wxEmptyString ) );
	m_staticText31->SetForegroundColour( wxColour( 209, 76, 16 ) );
	
	m_pTopSizer->Add( m_staticText31, 0, wxALL, 2 );
	
	m_panel2 = new wxPanel( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	m_panel2->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_panel2->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	wxBoxSizer* bSizer2;
	bSizer2 = new wxBoxSizer( wxVERTICAL );
	
	wxBoxSizer* bSizer3;
	bSizer3 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText2 = new wxStaticText( m_panel2, wxID_ANY, wxT("Region Setting Policy"), wxDefaultPosition, wxSize( -1,-1 ), 0 );
	m_staticText2->Wrap( -1 );
	m_staticText2->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	bSizer3->Add( m_staticText2, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );
	
	m_staticline132 = new wxStaticLine( m_panel2, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_VERTICAL );
	bSizer3->Add( m_staticline132, 0, wxEXPAND|wxALL, 5 );
	
	m_pPolicySizeRBtn = new wxRadioButton( m_panel2, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxRB_GROUP );
	bSizer3->Add( m_pPolicySizeRBtn, 0, wxALIGN_CENTER_VERTICAL|wxRIGHT|wxLEFT, 5 );
	
	m_staticText21 = new wxStaticText( m_panel2, wxID_ANY, wxT("Origin / Size"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText21->Wrap( -1 );
	m_staticText21->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	bSizer3->Add( m_staticText21, 0, wxALIGN_CENTER_VERTICAL|wxRIGHT, 5 );
	
	m_pPolicyMaxRBtn = new wxRadioButton( m_panel2, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizer3->Add( m_pPolicyMaxRBtn, 0, wxALIGN_CENTER_VERTICAL|wxRIGHT|wxLEFT, 5 );
	
	m_staticText211 = new wxStaticText( m_panel2, wxID_ANY, wxT("Origin / Max"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText211->Wrap( -1 );
	m_staticText211->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	bSizer3->Add( m_staticText211, 0, wxALIGN_CENTER_VERTICAL|wxRIGHT, 5 );
	
	bSizer2->Add( bSizer3, 0, wxEXPAND, 5 );
	
	wxFlexGridSizer* fgSizer1;
	fgSizer1 = new wxFlexGridSizer( 5, 4, 0, 0 );
	fgSizer1->SetFlexibleDirection( wxBOTH );
	fgSizer1->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );
	
	
	fgSizer1->Add( 0, 0, 1, wxEXPAND, 5 );
	
	m_staticText3 = new wxStaticText( m_panel2, wxID_ANY, wxT("X"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText3->Wrap( -1 );
	m_staticText3->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	fgSizer1->Add( m_staticText3, 0, wxALIGN_CENTER_HORIZONTAL|wxTOP|wxRIGHT|wxLEFT, 5 );
	
	m_staticText4 = new wxStaticText( m_panel2, wxID_ANY, wxT("Y"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText4->Wrap( -1 );
	m_staticText4->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	fgSizer1->Add( m_staticText4, 0, wxALIGN_CENTER_HORIZONTAL|wxTOP|wxRIGHT|wxLEFT, 5 );
	
	m_staticText41 = new wxStaticText( m_panel2, wxID_ANY, wxT("Z"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText41->Wrap( -1 );
	m_staticText41->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	fgSizer1->Add( m_staticText41, 0, wxALIGN_CENTER_HORIZONTAL|wxTOP|wxRIGHT|wxLEFT, 5 );
	
	m_staticText42 = new wxStaticText( m_panel2, wxID_ANY, wxT("Origin"), wxDefaultPosition, wxSize( 55,-1 ), 0 );
	m_staticText42->Wrap( -1 );
	m_staticText42->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	fgSizer1->Add( m_staticText42, 0, wxALIGN_CENTER_VERTICAL|wxALIGN_RIGHT|wxRIGHT|wxLEFT, 5 );
	
	m_pMinXTxt = new wxTextCtrl( m_panel2, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER|wxSIMPLE_BORDER );
	m_pMinXTxt->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pMinXTxt->SetBackgroundColour( wxColour( 0, 0, 0 ) );
	
	fgSizer1->Add( m_pMinXTxt, 0, 0, 5 );
	
	m_pMinYTxt = new wxTextCtrl( m_panel2, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER|wxSIMPLE_BORDER );
	m_pMinYTxt->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pMinYTxt->SetBackgroundColour( wxColour( 0, 0, 0 ) );
	
	fgSizer1->Add( m_pMinYTxt, 0, 0, 5 );
	
	m_pMinZTxt = new wxTextCtrl( m_panel2, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER|wxSIMPLE_BORDER );
	m_pMinZTxt->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pMinZTxt->SetBackgroundColour( wxColour( 0, 0, 0 ) );
	
	fgSizer1->Add( m_pMinZTxt, 0, 0, 5 );
	
	m_staticText7 = new wxStaticText( m_panel2, wxID_ANY, wxT("Max"), wxDefaultPosition, wxSize( 55,-1 ), 0 );
	m_staticText7->Wrap( -1 );
	m_staticText7->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	fgSizer1->Add( m_staticText7, 0, wxALIGN_CENTER_VERTICAL|wxALIGN_RIGHT|wxRIGHT|wxLEFT, 5 );
	
	m_pMaxXTxt = new wxTextCtrl( m_panel2, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER|wxSIMPLE_BORDER );
	m_pMaxXTxt->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pMaxXTxt->SetBackgroundColour( wxColour( 0, 0, 0 ) );
	
	fgSizer1->Add( m_pMaxXTxt, 0, 0, 5 );
	
	m_pMaxYTxt = new wxTextCtrl( m_panel2, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER|wxSIMPLE_BORDER );
	m_pMaxYTxt->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pMaxYTxt->SetBackgroundColour( wxColour( 0, 0, 0 ) );
	
	fgSizer1->Add( m_pMaxYTxt, 0, 0, 5 );
	
	m_pMaxZTxt = new wxTextCtrl( m_panel2, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER|wxSIMPLE_BORDER );
	m_pMaxZTxt->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pMaxZTxt->SetBackgroundColour( wxColour( 0, 0, 0 ) );
	
	fgSizer1->Add( m_pMaxZTxt, 0, 0, 5 );
	
	m_staticText11 = new wxStaticText( m_panel2, wxID_ANY, wxT("Size"), wxDefaultPosition, wxSize( 55,-1 ), 0 );
	m_staticText11->Wrap( -1 );
	m_staticText11->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	fgSizer1->Add( m_staticText11, 0, wxALIGN_CENTER_VERTICAL|wxALIGN_RIGHT|wxRIGHT|wxLEFT, 5 );
	
	m_pSizeXTxt = new wxTextCtrl( m_panel2, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER|wxSIMPLE_BORDER );
	m_pSizeXTxt->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pSizeXTxt->SetBackgroundColour( wxColour( 0, 0, 0 ) );
	
	fgSizer1->Add( m_pSizeXTxt, 0, 0, 5 );
	
	m_pSizeYTxt = new wxTextCtrl( m_panel2, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER|wxSIMPLE_BORDER );
	m_pSizeYTxt->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pSizeYTxt->SetBackgroundColour( wxColour( 0, 0, 0 ) );
	
	fgSizer1->Add( m_pSizeYTxt, 0, 0, 5 );
	
	m_pSizeZTxt = new wxTextCtrl( m_panel2, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER|wxSIMPLE_BORDER );
	m_pSizeZTxt->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pSizeZTxt->SetBackgroundColour( wxColour( 0, 0, 0 ) );
	
	fgSizer1->Add( m_pSizeZTxt, 0, 0, 5 );
	
	m_staticText421 = new wxStaticText( m_panel2, wxID_ANY, wxT("Centering"), wxDefaultPosition, wxSize( 55,-1 ), 0 );
	m_staticText421->Wrap( -1 );
	m_staticText421->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	fgSizer1->Add( m_staticText421, 0, wxALIGN_CENTER_VERTICAL|wxALIGN_CENTER_HORIZONTAL|wxEXPAND|wxRIGHT|wxLEFT, 5 );
	
	m_pCenterXChk = new wxCheckBox( m_panel2, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	fgSizer1->Add( m_pCenterXChk, 0, wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxTOP, 5 );
	
	m_pCenterYChk = new wxCheckBox( m_panel2, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	fgSizer1->Add( m_pCenterYChk, 0, wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxTOP, 5 );
	
	m_pCenterZChk = new wxCheckBox( m_panel2, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	fgSizer1->Add( m_pCenterZChk, 0, wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxTOP, 5 );
	
	bSizer2->Add( fgSizer1, 0, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer33;
	bSizer33 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText251 = new wxStaticText( m_panel2, wxID_ANY, wxT("Unit of Length"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText251->Wrap( -1 );
	m_staticText251->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	bSizer33->Add( m_staticText251, 0, wxALIGN_CENTER_VERTICAL|wxLEFT, 5 );
	
	wxArrayString m_pUnitChoiceChoices;
	m_pUnitChoice = new wxChoice( m_panel2, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_pUnitChoiceChoices, 0 );
	m_pUnitChoice->SetSelection( 0 );
	m_pUnitChoice->SetForegroundColour( wxColour( 0, 0, 0 ) );
	m_pUnitChoice->SetBackgroundColour( wxColour( 255, 255, 255 ) );
	
	bSizer33->Add( m_pUnitChoice, 1, wxALL, 5 );
	
	m_pDefaultBtn = new wxButton( m_panel2, wxID_ANY, wxT("Default"), wxDefaultPosition, wxDefaultSize, 0|wxSIMPLE_BORDER );
	m_pDefaultBtn->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pDefaultBtn->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	bSizer33->Add( m_pDefaultBtn, 0, wxALIGN_RIGHT|wxALL, 5 );
	
	bSizer2->Add( bSizer33, 1, wxEXPAND, 5 );
	
	m_panel2->SetSizer( bSizer2 );
	m_panel2->Layout();
	bSizer2->Fit( m_panel2 );
	m_pTopSizer->Add( m_panel2, 1, wxEXPAND | wxALL, 0 );
	
	wxBoxSizer* bSizer37;
	bSizer37 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText311 = new wxStaticText( this, wxID_ANY, wxT("Division Setting"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText311->Wrap( -1 );
	m_staticText311->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), 70, 90, 92, false, wxEmptyString ) );
	m_staticText311->SetForegroundColour( wxColour( 209, 76, 16 ) );
	
	bSizer37->Add( m_staticText311, 0, wxALL, 4 );
	
	m_pTopSizer->Add( bSizer37, 0, wxEXPAND, 5 );
	
	m_pDivisionSettingBoxSizer = new wxBoxSizer( wxVERTICAL );
	
	m_pVoxPanel = new wxPanel( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	m_pVoxPanel->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	wxBoxSizer* bSizer34;
	bSizer34 = new wxBoxSizer( wxVERTICAL );
	
	wxBoxSizer* bSizer180;
	bSizer180 = new wxBoxSizer( wxHORIZONTAL );
	
	m_pVoxBtn = new wxBitmapButton( m_pVoxPanel, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxSize( -1,-1 ), 0 );
	m_pVoxBtn->SetForegroundColour( wxColour( 88, 88, 88 ) );
	m_pVoxBtn->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	m_pVoxBtn->SetForegroundColour( wxColour( 88, 88, 88 ) );
	m_pVoxBtn->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	bSizer180->Add( m_pVoxBtn, 0, wxALL, 5 );
	
	m_staticText3111 = new wxStaticText( m_pVoxPanel, wxID_ANY, wxT("Voxel Setting"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText3111->Wrap( -1 );
	m_staticText3111->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), 70, 90, 92, false, wxEmptyString ) );
	m_staticText3111->SetForegroundColour( wxColour( 209, 76, 16 ) );
	
	bSizer180->Add( m_staticText3111, 0, wxALL, 5 );
	
	bSizer34->Add( bSizer180, 0, wxEXPAND, 5 );
	
	m_pVoxBoxSizer = new wxBoxSizer( wxVERTICAL );
	
	wxBoxSizer* bSizer38;
	bSizer38 = new wxBoxSizer( wxHORIZONTAL );
	
	m_pUnifChk = new wxCheckBox( m_pVoxPanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_pUnifChk->SetValue(true); 
	bSizer38->Add( m_pUnifChk, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_staticText24 = new wxStaticText( m_pVoxPanel, wxID_ANY, wxT("Force Uniform Pitch"), wxDefaultPosition, wxSize( -1,-1 ), 0 );
	m_staticText24->Wrap( -1 );
	m_staticText24->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	bSizer38->Add( m_staticText24, 0, wxTOP|wxBOTTOM|wxRIGHT, 5 );
	
	m_pVoxBoxSizer->Add( bSizer38, 0, wxEXPAND, 5 );
	
	wxFlexGridSizer* fgSizer2;
	fgSizer2 = new wxFlexGridSizer( 3, 4, 0, 0 );
	fgSizer2->SetFlexibleDirection( wxBOTH );
	fgSizer2->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );
	
	
	fgSizer2->Add( 0, 0, 1, wxEXPAND, 5 );
	
	m_staticText261 = new wxStaticText( m_pVoxPanel, wxID_ANY, wxT("X"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText261->Wrap( -1 );
	m_staticText261->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	fgSizer2->Add( m_staticText261, 0, wxALIGN_CENTER_HORIZONTAL|wxRIGHT|wxLEFT, 5 );
	
	m_staticText2612 = new wxStaticText( m_pVoxPanel, wxID_ANY, wxT("Y"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText2612->Wrap( -1 );
	m_staticText2612->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	fgSizer2->Add( m_staticText2612, 0, wxALIGN_CENTER_HORIZONTAL|wxRIGHT|wxLEFT, 5 );
	
	m_staticText2613 = new wxStaticText( m_pVoxPanel, wxID_ANY, wxT("Z"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText2613->Wrap( -1 );
	m_staticText2613->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	fgSizer2->Add( m_staticText2613, 0, wxALIGN_CENTER_HORIZONTAL|wxRIGHT|wxLEFT, 5 );
	
	m_staticText22 = new wxStaticText( m_pVoxPanel, wxID_ANY, wxT("Pitch"), wxDefaultPosition, wxSize( 55,-1 ), 0 );
	m_staticText22->Wrap( -1 );
	m_staticText22->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	fgSizer2->Add( m_staticText22, 0, wxALIGN_CENTER_VERTICAL|wxALIGN_RIGHT|wxRIGHT|wxLEFT, 5 );
	
	m_pPitchXTxt = new wxTextCtrl( m_pVoxPanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER|wxSIMPLE_BORDER );
	m_pPitchXTxt->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pPitchXTxt->SetBackgroundColour( wxColour( 0, 0, 0 ) );
	
	fgSizer2->Add( m_pPitchXTxt, 0, 0, 5 );
	
	m_pPitchYTxt = new wxTextCtrl( m_pVoxPanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER|wxSIMPLE_BORDER );
	m_pPitchYTxt->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pPitchYTxt->SetBackgroundColour( wxColour( 0, 0, 0 ) );
	
	fgSizer2->Add( m_pPitchYTxt, 0, 0, 5 );
	
	m_pPitchZTxt = new wxTextCtrl( m_pVoxPanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER|wxSIMPLE_BORDER );
	m_pPitchZTxt->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pPitchZTxt->SetBackgroundColour( wxColour( 0, 0, 0 ) );
	
	fgSizer2->Add( m_pPitchZTxt, 0, 0, 5 );
	
	m_staticText271 = new wxStaticText( m_pVoxPanel, wxID_ANY, wxT("Voxel"), wxDefaultPosition, wxSize( 55,-1 ), 0 );
	m_staticText271->Wrap( -1 );
	m_staticText271->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	fgSizer2->Add( m_staticText271, 0, wxALIGN_CENTER_VERTICAL|wxALIGN_RIGHT|wxRIGHT|wxLEFT, 5 );
	
	m_pVoxXTxt = new wxTextCtrl( m_pVoxPanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER|wxSIMPLE_BORDER );
	m_pVoxXTxt->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pVoxXTxt->SetBackgroundColour( wxColour( 0, 0, 0 ) );
	
	fgSizer2->Add( m_pVoxXTxt, 0, 0, 5 );
	
	m_pVoxYTxt = new wxTextCtrl( m_pVoxPanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER|wxSIMPLE_BORDER );
	m_pVoxYTxt->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pVoxYTxt->SetBackgroundColour( wxColour( 0, 0, 0 ) );
	
	fgSizer2->Add( m_pVoxYTxt, 0, 0, 5 );
	
	m_pVoxZTxt = new wxTextCtrl( m_pVoxPanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER|wxSIMPLE_BORDER );
	m_pVoxZTxt->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pVoxZTxt->SetBackgroundColour( wxColour( 0, 0, 0 ) );
	
	fgSizer2->Add( m_pVoxZTxt, 0, 0, 5 );
	
	m_pVoxBoxSizer->Add( fgSizer2, 0, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer31;
	bSizer31 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText23 = new wxStaticText( m_pVoxPanel, wxID_ANY, wxT("File Export Policy"), wxDefaultPosition, wxSize( -1,-1 ), 0 );
	m_staticText23->Wrap( -1 );
	m_staticText23->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	bSizer31->Add( m_staticText23, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );
	
	m_staticline91 = new wxStaticLine( m_pVoxPanel, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_VERTICAL );
	bSizer31->Add( m_staticline91, 0, wxEXPAND|wxALL, 5 );
	
	m_pExpPolicyPitchBtn = new wxRadioButton( m_pVoxPanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxRB_GROUP );
	bSizer31->Add( m_pExpPolicyPitchBtn, 0, wxALL, 5 );
	
	m_staticText212 = new wxStaticText( m_pVoxPanel, wxID_ANY, wxT("Pitch"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText212->Wrap( -1 );
	m_staticText212->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	bSizer31->Add( m_staticText212, 0, wxALIGN_CENTER_VERTICAL|wxTOP|wxBOTTOM|wxRIGHT, 5 );
	
	m_pExpPolicyVoxBtn = new wxRadioButton( m_pVoxPanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizer31->Add( m_pExpPolicyVoxBtn, 0, wxALL, 5 );
	
	m_staticText2111 = new wxStaticText( m_pVoxPanel, wxID_ANY, wxT("Voxel"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText2111->Wrap( -1 );
	m_staticText2111->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	bSizer31->Add( m_staticText2111, 0, wxTOP|wxBOTTOM|wxRIGHT, 5 );
	
	m_pVoxBoxSizer->Add( bSizer31, 0, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer52;
	bSizer52 = new wxBoxSizer( wxVERTICAL );
	
	m_pDvdPanel = new wxPanel( m_pVoxPanel, wxID_ANY, wxPoint( -1,-1 ), wxDefaultSize, wxDOUBLE_BORDER|wxTAB_TRAVERSAL );
	m_pDvdPanel->SetBackgroundColour( wxColour( 100, 100, 100 ) );
	
	wxBoxSizer* bSizer341;
	bSizer341 = new wxBoxSizer( wxVERTICAL );
	
	wxBoxSizer* bSizer391;
	bSizer391 = new wxBoxSizer( wxHORIZONTAL );
	
	m_pDvdBtn = new wxBitmapButton( m_pDvdPanel, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxSize( -1,-1 ), 0 );
	m_pDvdBtn->SetBackgroundColour( wxColour( 100, 100, 100 ) );
	
	m_pDvdBtn->SetBackgroundColour( wxColour( 100, 100, 100 ) );
	
	bSizer391->Add( m_pDvdBtn, 0, wxALL, 5 );
	
	m_staticText31111 = new wxStaticText( m_pDvdPanel, wxID_ANY, wxT("Subdomain Division"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText31111->Wrap( -1 );
	m_staticText31111->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), 70, 90, 92, false, wxEmptyString ) );
	m_staticText31111->SetForegroundColour( wxColour( 209, 76, 16 ) );
	
	bSizer391->Add( m_staticText31111, 0, wxALL, 5 );
	
	bSizer341->Add( bSizer391, 0, wxEXPAND, 5 );
	
	m_pDvdBoxSizer = new wxBoxSizer( wxVERTICAL );
	
	wxFlexGridSizer* fgSizer31;
	fgSizer31 = new wxFlexGridSizer( 3, 4, 0, 0 );
	fgSizer31->SetFlexibleDirection( wxBOTH );
	fgSizer31->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );
	
	
	fgSizer31->Add( 0, 0, 1, wxEXPAND, 5 );
	
	m_staticText28113 = new wxStaticText( m_pDvdPanel, wxID_ANY, wxT("X"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText28113->Wrap( -1 );
	m_staticText28113->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	fgSizer31->Add( m_staticText28113, 0, wxALIGN_CENTER_HORIZONTAL|wxRIGHT|wxLEFT, 5 );
	
	m_staticText281111 = new wxStaticText( m_pDvdPanel, wxID_ANY, wxT("Y"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText281111->Wrap( -1 );
	m_staticText281111->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	fgSizer31->Add( m_staticText281111, 0, wxALIGN_CENTER_HORIZONTAL|wxRIGHT|wxLEFT, 5 );
	
	m_staticText281121 = new wxStaticText( m_pDvdPanel, wxID_ANY, wxT("Z"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText281121->Wrap( -1 );
	m_staticText281121->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	fgSizer31->Add( m_staticText281121, 0, wxALIGN_CENTER_HORIZONTAL|wxRIGHT|wxLEFT, 5 );
	
	m_staticText2812 = new wxStaticText( m_pDvdPanel, wxID_ANY, wxT("Manual"), wxDefaultPosition, wxSize( 55,-1 ), 0 );
	m_staticText2812->Wrap( -1 );
	m_staticText2812->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	fgSizer31->Add( m_staticText2812, 0, wxALIGN_CENTER_VERTICAL|wxALIGN_RIGHT|wxRIGHT|wxLEFT, 5 );
	
	m_pDvdXTxt = new wxTextCtrl( m_pDvdPanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER|wxSIMPLE_BORDER );
	m_pDvdXTxt->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pDvdXTxt->SetBackgroundColour( wxColour( 0, 0, 0 ) );
	
	fgSizer31->Add( m_pDvdXTxt, 0, 0, 5 );
	
	m_pDvdYTxt = new wxTextCtrl( m_pDvdPanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER|wxSIMPLE_BORDER );
	m_pDvdYTxt->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pDvdYTxt->SetBackgroundColour( wxColour( 0, 0, 0 ) );
	
	fgSizer31->Add( m_pDvdYTxt, 0, 0, 5 );
	
	m_pDvdZTxt = new wxTextCtrl( m_pDvdPanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER|wxSIMPLE_BORDER );
	m_pDvdZTxt->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pDvdZTxt->SetBackgroundColour( wxColour( 0, 0, 0 ) );
	
	fgSizer31->Add( m_pDvdZTxt, 0, 0, 5 );
	
	m_staticText2911 = new wxStaticText( m_pDvdPanel, wxID_ANY, wxT("Auto"), wxDefaultPosition, wxSize( 55,-1 ), 0 );
	m_staticText2911->Wrap( -1 );
	m_staticText2911->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	fgSizer31->Add( m_staticText2911, 0, wxALIGN_CENTER_VERTICAL|wxALIGN_RIGHT|wxRIGHT|wxLEFT, 5 );
	
	m_pDvdAutoTxt = new wxTextCtrl( m_pDvdPanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER|wxSIMPLE_BORDER );
	m_pDvdAutoTxt->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pDvdAutoTxt->SetBackgroundColour( wxColour( 0, 0, 0 ) );
	
	fgSizer31->Add( m_pDvdAutoTxt, 0, 0, 5 );
	
	m_pDvdBoxSizer->Add( fgSizer31, 1, wxEXPAND, 5 );
	
	bSizer341->Add( m_pDvdBoxSizer, 0, wxEXPAND, 5 );
	
	m_pDvdPanel->SetSizer( bSizer341 );
	m_pDvdPanel->Layout();
	bSizer341->Fit( m_pDvdPanel );
	bSizer52->Add( m_pDvdPanel, 0, wxEXPAND, 5 );
	
	m_pVoxBoxSizer->Add( bSizer52, 1, wxEXPAND, 5 );
	
	bSizer34->Add( m_pVoxBoxSizer, 0, wxEXPAND, 5 );
	
	m_pVoxPanel->SetSizer( bSizer34 );
	m_pVoxPanel->Layout();
	bSizer34->Fit( m_pVoxPanel );
	m_pDivisionSettingBoxSizer->Add( m_pVoxPanel, 0, wxEXPAND, 5 );
	
	m_pTopSizer->Add( m_pDivisionSettingBoxSizer, 0, wxEXPAND, 5 );
	
	m_pBaseSizer->Add( m_pTopSizer, 0, wxALL|wxEXPAND, 4 );
	
	m_staticline1 = new wxStaticLine( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	m_pBaseSizer->Add( m_staticline1, 0, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer7;
	bSizer7 = new wxBoxSizer( wxHORIZONTAL );
	
	m_pOKBtn = new wxButton( this, wxID_ANY, wxT("OK"), wxDefaultPosition, wxDefaultSize, 0|wxSIMPLE_BORDER );
	m_pOKBtn->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pOKBtn->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	bSizer7->Add( m_pOKBtn, 0, wxALL, 5 );
	
	m_pCancelBtn = new wxButton( this, wxID_ANY, wxT("Cancel"), wxDefaultPosition, wxDefaultSize, 0|wxSIMPLE_BORDER );
	m_pCancelBtn->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pCancelBtn->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	bSizer7->Add( m_pCancelBtn, 0, wxALL, 5 );
	
	m_pBaseSizer->Add( bSizer7, 0, wxALIGN_RIGHT, 5 );
	
	this->SetSizer( m_pBaseSizer );
	this->Layout();
	m_pBaseSizer->Fit( this );
	
	this->Centre( wxBOTH );
	
	// Connect Events
	this->Connect( wxEVT_CLOSE_WINDOW, wxCloseEventHandler( DomainCartDlgBase::OnClose ) );
	m_pPolicySizeRBtn->Connect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( DomainCartDlgBase::OnPolicyRadio ), NULL, this );
	m_pPolicyMaxRBtn->Connect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( DomainCartDlgBase::OnPolicyRadio ), NULL, this );
	m_pMinXTxt->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( DomainCartDlgBase::OnBBoxTxtOnKillFocus ), NULL, this );
	m_pMinXTxt->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( DomainCartDlgBase::OnBBoxTxt ), NULL, this );
	m_pMinYTxt->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( DomainCartDlgBase::OnBBoxTxtOnKillFocus ), NULL, this );
	m_pMinYTxt->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( DomainCartDlgBase::OnBBoxTxt ), NULL, this );
	m_pMinZTxt->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( DomainCartDlgBase::OnBBoxTxtOnKillFocus ), NULL, this );
	m_pMinZTxt->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( DomainCartDlgBase::OnBBoxTxt ), NULL, this );
	m_pMaxXTxt->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( DomainCartDlgBase::OnBBoxTxtOnKillFocus ), NULL, this );
	m_pMaxXTxt->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( DomainCartDlgBase::OnBBoxTxt ), NULL, this );
	m_pMaxYTxt->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( DomainCartDlgBase::OnBBoxTxtOnKillFocus ), NULL, this );
	m_pMaxYTxt->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( DomainCartDlgBase::OnBBoxTxt ), NULL, this );
	m_pMaxZTxt->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( DomainCartDlgBase::OnBBoxTxtOnKillFocus ), NULL, this );
	m_pMaxZTxt->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( DomainCartDlgBase::OnBBoxTxt ), NULL, this );
	m_pSizeXTxt->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( DomainCartDlgBase::OnBBoxTxtOnKillFocus ), NULL, this );
	m_pSizeXTxt->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( DomainCartDlgBase::OnBBoxTxt ), NULL, this );
	m_pSizeYTxt->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( DomainCartDlgBase::OnBBoxTxtOnKillFocus ), NULL, this );
	m_pSizeYTxt->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( DomainCartDlgBase::OnBBoxTxt ), NULL, this );
	m_pSizeZTxt->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( DomainCartDlgBase::OnBBoxTxtOnKillFocus ), NULL, this );
	m_pSizeZTxt->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( DomainCartDlgBase::OnBBoxTxt ), NULL, this );
	m_pCenterXChk->Connect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( DomainCartDlgBase::OnCenterChk ), NULL, this );
	m_pCenterYChk->Connect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( DomainCartDlgBase::OnCenterChk ), NULL, this );
	m_pCenterZChk->Connect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( DomainCartDlgBase::OnCenterChk ), NULL, this );
	m_pUnitChoice->Connect( wxEVT_COMMAND_CHOICE_SELECTED, wxCommandEventHandler( DomainCartDlgBase::OnUnitChoice ), NULL, this );
	m_pDefaultBtn->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( DomainCartDlgBase::OnDefaultBtn ), NULL, this );
	m_pVoxBtn->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( DomainCartDlgBase::OnEnableVoxelBtn ), NULL, this );
	m_pUnifChk->Connect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( DomainCartDlgBase::OnForceUnifChk ), NULL, this );
	m_pPitchXTxt->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( DomainCartDlgBase::OnPitchTxtOnKillFocus ), NULL, this );
	m_pPitchXTxt->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( DomainCartDlgBase::OnPitchTxt ), NULL, this );
	m_pPitchYTxt->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( DomainCartDlgBase::OnPitchTxtOnKillFocus ), NULL, this );
	m_pPitchYTxt->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( DomainCartDlgBase::OnPitchTxt ), NULL, this );
	m_pPitchZTxt->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( DomainCartDlgBase::OnPitchTxtOnKillFocus ), NULL, this );
	m_pPitchZTxt->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( DomainCartDlgBase::OnPitchTxt ), NULL, this );
	m_pVoxXTxt->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( DomainCartDlgBase::OnVoxTxtOnKillFocus ), NULL, this );
	m_pVoxXTxt->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( DomainCartDlgBase::OnVoxTxt ), NULL, this );
	m_pVoxYTxt->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( DomainCartDlgBase::OnVoxTxtOnKillFocus ), NULL, this );
	m_pVoxYTxt->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( DomainCartDlgBase::OnVoxTxt ), NULL, this );
	m_pVoxZTxt->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( DomainCartDlgBase::OnVoxTxtOnKillFocus ), NULL, this );
	m_pVoxZTxt->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( DomainCartDlgBase::OnVoxTxt ), NULL, this );
	m_pExpPolicyPitchBtn->Connect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( DomainCartDlgBase::OnExportPolicyRadio ), NULL, this );
	m_pExpPolicyVoxBtn->Connect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( DomainCartDlgBase::OnExportPolicyRadio ), NULL, this );
	m_pDvdBtn->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( DomainCartDlgBase::OnEnableDvdBtn ), NULL, this );
	m_pDvdXTxt->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( DomainCartDlgBase::OnDvdManuTxtOnKillFocus ), NULL, this );
	m_pDvdXTxt->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( DomainCartDlgBase::OnDvdManuTxt ), NULL, this );
	m_pDvdYTxt->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( DomainCartDlgBase::OnDvdManuTxtOnKillFocus ), NULL, this );
	m_pDvdYTxt->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( DomainCartDlgBase::OnDvdManuTxt ), NULL, this );
	m_pDvdZTxt->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( DomainCartDlgBase::OnDvdManuTxtOnKillFocus ), NULL, this );
	m_pDvdZTxt->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( DomainCartDlgBase::OnDvdManuTxt ), NULL, this );
	m_pDvdAutoTxt->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( DomainCartDlgBase::OnDvdAutoTxtOnKillFocus ), NULL, this );
	m_pDvdAutoTxt->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( DomainCartDlgBase::OnDvdAutoTxt ), NULL, this );
	m_pOKBtn->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( DomainCartDlgBase::OnOKBtn ), NULL, this );
	m_pCancelBtn->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( DomainCartDlgBase::OnCancelBtn ), NULL, this );
}

DomainCartDlgBase::~DomainCartDlgBase()
{
	// Disconnect Events
	this->Disconnect( wxEVT_CLOSE_WINDOW, wxCloseEventHandler( DomainCartDlgBase::OnClose ) );
	m_pPolicySizeRBtn->Disconnect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( DomainCartDlgBase::OnPolicyRadio ), NULL, this );
	m_pPolicyMaxRBtn->Disconnect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( DomainCartDlgBase::OnPolicyRadio ), NULL, this );
	m_pMinXTxt->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( DomainCartDlgBase::OnBBoxTxtOnKillFocus ), NULL, this );
	m_pMinXTxt->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( DomainCartDlgBase::OnBBoxTxt ), NULL, this );
	m_pMinYTxt->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( DomainCartDlgBase::OnBBoxTxtOnKillFocus ), NULL, this );
	m_pMinYTxt->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( DomainCartDlgBase::OnBBoxTxt ), NULL, this );
	m_pMinZTxt->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( DomainCartDlgBase::OnBBoxTxtOnKillFocus ), NULL, this );
	m_pMinZTxt->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( DomainCartDlgBase::OnBBoxTxt ), NULL, this );
	m_pMaxXTxt->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( DomainCartDlgBase::OnBBoxTxtOnKillFocus ), NULL, this );
	m_pMaxXTxt->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( DomainCartDlgBase::OnBBoxTxt ), NULL, this );
	m_pMaxYTxt->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( DomainCartDlgBase::OnBBoxTxtOnKillFocus ), NULL, this );
	m_pMaxYTxt->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( DomainCartDlgBase::OnBBoxTxt ), NULL, this );
	m_pMaxZTxt->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( DomainCartDlgBase::OnBBoxTxtOnKillFocus ), NULL, this );
	m_pMaxZTxt->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( DomainCartDlgBase::OnBBoxTxt ), NULL, this );
	m_pSizeXTxt->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( DomainCartDlgBase::OnBBoxTxtOnKillFocus ), NULL, this );
	m_pSizeXTxt->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( DomainCartDlgBase::OnBBoxTxt ), NULL, this );
	m_pSizeYTxt->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( DomainCartDlgBase::OnBBoxTxtOnKillFocus ), NULL, this );
	m_pSizeYTxt->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( DomainCartDlgBase::OnBBoxTxt ), NULL, this );
	m_pSizeZTxt->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( DomainCartDlgBase::OnBBoxTxtOnKillFocus ), NULL, this );
	m_pSizeZTxt->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( DomainCartDlgBase::OnBBoxTxt ), NULL, this );
	m_pCenterXChk->Disconnect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( DomainCartDlgBase::OnCenterChk ), NULL, this );
	m_pCenterYChk->Disconnect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( DomainCartDlgBase::OnCenterChk ), NULL, this );
	m_pCenterZChk->Disconnect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( DomainCartDlgBase::OnCenterChk ), NULL, this );
	m_pUnitChoice->Disconnect( wxEVT_COMMAND_CHOICE_SELECTED, wxCommandEventHandler( DomainCartDlgBase::OnUnitChoice ), NULL, this );
	m_pDefaultBtn->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( DomainCartDlgBase::OnDefaultBtn ), NULL, this );
	m_pVoxBtn->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( DomainCartDlgBase::OnEnableVoxelBtn ), NULL, this );
	m_pUnifChk->Disconnect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( DomainCartDlgBase::OnForceUnifChk ), NULL, this );
	m_pPitchXTxt->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( DomainCartDlgBase::OnPitchTxtOnKillFocus ), NULL, this );
	m_pPitchXTxt->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( DomainCartDlgBase::OnPitchTxt ), NULL, this );
	m_pPitchYTxt->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( DomainCartDlgBase::OnPitchTxtOnKillFocus ), NULL, this );
	m_pPitchYTxt->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( DomainCartDlgBase::OnPitchTxt ), NULL, this );
	m_pPitchZTxt->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( DomainCartDlgBase::OnPitchTxtOnKillFocus ), NULL, this );
	m_pPitchZTxt->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( DomainCartDlgBase::OnPitchTxt ), NULL, this );
	m_pVoxXTxt->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( DomainCartDlgBase::OnVoxTxtOnKillFocus ), NULL, this );
	m_pVoxXTxt->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( DomainCartDlgBase::OnVoxTxt ), NULL, this );
	m_pVoxYTxt->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( DomainCartDlgBase::OnVoxTxtOnKillFocus ), NULL, this );
	m_pVoxYTxt->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( DomainCartDlgBase::OnVoxTxt ), NULL, this );
	m_pVoxZTxt->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( DomainCartDlgBase::OnVoxTxtOnKillFocus ), NULL, this );
	m_pVoxZTxt->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( DomainCartDlgBase::OnVoxTxt ), NULL, this );
	m_pExpPolicyPitchBtn->Disconnect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( DomainCartDlgBase::OnExportPolicyRadio ), NULL, this );
	m_pExpPolicyVoxBtn->Disconnect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( DomainCartDlgBase::OnExportPolicyRadio ), NULL, this );
	m_pDvdBtn->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( DomainCartDlgBase::OnEnableDvdBtn ), NULL, this );
	m_pDvdXTxt->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( DomainCartDlgBase::OnDvdManuTxtOnKillFocus ), NULL, this );
	m_pDvdXTxt->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( DomainCartDlgBase::OnDvdManuTxt ), NULL, this );
	m_pDvdYTxt->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( DomainCartDlgBase::OnDvdManuTxtOnKillFocus ), NULL, this );
	m_pDvdYTxt->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( DomainCartDlgBase::OnDvdManuTxt ), NULL, this );
	m_pDvdZTxt->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( DomainCartDlgBase::OnDvdManuTxtOnKillFocus ), NULL, this );
	m_pDvdZTxt->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( DomainCartDlgBase::OnDvdManuTxt ), NULL, this );
	m_pDvdAutoTxt->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( DomainCartDlgBase::OnDvdAutoTxtOnKillFocus ), NULL, this );
	m_pDvdAutoTxt->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( DomainCartDlgBase::OnDvdAutoTxt ), NULL, this );
	m_pOKBtn->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( DomainCartDlgBase::OnOKBtn ), NULL, this );
	m_pCancelBtn->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( DomainCartDlgBase::OnCancelBtn ), NULL, this );
	
}

DomainBCMDlgBase::DomainBCMDlgBase( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );
	this->SetBackgroundColour( wxColour( 44, 44, 44 ) );
	
	m_pBaseSizer = new wxBoxSizer( wxVERTICAL );
	
	m_pTopSizer = new wxBoxSizer( wxVERTICAL );
	
	m_staticText31 = new wxStaticText( this, wxID_ANY, wxT("Region Setting"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText31->Wrap( -1 );
	m_staticText31->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), 70, 90, 92, false, wxEmptyString ) );
	m_staticText31->SetForegroundColour( wxColour( 209, 76, 16 ) );
	
	m_pTopSizer->Add( m_staticText31, 0, wxALL, 2 );
	
	m_panel2 = new wxPanel( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	m_panel2->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_panel2->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	wxBoxSizer* bSizer2;
	bSizer2 = new wxBoxSizer( wxVERTICAL );
	
	wxBoxSizer* bSizer3;
	bSizer3 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText2 = new wxStaticText( m_panel2, wxID_ANY, wxT("Region Setting Policy"), wxDefaultPosition, wxSize( -1,-1 ), 0 );
	m_staticText2->Wrap( -1 );
	m_staticText2->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	bSizer3->Add( m_staticText2, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );
	
	m_staticline132 = new wxStaticLine( m_panel2, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_VERTICAL );
	bSizer3->Add( m_staticline132, 0, wxEXPAND|wxALL, 5 );
	
	m_pPolicySizeRBtn = new wxRadioButton( m_panel2, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxRB_GROUP );
	bSizer3->Add( m_pPolicySizeRBtn, 0, wxALIGN_CENTER_VERTICAL|wxRIGHT|wxLEFT, 5 );
	
	m_staticText21 = new wxStaticText( m_panel2, wxID_ANY, wxT("Origin / Size"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText21->Wrap( -1 );
	m_staticText21->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	bSizer3->Add( m_staticText21, 0, wxALIGN_CENTER_VERTICAL|wxRIGHT, 5 );
	
	m_pPolicyMaxRBtn = new wxRadioButton( m_panel2, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizer3->Add( m_pPolicyMaxRBtn, 0, wxALIGN_CENTER_VERTICAL|wxRIGHT|wxLEFT, 5 );
	
	m_staticText211 = new wxStaticText( m_panel2, wxID_ANY, wxT("Origin / Max"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText211->Wrap( -1 );
	m_staticText211->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	bSizer3->Add( m_staticText211, 0, wxALIGN_CENTER_VERTICAL|wxRIGHT, 5 );
	
	bSizer2->Add( bSizer3, 0, wxEXPAND, 5 );
	
	wxFlexGridSizer* fgSizer1;
	fgSizer1 = new wxFlexGridSizer( 5, 4, 0, 0 );
	fgSizer1->SetFlexibleDirection( wxBOTH );
	fgSizer1->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );
	
	
	fgSizer1->Add( 0, 0, 1, wxEXPAND, 5 );
	
	m_staticText3 = new wxStaticText( m_panel2, wxID_ANY, wxT("X"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText3->Wrap( -1 );
	m_staticText3->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	fgSizer1->Add( m_staticText3, 0, wxALIGN_CENTER_HORIZONTAL|wxRIGHT|wxLEFT, 5 );
	
	m_staticText4 = new wxStaticText( m_panel2, wxID_ANY, wxT("Y"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText4->Wrap( -1 );
	m_staticText4->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	fgSizer1->Add( m_staticText4, 0, wxALIGN_CENTER_HORIZONTAL|wxRIGHT|wxLEFT, 5 );
	
	m_staticText41 = new wxStaticText( m_panel2, wxID_ANY, wxT("Z"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText41->Wrap( -1 );
	m_staticText41->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	fgSizer1->Add( m_staticText41, 0, wxALIGN_CENTER_HORIZONTAL|wxRIGHT|wxLEFT, 5 );
	
	m_staticText42 = new wxStaticText( m_panel2, wxID_ANY, wxT("Origin"), wxDefaultPosition, wxSize( 55,-1 ), 0 );
	m_staticText42->Wrap( -1 );
	m_staticText42->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	fgSizer1->Add( m_staticText42, 0, wxRIGHT|wxLEFT|wxALIGN_CENTER_VERTICAL|wxALIGN_RIGHT, 5 );
	
	m_pMinXTxt = new wxTextCtrl( m_panel2, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER|wxSIMPLE_BORDER );
	m_pMinXTxt->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pMinXTxt->SetBackgroundColour( wxColour( 0, 0, 0 ) );
	
	fgSizer1->Add( m_pMinXTxt, 0, 0, 5 );
	
	m_pMinYTxt = new wxTextCtrl( m_panel2, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER|wxSIMPLE_BORDER );
	m_pMinYTxt->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pMinYTxt->SetBackgroundColour( wxColour( 0, 0, 0 ) );
	
	fgSizer1->Add( m_pMinYTxt, 0, 0, 5 );
	
	m_pMinZTxt = new wxTextCtrl( m_panel2, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER|wxSIMPLE_BORDER );
	m_pMinZTxt->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pMinZTxt->SetBackgroundColour( wxColour( 0, 0, 0 ) );
	
	fgSizer1->Add( m_pMinZTxt, 0, wxRIGHT, 5 );
	
	m_staticText7 = new wxStaticText( m_panel2, wxID_ANY, wxT("Max"), wxDefaultPosition, wxSize( 55,-1 ), 0 );
	m_staticText7->Wrap( -1 );
	m_staticText7->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	fgSizer1->Add( m_staticText7, 0, wxRIGHT|wxLEFT|wxALIGN_CENTER_VERTICAL|wxALIGN_RIGHT, 5 );
	
	m_pMaxXTxt = new wxTextCtrl( m_panel2, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER|wxSIMPLE_BORDER );
	m_pMaxXTxt->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pMaxXTxt->SetBackgroundColour( wxColour( 0, 0, 0 ) );
	
	fgSizer1->Add( m_pMaxXTxt, 0, 0, 5 );
	
	m_pMaxYTxt = new wxTextCtrl( m_panel2, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER|wxSIMPLE_BORDER );
	m_pMaxYTxt->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pMaxYTxt->SetBackgroundColour( wxColour( 0, 0, 0 ) );
	
	fgSizer1->Add( m_pMaxYTxt, 0, 0, 5 );
	
	m_pMaxZTxt = new wxTextCtrl( m_panel2, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER|wxSIMPLE_BORDER );
	m_pMaxZTxt->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pMaxZTxt->SetBackgroundColour( wxColour( 0, 0, 0 ) );
	
	fgSizer1->Add( m_pMaxZTxt, 0, wxRIGHT, 5 );
	
	m_staticText11 = new wxStaticText( m_panel2, wxID_ANY, wxT("Size"), wxDefaultPosition, wxSize( 55,-1 ), 0 );
	m_staticText11->Wrap( -1 );
	m_staticText11->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	fgSizer1->Add( m_staticText11, 0, wxRIGHT|wxLEFT|wxALIGN_CENTER_VERTICAL|wxALIGN_RIGHT, 5 );
	
	m_pSizeXTxt = new wxTextCtrl( m_panel2, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER|wxSIMPLE_BORDER );
	m_pSizeXTxt->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pSizeXTxt->SetBackgroundColour( wxColour( 0, 0, 0 ) );
	
	fgSizer1->Add( m_pSizeXTxt, 0, 0, 5 );
	
	m_pSizeYTxt = new wxTextCtrl( m_panel2, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER|wxSIMPLE_BORDER );
	m_pSizeYTxt->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pSizeYTxt->SetBackgroundColour( wxColour( 0, 0, 0 ) );
	
	fgSizer1->Add( m_pSizeYTxt, 0, 0, 5 );
	
	m_pSizeZTxt = new wxTextCtrl( m_panel2, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER|wxSIMPLE_BORDER );
	m_pSizeZTxt->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pSizeZTxt->SetBackgroundColour( wxColour( 0, 0, 0 ) );
	
	fgSizer1->Add( m_pSizeZTxt, 0, wxRIGHT, 5 );
	
	m_staticText421 = new wxStaticText( m_panel2, wxID_ANY, wxT("Centering"), wxDefaultPosition, wxSize( 55,-1 ), 0 );
	m_staticText421->Wrap( -1 );
	m_staticText421->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	fgSizer1->Add( m_staticText421, 0, wxALIGN_CENTER_VERTICAL|wxRIGHT|wxLEFT|wxALIGN_RIGHT, 5 );
	
	m_pCenterXChk = new wxCheckBox( m_panel2, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	fgSizer1->Add( m_pCenterXChk, 0, wxALIGN_CENTER_HORIZONTAL|wxTOP|wxRIGHT|wxLEFT|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_pCenterYChk = new wxCheckBox( m_panel2, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	fgSizer1->Add( m_pCenterYChk, 0, wxALIGN_CENTER_HORIZONTAL|wxTOP|wxRIGHT|wxLEFT|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_pCenterZChk = new wxCheckBox( m_panel2, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	fgSizer1->Add( m_pCenterZChk, 0, wxALIGN_CENTER_HORIZONTAL|wxTOP|wxRIGHT|wxLEFT|wxALIGN_CENTER_VERTICAL, 5 );
	
	bSizer2->Add( fgSizer1, 0, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer33;
	bSizer33 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText251 = new wxStaticText( m_panel2, wxID_ANY, wxT("Unit of Length"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText251->Wrap( -1 );
	m_staticText251->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	bSizer33->Add( m_staticText251, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	wxArrayString m_pUnitChoiceChoices;
	m_pUnitChoice = new wxChoice( m_panel2, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_pUnitChoiceChoices, 0 );
	m_pUnitChoice->SetSelection( 0 );
	m_pUnitChoice->SetForegroundColour( wxColour( 0, 0, 0 ) );
	m_pUnitChoice->SetBackgroundColour( wxColour( 255, 255, 255 ) );
	
	bSizer33->Add( m_pUnitChoice, 1, wxALL, 5 );
	
	m_pDefaultBtn = new wxButton( m_panel2, wxID_ANY, wxT("Default"), wxDefaultPosition, wxDefaultSize, 0|wxSIMPLE_BORDER );
	m_pDefaultBtn->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pDefaultBtn->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	bSizer33->Add( m_pDefaultBtn, 0, wxALIGN_CENTER_VERTICAL|wxALIGN_RIGHT, 5 );
	
	bSizer2->Add( bSizer33, 1, wxEXPAND, 5 );
	
	m_panel2->SetSizer( bSizer2 );
	m_panel2->Layout();
	bSizer2->Fit( m_panel2 );
	m_pTopSizer->Add( m_panel2, 1, wxEXPAND|wxALL, 0 );
	
	wxBoxSizer* bSizer37;
	bSizer37 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText311 = new wxStaticText( this, wxID_ANY, wxT("Octree Parameter Setting"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText311->Wrap( -1 );
	m_staticText311->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), 70, 90, 92, false, wxEmptyString ) );
	m_staticText311->SetForegroundColour( wxColour( 209, 76, 16 ) );
	
	bSizer37->Add( m_staticText311, 0, wxALL, 4 );
	
	m_pTopSizer->Add( bSizer37, 0, wxEXPAND, 5 );
	
	m_pVoxDvdBoxSizer = new wxBoxSizer( wxVERTICAL );
	
	m_choicebook1 = new wxChoicebook( this, wxID_ANY, wxDefaultPosition, wxSize( 370,-1 ), wxCHB_DEFAULT );
	m_choicebook1->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	m_pVoxPanel = new wxPanel( m_choicebook1, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	m_pVoxPanel->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	wxBoxSizer* bSizer131;
	bSizer131 = new wxBoxSizer( wxVERTICAL );
	
	m_staticText23212 = new wxStaticText( m_pVoxPanel, wxID_ANY, wxT("Root Grid Setting"), wxDefaultPosition, wxSize( -1,-1 ), 0 );
	m_staticText23212->Wrap( -1 );
	m_staticText23212->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), 70, 90, 92, false, wxEmptyString ) );
	m_staticText23212->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	bSizer131->Add( m_staticText23212, 0, wxALL, 5 );
	
	wxBoxSizer* bSizer132;
	bSizer132 = new wxBoxSizer( wxHORIZONTAL );
	
	m_pUnifChk = new wxCheckBox( m_pVoxPanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizer132->Add( m_pUnifChk, 0, wxALL, 5 );
	
	m_staticText24 = new wxStaticText( m_pVoxPanel, wxID_ANY, wxT("Force Uniform Length"), wxDefaultPosition, wxSize( -1,-1 ), 0 );
	m_staticText24->Wrap( -1 );
	m_staticText24->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	bSizer132->Add( m_staticText24, 0, wxALL, 5 );
	
	bSizer131->Add( bSizer132, 0, wxEXPAND, 5 );
	
	wxFlexGridSizer* fgSizer2;
	fgSizer2 = new wxFlexGridSizer( 3, 4, 0, 0 );
	fgSizer2->SetFlexibleDirection( wxBOTH );
	fgSizer2->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );
	
	
	fgSizer2->Add( 0, 0, 1, wxEXPAND, 5 );
	
	m_staticText261 = new wxStaticText( m_pVoxPanel, wxID_ANY, wxT("X"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText261->Wrap( -1 );
	m_staticText261->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	fgSizer2->Add( m_staticText261, 0, wxALIGN_CENTER_HORIZONTAL|wxRIGHT|wxLEFT, 5 );
	
	m_staticText2612 = new wxStaticText( m_pVoxPanel, wxID_ANY, wxT("Y"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText2612->Wrap( -1 );
	m_staticText2612->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	fgSizer2->Add( m_staticText2612, 0, wxALIGN_CENTER_HORIZONTAL|wxRIGHT|wxLEFT, 5 );
	
	m_staticText2613 = new wxStaticText( m_pVoxPanel, wxID_ANY, wxT("Z"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText2613->Wrap( -1 );
	m_staticText2613->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	fgSizer2->Add( m_staticText2613, 0, wxALIGN_CENTER_HORIZONTAL|wxRIGHT|wxLEFT, 5 );
	
	m_staticText22 = new wxStaticText( m_pVoxPanel, wxID_ANY, wxT("Length"), wxDefaultPosition, wxSize( 50,-1 ), 0 );
	m_staticText22->Wrap( -1 );
	m_staticText22->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	fgSizer2->Add( m_staticText22, 0, wxRIGHT|wxLEFT|wxALIGN_CENTER_VERTICAL|wxALIGN_RIGHT, 5 );
	
	m_pRootLengthXTxt = new wxTextCtrl( m_pVoxPanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER|wxSIMPLE_BORDER );
	m_pRootLengthXTxt->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pRootLengthXTxt->SetBackgroundColour( wxColour( 0, 0, 0 ) );
	
	fgSizer2->Add( m_pRootLengthXTxt, 0, 0, 5 );
	
	m_pRootLengthYTxt = new wxTextCtrl( m_pVoxPanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER|wxSIMPLE_BORDER );
	m_pRootLengthYTxt->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pRootLengthYTxt->SetBackgroundColour( wxColour( 0, 0, 0 ) );
	
	fgSizer2->Add( m_pRootLengthYTxt, 0, 0, 5 );
	
	m_pRootLengthZTxt = new wxTextCtrl( m_pVoxPanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER|wxSIMPLE_BORDER );
	m_pRootLengthZTxt->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pRootLengthZTxt->SetBackgroundColour( wxColour( 0, 0, 0 ) );
	
	fgSizer2->Add( m_pRootLengthZTxt, 0, wxRIGHT, 5 );
	
	m_staticText271 = new wxStaticText( m_pVoxPanel, wxID_ANY, wxT("Dims"), wxDefaultPosition, wxSize( 50,-1 ), 0 );
	m_staticText271->Wrap( -1 );
	m_staticText271->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	fgSizer2->Add( m_staticText271, 0, wxALIGN_CENTER_VERTICAL|wxALIGN_RIGHT|wxBOTTOM|wxRIGHT|wxLEFT, 5 );
	
	m_pRootDimsXTxt = new wxTextCtrl( m_pVoxPanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER|wxSIMPLE_BORDER );
	m_pRootDimsXTxt->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pRootDimsXTxt->SetBackgroundColour( wxColour( 0, 0, 0 ) );
	
	fgSizer2->Add( m_pRootDimsXTxt, 0, wxBOTTOM, 5 );
	
	m_pRootDimsYTxt = new wxTextCtrl( m_pVoxPanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER|wxSIMPLE_BORDER );
	m_pRootDimsYTxt->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pRootDimsYTxt->SetBackgroundColour( wxColour( 0, 0, 0 ) );
	
	fgSizer2->Add( m_pRootDimsYTxt, 0, wxBOTTOM, 5 );
	
	m_pRootDimsZTxt = new wxTextCtrl( m_pVoxPanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER|wxSIMPLE_BORDER );
	m_pRootDimsZTxt->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pRootDimsZTxt->SetBackgroundColour( wxColour( 0, 0, 0 ) );
	
	fgSizer2->Add( m_pRootDimsZTxt, 0, wxBOTTOM|wxRIGHT, 5 );
	
	bSizer131->Add( fgSizer2, 0, wxEXPAND, 5 );
	
	m_staticline40 = new wxStaticLine( m_pVoxPanel, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	bSizer131->Add( m_staticline40, 0, wxEXPAND | wxALL, 5 );
	
	wxBoxSizer* bSizer148;
	bSizer148 = new wxBoxSizer( wxVERTICAL );
	
	m_staticText232122 = new wxStaticText( m_pVoxPanel, wxID_ANY, wxT("Octree Level Setting"), wxDefaultPosition, wxSize( -1,-1 ), 0 );
	m_staticText232122->Wrap( -1 );
	m_staticText232122->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), 70, 90, 92, false, wxEmptyString ) );
	m_staticText232122->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	bSizer148->Add( m_staticText232122, 0, wxALL, 5 );
	
	wxBoxSizer* bSizer173;
	bSizer173 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText23 = new wxStaticText( m_pVoxPanel, wxID_ANY, wxT("Fine Level"), wxDefaultPosition, wxSize( 100,-1 ), 0 );
	m_staticText23->Wrap( -1 );
	m_staticText23->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	bSizer173->Add( m_staticText23, 0, wxALIGN_CENTER_VERTICAL|wxRIGHT|wxLEFT, 5 );
	
	m_pBaseLevelTxt = new wxTextCtrl( m_pVoxPanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER|wxSIMPLE_BORDER );
	m_pBaseLevelTxt->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pBaseLevelTxt->SetBackgroundColour( wxColour( 0, 0, 0 ) );
	
	bSizer173->Add( m_pBaseLevelTxt, 0, wxALIGN_CENTER_VERTICAL, 5 );
	
	m_staticText231 = new wxStaticText( m_pVoxPanel, wxID_ANY, wxT("0 < Level < 16"), wxDefaultPosition, wxSize( -1,-1 ), 0 );
	m_staticText231->Wrap( -1 );
	m_staticText231->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	bSizer173->Add( m_staticText231, 0, wxALIGN_CENTER_VERTICAL|wxRIGHT|wxLEFT, 5 );
	
	bSizer148->Add( bSizer173, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer1731;
	bSizer1731 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText233 = new wxStaticText( m_pVoxPanel, wxID_ANY, wxT("Coarse Level"), wxDefaultPosition, wxSize( 100,-1 ), 0 );
	m_staticText233->Wrap( -1 );
	m_staticText233->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	bSizer1731->Add( m_staticText233, 0, wxALIGN_CENTER_VERTICAL|wxBOTTOM|wxRIGHT|wxLEFT, 5 );
	
	m_pMinLevelTxt = new wxTextCtrl( m_pVoxPanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER|wxSIMPLE_BORDER );
	m_pMinLevelTxt->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pMinLevelTxt->SetBackgroundColour( wxColour( 0, 0, 0 ) );
	
	bSizer1731->Add( m_pMinLevelTxt, 0, wxALIGN_CENTER_VERTICAL|wxBOTTOM, 5 );
	
	m_staticText2311 = new wxStaticText( m_pVoxPanel, wxID_ANY, wxT("0 < Level < 16"), wxDefaultPosition, wxSize( -1,-1 ), 0 );
	m_staticText2311->Wrap( -1 );
	m_staticText2311->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	bSizer1731->Add( m_staticText2311, 0, wxALIGN_CENTER_VERTICAL|wxBOTTOM|wxRIGHT|wxLEFT, 5 );
	
	bSizer148->Add( bSizer1731, 1, wxEXPAND, 5 );
	
	bSizer131->Add( bSizer148, 0, wxEXPAND, 5 );
	
	m_staticline401 = new wxStaticLine( m_pVoxPanel, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	bSizer131->Add( m_staticline401, 0, wxEXPAND|wxTOP|wxRIGHT|wxLEFT, 5 );
	
	wxBoxSizer* bSizer311;
	bSizer311 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText2322 = new wxStaticText( m_pVoxPanel, wxID_ANY, wxT("File Export Policy"), wxDefaultPosition, wxSize( -1,-1 ), 0 );
	m_staticText2322->Wrap( -1 );
	m_staticText2322->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	bSizer311->Add( m_staticText2322, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );
	
	m_staticline911 = new wxStaticLine( m_pVoxPanel, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_VERTICAL );
	bSizer311->Add( m_staticline911, 0, wxEXPAND|wxALL, 5 );
	
	m_pExpPolicyParamBtn = new wxRadioButton( m_pVoxPanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxRB_GROUP );
	bSizer311->Add( m_pExpPolicyParamBtn, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );
	
	m_staticText2121 = new wxStaticText( m_pVoxPanel, wxID_ANY, wxT("Parameter"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText2121->Wrap( -1 );
	m_staticText2121->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	bSizer311->Add( m_staticText2121, 0, wxALIGN_CENTER_VERTICAL|wxTOP|wxBOTTOM|wxRIGHT, 5 );
	
	m_pExpPolicyOctBtn = new wxRadioButton( m_pVoxPanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizer311->Add( m_pExpPolicyOctBtn, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );
	
	m_staticText21111 = new wxStaticText( m_pVoxPanel, wxID_ANY, wxT("Octree"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText21111->Wrap( -1 );
	m_staticText21111->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	bSizer311->Add( m_staticText21111, 0, wxALIGN_CENTER_VERTICAL|wxTOP|wxBOTTOM|wxRIGHT, 5 );
	
	bSizer131->Add( bSizer311, 0, wxEXPAND, 5 );
	
	m_pVoxPanel->SetSizer( bSizer131 );
	m_pVoxPanel->Layout();
	bSizer131->Fit( m_pVoxPanel );
	m_choicebook1->AddPage( m_pVoxPanel, wxT("Basic"), true );
	m_pVoxPanel2 = new wxPanel( m_choicebook1, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	m_pVoxPanel2->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	wxBoxSizer* bSizer1312;
	bSizer1312 = new wxBoxSizer( wxVERTICAL );
	
	m_staticText2321211 = new wxStaticText( m_pVoxPanel2, wxID_ANY, wxT("Leaf Block Size"), wxDefaultPosition, wxSize( -1,-1 ), 0 );
	m_staticText2321211->Wrap( -1 );
	m_staticText2321211->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), 70, 90, 92, false, wxEmptyString ) );
	m_staticText2321211->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	bSizer1312->Add( m_staticText2321211, 0, wxALL, 5 );
	
	wxBoxSizer* bSizer1801;
	bSizer1801 = new wxBoxSizer( wxVERTICAL );
	
	wxBoxSizer* bSizer1401;
	bSizer1401 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText26111 = new wxStaticText( m_pVoxPanel2, wxID_ANY, wxT("X"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText26111->Wrap( -1 );
	m_staticText26111->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	bSizer1401->Add( m_staticText26111, 0, wxALIGN_CENTER_HORIZONTAL|wxRIGHT|wxLEFT|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_pBlockSizeXTxt = new wxTextCtrl( m_pVoxPanel2, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER|wxSIMPLE_BORDER );
	m_pBlockSizeXTxt->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pBlockSizeXTxt->SetBackgroundColour( wxColour( 0, 0, 0 ) );
	
	bSizer1401->Add( m_pBlockSizeXTxt, 0, wxALIGN_CENTER_VERTICAL, 5 );
	
	m_staticText261211 = new wxStaticText( m_pVoxPanel2, wxID_ANY, wxT("Y"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText261211->Wrap( -1 );
	m_staticText261211->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	bSizer1401->Add( m_staticText261211, 0, wxALIGN_CENTER_HORIZONTAL|wxRIGHT|wxLEFT|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_pBlockSizeYTxt = new wxTextCtrl( m_pVoxPanel2, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER|wxSIMPLE_BORDER );
	m_pBlockSizeYTxt->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pBlockSizeYTxt->SetBackgroundColour( wxColour( 0, 0, 0 ) );
	
	bSizer1401->Add( m_pBlockSizeYTxt, 0, wxALIGN_CENTER_VERTICAL, 5 );
	
	m_staticText261311 = new wxStaticText( m_pVoxPanel2, wxID_ANY, wxT("Z"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText261311->Wrap( -1 );
	m_staticText261311->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	bSizer1401->Add( m_staticText261311, 0, wxALIGN_CENTER_HORIZONTAL|wxRIGHT|wxLEFT|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_pBlockSizeZTxt = new wxTextCtrl( m_pVoxPanel2, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER|wxSIMPLE_BORDER );
	m_pBlockSizeZTxt->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pBlockSizeZTxt->SetBackgroundColour( wxColour( 0, 0, 0 ) );
	
	bSizer1401->Add( m_pBlockSizeZTxt, 0, wxALIGN_CENTER_VERTICAL, 5 );
	
	bSizer1801->Add( bSizer1401, 0, wxALIGN_CENTER_HORIZONTAL, 5 );
	
	bSizer1312->Add( bSizer1801, 0, wxEXPAND, 5 );
	
	m_staticline4021 = new wxStaticLine( m_pVoxPanel2, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	bSizer1312->Add( m_staticline4021, 0, wxEXPAND|wxTOP|wxRIGHT|wxLEFT, 5 );
	
	wxBoxSizer* bSizer1791;
	bSizer1791 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText23221 = new wxStaticText( m_pVoxPanel2, wxID_ANY, wxT("Leaf Block Ordering"), wxDefaultPosition, wxSize( -1,-1 ), 0 );
	m_staticText23221->Wrap( -1 );
	m_staticText23221->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	bSizer1791->Add( m_staticText23221, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	wxArrayString m_pLeafOrderingChoiceChoices;
	m_pLeafOrderingChoice = new wxChoice( m_pVoxPanel2, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_pLeafOrderingChoiceChoices, 0 );
	m_pLeafOrderingChoice->SetSelection( 0 );
	m_pLeafOrderingChoice->SetForegroundColour( wxColour( 0, 0, 0 ) );
	m_pLeafOrderingChoice->SetBackgroundColour( wxColour( 255, 255, 255 ) );
	
	bSizer1791->Add( m_pLeafOrderingChoice, 1, wxALIGN_CENTER_VERTICAL|wxALL, 5 );
	
	bSizer1312->Add( bSizer1791, 0, wxEXPAND, 5 );
	
	m_pVoxPanel2->SetSizer( bSizer1312 );
	m_pVoxPanel2->Layout();
	bSizer1312->Fit( m_pVoxPanel2 );
	m_choicebook1->AddPage( m_pVoxPanel2, wxT("Leaf Block"), false );
	m_pVoxPanel1 = new wxPanel( m_choicebook1, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	m_pVoxPanel1->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	wxBoxSizer* bSizer1311;
	bSizer1311 = new wxBoxSizer( wxVERTICAL );
	
	m_staticText2321 = new wxStaticText( m_pVoxPanel1, wxID_ANY, wxT("Region Scope Setting"), wxDefaultPosition, wxSize( -1,-1 ), 0 );
	m_staticText2321->Wrap( -1 );
	m_staticText2321->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), 70, 90, 92, false, wxEmptyString ) );
	m_staticText2321->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	bSizer1311->Add( m_staticText2321, 0, wxALL, 5 );
	
	wxBoxSizer* bSizer143;
	bSizer143 = new wxBoxSizer( wxVERTICAL );
	
	m_pRgnScopeListCtrl = new wxListCtrl( m_pVoxPanel1, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLC_REPORT|wxLC_SINGLE_SEL );
	m_pRgnScopeListCtrl->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pRgnScopeListCtrl->SetBackgroundColour( wxColour( 0, 0, 0 ) );
	
	bSizer143->Add( m_pRgnScopeListCtrl, 1, wxALL|wxEXPAND, 5 );
	
	wxBoxSizer* bSizer144;
	bSizer144 = new wxBoxSizer( wxHORIZONTAL );
	
	m_pAddRgnBtn = new wxButton( m_pVoxPanel1, wxID_ANY, wxT("Add"), wxDefaultPosition, wxDefaultSize, 0|wxSIMPLE_BORDER );
	m_pAddRgnBtn->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pAddRgnBtn->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	bSizer144->Add( m_pAddRgnBtn, 1, wxALL, 5 );
	
	m_pDeleteRgnBtn = new wxButton( m_pVoxPanel1, wxID_ANY, wxT("Delete"), wxDefaultPosition, wxDefaultSize, 0|wxSIMPLE_BORDER );
	m_pDeleteRgnBtn->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pDeleteRgnBtn->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	bSizer144->Add( m_pDeleteRgnBtn, 1, wxALL, 5 );
	
	bSizer143->Add( bSizer144, 0, wxEXPAND|wxALIGN_RIGHT, 5 );
	
	bSizer1311->Add( bSizer143, 1, wxEXPAND, 5 );
	
	m_pVoxPanel1->SetSizer( bSizer1311 );
	m_pVoxPanel1->Layout();
	bSizer1311->Fit( m_pVoxPanel1 );
	m_choicebook1->AddPage( m_pVoxPanel1, wxT("Region Scope"), false );
	m_pVoxPanel11 = new wxPanel( m_choicebook1, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	m_pVoxPanel11->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	wxBoxSizer* bSizer13111;
	bSizer13111 = new wxBoxSizer( wxVERTICAL );
	
	m_staticText232111 = new wxStaticText( m_pVoxPanel11, wxID_ANY, wxT("Geometry Scope Setting"), wxDefaultPosition, wxSize( -1,-1 ), 0 );
	m_staticText232111->Wrap( -1 );
	m_staticText232111->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), 70, 90, 92, false, wxEmptyString ) );
	m_staticText232111->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	bSizer13111->Add( m_staticText232111, 0, wxALL, 5 );
	
	wxBoxSizer* bSizer1431;
	bSizer1431 = new wxBoxSizer( wxVERTICAL );
	
	m_pGeoScopeListCtrl = new wxListCtrl( m_pVoxPanel11, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLC_REPORT|wxLC_SINGLE_SEL );
	m_pGeoScopeListCtrl->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pGeoScopeListCtrl->SetBackgroundColour( wxColour( 0, 0, 0 ) );
	
	bSizer1431->Add( m_pGeoScopeListCtrl, 1, wxALL|wxEXPAND, 5 );
	
	wxBoxSizer* bSizer1441;
	bSizer1441 = new wxBoxSizer( wxHORIZONTAL );
	
	m_pAddGeoBtn = new wxButton( m_pVoxPanel11, wxID_ANY, wxT("Add"), wxDefaultPosition, wxDefaultSize, 0|wxSIMPLE_BORDER );
	m_pAddGeoBtn->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pAddGeoBtn->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	bSizer1441->Add( m_pAddGeoBtn, 1, wxALL, 5 );
	
	m_pDeleteGeoBtn = new wxButton( m_pVoxPanel11, wxID_ANY, wxT("Delete"), wxDefaultPosition, wxDefaultSize, 0|wxSIMPLE_BORDER );
	m_pDeleteGeoBtn->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pDeleteGeoBtn->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	bSizer1441->Add( m_pDeleteGeoBtn, 1, wxALL, 5 );
	
	bSizer1431->Add( bSizer1441, 0, wxEXPAND, 5 );
	
	bSizer13111->Add( bSizer1431, 1, wxEXPAND, 5 );
	
	m_pVoxPanel11->SetSizer( bSizer13111 );
	m_pVoxPanel11->Layout();
	bSizer13111->Fit( m_pVoxPanel11 );
	m_choicebook1->AddPage( m_pVoxPanel11, wxT("Geometry Scope"), false );
	m_pVoxPanel3 = new wxPanel( m_choicebook1, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	m_pVoxPanel3->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	wxBoxSizer* bSizer97;
	bSizer97 = new wxBoxSizer( wxVERTICAL );
	
	m_staticText100 = new wxStaticText( m_pVoxPanel3, wxID_ANY, wxT("Distance Parameter Setting"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText100->Wrap( -1 );
	m_staticText100->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), 70, 90, 92, false, wxEmptyString ) );
	m_staticText100->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	bSizer97->Add( m_staticText100, 0, wxALL, 5 );
	
	wxBoxSizer* bSizer98;
	bSizer98 = new wxBoxSizer( wxVERTICAL );
	
	wxBoxSizer* bSizer104;
	bSizer104 = new wxBoxSizer( wxHORIZONTAL );
	
	m_pUseDistanceChk = new wxCheckBox( m_pVoxPanel3, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_pUseDistanceChk->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	bSizer104->Add( m_pUseDistanceChk, 0, wxALL, 5 );
	
	m_staticText109 = new wxStaticText( m_pVoxPanel3, wxID_ANY, wxT("Divide by Distance"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText109->Wrap( -1 );
	m_staticText109->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	bSizer104->Add( m_staticText109, 0, wxALL, 5 );
	
	bSizer98->Add( bSizer104, 0, wxEXPAND, 5 );
	
	m_pMarginGrid = new wxGrid( m_pVoxPanel3, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0 );
	
	// Grid
	m_pMarginGrid->CreateGrid( 0, 2 );
	m_pMarginGrid->EnableEditing( true );
	m_pMarginGrid->EnableGridLines( true );
	m_pMarginGrid->SetGridLineColour( wxColour( 128, 128, 128 ) );
	m_pMarginGrid->EnableDragGridSize( false );
	m_pMarginGrid->SetMargins( 0, 0 );
	
	// Columns
	m_pMarginGrid->SetColSize( 0, 220 );
	m_pMarginGrid->SetColSize( 1, 130 );
	m_pMarginGrid->EnableDragColMove( false );
	m_pMarginGrid->EnableDragColSize( true );
	m_pMarginGrid->SetColLabelSize( 25 );
	m_pMarginGrid->SetColLabelValue( 0, wxT("Margin") );
	m_pMarginGrid->SetColLabelValue( 1, wxT("Level") );
	m_pMarginGrid->SetColLabelAlignment( wxALIGN_CENTRE, wxALIGN_CENTRE );
	
	// Rows
	m_pMarginGrid->EnableDragRowSize( false );
	m_pMarginGrid->SetRowLabelSize( 1 );
	m_pMarginGrid->SetRowLabelAlignment( wxALIGN_CENTRE, wxALIGN_CENTRE );
	
	// Label Appearance
	m_pMarginGrid->SetLabelBackgroundColour( wxColour( 88, 88, 88 ) );
	m_pMarginGrid->SetLabelTextColour( wxColour( 255, 255, 255 ) );
	
	// Cell Defaults
	m_pMarginGrid->SetDefaultCellBackgroundColour( wxColour( 0, 0, 0 ) );
	m_pMarginGrid->SetDefaultCellTextColour( wxColour( 255, 255, 255 ) );
	m_pMarginGrid->SetDefaultCellAlignment( wxALIGN_LEFT, wxALIGN_TOP );
	m_pMarginGrid->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pMarginGrid->SetBackgroundColour( wxColour( 0, 0, 0 ) );
	
	bSizer98->Add( m_pMarginGrid, 1, wxALL|wxEXPAND, 5 );
	
	wxBoxSizer* bSizer101;
	bSizer101 = new wxBoxSizer( wxHORIZONTAL );
	
	m_pAddMarginBtn = new wxButton( m_pVoxPanel3, wxID_ANY, wxT("Add"), wxDefaultPosition, wxDefaultSize, 0|wxSIMPLE_BORDER );
	m_pAddMarginBtn->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pAddMarginBtn->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	bSizer101->Add( m_pAddMarginBtn, 1, wxALL, 5 );
	
	m_pDeleteMarginBtn = new wxButton( m_pVoxPanel3, wxID_ANY, wxT("Delete"), wxDefaultPosition, wxDefaultSize, 0|wxSIMPLE_BORDER );
	m_pDeleteMarginBtn->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pDeleteMarginBtn->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	bSizer101->Add( m_pDeleteMarginBtn, 1, wxALL, 5 );
	
	bSizer98->Add( bSizer101, 0, wxEXPAND, 5 );
	
	bSizer97->Add( bSizer98, 1, wxEXPAND, 5 );
	
	m_pVoxPanel3->SetSizer( bSizer97 );
	m_pVoxPanel3->Layout();
	bSizer97->Fit( m_pVoxPanel3 );
	m_choicebook1->AddPage( m_pVoxPanel3, wxT("Distance Parameter"), false );
	m_pVoxPanel12 = new wxPanel( m_choicebook1, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	m_pVoxPanel12->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	wxBoxSizer* bSizer13112;
	bSizer13112 = new wxBoxSizer( wxVERTICAL );
	
	m_staticText23211 = new wxStaticText( m_pVoxPanel12, wxID_ANY, wxT("Domain Information"), wxDefaultPosition, wxSize( -1,-1 ), 0 );
	m_staticText23211->Wrap( -1 );
	m_staticText23211->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), 70, 90, 92, false, wxEmptyString ) );
	m_staticText23211->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	bSizer13112->Add( m_staticText23211, 0, wxALL, 5 );
	
	m_pInfoText = new wxTextCtrl( m_pVoxPanel12, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_MULTILINE|wxTE_READONLY|wxNO_BORDER );
	m_pInfoText->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pInfoText->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	bSizer13112->Add( m_pInfoText, 1, wxALL|wxEXPAND, 5 );
	
	m_pVoxPanel12->SetSizer( bSizer13112 );
	m_pVoxPanel12->Layout();
	bSizer13112->Fit( m_pVoxPanel12 );
	m_choicebook1->AddPage( m_pVoxPanel12, wxT("Information"), false );
	m_pVoxDvdBoxSizer->Add( m_choicebook1, 0, wxEXPAND, 5 );
	
	m_pTopSizer->Add( m_pVoxDvdBoxSizer, 0, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer58;
	bSizer58 = new wxBoxSizer( wxHORIZONTAL );
	
	m_pCreateOctreeBtn = new wxButton( this, wxID_ANY, wxT("Create Octree"), wxDefaultPosition, wxDefaultSize, 0|wxSIMPLE_BORDER );
	m_pCreateOctreeBtn->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pCreateOctreeBtn->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	bSizer58->Add( m_pCreateOctreeBtn, 1, wxEXPAND|wxTOP, 5 );
	
	m_pDeleteOctreeBtn = new wxButton( this, wxID_ANY, wxT("Delete Octree"), wxDefaultPosition, wxDefaultSize, 0|wxSIMPLE_BORDER );
	m_pDeleteOctreeBtn->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pDeleteOctreeBtn->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	bSizer58->Add( m_pDeleteOctreeBtn, 1, wxALIGN_CENTER_HORIZONTAL|wxEXPAND|wxTOP, 5 );
	
	m_pTopSizer->Add( bSizer58, 0, wxALIGN_RIGHT|wxEXPAND, 5 );
	
	m_staticText312 = new wxStaticText( this, wxID_ANY, wxT("Parallel Divid Setting"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText312->Wrap( -1 );
	m_staticText312->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), 70, 90, 92, false, wxEmptyString ) );
	m_staticText312->SetForegroundColour( wxColour( 209, 76, 16 ) );
	
	m_pTopSizer->Add( m_staticText312, 0, wxALL, 5 );
	
	m_panel21 = new wxPanel( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	m_panel21->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_panel21->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	wxBoxSizer* bSizer21;
	bSizer21 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText2212 = new wxStaticText( m_panel21, wxID_ANY, wxT("Num Divides"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText2212->Wrap( -1 );
	m_staticText2212->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	bSizer21->Add( m_staticText2212, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_pParallelDivTxt = new wxTextCtrl( m_panel21, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER|wxSIMPLE_BORDER );
	m_pParallelDivTxt->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pParallelDivTxt->SetBackgroundColour( wxColour( 0, 0, 0 ) );
	
	bSizer21->Add( m_pParallelDivTxt, 0, wxALL, 5 );
	
	m_panel21->SetSizer( bSizer21 );
	m_panel21->Layout();
	bSizer21->Fit( m_panel21 );
	m_pTopSizer->Add( m_panel21, 0, wxEXPAND, 5 );
	
	m_pBaseSizer->Add( m_pTopSizer, 0, wxALL|wxEXPAND, 4 );
	
	m_staticline1 = new wxStaticLine( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	m_pBaseSizer->Add( m_staticline1, 0, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer7;
	bSizer7 = new wxBoxSizer( wxHORIZONTAL );
	
	m_pOKBtn = new wxButton( this, wxID_ANY, wxT("OK"), wxDefaultPosition, wxDefaultSize, 0|wxSIMPLE_BORDER );
	m_pOKBtn->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pOKBtn->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	bSizer7->Add( m_pOKBtn, 0, wxALL, 5 );
	
	m_pCancelBtn = new wxButton( this, wxID_ANY, wxT("Cancel"), wxDefaultPosition, wxDefaultSize, 0|wxSIMPLE_BORDER );
	m_pCancelBtn->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pCancelBtn->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	bSizer7->Add( m_pCancelBtn, 0, wxALL, 5 );
	
	m_pBaseSizer->Add( bSizer7, 0, wxALIGN_RIGHT, 5 );
	
	this->SetSizer( m_pBaseSizer );
	this->Layout();
	m_pBaseSizer->Fit( this );
	
	this->Centre( wxBOTH );
	
	// Connect Events
	this->Connect( wxEVT_CLOSE_WINDOW, wxCloseEventHandler( DomainBCMDlgBase::OnClose ) );
	m_pPolicySizeRBtn->Connect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( DomainBCMDlgBase::OnPolicyRadio ), NULL, this );
	m_pPolicyMaxRBtn->Connect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( DomainBCMDlgBase::OnPolicyRadio ), NULL, this );
	m_pMinXTxt->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( DomainBCMDlgBase::OnBBoxTxtOnKillFocus ), NULL, this );
	m_pMinXTxt->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( DomainBCMDlgBase::OnBBoxTxt ), NULL, this );
	m_pMinYTxt->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( DomainBCMDlgBase::OnBBoxTxtOnKillFocus ), NULL, this );
	m_pMinYTxt->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( DomainBCMDlgBase::OnBBoxTxt ), NULL, this );
	m_pMinZTxt->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( DomainBCMDlgBase::OnBBoxTxtOnKillFocus ), NULL, this );
	m_pMinZTxt->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( DomainBCMDlgBase::OnBBoxTxt ), NULL, this );
	m_pMaxXTxt->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( DomainBCMDlgBase::OnBBoxTxtOnKillFocus ), NULL, this );
	m_pMaxXTxt->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( DomainBCMDlgBase::OnBBoxTxt ), NULL, this );
	m_pMaxYTxt->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( DomainBCMDlgBase::OnBBoxTxtOnKillFocus ), NULL, this );
	m_pMaxYTxt->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( DomainBCMDlgBase::OnBBoxTxt ), NULL, this );
	m_pMaxZTxt->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( DomainBCMDlgBase::OnBBoxTxtOnKillFocus ), NULL, this );
	m_pMaxZTxt->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( DomainBCMDlgBase::OnBBoxTxt ), NULL, this );
	m_pSizeXTxt->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( DomainBCMDlgBase::OnBBoxTxtOnKillFocus ), NULL, this );
	m_pSizeXTxt->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( DomainBCMDlgBase::OnBBoxTxt ), NULL, this );
	m_pSizeYTxt->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( DomainBCMDlgBase::OnBBoxTxtOnKillFocus ), NULL, this );
	m_pSizeYTxt->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( DomainBCMDlgBase::OnBBoxTxt ), NULL, this );
	m_pSizeZTxt->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( DomainBCMDlgBase::OnBBoxTxtOnKillFocus ), NULL, this );
	m_pSizeZTxt->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( DomainBCMDlgBase::OnBBoxTxt ), NULL, this );
	m_pCenterXChk->Connect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( DomainBCMDlgBase::OnCenterChk ), NULL, this );
	m_pCenterYChk->Connect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( DomainBCMDlgBase::OnCenterChk ), NULL, this );
	m_pCenterZChk->Connect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( DomainBCMDlgBase::OnCenterChk ), NULL, this );
	m_pUnitChoice->Connect( wxEVT_COMMAND_CHOICE_SELECTED, wxCommandEventHandler( DomainBCMDlgBase::OnUnitChoice ), NULL, this );
	m_pDefaultBtn->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( DomainBCMDlgBase::OnDefaultBtn ), NULL, this );
	m_pUnifChk->Connect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( DomainBCMDlgBase::OnForceUnifChk ), NULL, this );
	m_pRootLengthXTxt->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( DomainBCMDlgBase::OnRootLengthTxtOnKillFocus ), NULL, this );
	m_pRootLengthXTxt->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( DomainBCMDlgBase::OnRootLengthTxt ), NULL, this );
	m_pRootLengthYTxt->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( DomainBCMDlgBase::OnRootLengthTxtOnKillFocus ), NULL, this );
	m_pRootLengthYTxt->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( DomainBCMDlgBase::OnRootLengthTxt ), NULL, this );
	m_pRootLengthZTxt->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( DomainBCMDlgBase::OnRootLengthTxtOnKillFocus ), NULL, this );
	m_pRootLengthZTxt->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( DomainBCMDlgBase::OnRootLengthTxt ), NULL, this );
	m_pRootDimsXTxt->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( DomainBCMDlgBase::OnRootDimsTxtOnKillFocus ), NULL, this );
	m_pRootDimsXTxt->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( DomainBCMDlgBase::OnRootDimsTxt ), NULL, this );
	m_pRootDimsYTxt->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( DomainBCMDlgBase::OnRootDimsTxtOnKillFocus ), NULL, this );
	m_pRootDimsYTxt->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( DomainBCMDlgBase::OnRootDimsTxt ), NULL, this );
	m_pRootDimsZTxt->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( DomainBCMDlgBase::OnRootDimsTxtOnKillFocus ), NULL, this );
	m_pRootDimsZTxt->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( DomainBCMDlgBase::OnRootDimsTxt ), NULL, this );
	m_pBaseLevelTxt->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( DomainBCMDlgBase::OnLevelTxtOnKillFocus ), NULL, this );
	m_pBaseLevelTxt->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( DomainBCMDlgBase::OnLevelTxt ), NULL, this );
	m_pMinLevelTxt->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( DomainBCMDlgBase::OnLevelTxtOnKillFocus ), NULL, this );
	m_pMinLevelTxt->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( DomainBCMDlgBase::OnLevelTxt ), NULL, this );
	m_pExpPolicyParamBtn->Connect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( DomainBCMDlgBase::OnExportPolicyRadio ), NULL, this );
	m_pExpPolicyOctBtn->Connect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( DomainBCMDlgBase::OnExportPolicyRadio ), NULL, this );
	m_pBlockSizeXTxt->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( DomainBCMDlgBase::OnBlockSizeTxt ), NULL, this );
	m_pBlockSizeYTxt->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( DomainBCMDlgBase::OnBlockSizeTxt ), NULL, this );
	m_pBlockSizeZTxt->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( DomainBCMDlgBase::OnBlockSizeTxt ), NULL, this );
	m_pLeafOrderingChoice->Connect( wxEVT_COMMAND_CHOICE_SELECTED, wxCommandEventHandler( DomainBCMDlgBase::OnLeafOrderingChoice ), NULL, this );
	m_pAddRgnBtn->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( DomainBCMDlgBase::OnAddRgnBtn ), NULL, this );
	m_pDeleteRgnBtn->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( DomainBCMDlgBase::OnDeleteRgnBtn ), NULL, this );
	m_pAddGeoBtn->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( DomainBCMDlgBase::OnAddGeoBtn ), NULL, this );
	m_pDeleteGeoBtn->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( DomainBCMDlgBase::OnDeleteGeoBtn ), NULL, this );
	m_pUseDistanceChk->Connect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( DomainBCMDlgBase::OnUseDistanceChk ), NULL, this );
	m_pMarginGrid->Connect( wxEVT_GRID_CELL_CHANGE, wxGridEventHandler( DomainBCMDlgBase::OnChangeMargin ), NULL, this );
	m_pAddMarginBtn->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( DomainBCMDlgBase::OnAddMarginBtn ), NULL, this );
	m_pDeleteMarginBtn->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( DomainBCMDlgBase::OnDeleteMarginBtn ), NULL, this );
	m_pCreateOctreeBtn->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( DomainBCMDlgBase::OnCreateOctreeBtn ), NULL, this );
	m_pDeleteOctreeBtn->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( DomainBCMDlgBase::OnDeleteOctreeBtn ), NULL, this );
	m_pParallelDivTxt->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( DomainBCMDlgBase::OnParallelDivTxtOnKillFocus ), NULL, this );
	m_pParallelDivTxt->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( DomainBCMDlgBase::OnParallelDivTxt ), NULL, this );
	m_pOKBtn->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( DomainBCMDlgBase::OnOKBtn ), NULL, this );
	m_pCancelBtn->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( DomainBCMDlgBase::OnCancelBtn ), NULL, this );
}

DomainBCMDlgBase::~DomainBCMDlgBase()
{
	// Disconnect Events
	this->Disconnect( wxEVT_CLOSE_WINDOW, wxCloseEventHandler( DomainBCMDlgBase::OnClose ) );
	m_pPolicySizeRBtn->Disconnect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( DomainBCMDlgBase::OnPolicyRadio ), NULL, this );
	m_pPolicyMaxRBtn->Disconnect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( DomainBCMDlgBase::OnPolicyRadio ), NULL, this );
	m_pMinXTxt->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( DomainBCMDlgBase::OnBBoxTxtOnKillFocus ), NULL, this );
	m_pMinXTxt->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( DomainBCMDlgBase::OnBBoxTxt ), NULL, this );
	m_pMinYTxt->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( DomainBCMDlgBase::OnBBoxTxtOnKillFocus ), NULL, this );
	m_pMinYTxt->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( DomainBCMDlgBase::OnBBoxTxt ), NULL, this );
	m_pMinZTxt->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( DomainBCMDlgBase::OnBBoxTxtOnKillFocus ), NULL, this );
	m_pMinZTxt->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( DomainBCMDlgBase::OnBBoxTxt ), NULL, this );
	m_pMaxXTxt->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( DomainBCMDlgBase::OnBBoxTxtOnKillFocus ), NULL, this );
	m_pMaxXTxt->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( DomainBCMDlgBase::OnBBoxTxt ), NULL, this );
	m_pMaxYTxt->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( DomainBCMDlgBase::OnBBoxTxtOnKillFocus ), NULL, this );
	m_pMaxYTxt->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( DomainBCMDlgBase::OnBBoxTxt ), NULL, this );
	m_pMaxZTxt->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( DomainBCMDlgBase::OnBBoxTxtOnKillFocus ), NULL, this );
	m_pMaxZTxt->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( DomainBCMDlgBase::OnBBoxTxt ), NULL, this );
	m_pSizeXTxt->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( DomainBCMDlgBase::OnBBoxTxtOnKillFocus ), NULL, this );
	m_pSizeXTxt->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( DomainBCMDlgBase::OnBBoxTxt ), NULL, this );
	m_pSizeYTxt->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( DomainBCMDlgBase::OnBBoxTxtOnKillFocus ), NULL, this );
	m_pSizeYTxt->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( DomainBCMDlgBase::OnBBoxTxt ), NULL, this );
	m_pSizeZTxt->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( DomainBCMDlgBase::OnBBoxTxtOnKillFocus ), NULL, this );
	m_pSizeZTxt->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( DomainBCMDlgBase::OnBBoxTxt ), NULL, this );
	m_pCenterXChk->Disconnect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( DomainBCMDlgBase::OnCenterChk ), NULL, this );
	m_pCenterYChk->Disconnect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( DomainBCMDlgBase::OnCenterChk ), NULL, this );
	m_pCenterZChk->Disconnect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( DomainBCMDlgBase::OnCenterChk ), NULL, this );
	m_pUnitChoice->Disconnect( wxEVT_COMMAND_CHOICE_SELECTED, wxCommandEventHandler( DomainBCMDlgBase::OnUnitChoice ), NULL, this );
	m_pDefaultBtn->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( DomainBCMDlgBase::OnDefaultBtn ), NULL, this );
	m_pUnifChk->Disconnect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( DomainBCMDlgBase::OnForceUnifChk ), NULL, this );
	m_pRootLengthXTxt->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( DomainBCMDlgBase::OnRootLengthTxtOnKillFocus ), NULL, this );
	m_pRootLengthXTxt->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( DomainBCMDlgBase::OnRootLengthTxt ), NULL, this );
	m_pRootLengthYTxt->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( DomainBCMDlgBase::OnRootLengthTxtOnKillFocus ), NULL, this );
	m_pRootLengthYTxt->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( DomainBCMDlgBase::OnRootLengthTxt ), NULL, this );
	m_pRootLengthZTxt->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( DomainBCMDlgBase::OnRootLengthTxtOnKillFocus ), NULL, this );
	m_pRootLengthZTxt->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( DomainBCMDlgBase::OnRootLengthTxt ), NULL, this );
	m_pRootDimsXTxt->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( DomainBCMDlgBase::OnRootDimsTxtOnKillFocus ), NULL, this );
	m_pRootDimsXTxt->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( DomainBCMDlgBase::OnRootDimsTxt ), NULL, this );
	m_pRootDimsYTxt->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( DomainBCMDlgBase::OnRootDimsTxtOnKillFocus ), NULL, this );
	m_pRootDimsYTxt->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( DomainBCMDlgBase::OnRootDimsTxt ), NULL, this );
	m_pRootDimsZTxt->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( DomainBCMDlgBase::OnRootDimsTxtOnKillFocus ), NULL, this );
	m_pRootDimsZTxt->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( DomainBCMDlgBase::OnRootDimsTxt ), NULL, this );
	m_pBaseLevelTxt->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( DomainBCMDlgBase::OnLevelTxtOnKillFocus ), NULL, this );
	m_pBaseLevelTxt->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( DomainBCMDlgBase::OnLevelTxt ), NULL, this );
	m_pMinLevelTxt->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( DomainBCMDlgBase::OnLevelTxtOnKillFocus ), NULL, this );
	m_pMinLevelTxt->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( DomainBCMDlgBase::OnLevelTxt ), NULL, this );
	m_pExpPolicyParamBtn->Disconnect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( DomainBCMDlgBase::OnExportPolicyRadio ), NULL, this );
	m_pExpPolicyOctBtn->Disconnect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( DomainBCMDlgBase::OnExportPolicyRadio ), NULL, this );
	m_pBlockSizeXTxt->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( DomainBCMDlgBase::OnBlockSizeTxt ), NULL, this );
	m_pBlockSizeYTxt->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( DomainBCMDlgBase::OnBlockSizeTxt ), NULL, this );
	m_pBlockSizeZTxt->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( DomainBCMDlgBase::OnBlockSizeTxt ), NULL, this );
	m_pLeafOrderingChoice->Disconnect( wxEVT_COMMAND_CHOICE_SELECTED, wxCommandEventHandler( DomainBCMDlgBase::OnLeafOrderingChoice ), NULL, this );
	m_pAddRgnBtn->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( DomainBCMDlgBase::OnAddRgnBtn ), NULL, this );
	m_pDeleteRgnBtn->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( DomainBCMDlgBase::OnDeleteRgnBtn ), NULL, this );
	m_pAddGeoBtn->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( DomainBCMDlgBase::OnAddGeoBtn ), NULL, this );
	m_pDeleteGeoBtn->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( DomainBCMDlgBase::OnDeleteGeoBtn ), NULL, this );
	m_pUseDistanceChk->Disconnect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( DomainBCMDlgBase::OnUseDistanceChk ), NULL, this );
	m_pMarginGrid->Disconnect( wxEVT_GRID_CELL_CHANGE, wxGridEventHandler( DomainBCMDlgBase::OnChangeMargin ), NULL, this );
	m_pAddMarginBtn->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( DomainBCMDlgBase::OnAddMarginBtn ), NULL, this );
	m_pDeleteMarginBtn->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( DomainBCMDlgBase::OnDeleteMarginBtn ), NULL, this );
	m_pCreateOctreeBtn->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( DomainBCMDlgBase::OnCreateOctreeBtn ), NULL, this );
	m_pDeleteOctreeBtn->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( DomainBCMDlgBase::OnDeleteOctreeBtn ), NULL, this );
	m_pParallelDivTxt->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( DomainBCMDlgBase::OnParallelDivTxtOnKillFocus ), NULL, this );
	m_pParallelDivTxt->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( DomainBCMDlgBase::OnParallelDivTxt ), NULL, this );
	m_pOKBtn->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( DomainBCMDlgBase::OnOKBtn ), NULL, this );
	m_pCancelBtn->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( DomainBCMDlgBase::OnCancelBtn ), NULL, this );
	
}

RegionScopeDlgBase::RegionScopeDlgBase( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );
	this->SetBackgroundColour( wxColour( 44, 44, 44 ) );
	
	m_pBaseSizer = new wxBoxSizer( wxVERTICAL );
	
	m_pTopSizer = new wxBoxSizer( wxVERTICAL );
	
	m_staticText31 = new wxStaticText( this, wxID_ANY, wxT("Region Setting"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText31->Wrap( -1 );
	m_staticText31->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), 70, 90, 92, false, wxEmptyString ) );
	m_staticText31->SetForegroundColour( wxColour( 209, 76, 16 ) );
	
	m_pTopSizer->Add( m_staticText31, 0, wxALL, 2 );
	
	m_panel2 = new wxPanel( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	m_panel2->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_panel2->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	wxBoxSizer* bSizer2;
	bSizer2 = new wxBoxSizer( wxVERTICAL );
	
	wxBoxSizer* bSizer3;
	bSizer3 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText2 = new wxStaticText( m_panel2, wxID_ANY, wxT("Region Setting Policy"), wxDefaultPosition, wxSize( -1,-1 ), 0 );
	m_staticText2->Wrap( -1 );
	m_staticText2->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	bSizer3->Add( m_staticText2, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );
	
	m_staticline132 = new wxStaticLine( m_panel2, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_VERTICAL );
	bSizer3->Add( m_staticline132, 0, wxEXPAND|wxALL, 5 );
	
	m_pPolicySizeRBtn = new wxRadioButton( m_panel2, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxRB_GROUP );
	bSizer3->Add( m_pPolicySizeRBtn, 0, wxALIGN_CENTER_VERTICAL|wxRIGHT|wxLEFT, 5 );
	
	m_staticText21 = new wxStaticText( m_panel2, wxID_ANY, wxT("Origin / Size"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText21->Wrap( -1 );
	m_staticText21->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	bSizer3->Add( m_staticText21, 0, wxALIGN_CENTER_VERTICAL|wxRIGHT, 5 );
	
	m_pPolicyMaxRBtn = new wxRadioButton( m_panel2, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizer3->Add( m_pPolicyMaxRBtn, 0, wxALIGN_CENTER_VERTICAL|wxRIGHT|wxLEFT, 5 );
	
	m_staticText211 = new wxStaticText( m_panel2, wxID_ANY, wxT("Origin / Max"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText211->Wrap( -1 );
	m_staticText211->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	bSizer3->Add( m_staticText211, 0, wxALIGN_CENTER_VERTICAL|wxRIGHT, 5 );
	
	bSizer2->Add( bSizer3, 0, wxEXPAND, 5 );
	
	wxFlexGridSizer* fgSizer1;
	fgSizer1 = new wxFlexGridSizer( 4, 4, 0, 0 );
	fgSizer1->SetFlexibleDirection( wxBOTH );
	fgSizer1->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );
	
	
	fgSizer1->Add( 0, 0, 1, wxEXPAND, 5 );
	
	m_staticText3 = new wxStaticText( m_panel2, wxID_ANY, wxT("X"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText3->Wrap( -1 );
	m_staticText3->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	fgSizer1->Add( m_staticText3, 0, wxALIGN_CENTER_HORIZONTAL|wxTOP|wxRIGHT|wxLEFT, 5 );
	
	m_staticText4 = new wxStaticText( m_panel2, wxID_ANY, wxT("Y"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText4->Wrap( -1 );
	m_staticText4->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	fgSizer1->Add( m_staticText4, 0, wxALIGN_CENTER_HORIZONTAL|wxTOP|wxRIGHT|wxLEFT, 5 );
	
	m_staticText41 = new wxStaticText( m_panel2, wxID_ANY, wxT("Z"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText41->Wrap( -1 );
	m_staticText41->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	fgSizer1->Add( m_staticText41, 0, wxALIGN_CENTER_HORIZONTAL|wxTOP|wxRIGHT|wxLEFT, 5 );
	
	m_staticText42 = new wxStaticText( m_panel2, wxID_ANY, wxT("Origin"), wxDefaultPosition, wxSize( 55,-1 ), 0 );
	m_staticText42->Wrap( -1 );
	m_staticText42->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	fgSizer1->Add( m_staticText42, 0, wxALIGN_CENTER_VERTICAL|wxALIGN_RIGHT|wxRIGHT|wxLEFT, 5 );
	
	m_pMinXTxt = new wxTextCtrl( m_panel2, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER|wxSIMPLE_BORDER );
	m_pMinXTxt->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pMinXTxt->SetBackgroundColour( wxColour( 0, 0, 0 ) );
	
	fgSizer1->Add( m_pMinXTxt, 0, 0, 5 );
	
	m_pMinYTxt = new wxTextCtrl( m_panel2, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER|wxSIMPLE_BORDER );
	m_pMinYTxt->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pMinYTxt->SetBackgroundColour( wxColour( 0, 0, 0 ) );
	
	fgSizer1->Add( m_pMinYTxt, 0, 0, 5 );
	
	m_pMinZTxt = new wxTextCtrl( m_panel2, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER|wxSIMPLE_BORDER );
	m_pMinZTxt->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pMinZTxt->SetBackgroundColour( wxColour( 0, 0, 0 ) );
	
	fgSizer1->Add( m_pMinZTxt, 0, 0, 5 );
	
	m_staticText7 = new wxStaticText( m_panel2, wxID_ANY, wxT("Max"), wxDefaultPosition, wxSize( 55,-1 ), 0 );
	m_staticText7->Wrap( -1 );
	m_staticText7->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	fgSizer1->Add( m_staticText7, 0, wxALIGN_CENTER_VERTICAL|wxALIGN_RIGHT|wxRIGHT|wxLEFT, 5 );
	
	m_pMaxXTxt = new wxTextCtrl( m_panel2, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER|wxSIMPLE_BORDER );
	m_pMaxXTxt->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pMaxXTxt->SetBackgroundColour( wxColour( 0, 0, 0 ) );
	
	fgSizer1->Add( m_pMaxXTxt, 0, 0, 5 );
	
	m_pMaxYTxt = new wxTextCtrl( m_panel2, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER|wxSIMPLE_BORDER );
	m_pMaxYTxt->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pMaxYTxt->SetBackgroundColour( wxColour( 0, 0, 0 ) );
	
	fgSizer1->Add( m_pMaxYTxt, 0, 0, 5 );
	
	m_pMaxZTxt = new wxTextCtrl( m_panel2, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER|wxSIMPLE_BORDER );
	m_pMaxZTxt->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pMaxZTxt->SetBackgroundColour( wxColour( 0, 0, 0 ) );
	
	fgSizer1->Add( m_pMaxZTxt, 0, 0, 5 );
	
	m_staticText11 = new wxStaticText( m_panel2, wxID_ANY, wxT("Size"), wxDefaultPosition, wxSize( 55,-1 ), 0 );
	m_staticText11->Wrap( -1 );
	m_staticText11->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	fgSizer1->Add( m_staticText11, 0, wxALIGN_CENTER_VERTICAL|wxALIGN_RIGHT|wxRIGHT|wxLEFT, 5 );
	
	m_pSizeXTxt = new wxTextCtrl( m_panel2, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER|wxSIMPLE_BORDER );
	m_pSizeXTxt->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pSizeXTxt->SetBackgroundColour( wxColour( 0, 0, 0 ) );
	
	fgSizer1->Add( m_pSizeXTxt, 0, 0, 5 );
	
	m_pSizeYTxt = new wxTextCtrl( m_panel2, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER|wxSIMPLE_BORDER );
	m_pSizeYTxt->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pSizeYTxt->SetBackgroundColour( wxColour( 0, 0, 0 ) );
	
	fgSizer1->Add( m_pSizeYTxt, 0, 0, 5 );
	
	m_pSizeZTxt = new wxTextCtrl( m_panel2, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER|wxSIMPLE_BORDER );
	m_pSizeZTxt->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pSizeZTxt->SetBackgroundColour( wxColour( 0, 0, 0 ) );
	
	fgSizer1->Add( m_pSizeZTxt, 0, 0, 5 );
	
	bSizer2->Add( fgSizer1, 0, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer31;
	bSizer31 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText22 = new wxStaticText( m_panel2, wxID_ANY, wxT("Level"), wxDefaultPosition, wxSize( 55,-1 ), 0 );
	m_staticText22->Wrap( -1 );
	m_staticText22->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	bSizer31->Add( m_staticText22, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );
	
	m_pLevelTxt = new wxTextCtrl( m_panel2, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER|wxSIMPLE_BORDER );
	m_pLevelTxt->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pLevelTxt->SetBackgroundColour( wxColour( 0, 0, 0 ) );
	
	bSizer31->Add( m_pLevelTxt, 0, 0, 5 );
	
	m_staticText184 = new wxStaticText( m_panel2, wxID_ANY, wxT("0 < Level < 16"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText184->Wrap( -1 );
	bSizer31->Add( m_staticText184, 0, wxALL, 5 );
	
	bSizer2->Add( bSizer31, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer32;
	bSizer32 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText23 = new wxStaticText( m_panel2, wxID_ANY, wxT("Margin Setting Mode"), wxDefaultPosition, wxSize( -1,-1 ), 0 );
	m_staticText23->Wrap( -1 );
	m_staticText23->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	bSizer32->Add( m_staticText23, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );
	
	m_staticline1321 = new wxStaticLine( m_panel2, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_VERTICAL );
	bSizer32->Add( m_staticline1321, 0, wxEXPAND|wxALL, 5 );
	
	m_pMarginCellRBtn = new wxRadioButton( m_panel2, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxRB_GROUP );
	bSizer32->Add( m_pMarginCellRBtn, 0, wxALIGN_CENTER_VERTICAL|wxRIGHT|wxLEFT, 5 );
	
	m_staticText212 = new wxStaticText( m_panel2, wxID_ANY, wxT("Cell"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText212->Wrap( -1 );
	m_staticText212->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	bSizer32->Add( m_staticText212, 0, wxALIGN_CENTER_VERTICAL|wxRIGHT, 5 );
	
	m_pMarginRatioRBtn = new wxRadioButton( m_panel2, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizer32->Add( m_pMarginRatioRBtn, 0, wxALIGN_CENTER_VERTICAL|wxRIGHT|wxLEFT, 5 );
	
	m_staticText2111 = new wxStaticText( m_panel2, wxID_ANY, wxT("Ratio"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText2111->Wrap( -1 );
	m_staticText2111->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	bSizer32->Add( m_staticText2111, 0, wxALIGN_CENTER_VERTICAL|wxRIGHT, 5 );
	
	bSizer2->Add( bSizer32, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer311;
	bSizer311 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText221 = new wxStaticText( m_panel2, wxID_ANY, wxT("Margin"), wxDefaultPosition, wxSize( 55,-1 ), 0 );
	m_staticText221->Wrap( -1 );
	m_staticText221->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	bSizer311->Add( m_staticText221, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );
	
	m_pMarginTxt = new wxTextCtrl( m_panel2, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER|wxSIMPLE_BORDER );
	m_pMarginTxt->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pMarginTxt->SetBackgroundColour( wxColour( 0, 0, 0 ) );
	
	bSizer311->Add( m_pMarginTxt, 0, 0, 5 );
	
	bSizer2->Add( bSizer311, 1, wxEXPAND, 5 );
	
	m_panel2->SetSizer( bSizer2 );
	m_panel2->Layout();
	bSizer2->Fit( m_panel2 );
	m_pTopSizer->Add( m_panel2, 1, wxEXPAND | wxALL, 0 );
	
	m_pBaseSizer->Add( m_pTopSizer, 0, wxEXPAND|wxALL, 4 );
	
	m_staticline33 = new wxStaticLine( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	m_pBaseSizer->Add( m_staticline33, 0, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer7;
	bSizer7 = new wxBoxSizer( wxHORIZONTAL );
	
	m_pOKBtn = new wxButton( this, wxID_ANY, wxT("OK"), wxDefaultPosition, wxDefaultSize, 0|wxSIMPLE_BORDER );
	m_pOKBtn->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pOKBtn->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	bSizer7->Add( m_pOKBtn, 0, wxALL, 5 );
	
	m_pCancelBtn = new wxButton( this, wxID_ANY, wxT("Cancel"), wxDefaultPosition, wxDefaultSize, 0|wxSIMPLE_BORDER );
	m_pCancelBtn->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pCancelBtn->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	bSizer7->Add( m_pCancelBtn, 0, wxALL, 5 );
	
	m_pBaseSizer->Add( bSizer7, 0, wxALIGN_RIGHT, 5 );
	
	this->SetSizer( m_pBaseSizer );
	this->Layout();
	m_pBaseSizer->Fit( this );
	
	this->Centre( wxBOTH );
	
	// Connect Events
	this->Connect( wxEVT_CLOSE_WINDOW, wxCloseEventHandler( RegionScopeDlgBase::OnClose ) );
	m_pPolicySizeRBtn->Connect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( RegionScopeDlgBase::OnPolicyRadio ), NULL, this );
	m_pPolicyMaxRBtn->Connect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( RegionScopeDlgBase::OnPolicyRadio ), NULL, this );
	m_pMinXTxt->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( RegionScopeDlgBase::OnBBoxTxt ), NULL, this );
	m_pMinYTxt->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( RegionScopeDlgBase::OnBBoxTxt ), NULL, this );
	m_pMinZTxt->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( RegionScopeDlgBase::OnBBoxTxt ), NULL, this );
	m_pMaxXTxt->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( RegionScopeDlgBase::OnBBoxTxt ), NULL, this );
	m_pMaxYTxt->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( RegionScopeDlgBase::OnBBoxTxt ), NULL, this );
	m_pMaxZTxt->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( RegionScopeDlgBase::OnBBoxTxt ), NULL, this );
	m_pSizeXTxt->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( RegionScopeDlgBase::OnBBoxTxt ), NULL, this );
	m_pSizeYTxt->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( RegionScopeDlgBase::OnBBoxTxt ), NULL, this );
	m_pSizeZTxt->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( RegionScopeDlgBase::OnBBoxTxt ), NULL, this );
	m_pLevelTxt->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( RegionScopeDlgBase::OnLevelTxt ), NULL, this );
	m_pMarginCellRBtn->Connect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( RegionScopeDlgBase::OnMarginRadio ), NULL, this );
	m_pMarginRatioRBtn->Connect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( RegionScopeDlgBase::OnMarginRadio ), NULL, this );
	m_pMarginTxt->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( RegionScopeDlgBase::OnMarginTxt ), NULL, this );
	m_pOKBtn->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( RegionScopeDlgBase::OnOKBtn ), NULL, this );
	m_pCancelBtn->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( RegionScopeDlgBase::OnCancelBtn ), NULL, this );
}

RegionScopeDlgBase::~RegionScopeDlgBase()
{
	// Disconnect Events
	this->Disconnect( wxEVT_CLOSE_WINDOW, wxCloseEventHandler( RegionScopeDlgBase::OnClose ) );
	m_pPolicySizeRBtn->Disconnect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( RegionScopeDlgBase::OnPolicyRadio ), NULL, this );
	m_pPolicyMaxRBtn->Disconnect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( RegionScopeDlgBase::OnPolicyRadio ), NULL, this );
	m_pMinXTxt->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( RegionScopeDlgBase::OnBBoxTxt ), NULL, this );
	m_pMinYTxt->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( RegionScopeDlgBase::OnBBoxTxt ), NULL, this );
	m_pMinZTxt->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( RegionScopeDlgBase::OnBBoxTxt ), NULL, this );
	m_pMaxXTxt->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( RegionScopeDlgBase::OnBBoxTxt ), NULL, this );
	m_pMaxYTxt->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( RegionScopeDlgBase::OnBBoxTxt ), NULL, this );
	m_pMaxZTxt->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( RegionScopeDlgBase::OnBBoxTxt ), NULL, this );
	m_pSizeXTxt->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( RegionScopeDlgBase::OnBBoxTxt ), NULL, this );
	m_pSizeYTxt->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( RegionScopeDlgBase::OnBBoxTxt ), NULL, this );
	m_pSizeZTxt->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( RegionScopeDlgBase::OnBBoxTxt ), NULL, this );
	m_pLevelTxt->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( RegionScopeDlgBase::OnLevelTxt ), NULL, this );
	m_pMarginCellRBtn->Disconnect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( RegionScopeDlgBase::OnMarginRadio ), NULL, this );
	m_pMarginRatioRBtn->Disconnect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( RegionScopeDlgBase::OnMarginRadio ), NULL, this );
	m_pMarginTxt->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( RegionScopeDlgBase::OnMarginTxt ), NULL, this );
	m_pOKBtn->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( RegionScopeDlgBase::OnOKBtn ), NULL, this );
	m_pCancelBtn->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( RegionScopeDlgBase::OnCancelBtn ), NULL, this );
	
}

GeometryScopeDlgBase::GeometryScopeDlgBase( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );
	this->SetBackgroundColour( wxColour( 44, 44, 44 ) );
	
	m_pBaseSizer = new wxBoxSizer( wxVERTICAL );
	
	m_pTopSizer = new wxBoxSizer( wxVERTICAL );
	
	m_staticText31 = new wxStaticText( this, wxID_ANY, wxT("Geometry Setting"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText31->Wrap( -1 );
	m_staticText31->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), 70, 90, 92, false, wxEmptyString ) );
	m_staticText31->SetForegroundColour( wxColour( 209, 76, 16 ) );
	
	m_pTopSizer->Add( m_staticText31, 0, wxALL, 2 );
	
	m_panel2 = new wxPanel( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	m_panel2->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_panel2->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	wxBoxSizer* bSizer2;
	bSizer2 = new wxBoxSizer( wxVERTICAL );
	
	wxBoxSizer* bSizer239;
	bSizer239 = new wxBoxSizer( wxVERTICAL );
	
	m_pTreeCtrl = new wxTreeCtrl( m_panel2, wxID_ANY, wxDefaultPosition, wxSize( 300,250 ), wxTR_DEFAULT_STYLE|wxTR_SINGLE );
	m_pTreeCtrl->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pTreeCtrl->SetBackgroundColour( wxColour( 0, 0, 0 ) );
	
	bSizer239->Add( m_pTreeCtrl, 1, wxEXPAND|wxALL, 5 );
	
	bSizer2->Add( bSizer239, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer31;
	bSizer31 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText22 = new wxStaticText( m_panel2, wxID_ANY, wxT("Level"), wxDefaultPosition, wxSize( 55,-1 ), 0 );
	m_staticText22->Wrap( -1 );
	m_staticText22->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	bSizer31->Add( m_staticText22, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );
	
	m_pLevelTxt = new wxTextCtrl( m_panel2, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER|wxSIMPLE_BORDER );
	m_pLevelTxt->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pLevelTxt->SetBackgroundColour( wxColour( 0, 0, 0 ) );
	
	bSizer31->Add( m_pLevelTxt, 0, 0, 5 );
	
	m_staticText183 = new wxStaticText( m_panel2, wxID_ANY, wxT("0 < Level < 16"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText183->Wrap( -1 );
	bSizer31->Add( m_staticText183, 0, wxALL, 5 );
	
	bSizer2->Add( bSizer31, 0, 0, 5 );
	
	wxBoxSizer* bSizer32;
	bSizer32 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText23 = new wxStaticText( m_panel2, wxID_ANY, wxT("Margin Setting Mode"), wxDefaultPosition, wxSize( -1,-1 ), 0 );
	m_staticText23->Wrap( -1 );
	m_staticText23->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	bSizer32->Add( m_staticText23, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );
	
	m_staticline1321 = new wxStaticLine( m_panel2, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_VERTICAL );
	bSizer32->Add( m_staticline1321, 0, wxEXPAND|wxALL, 5 );
	
	m_pGeoMarginCellRBtn = new wxRadioButton( m_panel2, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxRB_GROUP );
	bSizer32->Add( m_pGeoMarginCellRBtn, 0, wxALIGN_CENTER_VERTICAL|wxRIGHT|wxLEFT, 5 );
	
	m_staticText212 = new wxStaticText( m_panel2, wxID_ANY, wxT("Cell"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText212->Wrap( -1 );
	m_staticText212->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	bSizer32->Add( m_staticText212, 0, wxALIGN_CENTER_VERTICAL|wxRIGHT, 5 );
	
	m_pGeoMarginRatioRBtn = new wxRadioButton( m_panel2, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizer32->Add( m_pGeoMarginRatioRBtn, 0, wxALIGN_CENTER_VERTICAL|wxRIGHT|wxLEFT, 5 );
	
	m_staticText2111 = new wxStaticText( m_panel2, wxID_ANY, wxT("Ratio"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText2111->Wrap( -1 );
	m_staticText2111->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	bSizer32->Add( m_staticText2111, 0, wxALIGN_CENTER_VERTICAL|wxRIGHT, 5 );
	
	bSizer2->Add( bSizer32, 0, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer311;
	bSizer311 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText221 = new wxStaticText( m_panel2, wxID_ANY, wxT("Margin"), wxDefaultPosition, wxSize( 55,-1 ), 0 );
	m_staticText221->Wrap( -1 );
	m_staticText221->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	bSizer311->Add( m_staticText221, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );
	
	m_pGeoMarginTxt = new wxTextCtrl( m_panel2, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER|wxSIMPLE_BORDER );
	m_pGeoMarginTxt->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pGeoMarginTxt->SetBackgroundColour( wxColour( 0, 0, 0 ) );
	
	bSizer311->Add( m_pGeoMarginTxt, 0, 0, 5 );
	
	bSizer2->Add( bSizer311, 0, wxEXPAND, 5 );
	
	m_panel2->SetSizer( bSizer2 );
	m_panel2->Layout();
	bSizer2->Fit( m_panel2 );
	m_pTopSizer->Add( m_panel2, 1, wxEXPAND | wxALL, 0 );
	
	m_pBaseSizer->Add( m_pTopSizer, 0, wxEXPAND|wxALL, 4 );
	
	m_staticline33 = new wxStaticLine( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	m_pBaseSizer->Add( m_staticline33, 0, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer7;
	bSizer7 = new wxBoxSizer( wxHORIZONTAL );
	
	m_pOKBtn = new wxButton( this, wxID_ANY, wxT("OK"), wxDefaultPosition, wxDefaultSize, 0|wxSIMPLE_BORDER );
	m_pOKBtn->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pOKBtn->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	bSizer7->Add( m_pOKBtn, 0, wxALL, 5 );
	
	m_pCancelBtn = new wxButton( this, wxID_ANY, wxT("Cancel"), wxDefaultPosition, wxDefaultSize, 0|wxSIMPLE_BORDER );
	m_pCancelBtn->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pCancelBtn->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	bSizer7->Add( m_pCancelBtn, 0, wxALL, 5 );
	
	m_pBaseSizer->Add( bSizer7, 0, wxALIGN_RIGHT, 5 );
	
	this->SetSizer( m_pBaseSizer );
	this->Layout();
	m_pBaseSizer->Fit( this );
	
	this->Centre( wxBOTH );
	
	// Connect Events
	this->Connect( wxEVT_CLOSE_WINDOW, wxCloseEventHandler( GeometryScopeDlgBase::OnClose ) );
	m_pLevelTxt->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( GeometryScopeDlgBase::OnLevelTxt ), NULL, this );
	m_pGeoMarginCellRBtn->Connect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( GeometryScopeDlgBase::OnMarginRadio ), NULL, this );
	m_pGeoMarginRatioRBtn->Connect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( GeometryScopeDlgBase::OnMarginRadio ), NULL, this );
	m_pGeoMarginTxt->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( GeometryScopeDlgBase::OnMarginTxt ), NULL, this );
	m_pOKBtn->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( GeometryScopeDlgBase::OnOKBtn ), NULL, this );
	m_pCancelBtn->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( GeometryScopeDlgBase::OnCancelBtn ), NULL, this );
}

GeometryScopeDlgBase::~GeometryScopeDlgBase()
{
	// Disconnect Events
	this->Disconnect( wxEVT_CLOSE_WINDOW, wxCloseEventHandler( GeometryScopeDlgBase::OnClose ) );
	m_pLevelTxt->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( GeometryScopeDlgBase::OnLevelTxt ), NULL, this );
	m_pGeoMarginCellRBtn->Disconnect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( GeometryScopeDlgBase::OnMarginRadio ), NULL, this );
	m_pGeoMarginRatioRBtn->Disconnect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( GeometryScopeDlgBase::OnMarginRadio ), NULL, this );
	m_pGeoMarginTxt->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( GeometryScopeDlgBase::OnMarginTxt ), NULL, this );
	m_pOKBtn->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( GeometryScopeDlgBase::OnOKBtn ), NULL, this );
	m_pCancelBtn->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( GeometryScopeDlgBase::OnCancelBtn ), NULL, this );
	
}

SliceCtrlDlgBase::SliceCtrlDlgBase( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );
	this->SetBackgroundColour( wxColour( 44, 44, 44 ) );
	
	m_pBaseSizer = new wxBoxSizer( wxVERTICAL );
	
	m_pTopSizer = new wxBoxSizer( wxVERTICAL );
	
	m_staticText31 = new wxStaticText( this, wxID_ANY, wxT("Slice Control"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText31->Wrap( -1 );
	m_staticText31->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), 70, 90, 92, false, wxEmptyString ) );
	m_staticText31->SetForegroundColour( wxColour( 209, 76, 16 ) );
	
	m_pTopSizer->Add( m_staticText31, 0, wxALL, 2 );
	
	m_panel2 = new wxPanel( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	m_panel2->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_panel2->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	wxBoxSizer* bSizer60;
	bSizer60 = new wxBoxSizer( wxVERTICAL );
	
	wxBoxSizer* bSizer61;
	bSizer61 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText77 = new wxStaticText( m_panel2, wxID_ANY, wxT("Mode"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText77->Wrap( -1 );
	bSizer61->Add( m_staticText77, 0, wxALL, 5 );
	
	m_pPolicyIdxBtn = new wxRadioButton( m_panel2, SLICE_CTRL_IDX_RADIO, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxRB_GROUP );
	bSizer61->Add( m_pPolicyIdxBtn, 0, wxALL, 5 );
	
	m_pPolicyIdxLabel = new wxStaticText( m_panel2, wxID_ANY, wxT("Index Space"), wxDefaultPosition, wxDefaultSize, 0 );
	m_pPolicyIdxLabel->Wrap( -1 );
	bSizer61->Add( m_pPolicyIdxLabel, 0, wxALL, 5 );
	
	m_pPolicyCoordBtn = new wxRadioButton( m_panel2, SLICE_CTRL_COORD_RADIO, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizer61->Add( m_pPolicyCoordBtn, 0, wxALL, 5 );
	
	m_pPolicyCoordLabel = new wxStaticText( m_panel2, wxID_ANY, wxT("Coordinate Space"), wxDefaultPosition, wxDefaultSize, 0 );
	m_pPolicyCoordLabel->Wrap( -1 );
	bSizer61->Add( m_pPolicyCoordLabel, 0, wxALL, 5 );
	
	bSizer60->Add( bSizer61, 0, wxEXPAND, 5 );
	
	m_panel2->SetSizer( bSizer60 );
	m_panel2->Layout();
	bSizer60->Fit( m_panel2 );
	m_pTopSizer->Add( m_panel2, 0, wxEXPAND | wxALL, 0 );
	
	m_panel17 = new wxPanel( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	m_panel17->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_panel17->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	wxFlexGridSizer* fgSizer6;
	fgSizer6 = new wxFlexGridSizer( 4, 4, 0, 0 );
	fgSizer6->SetFlexibleDirection( wxBOTH );
	fgSizer6->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );
	
	m_staticText107 = new wxStaticText( m_panel17, wxID_ANY, wxT("AXIS"), wxDefaultPosition, wxSize( 30,-1 ), wxALIGN_CENTRE );
	m_staticText107->Wrap( -1 );
	fgSizer6->Add( m_staticText107, 0, wxALL, 5 );
	
	m_staticText108 = new wxStaticText( m_panel17, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 260,-1 ), 0 );
	m_staticText108->Wrap( -1 );
	fgSizer6->Add( m_staticText108, 0, wxALL, 5 );
	
	m_staticText109 = new wxStaticText( m_panel17, wxID_ANY, wxT("Position"), wxDefaultPosition, wxSize( 95,-1 ), 0 );
	m_staticText109->Wrap( -1 );
	fgSizer6->Add( m_staticText109, 0, wxALL, 5 );
	
	m_staticText110 = new wxStaticText( m_panel17, wxID_ANY, wxT("Pitch"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText110->Wrap( -1 );
	fgSizer6->Add( m_staticText110, 0, wxALL, 5 );
	
	m_staticText931 = new wxStaticText( m_panel17, wxID_ANY, wxT("X"), wxDefaultPosition, wxSize( 25,-1 ), wxALIGN_CENTRE );
	m_staticText931->Wrap( -1 );
	fgSizer6->Add( m_staticText931, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	wxBoxSizer* bSizer71;
	bSizer71 = new wxBoxSizer( wxHORIZONTAL );
	
	m_pXPosDownBtn = new wxBitmapButton( m_panel17, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxSize( -1,-1 ), 0 );
	m_pXPosDownBtn->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	m_pXPosDownBtn->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	bSizer71->Add( m_pXPosDownBtn, 0, wxALIGN_CENTER_VERTICAL, 5 );
	
	m_pXSlider = new wxSlider( m_panel17, wxID_ANY, 50, 0, 100, wxDefaultPosition, wxSize( 200,-1 ), wxSL_HORIZONTAL );
	bSizer71->Add( m_pXSlider, 0, wxALIGN_CENTER_VERTICAL, 5 );
	
	m_pXPosUpBtn = new wxBitmapButton( m_panel17, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxSize( -1,-1 ), 0 );
	m_pXPosUpBtn->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	m_pXPosUpBtn->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	bSizer71->Add( m_pXPosUpBtn, 0, wxALIGN_CENTER_VERTICAL, 5 );
	
	fgSizer6->Add( bSizer71, 0, 0, 5 );
	
	m_pXPosTxt = new wxTextCtrl( m_panel17, SLICE_CTRL_XPOS_TXT, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER|wxSIMPLE_BORDER );
	m_pXPosTxt->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pXPosTxt->SetBackgroundColour( wxColour( 0, 0, 0 ) );
	
	fgSizer6->Add( m_pXPosTxt, 0, wxALIGN_CENTER_VERTICAL, 5 );
	
	m_pXPitchTxt = new wxTextCtrl( m_panel17, SLICE_CTRL_XPITCH_TXT, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER|wxSIMPLE_BORDER );
	m_pXPitchTxt->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pXPitchTxt->SetBackgroundColour( wxColour( 0, 0, 0 ) );
	
	fgSizer6->Add( m_pXPitchTxt, 0, wxALIGN_CENTER_VERTICAL|wxRIGHT, 5 );
	
	m_staticText9311 = new wxStaticText( m_panel17, wxID_ANY, wxT("Y"), wxDefaultPosition, wxSize( 25,-1 ), wxALIGN_CENTRE );
	m_staticText9311->Wrap( -1 );
	fgSizer6->Add( m_staticText9311, 0, wxALL, 5 );
	
	wxBoxSizer* bSizer65;
	bSizer65 = new wxBoxSizer( wxHORIZONTAL );
	
	m_pYPosDownBtn = new wxBitmapButton( m_panel17, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxSize( -1,-1 ), 0 );
	m_pYPosDownBtn->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	m_pYPosDownBtn->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	bSizer65->Add( m_pYPosDownBtn, 0, wxALIGN_CENTER_VERTICAL, 5 );
	
	m_pYSlider = new wxSlider( m_panel17, wxID_ANY, 50, 0, 100, wxDefaultPosition, wxSize( 200,-1 ), wxSL_HORIZONTAL );
	bSizer65->Add( m_pYSlider, 0, wxALIGN_CENTER_VERTICAL, 5 );
	
	m_pYPosUpBtn = new wxBitmapButton( m_panel17, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxSize( -1,-1 ), 0 );
	m_pYPosUpBtn->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	m_pYPosUpBtn->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	bSizer65->Add( m_pYPosUpBtn, 0, wxALIGN_CENTER_VERTICAL, 5 );
	
	fgSizer6->Add( bSizer65, 0, 0, 5 );
	
	m_pYPosTxt = new wxTextCtrl( m_panel17, SLICE_CTRL_YPOS_TXT, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER|wxSIMPLE_BORDER );
	m_pYPosTxt->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pYPosTxt->SetBackgroundColour( wxColour( 0, 0, 0 ) );
	
	fgSizer6->Add( m_pYPosTxt, 0, wxALIGN_CENTER_VERTICAL, 5 );
	
	m_pYPitchTxt = new wxTextCtrl( m_panel17, SLICE_CTRL_YPITCH_TXT, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER|wxSIMPLE_BORDER );
	m_pYPitchTxt->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pYPitchTxt->SetBackgroundColour( wxColour( 0, 0, 0 ) );
	
	fgSizer6->Add( m_pYPitchTxt, 0, wxALIGN_CENTER_VERTICAL|wxRIGHT, 5 );
	
	m_staticText9312 = new wxStaticText( m_panel17, wxID_ANY, wxT("Z"), wxDefaultPosition, wxSize( 25,-1 ), wxALIGN_CENTRE );
	m_staticText9312->Wrap( -1 );
	fgSizer6->Add( m_staticText9312, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	wxBoxSizer* bSizer651;
	bSizer651 = new wxBoxSizer( wxHORIZONTAL );
	
	m_pZPosDownBtn = new wxBitmapButton( m_panel17, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxSize( -1,-1 ), 0 );
	m_pZPosDownBtn->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	m_pZPosDownBtn->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	bSizer651->Add( m_pZPosDownBtn, 0, wxALIGN_CENTER_VERTICAL, 5 );
	
	m_pZSlider = new wxSlider( m_panel17, wxID_ANY, 50, 0, 100, wxDefaultPosition, wxSize( 200,-1 ), wxSL_HORIZONTAL );
	bSizer651->Add( m_pZSlider, 0, wxALIGN_CENTER_VERTICAL, 5 );
	
	m_pZPosUpBtn = new wxBitmapButton( m_panel17, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxSize( -1,-1 ), 0 );
	m_pZPosUpBtn->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	m_pZPosUpBtn->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	bSizer651->Add( m_pZPosUpBtn, 0, wxALIGN_CENTER_VERTICAL, 5 );
	
	fgSizer6->Add( bSizer651, 0, 0, 5 );
	
	m_pZPosTxt = new wxTextCtrl( m_panel17, SLICE_CTRL_ZPOS_TXT, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER|wxSIMPLE_BORDER );
	m_pZPosTxt->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pZPosTxt->SetBackgroundColour( wxColour( 0, 0, 0 ) );
	
	fgSizer6->Add( m_pZPosTxt, 0, wxALIGN_CENTER_VERTICAL, 5 );
	
	m_pZPitchTxt = new wxTextCtrl( m_panel17, SLICE_CTRL_ZPITCH_TXT, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER|wxSIMPLE_BORDER );
	m_pZPitchTxt->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pZPitchTxt->SetBackgroundColour( wxColour( 0, 0, 0 ) );
	
	fgSizer6->Add( m_pZPitchTxt, 0, wxALIGN_CENTER_VERTICAL|wxRIGHT, 5 );
	
	m_panel17->SetSizer( fgSizer6 );
	m_panel17->Layout();
	fgSizer6->Fit( m_panel17 );
	m_pTopSizer->Add( m_panel17, 0, wxEXPAND|wxTOP, 5 );
	
	m_pBaseSizer->Add( m_pTopSizer, 0, wxEXPAND|wxALL, 4 );
	
	m_pColorMapSizer = new wxBoxSizer( wxVERTICAL );
	
	m_panel24 = new wxPanel( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	m_panel24->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_panel24->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	wxBoxSizer* bSizer103;
	bSizer103 = new wxBoxSizer( wxVERTICAL );
	
	m_staticText1071 = new wxStaticText( m_panel24, wxID_ANY, wxT("COLOR MAP"), wxDefaultPosition, wxSize( -1,-1 ), wxALIGN_CENTRE );
	m_staticText1071->Wrap( -1 );
	bSizer103->Add( m_staticText1071, 0, wxALL, 5 );
	
	m_pColorMapImage = new wxStaticBitmap( m_panel24, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxSize( -1,4 ), 0 );
	bSizer103->Add( m_pColorMapImage, 0, wxBOTTOM|wxLEFT|wxRIGHT, 5 );
	
	wxFlexGridSizer* fgSizer8;
	fgSizer8 = new wxFlexGridSizer( 2, 4, 0, 0 );
	fgSizer8->SetFlexibleDirection( wxBOTH );
	fgSizer8->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );
	
	m_staticText1072 = new wxStaticText( m_panel24, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 60,-1 ), 0 );
	m_staticText1072->Wrap( -1 );
	fgSizer8->Add( m_staticText1072, 0, wxALL, 5 );
	
	m_staticText103 = new wxStaticText( m_panel24, wxID_ANY, wxT("Min"), wxDefaultPosition, wxSize( 120,-1 ), 0 );
	m_staticText103->Wrap( -1 );
	fgSizer8->Add( m_staticText103, 0, wxALL, 5 );
	
	m_staticText104 = new wxStaticText( m_panel24, wxID_ANY, wxT("Max"), wxDefaultPosition, wxSize( 120,-1 ), 0 );
	m_staticText104->Wrap( -1 );
	fgSizer8->Add( m_staticText104, 0, wxALL, 5 );
	
	m_staticText1081 = new wxStaticText( m_panel24, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText1081->Wrap( -1 );
	fgSizer8->Add( m_staticText1081, 0, wxALL, 5 );
	
	m_staticText106 = new wxStaticText( m_panel24, wxID_ANY, wxT("Level"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText106->Wrap( -1 );
	fgSizer8->Add( m_staticText106, 0, wxALL, 5 );
	
	wxArrayString m_pColorMapRangeMinLevelChoices;
	m_pColorMapRangeMinLevel = new wxChoice( m_panel24, wxID_ANY, wxDefaultPosition, wxSize( 120,-1 ), m_pColorMapRangeMinLevelChoices, 0 );
	m_pColorMapRangeMinLevel->SetSelection( 0 );
	m_pColorMapRangeMinLevel->SetForegroundColour( wxColour( 0, 0, 0 ) );
	m_pColorMapRangeMinLevel->SetBackgroundColour( wxColour( 255, 255, 255 ) );
	
	fgSizer8->Add( m_pColorMapRangeMinLevel, 0, wxALIGN_CENTER_VERTICAL, 5 );
	
	wxArrayString m_pColorMapRangeMaxLevelChoices;
	m_pColorMapRangeMaxLevel = new wxChoice( m_panel24, wxID_ANY, wxDefaultPosition, wxSize( 120,-1 ), m_pColorMapRangeMaxLevelChoices, 0 );
	m_pColorMapRangeMaxLevel->SetSelection( 0 );
	m_pColorMapRangeMaxLevel->SetForegroundColour( wxColour( 0, 0, 0 ) );
	m_pColorMapRangeMaxLevel->SetBackgroundColour( wxColour( 255, 255, 255 ) );
	
	fgSizer8->Add( m_pColorMapRangeMaxLevel, 0, wxALIGN_CENTER_VERTICAL, 5 );
	
	m_pColorMapRangeTxt = new wxStaticText( m_panel24, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 140,-1 ), 0 );
	m_pColorMapRangeTxt->Wrap( -1 );
	fgSizer8->Add( m_pColorMapRangeTxt, 0, wxALL, 5 );
	
	bSizer103->Add( fgSizer8, 1, wxEXPAND|wxLEFT, 8 );
	
	m_panel24->SetSizer( bSizer103 );
	m_panel24->Layout();
	bSizer103->Fit( m_panel24 );
	m_pColorMapSizer->Add( m_panel24, 1, wxEXPAND | wxALL, 5 );
	
	m_pBaseSizer->Add( m_pColorMapSizer, 0, wxEXPAND, 5 );
	
	m_staticline1 = new wxStaticLine( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	m_pBaseSizer->Add( m_staticline1, 0, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer7;
	bSizer7 = new wxBoxSizer( wxHORIZONTAL );
	
	
	bSizer7->Add( 0, 0, 1, wxEXPAND, 5 );
	
	m_pCloseBtn = new wxButton( this, wxID_ANY, wxT("Close"), wxDefaultPosition, wxDefaultSize, 0|wxSIMPLE_BORDER );
	m_pCloseBtn->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pCloseBtn->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	bSizer7->Add( m_pCloseBtn, 0, wxALL, 5 );
	
	m_pBaseSizer->Add( bSizer7, 0, wxALIGN_RIGHT|wxBOTTOM|wxEXPAND, 5 );
	
	this->SetSizer( m_pBaseSizer );
	this->Layout();
	
	this->Centre( wxBOTH );
	
	// Connect Events
	m_pPolicyIdxBtn->Connect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( SliceCtrlDlgBase::OnRadioButton ), NULL, this );
	m_pPolicyCoordBtn->Connect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( SliceCtrlDlgBase::OnRadioButton ), NULL, this );
	m_pXPosDownBtn->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SliceCtrlDlgBase::OnXPosDown ), NULL, this );
	m_pXSlider->Connect( wxEVT_SCROLL_TOP, wxScrollEventHandler( SliceCtrlDlgBase::OnXSlideScroll ), NULL, this );
	m_pXSlider->Connect( wxEVT_SCROLL_BOTTOM, wxScrollEventHandler( SliceCtrlDlgBase::OnXSlideScroll ), NULL, this );
	m_pXSlider->Connect( wxEVT_SCROLL_LINEUP, wxScrollEventHandler( SliceCtrlDlgBase::OnXSlideScroll ), NULL, this );
	m_pXSlider->Connect( wxEVT_SCROLL_LINEDOWN, wxScrollEventHandler( SliceCtrlDlgBase::OnXSlideScroll ), NULL, this );
	m_pXSlider->Connect( wxEVT_SCROLL_PAGEUP, wxScrollEventHandler( SliceCtrlDlgBase::OnXSlideScroll ), NULL, this );
	m_pXSlider->Connect( wxEVT_SCROLL_PAGEDOWN, wxScrollEventHandler( SliceCtrlDlgBase::OnXSlideScroll ), NULL, this );
	m_pXSlider->Connect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( SliceCtrlDlgBase::OnXSlideScroll ), NULL, this );
	m_pXSlider->Connect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( SliceCtrlDlgBase::OnXSlideScroll ), NULL, this );
	m_pXSlider->Connect( wxEVT_SCROLL_CHANGED, wxScrollEventHandler( SliceCtrlDlgBase::OnXSlideScroll ), NULL, this );
	m_pXSlider->Connect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( SliceCtrlDlgBase::OnXSlideRelease ), NULL, this );
	m_pXSlider->Connect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( SliceCtrlDlgBase::OnXSlideDrag ), NULL, this );
	m_pXPosUpBtn->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SliceCtrlDlgBase::OnXPosUp ), NULL, this );
	m_pXPosTxt->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( SliceCtrlDlgBase::OnPosTxt ), NULL, this );
	m_pXPitchTxt->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( SliceCtrlDlgBase::OnPitchTxt ), NULL, this );
	m_pYPosDownBtn->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SliceCtrlDlgBase::OnYPosDown ), NULL, this );
	m_pYSlider->Connect( wxEVT_SCROLL_TOP, wxScrollEventHandler( SliceCtrlDlgBase::OnYSlideScroll ), NULL, this );
	m_pYSlider->Connect( wxEVT_SCROLL_BOTTOM, wxScrollEventHandler( SliceCtrlDlgBase::OnYSlideScroll ), NULL, this );
	m_pYSlider->Connect( wxEVT_SCROLL_LINEUP, wxScrollEventHandler( SliceCtrlDlgBase::OnYSlideScroll ), NULL, this );
	m_pYSlider->Connect( wxEVT_SCROLL_LINEDOWN, wxScrollEventHandler( SliceCtrlDlgBase::OnYSlideScroll ), NULL, this );
	m_pYSlider->Connect( wxEVT_SCROLL_PAGEUP, wxScrollEventHandler( SliceCtrlDlgBase::OnYSlideScroll ), NULL, this );
	m_pYSlider->Connect( wxEVT_SCROLL_PAGEDOWN, wxScrollEventHandler( SliceCtrlDlgBase::OnYSlideScroll ), NULL, this );
	m_pYSlider->Connect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( SliceCtrlDlgBase::OnYSlideScroll ), NULL, this );
	m_pYSlider->Connect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( SliceCtrlDlgBase::OnYSlideScroll ), NULL, this );
	m_pYSlider->Connect( wxEVT_SCROLL_CHANGED, wxScrollEventHandler( SliceCtrlDlgBase::OnYSlideScroll ), NULL, this );
	m_pYSlider->Connect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( SliceCtrlDlgBase::OnYSlideRelease ), NULL, this );
	m_pYSlider->Connect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( SliceCtrlDlgBase::OnYSlideDrag ), NULL, this );
	m_pYPosUpBtn->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SliceCtrlDlgBase::OnYPosUp ), NULL, this );
	m_pYPosTxt->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( SliceCtrlDlgBase::OnPosTxt ), NULL, this );
	m_pYPitchTxt->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( SliceCtrlDlgBase::OnPitchTxt ), NULL, this );
	m_pZPosDownBtn->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SliceCtrlDlgBase::OnZPosDown ), NULL, this );
	m_pZSlider->Connect( wxEVT_SCROLL_TOP, wxScrollEventHandler( SliceCtrlDlgBase::OnZSlideScroll ), NULL, this );
	m_pZSlider->Connect( wxEVT_SCROLL_BOTTOM, wxScrollEventHandler( SliceCtrlDlgBase::OnZSlideScroll ), NULL, this );
	m_pZSlider->Connect( wxEVT_SCROLL_LINEUP, wxScrollEventHandler( SliceCtrlDlgBase::OnZSlideScroll ), NULL, this );
	m_pZSlider->Connect( wxEVT_SCROLL_LINEDOWN, wxScrollEventHandler( SliceCtrlDlgBase::OnZSlideScroll ), NULL, this );
	m_pZSlider->Connect( wxEVT_SCROLL_PAGEUP, wxScrollEventHandler( SliceCtrlDlgBase::OnZSlideScroll ), NULL, this );
	m_pZSlider->Connect( wxEVT_SCROLL_PAGEDOWN, wxScrollEventHandler( SliceCtrlDlgBase::OnZSlideScroll ), NULL, this );
	m_pZSlider->Connect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( SliceCtrlDlgBase::OnZSlideScroll ), NULL, this );
	m_pZSlider->Connect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( SliceCtrlDlgBase::OnZSlideScroll ), NULL, this );
	m_pZSlider->Connect( wxEVT_SCROLL_CHANGED, wxScrollEventHandler( SliceCtrlDlgBase::OnZSlideScroll ), NULL, this );
	m_pZSlider->Connect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( SliceCtrlDlgBase::OnZSlideRelease ), NULL, this );
	m_pZSlider->Connect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( SliceCtrlDlgBase::OnZSlideDrag ), NULL, this );
	m_pZPosUpBtn->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SliceCtrlDlgBase::OnZPosUp ), NULL, this );
	m_pZPosTxt->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( SliceCtrlDlgBase::OnPosTxt ), NULL, this );
	m_pZPitchTxt->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( SliceCtrlDlgBase::OnPitchTxt ), NULL, this );
	m_pColorMapRangeMinLevel->Connect( wxEVT_COMMAND_CHOICE_SELECTED, wxCommandEventHandler( SliceCtrlDlgBase::OnColorMapMinLevelChoice ), NULL, this );
	m_pColorMapRangeMaxLevel->Connect( wxEVT_COMMAND_CHOICE_SELECTED, wxCommandEventHandler( SliceCtrlDlgBase::OnColorMapMaxLevelChoice ), NULL, this );
	m_pCloseBtn->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SliceCtrlDlgBase::OnCloseBtn ), NULL, this );
}

SliceCtrlDlgBase::~SliceCtrlDlgBase()
{
	// Disconnect Events
	m_pPolicyIdxBtn->Disconnect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( SliceCtrlDlgBase::OnRadioButton ), NULL, this );
	m_pPolicyCoordBtn->Disconnect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( SliceCtrlDlgBase::OnRadioButton ), NULL, this );
	m_pXPosDownBtn->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SliceCtrlDlgBase::OnXPosDown ), NULL, this );
	m_pXSlider->Disconnect( wxEVT_SCROLL_TOP, wxScrollEventHandler( SliceCtrlDlgBase::OnXSlideScroll ), NULL, this );
	m_pXSlider->Disconnect( wxEVT_SCROLL_BOTTOM, wxScrollEventHandler( SliceCtrlDlgBase::OnXSlideScroll ), NULL, this );
	m_pXSlider->Disconnect( wxEVT_SCROLL_LINEUP, wxScrollEventHandler( SliceCtrlDlgBase::OnXSlideScroll ), NULL, this );
	m_pXSlider->Disconnect( wxEVT_SCROLL_LINEDOWN, wxScrollEventHandler( SliceCtrlDlgBase::OnXSlideScroll ), NULL, this );
	m_pXSlider->Disconnect( wxEVT_SCROLL_PAGEUP, wxScrollEventHandler( SliceCtrlDlgBase::OnXSlideScroll ), NULL, this );
	m_pXSlider->Disconnect( wxEVT_SCROLL_PAGEDOWN, wxScrollEventHandler( SliceCtrlDlgBase::OnXSlideScroll ), NULL, this );
	m_pXSlider->Disconnect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( SliceCtrlDlgBase::OnXSlideScroll ), NULL, this );
	m_pXSlider->Disconnect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( SliceCtrlDlgBase::OnXSlideScroll ), NULL, this );
	m_pXSlider->Disconnect( wxEVT_SCROLL_CHANGED, wxScrollEventHandler( SliceCtrlDlgBase::OnXSlideScroll ), NULL, this );
	m_pXSlider->Disconnect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( SliceCtrlDlgBase::OnXSlideRelease ), NULL, this );
	m_pXSlider->Disconnect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( SliceCtrlDlgBase::OnXSlideDrag ), NULL, this );
	m_pXPosUpBtn->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SliceCtrlDlgBase::OnXPosUp ), NULL, this );
	m_pXPosTxt->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( SliceCtrlDlgBase::OnPosTxt ), NULL, this );
	m_pXPitchTxt->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( SliceCtrlDlgBase::OnPitchTxt ), NULL, this );
	m_pYPosDownBtn->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SliceCtrlDlgBase::OnYPosDown ), NULL, this );
	m_pYSlider->Disconnect( wxEVT_SCROLL_TOP, wxScrollEventHandler( SliceCtrlDlgBase::OnYSlideScroll ), NULL, this );
	m_pYSlider->Disconnect( wxEVT_SCROLL_BOTTOM, wxScrollEventHandler( SliceCtrlDlgBase::OnYSlideScroll ), NULL, this );
	m_pYSlider->Disconnect( wxEVT_SCROLL_LINEUP, wxScrollEventHandler( SliceCtrlDlgBase::OnYSlideScroll ), NULL, this );
	m_pYSlider->Disconnect( wxEVT_SCROLL_LINEDOWN, wxScrollEventHandler( SliceCtrlDlgBase::OnYSlideScroll ), NULL, this );
	m_pYSlider->Disconnect( wxEVT_SCROLL_PAGEUP, wxScrollEventHandler( SliceCtrlDlgBase::OnYSlideScroll ), NULL, this );
	m_pYSlider->Disconnect( wxEVT_SCROLL_PAGEDOWN, wxScrollEventHandler( SliceCtrlDlgBase::OnYSlideScroll ), NULL, this );
	m_pYSlider->Disconnect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( SliceCtrlDlgBase::OnYSlideScroll ), NULL, this );
	m_pYSlider->Disconnect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( SliceCtrlDlgBase::OnYSlideScroll ), NULL, this );
	m_pYSlider->Disconnect( wxEVT_SCROLL_CHANGED, wxScrollEventHandler( SliceCtrlDlgBase::OnYSlideScroll ), NULL, this );
	m_pYSlider->Disconnect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( SliceCtrlDlgBase::OnYSlideRelease ), NULL, this );
	m_pYSlider->Disconnect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( SliceCtrlDlgBase::OnYSlideDrag ), NULL, this );
	m_pYPosUpBtn->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SliceCtrlDlgBase::OnYPosUp ), NULL, this );
	m_pYPosTxt->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( SliceCtrlDlgBase::OnPosTxt ), NULL, this );
	m_pYPitchTxt->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( SliceCtrlDlgBase::OnPitchTxt ), NULL, this );
	m_pZPosDownBtn->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SliceCtrlDlgBase::OnZPosDown ), NULL, this );
	m_pZSlider->Disconnect( wxEVT_SCROLL_TOP, wxScrollEventHandler( SliceCtrlDlgBase::OnZSlideScroll ), NULL, this );
	m_pZSlider->Disconnect( wxEVT_SCROLL_BOTTOM, wxScrollEventHandler( SliceCtrlDlgBase::OnZSlideScroll ), NULL, this );
	m_pZSlider->Disconnect( wxEVT_SCROLL_LINEUP, wxScrollEventHandler( SliceCtrlDlgBase::OnZSlideScroll ), NULL, this );
	m_pZSlider->Disconnect( wxEVT_SCROLL_LINEDOWN, wxScrollEventHandler( SliceCtrlDlgBase::OnZSlideScroll ), NULL, this );
	m_pZSlider->Disconnect( wxEVT_SCROLL_PAGEUP, wxScrollEventHandler( SliceCtrlDlgBase::OnZSlideScroll ), NULL, this );
	m_pZSlider->Disconnect( wxEVT_SCROLL_PAGEDOWN, wxScrollEventHandler( SliceCtrlDlgBase::OnZSlideScroll ), NULL, this );
	m_pZSlider->Disconnect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( SliceCtrlDlgBase::OnZSlideScroll ), NULL, this );
	m_pZSlider->Disconnect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( SliceCtrlDlgBase::OnZSlideScroll ), NULL, this );
	m_pZSlider->Disconnect( wxEVT_SCROLL_CHANGED, wxScrollEventHandler( SliceCtrlDlgBase::OnZSlideScroll ), NULL, this );
	m_pZSlider->Disconnect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( SliceCtrlDlgBase::OnZSlideRelease ), NULL, this );
	m_pZSlider->Disconnect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( SliceCtrlDlgBase::OnZSlideDrag ), NULL, this );
	m_pZPosUpBtn->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SliceCtrlDlgBase::OnZPosUp ), NULL, this );
	m_pZPosTxt->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( SliceCtrlDlgBase::OnPosTxt ), NULL, this );
	m_pZPitchTxt->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( SliceCtrlDlgBase::OnPitchTxt ), NULL, this );
	m_pColorMapRangeMinLevel->Disconnect( wxEVT_COMMAND_CHOICE_SELECTED, wxCommandEventHandler( SliceCtrlDlgBase::OnColorMapMinLevelChoice ), NULL, this );
	m_pColorMapRangeMaxLevel->Disconnect( wxEVT_COMMAND_CHOICE_SELECTED, wxCommandEventHandler( SliceCtrlDlgBase::OnColorMapMaxLevelChoice ), NULL, this );
	m_pCloseBtn->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SliceCtrlDlgBase::OnCloseBtn ), NULL, this );
	
}

BCMediumManagerDlgBase::BCMediumManagerDlgBase( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );
	this->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), 70, 90, 90, false, wxEmptyString ) );
	this->SetBackgroundColour( wxColour( 44, 44, 44 ) );
	
	wxBoxSizer* bSizer19;
	bSizer19 = new wxBoxSizer( wxVERTICAL );
	
	m_staticText30 = new wxStaticText( this, wxID_ANY, wxT("Medium"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText30->Wrap( -1 );
	m_staticText30->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), 70, 90, 92, false, wxEmptyString ) );
	m_staticText30->SetForegroundColour( wxColour( 209, 76, 16 ) );
	
	bSizer19->Add( m_staticText30, 0, wxALL, 2 );
	
	m_panel3 = new wxPanel( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	m_panel3->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	wxBoxSizer* bSizer21;
	bSizer21 = new wxBoxSizer( wxHORIZONTAL );
	
	wxBoxSizer* bSizer22;
	bSizer22 = new wxBoxSizer( wxVERTICAL );
	
	m_pMedGrid = new wxGrid( m_panel3, wxID_ANY, wxDefaultPosition, wxSize( 465,150 ), wxVSCROLL );
	
	// Grid
	m_pMedGrid->CreateGrid( 0, 4 );
	m_pMedGrid->EnableEditing( true );
	m_pMedGrid->EnableGridLines( true );
	m_pMedGrid->SetGridLineColour( wxColour( 128, 128, 128 ) );
	m_pMedGrid->EnableDragGridSize( false );
	m_pMedGrid->SetMargins( 0, 0 );
	
	// Columns
	m_pMedGrid->SetColSize( 0, 274 );
	m_pMedGrid->SetColSize( 1, 59 );
	m_pMedGrid->SetColSize( 2, 48 );
	m_pMedGrid->SetColSize( 3, 60 );
	m_pMedGrid->EnableDragColMove( false );
	m_pMedGrid->EnableDragColSize( true );
	m_pMedGrid->SetColLabelSize( 25 );
	m_pMedGrid->SetColLabelValue( 0, wxT("Label") );
	m_pMedGrid->SetColLabelValue( 1, wxT("Color") );
	m_pMedGrid->SetColLabelValue( 2, wxT("ID") );
	m_pMedGrid->SetColLabelValue( 3, wxT("Type") );
	m_pMedGrid->SetColLabelAlignment( wxALIGN_CENTRE, wxALIGN_CENTRE );
	
	// Rows
	m_pMedGrid->EnableDragRowSize( true );
	m_pMedGrid->SetRowLabelSize( 1 );
	m_pMedGrid->SetRowLabelAlignment( wxALIGN_CENTRE, wxALIGN_CENTRE );
	
	// Label Appearance
	m_pMedGrid->SetLabelBackgroundColour( wxColour( 88, 88, 88 ) );
	m_pMedGrid->SetLabelTextColour( wxColour( 255, 255, 255 ) );
	
	// Cell Defaults
	m_pMedGrid->SetDefaultCellBackgroundColour( wxColour( 44, 44, 44 ) );
	m_pMedGrid->SetDefaultCellTextColour( wxColour( 255, 255, 255 ) );
	m_pMedGrid->SetDefaultCellFont( wxFont( wxNORMAL_FONT->GetPointSize(), 70, 90, 90, false, wxEmptyString ) );
	m_pMedGrid->SetDefaultCellAlignment( wxALIGN_LEFT, wxALIGN_TOP );
	m_pMedGrid->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pMedGrid->SetBackgroundColour( wxColour( 0, 0, 0 ) );
	
	bSizer22->Add( m_pMedGrid, 0, wxALL, 5 );
	
	bSizer21->Add( bSizer22, 0, 0, 5 );
	
	wxBoxSizer* bSizer23;
	bSizer23 = new wxBoxSizer( wxVERTICAL );
	
	m_pNewMedBtn = new wxButton( m_panel3, wxID_ANY, wxT("New"), wxDefaultPosition, wxDefaultSize, 0|wxSIMPLE_BORDER );
	m_pNewMedBtn->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pNewMedBtn->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	bSizer23->Add( m_pNewMedBtn, 0, wxALL, 5 );
	
	m_pDelMedBtn = new wxButton( m_panel3, wxID_ANY, wxT("Delete"), wxDefaultPosition, wxDefaultSize, 0|wxSIMPLE_BORDER );
	m_pDelMedBtn->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pDelMedBtn->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	bSizer23->Add( m_pDelMedBtn, 0, wxALL, 5 );
	
	bSizer21->Add( bSizer23, 0, wxEXPAND, 5 );
	
	m_panel3->SetSizer( bSizer21 );
	m_panel3->Layout();
	bSizer21->Fit( m_panel3 );
	bSizer19->Add( m_panel3, 1, wxEXPAND | wxALL, 5 );
	
	m_staticText301 = new wxStaticText( this, wxID_ANY, wxT("Outer Boundary"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText301->Wrap( -1 );
	m_staticText301->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), 70, 90, 92, false, wxEmptyString ) );
	m_staticText301->SetForegroundColour( wxColour( 209, 76, 16 ) );
	
	bSizer19->Add( m_staticText301, 0, wxALL, 2 );
	
	m_panel4 = new wxPanel( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	m_panel4->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	wxBoxSizer* bSizer24;
	bSizer24 = new wxBoxSizer( wxHORIZONTAL );
	
	wxBoxSizer* bSizer25;
	bSizer25 = new wxBoxSizer( wxVERTICAL );
	
	m_pOuterBCGrid = new wxGrid( m_panel4, wxID_ANY, wxDefaultPosition, wxSize( 465,150 ), wxVSCROLL );
	
	// Grid
	m_pOuterBCGrid->CreateGrid( 0, 2 );
	m_pOuterBCGrid->EnableEditing( true );
	m_pOuterBCGrid->EnableGridLines( true );
	m_pOuterBCGrid->SetGridLineColour( wxColour( 128, 128, 128 ) );
	m_pOuterBCGrid->EnableDragGridSize( false );
	m_pOuterBCGrid->SetMargins( 0, 0 );
	
	// Columns
	m_pOuterBCGrid->SetColSize( 0, 225 );
	m_pOuterBCGrid->SetColSize( 1, 225 );
	m_pOuterBCGrid->EnableDragColMove( false );
	m_pOuterBCGrid->EnableDragColSize( true );
	m_pOuterBCGrid->SetColLabelSize( 25 );
	m_pOuterBCGrid->SetColLabelValue( 0, wxT("Alias") );
	m_pOuterBCGrid->SetColLabelValue( 1, wxT("Class") );
	m_pOuterBCGrid->SetColLabelAlignment( wxALIGN_CENTRE, wxALIGN_CENTRE );
	
	// Rows
	m_pOuterBCGrid->EnableDragRowSize( true );
	m_pOuterBCGrid->SetRowLabelSize( 1 );
	m_pOuterBCGrid->SetRowLabelAlignment( wxALIGN_CENTRE, wxALIGN_CENTRE );
	
	// Label Appearance
	m_pOuterBCGrid->SetLabelBackgroundColour( wxColour( 88, 88, 88 ) );
	m_pOuterBCGrid->SetLabelTextColour( wxColour( 255, 255, 255 ) );
	
	// Cell Defaults
	m_pOuterBCGrid->SetDefaultCellBackgroundColour( wxColour( 44, 44, 44 ) );
	m_pOuterBCGrid->SetDefaultCellTextColour( wxColour( 255, 255, 255 ) );
	m_pOuterBCGrid->SetDefaultCellAlignment( wxALIGN_LEFT, wxALIGN_TOP );
	m_pOuterBCGrid->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pOuterBCGrid->SetBackgroundColour( wxColour( 0, 0, 0 ) );
	
	bSizer25->Add( m_pOuterBCGrid, 0, wxALL, 5 );
	
	bSizer24->Add( bSizer25, 0, 0, 5 );
	
	wxBoxSizer* bSizer26;
	bSizer26 = new wxBoxSizer( wxVERTICAL );
	
	m_pNewOuterBCBtn = new wxButton( m_panel4, wxID_ANY, wxT("New"), wxDefaultPosition, wxDefaultSize, 0|wxSIMPLE_BORDER );
	m_pNewOuterBCBtn->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pNewOuterBCBtn->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	bSizer26->Add( m_pNewOuterBCBtn, 0, wxALL, 5 );
	
	m_pDelOuterBCBtn = new wxButton( m_panel4, wxID_ANY, wxT("Delete"), wxDefaultPosition, wxDefaultSize, 0|wxSIMPLE_BORDER );
	m_pDelOuterBCBtn->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pDelOuterBCBtn->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	bSizer26->Add( m_pDelOuterBCBtn, 0, wxALL, 5 );
	
	bSizer24->Add( bSizer26, 1, wxEXPAND, 5 );
	
	m_panel4->SetSizer( bSizer24 );
	m_panel4->Layout();
	bSizer24->Fit( m_panel4 );
	bSizer19->Add( m_panel4, 1, wxEXPAND | wxALL, 5 );
	
	m_staticText3011 = new wxStaticText( this, wxID_ANY, wxT("Local Boundary"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText3011->Wrap( -1 );
	m_staticText3011->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), 70, 90, 92, false, wxEmptyString ) );
	m_staticText3011->SetForegroundColour( wxColour( 209, 76, 16 ) );
	
	bSizer19->Add( m_staticText3011, 0, wxALL, 2 );
	
	m_panel5 = new wxPanel( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	m_panel5->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	wxBoxSizer* bSizer27;
	bSizer27 = new wxBoxSizer( wxHORIZONTAL );
	
	wxBoxSizer* bSizer28;
	bSizer28 = new wxBoxSizer( wxVERTICAL );
	
	m_pLocalBCGrid = new wxGrid( m_panel5, wxID_ANY, wxDefaultPosition, wxSize( 465,150 ), wxVSCROLL );
	
	// Grid
	m_pLocalBCGrid->CreateGrid( 0, 4 );
	m_pLocalBCGrid->EnableEditing( true );
	m_pLocalBCGrid->EnableGridLines( true );
	m_pLocalBCGrid->SetGridLineColour( wxColour( 128, 128, 128 ) );
	m_pLocalBCGrid->EnableDragGridSize( false );
	m_pLocalBCGrid->SetMargins( 0, 0 );
	
	// Columns
	m_pLocalBCGrid->SetColSize( 0, 183 );
	m_pLocalBCGrid->SetColSize( 1, 162 );
	m_pLocalBCGrid->SetColSize( 2, 50 );
	m_pLocalBCGrid->SetColSize( 3, 52 );
	m_pLocalBCGrid->EnableDragColMove( false );
	m_pLocalBCGrid->EnableDragColSize( true );
	m_pLocalBCGrid->SetColLabelSize( 25 );
	m_pLocalBCGrid->SetColLabelValue( 0, wxT("Alias") );
	m_pLocalBCGrid->SetColLabelValue( 1, wxT("Class") );
	m_pLocalBCGrid->SetColLabelValue( 2, wxT("Color") );
	m_pLocalBCGrid->SetColLabelValue( 3, wxT("ID") );
	m_pLocalBCGrid->SetColLabelAlignment( wxALIGN_CENTRE, wxALIGN_CENTRE );
	
	// Rows
	m_pLocalBCGrid->EnableDragRowSize( true );
	m_pLocalBCGrid->SetRowLabelSize( 1 );
	m_pLocalBCGrid->SetRowLabelAlignment( wxALIGN_CENTRE, wxALIGN_CENTRE );
	
	// Label Appearance
	m_pLocalBCGrid->SetLabelBackgroundColour( wxColour( 88, 88, 88 ) );
	m_pLocalBCGrid->SetLabelTextColour( wxColour( 255, 255, 255 ) );
	
	// Cell Defaults
	m_pLocalBCGrid->SetDefaultCellBackgroundColour( wxColour( 44, 44, 44 ) );
	m_pLocalBCGrid->SetDefaultCellTextColour( wxColour( 255, 255, 255 ) );
	m_pLocalBCGrid->SetDefaultCellAlignment( wxALIGN_LEFT, wxALIGN_TOP );
	m_pLocalBCGrid->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pLocalBCGrid->SetBackgroundColour( wxColour( 0, 0, 0 ) );
	
	bSizer28->Add( m_pLocalBCGrid, 0, wxALL, 5 );
	
	bSizer27->Add( bSizer28, 0, 0, 5 );
	
	wxBoxSizer* bSizer29;
	bSizer29 = new wxBoxSizer( wxVERTICAL );
	
	m_pNewLocalBCBtn = new wxButton( m_panel5, wxID_ANY, wxT("New"), wxDefaultPosition, wxDefaultSize, 0|wxSIMPLE_BORDER );
	m_pNewLocalBCBtn->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pNewLocalBCBtn->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	bSizer29->Add( m_pNewLocalBCBtn, 0, wxALL, 5 );
	
	m_pDelLocalBCBtn = new wxButton( m_panel5, wxID_ANY, wxT("Delete"), wxDefaultPosition, wxDefaultSize, 0|wxSIMPLE_BORDER );
	m_pDelLocalBCBtn->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pDelLocalBCBtn->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	bSizer29->Add( m_pDelLocalBCBtn, 0, wxALL, 5 );
	
	bSizer27->Add( bSizer29, 1, wxEXPAND, 5 );
	
	m_panel5->SetSizer( bSizer27 );
	m_panel5->Layout();
	bSizer27->Fit( m_panel5 );
	bSizer19->Add( m_panel5, 1, wxEXPAND | wxALL, 5 );
	
	wxBoxSizer* bSizer20;
	bSizer20 = new wxBoxSizer( wxHORIZONTAL );
	
	m_pOKBtn = new wxButton( this, wxID_ANY, wxT("OK"), wxDefaultPosition, wxDefaultSize, 0|wxSIMPLE_BORDER );
	m_pOKBtn->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), 70, 90, 90, false, wxEmptyString ) );
	m_pOKBtn->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pOKBtn->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	bSizer20->Add( m_pOKBtn, 0, wxALL, 5 );
	
	m_pCancel = new wxButton( this, wxID_ANY, wxT("Cancel"), wxDefaultPosition, wxDefaultSize, 0|wxSIMPLE_BORDER );
	m_pCancel->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pCancel->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	bSizer20->Add( m_pCancel, 0, wxALL, 5 );
	
	bSizer19->Add( bSizer20, 0, wxALIGN_RIGHT, 5 );
	
	this->SetSizer( bSizer19 );
	this->Layout();
	bSizer19->Fit( this );
	
	this->Centre( wxBOTH );
	
	// Connect Events
	m_pMedGrid->Connect( wxEVT_GRID_CELL_CHANGE, wxGridEventHandler( BCMediumManagerDlgBase::OnChangeMedValue ), NULL, this );
	m_pMedGrid->Connect( wxEVT_GRID_CELL_LEFT_DCLICK, wxGridEventHandler( BCMediumManagerDlgBase::OnClickMedValue ), NULL, this );
	m_pNewMedBtn->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( BCMediumManagerDlgBase::OnNewMedBtn ), NULL, this );
	m_pDelMedBtn->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( BCMediumManagerDlgBase::OnDelMedBtn ), NULL, this );
	m_pOuterBCGrid->Connect( wxEVT_GRID_CELL_CHANGE, wxGridEventHandler( BCMediumManagerDlgBase::OnChangeOuterBCValue ), NULL, this );
	m_pOuterBCGrid->Connect( wxEVT_GRID_CELL_LEFT_DCLICK, wxGridEventHandler( BCMediumManagerDlgBase::OnClickOuterBCValue ), NULL, this );
	m_pNewOuterBCBtn->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( BCMediumManagerDlgBase::OnNewOuterBCBtn ), NULL, this );
	m_pDelOuterBCBtn->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( BCMediumManagerDlgBase::OnDelOuterBCBtn ), NULL, this );
	m_pLocalBCGrid->Connect( wxEVT_GRID_CELL_CHANGE, wxGridEventHandler( BCMediumManagerDlgBase::OnChangeLocalBCValue ), NULL, this );
	m_pLocalBCGrid->Connect( wxEVT_GRID_CELL_LEFT_DCLICK, wxGridEventHandler( BCMediumManagerDlgBase::OnClickLocalBCValue ), NULL, this );
	m_pNewLocalBCBtn->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( BCMediumManagerDlgBase::OnNewLocalBCBtn ), NULL, this );
	m_pDelLocalBCBtn->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( BCMediumManagerDlgBase::OnDelLocalBCBtn ), NULL, this );
	m_pOKBtn->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( BCMediumManagerDlgBase::OnOKBtn ), NULL, this );
	m_pCancel->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( BCMediumManagerDlgBase::OnCancelBtn ), NULL, this );
}

BCMediumManagerDlgBase::~BCMediumManagerDlgBase()
{
	// Disconnect Events
	m_pMedGrid->Disconnect( wxEVT_GRID_CELL_CHANGE, wxGridEventHandler( BCMediumManagerDlgBase::OnChangeMedValue ), NULL, this );
	m_pMedGrid->Disconnect( wxEVT_GRID_CELL_LEFT_DCLICK, wxGridEventHandler( BCMediumManagerDlgBase::OnClickMedValue ), NULL, this );
	m_pNewMedBtn->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( BCMediumManagerDlgBase::OnNewMedBtn ), NULL, this );
	m_pDelMedBtn->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( BCMediumManagerDlgBase::OnDelMedBtn ), NULL, this );
	m_pOuterBCGrid->Disconnect( wxEVT_GRID_CELL_CHANGE, wxGridEventHandler( BCMediumManagerDlgBase::OnChangeOuterBCValue ), NULL, this );
	m_pOuterBCGrid->Disconnect( wxEVT_GRID_CELL_LEFT_DCLICK, wxGridEventHandler( BCMediumManagerDlgBase::OnClickOuterBCValue ), NULL, this );
	m_pNewOuterBCBtn->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( BCMediumManagerDlgBase::OnNewOuterBCBtn ), NULL, this );
	m_pDelOuterBCBtn->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( BCMediumManagerDlgBase::OnDelOuterBCBtn ), NULL, this );
	m_pLocalBCGrid->Disconnect( wxEVT_GRID_CELL_CHANGE, wxGridEventHandler( BCMediumManagerDlgBase::OnChangeLocalBCValue ), NULL, this );
	m_pLocalBCGrid->Disconnect( wxEVT_GRID_CELL_LEFT_DCLICK, wxGridEventHandler( BCMediumManagerDlgBase::OnClickLocalBCValue ), NULL, this );
	m_pNewLocalBCBtn->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( BCMediumManagerDlgBase::OnNewLocalBCBtn ), NULL, this );
	m_pDelLocalBCBtn->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( BCMediumManagerDlgBase::OnDelLocalBCBtn ), NULL, this );
	m_pOKBtn->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( BCMediumManagerDlgBase::OnOKBtn ), NULL, this );
	m_pCancel->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( BCMediumManagerDlgBase::OnCancelBtn ), NULL, this );
	
}

BCMediumManagerDlgBase2::BCMediumManagerDlgBase2( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );
	this->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), 70, 90, 90, false, wxEmptyString ) );
	this->SetBackgroundColour( wxColour( 44, 44, 44 ) );
	
	wxBoxSizer* bSizer19;
	bSizer19 = new wxBoxSizer( wxVERTICAL );
	
	m_staticText30 = new wxStaticText( this, wxID_ANY, wxT("Medium"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText30->Wrap( -1 );
	m_staticText30->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), 70, 90, 92, false, wxEmptyString ) );
	m_staticText30->SetForegroundColour( wxColour( 209, 76, 16 ) );
	
	bSizer19->Add( m_staticText30, 0, wxALL, 2 );
	
	m_panel3 = new wxPanel( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	m_panel3->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	wxBoxSizer* bSizer21;
	bSizer21 = new wxBoxSizer( wxHORIZONTAL );
	
	wxBoxSizer* bSizer22;
	bSizer22 = new wxBoxSizer( wxVERTICAL );
	
	m_pMedGrid = new wxGrid( m_panel3, wxID_ANY, wxDefaultPosition, wxSize( 465,150 ), wxVSCROLL );
	
	// Grid
	m_pMedGrid->CreateGrid( 0, 4 );
	m_pMedGrid->EnableEditing( true );
	m_pMedGrid->EnableGridLines( true );
	m_pMedGrid->SetGridLineColour( wxColour( 128, 128, 128 ) );
	m_pMedGrid->EnableDragGridSize( false );
	m_pMedGrid->SetMargins( 0, 0 );
	
	// Columns
	m_pMedGrid->SetColSize( 0, 274 );
	m_pMedGrid->SetColSize( 1, 59 );
	m_pMedGrid->SetColSize( 2, 64 );
	m_pMedGrid->SetColSize( 3, 60 );
	m_pMedGrid->EnableDragColMove( false );
	m_pMedGrid->EnableDragColSize( true );
	m_pMedGrid->SetColLabelSize( 25 );
	m_pMedGrid->SetColLabelValue( 0, wxT("Label") );
	m_pMedGrid->SetColLabelValue( 1, wxT("Color") );
	m_pMedGrid->SetColLabelValue( 2, wxT("ID") );
	m_pMedGrid->SetColLabelValue( 3, wxT("Type") );
	m_pMedGrid->SetColLabelAlignment( wxALIGN_CENTRE, wxALIGN_CENTRE );
	
	// Rows
	m_pMedGrid->EnableDragRowSize( true );
	m_pMedGrid->SetRowLabelSize( 1 );
	m_pMedGrid->SetRowLabelAlignment( wxALIGN_CENTRE, wxALIGN_CENTRE );
	
	// Label Appearance
	m_pMedGrid->SetLabelBackgroundColour( wxColour( 88, 88, 88 ) );
	m_pMedGrid->SetLabelTextColour( wxColour( 255, 255, 255 ) );
	
	// Cell Defaults
	m_pMedGrid->SetDefaultCellBackgroundColour( wxColour( 44, 44, 44 ) );
	m_pMedGrid->SetDefaultCellTextColour( wxColour( 255, 255, 255 ) );
	m_pMedGrid->SetDefaultCellFont( wxFont( wxNORMAL_FONT->GetPointSize(), 70, 90, 90, false, wxEmptyString ) );
	m_pMedGrid->SetDefaultCellAlignment( wxALIGN_LEFT, wxALIGN_TOP );
	m_pMedGrid->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pMedGrid->SetBackgroundColour( wxColour( 0, 0, 0 ) );
	
	bSizer22->Add( m_pMedGrid, 0, wxALL, 5 );
	
	bSizer21->Add( bSizer22, 0, 0, 5 );
	
	wxBoxSizer* bSizer23;
	bSizer23 = new wxBoxSizer( wxVERTICAL );
	
	m_pNewMedBtn = new wxButton( m_panel3, wxID_ANY, wxT("New"), wxDefaultPosition, wxDefaultSize, 0|wxSIMPLE_BORDER );
	m_pNewMedBtn->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pNewMedBtn->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	bSizer23->Add( m_pNewMedBtn, 0, wxALL, 5 );
	
	m_pDelMedBtn = new wxButton( m_panel3, wxID_ANY, wxT("Delete"), wxDefaultPosition, wxDefaultSize, 0|wxSIMPLE_BORDER );
	m_pDelMedBtn->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pDelMedBtn->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	bSizer23->Add( m_pDelMedBtn, 0, wxALL, 5 );
	
	bSizer21->Add( bSizer23, 0, wxEXPAND, 5 );
	
	m_panel3->SetSizer( bSizer21 );
	m_panel3->Layout();
	bSizer21->Fit( m_panel3 );
	bSizer19->Add( m_panel3, 1, wxEXPAND | wxALL, 5 );
	
	m_staticText301 = new wxStaticText( this, wxID_ANY, wxT("Outer Boundary"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText301->Wrap( -1 );
	m_staticText301->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), 70, 90, 92, false, wxEmptyString ) );
	m_staticText301->SetForegroundColour( wxColour( 209, 76, 16 ) );
	
	bSizer19->Add( m_staticText301, 0, wxALL, 2 );
	
	m_panel4 = new wxPanel( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	m_panel4->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	wxBoxSizer* bSizer24;
	bSizer24 = new wxBoxSizer( wxHORIZONTAL );
	
	wxBoxSizer* bSizer25;
	bSizer25 = new wxBoxSizer( wxVERTICAL );
	
	m_pOuterBCGrid = new wxGrid( m_panel4, wxID_ANY, wxDefaultPosition, wxSize( 465,150 ), wxVSCROLL );
	
	// Grid
	m_pOuterBCGrid->CreateGrid( 0, 2 );
	m_pOuterBCGrid->EnableEditing( true );
	m_pOuterBCGrid->EnableGridLines( true );
	m_pOuterBCGrid->SetGridLineColour( wxColour( 128, 128, 128 ) );
	m_pOuterBCGrid->EnableDragGridSize( false );
	m_pOuterBCGrid->SetMargins( 0, 0 );
	
	// Columns
	m_pOuterBCGrid->SetColSize( 0, 225 );
	m_pOuterBCGrid->SetColSize( 1, 225 );
	m_pOuterBCGrid->EnableDragColMove( false );
	m_pOuterBCGrid->EnableDragColSize( true );
	m_pOuterBCGrid->SetColLabelSize( 25 );
	m_pOuterBCGrid->SetColLabelValue( 0, wxT("Alias") );
	m_pOuterBCGrid->SetColLabelValue( 1, wxT("Class") );
	m_pOuterBCGrid->SetColLabelAlignment( wxALIGN_CENTRE, wxALIGN_CENTRE );
	
	// Rows
	m_pOuterBCGrid->EnableDragRowSize( true );
	m_pOuterBCGrid->SetRowLabelSize( 1 );
	m_pOuterBCGrid->SetRowLabelAlignment( wxALIGN_CENTRE, wxALIGN_CENTRE );
	
	// Label Appearance
	m_pOuterBCGrid->SetLabelBackgroundColour( wxColour( 88, 88, 88 ) );
	m_pOuterBCGrid->SetLabelTextColour( wxColour( 255, 255, 255 ) );
	
	// Cell Defaults
	m_pOuterBCGrid->SetDefaultCellBackgroundColour( wxColour( 44, 44, 44 ) );
	m_pOuterBCGrid->SetDefaultCellTextColour( wxColour( 255, 255, 255 ) );
	m_pOuterBCGrid->SetDefaultCellAlignment( wxALIGN_LEFT, wxALIGN_TOP );
	m_pOuterBCGrid->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pOuterBCGrid->SetBackgroundColour( wxColour( 0, 0, 0 ) );
	
	bSizer25->Add( m_pOuterBCGrid, 0, wxALL, 5 );
	
	bSizer24->Add( bSizer25, 0, 0, 5 );
	
	wxBoxSizer* bSizer26;
	bSizer26 = new wxBoxSizer( wxVERTICAL );
	
	m_pNewOuterBCBtn = new wxButton( m_panel4, wxID_ANY, wxT("New"), wxDefaultPosition, wxDefaultSize, 0|wxSIMPLE_BORDER );
	m_pNewOuterBCBtn->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pNewOuterBCBtn->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	bSizer26->Add( m_pNewOuterBCBtn, 0, wxALL, 5 );
	
	m_pDelOuterBCBtn = new wxButton( m_panel4, wxID_ANY, wxT("Delete"), wxDefaultPosition, wxDefaultSize, 0|wxSIMPLE_BORDER );
	m_pDelOuterBCBtn->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pDelOuterBCBtn->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	bSizer26->Add( m_pDelOuterBCBtn, 0, wxALL, 5 );
	
	bSizer24->Add( bSizer26, 1, wxEXPAND, 5 );
	
	m_panel4->SetSizer( bSizer24 );
	m_panel4->Layout();
	bSizer24->Fit( m_panel4 );
	bSizer19->Add( m_panel4, 1, wxEXPAND | wxALL, 5 );
	
	m_staticText3011 = new wxStaticText( this, wxID_ANY, wxT("Local Boundary"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText3011->Wrap( -1 );
	m_staticText3011->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), 70, 90, 92, false, wxEmptyString ) );
	m_staticText3011->SetForegroundColour( wxColour( 209, 76, 16 ) );
	
	bSizer19->Add( m_staticText3011, 0, wxALL, 2 );
	
	m_panel5 = new wxPanel( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	m_panel5->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	wxBoxSizer* bSizer27;
	bSizer27 = new wxBoxSizer( wxHORIZONTAL );
	
	wxBoxSizer* bSizer28;
	bSizer28 = new wxBoxSizer( wxVERTICAL );
	
	m_pLocalBCGrid = new wxGrid( m_panel5, wxID_ANY, wxDefaultPosition, wxSize( 465,150 ), wxVSCROLL );
	
	// Grid
	m_pLocalBCGrid->CreateGrid( 0, 5 );
	m_pLocalBCGrid->EnableEditing( true );
	m_pLocalBCGrid->EnableGridLines( true );
	m_pLocalBCGrid->SetGridLineColour( wxColour( 128, 128, 128 ) );
	m_pLocalBCGrid->EnableDragGridSize( false );
	m_pLocalBCGrid->SetMargins( 0, 0 );
	
	// Columns
	m_pLocalBCGrid->SetColSize( 0, 136 );
	m_pLocalBCGrid->SetColSize( 1, 107 );
	m_pLocalBCGrid->SetColSize( 2, 73 );
	m_pLocalBCGrid->SetColSize( 3, 64 );
	m_pLocalBCGrid->SetColSize( 4, 80 );
	m_pLocalBCGrid->EnableDragColMove( false );
	m_pLocalBCGrid->EnableDragColSize( true );
	m_pLocalBCGrid->SetColLabelSize( 25 );
	m_pLocalBCGrid->SetColLabelValue( 0, wxT("Alias") );
	m_pLocalBCGrid->SetColLabelValue( 1, wxT("Class") );
	m_pLocalBCGrid->SetColLabelValue( 2, wxT("Color") );
	m_pLocalBCGrid->SetColLabelValue( 3, wxT("ID") );
	m_pLocalBCGrid->SetColLabelValue( 4, wxT("Medium") );
	m_pLocalBCGrid->SetColLabelAlignment( wxALIGN_CENTRE, wxALIGN_CENTRE );
	
	// Rows
	m_pLocalBCGrid->EnableDragRowSize( true );
	m_pLocalBCGrid->SetRowLabelSize( 1 );
	m_pLocalBCGrid->SetRowLabelAlignment( wxALIGN_CENTRE, wxALIGN_CENTRE );
	
	// Label Appearance
	m_pLocalBCGrid->SetLabelBackgroundColour( wxColour( 88, 88, 88 ) );
	m_pLocalBCGrid->SetLabelTextColour( wxColour( 255, 255, 255 ) );
	
	// Cell Defaults
	m_pLocalBCGrid->SetDefaultCellBackgroundColour( wxColour( 44, 44, 44 ) );
	m_pLocalBCGrid->SetDefaultCellTextColour( wxColour( 255, 255, 255 ) );
	m_pLocalBCGrid->SetDefaultCellAlignment( wxALIGN_LEFT, wxALIGN_TOP );
	m_pLocalBCGrid->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pLocalBCGrid->SetBackgroundColour( wxColour( 0, 0, 0 ) );
	
	bSizer28->Add( m_pLocalBCGrid, 0, wxALL, 5 );
	
	bSizer27->Add( bSizer28, 0, 0, 5 );
	
	wxBoxSizer* bSizer29;
	bSizer29 = new wxBoxSizer( wxVERTICAL );
	
	m_pNewLocalBCBtn = new wxButton( m_panel5, wxID_ANY, wxT("New"), wxDefaultPosition, wxDefaultSize, 0|wxSIMPLE_BORDER );
	m_pNewLocalBCBtn->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pNewLocalBCBtn->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	bSizer29->Add( m_pNewLocalBCBtn, 0, wxALL, 5 );
	
	m_pDelLocalBCBtn = new wxButton( m_panel5, wxID_ANY, wxT("Delete"), wxDefaultPosition, wxDefaultSize, 0|wxSIMPLE_BORDER );
	m_pDelLocalBCBtn->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pDelLocalBCBtn->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	bSizer29->Add( m_pDelLocalBCBtn, 0, wxALL, 5 );
	
	bSizer27->Add( bSizer29, 1, wxEXPAND, 5 );
	
	m_panel5->SetSizer( bSizer27 );
	m_panel5->Layout();
	bSizer27->Fit( m_panel5 );
	bSizer19->Add( m_panel5, 1, wxEXPAND | wxALL, 5 );
	
	wxBoxSizer* bSizer20;
	bSizer20 = new wxBoxSizer( wxHORIZONTAL );
	
	m_pOKBtn = new wxButton( this, wxID_ANY, wxT("Apply"), wxDefaultPosition, wxDefaultSize, 0|wxSIMPLE_BORDER );
	m_pOKBtn->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), 70, 90, 90, false, wxEmptyString ) );
	m_pOKBtn->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pOKBtn->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	bSizer20->Add( m_pOKBtn, 0, wxALL, 5 );
	
	m_pCancel = new wxButton( this, wxID_ANY, wxT("Close"), wxDefaultPosition, wxDefaultSize, 0|wxSIMPLE_BORDER );
	m_pCancel->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pCancel->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	bSizer20->Add( m_pCancel, 0, wxALL, 5 );
	
	bSizer19->Add( bSizer20, 0, wxALIGN_RIGHT, 5 );
	
	this->SetSizer( bSizer19 );
	this->Layout();
	
	this->Centre( wxBOTH );
	
	// Connect Events
	m_pMedGrid->Connect( wxEVT_GRID_CELL_CHANGE, wxGridEventHandler( BCMediumManagerDlgBase2::OnChangeMedValue ), NULL, this );
	m_pMedGrid->Connect( wxEVT_GRID_CELL_LEFT_DCLICK, wxGridEventHandler( BCMediumManagerDlgBase2::OnClickMedValue ), NULL, this );
	m_pNewMedBtn->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( BCMediumManagerDlgBase2::OnNewMedBtn ), NULL, this );
	m_pDelMedBtn->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( BCMediumManagerDlgBase2::OnDelMedBtn ), NULL, this );
	m_pOuterBCGrid->Connect( wxEVT_GRID_CELL_CHANGE, wxGridEventHandler( BCMediumManagerDlgBase2::OnChangeOuterBCValue ), NULL, this );
	m_pOuterBCGrid->Connect( wxEVT_GRID_CELL_LEFT_DCLICK, wxGridEventHandler( BCMediumManagerDlgBase2::OnClickOuterBCValue ), NULL, this );
	m_pNewOuterBCBtn->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( BCMediumManagerDlgBase2::OnNewOuterBCBtn ), NULL, this );
	m_pDelOuterBCBtn->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( BCMediumManagerDlgBase2::OnDelOuterBCBtn ), NULL, this );
	m_pLocalBCGrid->Connect( wxEVT_GRID_CELL_CHANGE, wxGridEventHandler( BCMediumManagerDlgBase2::OnChangeLocalBCValue ), NULL, this );
	m_pLocalBCGrid->Connect( wxEVT_GRID_CELL_LEFT_DCLICK, wxGridEventHandler( BCMediumManagerDlgBase2::OnClickLocalBCValue ), NULL, this );
	m_pNewLocalBCBtn->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( BCMediumManagerDlgBase2::OnNewLocalBCBtn ), NULL, this );
	m_pDelLocalBCBtn->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( BCMediumManagerDlgBase2::OnDelLocalBCBtn ), NULL, this );
	m_pOKBtn->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( BCMediumManagerDlgBase2::OnOKBtn ), NULL, this );
	m_pCancel->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( BCMediumManagerDlgBase2::OnCancelBtn ), NULL, this );
}

BCMediumManagerDlgBase2::~BCMediumManagerDlgBase2()
{
	// Disconnect Events
	m_pMedGrid->Disconnect( wxEVT_GRID_CELL_CHANGE, wxGridEventHandler( BCMediumManagerDlgBase2::OnChangeMedValue ), NULL, this );
	m_pMedGrid->Disconnect( wxEVT_GRID_CELL_LEFT_DCLICK, wxGridEventHandler( BCMediumManagerDlgBase2::OnClickMedValue ), NULL, this );
	m_pNewMedBtn->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( BCMediumManagerDlgBase2::OnNewMedBtn ), NULL, this );
	m_pDelMedBtn->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( BCMediumManagerDlgBase2::OnDelMedBtn ), NULL, this );
	m_pOuterBCGrid->Disconnect( wxEVT_GRID_CELL_CHANGE, wxGridEventHandler( BCMediumManagerDlgBase2::OnChangeOuterBCValue ), NULL, this );
	m_pOuterBCGrid->Disconnect( wxEVT_GRID_CELL_LEFT_DCLICK, wxGridEventHandler( BCMediumManagerDlgBase2::OnClickOuterBCValue ), NULL, this );
	m_pNewOuterBCBtn->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( BCMediumManagerDlgBase2::OnNewOuterBCBtn ), NULL, this );
	m_pDelOuterBCBtn->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( BCMediumManagerDlgBase2::OnDelOuterBCBtn ), NULL, this );
	m_pLocalBCGrid->Disconnect( wxEVT_GRID_CELL_CHANGE, wxGridEventHandler( BCMediumManagerDlgBase2::OnChangeLocalBCValue ), NULL, this );
	m_pLocalBCGrid->Disconnect( wxEVT_GRID_CELL_LEFT_DCLICK, wxGridEventHandler( BCMediumManagerDlgBase2::OnClickLocalBCValue ), NULL, this );
	m_pNewLocalBCBtn->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( BCMediumManagerDlgBase2::OnNewLocalBCBtn ), NULL, this );
	m_pDelLocalBCBtn->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( BCMediumManagerDlgBase2::OnDelLocalBCBtn ), NULL, this );
	m_pOKBtn->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( BCMediumManagerDlgBase2::OnOKBtn ), NULL, this );
	m_pCancel->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( BCMediumManagerDlgBase2::OnCancelBtn ), NULL, this );
	
}

LocalBCSettingDlgBase::LocalBCSettingDlgBase( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxSize( 348,-1 ), wxDefaultSize );
	this->SetBackgroundColour( wxColour( 44, 44, 44 ) );
	
	wxBoxSizer* bSizer24;
	bSizer24 = new wxBoxSizer( wxVERTICAL );
	
	m_pBCMedGrid = new wxGrid( this, wxID_ANY, wxDefaultPosition, wxSize( -1,-1 ), 0 );
	
	// Grid
	m_pBCMedGrid->CreateGrid( 0, 5 );
	m_pBCMedGrid->EnableEditing( false );
	m_pBCMedGrid->EnableGridLines( true );
	m_pBCMedGrid->SetGridLineColour( wxColour( 128, 128, 128 ) );
	m_pBCMedGrid->EnableDragGridSize( false );
	m_pBCMedGrid->SetMargins( 0, 0 );
	
	// Columns
	m_pBCMedGrid->SetColSize( 0, 98 );
	m_pBCMedGrid->SetColSize( 1, 57 );
	m_pBCMedGrid->SetColSize( 2, 36 );
	m_pBCMedGrid->SetColSize( 3, 139 );
	m_pBCMedGrid->SetColSize( 4, 134 );
	m_pBCMedGrid->EnableDragColMove( true );
	m_pBCMedGrid->EnableDragColSize( true );
	m_pBCMedGrid->SetColLabelSize( 20 );
	m_pBCMedGrid->SetColLabelValue( 0, wxT("Alias") );
	m_pBCMedGrid->SetColLabelValue( 1, wxT("Color") );
	m_pBCMedGrid->SetColLabelValue( 2, wxT("ID") );
	m_pBCMedGrid->SetColLabelValue( 3, wxT("LocalBC/Medium") );
	m_pBCMedGrid->SetColLabelValue( 4, wxT("Class/Type") );
	m_pBCMedGrid->SetColLabelAlignment( wxALIGN_CENTRE, wxALIGN_CENTRE );
	
	// Rows
	m_pBCMedGrid->AutoSizeRows();
	m_pBCMedGrid->EnableDragRowSize( false );
	m_pBCMedGrid->SetRowLabelSize( 0 );
	m_pBCMedGrid->SetRowLabelAlignment( wxALIGN_CENTRE, wxALIGN_CENTRE );
	
	// Label Appearance
	m_pBCMedGrid->SetLabelBackgroundColour( wxColour( 88, 88, 88 ) );
	m_pBCMedGrid->SetLabelTextColour( wxColour( 255, 255, 255 ) );
	
	// Cell Defaults
	m_pBCMedGrid->SetDefaultCellBackgroundColour( wxColour( 44, 44, 44 ) );
	m_pBCMedGrid->SetDefaultCellTextColour( wxColour( 255, 255, 255 ) );
	m_pBCMedGrid->SetDefaultCellAlignment( wxALIGN_LEFT, wxALIGN_TOP );
	bSizer24->Add( m_pBCMedGrid, 0, wxALL, 5 );
	
	wxBoxSizer* bSizer25;
	bSizer25 = new wxBoxSizer( wxHORIZONTAL );
	
	m_pOKBtn = new wxButton( this, wxID_ANY, wxT("OK"), wxDefaultPosition, wxDefaultSize, 0|wxSIMPLE_BORDER );
	m_pOKBtn->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pOKBtn->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	bSizer25->Add( m_pOKBtn, 0, wxALL, 5 );
	
	m_pCancelBtn = new wxButton( this, wxID_ANY, wxT("Cancel"), wxDefaultPosition, wxDefaultSize, 0|wxSIMPLE_BORDER );
	m_pCancelBtn->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pCancelBtn->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	bSizer25->Add( m_pCancelBtn, 0, wxALL, 5 );
	
	bSizer24->Add( bSizer25, 1, wxALIGN_CENTER_HORIZONTAL, 5 );
	
	this->SetSizer( bSizer24 );
	this->Layout();
	
	this->Centre( wxBOTH );
	
	// Connect Events
	m_pBCMedGrid->Connect( wxEVT_GRID_RANGE_SELECT, wxGridRangeSelectEventHandler( LocalBCSettingDlgBase::OnBCMListRangeSelect ), NULL, this );
	m_pOKBtn->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( LocalBCSettingDlgBase::OnOKBtn ), NULL, this );
	m_pCancelBtn->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( LocalBCSettingDlgBase::OnCancelBtn ), NULL, this );
}

LocalBCSettingDlgBase::~LocalBCSettingDlgBase()
{
	// Disconnect Events
	m_pBCMedGrid->Disconnect( wxEVT_GRID_RANGE_SELECT, wxGridRangeSelectEventHandler( LocalBCSettingDlgBase::OnBCMListRangeSelect ), NULL, this );
	m_pOKBtn->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( LocalBCSettingDlgBase::OnOKBtn ), NULL, this );
	m_pCancelBtn->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( LocalBCSettingDlgBase::OnCancelBtn ), NULL, this );
	
}

LocalBCSettingDlgBase2::LocalBCSettingDlgBase2( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxSize( 348,-1 ), wxDefaultSize );
	this->SetBackgroundColour( wxColour( 44, 44, 44 ) );
	
	wxBoxSizer* bSizer24;
	bSizer24 = new wxBoxSizer( wxVERTICAL );
	
	m_pBCMedGrid = new wxGrid( this, wxID_ANY, wxDefaultPosition, wxSize( -1,-1 ), 0 );
	
	// Grid
	m_pBCMedGrid->CreateGrid( 0, 5 );
	m_pBCMedGrid->EnableEditing( false );
	m_pBCMedGrid->EnableGridLines( true );
	m_pBCMedGrid->SetGridLineColour( wxColour( 128, 128, 128 ) );
	m_pBCMedGrid->EnableDragGridSize( false );
	m_pBCMedGrid->SetMargins( 0, 0 );
	
	// Columns
	m_pBCMedGrid->SetColSize( 0, 98 );
	m_pBCMedGrid->SetColSize( 1, 57 );
	m_pBCMedGrid->SetColSize( 2, 36 );
	m_pBCMedGrid->SetColSize( 3, 139 );
	m_pBCMedGrid->SetColSize( 4, 134 );
	m_pBCMedGrid->EnableDragColMove( true );
	m_pBCMedGrid->EnableDragColSize( true );
	m_pBCMedGrid->SetColLabelSize( 20 );
	m_pBCMedGrid->SetColLabelValue( 0, wxT("Alias") );
	m_pBCMedGrid->SetColLabelValue( 1, wxT("Color") );
	m_pBCMedGrid->SetColLabelValue( 2, wxT("ID") );
	m_pBCMedGrid->SetColLabelValue( 3, wxT("LocalBC/Medium") );
	m_pBCMedGrid->SetColLabelValue( 4, wxT("Class/Type") );
	m_pBCMedGrid->SetColLabelAlignment( wxALIGN_CENTRE, wxALIGN_CENTRE );
	
	// Rows
	m_pBCMedGrid->AutoSizeRows();
	m_pBCMedGrid->EnableDragRowSize( false );
	m_pBCMedGrid->SetRowLabelSize( 0 );
	m_pBCMedGrid->SetRowLabelAlignment( wxALIGN_CENTRE, wxALIGN_CENTRE );
	
	// Label Appearance
	m_pBCMedGrid->SetLabelBackgroundColour( wxColour( 88, 88, 88 ) );
	m_pBCMedGrid->SetLabelTextColour( wxColour( 255, 255, 255 ) );
	
	// Cell Defaults
	m_pBCMedGrid->SetDefaultCellBackgroundColour( wxColour( 44, 44, 44 ) );
	m_pBCMedGrid->SetDefaultCellTextColour( wxColour( 255, 255, 255 ) );
	m_pBCMedGrid->SetDefaultCellAlignment( wxALIGN_LEFT, wxALIGN_TOP );
	bSizer24->Add( m_pBCMedGrid, 0, wxALL, 5 );
	
	wxBoxSizer* bSizer25;
	bSizer25 = new wxBoxSizer( wxHORIZONTAL );
	
	m_pOKBtn = new wxButton( this, wxID_ANY, wxT("Apply"), wxDefaultPosition, wxDefaultSize, 0|wxSIMPLE_BORDER );
	m_pOKBtn->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pOKBtn->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	bSizer25->Add( m_pOKBtn, 0, wxALL, 5 );
	
	m_pCancelBtn = new wxButton( this, wxID_ANY, wxT("Close"), wxDefaultPosition, wxDefaultSize, 0|wxSIMPLE_BORDER );
	m_pCancelBtn->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pCancelBtn->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	bSizer25->Add( m_pCancelBtn, 0, wxALL, 5 );
	
	bSizer24->Add( bSizer25, 1, wxALIGN_CENTER_HORIZONTAL, 5 );
	
	this->SetSizer( bSizer24 );
	this->Layout();
	
	this->Centre( wxBOTH );
	
	// Connect Events
	m_pBCMedGrid->Connect( wxEVT_GRID_RANGE_SELECT, wxGridRangeSelectEventHandler( LocalBCSettingDlgBase2::OnBCMListRangeSelect ), NULL, this );
	m_pOKBtn->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( LocalBCSettingDlgBase2::OnOKBtn ), NULL, this );
	m_pCancelBtn->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( LocalBCSettingDlgBase2::OnCancelBtn ), NULL, this );
}

LocalBCSettingDlgBase2::~LocalBCSettingDlgBase2()
{
	// Disconnect Events
	m_pBCMedGrid->Disconnect( wxEVT_GRID_RANGE_SELECT, wxGridRangeSelectEventHandler( LocalBCSettingDlgBase2::OnBCMListRangeSelect ), NULL, this );
	m_pOKBtn->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( LocalBCSettingDlgBase2::OnOKBtn ), NULL, this );
	m_pCancelBtn->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( LocalBCSettingDlgBase2::OnCancelBtn ), NULL, this );
	
}

SubdomainSettingDlgBase::SubdomainSettingDlgBase( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );
	this->SetForegroundColour( wxColour( 255, 255, 255 ) );
	this->SetBackgroundColour( wxColour( 44, 44, 44 ) );
	
	wxBoxSizer* bSizer24;
	bSizer24 = new wxBoxSizer( wxVERTICAL );
	
	m_pStatusList = new wxListBox( this, wxID_ANY, wxDefaultPosition, wxSize( 160,40 ), 0, NULL, 0|wxSIMPLE_BORDER );
	m_pStatusList->Append( wxT("Inactive") );
	m_pStatusList->Append( wxT("Active") );
	m_pStatusList->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), 70, 90, 90, false, wxEmptyString ) );
	m_pStatusList->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pStatusList->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	bSizer24->Add( m_pStatusList, 0, wxALIGN_CENTER|wxALL, 5 );
	
	wxBoxSizer* bSizer25;
	bSizer25 = new wxBoxSizer( wxHORIZONTAL );
	
	m_pOKBtn = new wxButton( this, wxID_ANY, wxT("OK"), wxDefaultPosition, wxDefaultSize, 0|wxSIMPLE_BORDER );
	m_pOKBtn->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pOKBtn->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	bSizer25->Add( m_pOKBtn, 0, wxALL, 5 );
	
	m_pCancelBtn = new wxButton( this, wxID_ANY, wxT("Cancel"), wxDefaultPosition, wxDefaultSize, 0|wxSIMPLE_BORDER );
	m_pCancelBtn->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pCancelBtn->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	bSizer25->Add( m_pCancelBtn, 0, wxALL, 5 );
	
	bSizer24->Add( bSizer25, 1, wxALIGN_CENTER_HORIZONTAL, 5 );
	
	this->SetSizer( bSizer24 );
	this->Layout();
	bSizer24->Fit( this );
	
	this->Centre( wxBOTH );
	
	// Connect Events
	m_pOKBtn->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SubdomainSettingDlgBase::OnOKBtn ), NULL, this );
	m_pCancelBtn->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SubdomainSettingDlgBase::OnCancelBtn ), NULL, this );
}

SubdomainSettingDlgBase::~SubdomainSettingDlgBase()
{
	// Disconnect Events
	m_pOKBtn->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SubdomainSettingDlgBase::OnOKBtn ), NULL, this );
	m_pCancelBtn->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SubdomainSettingDlgBase::OnCancelBtn ), NULL, this );
	
}

OuterBCSettingDlgBase::OuterBCSettingDlgBase( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );
	this->SetBackgroundColour( wxColour( 44, 44, 44 ) );
	
	wxBoxSizer* bSizer26;
	bSizer26 = new wxBoxSizer( wxVERTICAL );
	
	m_panel6 = new wxPanel( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	m_panel6->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	wxBoxSizer* bSizer47;
	bSizer47 = new wxBoxSizer( wxVERTICAL );
	
	wxBoxSizer* bSizer27;
	bSizer27 = new wxBoxSizer( wxHORIZONTAL );
	
	bSizer47->Add( bSizer27, 0, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer28;
	bSizer28 = new wxBoxSizer( wxVERTICAL );
	
	m_pBCMedGrid = new wxGrid( m_panel6, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxNO_BORDER );
	
	// Grid
	m_pBCMedGrid->CreateGrid( 6, 2 );
	m_pBCMedGrid->EnableEditing( true );
	m_pBCMedGrid->EnableGridLines( true );
	m_pBCMedGrid->SetGridLineColour( wxColour( 128, 128, 128 ) );
	m_pBCMedGrid->EnableDragGridSize( false );
	m_pBCMedGrid->SetMargins( 0, 0 );
	
	// Columns
	m_pBCMedGrid->SetColSize( 0, 300 );
	m_pBCMedGrid->SetColSize( 1, 300 );
	m_pBCMedGrid->EnableDragColMove( false );
	m_pBCMedGrid->EnableDragColSize( true );
	m_pBCMedGrid->SetColLabelSize( 30 );
	m_pBCMedGrid->SetColLabelValue( 0, wxT("BC") );
	m_pBCMedGrid->SetColLabelValue( 1, wxT("Guide Cell Medium") );
	m_pBCMedGrid->SetColLabelAlignment( wxALIGN_CENTRE, wxALIGN_CENTRE );
	
	// Rows
	m_pBCMedGrid->SetRowSize( 0, 18 );
	m_pBCMedGrid->SetRowSize( 1, 18 );
	m_pBCMedGrid->SetRowSize( 2, 18 );
	m_pBCMedGrid->SetRowSize( 3, 18 );
	m_pBCMedGrid->SetRowSize( 4, 18 );
	m_pBCMedGrid->SetRowSize( 5, 18 );
	m_pBCMedGrid->EnableDragRowSize( false );
	m_pBCMedGrid->SetRowLabelSize( 50 );
	m_pBCMedGrid->SetRowLabelValue( 0, wxT("X -") );
	m_pBCMedGrid->SetRowLabelValue( 1, wxT("X+") );
	m_pBCMedGrid->SetRowLabelValue( 2, wxT("Y -") );
	m_pBCMedGrid->SetRowLabelValue( 3, wxT("Y+") );
	m_pBCMedGrid->SetRowLabelValue( 4, wxT("Z -") );
	m_pBCMedGrid->SetRowLabelValue( 5, wxT("Z+") );
	m_pBCMedGrid->SetRowLabelAlignment( wxALIGN_CENTRE, wxALIGN_CENTRE );
	
	// Label Appearance
	m_pBCMedGrid->SetLabelBackgroundColour( wxColour( 88, 88, 88 ) );
	m_pBCMedGrid->SetLabelTextColour( wxColour( 255, 255, 255 ) );
	
	// Cell Defaults
	m_pBCMedGrid->SetDefaultCellBackgroundColour( wxColour( 0, 0, 0 ) );
	m_pBCMedGrid->SetDefaultCellTextColour( wxColour( 255, 255, 255 ) );
	m_pBCMedGrid->SetDefaultCellAlignment( wxALIGN_LEFT, wxALIGN_TOP );
	m_pBCMedGrid->SetMinSize( wxSize( -1,150 ) );
	m_pBCMedGrid->SetMaxSize( wxSize( -1,150 ) );
	
	bSizer28->Add( m_pBCMedGrid, 0, wxALL, 5 );
	
	bSizer47->Add( bSizer28, 0, 0, 5 );
	
	m_panel6->SetSizer( bSizer47 );
	m_panel6->Layout();
	bSizer47->Fit( m_panel6 );
	bSizer26->Add( m_panel6, 1, wxEXPAND | wxALL, 5 );
	
	wxBoxSizer* bSizer29;
	bSizer29 = new wxBoxSizer( wxHORIZONTAL );
	
	m_pOKBtn = new wxButton( this, wxID_ANY, wxT("OK"), wxDefaultPosition, wxDefaultSize, 0 );
	m_pOKBtn->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pOKBtn->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	bSizer29->Add( m_pOKBtn, 0, wxALL, 5 );
	
	m_pCancelBtn = new wxButton( this, wxID_ANY, wxT("Cancel"), wxDefaultPosition, wxDefaultSize, 0 );
	m_pCancelBtn->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pCancelBtn->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	bSizer29->Add( m_pCancelBtn, 0, wxALL, 5 );
	
	bSizer26->Add( bSizer29, 0, wxALIGN_RIGHT, 5 );
	
	this->SetSizer( bSizer26 );
	this->Layout();
	bSizer26->Fit( this );
	
	this->Centre( wxBOTH );
	
	// Connect Events
	m_pBCMedGrid->Connect( wxEVT_GRID_CELL_CHANGE, wxGridEventHandler( OuterBCSettingDlgBase::OnBCMedGridChange ), NULL, this );
	m_pOKBtn->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( OuterBCSettingDlgBase::OnOKBtn ), NULL, this );
	m_pCancelBtn->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( OuterBCSettingDlgBase::OnCancelBtn ), NULL, this );
}

OuterBCSettingDlgBase::~OuterBCSettingDlgBase()
{
	// Disconnect Events
	m_pBCMedGrid->Disconnect( wxEVT_GRID_CELL_CHANGE, wxGridEventHandler( OuterBCSettingDlgBase::OnBCMedGridChange ), NULL, this );
	m_pOKBtn->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( OuterBCSettingDlgBase::OnOKBtn ), NULL, this );
	m_pCancelBtn->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( OuterBCSettingDlgBase::OnCancelBtn ), NULL, this );
	
}

ProgressBarDlgBase::ProgressBarDlgBase( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );
	this->SetBackgroundColour( wxColour( 44, 44, 44 ) );
	
	wxBoxSizer* bSizer54;
	bSizer54 = new wxBoxSizer( wxVERTICAL );
	
	m_panel6 = new wxPanel( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	m_panel6->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	wxBoxSizer* bSizer61;
	bSizer61 = new wxBoxSizer( wxVERTICAL );
	
	m_pProgress = new wxGauge( m_panel6, wxID_ANY, 100, wxDefaultPosition, wxSize( 256,20 ), wxGA_HORIZONTAL );
	bSizer61->Add( m_pProgress, 1, 0, 5 );
	
	m_panel6->SetSizer( bSizer61 );
	m_panel6->Layout();
	bSizer61->Fit( m_panel6 );
	bSizer54->Add( m_panel6, 1, wxEXPAND, 5 );
	
	this->SetSizer( bSizer54 );
	this->Layout();
	bSizer54->Fit( this );
	
	this->Centre( wxBOTH );
}

ProgressBarDlgBase::~ProgressBarDlgBase()
{
}

SDImportDlgBase::SDImportDlgBase( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );
	this->SetForegroundColour( wxColour( 255, 255, 255 ) );
	this->SetBackgroundColour( wxColour( 44, 44, 44 ) );
	
	wxBoxSizer* bSizer24;
	bSizer24 = new wxBoxSizer( wxVERTICAL );
	
	wxBoxSizer* bSizer136;
	bSizer136 = new wxBoxSizer( wxVERTICAL );
	
	wxBoxSizer* bSizer147;
	bSizer147 = new wxBoxSizer( wxVERTICAL );
	
	m_staticText302 = new wxStaticText( this, wxID_ANY, wxT("index.dfi"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText302->Wrap( -1 );
	m_staticText302->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), 70, 90, 92, false, wxEmptyString ) );
	m_staticText302->SetForegroundColour( wxColour( 209, 76, 16 ) );
	
	bSizer147->Add( m_staticText302, 0, wxALL, 5 );
	
	wxBoxSizer* bSizer146;
	bSizer146 = new wxBoxSizer( wxHORIZONTAL );
	
	m_textCtrl_dfi = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_textCtrl_dfi->SetMinSize( wxSize( 600,-1 ) );
	
	bSizer146->Add( m_textCtrl_dfi, 0, wxALL, 5 );
	
	m_button_file_dfi = new wxButton( this, wxID_ANY, wxT("File"), wxDefaultPosition, wxDefaultSize, 0 );
	m_button_file_dfi->SetMinSize( wxSize( 50,-1 ) );
	
	bSizer146->Add( m_button_file_dfi, 0, wxALL, 5 );
	
	
	bSizer146->Add( 0, 0, 1, wxEXPAND, 5 );
	
	bSizer147->Add( bSizer146, 0, wxEXPAND, 5 );
	
	m_staticText301 = new wxStaticText( this, wxID_ANY, wxT("cid_*.bvx"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText301->Wrap( -1 );
	m_staticText301->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), 70, 90, 92, false, wxEmptyString ) );
	m_staticText301->SetForegroundColour( wxColour( 209, 76, 16 ) );
	
	bSizer147->Add( m_staticText301, 0, wxALL, 5 );
	
	wxBoxSizer* bSizer1461;
	bSizer1461 = new wxBoxSizer( wxHORIZONTAL );
	
	m_textCtrl_cellid = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_textCtrl_cellid->SetMinSize( wxSize( 600,-1 ) );
	
	bSizer1461->Add( m_textCtrl_cellid, 0, wxALL, 5 );
	
	m_button_file_cellid = new wxButton( this, wxID_ANY, wxT("File"), wxDefaultPosition, wxDefaultSize, 0 );
	m_button_file_cellid->SetMinSize( wxSize( 50,-1 ) );
	
	bSizer1461->Add( m_button_file_cellid, 0, wxALL, 5 );
	
	m_button_dir_cellid = new wxButton( this, wxID_ANY, wxT("Dir"), wxDefaultPosition, wxDefaultSize, 0 );
	m_button_dir_cellid->SetMinSize( wxSize( 50,-1 ) );
	
	bSizer1461->Add( m_button_dir_cellid, 0, wxALL, 5 );
	
	m_button_cid_clear = new wxButton( this, wxID_ANY, wxT("Clear"), wxDefaultPosition, wxDefaultSize, 0 );
	m_button_cid_clear->SetMinSize( wxSize( 50,-1 ) );
	
	bSizer1461->Add( m_button_cid_clear, 0, wxALL, 5 );
	
	bSizer147->Add( bSizer1461, 0, wxEXPAND, 5 );
	
	m_staticText3011 = new wxStaticText( this, wxID_ANY, wxT("bcf_*.bvx"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText3011->Wrap( -1 );
	m_staticText3011->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), 70, 90, 92, false, wxEmptyString ) );
	m_staticText3011->SetForegroundColour( wxColour( 209, 76, 16 ) );
	
	bSizer147->Add( m_staticText3011, 0, wxALL, 5 );
	
	wxBoxSizer* bSizer14611;
	bSizer14611 = new wxBoxSizer( wxHORIZONTAL );
	
	m_textCtrl_bcflg = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_textCtrl_bcflg->SetMinSize( wxSize( 600,-1 ) );
	
	bSizer14611->Add( m_textCtrl_bcflg, 0, wxALL, 5 );
	
	m_button_file_bcflg = new wxButton( this, wxID_ANY, wxT("File"), wxDefaultPosition, wxDefaultSize, 0 );
	m_button_file_bcflg->SetMinSize( wxSize( 50,-1 ) );
	
	bSizer14611->Add( m_button_file_bcflg, 0, wxALL, 5 );
	
	m_button_dir_bcflg = new wxButton( this, wxID_ANY, wxT("Dir"), wxDefaultPosition, wxDefaultSize, 0 );
	m_button_dir_bcflg->SetMinSize( wxSize( 50,-1 ) );
	
	bSizer14611->Add( m_button_dir_bcflg, 0, wxALL, 5 );
	
	
	bSizer14611->Add( 0, 0, 1, wxEXPAND, 5 );
	
	m_button_bcf_clear = new wxButton( this, wxID_ANY, wxT("Clear"), wxDefaultPosition, wxDefaultSize, 0 );
	m_button_bcf_clear->SetMinSize( wxSize( 50,-1 ) );
	
	bSizer14611->Add( m_button_bcf_clear, 0, wxALL, 5 );
	
	bSizer147->Add( bSizer14611, 0, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer135;
	bSizer135 = new wxBoxSizer( wxHORIZONTAL );
	
	
	bSizer135->Add( 0, 0, 1, wxEXPAND, 5 );
	
	m_pOKBtn = new wxButton( this, wxID_ANY, wxT("OK"), wxDefaultPosition, wxDefaultSize, 0|wxSIMPLE_BORDER );
	m_pOKBtn->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pOKBtn->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	bSizer135->Add( m_pOKBtn, 0, wxALL, 5 );
	
	m_pCancelBtn = new wxButton( this, wxID_ANY, wxT("Cancel"), wxDefaultPosition, wxSize( -1,-1 ), 0|wxSIMPLE_BORDER );
	m_pCancelBtn->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pCancelBtn->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	bSizer135->Add( m_pCancelBtn, 0, wxALL, 5 );
	
	bSizer147->Add( bSizer135, 0, wxALIGN_BOTTOM|wxEXPAND, 5 );
	
	bSizer136->Add( bSizer147, 1, wxEXPAND, 5 );
	
	bSizer24->Add( bSizer136, 1, wxALL, 5 );
	
	this->SetSizer( bSizer24 );
	this->Layout();
	
	this->Centre( wxBOTH );
	
	// Connect Events
	this->Connect( wxEVT_ACTIVATE, wxActivateEventHandler( SDImportDlgBase::SDImportDlgBaseOnActivate ) );
	this->Connect( wxEVT_ACTIVATE_APP, wxActivateEventHandler( SDImportDlgBase::SDImportDlgBaseOnActivateApp ) );
	this->Connect( wxEVT_CLOSE_WINDOW, wxCloseEventHandler( SDImportDlgBase::SDImportDlgBaseOnClose ) );
	this->Connect( wxEVT_INIT_DIALOG, wxInitDialogEventHandler( SDImportDlgBase::SDImportDlgBaseOnInitDialog ) );
	m_button_file_dfi->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SDImportDlgBase::m_button_file_dfiOnButtonClick ), NULL, this );
	m_button_file_cellid->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SDImportDlgBase::m_button_file_cellidOnButtonClick ), NULL, this );
	m_button_dir_cellid->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SDImportDlgBase::m_button_dir_cellidOnButtonClick ), NULL, this );
	m_button_cid_clear->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SDImportDlgBase::m_button_cid_clearOnButtonClick ), NULL, this );
	m_button_file_bcflg->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SDImportDlgBase::m_button_file_bcflgOnButtonClick ), NULL, this );
	m_button_dir_bcflg->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SDImportDlgBase::m_button_dir_bcflgOnButtonClick ), NULL, this );
	m_button_bcf_clear->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SDImportDlgBase::m_button_bcf_clearOnButtonClick ), NULL, this );
	m_pOKBtn->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SDImportDlgBase::m_pOKBtnOnButtonClick ), NULL, this );
	m_pCancelBtn->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SDImportDlgBase::m_pCancelBtnOnButtonClick ), NULL, this );
}

SDImportDlgBase::~SDImportDlgBase()
{
	// Disconnect Events
	this->Disconnect( wxEVT_ACTIVATE, wxActivateEventHandler( SDImportDlgBase::SDImportDlgBaseOnActivate ) );
	this->Disconnect( wxEVT_ACTIVATE_APP, wxActivateEventHandler( SDImportDlgBase::SDImportDlgBaseOnActivateApp ) );
	this->Disconnect( wxEVT_CLOSE_WINDOW, wxCloseEventHandler( SDImportDlgBase::SDImportDlgBaseOnClose ) );
	this->Disconnect( wxEVT_INIT_DIALOG, wxInitDialogEventHandler( SDImportDlgBase::SDImportDlgBaseOnInitDialog ) );
	m_button_file_dfi->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SDImportDlgBase::m_button_file_dfiOnButtonClick ), NULL, this );
	m_button_file_cellid->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SDImportDlgBase::m_button_file_cellidOnButtonClick ), NULL, this );
	m_button_dir_cellid->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SDImportDlgBase::m_button_dir_cellidOnButtonClick ), NULL, this );
	m_button_cid_clear->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SDImportDlgBase::m_button_cid_clearOnButtonClick ), NULL, this );
	m_button_file_bcflg->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SDImportDlgBase::m_button_file_bcflgOnButtonClick ), NULL, this );
	m_button_dir_bcflg->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SDImportDlgBase::m_button_dir_bcflgOnButtonClick ), NULL, this );
	m_button_bcf_clear->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SDImportDlgBase::m_button_bcf_clearOnButtonClick ), NULL, this );
	m_pOKBtn->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SDImportDlgBase::m_pOKBtnOnButtonClick ), NULL, this );
	m_pCancelBtn->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SDImportDlgBase::m_pCancelBtnOnButtonClick ), NULL, this );
	
}

SD_Frame::SD_Frame( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxFrame( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );
	
	m_pBaseSizer = new wxBoxSizer( wxVERTICAL );
	
	m_pTopSizer = new wxBoxSizer( wxVERTICAL );
	
	m_topdumyPanel = new wxPanel( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer186;
	bSizer186 = new wxBoxSizer( wxVERTICAL );
	
	m_button75 = new wxButton( m_topdumyPanel, wxID_ANY, wxT("MyButton"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer186->Add( m_button75, 0, wxALL, 5 );
	
	m_button76 = new wxButton( m_topdumyPanel, wxID_ANY, wxT("MyButton"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer186->Add( m_button76, 0, wxALL, 5 );
	
	m_button77 = new wxButton( m_topdumyPanel, wxID_ANY, wxT("MyButton"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer186->Add( m_button77, 0, wxALL, 5 );
	
	m_topdumyPanel->SetSizer( bSizer186 );
	m_topdumyPanel->Layout();
	bSizer186->Fit( m_topdumyPanel );
	m_pTopSizer->Add( m_topdumyPanel, 1, wxALL|wxEXPAND, 10 );
	
	m_pBaseSizer->Add( m_pTopSizer, 0, wxALL|wxEXPAND, 0 );
	
	wxBoxSizer* m_pBodySizer;
	m_pBodySizer = new wxBoxSizer( wxVERTICAL );
	
	m_notebook = new wxNotebook( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0 );
	m_panel_stagein = new wxPanel( m_notebook, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* m_bSizer_stagingPage;
	m_bSizer_stagingPage = new wxBoxSizer( wxVERTICAL );
	
	m_panel_stagein->SetSizer( m_bSizer_stagingPage );
	m_panel_stagein->Layout();
	m_bSizer_stagingPage->Fit( m_panel_stagein );
	m_notebook->AddPage( m_panel_stagein, wxT("select"), true );
	m_panel_globallocal = new wxPanel( m_notebook, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* m_bSizer_planePage;
	m_bSizer_planePage = new wxBoxSizer( wxVERTICAL );
	
	m_panel58 = new wxPanel( m_panel_globallocal, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer235;
	bSizer235 = new wxBoxSizer( wxVERTICAL );
	
	m_button94 = new wxButton( m_panel58, wxID_ANY, wxT("MyButton"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer235->Add( m_button94, 0, wxALL, 5 );
	
	m_panel58->SetSizer( bSizer235 );
	m_panel58->Layout();
	bSizer235->Fit( m_panel58 );
	m_bSizer_planePage->Add( m_panel58, 1, wxEXPAND | wxALL, 5 );
	
	m_panel59 = new wxPanel( m_panel_globallocal, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer236;
	bSizer236 = new wxBoxSizer( wxVERTICAL );
	
	m_button95 = new wxButton( m_panel59, wxID_ANY, wxT("MyButton"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer236->Add( m_button95, 0, wxALL, 5 );
	
	m_panel59->SetSizer( bSizer236 );
	m_panel59->Layout();
	bSizer236->Fit( m_panel59 );
	m_bSizer_planePage->Add( m_panel59, 1, wxEXPAND | wxALL, 5 );
	
	m_panel_globallocal->SetSizer( m_bSizer_planePage );
	m_panel_globallocal->Layout();
	m_bSizer_planePage->Fit( m_panel_globallocal );
	m_notebook->AddPage( m_panel_globallocal, wxT("cross plane"), false );
	m_panel_bcflag = new wxPanel( m_notebook, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	m_notebook->AddPage( m_panel_bcflag, wxT("bcflag"), false );
	
	m_pBodySizer->Add( m_notebook, 1, wxEXPAND | wxALL, 5 );
	
	m_pBaseSizer->Add( m_pBodySizer, 1, wxEXPAND, 0 );
	
	wxBoxSizer* m_pBottomSizer;
	m_pBottomSizer = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText191 = new wxStaticText( this, wxID_ANY, wxT("Play Interval(msec)"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText191->Wrap( -1 );
	m_pBottomSizer->Add( m_staticText191, 0, wxALIGN_CENTER|wxALL, 5 );
	
	m_playSpeedspinCtrl = new wxSpinCtrl( this, wxID_ANY, wxT("100"), wxDefaultPosition, wxSize( 70,-1 ), wxSP_ARROW_KEYS, 10, 10000, 100 );
	m_pBottomSizer->Add( m_playSpeedspinCtrl, 0, wxALL, 5 );
	
	
	m_pBottomSizer->Add( 0, 0, 1, wxEXPAND, 5 );
	
	m_pCloseBtn = new wxButton( this, wxID_ANY, wxT("Close"), wxDefaultPosition, wxDefaultSize, 0 );
	m_pBottomSizer->Add( m_pCloseBtn, 0, wxALL, 5 );
	
	m_pBaseSizer->Add( m_pBottomSizer, 0, wxEXPAND, 0 );
	
	this->SetSizer( m_pBaseSizer );
	this->Layout();
	
	this->Centre( wxBOTH );
	
	// Connect Events
	this->Connect( wxEVT_ACTIVATE, wxActivateEventHandler( SD_Frame::OnActivate ) );
	this->Connect( wxEVT_ACTIVATE_APP, wxActivateEventHandler( SD_Frame::OnActivateApp ) );
	this->Connect( wxEVT_CLOSE_WINDOW, wxCloseEventHandler( SD_Frame::OnClose ) );
	m_notebook->Connect( wxEVT_COMMAND_NOTEBOOK_PAGE_CHANGED, wxNotebookEventHandler( SD_Frame::m_notebookOnNotebookPageChanged ), NULL, this );
	m_notebook->Connect( wxEVT_COMMAND_NOTEBOOK_PAGE_CHANGING, wxNotebookEventHandler( SD_Frame::m_notebookOnNotebookPageChanging ), NULL, this );
	m_pCloseBtn->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SD_Frame::m_pCloseBtnOnButtonClick ), NULL, this );
}

SD_Frame::~SD_Frame()
{
	// Disconnect Events
	this->Disconnect( wxEVT_ACTIVATE, wxActivateEventHandler( SD_Frame::OnActivate ) );
	this->Disconnect( wxEVT_ACTIVATE_APP, wxActivateEventHandler( SD_Frame::OnActivateApp ) );
	this->Disconnect( wxEVT_CLOSE_WINDOW, wxCloseEventHandler( SD_Frame::OnClose ) );
	m_notebook->Disconnect( wxEVT_COMMAND_NOTEBOOK_PAGE_CHANGED, wxNotebookEventHandler( SD_Frame::m_notebookOnNotebookPageChanged ), NULL, this );
	m_notebook->Disconnect( wxEVT_COMMAND_NOTEBOOK_PAGE_CHANGING, wxNotebookEventHandler( SD_Frame::m_notebookOnNotebookPageChanging ), NULL, this );
	m_pCloseBtn->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SD_Frame::m_pCloseBtnOnButtonClick ), NULL, this );
	
}

SD_SelectPanel::SD_SelectPanel( wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style ) : wxPanel( parent, id, pos, size, style )
{
	wxBoxSizer* bSizer144;
	bSizer144 = new wxBoxSizer( wxVERTICAL );
	
	m_pBaseSizer = new wxBoxSizer( wxVERTICAL );
	
	wxBoxSizer* bSizer193;
	bSizer193 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText197 = new wxStaticText( this, wxID_ANY, wxT("SubDomain Selection"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText197->Wrap( -1 );
	bSizer193->Add( m_staticText197, 0, wxALIGN_CENTER, 5 );
	
	
	bSizer193->Add( 0, 0, 1, wxEXPAND, 5 );
	
	m_pBaseSizer->Add( bSizer193, 0, wxEXPAND, 5 );
	
	wxBoxSizer* m_pBodySizer;
	m_pBodySizer = new wxBoxSizer( wxVERTICAL );
	
	m_pSubDomainGrid = new wxGrid( this, wxID_ANY, wxDefaultPosition, wxSize( -1,-1 ), wxHSCROLL|wxVSCROLL );
	
	// Grid
	m_pSubDomainGrid->CreateGrid( 0, 2 );
	m_pSubDomainGrid->EnableEditing( true );
	m_pSubDomainGrid->EnableGridLines( true );
	m_pSubDomainGrid->EnableDragGridSize( false );
	m_pSubDomainGrid->SetMargins( 0, 0 );
	
	// Columns
	m_pSubDomainGrid->SetColSize( 0, 52 );
	m_pSubDomainGrid->SetColSize( 1, 300 );
	m_pSubDomainGrid->EnableDragColMove( false );
	m_pSubDomainGrid->EnableDragColSize( true );
	m_pSubDomainGrid->SetColLabelSize( 25 );
	m_pSubDomainGrid->SetColLabelValue( 0, wxT("Load") );
	m_pSubDomainGrid->SetColLabelValue( 1, wxT("SubDomain Detail") );
	m_pSubDomainGrid->SetColLabelAlignment( wxALIGN_LEFT, wxALIGN_CENTRE );
	
	// Rows
	m_pSubDomainGrid->AutoSizeRows();
	m_pSubDomainGrid->EnableDragRowSize( true );
	m_pSubDomainGrid->SetRowLabelSize( 1 );
	m_pSubDomainGrid->SetRowLabelAlignment( wxALIGN_CENTRE, wxALIGN_CENTRE );
	
	// Label Appearance
	m_pSubDomainGrid->SetLabelBackgroundColour( wxColour( 88, 88, 88 ) );
	m_pSubDomainGrid->SetLabelTextColour( wxColour( 255, 255, 255 ) );
	
	// Cell Defaults
	m_pSubDomainGrid->SetDefaultCellBackgroundColour( wxColour( 44, 44, 44 ) );
	m_pSubDomainGrid->SetDefaultCellTextColour( wxColour( 255, 255, 255 ) );
	m_pSubDomainGrid->SetDefaultCellAlignment( wxALIGN_LEFT, wxALIGN_TOP );
	m_pSubDomainGrid->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pSubDomainGrid->SetBackgroundColour( wxColour( 0, 0, 0 ) );
	
	m_pBodySizer->Add( m_pSubDomainGrid, 1, wxEXPAND, 5 );
	
	m_pBaseSizer->Add( m_pBodySizer, 1, wxEXPAND, 5 );
	
	wxBoxSizer* m_pBottomSizer;
	m_pBottomSizer = new wxBoxSizer( wxHORIZONTAL );
	
	m_button_reselection = new wxButton( this, wxID_ANY, wxT("SelectAll"), wxDefaultPosition, wxDefaultSize, 0 );
	m_pBottomSizer->Add( m_button_reselection, 0, wxALL, 5 );
	
	m_button_staging = new wxButton( this, wxID_ANY, wxT("Listing"), wxDefaultPosition, wxDefaultSize, 0 );
	m_pBottomSizer->Add( m_button_staging, 0, wxALL, 5 );
	
	m_button_subdomainload = new wxButton( this, wxID_ANY, wxT("Apply"), wxDefaultPosition, wxDefaultSize, 0 );
	m_pBottomSizer->Add( m_button_subdomainload, 0, wxALL, 5 );
	
	m_button_clear = new wxButton( this, wxID_ANY, wxT("Clear"), wxDefaultPosition, wxDefaultSize, 0 );
	m_pBottomSizer->Add( m_button_clear, 0, wxALL, 5 );
	
	
	m_pBottomSizer->Add( 0, 0, 1, wxEXPAND, 5 );
	
	m_pBaseSizer->Add( m_pBottomSizer, 0, wxALIGN_RIGHT, 5 );
	
	bSizer144->Add( m_pBaseSizer, 1, wxEXPAND, 0 );
	
	this->SetSizer( bSizer144 );
	this->Layout();
	
	// Connect Events
	m_pSubDomainGrid->Connect( wxEVT_GRID_SELECT_CELL, wxGridEventHandler( SD_SelectPanel::m_pSubDomainGridOnGridSelectCell ), NULL, this );
	m_button_reselection->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SD_SelectPanel::m_button_reselectionOnButtonClick ), NULL, this );
	m_button_staging->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SD_SelectPanel::m_button_stagingOnButtonClick ), NULL, this );
	m_button_subdomainload->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SD_SelectPanel::m_button_subdomainloadOnButtonClick ), NULL, this );
	m_button_clear->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SD_SelectPanel::m_button_clearOnButtonClick ), NULL, this );
}

SD_SelectPanel::~SD_SelectPanel()
{
	// Disconnect Events
	m_pSubDomainGrid->Disconnect( wxEVT_GRID_SELECT_CELL, wxGridEventHandler( SD_SelectPanel::m_pSubDomainGridOnGridSelectCell ), NULL, this );
	m_button_reselection->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SD_SelectPanel::m_button_reselectionOnButtonClick ), NULL, this );
	m_button_staging->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SD_SelectPanel::m_button_stagingOnButtonClick ), NULL, this );
	m_button_subdomainload->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SD_SelectPanel::m_button_subdomainloadOnButtonClick ), NULL, this );
	m_button_clear->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SD_SelectPanel::m_button_clearOnButtonClick ), NULL, this );
	
}

SD_ClippingPanel::SD_ClippingPanel( wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style ) : wxPanel( parent, id, pos, size, style )
{
	wxBoxSizer* bSizer193;
	bSizer193 = new wxBoxSizer( wxVERTICAL );
	
	wxBoxSizer* bSizer144;
	bSizer144 = new wxBoxSizer( wxVERTICAL );
	
	wxBoxSizer* bSizer143;
	bSizer143 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText191 = new wxStaticText( this, wxID_ANY, wxT("Clip   "), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText191->Wrap( -1 );
	bSizer143->Add( m_staticText191, 0, wxALL, 5 );
	
	m_checkBox_clipslice_visiable = new wxCheckBox( this, wxID_ANY, wxT("Visible"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer143->Add( m_checkBox_clipslice_visiable, 0, wxALL, 5 );
	
	bSizer144->Add( bSizer143, 0, wxEXPAND, 5 );
	
	m_panel2 = new wxPanel( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer60;
	bSizer60 = new wxBoxSizer( wxVERTICAL );
	
	wxBoxSizer* bSizer61;
	bSizer61 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText77 = new wxStaticText( m_panel2, wxID_ANY, wxT("Mode"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText77->Wrap( -1 );
	bSizer61->Add( m_staticText77, 0, wxALL, 5 );
	
	m_pPolicyIdxBtn = new wxRadioButton( m_panel2, SLICE_CTRL_IDX_RADIO, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxRB_GROUP );
	bSizer61->Add( m_pPolicyIdxBtn, 0, wxALL, 5 );
	
	m_pPolicyIdxLabel = new wxStaticText( m_panel2, wxID_ANY, wxT("Index Space"), wxDefaultPosition, wxDefaultSize, 0 );
	m_pPolicyIdxLabel->Wrap( -1 );
	bSizer61->Add( m_pPolicyIdxLabel, 0, wxALL, 5 );
	
	m_pPolicyCoordBtn = new wxRadioButton( m_panel2, SLICE_CTRL_COORD_RADIO, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizer61->Add( m_pPolicyCoordBtn, 0, wxALL, 5 );
	
	m_pPolicyCoordLabel = new wxStaticText( m_panel2, wxID_ANY, wxT("Coordinate Space"), wxDefaultPosition, wxDefaultSize, 0 );
	m_pPolicyCoordLabel->Wrap( -1 );
	bSizer61->Add( m_pPolicyCoordLabel, 0, wxALL, 5 );
	
	bSizer60->Add( bSizer61, 0, wxEXPAND, 5 );
	
	m_panel2->SetSizer( bSizer60 );
	m_panel2->Layout();
	bSizer60->Fit( m_panel2 );
	bSizer144->Add( m_panel2, 0, wxEXPAND | wxALL, 0 );
	
	m_panel17 = new wxPanel( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxFlexGridSizer* fgSizer6;
	fgSizer6 = new wxFlexGridSizer( 4, 4, 0, 0 );
	fgSizer6->SetFlexibleDirection( wxBOTH );
	fgSizer6->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );
	
	m_staticText107 = new wxStaticText( m_panel17, wxID_ANY, wxT("AXIS"), wxDefaultPosition, wxSize( -1,-1 ), wxALIGN_CENTRE );
	m_staticText107->Wrap( -1 );
	fgSizer6->Add( m_staticText107, 0, wxALL, 5 );
	
	m_staticText108 = new wxStaticText( m_panel17, wxID_ANY, wxT("Play"), wxDefaultPosition, wxSize( -1,-1 ), 0 );
	m_staticText108->Wrap( -1 );
	fgSizer6->Add( m_staticText108, 1, wxALIGN_RIGHT|wxALL, 5 );
	
	m_staticText109 = new wxStaticText( m_panel17, wxID_ANY, wxT("Position"), wxDefaultPosition, wxSize( 70,-1 ), 0 );
	m_staticText109->Wrap( -1 );
	fgSizer6->Add( m_staticText109, 0, wxALL, 5 );
	
	m_staticText110 = new wxStaticText( m_panel17, wxID_ANY, wxT("Interval"), wxDefaultPosition, wxSize( 70,-1 ), 0 );
	m_staticText110->Wrap( -1 );
	fgSizer6->Add( m_staticText110, 0, wxALL, 5 );
	
	m_staticText931 = new wxStaticText( m_panel17, wxID_ANY, wxT("X"), wxDefaultPosition, wxSize( 25,-1 ), wxALIGN_CENTRE );
	m_staticText931->Wrap( -1 );
	fgSizer6->Add( m_staticText931, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	wxBoxSizer* bSizer71;
	bSizer71 = new wxBoxSizer( wxHORIZONTAL );
	
	m_pXPosDownBtn = new wxBitmapButton( m_panel17, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxSize( -1,-1 ), 0 );
	bSizer71->Add( m_pXPosDownBtn, 0, wxALIGN_CENTER_VERTICAL, 5 );
	
	m_pXSlider = new wxSlider( m_panel17, wxID_ANY, 50, 0, 100, wxDefaultPosition, wxSize( 100,-1 ), wxSL_HORIZONTAL );
	bSizer71->Add( m_pXSlider, 0, wxALIGN_CENTER_VERTICAL, 5 );
	
	m_pXPosUpBtn = new wxBitmapButton( m_panel17, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxSize( -1,-1 ), 0 );
	bSizer71->Add( m_pXPosUpBtn, 0, wxALIGN_CENTER_VERTICAL, 5 );
	
	m_play_check_x = new wxCheckBox( m_panel17, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizer71->Add( m_play_check_x, 0, wxALL, 5 );
	
	fgSizer6->Add( bSizer71, 0, 0, 5 );
	
	m_pXPosTxt = new wxTextCtrl( m_panel17, SLICE_CTRL_XPOS_TXT, wxEmptyString, wxDefaultPosition, wxSize( 70,-1 ), wxTE_PROCESS_ENTER|wxSIMPLE_BORDER );
	fgSizer6->Add( m_pXPosTxt, 0, wxALIGN_CENTER_VERTICAL, 5 );
	
	m_pXPitchTxt = new wxTextCtrl( m_panel17, SLICE_CTRL_XPITCH_TXT, wxEmptyString, wxDefaultPosition, wxSize( 70,-1 ), wxTE_PROCESS_ENTER|wxSIMPLE_BORDER );
	fgSizer6->Add( m_pXPitchTxt, 0, wxALIGN_CENTER_VERTICAL|wxRIGHT, 5 );
	
	m_staticText9311 = new wxStaticText( m_panel17, wxID_ANY, wxT("Y"), wxDefaultPosition, wxSize( 25,-1 ), wxALIGN_CENTRE );
	m_staticText9311->Wrap( -1 );
	fgSizer6->Add( m_staticText9311, 0, wxALL, 5 );
	
	wxBoxSizer* bSizer65;
	bSizer65 = new wxBoxSizer( wxHORIZONTAL );
	
	m_pYPosDownBtn = new wxBitmapButton( m_panel17, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxSize( -1,-1 ), 0 );
	bSizer65->Add( m_pYPosDownBtn, 0, wxALIGN_CENTER_VERTICAL, 5 );
	
	m_pYSlider = new wxSlider( m_panel17, wxID_ANY, 50, 0, 100, wxDefaultPosition, wxSize( 100,-1 ), wxSL_HORIZONTAL );
	bSizer65->Add( m_pYSlider, 0, wxALIGN_CENTER_VERTICAL, 5 );
	
	m_pYPosUpBtn = new wxBitmapButton( m_panel17, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxSize( -1,-1 ), 0 );
	bSizer65->Add( m_pYPosUpBtn, 0, wxALIGN_CENTER_VERTICAL, 5 );
	
	m_play_check_y = new wxCheckBox( m_panel17, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizer65->Add( m_play_check_y, 0, wxALL, 5 );
	
	fgSizer6->Add( bSizer65, 0, 0, 5 );
	
	m_pYPosTxt = new wxTextCtrl( m_panel17, SLICE_CTRL_YPOS_TXT, wxEmptyString, wxDefaultPosition, wxSize( 70,-1 ), wxTE_PROCESS_ENTER|wxSIMPLE_BORDER );
	fgSizer6->Add( m_pYPosTxt, 0, wxALIGN_CENTER_VERTICAL, 5 );
	
	m_pYPitchTxt = new wxTextCtrl( m_panel17, SLICE_CTRL_YPITCH_TXT, wxEmptyString, wxDefaultPosition, wxSize( 70,-1 ), wxTE_PROCESS_ENTER|wxSIMPLE_BORDER );
	fgSizer6->Add( m_pYPitchTxt, 0, wxALIGN_CENTER_VERTICAL|wxRIGHT, 5 );
	
	m_staticText9312 = new wxStaticText( m_panel17, wxID_ANY, wxT("Z"), wxDefaultPosition, wxSize( 25,-1 ), wxALIGN_CENTRE );
	m_staticText9312->Wrap( -1 );
	fgSizer6->Add( m_staticText9312, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	wxBoxSizer* bSizer651;
	bSizer651 = new wxBoxSizer( wxHORIZONTAL );
	
	m_pZPosDownBtn = new wxBitmapButton( m_panel17, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxSize( -1,-1 ), 0 );
	bSizer651->Add( m_pZPosDownBtn, 0, wxALIGN_CENTER_VERTICAL, 5 );
	
	m_pZSlider = new wxSlider( m_panel17, wxID_ANY, 50, 0, 100, wxDefaultPosition, wxSize( 100,-1 ), wxSL_HORIZONTAL );
	bSizer651->Add( m_pZSlider, 0, wxALIGN_CENTER_VERTICAL, 5 );
	
	m_pZPosUpBtn = new wxBitmapButton( m_panel17, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxSize( -1,-1 ), 0 );
	bSizer651->Add( m_pZPosUpBtn, 0, wxALIGN_CENTER_VERTICAL, 5 );
	
	m_play_check_z = new wxCheckBox( m_panel17, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizer651->Add( m_play_check_z, 0, wxALL, 5 );
	
	fgSizer6->Add( bSizer651, 0, 0, 5 );
	
	m_pZPosTxt = new wxTextCtrl( m_panel17, SLICE_CTRL_ZPOS_TXT, wxEmptyString, wxDefaultPosition, wxSize( 70,-1 ), wxTE_PROCESS_ENTER|wxSIMPLE_BORDER );
	fgSizer6->Add( m_pZPosTxt, 0, wxALIGN_CENTER_VERTICAL, 5 );
	
	m_pZPitchTxt = new wxTextCtrl( m_panel17, SLICE_CTRL_ZPITCH_TXT, wxEmptyString, wxDefaultPosition, wxSize( 70,-1 ), wxTE_PROCESS_ENTER|wxSIMPLE_BORDER );
	fgSizer6->Add( m_pZPitchTxt, 0, wxALIGN_CENTER_VERTICAL|wxRIGHT, 5 );
	
	m_panel17->SetSizer( fgSizer6 );
	m_panel17->Layout();
	fgSizer6->Fit( m_panel17 );
	bSizer144->Add( m_panel17, 0, wxEXPAND|wxTOP, 5 );
	
	bSizer193->Add( bSizer144, 0, wxEXPAND, 0 );
	
	this->SetSizer( bSizer193 );
	this->Layout();
	
	// Connect Events
	m_checkBox_clipslice_visiable->Connect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( SD_ClippingPanel::m_checkBox_clipslice_visiableOnCheckBox ), NULL, this );
	m_pPolicyIdxBtn->Connect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( SD_ClippingPanel::OnRadioButton ), NULL, this );
	m_pPolicyCoordBtn->Connect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( SD_ClippingPanel::OnRadioButton ), NULL, this );
	m_pXPosDownBtn->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SD_ClippingPanel::OnXPosDown ), NULL, this );
	m_pXSlider->Connect( wxEVT_SCROLL_TOP, wxScrollEventHandler( SD_ClippingPanel::OnXSlideScroll ), NULL, this );
	m_pXSlider->Connect( wxEVT_SCROLL_BOTTOM, wxScrollEventHandler( SD_ClippingPanel::OnXSlideScroll ), NULL, this );
	m_pXSlider->Connect( wxEVT_SCROLL_LINEUP, wxScrollEventHandler( SD_ClippingPanel::OnXSlideScroll ), NULL, this );
	m_pXSlider->Connect( wxEVT_SCROLL_LINEDOWN, wxScrollEventHandler( SD_ClippingPanel::OnXSlideScroll ), NULL, this );
	m_pXSlider->Connect( wxEVT_SCROLL_PAGEUP, wxScrollEventHandler( SD_ClippingPanel::OnXSlideScroll ), NULL, this );
	m_pXSlider->Connect( wxEVT_SCROLL_PAGEDOWN, wxScrollEventHandler( SD_ClippingPanel::OnXSlideScroll ), NULL, this );
	m_pXSlider->Connect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( SD_ClippingPanel::OnXSlideScroll ), NULL, this );
	m_pXSlider->Connect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( SD_ClippingPanel::OnXSlideScroll ), NULL, this );
	m_pXSlider->Connect( wxEVT_SCROLL_CHANGED, wxScrollEventHandler( SD_ClippingPanel::OnXSlideScroll ), NULL, this );
	m_pXSlider->Connect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( SD_ClippingPanel::OnXSlideRelease ), NULL, this );
	m_pXSlider->Connect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( SD_ClippingPanel::OnXSlideDrag ), NULL, this );
	m_pXPosUpBtn->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SD_ClippingPanel::OnXPosUp ), NULL, this );
	m_play_check_x->Connect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( SD_ClippingPanel::m_play_check_xOnCheckBox ), NULL, this );
	m_pXPosTxt->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( SD_ClippingPanel::OnPosTxtKillFocus ), NULL, this );
	m_pXPosTxt->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( SD_ClippingPanel::OnPosTxt ), NULL, this );
	m_pXPitchTxt->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( SD_ClippingPanel::OnPitchTxtKillFocus ), NULL, this );
	m_pXPitchTxt->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( SD_ClippingPanel::OnPitchTxt ), NULL, this );
	m_pYPosDownBtn->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SD_ClippingPanel::OnYPosDown ), NULL, this );
	m_pYSlider->Connect( wxEVT_SCROLL_TOP, wxScrollEventHandler( SD_ClippingPanel::OnYSlideScroll ), NULL, this );
	m_pYSlider->Connect( wxEVT_SCROLL_BOTTOM, wxScrollEventHandler( SD_ClippingPanel::OnYSlideScroll ), NULL, this );
	m_pYSlider->Connect( wxEVT_SCROLL_LINEUP, wxScrollEventHandler( SD_ClippingPanel::OnYSlideScroll ), NULL, this );
	m_pYSlider->Connect( wxEVT_SCROLL_LINEDOWN, wxScrollEventHandler( SD_ClippingPanel::OnYSlideScroll ), NULL, this );
	m_pYSlider->Connect( wxEVT_SCROLL_PAGEUP, wxScrollEventHandler( SD_ClippingPanel::OnYSlideScroll ), NULL, this );
	m_pYSlider->Connect( wxEVT_SCROLL_PAGEDOWN, wxScrollEventHandler( SD_ClippingPanel::OnYSlideScroll ), NULL, this );
	m_pYSlider->Connect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( SD_ClippingPanel::OnYSlideScroll ), NULL, this );
	m_pYSlider->Connect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( SD_ClippingPanel::OnYSlideScroll ), NULL, this );
	m_pYSlider->Connect( wxEVT_SCROLL_CHANGED, wxScrollEventHandler( SD_ClippingPanel::OnYSlideScroll ), NULL, this );
	m_pYSlider->Connect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( SD_ClippingPanel::OnYSlideRelease ), NULL, this );
	m_pYSlider->Connect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( SD_ClippingPanel::OnYSlideDrag ), NULL, this );
	m_pYPosUpBtn->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SD_ClippingPanel::OnYPosUp ), NULL, this );
	m_play_check_y->Connect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( SD_ClippingPanel::m_play_check_yOnCheckBox ), NULL, this );
	m_pYPosTxt->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( SD_ClippingPanel::OnPosTxtKillFocus ), NULL, this );
	m_pYPosTxt->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( SD_ClippingPanel::OnPosTxt ), NULL, this );
	m_pYPitchTxt->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( SD_ClippingPanel::OnPitchTxtKillFocus ), NULL, this );
	m_pYPitchTxt->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( SD_ClippingPanel::OnPitchTxt ), NULL, this );
	m_pZPosDownBtn->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SD_ClippingPanel::OnZPosDown ), NULL, this );
	m_pZSlider->Connect( wxEVT_SCROLL_TOP, wxScrollEventHandler( SD_ClippingPanel::OnZSlideScroll ), NULL, this );
	m_pZSlider->Connect( wxEVT_SCROLL_BOTTOM, wxScrollEventHandler( SD_ClippingPanel::OnZSlideScroll ), NULL, this );
	m_pZSlider->Connect( wxEVT_SCROLL_LINEUP, wxScrollEventHandler( SD_ClippingPanel::OnZSlideScroll ), NULL, this );
	m_pZSlider->Connect( wxEVT_SCROLL_LINEDOWN, wxScrollEventHandler( SD_ClippingPanel::OnZSlideScroll ), NULL, this );
	m_pZSlider->Connect( wxEVT_SCROLL_PAGEUP, wxScrollEventHandler( SD_ClippingPanel::OnZSlideScroll ), NULL, this );
	m_pZSlider->Connect( wxEVT_SCROLL_PAGEDOWN, wxScrollEventHandler( SD_ClippingPanel::OnZSlideScroll ), NULL, this );
	m_pZSlider->Connect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( SD_ClippingPanel::OnZSlideScroll ), NULL, this );
	m_pZSlider->Connect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( SD_ClippingPanel::OnZSlideScroll ), NULL, this );
	m_pZSlider->Connect( wxEVT_SCROLL_CHANGED, wxScrollEventHandler( SD_ClippingPanel::OnZSlideScroll ), NULL, this );
	m_pZSlider->Connect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( SD_ClippingPanel::OnZSlideRelease ), NULL, this );
	m_pZSlider->Connect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( SD_ClippingPanel::OnZSlideDrag ), NULL, this );
	m_pZPosUpBtn->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SD_ClippingPanel::OnZPosUp ), NULL, this );
	m_play_check_z->Connect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( SD_ClippingPanel::m_play_check_zOnCheckBox ), NULL, this );
	m_pZPosTxt->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( SD_ClippingPanel::OnPosTxtKillFocus ), NULL, this );
	m_pZPosTxt->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( SD_ClippingPanel::OnPosTxt ), NULL, this );
	m_pZPitchTxt->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( SD_ClippingPanel::OnPitchTxtKillFocus ), NULL, this );
	m_pZPitchTxt->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( SD_ClippingPanel::OnPitchTxt ), NULL, this );
}

SD_ClippingPanel::~SD_ClippingPanel()
{
	// Disconnect Events
	m_checkBox_clipslice_visiable->Disconnect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( SD_ClippingPanel::m_checkBox_clipslice_visiableOnCheckBox ), NULL, this );
	m_pPolicyIdxBtn->Disconnect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( SD_ClippingPanel::OnRadioButton ), NULL, this );
	m_pPolicyCoordBtn->Disconnect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( SD_ClippingPanel::OnRadioButton ), NULL, this );
	m_pXPosDownBtn->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SD_ClippingPanel::OnXPosDown ), NULL, this );
	m_pXSlider->Disconnect( wxEVT_SCROLL_TOP, wxScrollEventHandler( SD_ClippingPanel::OnXSlideScroll ), NULL, this );
	m_pXSlider->Disconnect( wxEVT_SCROLL_BOTTOM, wxScrollEventHandler( SD_ClippingPanel::OnXSlideScroll ), NULL, this );
	m_pXSlider->Disconnect( wxEVT_SCROLL_LINEUP, wxScrollEventHandler( SD_ClippingPanel::OnXSlideScroll ), NULL, this );
	m_pXSlider->Disconnect( wxEVT_SCROLL_LINEDOWN, wxScrollEventHandler( SD_ClippingPanel::OnXSlideScroll ), NULL, this );
	m_pXSlider->Disconnect( wxEVT_SCROLL_PAGEUP, wxScrollEventHandler( SD_ClippingPanel::OnXSlideScroll ), NULL, this );
	m_pXSlider->Disconnect( wxEVT_SCROLL_PAGEDOWN, wxScrollEventHandler( SD_ClippingPanel::OnXSlideScroll ), NULL, this );
	m_pXSlider->Disconnect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( SD_ClippingPanel::OnXSlideScroll ), NULL, this );
	m_pXSlider->Disconnect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( SD_ClippingPanel::OnXSlideScroll ), NULL, this );
	m_pXSlider->Disconnect( wxEVT_SCROLL_CHANGED, wxScrollEventHandler( SD_ClippingPanel::OnXSlideScroll ), NULL, this );
	m_pXSlider->Disconnect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( SD_ClippingPanel::OnXSlideRelease ), NULL, this );
	m_pXSlider->Disconnect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( SD_ClippingPanel::OnXSlideDrag ), NULL, this );
	m_pXPosUpBtn->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SD_ClippingPanel::OnXPosUp ), NULL, this );
	m_play_check_x->Disconnect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( SD_ClippingPanel::m_play_check_xOnCheckBox ), NULL, this );
	m_pXPosTxt->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( SD_ClippingPanel::OnPosTxtKillFocus ), NULL, this );
	m_pXPosTxt->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( SD_ClippingPanel::OnPosTxt ), NULL, this );
	m_pXPitchTxt->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( SD_ClippingPanel::OnPitchTxtKillFocus ), NULL, this );
	m_pXPitchTxt->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( SD_ClippingPanel::OnPitchTxt ), NULL, this );
	m_pYPosDownBtn->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SD_ClippingPanel::OnYPosDown ), NULL, this );
	m_pYSlider->Disconnect( wxEVT_SCROLL_TOP, wxScrollEventHandler( SD_ClippingPanel::OnYSlideScroll ), NULL, this );
	m_pYSlider->Disconnect( wxEVT_SCROLL_BOTTOM, wxScrollEventHandler( SD_ClippingPanel::OnYSlideScroll ), NULL, this );
	m_pYSlider->Disconnect( wxEVT_SCROLL_LINEUP, wxScrollEventHandler( SD_ClippingPanel::OnYSlideScroll ), NULL, this );
	m_pYSlider->Disconnect( wxEVT_SCROLL_LINEDOWN, wxScrollEventHandler( SD_ClippingPanel::OnYSlideScroll ), NULL, this );
	m_pYSlider->Disconnect( wxEVT_SCROLL_PAGEUP, wxScrollEventHandler( SD_ClippingPanel::OnYSlideScroll ), NULL, this );
	m_pYSlider->Disconnect( wxEVT_SCROLL_PAGEDOWN, wxScrollEventHandler( SD_ClippingPanel::OnYSlideScroll ), NULL, this );
	m_pYSlider->Disconnect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( SD_ClippingPanel::OnYSlideScroll ), NULL, this );
	m_pYSlider->Disconnect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( SD_ClippingPanel::OnYSlideScroll ), NULL, this );
	m_pYSlider->Disconnect( wxEVT_SCROLL_CHANGED, wxScrollEventHandler( SD_ClippingPanel::OnYSlideScroll ), NULL, this );
	m_pYSlider->Disconnect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( SD_ClippingPanel::OnYSlideRelease ), NULL, this );
	m_pYSlider->Disconnect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( SD_ClippingPanel::OnYSlideDrag ), NULL, this );
	m_pYPosUpBtn->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SD_ClippingPanel::OnYPosUp ), NULL, this );
	m_play_check_y->Disconnect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( SD_ClippingPanel::m_play_check_yOnCheckBox ), NULL, this );
	m_pYPosTxt->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( SD_ClippingPanel::OnPosTxtKillFocus ), NULL, this );
	m_pYPosTxt->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( SD_ClippingPanel::OnPosTxt ), NULL, this );
	m_pYPitchTxt->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( SD_ClippingPanel::OnPitchTxtKillFocus ), NULL, this );
	m_pYPitchTxt->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( SD_ClippingPanel::OnPitchTxt ), NULL, this );
	m_pZPosDownBtn->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SD_ClippingPanel::OnZPosDown ), NULL, this );
	m_pZSlider->Disconnect( wxEVT_SCROLL_TOP, wxScrollEventHandler( SD_ClippingPanel::OnZSlideScroll ), NULL, this );
	m_pZSlider->Disconnect( wxEVT_SCROLL_BOTTOM, wxScrollEventHandler( SD_ClippingPanel::OnZSlideScroll ), NULL, this );
	m_pZSlider->Disconnect( wxEVT_SCROLL_LINEUP, wxScrollEventHandler( SD_ClippingPanel::OnZSlideScroll ), NULL, this );
	m_pZSlider->Disconnect( wxEVT_SCROLL_LINEDOWN, wxScrollEventHandler( SD_ClippingPanel::OnZSlideScroll ), NULL, this );
	m_pZSlider->Disconnect( wxEVT_SCROLL_PAGEUP, wxScrollEventHandler( SD_ClippingPanel::OnZSlideScroll ), NULL, this );
	m_pZSlider->Disconnect( wxEVT_SCROLL_PAGEDOWN, wxScrollEventHandler( SD_ClippingPanel::OnZSlideScroll ), NULL, this );
	m_pZSlider->Disconnect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( SD_ClippingPanel::OnZSlideScroll ), NULL, this );
	m_pZSlider->Disconnect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( SD_ClippingPanel::OnZSlideScroll ), NULL, this );
	m_pZSlider->Disconnect( wxEVT_SCROLL_CHANGED, wxScrollEventHandler( SD_ClippingPanel::OnZSlideScroll ), NULL, this );
	m_pZSlider->Disconnect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( SD_ClippingPanel::OnZSlideRelease ), NULL, this );
	m_pZSlider->Disconnect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( SD_ClippingPanel::OnZSlideDrag ), NULL, this );
	m_pZPosUpBtn->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SD_ClippingPanel::OnZPosUp ), NULL, this );
	m_play_check_z->Disconnect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( SD_ClippingPanel::m_play_check_zOnCheckBox ), NULL, this );
	m_pZPosTxt->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( SD_ClippingPanel::OnPosTxtKillFocus ), NULL, this );
	m_pZPosTxt->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( SD_ClippingPanel::OnPosTxt ), NULL, this );
	m_pZPitchTxt->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( SD_ClippingPanel::OnPitchTxtKillFocus ), NULL, this );
	m_pZPitchTxt->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( SD_ClippingPanel::OnPitchTxt ), NULL, this );
	
}

SD_GlobalPanel::SD_GlobalPanel( wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style ) : wxPanel( parent, id, pos, size, style )
{
	wxBoxSizer* bSizer144;
	bSizer144 = new wxBoxSizer( wxVERTICAL );
	
	m_pTopSizer = new wxBoxSizer( wxVERTICAL );
	
	wxBoxSizer* bSizer1441;
	bSizer1441 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText192 = new wxStaticText( this, wxID_ANY, wxT("Global"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText192->Wrap( -1 );
	bSizer1441->Add( m_staticText192, 0, wxALL, 5 );
	
	m_checkBox_global_slice_visiable = new wxCheckBox( this, wxID_ANY, wxT("Visible"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer1441->Add( m_checkBox_global_slice_visiable, 0, wxALL, 5 );
	
	m_pTopSizer->Add( bSizer1441, 0, wxEXPAND, 5 );
	
	m_panel2 = new wxPanel( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer60;
	bSizer60 = new wxBoxSizer( wxVERTICAL );
	
	wxBoxSizer* bSizer61;
	bSizer61 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText77 = new wxStaticText( m_panel2, wxID_ANY, wxT("Mode"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText77->Wrap( -1 );
	bSizer61->Add( m_staticText77, 0, wxALL, 5 );
	
	m_pPolicyIdxBtn = new wxRadioButton( m_panel2, SLICE_CTRL_IDX_RADIO, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxRB_GROUP );
	bSizer61->Add( m_pPolicyIdxBtn, 0, wxALL, 5 );
	
	m_pPolicyIdxLabel = new wxStaticText( m_panel2, wxID_ANY, wxT("Index Space"), wxDefaultPosition, wxDefaultSize, 0 );
	m_pPolicyIdxLabel->Wrap( -1 );
	bSizer61->Add( m_pPolicyIdxLabel, 0, wxALL, 5 );
	
	m_pPolicyCoordBtn = new wxRadioButton( m_panel2, SLICE_CTRL_COORD_RADIO, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizer61->Add( m_pPolicyCoordBtn, 0, wxALL, 5 );
	
	m_pPolicyCoordLabel = new wxStaticText( m_panel2, wxID_ANY, wxT("Coordinate Space"), wxDefaultPosition, wxDefaultSize, 0 );
	m_pPolicyCoordLabel->Wrap( -1 );
	bSizer61->Add( m_pPolicyCoordLabel, 0, wxALL, 5 );
	
	bSizer60->Add( bSizer61, 0, wxEXPAND, 5 );
	
	m_panel2->SetSizer( bSizer60 );
	m_panel2->Layout();
	bSizer60->Fit( m_panel2 );
	m_pTopSizer->Add( m_panel2, 0, wxEXPAND | wxALL, 0 );
	
	m_panel17 = new wxPanel( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxFlexGridSizer* fgSizer6;
	fgSizer6 = new wxFlexGridSizer( 4, 4, 0, 0 );
	fgSizer6->SetFlexibleDirection( wxBOTH );
	fgSizer6->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );
	
	m_staticText107 = new wxStaticText( m_panel17, wxID_ANY, wxT("AXIS"), wxDefaultPosition, wxSize( -1,-1 ), wxALIGN_CENTRE );
	m_staticText107->Wrap( -1 );
	fgSizer6->Add( m_staticText107, 0, wxALL, 5 );
	
	m_staticText108 = new wxStaticText( m_panel17, wxID_ANY, wxT("Play"), wxDefaultPosition, wxSize( -1,-1 ), 0 );
	m_staticText108->Wrap( -1 );
	fgSizer6->Add( m_staticText108, 1, wxALIGN_RIGHT|wxALL, 5 );
	
	m_staticText109 = new wxStaticText( m_panel17, wxID_ANY, wxT("Position"), wxDefaultPosition, wxSize( 70,-1 ), 0 );
	m_staticText109->Wrap( -1 );
	fgSizer6->Add( m_staticText109, 0, wxALL, 5 );
	
	m_staticText110 = new wxStaticText( m_panel17, wxID_ANY, wxT("Interval"), wxDefaultPosition, wxSize( 70,-1 ), 0 );
	m_staticText110->Wrap( -1 );
	fgSizer6->Add( m_staticText110, 0, wxALL, 5 );
	
	m_staticText931 = new wxStaticText( m_panel17, wxID_ANY, wxT("X"), wxDefaultPosition, wxSize( 25,-1 ), wxALIGN_CENTRE );
	m_staticText931->Wrap( -1 );
	fgSizer6->Add( m_staticText931, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	wxBoxSizer* bSizer71;
	bSizer71 = new wxBoxSizer( wxHORIZONTAL );
	
	m_pXPosDownBtn = new wxBitmapButton( m_panel17, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxSize( -1,-1 ), 0 );
	bSizer71->Add( m_pXPosDownBtn, 0, wxALIGN_CENTER_VERTICAL, 5 );
	
	m_pXSlider = new wxSlider( m_panel17, wxID_ANY, 50, 0, 100, wxDefaultPosition, wxSize( 100,-1 ), wxSL_HORIZONTAL );
	bSizer71->Add( m_pXSlider, 0, wxALIGN_CENTER_VERTICAL, 5 );
	
	m_pXPosUpBtn = new wxBitmapButton( m_panel17, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxSize( -1,-1 ), 0 );
	bSizer71->Add( m_pXPosUpBtn, 0, wxALIGN_CENTER_VERTICAL, 5 );
	
	m_play_check_x = new wxCheckBox( m_panel17, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizer71->Add( m_play_check_x, 0, wxALL, 5 );
	
	fgSizer6->Add( bSizer71, 0, 0, 5 );
	
	m_pXPosTxt = new wxTextCtrl( m_panel17, SLICE_CTRL_XPOS_TXT, wxEmptyString, wxDefaultPosition, wxSize( 70,-1 ), wxTE_PROCESS_ENTER|wxSIMPLE_BORDER );
	fgSizer6->Add( m_pXPosTxt, 0, wxALIGN_CENTER_VERTICAL, 5 );
	
	m_pXPitchTxt = new wxTextCtrl( m_panel17, SLICE_CTRL_XPITCH_TXT, wxEmptyString, wxDefaultPosition, wxSize( 70,-1 ), wxTE_PROCESS_ENTER|wxSIMPLE_BORDER );
	fgSizer6->Add( m_pXPitchTxt, 0, wxALIGN_CENTER_VERTICAL|wxRIGHT, 5 );
	
	m_staticText9311 = new wxStaticText( m_panel17, wxID_ANY, wxT("Y"), wxDefaultPosition, wxSize( 25,-1 ), wxALIGN_CENTRE );
	m_staticText9311->Wrap( -1 );
	fgSizer6->Add( m_staticText9311, 0, wxALL, 5 );
	
	wxBoxSizer* bSizer65;
	bSizer65 = new wxBoxSizer( wxHORIZONTAL );
	
	m_pYPosDownBtn = new wxBitmapButton( m_panel17, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxSize( -1,-1 ), 0 );
	bSizer65->Add( m_pYPosDownBtn, 0, wxALIGN_CENTER_VERTICAL, 5 );
	
	m_pYSlider = new wxSlider( m_panel17, wxID_ANY, 50, 0, 100, wxDefaultPosition, wxSize( 100,-1 ), wxSL_HORIZONTAL );
	bSizer65->Add( m_pYSlider, 0, wxALIGN_CENTER_VERTICAL, 5 );
	
	m_pYPosUpBtn = new wxBitmapButton( m_panel17, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxSize( -1,-1 ), 0 );
	bSizer65->Add( m_pYPosUpBtn, 0, wxALIGN_CENTER_VERTICAL, 5 );
	
	m_play_check_y = new wxCheckBox( m_panel17, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizer65->Add( m_play_check_y, 0, wxALL, 5 );
	
	fgSizer6->Add( bSizer65, 0, 0, 5 );
	
	m_pYPosTxt = new wxTextCtrl( m_panel17, SLICE_CTRL_YPOS_TXT, wxEmptyString, wxDefaultPosition, wxSize( 70,-1 ), wxTE_PROCESS_ENTER|wxSIMPLE_BORDER );
	fgSizer6->Add( m_pYPosTxt, 0, wxALIGN_CENTER_VERTICAL, 5 );
	
	m_pYPitchTxt = new wxTextCtrl( m_panel17, SLICE_CTRL_YPITCH_TXT, wxEmptyString, wxDefaultPosition, wxSize( 70,-1 ), wxTE_PROCESS_ENTER|wxSIMPLE_BORDER );
	fgSizer6->Add( m_pYPitchTxt, 0, wxALIGN_CENTER_VERTICAL|wxRIGHT, 5 );
	
	m_staticText9312 = new wxStaticText( m_panel17, wxID_ANY, wxT("Z"), wxDefaultPosition, wxSize( 25,-1 ), wxALIGN_CENTRE );
	m_staticText9312->Wrap( -1 );
	fgSizer6->Add( m_staticText9312, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	wxBoxSizer* bSizer651;
	bSizer651 = new wxBoxSizer( wxHORIZONTAL );
	
	m_pZPosDownBtn = new wxBitmapButton( m_panel17, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxSize( -1,-1 ), 0 );
	bSizer651->Add( m_pZPosDownBtn, 0, wxALIGN_CENTER_VERTICAL, 5 );
	
	m_pZSlider = new wxSlider( m_panel17, wxID_ANY, 50, 0, 100, wxDefaultPosition, wxSize( 100,-1 ), wxSL_HORIZONTAL );
	bSizer651->Add( m_pZSlider, 0, wxALIGN_CENTER_VERTICAL, 5 );
	
	m_pZPosUpBtn = new wxBitmapButton( m_panel17, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxSize( -1,-1 ), 0 );
	bSizer651->Add( m_pZPosUpBtn, 0, wxALIGN_CENTER_VERTICAL, 5 );
	
	m_play_check_z = new wxCheckBox( m_panel17, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizer651->Add( m_play_check_z, 0, wxALL, 5 );
	
	fgSizer6->Add( bSizer651, 0, 0, 5 );
	
	m_pZPosTxt = new wxTextCtrl( m_panel17, SLICE_CTRL_ZPOS_TXT, wxEmptyString, wxDefaultPosition, wxSize( 70,-1 ), wxTE_PROCESS_ENTER|wxSIMPLE_BORDER );
	fgSizer6->Add( m_pZPosTxt, 0, wxALIGN_CENTER_VERTICAL, 5 );
	
	m_pZPitchTxt = new wxTextCtrl( m_panel17, SLICE_CTRL_ZPITCH_TXT, wxEmptyString, wxDefaultPosition, wxSize( 70,-1 ), wxTE_PROCESS_ENTER|wxSIMPLE_BORDER );
	fgSizer6->Add( m_pZPitchTxt, 0, wxALIGN_CENTER_VERTICAL|wxRIGHT, 5 );
	
	m_panel17->SetSizer( fgSizer6 );
	m_panel17->Layout();
	fgSizer6->Fit( m_panel17 );
	m_pTopSizer->Add( m_panel17, 0, wxEXPAND|wxTOP, 5 );
	
	bSizer144->Add( m_pTopSizer, 0, wxEXPAND, 0 );
	
	this->SetSizer( bSizer144 );
	this->Layout();
	
	// Connect Events
	m_checkBox_global_slice_visiable->Connect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( SD_GlobalPanel::m_checkBox_global_slice_visiableOnCheckBox ), NULL, this );
	m_pPolicyIdxBtn->Connect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( SD_GlobalPanel::OnRadioButton ), NULL, this );
	m_pPolicyCoordBtn->Connect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( SD_GlobalPanel::OnRadioButton ), NULL, this );
	m_pXPosDownBtn->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SD_GlobalPanel::OnXPosDown ), NULL, this );
	m_pXSlider->Connect( wxEVT_SCROLL_TOP, wxScrollEventHandler( SD_GlobalPanel::OnXSlideScroll ), NULL, this );
	m_pXSlider->Connect( wxEVT_SCROLL_BOTTOM, wxScrollEventHandler( SD_GlobalPanel::OnXSlideScroll ), NULL, this );
	m_pXSlider->Connect( wxEVT_SCROLL_LINEUP, wxScrollEventHandler( SD_GlobalPanel::OnXSlideScroll ), NULL, this );
	m_pXSlider->Connect( wxEVT_SCROLL_LINEDOWN, wxScrollEventHandler( SD_GlobalPanel::OnXSlideScroll ), NULL, this );
	m_pXSlider->Connect( wxEVT_SCROLL_PAGEUP, wxScrollEventHandler( SD_GlobalPanel::OnXSlideScroll ), NULL, this );
	m_pXSlider->Connect( wxEVT_SCROLL_PAGEDOWN, wxScrollEventHandler( SD_GlobalPanel::OnXSlideScroll ), NULL, this );
	m_pXSlider->Connect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( SD_GlobalPanel::OnXSlideScroll ), NULL, this );
	m_pXSlider->Connect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( SD_GlobalPanel::OnXSlideScroll ), NULL, this );
	m_pXSlider->Connect( wxEVT_SCROLL_CHANGED, wxScrollEventHandler( SD_GlobalPanel::OnXSlideScroll ), NULL, this );
	m_pXSlider->Connect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( SD_GlobalPanel::OnXSlideRelease ), NULL, this );
	m_pXSlider->Connect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( SD_GlobalPanel::OnXSlideDrag ), NULL, this );
	m_pXPosUpBtn->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SD_GlobalPanel::OnXPosUp ), NULL, this );
	m_play_check_x->Connect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( SD_GlobalPanel::m_play_check_xOnCheckBox ), NULL, this );
	m_pXPosTxt->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( SD_GlobalPanel::OnPosTxtKillFocus ), NULL, this );
	m_pXPosTxt->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( SD_GlobalPanel::OnPosTxt ), NULL, this );
	m_pXPitchTxt->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( SD_GlobalPanel::OnPitchTxtKillFocus ), NULL, this );
	m_pXPitchTxt->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( SD_GlobalPanel::OnPitchTxt ), NULL, this );
	m_pYPosDownBtn->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SD_GlobalPanel::OnYPosDown ), NULL, this );
	m_pYSlider->Connect( wxEVT_SCROLL_TOP, wxScrollEventHandler( SD_GlobalPanel::OnYSlideScroll ), NULL, this );
	m_pYSlider->Connect( wxEVT_SCROLL_BOTTOM, wxScrollEventHandler( SD_GlobalPanel::OnYSlideScroll ), NULL, this );
	m_pYSlider->Connect( wxEVT_SCROLL_LINEUP, wxScrollEventHandler( SD_GlobalPanel::OnYSlideScroll ), NULL, this );
	m_pYSlider->Connect( wxEVT_SCROLL_LINEDOWN, wxScrollEventHandler( SD_GlobalPanel::OnYSlideScroll ), NULL, this );
	m_pYSlider->Connect( wxEVT_SCROLL_PAGEUP, wxScrollEventHandler( SD_GlobalPanel::OnYSlideScroll ), NULL, this );
	m_pYSlider->Connect( wxEVT_SCROLL_PAGEDOWN, wxScrollEventHandler( SD_GlobalPanel::OnYSlideScroll ), NULL, this );
	m_pYSlider->Connect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( SD_GlobalPanel::OnYSlideScroll ), NULL, this );
	m_pYSlider->Connect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( SD_GlobalPanel::OnYSlideScroll ), NULL, this );
	m_pYSlider->Connect( wxEVT_SCROLL_CHANGED, wxScrollEventHandler( SD_GlobalPanel::OnYSlideScroll ), NULL, this );
	m_pYSlider->Connect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( SD_GlobalPanel::OnYSlideRelease ), NULL, this );
	m_pYSlider->Connect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( SD_GlobalPanel::OnYSlideDrag ), NULL, this );
	m_pYPosUpBtn->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SD_GlobalPanel::OnYPosUp ), NULL, this );
	m_play_check_y->Connect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( SD_GlobalPanel::m_play_check_yOnCheckBox ), NULL, this );
	m_pYPosTxt->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( SD_GlobalPanel::OnPosTxtKillFocus ), NULL, this );
	m_pYPosTxt->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( SD_GlobalPanel::OnPosTxt ), NULL, this );
	m_pYPitchTxt->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( SD_GlobalPanel::OnPitchTxtKillFocus ), NULL, this );
	m_pYPitchTxt->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( SD_GlobalPanel::OnPitchTxt ), NULL, this );
	m_pZPosDownBtn->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SD_GlobalPanel::OnZPosDown ), NULL, this );
	m_pZSlider->Connect( wxEVT_SCROLL_TOP, wxScrollEventHandler( SD_GlobalPanel::OnZSlideScroll ), NULL, this );
	m_pZSlider->Connect( wxEVT_SCROLL_BOTTOM, wxScrollEventHandler( SD_GlobalPanel::OnZSlideScroll ), NULL, this );
	m_pZSlider->Connect( wxEVT_SCROLL_LINEUP, wxScrollEventHandler( SD_GlobalPanel::OnZSlideScroll ), NULL, this );
	m_pZSlider->Connect( wxEVT_SCROLL_LINEDOWN, wxScrollEventHandler( SD_GlobalPanel::OnZSlideScroll ), NULL, this );
	m_pZSlider->Connect( wxEVT_SCROLL_PAGEUP, wxScrollEventHandler( SD_GlobalPanel::OnZSlideScroll ), NULL, this );
	m_pZSlider->Connect( wxEVT_SCROLL_PAGEDOWN, wxScrollEventHandler( SD_GlobalPanel::OnZSlideScroll ), NULL, this );
	m_pZSlider->Connect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( SD_GlobalPanel::OnZSlideScroll ), NULL, this );
	m_pZSlider->Connect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( SD_GlobalPanel::OnZSlideScroll ), NULL, this );
	m_pZSlider->Connect( wxEVT_SCROLL_CHANGED, wxScrollEventHandler( SD_GlobalPanel::OnZSlideScroll ), NULL, this );
	m_pZSlider->Connect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( SD_GlobalPanel::OnZSlideRelease ), NULL, this );
	m_pZSlider->Connect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( SD_GlobalPanel::OnZSlideDrag ), NULL, this );
	m_pZPosUpBtn->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SD_GlobalPanel::OnZPosUp ), NULL, this );
	m_play_check_z->Connect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( SD_GlobalPanel::m_play_check_zOnCheckBox ), NULL, this );
	m_pZPosTxt->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( SD_GlobalPanel::OnPosTxtKillFocus ), NULL, this );
	m_pZPosTxt->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( SD_GlobalPanel::OnPosTxt ), NULL, this );
	m_pZPitchTxt->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( SD_GlobalPanel::OnPitchTxtKillFocus ), NULL, this );
	m_pZPitchTxt->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( SD_GlobalPanel::OnPitchTxt ), NULL, this );
}

SD_GlobalPanel::~SD_GlobalPanel()
{
	// Disconnect Events
	m_checkBox_global_slice_visiable->Disconnect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( SD_GlobalPanel::m_checkBox_global_slice_visiableOnCheckBox ), NULL, this );
	m_pPolicyIdxBtn->Disconnect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( SD_GlobalPanel::OnRadioButton ), NULL, this );
	m_pPolicyCoordBtn->Disconnect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( SD_GlobalPanel::OnRadioButton ), NULL, this );
	m_pXPosDownBtn->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SD_GlobalPanel::OnXPosDown ), NULL, this );
	m_pXSlider->Disconnect( wxEVT_SCROLL_TOP, wxScrollEventHandler( SD_GlobalPanel::OnXSlideScroll ), NULL, this );
	m_pXSlider->Disconnect( wxEVT_SCROLL_BOTTOM, wxScrollEventHandler( SD_GlobalPanel::OnXSlideScroll ), NULL, this );
	m_pXSlider->Disconnect( wxEVT_SCROLL_LINEUP, wxScrollEventHandler( SD_GlobalPanel::OnXSlideScroll ), NULL, this );
	m_pXSlider->Disconnect( wxEVT_SCROLL_LINEDOWN, wxScrollEventHandler( SD_GlobalPanel::OnXSlideScroll ), NULL, this );
	m_pXSlider->Disconnect( wxEVT_SCROLL_PAGEUP, wxScrollEventHandler( SD_GlobalPanel::OnXSlideScroll ), NULL, this );
	m_pXSlider->Disconnect( wxEVT_SCROLL_PAGEDOWN, wxScrollEventHandler( SD_GlobalPanel::OnXSlideScroll ), NULL, this );
	m_pXSlider->Disconnect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( SD_GlobalPanel::OnXSlideScroll ), NULL, this );
	m_pXSlider->Disconnect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( SD_GlobalPanel::OnXSlideScroll ), NULL, this );
	m_pXSlider->Disconnect( wxEVT_SCROLL_CHANGED, wxScrollEventHandler( SD_GlobalPanel::OnXSlideScroll ), NULL, this );
	m_pXSlider->Disconnect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( SD_GlobalPanel::OnXSlideRelease ), NULL, this );
	m_pXSlider->Disconnect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( SD_GlobalPanel::OnXSlideDrag ), NULL, this );
	m_pXPosUpBtn->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SD_GlobalPanel::OnXPosUp ), NULL, this );
	m_play_check_x->Disconnect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( SD_GlobalPanel::m_play_check_xOnCheckBox ), NULL, this );
	m_pXPosTxt->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( SD_GlobalPanel::OnPosTxtKillFocus ), NULL, this );
	m_pXPosTxt->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( SD_GlobalPanel::OnPosTxt ), NULL, this );
	m_pXPitchTxt->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( SD_GlobalPanel::OnPitchTxtKillFocus ), NULL, this );
	m_pXPitchTxt->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( SD_GlobalPanel::OnPitchTxt ), NULL, this );
	m_pYPosDownBtn->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SD_GlobalPanel::OnYPosDown ), NULL, this );
	m_pYSlider->Disconnect( wxEVT_SCROLL_TOP, wxScrollEventHandler( SD_GlobalPanel::OnYSlideScroll ), NULL, this );
	m_pYSlider->Disconnect( wxEVT_SCROLL_BOTTOM, wxScrollEventHandler( SD_GlobalPanel::OnYSlideScroll ), NULL, this );
	m_pYSlider->Disconnect( wxEVT_SCROLL_LINEUP, wxScrollEventHandler( SD_GlobalPanel::OnYSlideScroll ), NULL, this );
	m_pYSlider->Disconnect( wxEVT_SCROLL_LINEDOWN, wxScrollEventHandler( SD_GlobalPanel::OnYSlideScroll ), NULL, this );
	m_pYSlider->Disconnect( wxEVT_SCROLL_PAGEUP, wxScrollEventHandler( SD_GlobalPanel::OnYSlideScroll ), NULL, this );
	m_pYSlider->Disconnect( wxEVT_SCROLL_PAGEDOWN, wxScrollEventHandler( SD_GlobalPanel::OnYSlideScroll ), NULL, this );
	m_pYSlider->Disconnect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( SD_GlobalPanel::OnYSlideScroll ), NULL, this );
	m_pYSlider->Disconnect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( SD_GlobalPanel::OnYSlideScroll ), NULL, this );
	m_pYSlider->Disconnect( wxEVT_SCROLL_CHANGED, wxScrollEventHandler( SD_GlobalPanel::OnYSlideScroll ), NULL, this );
	m_pYSlider->Disconnect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( SD_GlobalPanel::OnYSlideRelease ), NULL, this );
	m_pYSlider->Disconnect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( SD_GlobalPanel::OnYSlideDrag ), NULL, this );
	m_pYPosUpBtn->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SD_GlobalPanel::OnYPosUp ), NULL, this );
	m_play_check_y->Disconnect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( SD_GlobalPanel::m_play_check_yOnCheckBox ), NULL, this );
	m_pYPosTxt->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( SD_GlobalPanel::OnPosTxtKillFocus ), NULL, this );
	m_pYPosTxt->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( SD_GlobalPanel::OnPosTxt ), NULL, this );
	m_pYPitchTxt->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( SD_GlobalPanel::OnPitchTxtKillFocus ), NULL, this );
	m_pYPitchTxt->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( SD_GlobalPanel::OnPitchTxt ), NULL, this );
	m_pZPosDownBtn->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SD_GlobalPanel::OnZPosDown ), NULL, this );
	m_pZSlider->Disconnect( wxEVT_SCROLL_TOP, wxScrollEventHandler( SD_GlobalPanel::OnZSlideScroll ), NULL, this );
	m_pZSlider->Disconnect( wxEVT_SCROLL_BOTTOM, wxScrollEventHandler( SD_GlobalPanel::OnZSlideScroll ), NULL, this );
	m_pZSlider->Disconnect( wxEVT_SCROLL_LINEUP, wxScrollEventHandler( SD_GlobalPanel::OnZSlideScroll ), NULL, this );
	m_pZSlider->Disconnect( wxEVT_SCROLL_LINEDOWN, wxScrollEventHandler( SD_GlobalPanel::OnZSlideScroll ), NULL, this );
	m_pZSlider->Disconnect( wxEVT_SCROLL_PAGEUP, wxScrollEventHandler( SD_GlobalPanel::OnZSlideScroll ), NULL, this );
	m_pZSlider->Disconnect( wxEVT_SCROLL_PAGEDOWN, wxScrollEventHandler( SD_GlobalPanel::OnZSlideScroll ), NULL, this );
	m_pZSlider->Disconnect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( SD_GlobalPanel::OnZSlideScroll ), NULL, this );
	m_pZSlider->Disconnect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( SD_GlobalPanel::OnZSlideScroll ), NULL, this );
	m_pZSlider->Disconnect( wxEVT_SCROLL_CHANGED, wxScrollEventHandler( SD_GlobalPanel::OnZSlideScroll ), NULL, this );
	m_pZSlider->Disconnect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( SD_GlobalPanel::OnZSlideRelease ), NULL, this );
	m_pZSlider->Disconnect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( SD_GlobalPanel::OnZSlideDrag ), NULL, this );
	m_pZPosUpBtn->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SD_GlobalPanel::OnZPosUp ), NULL, this );
	m_play_check_z->Disconnect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( SD_GlobalPanel::m_play_check_zOnCheckBox ), NULL, this );
	m_pZPosTxt->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( SD_GlobalPanel::OnPosTxtKillFocus ), NULL, this );
	m_pZPosTxt->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( SD_GlobalPanel::OnPosTxt ), NULL, this );
	m_pZPitchTxt->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( SD_GlobalPanel::OnPitchTxtKillFocus ), NULL, this );
	m_pZPitchTxt->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( SD_GlobalPanel::OnPitchTxt ), NULL, this );
	
}

SD_LocalPanel::SD_LocalPanel( wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style ) : wxPanel( parent, id, pos, size, style )
{
	wxBoxSizer* bSizer144;
	bSizer144 = new wxBoxSizer( wxVERTICAL );
	
	m_pTopSizer = new wxBoxSizer( wxVERTICAL );
	
	wxBoxSizer* bSizer145;
	bSizer145 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText193 = new wxStaticText( this, wxID_ANY, wxT("Local "), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText193->Wrap( -1 );
	bSizer145->Add( m_staticText193, 0, wxALL, 5 );
	
	m_checkBox_local_slice_visiable = new wxCheckBox( this, wxID_ANY, wxT("Visible"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer145->Add( m_checkBox_local_slice_visiable, 0, wxALL, 5 );
	
	m_pTopSizer->Add( bSizer145, 0, wxEXPAND, 5 );
	
	m_panel2 = new wxPanel( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer60;
	bSizer60 = new wxBoxSizer( wxVERTICAL );
	
	wxBoxSizer* bSizer61;
	bSizer61 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText77 = new wxStaticText( m_panel2, wxID_ANY, wxT("Mode"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText77->Wrap( -1 );
	bSizer61->Add( m_staticText77, 0, wxALL, 5 );
	
	m_pPolicyIdxBtn = new wxRadioButton( m_panel2, SLICE_CTRL_IDX_RADIO, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxRB_GROUP );
	bSizer61->Add( m_pPolicyIdxBtn, 0, wxALL, 5 );
	
	m_pPolicyIdxLabel = new wxStaticText( m_panel2, wxID_ANY, wxT("Index Space"), wxDefaultPosition, wxDefaultSize, 0 );
	m_pPolicyIdxLabel->Wrap( -1 );
	bSizer61->Add( m_pPolicyIdxLabel, 0, wxALL, 5 );
	
	m_pPolicyCoordBtn = new wxRadioButton( m_panel2, SLICE_CTRL_COORD_RADIO, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizer61->Add( m_pPolicyCoordBtn, 0, wxALL, 5 );
	
	m_pPolicyCoordLabel = new wxStaticText( m_panel2, wxID_ANY, wxT("Coordinate Space"), wxDefaultPosition, wxDefaultSize, 0 );
	m_pPolicyCoordLabel->Wrap( -1 );
	bSizer61->Add( m_pPolicyCoordLabel, 0, wxALL, 5 );
	
	bSizer60->Add( bSizer61, 0, wxEXPAND, 5 );
	
	m_panel2->SetSizer( bSizer60 );
	m_panel2->Layout();
	bSizer60->Fit( m_panel2 );
	m_pTopSizer->Add( m_panel2, 0, wxALL|wxEXPAND, 0 );
	
	m_panel17 = new wxPanel( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxFlexGridSizer* fgSizer6;
	fgSizer6 = new wxFlexGridSizer( 4, 4, 0, 0 );
	fgSizer6->SetFlexibleDirection( wxBOTH );
	fgSizer6->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );
	
	m_staticText107 = new wxStaticText( m_panel17, wxID_ANY, wxT("AXIS"), wxDefaultPosition, wxSize( -1,-1 ), wxALIGN_CENTRE );
	m_staticText107->Wrap( -1 );
	fgSizer6->Add( m_staticText107, 0, wxALL, 5 );
	
	m_staticText108 = new wxStaticText( m_panel17, wxID_ANY, wxT("Play"), wxDefaultPosition, wxSize( -1,-1 ), 0 );
	m_staticText108->Wrap( -1 );
	fgSizer6->Add( m_staticText108, 1, wxALIGN_RIGHT|wxALL, 5 );
	
	m_staticText109 = new wxStaticText( m_panel17, wxID_ANY, wxT("Position"), wxDefaultPosition, wxSize( 70,-1 ), 0 );
	m_staticText109->Wrap( -1 );
	fgSizer6->Add( m_staticText109, 0, wxALL, 5 );
	
	m_staticText110 = new wxStaticText( m_panel17, wxID_ANY, wxT("Interval"), wxDefaultPosition, wxSize( 70,-1 ), 0 );
	m_staticText110->Wrap( -1 );
	fgSizer6->Add( m_staticText110, 0, wxALL, 5 );
	
	m_staticText931 = new wxStaticText( m_panel17, wxID_ANY, wxT("X"), wxDefaultPosition, wxSize( 25,-1 ), wxALIGN_CENTRE );
	m_staticText931->Wrap( -1 );
	fgSizer6->Add( m_staticText931, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	wxBoxSizer* bSizer71;
	bSizer71 = new wxBoxSizer( wxHORIZONTAL );
	
	m_pXPosDownBtn = new wxBitmapButton( m_panel17, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxSize( -1,-1 ), 0 );
	bSizer71->Add( m_pXPosDownBtn, 0, wxALIGN_CENTER_VERTICAL, 5 );
	
	m_pXSlider = new wxSlider( m_panel17, wxID_ANY, 50, 0, 100, wxDefaultPosition, wxSize( 100,-1 ), wxSL_HORIZONTAL );
	bSizer71->Add( m_pXSlider, 0, wxALIGN_CENTER_VERTICAL, 5 );
	
	m_pXPosUpBtn = new wxBitmapButton( m_panel17, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxSize( -1,-1 ), 0 );
	bSizer71->Add( m_pXPosUpBtn, 0, wxALIGN_CENTER_VERTICAL, 5 );
	
	m_play_check_x = new wxCheckBox( m_panel17, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizer71->Add( m_play_check_x, 0, wxALL, 5 );
	
	fgSizer6->Add( bSizer71, 0, 0, 5 );
	
	m_pXPosTxt = new wxTextCtrl( m_panel17, SLICE_CTRL_XPOS_TXT, wxEmptyString, wxDefaultPosition, wxSize( 70,-1 ), wxTE_PROCESS_ENTER|wxSIMPLE_BORDER );
	fgSizer6->Add( m_pXPosTxt, 0, wxALIGN_CENTER_VERTICAL, 5 );
	
	m_pXPitchTxt = new wxTextCtrl( m_panel17, SLICE_CTRL_XPITCH_TXT, wxEmptyString, wxDefaultPosition, wxSize( 70,-1 ), wxTE_PROCESS_ENTER|wxSIMPLE_BORDER );
	fgSizer6->Add( m_pXPitchTxt, 0, wxALIGN_CENTER_VERTICAL|wxRIGHT, 5 );
	
	m_staticText9311 = new wxStaticText( m_panel17, wxID_ANY, wxT("Y"), wxDefaultPosition, wxSize( 25,-1 ), wxALIGN_CENTRE );
	m_staticText9311->Wrap( -1 );
	fgSizer6->Add( m_staticText9311, 0, wxALL, 5 );
	
	wxBoxSizer* bSizer65;
	bSizer65 = new wxBoxSizer( wxHORIZONTAL );
	
	m_pYPosDownBtn = new wxBitmapButton( m_panel17, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxSize( -1,-1 ), 0 );
	bSizer65->Add( m_pYPosDownBtn, 0, wxALIGN_CENTER_VERTICAL, 5 );
	
	m_pYSlider = new wxSlider( m_panel17, wxID_ANY, 50, 0, 100, wxDefaultPosition, wxSize( 100,-1 ), wxSL_HORIZONTAL );
	bSizer65->Add( m_pYSlider, 0, wxALIGN_CENTER_VERTICAL, 5 );
	
	m_pYPosUpBtn = new wxBitmapButton( m_panel17, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxSize( -1,-1 ), 0 );
	bSizer65->Add( m_pYPosUpBtn, 0, wxALIGN_CENTER_VERTICAL, 5 );
	
	m_play_check_y = new wxCheckBox( m_panel17, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizer65->Add( m_play_check_y, 0, wxALL, 5 );
	
	fgSizer6->Add( bSizer65, 0, 0, 5 );
	
	m_pYPosTxt = new wxTextCtrl( m_panel17, SLICE_CTRL_YPOS_TXT, wxEmptyString, wxDefaultPosition, wxSize( 70,-1 ), wxTE_PROCESS_ENTER|wxSIMPLE_BORDER );
	fgSizer6->Add( m_pYPosTxt, 0, wxALIGN_CENTER_VERTICAL, 5 );
	
	m_pYPitchTxt = new wxTextCtrl( m_panel17, SLICE_CTRL_YPITCH_TXT, wxEmptyString, wxDefaultPosition, wxSize( 70,-1 ), wxTE_PROCESS_ENTER|wxSIMPLE_BORDER );
	fgSizer6->Add( m_pYPitchTxt, 0, wxALIGN_CENTER_VERTICAL|wxRIGHT, 5 );
	
	m_staticText9312 = new wxStaticText( m_panel17, wxID_ANY, wxT("Z"), wxDefaultPosition, wxSize( 25,-1 ), wxALIGN_CENTRE );
	m_staticText9312->Wrap( -1 );
	fgSizer6->Add( m_staticText9312, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	wxBoxSizer* bSizer651;
	bSizer651 = new wxBoxSizer( wxHORIZONTAL );
	
	m_pZPosDownBtn = new wxBitmapButton( m_panel17, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxSize( -1,-1 ), 0 );
	bSizer651->Add( m_pZPosDownBtn, 0, wxALIGN_CENTER_VERTICAL, 5 );
	
	m_pZSlider = new wxSlider( m_panel17, wxID_ANY, 50, 0, 100, wxDefaultPosition, wxSize( 100,-1 ), wxSL_HORIZONTAL );
	bSizer651->Add( m_pZSlider, 0, wxALIGN_CENTER_VERTICAL, 5 );
	
	m_pZPosUpBtn = new wxBitmapButton( m_panel17, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxSize( -1,-1 ), 0 );
	bSizer651->Add( m_pZPosUpBtn, 0, wxALIGN_CENTER_VERTICAL, 5 );
	
	m_play_check_z = new wxCheckBox( m_panel17, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizer651->Add( m_play_check_z, 0, wxALL, 5 );
	
	fgSizer6->Add( bSizer651, 0, 0, 5 );
	
	m_pZPosTxt = new wxTextCtrl( m_panel17, SLICE_CTRL_ZPOS_TXT, wxEmptyString, wxDefaultPosition, wxSize( 70,-1 ), wxTE_PROCESS_ENTER|wxSIMPLE_BORDER );
	fgSizer6->Add( m_pZPosTxt, 0, wxALIGN_CENTER_VERTICAL, 5 );
	
	m_pZPitchTxt = new wxTextCtrl( m_panel17, SLICE_CTRL_ZPITCH_TXT, wxEmptyString, wxDefaultPosition, wxSize( 70,-1 ), wxTE_PROCESS_ENTER|wxSIMPLE_BORDER );
	fgSizer6->Add( m_pZPitchTxt, 0, wxALIGN_CENTER_VERTICAL|wxRIGHT, 5 );
	
	m_panel17->SetSizer( fgSizer6 );
	m_panel17->Layout();
	fgSizer6->Fit( m_panel17 );
	m_pTopSizer->Add( m_panel17, 0, wxEXPAND|wxTOP, 5 );
	
	bSizer144->Add( m_pTopSizer, 0, wxEXPAND, 0 );
	
	this->SetSizer( bSizer144 );
	this->Layout();
	
	// Connect Events
	m_checkBox_local_slice_visiable->Connect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( SD_LocalPanel::m_checkBox_local_slice_visiableOnCheckBox ), NULL, this );
	m_pPolicyIdxBtn->Connect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( SD_LocalPanel::OnRadioButton ), NULL, this );
	m_pPolicyCoordBtn->Connect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( SD_LocalPanel::OnRadioButton ), NULL, this );
	m_pXPosDownBtn->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SD_LocalPanel::OnXPosDown ), NULL, this );
	m_pXSlider->Connect( wxEVT_SCROLL_TOP, wxScrollEventHandler( SD_LocalPanel::OnXSlideScroll ), NULL, this );
	m_pXSlider->Connect( wxEVT_SCROLL_BOTTOM, wxScrollEventHandler( SD_LocalPanel::OnXSlideScroll ), NULL, this );
	m_pXSlider->Connect( wxEVT_SCROLL_LINEUP, wxScrollEventHandler( SD_LocalPanel::OnXSlideScroll ), NULL, this );
	m_pXSlider->Connect( wxEVT_SCROLL_LINEDOWN, wxScrollEventHandler( SD_LocalPanel::OnXSlideScroll ), NULL, this );
	m_pXSlider->Connect( wxEVT_SCROLL_PAGEUP, wxScrollEventHandler( SD_LocalPanel::OnXSlideScroll ), NULL, this );
	m_pXSlider->Connect( wxEVT_SCROLL_PAGEDOWN, wxScrollEventHandler( SD_LocalPanel::OnXSlideScroll ), NULL, this );
	m_pXSlider->Connect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( SD_LocalPanel::OnXSlideScroll ), NULL, this );
	m_pXSlider->Connect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( SD_LocalPanel::OnXSlideScroll ), NULL, this );
	m_pXSlider->Connect( wxEVT_SCROLL_CHANGED, wxScrollEventHandler( SD_LocalPanel::OnXSlideScroll ), NULL, this );
	m_pXSlider->Connect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( SD_LocalPanel::OnXSlideRelease ), NULL, this );
	m_pXSlider->Connect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( SD_LocalPanel::OnXSlideDrag ), NULL, this );
	m_pXPosUpBtn->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SD_LocalPanel::OnXPosUp ), NULL, this );
	m_play_check_x->Connect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( SD_LocalPanel::m_play_check_xOnCheckBox ), NULL, this );
	m_pXPosTxt->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( SD_LocalPanel::OnPosTxtKillFocus ), NULL, this );
	m_pXPosTxt->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( SD_LocalPanel::OnPosTxt ), NULL, this );
	m_pXPitchTxt->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( SD_LocalPanel::OnPitchTxtKillFocus ), NULL, this );
	m_pXPitchTxt->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( SD_LocalPanel::OnPitchTxt ), NULL, this );
	m_pYPosDownBtn->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SD_LocalPanel::OnYPosDown ), NULL, this );
	m_pYSlider->Connect( wxEVT_SCROLL_TOP, wxScrollEventHandler( SD_LocalPanel::OnYSlideScroll ), NULL, this );
	m_pYSlider->Connect( wxEVT_SCROLL_BOTTOM, wxScrollEventHandler( SD_LocalPanel::OnYSlideScroll ), NULL, this );
	m_pYSlider->Connect( wxEVT_SCROLL_LINEUP, wxScrollEventHandler( SD_LocalPanel::OnYSlideScroll ), NULL, this );
	m_pYSlider->Connect( wxEVT_SCROLL_LINEDOWN, wxScrollEventHandler( SD_LocalPanel::OnYSlideScroll ), NULL, this );
	m_pYSlider->Connect( wxEVT_SCROLL_PAGEUP, wxScrollEventHandler( SD_LocalPanel::OnYSlideScroll ), NULL, this );
	m_pYSlider->Connect( wxEVT_SCROLL_PAGEDOWN, wxScrollEventHandler( SD_LocalPanel::OnYSlideScroll ), NULL, this );
	m_pYSlider->Connect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( SD_LocalPanel::OnYSlideScroll ), NULL, this );
	m_pYSlider->Connect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( SD_LocalPanel::OnYSlideScroll ), NULL, this );
	m_pYSlider->Connect( wxEVT_SCROLL_CHANGED, wxScrollEventHandler( SD_LocalPanel::OnYSlideScroll ), NULL, this );
	m_pYSlider->Connect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( SD_LocalPanel::OnYSlideRelease ), NULL, this );
	m_pYSlider->Connect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( SD_LocalPanel::OnYSlideDrag ), NULL, this );
	m_pYPosUpBtn->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SD_LocalPanel::OnYPosUp ), NULL, this );
	m_play_check_y->Connect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( SD_LocalPanel::m_play_check_yOnCheckBox ), NULL, this );
	m_pYPosTxt->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( SD_LocalPanel::OnPosTxtKillFocus ), NULL, this );
	m_pYPosTxt->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( SD_LocalPanel::OnPosTxt ), NULL, this );
	m_pYPitchTxt->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( SD_LocalPanel::OnPitchTxtKillFocus ), NULL, this );
	m_pYPitchTxt->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( SD_LocalPanel::OnPitchTxt ), NULL, this );
	m_pZPosDownBtn->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SD_LocalPanel::OnZPosDown ), NULL, this );
	m_pZSlider->Connect( wxEVT_SCROLL_TOP, wxScrollEventHandler( SD_LocalPanel::OnZSlideScroll ), NULL, this );
	m_pZSlider->Connect( wxEVT_SCROLL_BOTTOM, wxScrollEventHandler( SD_LocalPanel::OnZSlideScroll ), NULL, this );
	m_pZSlider->Connect( wxEVT_SCROLL_LINEUP, wxScrollEventHandler( SD_LocalPanel::OnZSlideScroll ), NULL, this );
	m_pZSlider->Connect( wxEVT_SCROLL_LINEDOWN, wxScrollEventHandler( SD_LocalPanel::OnZSlideScroll ), NULL, this );
	m_pZSlider->Connect( wxEVT_SCROLL_PAGEUP, wxScrollEventHandler( SD_LocalPanel::OnZSlideScroll ), NULL, this );
	m_pZSlider->Connect( wxEVT_SCROLL_PAGEDOWN, wxScrollEventHandler( SD_LocalPanel::OnZSlideScroll ), NULL, this );
	m_pZSlider->Connect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( SD_LocalPanel::OnZSlideScroll ), NULL, this );
	m_pZSlider->Connect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( SD_LocalPanel::OnZSlideScroll ), NULL, this );
	m_pZSlider->Connect( wxEVT_SCROLL_CHANGED, wxScrollEventHandler( SD_LocalPanel::OnZSlideScroll ), NULL, this );
	m_pZSlider->Connect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( SD_LocalPanel::OnZSlideRelease ), NULL, this );
	m_pZSlider->Connect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( SD_LocalPanel::OnZSlideDrag ), NULL, this );
	m_pZPosUpBtn->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SD_LocalPanel::OnZPosUp ), NULL, this );
	m_play_check_z->Connect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( SD_LocalPanel::m_play_check_zOnCheckBox ), NULL, this );
	m_pZPosTxt->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( SD_LocalPanel::OnPosTxtKillFocus ), NULL, this );
	m_pZPosTxt->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( SD_LocalPanel::OnPosTxt ), NULL, this );
	m_pZPitchTxt->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( SD_LocalPanel::OnPitchTxtKillFocus ), NULL, this );
	m_pZPitchTxt->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( SD_LocalPanel::OnPitchTxt ), NULL, this );
}

SD_LocalPanel::~SD_LocalPanel()
{
	// Disconnect Events
	m_checkBox_local_slice_visiable->Disconnect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( SD_LocalPanel::m_checkBox_local_slice_visiableOnCheckBox ), NULL, this );
	m_pPolicyIdxBtn->Disconnect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( SD_LocalPanel::OnRadioButton ), NULL, this );
	m_pPolicyCoordBtn->Disconnect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( SD_LocalPanel::OnRadioButton ), NULL, this );
	m_pXPosDownBtn->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SD_LocalPanel::OnXPosDown ), NULL, this );
	m_pXSlider->Disconnect( wxEVT_SCROLL_TOP, wxScrollEventHandler( SD_LocalPanel::OnXSlideScroll ), NULL, this );
	m_pXSlider->Disconnect( wxEVT_SCROLL_BOTTOM, wxScrollEventHandler( SD_LocalPanel::OnXSlideScroll ), NULL, this );
	m_pXSlider->Disconnect( wxEVT_SCROLL_LINEUP, wxScrollEventHandler( SD_LocalPanel::OnXSlideScroll ), NULL, this );
	m_pXSlider->Disconnect( wxEVT_SCROLL_LINEDOWN, wxScrollEventHandler( SD_LocalPanel::OnXSlideScroll ), NULL, this );
	m_pXSlider->Disconnect( wxEVT_SCROLL_PAGEUP, wxScrollEventHandler( SD_LocalPanel::OnXSlideScroll ), NULL, this );
	m_pXSlider->Disconnect( wxEVT_SCROLL_PAGEDOWN, wxScrollEventHandler( SD_LocalPanel::OnXSlideScroll ), NULL, this );
	m_pXSlider->Disconnect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( SD_LocalPanel::OnXSlideScroll ), NULL, this );
	m_pXSlider->Disconnect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( SD_LocalPanel::OnXSlideScroll ), NULL, this );
	m_pXSlider->Disconnect( wxEVT_SCROLL_CHANGED, wxScrollEventHandler( SD_LocalPanel::OnXSlideScroll ), NULL, this );
	m_pXSlider->Disconnect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( SD_LocalPanel::OnXSlideRelease ), NULL, this );
	m_pXSlider->Disconnect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( SD_LocalPanel::OnXSlideDrag ), NULL, this );
	m_pXPosUpBtn->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SD_LocalPanel::OnXPosUp ), NULL, this );
	m_play_check_x->Disconnect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( SD_LocalPanel::m_play_check_xOnCheckBox ), NULL, this );
	m_pXPosTxt->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( SD_LocalPanel::OnPosTxtKillFocus ), NULL, this );
	m_pXPosTxt->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( SD_LocalPanel::OnPosTxt ), NULL, this );
	m_pXPitchTxt->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( SD_LocalPanel::OnPitchTxtKillFocus ), NULL, this );
	m_pXPitchTxt->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( SD_LocalPanel::OnPitchTxt ), NULL, this );
	m_pYPosDownBtn->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SD_LocalPanel::OnYPosDown ), NULL, this );
	m_pYSlider->Disconnect( wxEVT_SCROLL_TOP, wxScrollEventHandler( SD_LocalPanel::OnYSlideScroll ), NULL, this );
	m_pYSlider->Disconnect( wxEVT_SCROLL_BOTTOM, wxScrollEventHandler( SD_LocalPanel::OnYSlideScroll ), NULL, this );
	m_pYSlider->Disconnect( wxEVT_SCROLL_LINEUP, wxScrollEventHandler( SD_LocalPanel::OnYSlideScroll ), NULL, this );
	m_pYSlider->Disconnect( wxEVT_SCROLL_LINEDOWN, wxScrollEventHandler( SD_LocalPanel::OnYSlideScroll ), NULL, this );
	m_pYSlider->Disconnect( wxEVT_SCROLL_PAGEUP, wxScrollEventHandler( SD_LocalPanel::OnYSlideScroll ), NULL, this );
	m_pYSlider->Disconnect( wxEVT_SCROLL_PAGEDOWN, wxScrollEventHandler( SD_LocalPanel::OnYSlideScroll ), NULL, this );
	m_pYSlider->Disconnect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( SD_LocalPanel::OnYSlideScroll ), NULL, this );
	m_pYSlider->Disconnect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( SD_LocalPanel::OnYSlideScroll ), NULL, this );
	m_pYSlider->Disconnect( wxEVT_SCROLL_CHANGED, wxScrollEventHandler( SD_LocalPanel::OnYSlideScroll ), NULL, this );
	m_pYSlider->Disconnect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( SD_LocalPanel::OnYSlideRelease ), NULL, this );
	m_pYSlider->Disconnect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( SD_LocalPanel::OnYSlideDrag ), NULL, this );
	m_pYPosUpBtn->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SD_LocalPanel::OnYPosUp ), NULL, this );
	m_play_check_y->Disconnect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( SD_LocalPanel::m_play_check_yOnCheckBox ), NULL, this );
	m_pYPosTxt->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( SD_LocalPanel::OnPosTxtKillFocus ), NULL, this );
	m_pYPosTxt->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( SD_LocalPanel::OnPosTxt ), NULL, this );
	m_pYPitchTxt->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( SD_LocalPanel::OnPitchTxtKillFocus ), NULL, this );
	m_pYPitchTxt->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( SD_LocalPanel::OnPitchTxt ), NULL, this );
	m_pZPosDownBtn->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SD_LocalPanel::OnZPosDown ), NULL, this );
	m_pZSlider->Disconnect( wxEVT_SCROLL_TOP, wxScrollEventHandler( SD_LocalPanel::OnZSlideScroll ), NULL, this );
	m_pZSlider->Disconnect( wxEVT_SCROLL_BOTTOM, wxScrollEventHandler( SD_LocalPanel::OnZSlideScroll ), NULL, this );
	m_pZSlider->Disconnect( wxEVT_SCROLL_LINEUP, wxScrollEventHandler( SD_LocalPanel::OnZSlideScroll ), NULL, this );
	m_pZSlider->Disconnect( wxEVT_SCROLL_LINEDOWN, wxScrollEventHandler( SD_LocalPanel::OnZSlideScroll ), NULL, this );
	m_pZSlider->Disconnect( wxEVT_SCROLL_PAGEUP, wxScrollEventHandler( SD_LocalPanel::OnZSlideScroll ), NULL, this );
	m_pZSlider->Disconnect( wxEVT_SCROLL_PAGEDOWN, wxScrollEventHandler( SD_LocalPanel::OnZSlideScroll ), NULL, this );
	m_pZSlider->Disconnect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( SD_LocalPanel::OnZSlideScroll ), NULL, this );
	m_pZSlider->Disconnect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( SD_LocalPanel::OnZSlideScroll ), NULL, this );
	m_pZSlider->Disconnect( wxEVT_SCROLL_CHANGED, wxScrollEventHandler( SD_LocalPanel::OnZSlideScroll ), NULL, this );
	m_pZSlider->Disconnect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( SD_LocalPanel::OnZSlideRelease ), NULL, this );
	m_pZSlider->Disconnect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( SD_LocalPanel::OnZSlideDrag ), NULL, this );
	m_pZPosUpBtn->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SD_LocalPanel::OnZPosUp ), NULL, this );
	m_play_check_z->Disconnect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( SD_LocalPanel::m_play_check_zOnCheckBox ), NULL, this );
	m_pZPosTxt->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( SD_LocalPanel::OnPosTxtKillFocus ), NULL, this );
	m_pZPosTxt->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( SD_LocalPanel::OnPosTxt ), NULL, this );
	m_pZPitchTxt->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( SD_LocalPanel::OnPitchTxtKillFocus ), NULL, this );
	m_pZPitchTxt->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( SD_LocalPanel::OnPitchTxt ), NULL, this );
	
}

SD_BcflagPanel::SD_BcflagPanel( wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style ) : wxPanel( parent, id, pos, size, style )
{
	wxBoxSizer* bSizer133;
	bSizer133 = new wxBoxSizer( wxVERTICAL );
	
	wxBoxSizer* bSizer139;
	bSizer139 = new wxBoxSizer( wxVERTICAL );
	
	wxBoxSizer* bSizer150;
	bSizer150 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText194 = new wxStaticText( this, wxID_ANY, wxT("Medium"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText194->Wrap( -1 );
	bSizer150->Add( m_staticText194, 0, wxALL, 5 );
	
	bSizer139->Add( bSizer150, 0, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer189;
	bSizer189 = new wxBoxSizer( wxVERTICAL );
	
	m_pMedGrid = new wxGrid( this, wxID_ANY, wxDefaultPosition, wxSize( -1,-1 ), wxHSCROLL|wxVSCROLL );
	
	// Grid
	m_pMedGrid->CreateGrid( 0, 4 );
	m_pMedGrid->EnableEditing( true );
	m_pMedGrid->EnableGridLines( true );
	m_pMedGrid->SetGridLineColour( wxColour( 128, 128, 128 ) );
	m_pMedGrid->EnableDragGridSize( false );
	m_pMedGrid->SetMargins( 0, 0 );
	
	// Columns
	m_pMedGrid->SetColSize( 0, 274 );
	m_pMedGrid->SetColSize( 1, 59 );
	m_pMedGrid->SetColSize( 2, 64 );
	m_pMedGrid->SetColSize( 3, 60 );
	m_pMedGrid->EnableDragColMove( false );
	m_pMedGrid->EnableDragColSize( true );
	m_pMedGrid->SetColLabelSize( 25 );
	m_pMedGrid->SetColLabelValue( 0, wxT("Label") );
	m_pMedGrid->SetColLabelValue( 1, wxT("Color") );
	m_pMedGrid->SetColLabelValue( 2, wxT("ID") );
	m_pMedGrid->SetColLabelValue( 3, wxT("Type") );
	m_pMedGrid->SetColLabelAlignment( wxALIGN_LEFT, wxALIGN_CENTRE );
	
	// Rows
	m_pMedGrid->AutoSizeRows();
	m_pMedGrid->EnableDragRowSize( true );
	m_pMedGrid->SetRowLabelSize( 1 );
	m_pMedGrid->SetRowLabelAlignment( wxALIGN_CENTRE, wxALIGN_CENTRE );
	
	// Label Appearance
	m_pMedGrid->SetLabelBackgroundColour( wxColour( 88, 88, 88 ) );
	m_pMedGrid->SetLabelTextColour( wxColour( 255, 255, 255 ) );
	
	// Cell Defaults
	m_pMedGrid->SetDefaultCellBackgroundColour( wxColour( 44, 44, 44 ) );
	m_pMedGrid->SetDefaultCellTextColour( wxColour( 255, 255, 255 ) );
	m_pMedGrid->SetDefaultCellFont( wxFont( wxNORMAL_FONT->GetPointSize(), 70, 90, 90, false, wxEmptyString ) );
	m_pMedGrid->SetDefaultCellAlignment( wxALIGN_LEFT, wxALIGN_TOP );
	m_pMedGrid->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pMedGrid->SetBackgroundColour( wxColour( 0, 0, 0 ) );
	
	bSizer189->Add( m_pMedGrid, 1, wxEXPAND, 5 );
	
	bSizer139->Add( bSizer189, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer190;
	bSizer190 = new wxBoxSizer( wxVERTICAL );
	
	m_staticText195 = new wxStaticText( this, wxID_ANY, wxT("Local Boundary"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText195->Wrap( -1 );
	bSizer190->Add( m_staticText195, 0, wxALL, 5 );
	
	bSizer139->Add( bSizer190, 0, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer191;
	bSizer191 = new wxBoxSizer( wxVERTICAL );
	
	m_pLocalBCGrid = new wxGrid( this, wxID_ANY, wxDefaultPosition, wxSize( -1,-1 ), wxHSCROLL|wxVSCROLL );
	
	// Grid
	m_pLocalBCGrid->CreateGrid( 0, 6 );
	m_pLocalBCGrid->EnableEditing( true );
	m_pLocalBCGrid->EnableGridLines( true );
	m_pLocalBCGrid->SetGridLineColour( wxColour( 128, 128, 128 ) );
	m_pLocalBCGrid->EnableDragGridSize( false );
	m_pLocalBCGrid->SetMargins( 0, 0 );
	
	// Columns
	m_pLocalBCGrid->SetColSize( 0, 50 );
	m_pLocalBCGrid->SetColSize( 1, 136 );
	m_pLocalBCGrid->SetColSize( 2, 107 );
	m_pLocalBCGrid->SetColSize( 3, 73 );
	m_pLocalBCGrid->SetColSize( 4, 64 );
	m_pLocalBCGrid->SetColSize( 5, 100 );
	m_pLocalBCGrid->EnableDragColMove( false );
	m_pLocalBCGrid->EnableDragColSize( true );
	m_pLocalBCGrid->SetColLabelSize( 25 );
	m_pLocalBCGrid->SetColLabelValue( 0, wxT("Show") );
	m_pLocalBCGrid->SetColLabelValue( 1, wxT("Alias") );
	m_pLocalBCGrid->SetColLabelValue( 2, wxT("Class") );
	m_pLocalBCGrid->SetColLabelValue( 3, wxT("Color") );
	m_pLocalBCGrid->SetColLabelValue( 4, wxT("ID") );
	m_pLocalBCGrid->SetColLabelValue( 5, wxT("Medium") );
	m_pLocalBCGrid->SetColLabelAlignment( wxALIGN_LEFT, wxALIGN_CENTRE );
	
	// Rows
	m_pLocalBCGrid->EnableDragRowSize( true );
	m_pLocalBCGrid->SetRowLabelSize( 1 );
	m_pLocalBCGrid->SetRowLabelAlignment( wxALIGN_CENTRE, wxALIGN_CENTRE );
	
	// Label Appearance
	m_pLocalBCGrid->SetLabelBackgroundColour( wxColour( 88, 88, 88 ) );
	m_pLocalBCGrid->SetLabelTextColour( wxColour( 255, 255, 255 ) );
	
	// Cell Defaults
	m_pLocalBCGrid->SetDefaultCellBackgroundColour( wxColour( 44, 44, 44 ) );
	m_pLocalBCGrid->SetDefaultCellTextColour( wxColour( 255, 255, 255 ) );
	m_pLocalBCGrid->SetDefaultCellAlignment( wxALIGN_LEFT, wxALIGN_TOP );
	m_pLocalBCGrid->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pLocalBCGrid->SetBackgroundColour( wxColour( 0, 0, 0 ) );
	
	bSizer191->Add( m_pLocalBCGrid, 1, wxEXPAND, 5 );
	
	bSizer139->Add( bSizer191, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer143;
	bSizer143 = new wxBoxSizer( wxHORIZONTAL );
	
	
	bSizer143->Add( 0, 0, 1, wxEXPAND, 5 );
	
	m_checkBox_hyde_globalPlane = new wxCheckBox( this, wxID_ANY, wxT("Hide Global Plane"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer143->Add( m_checkBox_hyde_globalPlane, 0, wxALL, 10 );
	
	m_checkBox_hyde_LocalPlanes = new wxCheckBox( this, wxID_ANY, wxT("Hide All Local Plane"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer143->Add( m_checkBox_hyde_LocalPlanes, 0, wxALL, 10 );
	
	m_button_apply = new wxButton( this, wxID_ANY, wxT("Apply"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer143->Add( m_button_apply, 0, wxALL, 5 );
	
	bSizer139->Add( bSizer143, 0, wxEXPAND, 0 );
	
	bSizer133->Add( bSizer139, 1, wxEXPAND, 5 );
	
	this->SetSizer( bSizer133 );
	this->Layout();
	
	// Connect Events
	m_checkBox_hyde_globalPlane->Connect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( SD_BcflagPanel::m_checkBox_hyde_globalPlaneOnCheckBox ), NULL, this );
	m_checkBox_hyde_LocalPlanes->Connect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( SD_BcflagPanel::m_checkBox_hyde_LocalPlanesOnCheckBox ), NULL, this );
	m_button_apply->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SD_BcflagPanel::m_button_applyOnButtonClick ), NULL, this );
}

SD_BcflagPanel::~SD_BcflagPanel()
{
	// Disconnect Events
	m_checkBox_hyde_globalPlane->Disconnect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( SD_BcflagPanel::m_checkBox_hyde_globalPlaneOnCheckBox ), NULL, this );
	m_checkBox_hyde_LocalPlanes->Disconnect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( SD_BcflagPanel::m_checkBox_hyde_LocalPlanesOnCheckBox ), NULL, this );
	m_button_apply->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( SD_BcflagPanel::m_button_applyOnButtonClick ), NULL, this );
	
}

dumy::dumy( wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style ) : wxPanel( parent, id, pos, size, style )
{
}

dumy::~dumy()
{
}

CoordinateViewerFrame::CoordinateViewerFrame( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxFrame( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );
	
	wxBoxSizer* bSizer167;
	bSizer167 = new wxBoxSizer( wxVERTICAL );
	
	m_notebook4 = new wxNotebook( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0 );
	m_panel41 = new wxPanel( m_notebook4, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	m_panel41->SetBackgroundColour( wxColour( 44, 44, 44 ) );
	
	wxBoxSizer* bSizer175;
	bSizer175 = new wxBoxSizer( wxVERTICAL );
	
	m_panel44 = new wxPanel( m_panel41, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	m_panel44->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	wxBoxSizer* bSizer181;
	bSizer181 = new wxBoxSizer( wxVERTICAL );
	
	m_staticText186 = new wxStaticText( m_panel44, wxID_ANY, wxT("Central coordinate"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText186->Wrap( -1 );
	m_staticText186->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), 70, 90, 92, false, wxEmptyString ) );
	m_staticText186->SetForegroundColour( wxColour( 209, 76, 16 ) );
	
	bSizer181->Add( m_staticText186, 0, wxALL, 0 );
	
	wxFlexGridSizer* fgSizer13;
	fgSizer13 = new wxFlexGridSizer( 4, 6, 0, 0 );
	fgSizer13->SetFlexibleDirection( wxBOTH );
	fgSizer13->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );
	
	m_staticText187 = new wxStaticText( m_panel44, wxID_ANY, wxT("Axis"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText187->Wrap( -1 );
	m_staticText187->SetForegroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_HIGHLIGHTTEXT ) );
	
	fgSizer13->Add( m_staticText187, 0, wxALL, 5 );
	
	m_staticText202 = new wxStaticText( m_panel44, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText202->Wrap( -1 );
	fgSizer13->Add( m_staticText202, 0, wxALL, 5 );
	
	m_staticText188 = new wxStaticText( m_panel44, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 100,-1 ), 0 );
	m_staticText188->Wrap( -1 );
	fgSizer13->Add( m_staticText188, 0, wxALL, 5 );
	
	m_staticText201 = new wxStaticText( m_panel44, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText201->Wrap( -1 );
	fgSizer13->Add( m_staticText201, 0, wxALL, 5 );
	
	m_staticText189 = new wxStaticText( m_panel44, wxID_ANY, wxT("Position"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText189->Wrap( -1 );
	m_staticText189->SetForegroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_HIGHLIGHTTEXT ) );
	
	fgSizer13->Add( m_staticText189, 0, wxALL, 5 );
	
	m_staticText190 = new wxStaticText( m_panel44, wxID_ANY, wxT("Pitch"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText190->Wrap( -1 );
	m_staticText190->SetForegroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_HIGHLIGHTTEXT ) );
	
	fgSizer13->Add( m_staticText190, 0, wxALL, 5 );
	
	m_staticText161 = new wxStaticText( m_panel44, wxID_ANY, wxT("X"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText161->Wrap( -1 );
	m_staticText161->SetForegroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_HIGHLIGHTTEXT ) );
	
	fgSizer13->Add( m_staticText161, 0, wxALL, 5 );
	
	m_pXPosDownBtn_p = new wxBitmapButton( m_panel44, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxSize( -1,-1 ), 0 );
	m_pXPosDownBtn_p->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	m_pXPosDownBtn_p->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	fgSizer13->Add( m_pXPosDownBtn_p, 0, wxALL, 5 );
	
	m_pXSlider_p = new wxSlider( m_panel44, wxID_ANY, 50, 0, 100, wxDefaultPosition, wxSize( 100,-1 ), wxSL_HORIZONTAL );
	fgSizer13->Add( m_pXSlider_p, 0, wxALL, 5 );
	
	m_pXPosUpBtn_p = new wxBitmapButton( m_panel44, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxSize( -1,-1 ), 0 );
	m_pXPosUpBtn_p->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	m_pXPosUpBtn_p->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	fgSizer13->Add( m_pXPosUpBtn_p, 0, wxALL, 5 );
	
	m_pXPosTxt_p = new wxTextCtrl( m_panel44, SLICE_CTRL_XPOS_P_TXT, wxEmptyString, wxDefaultPosition, wxSize( 70,-1 ), wxTE_PROCESS_ENTER );
	fgSizer13->Add( m_pXPosTxt_p, 0, wxALL, 5 );
	
	m_pXPitchTxt_p = new wxTextCtrl( m_panel44, SLICE_CTRL_XPITCH_P_TXT, wxEmptyString, wxDefaultPosition, wxSize( 70,-1 ), wxTE_PROCESS_ENTER );
	fgSizer13->Add( m_pXPitchTxt_p, 0, wxALL, 5 );
	
	m_staticText162 = new wxStaticText( m_panel44, wxID_ANY, wxT("Y"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText162->Wrap( -1 );
	m_staticText162->SetForegroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_HIGHLIGHTTEXT ) );
	
	fgSizer13->Add( m_staticText162, 0, wxALL, 5 );
	
	m_pYPosDownBtn_p = new wxBitmapButton( m_panel44, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxSize( -1,-1 ), 0 );
	m_pYPosDownBtn_p->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	m_pYPosDownBtn_p->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	fgSizer13->Add( m_pYPosDownBtn_p, 0, wxALL, 5 );
	
	m_pYSlider_p = new wxSlider( m_panel44, wxID_ANY, 50, 0, 100, wxDefaultPosition, wxSize( 100,-1 ), wxSL_HORIZONTAL );
	fgSizer13->Add( m_pYSlider_p, 0, wxALL, 5 );
	
	m_pYPosUpBtn_p = new wxBitmapButton( m_panel44, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxSize( -1,-1 ), 0 );
	m_pYPosUpBtn_p->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	m_pYPosUpBtn_p->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	fgSizer13->Add( m_pYPosUpBtn_p, 0, wxALL, 5 );
	
	m_pYPosTxt_p = new wxTextCtrl( m_panel44, SLICE_CTRL_YPOS_P_TXT, wxEmptyString, wxDefaultPosition, wxSize( 70,-1 ), wxTE_PROCESS_ENTER );
	fgSizer13->Add( m_pYPosTxt_p, 0, wxALL, 5 );
	
	m_pYPitchTxt_p = new wxTextCtrl( m_panel44, SLICE_CTRL_YPITCH_P_TXT, wxEmptyString, wxDefaultPosition, wxSize( 70,-1 ), wxTE_PROCESS_ENTER );
	fgSizer13->Add( m_pYPitchTxt_p, 0, wxALL, 5 );
	
	m_staticText163 = new wxStaticText( m_panel44, wxID_ANY, wxT("Z"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText163->Wrap( -1 );
	m_staticText163->SetForegroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_HIGHLIGHTTEXT ) );
	
	fgSizer13->Add( m_staticText163, 0, wxALL, 5 );
	
	m_pZPosDownBtn_p = new wxBitmapButton( m_panel44, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxSize( -1,-1 ), 0 );
	m_pZPosDownBtn_p->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	m_pZPosDownBtn_p->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	fgSizer13->Add( m_pZPosDownBtn_p, 0, wxALL, 5 );
	
	m_pZSlider_p = new wxSlider( m_panel44, wxID_ANY, 50, 0, 100, wxDefaultPosition, wxSize( 100,-1 ), wxSL_HORIZONTAL );
	fgSizer13->Add( m_pZSlider_p, 0, wxALL, 5 );
	
	m_pZPosUpBtn_p = new wxBitmapButton( m_panel44, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxSize( -1,-1 ), 0 );
	m_pZPosUpBtn_p->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	m_pZPosUpBtn_p->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	fgSizer13->Add( m_pZPosUpBtn_p, 0, wxALL, 5 );
	
	m_pZPosTxt_p = new wxTextCtrl( m_panel44, SLICE_CTRL_ZPOS_P_TXT, wxEmptyString, wxDefaultPosition, wxSize( 70,-1 ), wxTE_PROCESS_ENTER );
	fgSizer13->Add( m_pZPosTxt_p, 0, wxALL, 5 );
	
	m_pZPitchTxt_p = new wxTextCtrl( m_panel44, SLICE_CTRL_ZPITCH_P_TXT, wxEmptyString, wxDefaultPosition, wxSize( 70,-1 ), wxTE_PROCESS_ENTER );
	fgSizer13->Add( m_pZPitchTxt_p, 0, wxALL, 5 );
	
	bSizer181->Add( fgSizer13, 0, wxEXPAND, 5 );
	
	m_panel44->SetSizer( bSizer181 );
	m_panel44->Layout();
	bSizer181->Fit( m_panel44 );
	bSizer175->Add( m_panel44, 0, wxALL|wxEXPAND, 5 );
	
	m_panel421 = new wxPanel( m_panel41, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	m_panel421->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	wxFlexGridSizer* fgSizer17;
	fgSizer17 = new wxFlexGridSizer( 2, 3, 0, 0 );
	fgSizer17->SetFlexibleDirection( wxBOTH );
	fgSizer17->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );
	
	m_staticText166 = new wxStaticText( m_panel421, wxID_ANY, wxT("Size"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText166->Wrap( -1 );
	m_staticText166->SetForegroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_HIGHLIGHTTEXT ) );
	
	fgSizer17->Add( m_staticText166, 0, wxALL, 0 );
	
	m_staticText167 = new wxStaticText( m_panel421, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 50,-1 ), 0 );
	m_staticText167->Wrap( -1 );
	fgSizer17->Add( m_staticText167, 0, wxALL, 5 );
	
	m_pSizeTxt_p = new wxTextCtrl( m_panel421, SIZE_P_TXT, wxEmptyString, wxDefaultPosition, wxSize( 70,-1 ), wxTE_PROCESS_ENTER );
	fgSizer17->Add( m_pSizeTxt_p, 0, wxALL, 5 );
	
	m_panel421->SetSizer( fgSizer17 );
	m_panel421->Layout();
	fgSizer17->Fit( m_panel421 );
	bSizer175->Add( m_panel421, 0, wxALL|wxEXPAND, 5 );
	
	m_staticline16 = new wxStaticLine( m_panel41, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	bSizer175->Add( m_staticline16, 0, wxEXPAND | wxALL, 5 );
	
	wxBoxSizer* bSizer164;
	bSizer164 = new wxBoxSizer( wxHORIZONTAL );
	
	m_button_Add_p = new wxButton( m_panel41, wxID_ANY, wxT("Add"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer164->Add( m_button_Add_p, 0, wxALL, 5 );
	
	m_button_Clear_p = new wxButton( m_panel41, wxID_ANY, wxT("Clear"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer164->Add( m_button_Clear_p, 0, wxALL, 5 );
	
	m_button_AllClear_p = new wxButton( m_panel41, wxID_ANY, wxT("All Clear"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer164->Add( m_button_AllClear_p, 0, wxALL, 5 );
	
	bSizer175->Add( bSizer164, 0, wxALIGN_RIGHT, 5 );
	
	m_staticline17 = new wxStaticLine( m_panel41, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	bSizer175->Add( m_staticline17, 0, wxEXPAND | wxALL, 5 );
	
	m_panel62 = new wxPanel( m_panel41, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	m_panel62->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	wxBoxSizer* bSizer178;
	bSizer178 = new wxBoxSizer( wxVERTICAL );
	
	wxBoxSizer* bSizer174;
	bSizer174 = new wxBoxSizer( wxVERTICAL );
	
	m_staticText1861 = new wxStaticText( m_panel62, wxID_ANY, wxT("PickUp List"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText1861->Wrap( -1 );
	m_staticText1861->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), 70, 90, 92, false, wxEmptyString ) );
	m_staticText1861->SetForegroundColour( wxColour( 209, 76, 16 ) );
	
	bSizer174->Add( m_staticText1861, 0, wxALL, 0 );
	
	bSizer178->Add( bSizer174, 0, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer1751;
	bSizer1751 = new wxBoxSizer( wxVERTICAL );
	
	m_pPointGrid_p = new wxGrid( m_panel62, wxID_ANY, wxDefaultPosition, wxSize( 100,-1 ), wxHSCROLL|wxVSCROLL );
	
	// Grid
	m_pPointGrid_p->CreateGrid( 0, 3 );
	m_pPointGrid_p->EnableEditing( true );
	m_pPointGrid_p->EnableGridLines( true );
	m_pPointGrid_p->SetGridLineColour( wxColour( 128, 128, 128 ) );
	m_pPointGrid_p->EnableDragGridSize( false );
	m_pPointGrid_p->SetMargins( 0, 0 );
	
	// Columns
	m_pPointGrid_p->SetColSize( 0, 80 );
	m_pPointGrid_p->SetColSize( 1, 210 );
	m_pPointGrid_p->SetColSize( 2, 100 );
	m_pPointGrid_p->EnableDragColMove( false );
	m_pPointGrid_p->EnableDragColSize( true );
	m_pPointGrid_p->SetColLabelSize( 25 );
	m_pPointGrid_p->SetColLabelValue( 0, wxT("ID") );
	m_pPointGrid_p->SetColLabelValue( 1, wxT("Coordinate(x,y,z)") );
	m_pPointGrid_p->SetColLabelValue( 2, wxT("Size") );
	m_pPointGrid_p->SetColLabelAlignment( wxALIGN_LEFT, wxALIGN_CENTRE );
	
	// Rows
	m_pPointGrid_p->AutoSizeRows();
	m_pPointGrid_p->EnableDragRowSize( true );
	m_pPointGrid_p->SetRowLabelSize( 1 );
	m_pPointGrid_p->SetRowLabelAlignment( wxALIGN_CENTRE, wxALIGN_CENTRE );
	
	// Label Appearance
	m_pPointGrid_p->SetLabelBackgroundColour( wxColour( 88, 88, 88 ) );
	m_pPointGrid_p->SetLabelTextColour( wxColour( 255, 255, 255 ) );
	
	// Cell Defaults
	m_pPointGrid_p->SetDefaultCellBackgroundColour( wxColour( 44, 44, 44 ) );
	m_pPointGrid_p->SetDefaultCellTextColour( wxColour( 255, 255, 255 ) );
	m_pPointGrid_p->SetDefaultCellAlignment( wxALIGN_LEFT, wxALIGN_TOP );
	m_pPointGrid_p->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pPointGrid_p->SetBackgroundColour( wxColour( 0, 0, 0 ) );
	
	bSizer1751->Add( m_pPointGrid_p, 1, wxALL|wxEXPAND, 5 );
	
	bSizer178->Add( bSizer1751, 1, wxEXPAND, 5 );
	
	m_panel62->SetSizer( bSizer178 );
	m_panel62->Layout();
	bSizer178->Fit( m_panel62 );
	bSizer175->Add( m_panel62, 1, wxEXPAND | wxALL, 5 );
	
	m_panel41->SetSizer( bSizer175 );
	m_panel41->Layout();
	bSizer175->Fit( m_panel41 );
	m_notebook4->AddPage( m_panel41, wxT("Point"), true );
	m_panel413 = new wxPanel( m_notebook4, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	m_panel413->SetBackgroundColour( wxColour( 44, 44, 44 ) );
	
	wxBoxSizer* bSizer1753;
	bSizer1753 = new wxBoxSizer( wxVERTICAL );
	
	m_panel441 = new wxPanel( m_panel413, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	m_panel441->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	wxBoxSizer* bSizer1811;
	bSizer1811 = new wxBoxSizer( wxVERTICAL );
	
	m_staticText1862 = new wxStaticText( m_panel441, wxID_ANY, wxT("Central coordinate"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText1862->Wrap( -1 );
	m_staticText1862->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), 70, 90, 92, false, wxEmptyString ) );
	m_staticText1862->SetForegroundColour( wxColour( 209, 76, 16 ) );
	
	bSizer1811->Add( m_staticText1862, 0, wxALL, 0 );
	
	wxFlexGridSizer* fgSizer132;
	fgSizer132 = new wxFlexGridSizer( 4, 6, 0, 0 );
	fgSizer132->SetFlexibleDirection( wxBOTH );
	fgSizer132->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );
	
	m_staticText1872 = new wxStaticText( m_panel441, wxID_ANY, wxT("Axis"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText1872->Wrap( -1 );
	m_staticText1872->SetForegroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_HIGHLIGHTTEXT ) );
	
	fgSizer132->Add( m_staticText1872, 0, wxALL, 5 );
	
	m_staticText2021 = new wxStaticText( m_panel441, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText2021->Wrap( -1 );
	fgSizer132->Add( m_staticText2021, 0, wxALL, 5 );
	
	m_staticText1882 = new wxStaticText( m_panel441, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 100,-1 ), 0 );
	m_staticText1882->Wrap( -1 );
	fgSizer132->Add( m_staticText1882, 0, wxALL, 5 );
	
	m_staticText2011 = new wxStaticText( m_panel441, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText2011->Wrap( -1 );
	fgSizer132->Add( m_staticText2011, 0, wxALL, 5 );
	
	m_staticText1892 = new wxStaticText( m_panel441, wxID_ANY, wxT("Position"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText1892->Wrap( -1 );
	m_staticText1892->SetForegroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_HIGHLIGHTTEXT ) );
	
	fgSizer132->Add( m_staticText1892, 0, wxALL, 5 );
	
	m_staticText1902 = new wxStaticText( m_panel441, wxID_ANY, wxT("Pitch"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText1902->Wrap( -1 );
	m_staticText1902->SetForegroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_HIGHLIGHTTEXT ) );
	
	fgSizer132->Add( m_staticText1902, 0, wxALL, 5 );
	
	m_staticText1612 = new wxStaticText( m_panel441, wxID_ANY, wxT("X"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText1612->Wrap( -1 );
	m_staticText1612->SetForegroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_HIGHLIGHTTEXT ) );
	
	fgSizer132->Add( m_staticText1612, 0, wxALL, 5 );
	
	m_pXPosDownBtn_r = new wxBitmapButton( m_panel441, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxSize( -1,-1 ), 0 );
	m_pXPosDownBtn_r->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	m_pXPosDownBtn_r->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	fgSizer132->Add( m_pXPosDownBtn_r, 0, wxALL, 5 );
	
	m_pXSlider_r = new wxSlider( m_panel441, wxID_ANY, 50, 0, 100, wxDefaultPosition, wxSize( 100,-1 ), wxSL_HORIZONTAL );
	fgSizer132->Add( m_pXSlider_r, 0, wxALL, 5 );
	
	m_pXPosUpBtn_r = new wxBitmapButton( m_panel441, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxSize( -1,-1 ), 0 );
	m_pXPosUpBtn_r->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	m_pXPosUpBtn_r->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	fgSizer132->Add( m_pXPosUpBtn_r, 0, wxALL, 5 );
	
	m_pXPosTxt_r = new wxTextCtrl( m_panel441, SLICE_CTRL_XPOS_R_TXT, wxEmptyString, wxDefaultPosition, wxSize( 70,-1 ), wxTE_PROCESS_ENTER );
	fgSizer132->Add( m_pXPosTxt_r, 0, wxALL, 5 );
	
	m_pXPitchTxt_r = new wxTextCtrl( m_panel441, SLICE_CTRL_XPITCH_R_TXT, wxEmptyString, wxDefaultPosition, wxSize( 70,-1 ), wxTE_PROCESS_ENTER );
	fgSizer132->Add( m_pXPitchTxt_r, 0, wxALL, 5 );
	
	m_staticText1622 = new wxStaticText( m_panel441, wxID_ANY, wxT("Y"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText1622->Wrap( -1 );
	m_staticText1622->SetForegroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_HIGHLIGHTTEXT ) );
	
	fgSizer132->Add( m_staticText1622, 0, wxALL, 5 );
	
	m_pYPosDownBtn_r = new wxBitmapButton( m_panel441, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxSize( -1,-1 ), 0 );
	m_pYPosDownBtn_r->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	m_pYPosDownBtn_r->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	fgSizer132->Add( m_pYPosDownBtn_r, 0, wxALL, 5 );
	
	m_pYSlider_r = new wxSlider( m_panel441, wxID_ANY, 50, 0, 100, wxDefaultPosition, wxSize( 100,-1 ), wxSL_HORIZONTAL );
	fgSizer132->Add( m_pYSlider_r, 0, wxALL, 5 );
	
	m_pYPosUpBtn_r = new wxBitmapButton( m_panel441, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxSize( -1,-1 ), 0 );
	m_pYPosUpBtn_r->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	m_pYPosUpBtn_r->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	fgSizer132->Add( m_pYPosUpBtn_r, 0, wxALL, 5 );
	
	m_pYPosTxt_r = new wxTextCtrl( m_panel441, SLICE_CTRL_YPOS_R_TXT, wxEmptyString, wxDefaultPosition, wxSize( 70,-1 ), wxTE_PROCESS_ENTER );
	fgSizer132->Add( m_pYPosTxt_r, 0, wxALL, 5 );
	
	m_pYPitchTxt_r = new wxTextCtrl( m_panel441, SLICE_CTRL_YPITCH_R_TXT, wxEmptyString, wxDefaultPosition, wxSize( 70,-1 ), wxTE_PROCESS_ENTER );
	fgSizer132->Add( m_pYPitchTxt_r, 0, wxALL, 5 );
	
	m_staticText1632 = new wxStaticText( m_panel441, wxID_ANY, wxT("Z"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText1632->Wrap( -1 );
	m_staticText1632->SetForegroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_HIGHLIGHTTEXT ) );
	
	fgSizer132->Add( m_staticText1632, 0, wxALL, 5 );
	
	m_pZPosDownBtn_r = new wxBitmapButton( m_panel441, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxSize( -1,-1 ), 0 );
	m_pZPosDownBtn_r->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	m_pZPosDownBtn_r->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	fgSizer132->Add( m_pZPosDownBtn_r, 0, wxALL, 5 );
	
	m_pZSlider_r = new wxSlider( m_panel441, wxID_ANY, 50, 0, 100, wxDefaultPosition, wxSize( 100,-1 ), wxSL_HORIZONTAL );
	fgSizer132->Add( m_pZSlider_r, 0, wxALL, 5 );
	
	m_pZPosUpBtn_r = new wxBitmapButton( m_panel441, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxSize( -1,-1 ), 0 );
	m_pZPosUpBtn_r->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	m_pZPosUpBtn_r->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	fgSizer132->Add( m_pZPosUpBtn_r, 0, wxALL, 5 );
	
	m_pZPosTxt_r = new wxTextCtrl( m_panel441, SLICE_CTRL_ZPOS_R_TXT, wxEmptyString, wxDefaultPosition, wxSize( 70,-1 ), wxTE_PROCESS_ENTER );
	fgSizer132->Add( m_pZPosTxt_r, 0, wxALL, 5 );
	
	m_pZPitchTxt_r = new wxTextCtrl( m_panel441, SLICE_CTRL_ZPITCH_R_TXT, wxEmptyString, wxDefaultPosition, wxSize( 70,-1 ), wxTE_PROCESS_ENTER );
	fgSizer132->Add( m_pZPitchTxt_r, 0, wxALL, 5 );
	
	bSizer1811->Add( fgSizer132, 1, wxEXPAND, 5 );
	
	m_panel441->SetSizer( bSizer1811 );
	m_panel441->Layout();
	bSizer1811->Fit( m_panel441 );
	bSizer1753->Add( m_panel441, 0, wxEXPAND | wxALL, 5 );
	
	m_panel4213 = new wxPanel( m_panel413, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	m_panel4213->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	wxFlexGridSizer* fgSizer173;
	fgSizer173 = new wxFlexGridSizer( 5, 7, 0, 0 );
	fgSizer173->SetFlexibleDirection( wxBOTH );
	fgSizer173->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );
	
	m_staticText1663 = new wxStaticText( m_panel4213, wxID_ANY, wxT("Width"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText1663->Wrap( -1 );
	m_staticText1663->SetForegroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_HIGHLIGHTTEXT ) );
	
	fgSizer173->Add( m_staticText1663, 0, wxALL, 5 );
	
	m_staticText1673 = new wxStaticText( m_panel4213, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 50,-1 ), 0 );
	m_staticText1673->Wrap( -1 );
	fgSizer173->Add( m_staticText1673, 0, wxALL, 5 );
	
	m_pWidthTxt_r = new wxTextCtrl( m_panel4213, WIDTH_R_TXT, wxEmptyString, wxDefaultPosition, wxSize( 70,-1 ), wxTE_PROCESS_ENTER );
	fgSizer173->Add( m_pWidthTxt_r, 0, wxALL, 0 );
	
	m_staticText207 = new wxStaticText( m_panel4213, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText207->Wrap( -1 );
	fgSizer173->Add( m_staticText207, 0, wxALL, 5 );
	
	m_staticText208 = new wxStaticText( m_panel4213, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText208->Wrap( -1 );
	fgSizer173->Add( m_staticText208, 0, wxALL, 5 );
	
	m_staticText210 = new wxStaticText( m_panel4213, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText210->Wrap( -1 );
	fgSizer173->Add( m_staticText210, 0, wxALL, 5 );
	
	m_staticText211 = new wxStaticText( m_panel4213, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText211->Wrap( -1 );
	fgSizer173->Add( m_staticText211, 0, wxALL, 5 );
	
	m_staticText260 = new wxStaticText( m_panel4213, wxID_ANY, wxT("Depth"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText260->Wrap( -1 );
	m_staticText260->SetForegroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_HIGHLIGHTTEXT ) );
	
	fgSizer173->Add( m_staticText260, 0, wxALL, 5 );
	
	m_staticText261 = new wxStaticText( m_panel4213, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText261->Wrap( -1 );
	fgSizer173->Add( m_staticText261, 0, wxALL, 5 );
	
	m_pDepthTxt_r = new wxTextCtrl( m_panel4213, DEPTH_R_TXT, wxEmptyString, wxDefaultPosition, wxSize( 70,-1 ), wxTE_PROCESS_ENTER );
	fgSizer173->Add( m_pDepthTxt_r, 0, wxALL, 0 );
	
	m_staticText212 = new wxStaticText( m_panel4213, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText212->Wrap( -1 );
	fgSizer173->Add( m_staticText212, 0, wxALL, 5 );
	
	m_staticText213 = new wxStaticText( m_panel4213, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText213->Wrap( -1 );
	fgSizer173->Add( m_staticText213, 0, wxALL, 5 );
	
	m_staticText214 = new wxStaticText( m_panel4213, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText214->Wrap( -1 );
	fgSizer173->Add( m_staticText214, 0, wxALL, 5 );
	
	m_staticText215 = new wxStaticText( m_panel4213, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText215->Wrap( -1 );
	fgSizer173->Add( m_staticText215, 0, wxALL, 5 );
	
	m_staticText262 = new wxStaticText( m_panel4213, wxID_ANY, wxT("Height"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText262->Wrap( -1 );
	m_staticText262->SetForegroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_HIGHLIGHTTEXT ) );
	
	fgSizer173->Add( m_staticText262, 0, wxALL, 5 );
	
	m_staticText263 = new wxStaticText( m_panel4213, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText263->Wrap( -1 );
	fgSizer173->Add( m_staticText263, 0, wxALL, 5 );
	
	m_pHeightTxt_r = new wxTextCtrl( m_panel4213, HEIGHT_R_TXT, wxEmptyString, wxDefaultPosition, wxSize( 70,-1 ), wxTE_PROCESS_ENTER );
	fgSizer173->Add( m_pHeightTxt_r, 0, wxALL, 0 );
	
	m_staticText216 = new wxStaticText( m_panel4213, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText216->Wrap( -1 );
	fgSizer173->Add( m_staticText216, 0, wxALL, 5 );
	
	m_staticText217 = new wxStaticText( m_panel4213, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText217->Wrap( -1 );
	fgSizer173->Add( m_staticText217, 0, wxALL, 5 );
	
	m_staticText218 = new wxStaticText( m_panel4213, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText218->Wrap( -1 );
	fgSizer173->Add( m_staticText218, 0, wxALL, 5 );
	
	m_staticText219 = new wxStaticText( m_panel4213, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText219->Wrap( -1 );
	fgSizer173->Add( m_staticText219, 0, wxALL, 5 );
	
	m_staticText2621 = new wxStaticText( m_panel4213, wxID_ANY, wxT("OrientationVector"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText2621->Wrap( -1 );
	m_staticText2621->SetForegroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_HIGHLIGHTTEXT ) );
	
	fgSizer173->Add( m_staticText2621, 0, wxALL, 5 );
	
	m_staticText267 = new wxStaticText( m_panel4213, wxID_ANY, wxT("X:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText267->Wrap( -1 );
	m_staticText267->SetForegroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_HIGHLIGHTTEXT ) );
	
	fgSizer173->Add( m_staticText267, 0, wxALIGN_RIGHT|wxALL, 5 );
	
	m_pOrientationVecXTxt_r = new wxTextCtrl( m_panel4213, ORIENTATION_VEC_X_R_TXT, wxEmptyString, wxDefaultPosition, wxSize( 70,-1 ), wxTE_PROCESS_ENTER );
	fgSizer173->Add( m_pOrientationVecXTxt_r, 0, wxALL, 0 );
	
	m_staticText220 = new wxStaticText( m_panel4213, wxID_ANY, wxT("Y:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText220->Wrap( -1 );
	m_staticText220->SetForegroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_HIGHLIGHTTEXT ) );
	
	fgSizer173->Add( m_staticText220, 0, wxALIGN_RIGHT|wxALL, 5 );
	
	m_pOrientationVecYTxt_r = new wxTextCtrl( m_panel4213, ORIENTATION_VEC_X_R_TXT, wxEmptyString, wxDefaultPosition, wxSize( 70,-1 ), wxTE_PROCESS_ENTER );
	fgSizer173->Add( m_pOrientationVecYTxt_r, 0, wxALL, 0 );
	
	m_staticText221 = new wxStaticText( m_panel4213, wxID_ANY, wxT("Z:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText221->Wrap( -1 );
	m_staticText221->SetForegroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_HIGHLIGHTTEXT ) );
	
	fgSizer173->Add( m_staticText221, 0, wxALIGN_RIGHT|wxALL, 5 );
	
	m_pOrientationVecZTxt_r = new wxTextCtrl( m_panel4213, ORIENTATION_VEC_X_R_TXT, wxEmptyString, wxDefaultPosition, wxSize( 70,-1 ), wxTE_PROCESS_ENTER );
	fgSizer173->Add( m_pOrientationVecZTxt_r, 0, wxALL, 0 );
	
	m_staticText236 = new wxStaticText( m_panel4213, wxID_ANY, wxT("DirectionVector"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText236->Wrap( -1 );
	m_staticText236->SetForegroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_HIGHLIGHTTEXT ) );
	
	fgSizer173->Add( m_staticText236, 0, wxALL, 5 );
	
	m_staticText237 = new wxStaticText( m_panel4213, wxID_ANY, wxT("X:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText237->Wrap( -1 );
	m_staticText237->SetForegroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_HIGHLIGHTTEXT ) );
	
	fgSizer173->Add( m_staticText237, 0, wxALIGN_RIGHT|wxALL, 5 );
	
	m_pDirVecXTxt_r = new wxTextCtrl( m_panel4213, DIR_VEC_X_R_TXT, wxEmptyString, wxDefaultPosition, wxSize( 70,-1 ), wxTE_PROCESS_ENTER );
	fgSizer173->Add( m_pDirVecXTxt_r, 0, wxALL, 0 );
	
	m_staticText238 = new wxStaticText( m_panel4213, wxID_ANY, wxT("Y:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText238->Wrap( -1 );
	m_staticText238->SetForegroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_HIGHLIGHTTEXT ) );
	
	fgSizer173->Add( m_staticText238, 0, wxALIGN_RIGHT|wxALL, 5 );
	
	m_pDirVecYTxt_r = new wxTextCtrl( m_panel4213, DIR_VEC_Y_R_TXT, wxEmptyString, wxDefaultPosition, wxSize( 70,-1 ), wxTE_PROCESS_ENTER );
	fgSizer173->Add( m_pDirVecYTxt_r, 0, wxALL, 0 );
	
	m_staticText239 = new wxStaticText( m_panel4213, wxID_ANY, wxT("Z:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText239->Wrap( -1 );
	m_staticText239->SetForegroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_HIGHLIGHTTEXT ) );
	
	fgSizer173->Add( m_staticText239, 0, wxALIGN_RIGHT|wxALL, 5 );
	
	m_pDirVecZTxt_r = new wxTextCtrl( m_panel4213, DIR_VEC_Z_R_TXT, wxEmptyString, wxDefaultPosition, wxSize( 70,-1 ), wxTE_PROCESS_ENTER );
	fgSizer173->Add( m_pDirVecZTxt_r, 0, wxALL, 0 );
	
	m_panel4213->SetSizer( fgSizer173 );
	m_panel4213->Layout();
	fgSizer173->Fit( m_panel4213 );
	bSizer1753->Add( m_panel4213, 0, wxALL|wxEXPAND, 5 );
	
	m_staticline163 = new wxStaticLine( m_panel413, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	bSizer1753->Add( m_staticline163, 0, wxEXPAND | wxALL, 5 );
	
	wxBoxSizer* bSizer1643;
	bSizer1643 = new wxBoxSizer( wxHORIZONTAL );
	
	m_button_Add_r = new wxButton( m_panel413, wxID_ANY, wxT("Add"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer1643->Add( m_button_Add_r, 0, wxALL, 5 );
	
	m_button_Clear_r = new wxButton( m_panel413, wxID_ANY, wxT("Clear"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer1643->Add( m_button_Clear_r, 0, wxALL, 5 );
	
	m_button_AllClear_r = new wxButton( m_panel413, wxID_ANY, wxT("All Clear"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer1643->Add( m_button_AllClear_r, 0, wxALL, 5 );
	
	bSizer1753->Add( bSizer1643, 0, wxALIGN_RIGHT, 5 );
	
	m_staticline173 = new wxStaticLine( m_panel413, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	bSizer1753->Add( m_staticline173, 0, wxEXPAND | wxALL, 5 );
	
	m_panel621 = new wxPanel( m_panel413, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	m_panel621->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	wxBoxSizer* bSizer1781;
	bSizer1781 = new wxBoxSizer( wxVERTICAL );
	
	wxBoxSizer* bSizer176;
	bSizer176 = new wxBoxSizer( wxVERTICAL );
	
	m_staticText18613 = new wxStaticText( m_panel621, wxID_ANY, wxT("PickUp List"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText18613->Wrap( -1 );
	m_staticText18613->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), 70, 90, 92, false, wxEmptyString ) );
	m_staticText18613->SetForegroundColour( wxColour( 209, 76, 16 ) );
	
	bSizer176->Add( m_staticText18613, 0, wxALL, 0 );
	
	bSizer1781->Add( bSizer176, 0, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer177;
	bSizer177 = new wxBoxSizer( wxVERTICAL );
	
	m_pRectangularGrid_r = new wxGrid( m_panel621, wxID_ANY, wxDefaultPosition, wxSize( 100,100 ), wxHSCROLL|wxVSCROLL );
	
	// Grid
	m_pRectangularGrid_r->CreateGrid( 0, 7 );
	m_pRectangularGrid_r->EnableEditing( true );
	m_pRectangularGrid_r->EnableGridLines( true );
	m_pRectangularGrid_r->SetGridLineColour( wxColour( 128, 128, 128 ) );
	m_pRectangularGrid_r->EnableDragGridSize( false );
	m_pRectangularGrid_r->SetMargins( 0, 0 );
	
	// Columns
	m_pRectangularGrid_r->SetColSize( 0, 60 );
	m_pRectangularGrid_r->SetColSize( 1, 210 );
	m_pRectangularGrid_r->SetColSize( 2, 80 );
	m_pRectangularGrid_r->SetColSize( 3, 50 );
	m_pRectangularGrid_r->SetColSize( 4, 50 );
	m_pRectangularGrid_r->SetColSize( 5, 210 );
	m_pRectangularGrid_r->SetColSize( 6, 210 );
	m_pRectangularGrid_r->EnableDragColMove( false );
	m_pRectangularGrid_r->EnableDragColSize( true );
	m_pRectangularGrid_r->SetColLabelSize( 25 );
	m_pRectangularGrid_r->SetColLabelValue( 0, wxT("ID") );
	m_pRectangularGrid_r->SetColLabelValue( 1, wxT("Coordinate(x,y,z)") );
	m_pRectangularGrid_r->SetColLabelValue( 2, wxT("Width") );
	m_pRectangularGrid_r->SetColLabelValue( 3, wxT("Depth") );
	m_pRectangularGrid_r->SetColLabelValue( 4, wxT("Height") );
	m_pRectangularGrid_r->SetColLabelValue( 5, wxT("OrientationVector(x,y,z)") );
	m_pRectangularGrid_r->SetColLabelValue( 6, wxT("Dir(x,y,z)") );
	m_pRectangularGrid_r->SetColLabelAlignment( wxALIGN_LEFT, wxALIGN_CENTRE );
	
	// Rows
	m_pRectangularGrid_r->AutoSizeRows();
	m_pRectangularGrid_r->EnableDragRowSize( true );
	m_pRectangularGrid_r->SetRowLabelSize( 1 );
	m_pRectangularGrid_r->SetRowLabelAlignment( wxALIGN_CENTRE, wxALIGN_CENTRE );
	
	// Label Appearance
	m_pRectangularGrid_r->SetLabelBackgroundColour( wxColour( 88, 88, 88 ) );
	m_pRectangularGrid_r->SetLabelTextColour( wxColour( 255, 255, 255 ) );
	
	// Cell Defaults
	m_pRectangularGrid_r->SetDefaultCellBackgroundColour( wxColour( 44, 44, 44 ) );
	m_pRectangularGrid_r->SetDefaultCellTextColour( wxColour( 255, 255, 255 ) );
	m_pRectangularGrid_r->SetDefaultCellAlignment( wxALIGN_LEFT, wxALIGN_TOP );
	m_pRectangularGrid_r->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pRectangularGrid_r->SetBackgroundColour( wxColour( 0, 0, 0 ) );
	
	bSizer177->Add( m_pRectangularGrid_r, 1, wxALL|wxEXPAND, 5 );
	
	bSizer1781->Add( bSizer177, 1, wxEXPAND, 5 );
	
	m_panel621->SetSizer( bSizer1781 );
	m_panel621->Layout();
	bSizer1781->Fit( m_panel621 );
	bSizer1753->Add( m_panel621, 1, wxALL|wxEXPAND, 5 );
	
	m_panel413->SetSizer( bSizer1753 );
	m_panel413->Layout();
	bSizer1753->Fit( m_panel413 );
	m_notebook4->AddPage( m_panel413, wxT("Rectangular"), false );
	m_panel4131 = new wxPanel( m_notebook4, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	m_panel4131->SetBackgroundColour( wxColour( 44, 44, 44 ) );
	
	wxBoxSizer* bSizer17531;
	bSizer17531 = new wxBoxSizer( wxVERTICAL );
	
	m_panel4411 = new wxPanel( m_panel4131, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	m_panel4411->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	wxBoxSizer* bSizer18111;
	bSizer18111 = new wxBoxSizer( wxVERTICAL );
	
	m_staticText18621 = new wxStaticText( m_panel4411, wxID_ANY, wxT("Central coordinate"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText18621->Wrap( -1 );
	m_staticText18621->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), 70, 90, 92, false, wxEmptyString ) );
	m_staticText18621->SetForegroundColour( wxColour( 209, 76, 16 ) );
	
	bSizer18111->Add( m_staticText18621, 0, wxALL, 0 );
	
	wxFlexGridSizer* fgSizer1321;
	fgSizer1321 = new wxFlexGridSizer( 4, 6, 0, 0 );
	fgSizer1321->SetFlexibleDirection( wxBOTH );
	fgSizer1321->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );
	
	m_staticText18721 = new wxStaticText( m_panel4411, wxID_ANY, wxT("Axis"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText18721->Wrap( -1 );
	m_staticText18721->SetForegroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_HIGHLIGHTTEXT ) );
	
	fgSizer1321->Add( m_staticText18721, 0, wxALL, 5 );
	
	m_staticText20211 = new wxStaticText( m_panel4411, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText20211->Wrap( -1 );
	fgSizer1321->Add( m_staticText20211, 0, wxALL, 5 );
	
	m_staticText18821 = new wxStaticText( m_panel4411, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 100,-1 ), 0 );
	m_staticText18821->Wrap( -1 );
	fgSizer1321->Add( m_staticText18821, 0, wxALL, 5 );
	
	m_staticText20111 = new wxStaticText( m_panel4411, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText20111->Wrap( -1 );
	fgSizer1321->Add( m_staticText20111, 0, wxALL, 5 );
	
	m_staticText18921 = new wxStaticText( m_panel4411, wxID_ANY, wxT("Position"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText18921->Wrap( -1 );
	m_staticText18921->SetForegroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_HIGHLIGHTTEXT ) );
	
	fgSizer1321->Add( m_staticText18921, 0, wxALL, 5 );
	
	m_staticText19021 = new wxStaticText( m_panel4411, wxID_ANY, wxT("Pitch"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText19021->Wrap( -1 );
	m_staticText19021->SetForegroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_HIGHLIGHTTEXT ) );
	
	fgSizer1321->Add( m_staticText19021, 0, wxALL, 5 );
	
	m_staticText16121 = new wxStaticText( m_panel4411, wxID_ANY, wxT("X"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText16121->Wrap( -1 );
	m_staticText16121->SetForegroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_HIGHLIGHTTEXT ) );
	
	fgSizer1321->Add( m_staticText16121, 0, wxALL, 5 );
	
	m_pXPosDownBtn_c = new wxBitmapButton( m_panel4411, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxSize( -1,-1 ), 0 );
	m_pXPosDownBtn_c->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	m_pXPosDownBtn_c->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	fgSizer1321->Add( m_pXPosDownBtn_c, 0, wxALL, 5 );
	
	m_pXSlider_c = new wxSlider( m_panel4411, wxID_ANY, 50, 0, 100, wxDefaultPosition, wxSize( 100,-1 ), wxSL_HORIZONTAL );
	fgSizer1321->Add( m_pXSlider_c, 0, wxALL, 5 );
	
	m_pXPosUpBtn_c = new wxBitmapButton( m_panel4411, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxSize( -1,-1 ), 0 );
	m_pXPosUpBtn_c->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	m_pXPosUpBtn_c->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	fgSizer1321->Add( m_pXPosUpBtn_c, 0, wxALL, 5 );
	
	m_pXPosTxt_c = new wxTextCtrl( m_panel4411, SLICE_CTRL_XPOS_C_TXT, wxEmptyString, wxDefaultPosition, wxSize( 70,-1 ), wxTE_PROCESS_ENTER );
	fgSizer1321->Add( m_pXPosTxt_c, 0, wxALL, 5 );
	
	m_pXPitchTxt_c = new wxTextCtrl( m_panel4411, SLICE_CTRL_XPITCH_C_TXT, wxEmptyString, wxDefaultPosition, wxSize( 70,-1 ), wxTE_PROCESS_ENTER );
	fgSizer1321->Add( m_pXPitchTxt_c, 0, wxALL, 5 );
	
	m_staticText16221 = new wxStaticText( m_panel4411, wxID_ANY, wxT("Y"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText16221->Wrap( -1 );
	m_staticText16221->SetForegroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_HIGHLIGHTTEXT ) );
	
	fgSizer1321->Add( m_staticText16221, 0, wxALL, 5 );
	
	m_pYPosDownBtn_c = new wxBitmapButton( m_panel4411, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxSize( -1,-1 ), 0 );
	m_pYPosDownBtn_c->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	m_pYPosDownBtn_c->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	fgSizer1321->Add( m_pYPosDownBtn_c, 0, wxALL, 5 );
	
	m_pYSlider_c = new wxSlider( m_panel4411, wxID_ANY, 50, 0, 100, wxDefaultPosition, wxSize( 100,-1 ), wxSL_HORIZONTAL );
	fgSizer1321->Add( m_pYSlider_c, 0, wxALL, 5 );
	
	m_pYPosUpBtn_c = new wxBitmapButton( m_panel4411, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxSize( -1,-1 ), 0 );
	m_pYPosUpBtn_c->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	m_pYPosUpBtn_c->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	fgSizer1321->Add( m_pYPosUpBtn_c, 0, wxALL, 5 );
	
	m_pYPosTxt_c = new wxTextCtrl( m_panel4411, SLICE_CTRL_YPOS_C_TXT, wxEmptyString, wxDefaultPosition, wxSize( 70,-1 ), wxTE_PROCESS_ENTER );
	fgSizer1321->Add( m_pYPosTxt_c, 0, wxALL, 5 );
	
	m_pYPitchTxt_c = new wxTextCtrl( m_panel4411, SLICE_CTRL_YPITCH_C_TXT, wxEmptyString, wxDefaultPosition, wxSize( 70,-1 ), wxTE_PROCESS_ENTER );
	fgSizer1321->Add( m_pYPitchTxt_c, 0, wxALL, 5 );
	
	m_staticText16321 = new wxStaticText( m_panel4411, wxID_ANY, wxT("Z"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText16321->Wrap( -1 );
	m_staticText16321->SetForegroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_HIGHLIGHTTEXT ) );
	
	fgSizer1321->Add( m_staticText16321, 0, wxALL, 5 );
	
	m_pZPosDownBtn_c = new wxBitmapButton( m_panel4411, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxSize( -1,-1 ), 0 );
	m_pZPosDownBtn_c->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	m_pZPosDownBtn_c->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	fgSizer1321->Add( m_pZPosDownBtn_c, 0, wxALL, 5 );
	
	m_pZSlider_c = new wxSlider( m_panel4411, wxID_ANY, 50, 0, 100, wxDefaultPosition, wxSize( 100,-1 ), wxSL_HORIZONTAL );
	fgSizer1321->Add( m_pZSlider_c, 0, wxALL, 5 );
	
	m_pZPosUpBtn_c = new wxBitmapButton( m_panel4411, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxSize( -1,-1 ), 0 );
	m_pZPosUpBtn_c->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	m_pZPosUpBtn_c->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	fgSizer1321->Add( m_pZPosUpBtn_c, 0, wxALL, 5 );
	
	m_pZPosTxt_c = new wxTextCtrl( m_panel4411, SLICE_CTRL_ZPOS_C_TXT, wxEmptyString, wxDefaultPosition, wxSize( 70,-1 ), wxTE_PROCESS_ENTER );
	fgSizer1321->Add( m_pZPosTxt_c, 0, wxALL, 5 );
	
	m_pZPitchTxt_c = new wxTextCtrl( m_panel4411, SLICE_CTRL_ZPITCH_C_TXT, wxEmptyString, wxDefaultPosition, wxSize( 70,-1 ), wxTE_PROCESS_ENTER );
	fgSizer1321->Add( m_pZPitchTxt_c, 0, wxALL, 5 );
	
	bSizer18111->Add( fgSizer1321, 1, wxEXPAND, 5 );
	
	m_panel4411->SetSizer( bSizer18111 );
	m_panel4411->Layout();
	bSizer18111->Fit( m_panel4411 );
	bSizer17531->Add( m_panel4411, 0, wxEXPAND | wxALL, 5 );
	
	m_panel42132 = new wxPanel( m_panel4131, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	m_panel42132->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	wxFlexGridSizer* fgSizer1732;
	fgSizer1732 = new wxFlexGridSizer( 4, 7, 0, 0 );
	fgSizer1732->SetFlexibleDirection( wxBOTH );
	fgSizer1732->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );
	
	m_staticText16632 = new wxStaticText( m_panel42132, wxID_ANY, wxT("Depth"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText16632->Wrap( -1 );
	m_staticText16632->SetForegroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_HIGHLIGHTTEXT ) );
	
	fgSizer1732->Add( m_staticText16632, 0, wxALL, 5 );
	
	m_staticText16732 = new wxStaticText( m_panel42132, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 50,-1 ), 0 );
	m_staticText16732->Wrap( -1 );
	fgSizer1732->Add( m_staticText16732, 0, wxALL, 5 );
	
	m_pDepthTxt_c = new wxTextCtrl( m_panel42132, DEPTH_C_TXT, wxEmptyString, wxDefaultPosition, wxSize( 70,-1 ), wxTE_PROCESS_ENTER );
	fgSizer1732->Add( m_pDepthTxt_c, 0, wxALL, 0 );
	
	m_staticText222 = new wxStaticText( m_panel42132, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText222->Wrap( -1 );
	fgSizer1732->Add( m_staticText222, 0, wxALL, 5 );
	
	m_staticText223 = new wxStaticText( m_panel42132, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText223->Wrap( -1 );
	fgSizer1732->Add( m_staticText223, 0, wxALL, 5 );
	
	m_staticText224 = new wxStaticText( m_panel42132, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText224->Wrap( -1 );
	fgSizer1732->Add( m_staticText224, 0, wxALL, 5 );
	
	m_staticText225 = new wxStaticText( m_panel42132, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText225->Wrap( -1 );
	fgSizer1732->Add( m_staticText225, 0, wxALL, 5 );
	
	m_staticText2601 = new wxStaticText( m_panel42132, wxID_ANY, wxT("FanRadius"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText2601->Wrap( -1 );
	m_staticText2601->SetForegroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_HIGHLIGHTTEXT ) );
	
	fgSizer1732->Add( m_staticText2601, 0, wxALL, 5 );
	
	m_staticText2611 = new wxStaticText( m_panel42132, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText2611->Wrap( -1 );
	fgSizer1732->Add( m_staticText2611, 0, wxALL, 5 );
	
	m_pFanRadiusTxt_c = new wxTextCtrl( m_panel42132, FAN_RADIUS_C_TXT, wxEmptyString, wxDefaultPosition, wxSize( 70,-1 ), wxTE_PROCESS_ENTER );
	fgSizer1732->Add( m_pFanRadiusTxt_c, 0, wxALL, 0 );
	
	m_staticText226 = new wxStaticText( m_panel42132, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText226->Wrap( -1 );
	fgSizer1732->Add( m_staticText226, 0, wxALL, 5 );
	
	m_staticText227 = new wxStaticText( m_panel42132, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText227->Wrap( -1 );
	fgSizer1732->Add( m_staticText227, 0, wxALL, 5 );
	
	m_staticText228 = new wxStaticText( m_panel42132, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText228->Wrap( -1 );
	fgSizer1732->Add( m_staticText228, 0, wxALL, 5 );
	
	m_staticText229 = new wxStaticText( m_panel42132, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText229->Wrap( -1 );
	fgSizer1732->Add( m_staticText229, 0, wxALL, 5 );
	
	m_staticText2622 = new wxStaticText( m_panel42132, wxID_ANY, wxT("BossRadius"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText2622->Wrap( -1 );
	m_staticText2622->SetForegroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_HIGHLIGHTTEXT ) );
	
	fgSizer1732->Add( m_staticText2622, 0, wxALL, 5 );
	
	m_staticText2631 = new wxStaticText( m_panel42132, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText2631->Wrap( -1 );
	fgSizer1732->Add( m_staticText2631, 0, wxALL, 5 );
	
	m_pBossRadTxt_c = new wxTextCtrl( m_panel42132, BOSS_RADIUS_C_TXT, wxEmptyString, wxDefaultPosition, wxSize( 70,-1 ), wxTE_PROCESS_ENTER );
	fgSizer1732->Add( m_pBossRadTxt_c, 0, wxALL, 0 );
	
	m_staticText230 = new wxStaticText( m_panel42132, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText230->Wrap( -1 );
	fgSizer1732->Add( m_staticText230, 0, wxALL, 5 );
	
	m_staticText231 = new wxStaticText( m_panel42132, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText231->Wrap( -1 );
	fgSizer1732->Add( m_staticText231, 0, wxALL, 5 );
	
	m_staticText232 = new wxStaticText( m_panel42132, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText232->Wrap( -1 );
	fgSizer1732->Add( m_staticText232, 0, wxALL, 5 );
	
	m_staticText233 = new wxStaticText( m_panel42132, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText233->Wrap( -1 );
	fgSizer1732->Add( m_staticText233, 0, wxALL, 5 );
	
	m_staticText26211 = new wxStaticText( m_panel42132, wxID_ANY, wxT("OrientationVector"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText26211->Wrap( -1 );
	m_staticText26211->SetForegroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_HIGHLIGHTTEXT ) );
	
	fgSizer1732->Add( m_staticText26211, 0, wxALL, 5 );
	
	m_staticText2671 = new wxStaticText( m_panel42132, wxID_ANY, wxT("X:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText2671->Wrap( -1 );
	m_staticText2671->SetForegroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_HIGHLIGHTTEXT ) );
	
	fgSizer1732->Add( m_staticText2671, 0, wxALIGN_RIGHT|wxALL, 5 );
	
	m_pOrientationVecXTxt_c = new wxTextCtrl( m_panel42132, ORIENTATION_VEC_X_C_TXT, wxEmptyString, wxDefaultPosition, wxSize( 70,-1 ), wxTE_PROCESS_ENTER );
	fgSizer1732->Add( m_pOrientationVecXTxt_c, 0, wxALL, 0 );
	
	m_staticText234 = new wxStaticText( m_panel42132, wxID_ANY, wxT("Y:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText234->Wrap( -1 );
	m_staticText234->SetForegroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_HIGHLIGHTTEXT ) );
	
	fgSizer1732->Add( m_staticText234, 0, wxALIGN_RIGHT|wxALL, 5 );
	
	m_pOrientationVecYTxt_c = new wxTextCtrl( m_panel42132, ORIENTATION_VEC_Y_C_TXT, wxEmptyString, wxDefaultPosition, wxSize( 70,-1 ), wxTE_PROCESS_ENTER );
	fgSizer1732->Add( m_pOrientationVecYTxt_c, 0, wxALL, 0 );
	
	m_staticText235 = new wxStaticText( m_panel42132, wxID_ANY, wxT("Z:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText235->Wrap( -1 );
	m_staticText235->SetForegroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_HIGHLIGHTTEXT ) );
	
	fgSizer1732->Add( m_staticText235, 0, wxALIGN_RIGHT|wxALL, 5 );
	
	m_pOrientationVecZTxt_c = new wxTextCtrl( m_panel42132, ORIENTATION_VEC_Z_C_TXT, wxEmptyString, wxDefaultPosition, wxSize( 70,-1 ), wxTE_PROCESS_ENTER );
	fgSizer1732->Add( m_pOrientationVecZTxt_c, 0, wxALL, 0 );
	
	m_panel42132->SetSizer( fgSizer1732 );
	m_panel42132->Layout();
	fgSizer1732->Fit( m_panel42132 );
	bSizer17531->Add( m_panel42132, 0, wxEXPAND | wxALL, 5 );
	
	m_staticline1631 = new wxStaticLine( m_panel4131, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	bSizer17531->Add( m_staticline1631, 0, wxEXPAND | wxALL, 5 );
	
	wxBoxSizer* bSizer16431;
	bSizer16431 = new wxBoxSizer( wxHORIZONTAL );
	
	m_button_Add_c = new wxButton( m_panel4131, wxID_ANY, wxT("Add"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer16431->Add( m_button_Add_c, 0, wxALL, 5 );
	
	m_button_Clear_c = new wxButton( m_panel4131, wxID_ANY, wxT("Clear"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer16431->Add( m_button_Clear_c, 0, wxALL, 5 );
	
	m_button_AllClear_c = new wxButton( m_panel4131, wxID_ANY, wxT("All Clear"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer16431->Add( m_button_AllClear_c, 0, wxALL, 5 );
	
	bSizer17531->Add( bSizer16431, 0, wxALIGN_RIGHT, 5 );
	
	m_staticline1731 = new wxStaticLine( m_panel4131, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	bSizer17531->Add( m_staticline1731, 0, wxEXPAND | wxALL, 5 );
	
	m_panel6211 = new wxPanel( m_panel4131, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	m_panel6211->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	wxBoxSizer* bSizer17811;
	bSizer17811 = new wxBoxSizer( wxVERTICAL );
	
	wxBoxSizer* bSizer178_a;
	bSizer178_a = new wxBoxSizer( wxVERTICAL );
	
	m_staticText186131 = new wxStaticText( m_panel6211, wxID_ANY, wxT("PickUp List"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText186131->Wrap( -1 );
	m_staticText186131->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), 70, 90, 92, false, wxEmptyString ) );
	m_staticText186131->SetForegroundColour( wxColour( 209, 76, 16 ) );
	
	bSizer178_a->Add( m_staticText186131, 0, wxALL, 0 );
	
	bSizer17811->Add( bSizer178_a, 0, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer179;
	bSizer179 = new wxBoxSizer( wxVERTICAL );
	
	m_pCylinderGrid_c = new wxGrid( m_panel6211, wxID_ANY, wxDefaultPosition, wxSize( 100,100 ), wxHSCROLL|wxVSCROLL );
	
	// Grid
	m_pCylinderGrid_c->CreateGrid( 0, 6 );
	m_pCylinderGrid_c->EnableEditing( true );
	m_pCylinderGrid_c->EnableGridLines( true );
	m_pCylinderGrid_c->SetGridLineColour( wxColour( 128, 128, 128 ) );
	m_pCylinderGrid_c->EnableDragGridSize( false );
	m_pCylinderGrid_c->SetMargins( 0, 0 );
	
	// Columns
	m_pCylinderGrid_c->SetColSize( 0, 80 );
	m_pCylinderGrid_c->SetColSize( 1, 210 );
	m_pCylinderGrid_c->SetColSize( 2, 80 );
	m_pCylinderGrid_c->SetColSize( 3, 80 );
	m_pCylinderGrid_c->SetColSize( 4, 80 );
	m_pCylinderGrid_c->SetColSize( 5, 210 );
	m_pCylinderGrid_c->EnableDragColMove( false );
	m_pCylinderGrid_c->EnableDragColSize( true );
	m_pCylinderGrid_c->SetColLabelSize( 25 );
	m_pCylinderGrid_c->SetColLabelValue( 0, wxT("ID") );
	m_pCylinderGrid_c->SetColLabelValue( 1, wxT("Coordinate(x,y,z)") );
	m_pCylinderGrid_c->SetColLabelValue( 2, wxT("Depth") );
	m_pCylinderGrid_c->SetColLabelValue( 3, wxT("FanRadius") );
	m_pCylinderGrid_c->SetColLabelValue( 4, wxT("BossRadius") );
	m_pCylinderGrid_c->SetColLabelValue( 5, wxT("OrientationVector(x,y,z)") );
	m_pCylinderGrid_c->SetColLabelAlignment( wxALIGN_LEFT, wxALIGN_CENTRE );
	
	// Rows
	m_pCylinderGrid_c->AutoSizeRows();
	m_pCylinderGrid_c->EnableDragRowSize( true );
	m_pCylinderGrid_c->SetRowLabelSize( 1 );
	m_pCylinderGrid_c->SetRowLabelAlignment( wxALIGN_CENTRE, wxALIGN_CENTRE );
	
	// Label Appearance
	m_pCylinderGrid_c->SetLabelBackgroundColour( wxColour( 88, 88, 88 ) );
	m_pCylinderGrid_c->SetLabelTextColour( wxColour( 255, 255, 255 ) );
	
	// Cell Defaults
	m_pCylinderGrid_c->SetDefaultCellBackgroundColour( wxColour( 44, 44, 44 ) );
	m_pCylinderGrid_c->SetDefaultCellTextColour( wxColour( 255, 255, 255 ) );
	m_pCylinderGrid_c->SetDefaultCellAlignment( wxALIGN_LEFT, wxALIGN_TOP );
	m_pCylinderGrid_c->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pCylinderGrid_c->SetBackgroundColour( wxColour( 0, 0, 0 ) );
	
	bSizer179->Add( m_pCylinderGrid_c, 1, wxALL|wxEXPAND, 5 );
	
	bSizer17811->Add( bSizer179, 1, wxEXPAND, 5 );
	
	m_panel6211->SetSizer( bSizer17811 );
	m_panel6211->Layout();
	bSizer17811->Fit( m_panel6211 );
	bSizer17531->Add( m_panel6211, 1, wxALL|wxEXPAND, 5 );
	
	m_panel4131->SetSizer( bSizer17531 );
	m_panel4131->Layout();
	bSizer17531->Fit( m_panel4131 );
	m_notebook4->AddPage( m_panel4131, wxT("Cylinder"), false );
	
	bSizer167->Add( m_notebook4, 0, wxALL|wxEXPAND, 0 );
	
	m_panel49 = new wxPanel( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	m_panel49->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	wxBoxSizer* bSizer1812;
	bSizer1812 = new wxBoxSizer( wxVERTICAL );
	
	wxBoxSizer* bSizer182;
	bSizer182 = new wxBoxSizer( wxVERTICAL );
	
	m_pCloseBtn = new wxButton( m_panel49, wxID_ANY, wxT("Close"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer182->Add( m_pCloseBtn, 0, wxALL, 5 );
	
	bSizer1812->Add( bSizer182, 1, wxALIGN_RIGHT, 5 );
	
	m_panel49->SetSizer( bSizer1812 );
	m_panel49->Layout();
	bSizer1812->Fit( m_panel49 );
	bSizer167->Add( m_panel49, 1, wxEXPAND, 5 );
	
	this->SetSizer( bSizer167 );
	this->Layout();
	
	this->Centre( wxBOTH );
	
	// Connect Events
	this->Connect( wxEVT_ACTIVATE, wxActivateEventHandler( CoordinateViewerFrame::OnActivate ) );
	this->Connect( wxEVT_ACTIVATE_APP, wxActivateEventHandler( CoordinateViewerFrame::OnActivateApp ) );
	this->Connect( wxEVT_CLOSE_WINDOW, wxCloseEventHandler( CoordinateViewerFrame::OnClose ) );
	m_notebook4->Connect( wxEVT_COMMAND_NOTEBOOK_PAGE_CHANGED, wxNotebookEventHandler( CoordinateViewerFrame::m_notebookOnNotebookPageChanged ), NULL, this );
	m_notebook4->Connect( wxEVT_COMMAND_NOTEBOOK_PAGE_CHANGING, wxNotebookEventHandler( CoordinateViewerFrame::m_notebookOnNotebookPageChanging ), NULL, this );
	m_pXPosDownBtn_p->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CoordinateViewerFrame::OnXPosDown_p ), NULL, this );
	m_pXSlider_p->Connect( wxEVT_SCROLL_TOP, wxScrollEventHandler( CoordinateViewerFrame::OnXSlideScroll_p ), NULL, this );
	m_pXSlider_p->Connect( wxEVT_SCROLL_BOTTOM, wxScrollEventHandler( CoordinateViewerFrame::OnXSlideScroll_p ), NULL, this );
	m_pXSlider_p->Connect( wxEVT_SCROLL_LINEUP, wxScrollEventHandler( CoordinateViewerFrame::OnXSlideScroll_p ), NULL, this );
	m_pXSlider_p->Connect( wxEVT_SCROLL_LINEDOWN, wxScrollEventHandler( CoordinateViewerFrame::OnXSlideScroll_p ), NULL, this );
	m_pXSlider_p->Connect( wxEVT_SCROLL_PAGEUP, wxScrollEventHandler( CoordinateViewerFrame::OnXSlideScroll_p ), NULL, this );
	m_pXSlider_p->Connect( wxEVT_SCROLL_PAGEDOWN, wxScrollEventHandler( CoordinateViewerFrame::OnXSlideScroll_p ), NULL, this );
	m_pXSlider_p->Connect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( CoordinateViewerFrame::OnXSlideScroll_p ), NULL, this );
	m_pXSlider_p->Connect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( CoordinateViewerFrame::OnXSlideScroll_p ), NULL, this );
	m_pXSlider_p->Connect( wxEVT_SCROLL_CHANGED, wxScrollEventHandler( CoordinateViewerFrame::OnXSlideScroll_p ), NULL, this );
	m_pXSlider_p->Connect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( CoordinateViewerFrame::OnXSlideRelease_p ), NULL, this );
	m_pXSlider_p->Connect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( CoordinateViewerFrame::OnXSlideDrag_p ), NULL, this );
	m_pXPosUpBtn_p->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CoordinateViewerFrame::OnXPosUp_p ), NULL, this );
	m_pXPosTxt_p->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( CoordinateViewerFrame::m_pXPosTxt_pOnKillFocus ), NULL, this );
	m_pXPosTxt_p->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( CoordinateViewerFrame::OnPosTxt_p ), NULL, this );
	m_pXPitchTxt_p->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( CoordinateViewerFrame::m_pXPitchTxt_pOnKillFocus ), NULL, this );
	m_pXPitchTxt_p->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( CoordinateViewerFrame::OnPitchTxt_p ), NULL, this );
	m_pYPosDownBtn_p->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CoordinateViewerFrame::OnYPosDown_p ), NULL, this );
	m_pYSlider_p->Connect( wxEVT_SCROLL_TOP, wxScrollEventHandler( CoordinateViewerFrame::OnYSlideScroll_p ), NULL, this );
	m_pYSlider_p->Connect( wxEVT_SCROLL_BOTTOM, wxScrollEventHandler( CoordinateViewerFrame::OnYSlideScroll_p ), NULL, this );
	m_pYSlider_p->Connect( wxEVT_SCROLL_LINEUP, wxScrollEventHandler( CoordinateViewerFrame::OnYSlideScroll_p ), NULL, this );
	m_pYSlider_p->Connect( wxEVT_SCROLL_LINEDOWN, wxScrollEventHandler( CoordinateViewerFrame::OnYSlideScroll_p ), NULL, this );
	m_pYSlider_p->Connect( wxEVT_SCROLL_PAGEUP, wxScrollEventHandler( CoordinateViewerFrame::OnYSlideScroll_p ), NULL, this );
	m_pYSlider_p->Connect( wxEVT_SCROLL_PAGEDOWN, wxScrollEventHandler( CoordinateViewerFrame::OnYSlideScroll_p ), NULL, this );
	m_pYSlider_p->Connect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( CoordinateViewerFrame::OnYSlideScroll_p ), NULL, this );
	m_pYSlider_p->Connect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( CoordinateViewerFrame::OnYSlideScroll_p ), NULL, this );
	m_pYSlider_p->Connect( wxEVT_SCROLL_CHANGED, wxScrollEventHandler( CoordinateViewerFrame::OnYSlideScroll_p ), NULL, this );
	m_pYSlider_p->Connect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( CoordinateViewerFrame::OnYSlideRelease_p ), NULL, this );
	m_pYSlider_p->Connect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( CoordinateViewerFrame::OnYSlideDrag_p ), NULL, this );
	m_pYPosUpBtn_p->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CoordinateViewerFrame::OnYPosUp_p ), NULL, this );
	m_pYPosTxt_p->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( CoordinateViewerFrame::m_pYPosTxt_pOnKillFocus ), NULL, this );
	m_pYPosTxt_p->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( CoordinateViewerFrame::OnPosTxt_p ), NULL, this );
	m_pYPitchTxt_p->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( CoordinateViewerFrame::m_pYPitchTxt_pOnKillFocus ), NULL, this );
	m_pYPitchTxt_p->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( CoordinateViewerFrame::OnPitchTxt_p ), NULL, this );
	m_pZPosDownBtn_p->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CoordinateViewerFrame::OnZPosDown_p ), NULL, this );
	m_pZSlider_p->Connect( wxEVT_SCROLL_TOP, wxScrollEventHandler( CoordinateViewerFrame::OnZSlideScroll_p ), NULL, this );
	m_pZSlider_p->Connect( wxEVT_SCROLL_BOTTOM, wxScrollEventHandler( CoordinateViewerFrame::OnZSlideScroll_p ), NULL, this );
	m_pZSlider_p->Connect( wxEVT_SCROLL_LINEUP, wxScrollEventHandler( CoordinateViewerFrame::OnZSlideScroll_p ), NULL, this );
	m_pZSlider_p->Connect( wxEVT_SCROLL_LINEDOWN, wxScrollEventHandler( CoordinateViewerFrame::OnZSlideScroll_p ), NULL, this );
	m_pZSlider_p->Connect( wxEVT_SCROLL_PAGEUP, wxScrollEventHandler( CoordinateViewerFrame::OnZSlideScroll_p ), NULL, this );
	m_pZSlider_p->Connect( wxEVT_SCROLL_PAGEDOWN, wxScrollEventHandler( CoordinateViewerFrame::OnZSlideScroll_p ), NULL, this );
	m_pZSlider_p->Connect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( CoordinateViewerFrame::OnZSlideScroll_p ), NULL, this );
	m_pZSlider_p->Connect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( CoordinateViewerFrame::OnZSlideScroll_p ), NULL, this );
	m_pZSlider_p->Connect( wxEVT_SCROLL_CHANGED, wxScrollEventHandler( CoordinateViewerFrame::OnZSlideScroll_p ), NULL, this );
	m_pZSlider_p->Connect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( CoordinateViewerFrame::OnZSlideRelease_p ), NULL, this );
	m_pZSlider_p->Connect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( CoordinateViewerFrame::OnZSlideDrag_p ), NULL, this );
	m_pZPosUpBtn_p->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CoordinateViewerFrame::OnZPosUp_p ), NULL, this );
	m_pZPosTxt_p->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( CoordinateViewerFrame::m_pZPosTxt_pOnKillFocus ), NULL, this );
	m_pZPosTxt_p->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( CoordinateViewerFrame::OnPosTxt_p ), NULL, this );
	m_pZPitchTxt_p->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( CoordinateViewerFrame::m_pZPitchTxt_pOnKillFocus ), NULL, this );
	m_pZPitchTxt_p->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( CoordinateViewerFrame::OnPitchTxt_p ), NULL, this );
	m_pSizeTxt_p->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( CoordinateViewerFrame::m_pSizeTxt_pOnKillFocus ), NULL, this );
	m_pSizeTxt_p->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( CoordinateViewerFrame::OnSizeTxt_p ), NULL, this );
	m_button_Add_p->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CoordinateViewerFrame::m_button_list_AddButtonClick_p ), NULL, this );
	m_button_Clear_p->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CoordinateViewerFrame::m_button_list_ClearButtonClick_p ), NULL, this );
	m_button_AllClear_p->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CoordinateViewerFrame::m_button_list_AllClearButtonClick_p ), NULL, this );
	m_pPointGrid_p->Connect( wxEVT_GRID_SELECT_CELL, wxGridEventHandler( CoordinateViewerFrame::m_pPointGrid_pOnGridSelectCell ), NULL, this );
	m_pXPosDownBtn_r->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CoordinateViewerFrame::OnXPosDown_r ), NULL, this );
	m_pXSlider_r->Connect( wxEVT_SCROLL_TOP, wxScrollEventHandler( CoordinateViewerFrame::OnXSlideScroll_r ), NULL, this );
	m_pXSlider_r->Connect( wxEVT_SCROLL_BOTTOM, wxScrollEventHandler( CoordinateViewerFrame::OnXSlideScroll_r ), NULL, this );
	m_pXSlider_r->Connect( wxEVT_SCROLL_LINEUP, wxScrollEventHandler( CoordinateViewerFrame::OnXSlideScroll_r ), NULL, this );
	m_pXSlider_r->Connect( wxEVT_SCROLL_LINEDOWN, wxScrollEventHandler( CoordinateViewerFrame::OnXSlideScroll_r ), NULL, this );
	m_pXSlider_r->Connect( wxEVT_SCROLL_PAGEUP, wxScrollEventHandler( CoordinateViewerFrame::OnXSlideScroll_r ), NULL, this );
	m_pXSlider_r->Connect( wxEVT_SCROLL_PAGEDOWN, wxScrollEventHandler( CoordinateViewerFrame::OnXSlideScroll_r ), NULL, this );
	m_pXSlider_r->Connect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( CoordinateViewerFrame::OnXSlideScroll_r ), NULL, this );
	m_pXSlider_r->Connect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( CoordinateViewerFrame::OnXSlideScroll_r ), NULL, this );
	m_pXSlider_r->Connect( wxEVT_SCROLL_CHANGED, wxScrollEventHandler( CoordinateViewerFrame::OnXSlideScroll_r ), NULL, this );
	m_pXSlider_r->Connect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( CoordinateViewerFrame::OnXSlideRelease_r ), NULL, this );
	m_pXSlider_r->Connect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( CoordinateViewerFrame::OnXSlideDrag_r ), NULL, this );
	m_pXPosUpBtn_r->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CoordinateViewerFrame::OnXPosUp_r ), NULL, this );
	m_pXPosTxt_r->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( CoordinateViewerFrame::m_pXPosTxt_rOnKillFocus ), NULL, this );
	m_pXPosTxt_r->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( CoordinateViewerFrame::OnPosTxt_r ), NULL, this );
	m_pXPitchTxt_r->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( CoordinateViewerFrame::m_pXPitchTxt_rOnKillFocus ), NULL, this );
	m_pXPitchTxt_r->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( CoordinateViewerFrame::OnPitchTxt_r ), NULL, this );
	m_pYPosDownBtn_r->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CoordinateViewerFrame::OnYPosDown_r ), NULL, this );
	m_pYSlider_r->Connect( wxEVT_SCROLL_TOP, wxScrollEventHandler( CoordinateViewerFrame::OnYSlideScroll_r ), NULL, this );
	m_pYSlider_r->Connect( wxEVT_SCROLL_BOTTOM, wxScrollEventHandler( CoordinateViewerFrame::OnYSlideScroll_r ), NULL, this );
	m_pYSlider_r->Connect( wxEVT_SCROLL_LINEUP, wxScrollEventHandler( CoordinateViewerFrame::OnYSlideScroll_r ), NULL, this );
	m_pYSlider_r->Connect( wxEVT_SCROLL_LINEDOWN, wxScrollEventHandler( CoordinateViewerFrame::OnYSlideScroll_r ), NULL, this );
	m_pYSlider_r->Connect( wxEVT_SCROLL_PAGEUP, wxScrollEventHandler( CoordinateViewerFrame::OnYSlideScroll_r ), NULL, this );
	m_pYSlider_r->Connect( wxEVT_SCROLL_PAGEDOWN, wxScrollEventHandler( CoordinateViewerFrame::OnYSlideScroll_r ), NULL, this );
	m_pYSlider_r->Connect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( CoordinateViewerFrame::OnYSlideScroll_r ), NULL, this );
	m_pYSlider_r->Connect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( CoordinateViewerFrame::OnYSlideScroll_r ), NULL, this );
	m_pYSlider_r->Connect( wxEVT_SCROLL_CHANGED, wxScrollEventHandler( CoordinateViewerFrame::OnYSlideScroll_r ), NULL, this );
	m_pYSlider_r->Connect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( CoordinateViewerFrame::OnYSlideRelease_r ), NULL, this );
	m_pYSlider_r->Connect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( CoordinateViewerFrame::OnYSlideDrag_r ), NULL, this );
	m_pYPosUpBtn_r->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CoordinateViewerFrame::OnYPosUp_r ), NULL, this );
	m_pYPosTxt_r->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( CoordinateViewerFrame::m_pYPosTxt_rOnKillFocus ), NULL, this );
	m_pYPosTxt_r->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( CoordinateViewerFrame::OnPosTxt_r ), NULL, this );
	m_pYPitchTxt_r->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( CoordinateViewerFrame::m_pYPitchTxt_rOnKillFocus ), NULL, this );
	m_pYPitchTxt_r->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( CoordinateViewerFrame::OnPitchTxt_r ), NULL, this );
	m_pZPosDownBtn_r->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CoordinateViewerFrame::OnZPosDown_r ), NULL, this );
	m_pZSlider_r->Connect( wxEVT_SCROLL_TOP, wxScrollEventHandler( CoordinateViewerFrame::OnZSlideScroll_r ), NULL, this );
	m_pZSlider_r->Connect( wxEVT_SCROLL_BOTTOM, wxScrollEventHandler( CoordinateViewerFrame::OnZSlideScroll_r ), NULL, this );
	m_pZSlider_r->Connect( wxEVT_SCROLL_LINEUP, wxScrollEventHandler( CoordinateViewerFrame::OnZSlideScroll_r ), NULL, this );
	m_pZSlider_r->Connect( wxEVT_SCROLL_LINEDOWN, wxScrollEventHandler( CoordinateViewerFrame::OnZSlideScroll_r ), NULL, this );
	m_pZSlider_r->Connect( wxEVT_SCROLL_PAGEUP, wxScrollEventHandler( CoordinateViewerFrame::OnZSlideScroll_r ), NULL, this );
	m_pZSlider_r->Connect( wxEVT_SCROLL_PAGEDOWN, wxScrollEventHandler( CoordinateViewerFrame::OnZSlideScroll_r ), NULL, this );
	m_pZSlider_r->Connect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( CoordinateViewerFrame::OnZSlideScroll_r ), NULL, this );
	m_pZSlider_r->Connect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( CoordinateViewerFrame::OnZSlideScroll_r ), NULL, this );
	m_pZSlider_r->Connect( wxEVT_SCROLL_CHANGED, wxScrollEventHandler( CoordinateViewerFrame::OnZSlideScroll_r ), NULL, this );
	m_pZSlider_r->Connect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( CoordinateViewerFrame::OnZSlideRelease_r ), NULL, this );
	m_pZSlider_r->Connect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( CoordinateViewerFrame::OnZSlideDrag_r ), NULL, this );
	m_pZPosUpBtn_r->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CoordinateViewerFrame::OnZPosUp_r ), NULL, this );
	m_pZPosTxt_r->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( CoordinateViewerFrame::m_pZPosTxt_rOnKillFocus ), NULL, this );
	m_pZPosTxt_r->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( CoordinateViewerFrame::OnPosTxt_r ), NULL, this );
	m_pZPitchTxt_r->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( CoordinateViewerFrame::m_pZPitchTxt_rOnKillFocus ), NULL, this );
	m_pZPitchTxt_r->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( CoordinateViewerFrame::OnPitchTxt_r ), NULL, this );
	m_pWidthTxt_r->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( CoordinateViewerFrame::m_pWidthTxt_rOnKillFocus ), NULL, this );
	m_pWidthTxt_r->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( CoordinateViewerFrame::OnWidthTxt_r ), NULL, this );
	m_pDepthTxt_r->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( CoordinateViewerFrame::m_pDepthTxt_rOnKillFocus ), NULL, this );
	m_pDepthTxt_r->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( CoordinateViewerFrame::OnDepthTxt_r ), NULL, this );
	m_pHeightTxt_r->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( CoordinateViewerFrame::m_pHeightTxt_rOnKillFocus ), NULL, this );
	m_pHeightTxt_r->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( CoordinateViewerFrame::OnHeightTxt_r ), NULL, this );
	m_pOrientationVecXTxt_r->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( CoordinateViewerFrame::m_pOrientationVecXTxt_rOnKillFocus ), NULL, this );
	m_pOrientationVecXTxt_r->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( CoordinateViewerFrame::OnOrientationVectorXTxt_r ), NULL, this );
	m_pOrientationVecYTxt_r->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( CoordinateViewerFrame::m_pOrientationVecYTxt_rOnKillFocus ), NULL, this );
	m_pOrientationVecYTxt_r->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( CoordinateViewerFrame::OnOrientationVectorYTxt_r ), NULL, this );
	m_pOrientationVecZTxt_r->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( CoordinateViewerFrame::m_pOrientationVecZTxt_rOnKillFocus ), NULL, this );
	m_pOrientationVecZTxt_r->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( CoordinateViewerFrame::OnOrientationVectorZTxt_r ), NULL, this );
	m_pDirVecXTxt_r->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( CoordinateViewerFrame::m_pDirVecXTxt_rOnKillFocus ), NULL, this );
	m_pDirVecXTxt_r->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( CoordinateViewerFrame::OnDirVectorXTxt_r ), NULL, this );
	m_pDirVecYTxt_r->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( CoordinateViewerFrame::m_pDirVecYTxt_rOnKillFocus ), NULL, this );
	m_pDirVecYTxt_r->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( CoordinateViewerFrame::OnDirVectorYTxt_r ), NULL, this );
	m_pDirVecZTxt_r->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( CoordinateViewerFrame::m_pDirVecZTxt_rOnKillFocus ), NULL, this );
	m_pDirVecZTxt_r->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( CoordinateViewerFrame::OnDirVectorZTxt_r ), NULL, this );
	m_button_Add_r->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CoordinateViewerFrame::m_button_list_AddButtonClick_r ), NULL, this );
	m_button_Clear_r->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CoordinateViewerFrame::m_button_list_ClearButtonClick_r ), NULL, this );
	m_button_AllClear_r->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CoordinateViewerFrame::m_button_list_AllClearButtonClick_r ), NULL, this );
	m_pRectangularGrid_r->Connect( wxEVT_GRID_SELECT_CELL, wxGridEventHandler( CoordinateViewerFrame::m_pRectangularGrid_rOnGridSelectCell ), NULL, this );
	m_pXPosDownBtn_c->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CoordinateViewerFrame::OnXPosDown_c ), NULL, this );
	m_pXSlider_c->Connect( wxEVT_SCROLL_TOP, wxScrollEventHandler( CoordinateViewerFrame::OnXSlideScroll_c ), NULL, this );
	m_pXSlider_c->Connect( wxEVT_SCROLL_BOTTOM, wxScrollEventHandler( CoordinateViewerFrame::OnXSlideScroll_c ), NULL, this );
	m_pXSlider_c->Connect( wxEVT_SCROLL_LINEUP, wxScrollEventHandler( CoordinateViewerFrame::OnXSlideScroll_c ), NULL, this );
	m_pXSlider_c->Connect( wxEVT_SCROLL_LINEDOWN, wxScrollEventHandler( CoordinateViewerFrame::OnXSlideScroll_c ), NULL, this );
	m_pXSlider_c->Connect( wxEVT_SCROLL_PAGEUP, wxScrollEventHandler( CoordinateViewerFrame::OnXSlideScroll_c ), NULL, this );
	m_pXSlider_c->Connect( wxEVT_SCROLL_PAGEDOWN, wxScrollEventHandler( CoordinateViewerFrame::OnXSlideScroll_c ), NULL, this );
	m_pXSlider_c->Connect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( CoordinateViewerFrame::OnXSlideScroll_c ), NULL, this );
	m_pXSlider_c->Connect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( CoordinateViewerFrame::OnXSlideScroll_c ), NULL, this );
	m_pXSlider_c->Connect( wxEVT_SCROLL_CHANGED, wxScrollEventHandler( CoordinateViewerFrame::OnXSlideScroll_c ), NULL, this );
	m_pXSlider_c->Connect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( CoordinateViewerFrame::OnXSlideRelease_c ), NULL, this );
	m_pXSlider_c->Connect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( CoordinateViewerFrame::OnXSlideDrag_c ), NULL, this );
	m_pXPosUpBtn_c->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CoordinateViewerFrame::OnXPosUp_c ), NULL, this );
	m_pXPosTxt_c->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( CoordinateViewerFrame::m_pXPosTxt_cOnKillFocus ), NULL, this );
	m_pXPosTxt_c->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( CoordinateViewerFrame::OnPosTxt_c ), NULL, this );
	m_pXPitchTxt_c->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( CoordinateViewerFrame::m_pXPitchTxt_cOnKillFocus ), NULL, this );
	m_pXPitchTxt_c->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( CoordinateViewerFrame::OnPitchTxt_c ), NULL, this );
	m_pYPosDownBtn_c->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CoordinateViewerFrame::OnYPosDown_c ), NULL, this );
	m_pYSlider_c->Connect( wxEVT_SCROLL_TOP, wxScrollEventHandler( CoordinateViewerFrame::OnYSlideScroll_c ), NULL, this );
	m_pYSlider_c->Connect( wxEVT_SCROLL_BOTTOM, wxScrollEventHandler( CoordinateViewerFrame::OnYSlideScroll_c ), NULL, this );
	m_pYSlider_c->Connect( wxEVT_SCROLL_LINEUP, wxScrollEventHandler( CoordinateViewerFrame::OnYSlideScroll_c ), NULL, this );
	m_pYSlider_c->Connect( wxEVT_SCROLL_LINEDOWN, wxScrollEventHandler( CoordinateViewerFrame::OnYSlideScroll_c ), NULL, this );
	m_pYSlider_c->Connect( wxEVT_SCROLL_PAGEUP, wxScrollEventHandler( CoordinateViewerFrame::OnYSlideScroll_c ), NULL, this );
	m_pYSlider_c->Connect( wxEVT_SCROLL_PAGEDOWN, wxScrollEventHandler( CoordinateViewerFrame::OnYSlideScroll_c ), NULL, this );
	m_pYSlider_c->Connect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( CoordinateViewerFrame::OnYSlideScroll_c ), NULL, this );
	m_pYSlider_c->Connect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( CoordinateViewerFrame::OnYSlideScroll_c ), NULL, this );
	m_pYSlider_c->Connect( wxEVT_SCROLL_CHANGED, wxScrollEventHandler( CoordinateViewerFrame::OnYSlideScroll_c ), NULL, this );
	m_pYSlider_c->Connect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( CoordinateViewerFrame::OnYSlideRelease_c ), NULL, this );
	m_pYSlider_c->Connect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( CoordinateViewerFrame::OnYSlideDrag_c ), NULL, this );
	m_pYPosUpBtn_c->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CoordinateViewerFrame::OnYPosUp_c ), NULL, this );
	m_pYPosTxt_c->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( CoordinateViewerFrame::m_pYPosTxt_cOnKillFocus ), NULL, this );
	m_pYPosTxt_c->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( CoordinateViewerFrame::OnPosTxt_c ), NULL, this );
	m_pYPitchTxt_c->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( CoordinateViewerFrame::m_pYPitchTxt_cOnKillFocus ), NULL, this );
	m_pYPitchTxt_c->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( CoordinateViewerFrame::OnPitchTxt_c ), NULL, this );
	m_pZPosDownBtn_c->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CoordinateViewerFrame::OnZPosDown_c ), NULL, this );
	m_pZSlider_c->Connect( wxEVT_SCROLL_TOP, wxScrollEventHandler( CoordinateViewerFrame::OnZSlideScroll_c ), NULL, this );
	m_pZSlider_c->Connect( wxEVT_SCROLL_BOTTOM, wxScrollEventHandler( CoordinateViewerFrame::OnZSlideScroll_c ), NULL, this );
	m_pZSlider_c->Connect( wxEVT_SCROLL_LINEUP, wxScrollEventHandler( CoordinateViewerFrame::OnZSlideScroll_c ), NULL, this );
	m_pZSlider_c->Connect( wxEVT_SCROLL_LINEDOWN, wxScrollEventHandler( CoordinateViewerFrame::OnZSlideScroll_c ), NULL, this );
	m_pZSlider_c->Connect( wxEVT_SCROLL_PAGEUP, wxScrollEventHandler( CoordinateViewerFrame::OnZSlideScroll_c ), NULL, this );
	m_pZSlider_c->Connect( wxEVT_SCROLL_PAGEDOWN, wxScrollEventHandler( CoordinateViewerFrame::OnZSlideScroll_c ), NULL, this );
	m_pZSlider_c->Connect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( CoordinateViewerFrame::OnZSlideScroll_c ), NULL, this );
	m_pZSlider_c->Connect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( CoordinateViewerFrame::OnZSlideScroll_c ), NULL, this );
	m_pZSlider_c->Connect( wxEVT_SCROLL_CHANGED, wxScrollEventHandler( CoordinateViewerFrame::OnZSlideScroll_c ), NULL, this );
	m_pZSlider_c->Connect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( CoordinateViewerFrame::OnZSlideRelease_c ), NULL, this );
	m_pZSlider_c->Connect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( CoordinateViewerFrame::OnZSlideDrag_c ), NULL, this );
	m_pZPosUpBtn_c->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CoordinateViewerFrame::OnZPosUp_c ), NULL, this );
	m_pZPosTxt_c->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( CoordinateViewerFrame::m_pZPosTxt_cOnKillFocus ), NULL, this );
	m_pZPosTxt_c->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( CoordinateViewerFrame::OnPosTxt_c ), NULL, this );
	m_pZPitchTxt_c->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( CoordinateViewerFrame::m_pZPitchTxt_cOnKillFocus ), NULL, this );
	m_pZPitchTxt_c->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( CoordinateViewerFrame::OnPitchTxt_c ), NULL, this );
	m_pDepthTxt_c->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( CoordinateViewerFrame::m_pDepthTxt_cOnKillFocus ), NULL, this );
	m_pDepthTxt_c->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( CoordinateViewerFrame::OnDepthTxt_c ), NULL, this );
	m_pFanRadiusTxt_c->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( CoordinateViewerFrame::m_pFanRadiusTxt_cOnKillFocus ), NULL, this );
	m_pFanRadiusTxt_c->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( CoordinateViewerFrame::OnFanRadiusTxt_c ), NULL, this );
	m_pBossRadTxt_c->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( CoordinateViewerFrame::m_pBossRadTxt_cOnKillFocus ), NULL, this );
	m_pBossRadTxt_c->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( CoordinateViewerFrame::OnBossRadiusTxt_c ), NULL, this );
	m_pOrientationVecXTxt_c->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( CoordinateViewerFrame::m_pOrientationVecXTxt_cOnKillFocus ), NULL, this );
	m_pOrientationVecXTxt_c->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( CoordinateViewerFrame::OnOrientationVectorXTxt_c ), NULL, this );
	m_pOrientationVecYTxt_c->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( CoordinateViewerFrame::m_pOrientationVecYTxt_cOnKillFocus ), NULL, this );
	m_pOrientationVecYTxt_c->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( CoordinateViewerFrame::OnOrientationVectorYTxt_c ), NULL, this );
	m_pOrientationVecZTxt_c->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( CoordinateViewerFrame::m_pOrientationVecZTxt_cOnKillFocus ), NULL, this );
	m_pOrientationVecZTxt_c->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( CoordinateViewerFrame::OnOrientationVectorZTxt_c ), NULL, this );
	m_button_Add_c->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CoordinateViewerFrame::m_button_list_AddButtonClick_c ), NULL, this );
	m_button_Clear_c->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CoordinateViewerFrame::m_button_list_ClearButtonClick_c ), NULL, this );
	m_button_AllClear_c->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CoordinateViewerFrame::m_button_list_AllClearButtonClick_c ), NULL, this );
	m_pCylinderGrid_c->Connect( wxEVT_GRID_SELECT_CELL, wxGridEventHandler( CoordinateViewerFrame::m_pCylinderGrid_cOnGridSelectCell ), NULL, this );
	m_pCloseBtn->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CoordinateViewerFrame::m_pCloseBtnOnButtonClick ), NULL, this );
}

CoordinateViewerFrame::~CoordinateViewerFrame()
{
	// Disconnect Events
	this->Disconnect( wxEVT_ACTIVATE, wxActivateEventHandler( CoordinateViewerFrame::OnActivate ) );
	this->Disconnect( wxEVT_ACTIVATE_APP, wxActivateEventHandler( CoordinateViewerFrame::OnActivateApp ) );
	this->Disconnect( wxEVT_CLOSE_WINDOW, wxCloseEventHandler( CoordinateViewerFrame::OnClose ) );
	m_notebook4->Disconnect( wxEVT_COMMAND_NOTEBOOK_PAGE_CHANGED, wxNotebookEventHandler( CoordinateViewerFrame::m_notebookOnNotebookPageChanged ), NULL, this );
	m_notebook4->Disconnect( wxEVT_COMMAND_NOTEBOOK_PAGE_CHANGING, wxNotebookEventHandler( CoordinateViewerFrame::m_notebookOnNotebookPageChanging ), NULL, this );
	m_pXPosDownBtn_p->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CoordinateViewerFrame::OnXPosDown_p ), NULL, this );
	m_pXSlider_p->Disconnect( wxEVT_SCROLL_TOP, wxScrollEventHandler( CoordinateViewerFrame::OnXSlideScroll_p ), NULL, this );
	m_pXSlider_p->Disconnect( wxEVT_SCROLL_BOTTOM, wxScrollEventHandler( CoordinateViewerFrame::OnXSlideScroll_p ), NULL, this );
	m_pXSlider_p->Disconnect( wxEVT_SCROLL_LINEUP, wxScrollEventHandler( CoordinateViewerFrame::OnXSlideScroll_p ), NULL, this );
	m_pXSlider_p->Disconnect( wxEVT_SCROLL_LINEDOWN, wxScrollEventHandler( CoordinateViewerFrame::OnXSlideScroll_p ), NULL, this );
	m_pXSlider_p->Disconnect( wxEVT_SCROLL_PAGEUP, wxScrollEventHandler( CoordinateViewerFrame::OnXSlideScroll_p ), NULL, this );
	m_pXSlider_p->Disconnect( wxEVT_SCROLL_PAGEDOWN, wxScrollEventHandler( CoordinateViewerFrame::OnXSlideScroll_p ), NULL, this );
	m_pXSlider_p->Disconnect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( CoordinateViewerFrame::OnXSlideScroll_p ), NULL, this );
	m_pXSlider_p->Disconnect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( CoordinateViewerFrame::OnXSlideScroll_p ), NULL, this );
	m_pXSlider_p->Disconnect( wxEVT_SCROLL_CHANGED, wxScrollEventHandler( CoordinateViewerFrame::OnXSlideScroll_p ), NULL, this );
	m_pXSlider_p->Disconnect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( CoordinateViewerFrame::OnXSlideRelease_p ), NULL, this );
	m_pXSlider_p->Disconnect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( CoordinateViewerFrame::OnXSlideDrag_p ), NULL, this );
	m_pXPosUpBtn_p->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CoordinateViewerFrame::OnXPosUp_p ), NULL, this );
	m_pXPosTxt_p->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( CoordinateViewerFrame::m_pXPosTxt_pOnKillFocus ), NULL, this );
	m_pXPosTxt_p->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( CoordinateViewerFrame::OnPosTxt_p ), NULL, this );
	m_pXPitchTxt_p->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( CoordinateViewerFrame::m_pXPitchTxt_pOnKillFocus ), NULL, this );
	m_pXPitchTxt_p->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( CoordinateViewerFrame::OnPitchTxt_p ), NULL, this );
	m_pYPosDownBtn_p->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CoordinateViewerFrame::OnYPosDown_p ), NULL, this );
	m_pYSlider_p->Disconnect( wxEVT_SCROLL_TOP, wxScrollEventHandler( CoordinateViewerFrame::OnYSlideScroll_p ), NULL, this );
	m_pYSlider_p->Disconnect( wxEVT_SCROLL_BOTTOM, wxScrollEventHandler( CoordinateViewerFrame::OnYSlideScroll_p ), NULL, this );
	m_pYSlider_p->Disconnect( wxEVT_SCROLL_LINEUP, wxScrollEventHandler( CoordinateViewerFrame::OnYSlideScroll_p ), NULL, this );
	m_pYSlider_p->Disconnect( wxEVT_SCROLL_LINEDOWN, wxScrollEventHandler( CoordinateViewerFrame::OnYSlideScroll_p ), NULL, this );
	m_pYSlider_p->Disconnect( wxEVT_SCROLL_PAGEUP, wxScrollEventHandler( CoordinateViewerFrame::OnYSlideScroll_p ), NULL, this );
	m_pYSlider_p->Disconnect( wxEVT_SCROLL_PAGEDOWN, wxScrollEventHandler( CoordinateViewerFrame::OnYSlideScroll_p ), NULL, this );
	m_pYSlider_p->Disconnect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( CoordinateViewerFrame::OnYSlideScroll_p ), NULL, this );
	m_pYSlider_p->Disconnect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( CoordinateViewerFrame::OnYSlideScroll_p ), NULL, this );
	m_pYSlider_p->Disconnect( wxEVT_SCROLL_CHANGED, wxScrollEventHandler( CoordinateViewerFrame::OnYSlideScroll_p ), NULL, this );
	m_pYSlider_p->Disconnect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( CoordinateViewerFrame::OnYSlideRelease_p ), NULL, this );
	m_pYSlider_p->Disconnect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( CoordinateViewerFrame::OnYSlideDrag_p ), NULL, this );
	m_pYPosUpBtn_p->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CoordinateViewerFrame::OnYPosUp_p ), NULL, this );
	m_pYPosTxt_p->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( CoordinateViewerFrame::m_pYPosTxt_pOnKillFocus ), NULL, this );
	m_pYPosTxt_p->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( CoordinateViewerFrame::OnPosTxt_p ), NULL, this );
	m_pYPitchTxt_p->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( CoordinateViewerFrame::m_pYPitchTxt_pOnKillFocus ), NULL, this );
	m_pYPitchTxt_p->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( CoordinateViewerFrame::OnPitchTxt_p ), NULL, this );
	m_pZPosDownBtn_p->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CoordinateViewerFrame::OnZPosDown_p ), NULL, this );
	m_pZSlider_p->Disconnect( wxEVT_SCROLL_TOP, wxScrollEventHandler( CoordinateViewerFrame::OnZSlideScroll_p ), NULL, this );
	m_pZSlider_p->Disconnect( wxEVT_SCROLL_BOTTOM, wxScrollEventHandler( CoordinateViewerFrame::OnZSlideScroll_p ), NULL, this );
	m_pZSlider_p->Disconnect( wxEVT_SCROLL_LINEUP, wxScrollEventHandler( CoordinateViewerFrame::OnZSlideScroll_p ), NULL, this );
	m_pZSlider_p->Disconnect( wxEVT_SCROLL_LINEDOWN, wxScrollEventHandler( CoordinateViewerFrame::OnZSlideScroll_p ), NULL, this );
	m_pZSlider_p->Disconnect( wxEVT_SCROLL_PAGEUP, wxScrollEventHandler( CoordinateViewerFrame::OnZSlideScroll_p ), NULL, this );
	m_pZSlider_p->Disconnect( wxEVT_SCROLL_PAGEDOWN, wxScrollEventHandler( CoordinateViewerFrame::OnZSlideScroll_p ), NULL, this );
	m_pZSlider_p->Disconnect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( CoordinateViewerFrame::OnZSlideScroll_p ), NULL, this );
	m_pZSlider_p->Disconnect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( CoordinateViewerFrame::OnZSlideScroll_p ), NULL, this );
	m_pZSlider_p->Disconnect( wxEVT_SCROLL_CHANGED, wxScrollEventHandler( CoordinateViewerFrame::OnZSlideScroll_p ), NULL, this );
	m_pZSlider_p->Disconnect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( CoordinateViewerFrame::OnZSlideRelease_p ), NULL, this );
	m_pZSlider_p->Disconnect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( CoordinateViewerFrame::OnZSlideDrag_p ), NULL, this );
	m_pZPosUpBtn_p->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CoordinateViewerFrame::OnZPosUp_p ), NULL, this );
	m_pZPosTxt_p->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( CoordinateViewerFrame::m_pZPosTxt_pOnKillFocus ), NULL, this );
	m_pZPosTxt_p->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( CoordinateViewerFrame::OnPosTxt_p ), NULL, this );
	m_pZPitchTxt_p->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( CoordinateViewerFrame::m_pZPitchTxt_pOnKillFocus ), NULL, this );
	m_pZPitchTxt_p->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( CoordinateViewerFrame::OnPitchTxt_p ), NULL, this );
	m_pSizeTxt_p->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( CoordinateViewerFrame::m_pSizeTxt_pOnKillFocus ), NULL, this );
	m_pSizeTxt_p->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( CoordinateViewerFrame::OnSizeTxt_p ), NULL, this );
	m_button_Add_p->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CoordinateViewerFrame::m_button_list_AddButtonClick_p ), NULL, this );
	m_button_Clear_p->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CoordinateViewerFrame::m_button_list_ClearButtonClick_p ), NULL, this );
	m_button_AllClear_p->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CoordinateViewerFrame::m_button_list_AllClearButtonClick_p ), NULL, this );
	m_pPointGrid_p->Disconnect( wxEVT_GRID_SELECT_CELL, wxGridEventHandler( CoordinateViewerFrame::m_pPointGrid_pOnGridSelectCell ), NULL, this );
	m_pXPosDownBtn_r->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CoordinateViewerFrame::OnXPosDown_r ), NULL, this );
	m_pXSlider_r->Disconnect( wxEVT_SCROLL_TOP, wxScrollEventHandler( CoordinateViewerFrame::OnXSlideScroll_r ), NULL, this );
	m_pXSlider_r->Disconnect( wxEVT_SCROLL_BOTTOM, wxScrollEventHandler( CoordinateViewerFrame::OnXSlideScroll_r ), NULL, this );
	m_pXSlider_r->Disconnect( wxEVT_SCROLL_LINEUP, wxScrollEventHandler( CoordinateViewerFrame::OnXSlideScroll_r ), NULL, this );
	m_pXSlider_r->Disconnect( wxEVT_SCROLL_LINEDOWN, wxScrollEventHandler( CoordinateViewerFrame::OnXSlideScroll_r ), NULL, this );
	m_pXSlider_r->Disconnect( wxEVT_SCROLL_PAGEUP, wxScrollEventHandler( CoordinateViewerFrame::OnXSlideScroll_r ), NULL, this );
	m_pXSlider_r->Disconnect( wxEVT_SCROLL_PAGEDOWN, wxScrollEventHandler( CoordinateViewerFrame::OnXSlideScroll_r ), NULL, this );
	m_pXSlider_r->Disconnect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( CoordinateViewerFrame::OnXSlideScroll_r ), NULL, this );
	m_pXSlider_r->Disconnect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( CoordinateViewerFrame::OnXSlideScroll_r ), NULL, this );
	m_pXSlider_r->Disconnect( wxEVT_SCROLL_CHANGED, wxScrollEventHandler( CoordinateViewerFrame::OnXSlideScroll_r ), NULL, this );
	m_pXSlider_r->Disconnect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( CoordinateViewerFrame::OnXSlideRelease_r ), NULL, this );
	m_pXSlider_r->Disconnect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( CoordinateViewerFrame::OnXSlideDrag_r ), NULL, this );
	m_pXPosUpBtn_r->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CoordinateViewerFrame::OnXPosUp_r ), NULL, this );
	m_pXPosTxt_r->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( CoordinateViewerFrame::m_pXPosTxt_rOnKillFocus ), NULL, this );
	m_pXPosTxt_r->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( CoordinateViewerFrame::OnPosTxt_r ), NULL, this );
	m_pXPitchTxt_r->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( CoordinateViewerFrame::m_pXPitchTxt_rOnKillFocus ), NULL, this );
	m_pXPitchTxt_r->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( CoordinateViewerFrame::OnPitchTxt_r ), NULL, this );
	m_pYPosDownBtn_r->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CoordinateViewerFrame::OnYPosDown_r ), NULL, this );
	m_pYSlider_r->Disconnect( wxEVT_SCROLL_TOP, wxScrollEventHandler( CoordinateViewerFrame::OnYSlideScroll_r ), NULL, this );
	m_pYSlider_r->Disconnect( wxEVT_SCROLL_BOTTOM, wxScrollEventHandler( CoordinateViewerFrame::OnYSlideScroll_r ), NULL, this );
	m_pYSlider_r->Disconnect( wxEVT_SCROLL_LINEUP, wxScrollEventHandler( CoordinateViewerFrame::OnYSlideScroll_r ), NULL, this );
	m_pYSlider_r->Disconnect( wxEVT_SCROLL_LINEDOWN, wxScrollEventHandler( CoordinateViewerFrame::OnYSlideScroll_r ), NULL, this );
	m_pYSlider_r->Disconnect( wxEVT_SCROLL_PAGEUP, wxScrollEventHandler( CoordinateViewerFrame::OnYSlideScroll_r ), NULL, this );
	m_pYSlider_r->Disconnect( wxEVT_SCROLL_PAGEDOWN, wxScrollEventHandler( CoordinateViewerFrame::OnYSlideScroll_r ), NULL, this );
	m_pYSlider_r->Disconnect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( CoordinateViewerFrame::OnYSlideScroll_r ), NULL, this );
	m_pYSlider_r->Disconnect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( CoordinateViewerFrame::OnYSlideScroll_r ), NULL, this );
	m_pYSlider_r->Disconnect( wxEVT_SCROLL_CHANGED, wxScrollEventHandler( CoordinateViewerFrame::OnYSlideScroll_r ), NULL, this );
	m_pYSlider_r->Disconnect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( CoordinateViewerFrame::OnYSlideRelease_r ), NULL, this );
	m_pYSlider_r->Disconnect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( CoordinateViewerFrame::OnYSlideDrag_r ), NULL, this );
	m_pYPosUpBtn_r->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CoordinateViewerFrame::OnYPosUp_r ), NULL, this );
	m_pYPosTxt_r->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( CoordinateViewerFrame::m_pYPosTxt_rOnKillFocus ), NULL, this );
	m_pYPosTxt_r->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( CoordinateViewerFrame::OnPosTxt_r ), NULL, this );
	m_pYPitchTxt_r->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( CoordinateViewerFrame::m_pYPitchTxt_rOnKillFocus ), NULL, this );
	m_pYPitchTxt_r->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( CoordinateViewerFrame::OnPitchTxt_r ), NULL, this );
	m_pZPosDownBtn_r->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CoordinateViewerFrame::OnZPosDown_r ), NULL, this );
	m_pZSlider_r->Disconnect( wxEVT_SCROLL_TOP, wxScrollEventHandler( CoordinateViewerFrame::OnZSlideScroll_r ), NULL, this );
	m_pZSlider_r->Disconnect( wxEVT_SCROLL_BOTTOM, wxScrollEventHandler( CoordinateViewerFrame::OnZSlideScroll_r ), NULL, this );
	m_pZSlider_r->Disconnect( wxEVT_SCROLL_LINEUP, wxScrollEventHandler( CoordinateViewerFrame::OnZSlideScroll_r ), NULL, this );
	m_pZSlider_r->Disconnect( wxEVT_SCROLL_LINEDOWN, wxScrollEventHandler( CoordinateViewerFrame::OnZSlideScroll_r ), NULL, this );
	m_pZSlider_r->Disconnect( wxEVT_SCROLL_PAGEUP, wxScrollEventHandler( CoordinateViewerFrame::OnZSlideScroll_r ), NULL, this );
	m_pZSlider_r->Disconnect( wxEVT_SCROLL_PAGEDOWN, wxScrollEventHandler( CoordinateViewerFrame::OnZSlideScroll_r ), NULL, this );
	m_pZSlider_r->Disconnect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( CoordinateViewerFrame::OnZSlideScroll_r ), NULL, this );
	m_pZSlider_r->Disconnect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( CoordinateViewerFrame::OnZSlideScroll_r ), NULL, this );
	m_pZSlider_r->Disconnect( wxEVT_SCROLL_CHANGED, wxScrollEventHandler( CoordinateViewerFrame::OnZSlideScroll_r ), NULL, this );
	m_pZSlider_r->Disconnect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( CoordinateViewerFrame::OnZSlideRelease_r ), NULL, this );
	m_pZSlider_r->Disconnect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( CoordinateViewerFrame::OnZSlideDrag_r ), NULL, this );
	m_pZPosUpBtn_r->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CoordinateViewerFrame::OnZPosUp_r ), NULL, this );
	m_pZPosTxt_r->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( CoordinateViewerFrame::m_pZPosTxt_rOnKillFocus ), NULL, this );
	m_pZPosTxt_r->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( CoordinateViewerFrame::OnPosTxt_r ), NULL, this );
	m_pZPitchTxt_r->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( CoordinateViewerFrame::m_pZPitchTxt_rOnKillFocus ), NULL, this );
	m_pZPitchTxt_r->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( CoordinateViewerFrame::OnPitchTxt_r ), NULL, this );
	m_pWidthTxt_r->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( CoordinateViewerFrame::m_pWidthTxt_rOnKillFocus ), NULL, this );
	m_pWidthTxt_r->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( CoordinateViewerFrame::OnWidthTxt_r ), NULL, this );
	m_pDepthTxt_r->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( CoordinateViewerFrame::m_pDepthTxt_rOnKillFocus ), NULL, this );
	m_pDepthTxt_r->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( CoordinateViewerFrame::OnDepthTxt_r ), NULL, this );
	m_pHeightTxt_r->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( CoordinateViewerFrame::m_pHeightTxt_rOnKillFocus ), NULL, this );
	m_pHeightTxt_r->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( CoordinateViewerFrame::OnHeightTxt_r ), NULL, this );
	m_pOrientationVecXTxt_r->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( CoordinateViewerFrame::m_pOrientationVecXTxt_rOnKillFocus ), NULL, this );
	m_pOrientationVecXTxt_r->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( CoordinateViewerFrame::OnOrientationVectorXTxt_r ), NULL, this );
	m_pOrientationVecYTxt_r->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( CoordinateViewerFrame::m_pOrientationVecYTxt_rOnKillFocus ), NULL, this );
	m_pOrientationVecYTxt_r->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( CoordinateViewerFrame::OnOrientationVectorYTxt_r ), NULL, this );
	m_pOrientationVecZTxt_r->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( CoordinateViewerFrame::m_pOrientationVecZTxt_rOnKillFocus ), NULL, this );
	m_pOrientationVecZTxt_r->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( CoordinateViewerFrame::OnOrientationVectorZTxt_r ), NULL, this );
	m_pDirVecXTxt_r->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( CoordinateViewerFrame::m_pDirVecXTxt_rOnKillFocus ), NULL, this );
	m_pDirVecXTxt_r->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( CoordinateViewerFrame::OnDirVectorXTxt_r ), NULL, this );
	m_pDirVecYTxt_r->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( CoordinateViewerFrame::m_pDirVecYTxt_rOnKillFocus ), NULL, this );
	m_pDirVecYTxt_r->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( CoordinateViewerFrame::OnDirVectorYTxt_r ), NULL, this );
	m_pDirVecZTxt_r->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( CoordinateViewerFrame::m_pDirVecZTxt_rOnKillFocus ), NULL, this );
	m_pDirVecZTxt_r->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( CoordinateViewerFrame::OnDirVectorZTxt_r ), NULL, this );
	m_button_Add_r->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CoordinateViewerFrame::m_button_list_AddButtonClick_r ), NULL, this );
	m_button_Clear_r->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CoordinateViewerFrame::m_button_list_ClearButtonClick_r ), NULL, this );
	m_button_AllClear_r->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CoordinateViewerFrame::m_button_list_AllClearButtonClick_r ), NULL, this );
	m_pRectangularGrid_r->Disconnect( wxEVT_GRID_SELECT_CELL, wxGridEventHandler( CoordinateViewerFrame::m_pRectangularGrid_rOnGridSelectCell ), NULL, this );
	m_pXPosDownBtn_c->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CoordinateViewerFrame::OnXPosDown_c ), NULL, this );
	m_pXSlider_c->Disconnect( wxEVT_SCROLL_TOP, wxScrollEventHandler( CoordinateViewerFrame::OnXSlideScroll_c ), NULL, this );
	m_pXSlider_c->Disconnect( wxEVT_SCROLL_BOTTOM, wxScrollEventHandler( CoordinateViewerFrame::OnXSlideScroll_c ), NULL, this );
	m_pXSlider_c->Disconnect( wxEVT_SCROLL_LINEUP, wxScrollEventHandler( CoordinateViewerFrame::OnXSlideScroll_c ), NULL, this );
	m_pXSlider_c->Disconnect( wxEVT_SCROLL_LINEDOWN, wxScrollEventHandler( CoordinateViewerFrame::OnXSlideScroll_c ), NULL, this );
	m_pXSlider_c->Disconnect( wxEVT_SCROLL_PAGEUP, wxScrollEventHandler( CoordinateViewerFrame::OnXSlideScroll_c ), NULL, this );
	m_pXSlider_c->Disconnect( wxEVT_SCROLL_PAGEDOWN, wxScrollEventHandler( CoordinateViewerFrame::OnXSlideScroll_c ), NULL, this );
	m_pXSlider_c->Disconnect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( CoordinateViewerFrame::OnXSlideScroll_c ), NULL, this );
	m_pXSlider_c->Disconnect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( CoordinateViewerFrame::OnXSlideScroll_c ), NULL, this );
	m_pXSlider_c->Disconnect( wxEVT_SCROLL_CHANGED, wxScrollEventHandler( CoordinateViewerFrame::OnXSlideScroll_c ), NULL, this );
	m_pXSlider_c->Disconnect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( CoordinateViewerFrame::OnXSlideRelease_c ), NULL, this );
	m_pXSlider_c->Disconnect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( CoordinateViewerFrame::OnXSlideDrag_c ), NULL, this );
	m_pXPosUpBtn_c->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CoordinateViewerFrame::OnXPosUp_c ), NULL, this );
	m_pXPosTxt_c->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( CoordinateViewerFrame::m_pXPosTxt_cOnKillFocus ), NULL, this );
	m_pXPosTxt_c->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( CoordinateViewerFrame::OnPosTxt_c ), NULL, this );
	m_pXPitchTxt_c->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( CoordinateViewerFrame::m_pXPitchTxt_cOnKillFocus ), NULL, this );
	m_pXPitchTxt_c->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( CoordinateViewerFrame::OnPitchTxt_c ), NULL, this );
	m_pYPosDownBtn_c->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CoordinateViewerFrame::OnYPosDown_c ), NULL, this );
	m_pYSlider_c->Disconnect( wxEVT_SCROLL_TOP, wxScrollEventHandler( CoordinateViewerFrame::OnYSlideScroll_c ), NULL, this );
	m_pYSlider_c->Disconnect( wxEVT_SCROLL_BOTTOM, wxScrollEventHandler( CoordinateViewerFrame::OnYSlideScroll_c ), NULL, this );
	m_pYSlider_c->Disconnect( wxEVT_SCROLL_LINEUP, wxScrollEventHandler( CoordinateViewerFrame::OnYSlideScroll_c ), NULL, this );
	m_pYSlider_c->Disconnect( wxEVT_SCROLL_LINEDOWN, wxScrollEventHandler( CoordinateViewerFrame::OnYSlideScroll_c ), NULL, this );
	m_pYSlider_c->Disconnect( wxEVT_SCROLL_PAGEUP, wxScrollEventHandler( CoordinateViewerFrame::OnYSlideScroll_c ), NULL, this );
	m_pYSlider_c->Disconnect( wxEVT_SCROLL_PAGEDOWN, wxScrollEventHandler( CoordinateViewerFrame::OnYSlideScroll_c ), NULL, this );
	m_pYSlider_c->Disconnect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( CoordinateViewerFrame::OnYSlideScroll_c ), NULL, this );
	m_pYSlider_c->Disconnect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( CoordinateViewerFrame::OnYSlideScroll_c ), NULL, this );
	m_pYSlider_c->Disconnect( wxEVT_SCROLL_CHANGED, wxScrollEventHandler( CoordinateViewerFrame::OnYSlideScroll_c ), NULL, this );
	m_pYSlider_c->Disconnect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( CoordinateViewerFrame::OnYSlideRelease_c ), NULL, this );
	m_pYSlider_c->Disconnect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( CoordinateViewerFrame::OnYSlideDrag_c ), NULL, this );
	m_pYPosUpBtn_c->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CoordinateViewerFrame::OnYPosUp_c ), NULL, this );
	m_pYPosTxt_c->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( CoordinateViewerFrame::m_pYPosTxt_cOnKillFocus ), NULL, this );
	m_pYPosTxt_c->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( CoordinateViewerFrame::OnPosTxt_c ), NULL, this );
	m_pYPitchTxt_c->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( CoordinateViewerFrame::m_pYPitchTxt_cOnKillFocus ), NULL, this );
	m_pYPitchTxt_c->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( CoordinateViewerFrame::OnPitchTxt_c ), NULL, this );
	m_pZPosDownBtn_c->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CoordinateViewerFrame::OnZPosDown_c ), NULL, this );
	m_pZSlider_c->Disconnect( wxEVT_SCROLL_TOP, wxScrollEventHandler( CoordinateViewerFrame::OnZSlideScroll_c ), NULL, this );
	m_pZSlider_c->Disconnect( wxEVT_SCROLL_BOTTOM, wxScrollEventHandler( CoordinateViewerFrame::OnZSlideScroll_c ), NULL, this );
	m_pZSlider_c->Disconnect( wxEVT_SCROLL_LINEUP, wxScrollEventHandler( CoordinateViewerFrame::OnZSlideScroll_c ), NULL, this );
	m_pZSlider_c->Disconnect( wxEVT_SCROLL_LINEDOWN, wxScrollEventHandler( CoordinateViewerFrame::OnZSlideScroll_c ), NULL, this );
	m_pZSlider_c->Disconnect( wxEVT_SCROLL_PAGEUP, wxScrollEventHandler( CoordinateViewerFrame::OnZSlideScroll_c ), NULL, this );
	m_pZSlider_c->Disconnect( wxEVT_SCROLL_PAGEDOWN, wxScrollEventHandler( CoordinateViewerFrame::OnZSlideScroll_c ), NULL, this );
	m_pZSlider_c->Disconnect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( CoordinateViewerFrame::OnZSlideScroll_c ), NULL, this );
	m_pZSlider_c->Disconnect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( CoordinateViewerFrame::OnZSlideScroll_c ), NULL, this );
	m_pZSlider_c->Disconnect( wxEVT_SCROLL_CHANGED, wxScrollEventHandler( CoordinateViewerFrame::OnZSlideScroll_c ), NULL, this );
	m_pZSlider_c->Disconnect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( CoordinateViewerFrame::OnZSlideRelease_c ), NULL, this );
	m_pZSlider_c->Disconnect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( CoordinateViewerFrame::OnZSlideDrag_c ), NULL, this );
	m_pZPosUpBtn_c->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CoordinateViewerFrame::OnZPosUp_c ), NULL, this );
	m_pZPosTxt_c->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( CoordinateViewerFrame::m_pZPosTxt_cOnKillFocus ), NULL, this );
	m_pZPosTxt_c->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( CoordinateViewerFrame::OnPosTxt_c ), NULL, this );
	m_pZPitchTxt_c->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( CoordinateViewerFrame::m_pZPitchTxt_cOnKillFocus ), NULL, this );
	m_pZPitchTxt_c->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( CoordinateViewerFrame::OnPitchTxt_c ), NULL, this );
	m_pDepthTxt_c->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( CoordinateViewerFrame::m_pDepthTxt_cOnKillFocus ), NULL, this );
	m_pDepthTxt_c->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( CoordinateViewerFrame::OnDepthTxt_c ), NULL, this );
	m_pFanRadiusTxt_c->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( CoordinateViewerFrame::m_pFanRadiusTxt_cOnKillFocus ), NULL, this );
	m_pFanRadiusTxt_c->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( CoordinateViewerFrame::OnFanRadiusTxt_c ), NULL, this );
	m_pBossRadTxt_c->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( CoordinateViewerFrame::m_pBossRadTxt_cOnKillFocus ), NULL, this );
	m_pBossRadTxt_c->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( CoordinateViewerFrame::OnBossRadiusTxt_c ), NULL, this );
	m_pOrientationVecXTxt_c->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( CoordinateViewerFrame::m_pOrientationVecXTxt_cOnKillFocus ), NULL, this );
	m_pOrientationVecXTxt_c->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( CoordinateViewerFrame::OnOrientationVectorXTxt_c ), NULL, this );
	m_pOrientationVecYTxt_c->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( CoordinateViewerFrame::m_pOrientationVecYTxt_cOnKillFocus ), NULL, this );
	m_pOrientationVecYTxt_c->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( CoordinateViewerFrame::OnOrientationVectorYTxt_c ), NULL, this );
	m_pOrientationVecZTxt_c->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( CoordinateViewerFrame::m_pOrientationVecZTxt_cOnKillFocus ), NULL, this );
	m_pOrientationVecZTxt_c->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( CoordinateViewerFrame::OnOrientationVectorZTxt_c ), NULL, this );
	m_button_Add_c->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CoordinateViewerFrame::m_button_list_AddButtonClick_c ), NULL, this );
	m_button_Clear_c->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CoordinateViewerFrame::m_button_list_ClearButtonClick_c ), NULL, this );
	m_button_AllClear_c->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CoordinateViewerFrame::m_button_list_AllClearButtonClick_c ), NULL, this );
	m_pCylinderGrid_c->Disconnect( wxEVT_GRID_SELECT_CELL, wxGridEventHandler( CoordinateViewerFrame::m_pCylinderGrid_cOnGridSelectCell ), NULL, this );
	m_pCloseBtn->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CoordinateViewerFrame::m_pCloseBtnOnButtonClick ), NULL, this );
	
}

FreeRotateDlgBase::FreeRotateDlgBase( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );
	this->SetForegroundColour( wxColour( 255, 255, 255 ) );
	this->SetBackgroundColour( wxColour( 44, 44, 44 ) );
	
	wxBoxSizer* bSizer24;
	bSizer24 = new wxBoxSizer( wxVERTICAL );
	
	wxBoxSizer* bSizer147;
	bSizer147 = new wxBoxSizer( wxVERTICAL );
	
	m_staticText302 = new wxStaticText( this, wxID_ANY, wxT("Arbitrary Rotate"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText302->Wrap( -1 );
	m_staticText302->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), 70, 90, 92, false, wxEmptyString ) );
	m_staticText302->SetForegroundColour( wxColour( 209, 76, 16 ) );
	
	bSizer147->Add( m_staticText302, 0, wxALL, 5 );
	
	m_panel41 = new wxPanel( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	m_panel41->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	wxBoxSizer* bSizer175;
	bSizer175 = new wxBoxSizer( wxVERTICAL );
	
	wxBoxSizer* bSizer146;
	bSizer146 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText252 = new wxStaticText( m_panel41, wxID_ANY, wxT("rotate degree"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText252->Wrap( -1 );
	m_staticText252->SetForegroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_MENU ) );
	
	bSizer146->Add( m_staticText252, 0, wxALL, 5 );
	
	m_rotateDegreeTxt = new wxTextCtrl( m_panel41, wxID_ANY, wxT("+45"), wxDefaultPosition, wxSize( 40,-1 ), wxTE_PROCESS_ENTER|wxSIMPLE_BORDER );
	m_rotateDegreeTxt->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_rotateDegreeTxt->SetBackgroundColour( wxColour( 0, 0, 0 ) );
	
	bSizer146->Add( m_rotateDegreeTxt, 0, wxALL, 5 );
	
	m_pFreeRotateBtn = new wxButton( m_panel41, wxID_ANY, wxT("Rotate"), wxDefaultPosition, wxDefaultSize, 0|wxSIMPLE_BORDER );
	m_pFreeRotateBtn->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pFreeRotateBtn->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	bSizer146->Add( m_pFreeRotateBtn, 0, wxALL, 5 );
	
	bSizer175->Add( bSizer146, 0, wxEXPAND|wxLEFT, 5 );
	
	m_panel41->SetSizer( bSizer175 );
	m_panel41->Layout();
	bSizer175->Fit( m_panel41 );
	bSizer147->Add( m_panel41, 0, wxALL|wxEXPAND, 5 );
	
	m_staticText301 = new wxStaticText( this, wxID_ANY, wxT("Static Rotate"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText301->Wrap( -1 );
	m_staticText301->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), 70, 90, 92, false, wxEmptyString ) );
	m_staticText301->SetForegroundColour( wxColour( 209, 76, 16 ) );
	
	bSizer147->Add( m_staticText301, 0, wxALL, 5 );
	
	m_panel411 = new wxPanel( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	m_panel411->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	wxBoxSizer* bSizer1751;
	bSizer1751 = new wxBoxSizer( wxVERTICAL );
	
	wxBoxSizer* bSizer1461;
	bSizer1461 = new wxBoxSizer( wxHORIZONTAL );
	
	
	bSizer1461->Add( 0, 0, 1, 0, 5 );
	
	m_pPlus90RotateBtn = new wxButton( m_panel411, wxID_ANY, wxT("+ 90"), wxDefaultPosition, wxDefaultSize, 0|wxSIMPLE_BORDER );
	m_pPlus90RotateBtn->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pPlus90RotateBtn->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	bSizer1461->Add( m_pPlus90RotateBtn, 0, wxALL, 5 );
	
	m_pMinus90RotateBtn = new wxButton( m_panel411, wxID_ANY, wxT("- 90"), wxDefaultPosition, wxDefaultSize, 0|wxSIMPLE_BORDER );
	m_pMinus90RotateBtn->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_pMinus90RotateBtn->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	bSizer1461->Add( m_pMinus90RotateBtn, 0, wxALL, 5 );
	
	bSizer1751->Add( bSizer1461, 0, wxEXPAND, 5 );
	
	m_panel411->SetSizer( bSizer1751 );
	m_panel411->Layout();
	bSizer1751->Fit( m_panel411 );
	bSizer147->Add( m_panel411, 0, wxALL|wxEXPAND, 5 );
	
	wxBoxSizer* bSizer135;
	bSizer135 = new wxBoxSizer( wxHORIZONTAL );
	
	
	bSizer135->Add( 0, 0, 1, wxEXPAND, 5 );
	
	m_pCloseBtn1 = new wxButton( this, wxID_ANY, wxT("Close"), wxDefaultPosition, wxDefaultSize, 0 );
	m_pCloseBtn1->SetForegroundColour( wxColour( 44, 44, 44 ) );
	
	bSizer135->Add( m_pCloseBtn1, 0, wxALL, 5 );
	
	bSizer147->Add( bSizer135, 0, wxALIGN_BOTTOM|wxEXPAND, 5 );
	
	bSizer24->Add( bSizer147, 0, wxFIXED_MINSIZE, 5 );
	
	this->SetSizer( bSizer24 );
	this->Layout();
	bSizer24->Fit( this );
	
	this->Centre( wxVERTICAL );
	
	// Connect Events
	this->Connect( wxEVT_CLOSE_WINDOW, wxCloseEventHandler( FreeRotateDlgBase::FreerotateDlgBaseOnClose ) );
	m_rotateDegreeTxt->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( FreeRotateDlgBase::OnDvdManuTxtOnKillFocus ), NULL, this );
	m_rotateDegreeTxt->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( FreeRotateDlgBase::OnDvdManuTxt ), NULL, this );
	m_pFreeRotateBtn->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( FreeRotateDlgBase::OnFreeRotateBtn ), NULL, this );
	m_pPlus90RotateBtn->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( FreeRotateDlgBase::OnPlus90RotateBCBtn ), NULL, this );
	m_pMinus90RotateBtn->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( FreeRotateDlgBase::OnMinus90RotateBtn ), NULL, this );
	m_pCloseBtn1->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( FreeRotateDlgBase::OnCloseBtn ), NULL, this );
}

FreeRotateDlgBase::~FreeRotateDlgBase()
{
	// Disconnect Events
	this->Disconnect( wxEVT_CLOSE_WINDOW, wxCloseEventHandler( FreeRotateDlgBase::FreerotateDlgBaseOnClose ) );
	m_rotateDegreeTxt->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( FreeRotateDlgBase::OnDvdManuTxtOnKillFocus ), NULL, this );
	m_rotateDegreeTxt->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( FreeRotateDlgBase::OnDvdManuTxt ), NULL, this );
	m_pFreeRotateBtn->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( FreeRotateDlgBase::OnFreeRotateBtn ), NULL, this );
	m_pPlus90RotateBtn->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( FreeRotateDlgBase::OnPlus90RotateBCBtn ), NULL, this );
	m_pMinus90RotateBtn->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( FreeRotateDlgBase::OnMinus90RotateBtn ), NULL, this );
	m_pCloseBtn1->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( FreeRotateDlgBase::OnCloseBtn ), NULL, this );
	
}

ExportSTLDlgBase::ExportSTLDlgBase( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );
	this->SetForegroundColour( wxColour( 255, 255, 255 ) );
	this->SetBackgroundColour( wxColour( 44, 44, 44 ) );
	
	wxBoxSizer* bSizer24;
	bSizer24 = new wxBoxSizer( wxVERTICAL );
	
	wxBoxSizer* bSizer147;
	bSizer147 = new wxBoxSizer( wxVERTICAL );
	
	m_staticText302 = new wxStaticText( this, wxID_ANY, wxT("STL Format"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText302->Wrap( -1 );
	m_staticText302->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), 70, 90, 92, false, wxEmptyString ) );
	m_staticText302->SetForegroundColour( wxColour( 209, 76, 16 ) );
	
	bSizer147->Add( m_staticText302, 0, wxALL, 5 );
	
	m_panel411 = new wxPanel( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	m_panel411->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	wxBoxSizer* bSizer1751;
	bSizer1751 = new wxBoxSizer( wxVERTICAL );
	
	wxGridSizer* gSizer5;
	gSizer5 = new wxGridSizer( 1, 2, 0, 0 );
	
	wxBoxSizer* bSizer200;
	bSizer200 = new wxBoxSizer( wxHORIZONTAL );
	
	m_exportSTLTypeAsciiRadioBtn = new wxRadioButton( m_panel411, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_exportSTLTypeAsciiRadioBtn->SetValue( true ); 
	bSizer200->Add( m_exportSTLTypeAsciiRadioBtn, 0, wxALL, 5 );
	
	m_staticText1 = new wxStaticText( m_panel411, wxID_ANY, wxT("Ascii"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText1->Wrap( -1 );
	m_staticText1->SetForegroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_MENU ) );
	
	bSizer200->Add( m_staticText1, 0, wxALL, 5 );
	
	gSizer5->Add( bSizer200, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer201;
	bSizer201 = new wxBoxSizer( wxHORIZONTAL );
	
	m_exportSTLTypeBinaryRadioBtn = new wxRadioButton( m_panel411, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizer201->Add( m_exportSTLTypeBinaryRadioBtn, 0, wxALL, 5 );
	
	m_staticText2 = new wxStaticText( m_panel411, wxID_ANY, wxT("Binary"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText2->Wrap( -1 );
	m_staticText2->SetForegroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_MENU ) );
	
	bSizer201->Add( m_staticText2, 0, wxALL, 5 );
	
	gSizer5->Add( bSizer201, 1, wxEXPAND, 5 );
	
	bSizer1751->Add( gSizer5, 1, wxEXPAND|wxLEFT, 5 );
	
	m_panel411->SetSizer( bSizer1751 );
	m_panel411->Layout();
	bSizer1751->Fit( m_panel411 );
	bSizer147->Add( m_panel411, 0, wxEXPAND | wxALL, 5 );
	
	m_staticText3021 = new wxStaticText( this, wxID_ANY, wxT("Target"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText3021->Wrap( -1 );
	m_staticText3021->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), 70, 90, 92, false, wxEmptyString ) );
	m_staticText3021->SetForegroundColour( wxColour( 209, 76, 16 ) );
	
	bSizer147->Add( m_staticText3021, 0, wxALL, 5 );
	
	m_panel41 = new wxPanel( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	m_panel41->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	wxBoxSizer* bSizer175;
	bSizer175 = new wxBoxSizer( wxVERTICAL );
	
	wxGridSizer* gSizer6;
	gSizer6 = new wxGridSizer( 1, 2, 0, 0 );
	
	wxBoxSizer* bSizer202;
	bSizer202 = new wxBoxSizer( wxHORIZONTAL );
	
	m_exportTypeAllRadioBtn = new wxRadioButton( m_panel41, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_exportTypeAllRadioBtn->SetValue( true ); 
	bSizer202->Add( m_exportTypeAllRadioBtn, 0, wxALL, 5 );
	
	m_staticText3 = new wxStaticText( m_panel41, wxID_ANY, wxT("All"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText3->Wrap( -1 );
	m_staticText3->SetForegroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_MENU ) );
	
	bSizer202->Add( m_staticText3, 0, wxALL, 5 );
	
	gSizer6->Add( bSizer202, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer203;
	bSizer203 = new wxBoxSizer( wxHORIZONTAL );
	
	m_exportTypeOnlyRadioBtn = new wxRadioButton( m_panel41, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizer203->Add( m_exportTypeOnlyRadioBtn, 0, wxALL, 5 );
	
	m_staticText4 = new wxStaticText( m_panel41, wxID_ANY, wxT("Displayed"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText4->Wrap( -1 );
	m_staticText4->SetForegroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_MENU ) );
	
	bSizer203->Add( m_staticText4, 0, wxALL, 5 );
	
	gSizer6->Add( bSizer203, 1, wxEXPAND, 5 );
	
	bSizer175->Add( gSizer6, 1, wxEXPAND|wxLEFT, 5 );
	
	m_panel41->SetSizer( bSizer175 );
	m_panel41->Layout();
	bSizer175->Fit( m_panel41 );
	bSizer147->Add( m_panel41, 0, wxEXPAND|wxALL, 5 );
	
	m_staticText3022 = new wxStaticText( this, wxID_ANY, wxT("Model Scale"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText3022->Wrap( -1 );
	m_staticText3022->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), 70, 90, 92, false, wxEmptyString ) );
	m_staticText3022->SetForegroundColour( wxColour( 209, 76, 16 ) );
	
	bSizer147->Add( m_staticText3022, 0, wxALL, 5 );
	
	m_panel412 = new wxPanel( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	m_panel412->SetBackgroundColour( wxColour( 88, 88, 88 ) );
	
	wxBoxSizer* bSizer1752;
	bSizer1752 = new wxBoxSizer( wxVERTICAL );
	
	wxBoxSizer* bSizer1462;
	bSizer1462 = new wxBoxSizer( wxVERTICAL );
	
	wxBoxSizer* bSizer2062;
	bSizer2062 = new wxBoxSizer( wxVERTICAL );
	
	wxBoxSizer* bSizer204;
	bSizer204 = new wxBoxSizer( wxHORIZONTAL );
	
	m_modelScaleRadioBtn1 = new wxRadioButton( m_panel412, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_modelScaleRadioBtn1->SetValue( true ); 
	bSizer204->Add( m_modelScaleRadioBtn1, 0, wxALL, 5 );
	
	m_staticText5 = new wxStaticText( m_panel412, wxID_ANY, wxT("x 1"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText5->Wrap( -1 );
	m_staticText5->SetForegroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_MENU ) );
	
	bSizer204->Add( m_staticText5, 0, wxALL, 5 );
	
	bSizer2062->Add( bSizer204, 1, wxEXPAND, 5 );
	
	bSizer1462->Add( bSizer2062, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer2042;
	bSizer2042 = new wxBoxSizer( wxVERTICAL );
	
	wxBoxSizer* bSizer2052;
	bSizer2052 = new wxBoxSizer( wxHORIZONTAL );
	
	m_modelScaleRadioBtn2 = new wxRadioButton( m_panel412, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizer2052->Add( m_modelScaleRadioBtn2, 0, wxALL, 5 );
	
	m_staticText6 = new wxStaticText( m_panel412, wxID_ANY, wxT("x"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText6->Wrap( -1 );
	m_staticText6->SetForegroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_MENU ) );
	
	bSizer2052->Add( m_staticText6, 0, wxALL, 5 );
	
	m_modelScaleTextCtrl = new wxTextCtrl( m_panel412, wxID_ANY, wxT("2.0"), wxDefaultPosition, wxSize( -1,-1 ), wxTE_PROCESS_ENTER|wxSIMPLE_BORDER );
	m_modelScaleTextCtrl->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_modelScaleTextCtrl->SetBackgroundColour( wxColour( 0, 0, 0 ) );
	
	bSizer2052->Add( m_modelScaleTextCtrl, 0, wxALL, 5 );
	
	bSizer2042->Add( bSizer2052, 1, wxEXPAND, 5 );
	
	wxGridSizer* gSizer12;
	gSizer12 = new wxGridSizer( 2, 3, 0, 0 );
	
	wxBoxSizer* bSizer205;
	bSizer205 = new wxBoxSizer( wxHORIZONTAL );
	
	m_modelScaleRadioBtn3 = new wxRadioButton( m_panel412, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizer205->Add( m_modelScaleRadioBtn3, 0, wxALL, 5 );
	
	m_staticText7 = new wxStaticText( m_panel412, wxID_ANY, wxT("x 10"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText7->Wrap( -1 );
	m_staticText7->SetForegroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_MENU ) );
	
	bSizer205->Add( m_staticText7, 0, wxALL, 5 );
	
	gSizer12->Add( bSizer205, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer206;
	bSizer206 = new wxBoxSizer( wxHORIZONTAL );
	
	m_modelScaleRadioBtn4 = new wxRadioButton( m_panel412, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizer206->Add( m_modelScaleRadioBtn4, 0, wxALL, 5 );
	
	m_staticText8 = new wxStaticText( m_panel412, wxID_ANY, wxT("x 100"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText8->Wrap( -1 );
	m_staticText8->SetForegroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_MENU ) );
	
	bSizer206->Add( m_staticText8, 0, wxALL, 5 );
	
	gSizer12->Add( bSizer206, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer207;
	bSizer207 = new wxBoxSizer( wxHORIZONTAL );
	
	m_modelScaleRadioBtn5 = new wxRadioButton( m_panel412, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizer207->Add( m_modelScaleRadioBtn5, 0, wxALL, 5 );
	
	m_staticText9 = new wxStaticText( m_panel412, wxID_ANY, wxT("x 1000"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText9->Wrap( -1 );
	m_staticText9->SetForegroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_MENU ) );
	
	bSizer207->Add( m_staticText9, 0, wxALL, 5 );
	
	gSizer12->Add( bSizer207, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer208;
	bSizer208 = new wxBoxSizer( wxHORIZONTAL );
	
	m_modelScaleRadioBtn6 = new wxRadioButton( m_panel412, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizer208->Add( m_modelScaleRadioBtn6, 0, wxALL, 5 );
	
	m_staticText10 = new wxStaticText( m_panel412, wxID_ANY, wxT("x 1/10"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText10->Wrap( -1 );
	m_staticText10->SetForegroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_MENU ) );
	
	bSizer208->Add( m_staticText10, 0, wxALL, 5 );
	
	gSizer12->Add( bSizer208, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer209;
	bSizer209 = new wxBoxSizer( wxHORIZONTAL );
	
	m_modelScaleRadioBtn7 = new wxRadioButton( m_panel412, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizer209->Add( m_modelScaleRadioBtn7, 0, wxALL, 5 );
	
	m_staticText11 = new wxStaticText( m_panel412, wxID_ANY, wxT("x 1/100"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText11->Wrap( -1 );
	m_staticText11->SetForegroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_MENU ) );
	
	bSizer209->Add( m_staticText11, 0, wxALL, 5 );
	
	gSizer12->Add( bSizer209, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer210;
	bSizer210 = new wxBoxSizer( wxHORIZONTAL );
	
	m_modelScaleRadioBtn8 = new wxRadioButton( m_panel412, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizer210->Add( m_modelScaleRadioBtn8, 0, wxALL, 5 );
	
	m_staticText12 = new wxStaticText( m_panel412, wxID_ANY, wxT("x 1/1000"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText12->Wrap( -1 );
	m_staticText12->SetForegroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_MENU ) );
	
	bSizer210->Add( m_staticText12, 0, wxALL, 5 );
	
	gSizer12->Add( bSizer210, 1, wxEXPAND, 5 );
	
	bSizer2042->Add( gSizer12, 2, wxEXPAND, 10 );
	
	bSizer1462->Add( bSizer2042, 3, wxEXPAND|wxLEFT, 10 );
	
	bSizer1752->Add( bSizer1462, 0, wxEXPAND|wxLEFT, 5 );
	
	m_panel412->SetSizer( bSizer1752 );
	m_panel412->Layout();
	bSizer1752->Fit( m_panel412 );
	bSizer147->Add( m_panel412, 1, wxEXPAND | wxALL, 5 );
	
	wxBoxSizer* bSizer135;
	bSizer135 = new wxBoxSizer( wxHORIZONTAL );
	
	
	bSizer135->Add( 0, 0, 1, wxEXPAND, 5 );
	
	m_pNextBtn = new wxButton( this, wxID_ANY, wxT("Next"), wxDefaultPosition, wxDefaultSize, 0 );
	m_pNextBtn->SetForegroundColour( wxColour( 44, 44, 44 ) );
	
	bSizer135->Add( m_pNextBtn, 0, wxALL, 5 );
	
	m_pCloseBtn = new wxButton( this, wxID_ANY, wxT("Cancel"), wxDefaultPosition, wxDefaultSize, 0 );
	m_pCloseBtn->SetForegroundColour( wxColour( 44, 44, 44 ) );
	
	bSizer135->Add( m_pCloseBtn, 0, wxALL, 5 );
	
	bSizer147->Add( bSizer135, 0, wxALIGN_BOTTOM|wxEXPAND, 5 );
	
	bSizer24->Add( bSizer147, 0, wxFIXED_MINSIZE, 5 );
	
	this->SetSizer( bSizer24 );
	this->Layout();
	bSizer24->Fit( this );
	
	this->Centre( wxVERTICAL );
	
	// Connect Events
	this->Connect( wxEVT_CLOSE_WINDOW, wxCloseEventHandler( ExportSTLDlgBase::ExportSTLDlgBaseOnClose ) );
	m_modelScaleRadioBtn1->Connect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( ExportSTLDlgBase::OnExportModelScaleRadio ), NULL, this );
	m_modelScaleRadioBtn2->Connect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( ExportSTLDlgBase::OnExportModelScaleRadio ), NULL, this );
	m_modelScaleTextCtrl->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( ExportSTLDlgBase::OnDvdManuTxtOnKillFocus ), NULL, this );
	m_modelScaleTextCtrl->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( ExportSTLDlgBase::OnDvdManuTxt ), NULL, this );
	m_modelScaleRadioBtn3->Connect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( ExportSTLDlgBase::OnExportModelScaleRadio ), NULL, this );
	m_modelScaleRadioBtn4->Connect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( ExportSTLDlgBase::OnExportModelScaleRadio ), NULL, this );
	m_modelScaleRadioBtn5->Connect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( ExportSTLDlgBase::OnExportModelScaleRadio ), NULL, this );
	m_modelScaleRadioBtn6->Connect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( ExportSTLDlgBase::OnExportModelScaleRadio ), NULL, this );
	m_modelScaleRadioBtn7->Connect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( ExportSTLDlgBase::OnExportModelScaleRadio ), NULL, this );
	m_modelScaleRadioBtn8->Connect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( ExportSTLDlgBase::OnExportModelScaleRadio ), NULL, this );
	m_pNextBtn->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( ExportSTLDlgBase::OnNextBtn ), NULL, this );
	m_pCloseBtn->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( ExportSTLDlgBase::OnCancelBtn ), NULL, this );
}

ExportSTLDlgBase::~ExportSTLDlgBase()
{
	// Disconnect Events
	this->Disconnect( wxEVT_CLOSE_WINDOW, wxCloseEventHandler( ExportSTLDlgBase::ExportSTLDlgBaseOnClose ) );
	m_modelScaleRadioBtn1->Disconnect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( ExportSTLDlgBase::OnExportModelScaleRadio ), NULL, this );
	m_modelScaleRadioBtn2->Disconnect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( ExportSTLDlgBase::OnExportModelScaleRadio ), NULL, this );
	m_modelScaleTextCtrl->Disconnect( wxEVT_KILL_FOCUS, wxFocusEventHandler( ExportSTLDlgBase::OnDvdManuTxtOnKillFocus ), NULL, this );
	m_modelScaleTextCtrl->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( ExportSTLDlgBase::OnDvdManuTxt ), NULL, this );
	m_modelScaleRadioBtn3->Disconnect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( ExportSTLDlgBase::OnExportModelScaleRadio ), NULL, this );
	m_modelScaleRadioBtn4->Disconnect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( ExportSTLDlgBase::OnExportModelScaleRadio ), NULL, this );
	m_modelScaleRadioBtn5->Disconnect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( ExportSTLDlgBase::OnExportModelScaleRadio ), NULL, this );
	m_modelScaleRadioBtn6->Disconnect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( ExportSTLDlgBase::OnExportModelScaleRadio ), NULL, this );
	m_modelScaleRadioBtn7->Disconnect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( ExportSTLDlgBase::OnExportModelScaleRadio ), NULL, this );
	m_modelScaleRadioBtn8->Disconnect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( ExportSTLDlgBase::OnExportModelScaleRadio ), NULL, this );
	m_pNextBtn->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( ExportSTLDlgBase::OnNextBtn ), NULL, this );
	m_pCloseBtn->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( ExportSTLDlgBase::OnCancelBtn ), NULL, this );
	
}
