#include "fxgenGUIOuterBCSettingDlgBase.h"
#include "../Core/DomainEditor.h"
#include <wx/grid.h>
#include <vector>
#include <string>

fxgenGUIOuterBCSettingDlgBase::fxgenGUIOuterBCSettingDlgBase( wxWindow* parent, DomainEditor *editor )
:
OuterBCSettingDlgBase( parent )
{
	m_editor = editor;
	if (!m_editor)
		return;
		
	std::vector<std::string> outerbcs, meds;
	m_editor->GetOuterBCNames(outerbcs);
	m_editor->GetMediumNames(meds);
	
	m_pBCMedGrid->SetSelectionMode(wxGrid::wxGridSelectRows);
	m_pBCMedGrid->SetSelectionForeground(wxColour(204,102,0));
	m_pBCMedGrid->SetSelectionBackground(wxColour(72,31,0));
										 
	for(size_t i = 0; i < outerbcs.size(); ++i)
		m_outerbclistArray.push_back(wxString(outerbcs[i].c_str()));
	for(size_t i = 0; i < meds.size(); ++i)
		m_medlistArray.push_back(wxString(meds[i].c_str()));
	
	b8 validFlowbc = false, validThermalbc = false;
	const u32 n = m_editor->GetAxisNum();
	for (u32 i = 0; i < n; ++i)
	{
		std::string flowbc, med;
		m_editor->GetAxisBCMedium(i, flowbc, med);
		if (flowbc != "")
			validFlowbc = true;
		//if (thermalbc != "")
		//	validThermalbc = true;
		setItem(i, flowbc, med);
	}
}

void fxgenGUIOuterBCSettingDlgBase::enableCell(int row, int col, bool enable)
{
	if (col > 1) // only flowbc, thermalbc
		return;
	
	if (!enable) {
		m_pBCMedGrid->SetReadOnly(row, col, true);
		m_pBCMedGrid->SetCellBackgroundColour(row, col, wxColour(88,88,88));
		m_pBCMedGrid->SetCellValue(row, col, "");
	} else {
		if (m_pBCMedGrid->IsReadOnly(row,col))
			m_pBCMedGrid->SetCellValue(row, col, m_outerbclistArray[0]);

		m_pBCMedGrid->SetReadOnly(row, col, false);
		m_pBCMedGrid->SetCellBackgroundColour(row, col, wxColour(0,0,0));
	}
}

void fxgenGUIOuterBCSettingDlgBase::setItem(int i, const std::string& flowbc, const std::string& med)
{
	wxGridCellEditor *celeditor[2];
	celeditor[0] = new wxGridCellChoiceEditor(m_outerbclistArray);
	celeditor[1] = new wxGridCellChoiceEditor(m_medlistArray);
	wxString flowbcstr = flowbc.c_str();
	wxString medstr = med.c_str();
	if (flowbcstr == "")
		flowbcstr = m_outerbclistArray[0];
	if (medstr == "")
		medstr = m_medlistArray[0];

	m_pBCMedGrid->SetCellValue(i, 0, flowbcstr);
	m_pBCMedGrid->SetCellValue(i, 1, medstr);

	for(int i = 0; i < 2; i++){
		wxGridCellAttr* attr = new wxGridCellAttr();
		attr->SetEditor(celeditor[i]);
		m_pBCMedGrid->SetColAttr(i, attr);
	}
}

void fxgenGUIOuterBCSettingDlgBase::OnBCMedGridChange( wxGridEvent& event )
{
	OuterBCSettingDlgBase::OnBCMedGridChange(event);
	m_pBCMedGrid->EnableCellEditControl(true);
}
void fxgenGUIOuterBCSettingDlgBase::OnBCMedGridClick( wxGridEvent& event )
{
//	OuterBCSettingDlgBase::OnBCMedGridClick(event);
}

void fxgenGUIOuterBCSettingDlgBase::OnOKBtn( wxCommandEvent& event )
{
	m_pBCMedGrid->EnableCellEditControl(false);
	const u32 n = m_editor->GetAxisNum();
	for (u32 i = 0; i < n; ++i)
	{
		wxString flowbcStr    = m_pBCMedGrid->GetCellValue(i, 0);
		wxString mediumStr    = m_pBCMedGrid->GetCellValue(i, 1);
		m_editor->SetAxisBCMedium(i, std::string(flowbcStr.c_str()), std::string(mediumStr.c_str()));
	}
	EndDialog(1);
}

void fxgenGUIOuterBCSettingDlgBase::OnCancelBtn( wxCommandEvent& event )
{
	m_pBCMedGrid->EnableCellEditControl(false);
	EndDialog(0);
}
