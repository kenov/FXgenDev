/*
 *
 * BCMedFunc.h
 * 
 */

#ifndef _BCMedFunc_H_
#define _BCMedFunc_H_

#include <string>

namespace VX {
	namespace SG {
		class LocalBC;
		class OuterBC;
		class Medium;
		class LocalBCClass;
		class OuterBCClass;
	} 
} 
namespace UI
{
class BCMedFunc
{
protected:
	BCMedFunc(){};
public:
	virtual ~BCMedFunc(){}
	virtual int GetLocalBCClassNum() const = 0;
	virtual int GetOuterBCClassNum() const = 0;
	virtual std::string GetLocalBCClass(int i) const = 0;
	virtual std::string GetOuterBCClass(int i) const = 0;
		
	virtual int GetLocalBCNum() const = 0;
	virtual int GetOuterBCNum() const = 0;
	virtual int GetMediumNum () const = 0;
	virtual const VX::SG::LocalBC* GetLocalBC(int i) const = 0;
	virtual const VX::SG::OuterBC* GetOuterBC(int i) const = 0;
	virtual const VX::SG::Medium*  GetMedium (int i) const = 0;

	virtual void ClearLocalBC() = 0;
	virtual void ClearOuterBC() = 0;
	virtual void ClearMedium()  = 0;
	virtual void AddLocalBC(const VX::SG::LocalBC* localbc) = 0;
	virtual void AddOuterBC(const VX::SG::OuterBC* outerbc) = 0;
	virtual void AddMedium (const VX::SG::Medium*  medium ) = 0;
	virtual void AddLocalBCClass(const VX::SG::LocalBCClass* localbcclass) = 0;
	virtual void AddOuterBCClass(const VX::SG::OuterBCClass* outerbcclass) = 0;
		
	virtual const VX::SG::LocalBC* CreateLocalBC(const std::string& name, const std::string& classname, float col[4], unsigned int id, const std::string& mediumname) const = 0;
	virtual const VX::SG::OuterBC* CreateOuterBC(const std::string& name, const std::string& classname) const = 0;
	virtual const VX::SG::Medium*  CreateMedium (const std::string& label, float col[4], unsigned int id, const std::string& typeName) const = 0;
};

}

#endif // _VX_DIALOG_UTIL_H_
