
#include "SliceCtrlDlg.h"
#include "../Core/SliceController.h"

#include "vxWindow.h"

namespace {

	unsigned char leftArrow[] = {
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X60, 0X60, 0X60, 0X8B, 0X8B, 0X8B, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0XA1, 0XA1, 0XA1, 
		0XCB, 0XCB, 0XCB, 0XB8, 0XB8, 0XB8, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X88, 0X88, 0X88, 0XCF, 0XCF, 0XCF, 0XDB, 0XDB, 0XDB, 
		0XC9, 0XC9, 0XC9, 0X92, 0X92, 0X92, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X71, 0X71, 0X71, 
		0XC5, 0XC5, 0XC5, 0XDB, 0XDB, 0XDB, 0XD9, 0XD9, 0XD9, 0XCD, 0XCD, 0XCD, 
		0XAF, 0XAF, 0XAF, 0X74, 0X74, 0X74, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0XB4, 0XB4, 0XB4, 0XDA, 0XDA, 0XDA, 
		0XDB, 0XDB, 0XDB, 0XD3, 0XD3, 0XD3, 0XCB, 0XCB, 0XCB, 0XC2, 0XC2, 0XC2, 
		0X97, 0X97, 0X97, 0X6A, 0X6A, 0X6A, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X96, 0X96, 0X96, 0XD6, 0XD6, 0XD6, 0XDD, 0XDD, 0XDD, 0XD5, 0XD5, 0XD5, 
		0XCE, 0XCE, 0XCE, 0XC7, 0XC7, 0XC7, 0XC3, 0XC3, 0XC3, 0XB6, 0XB6, 0XB6, 
		0X91, 0X91, 0X91, 0X67, 0X67, 0X67, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X79, 0X79, 0X79, 0XD0, 0XD0, 0XD0, 
		0XDF, 0XDF, 0XDF, 0XD8, 0XD8, 0XD8, 0XD0, 0XD0, 0XD0, 0XC9, 0XC9, 0XC9, 
		0XC4, 0XC4, 0XC4, 0XC1, 0XC1, 0XC1, 0XC0, 0XC0, 0XC0, 0XB4, 0XB4, 0XB4, 
		0X90, 0X90, 0X90, 0X67, 0X67, 0X67, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X61, 0X61, 0X61, 0XC5, 0XC5, 0XC5, 0XDC, 0XDC, 0XDC, 0XDA, 0XDA, 0XDA, 
		0XD2, 0XD2, 0XD2, 0XCA, 0XCA, 0XCA, 0XC5, 0XC5, 0XC5, 0XC2, 0XC2, 0XC2, 
		0XC0, 0XC0, 0XC0, 0XC0, 0XC0, 0XC0, 0XBF, 0XBF, 0XBF, 0XB3, 0XB3, 0XB3, 
		0X90, 0X90, 0X90, 0X67, 0X67, 0X67, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0XA3, 0XA3, 0XA3, 
		0XD6, 0XD6, 0XD6, 0XD9, 0XD9, 0XD9, 0XD3, 0XD3, 0XD3, 0XCC, 0XCC, 0XCC, 
		0XC6, 0XC6, 0XC6, 0XC2, 0XC2, 0XC2, 0XC1, 0XC1, 0XC1, 0XC0, 0XC0, 0XC0, 
		0XC0, 0XC0, 0XC0, 0XC0, 0XC0, 0XC0, 0XBF, 0XBF, 0XBF, 0XB3, 0XB3, 0XB3, 
		0X90, 0X90, 0X90, 0X67, 0X67, 0X67, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X8F, 0X8F, 0X8F, 0XC4, 0XC4, 0XC4, 
		0XC4, 0XC4, 0XC4, 0XC2, 0XC2, 0XC2, 0XC1, 0XC1, 0XC1, 0XC0, 0XC0, 0XC0, 
		0XC0, 0XC0, 0XC0, 0XC0, 0XC0, 0XC0, 0XC0, 0XC0, 0XC0, 0XC0, 0XC0, 0XC0, 
		0XC0, 0XC0, 0XC0, 0XC0, 0XC0, 0XC0, 0XBF, 0XBF, 0XBF, 0XB3, 0XB3, 0XB3, 
		0X90, 0X90, 0X90, 0X67, 0X67, 0X67, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X63, 0X63, 0X63, 
		0X7C, 0X7C, 0X7C, 0X82, 0X82, 0X82, 0X8E, 0X8E, 0X8E, 0XA0, 0XA0, 0XA0, 
		0XB0, 0XB0, 0XB0, 0XBA, 0XBA, 0XBA, 0XBE, 0XBE, 0XBE, 0XBF, 0XBF, 0XBF, 
		0XC0, 0XC0, 0XC0, 0XC0, 0XC0, 0XC0, 0XBF, 0XBF, 0XBF, 0XB3, 0XB3, 0XB3, 
		0X90, 0X90, 0X90, 0X67, 0X67, 0X67, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X5D, 0X5D, 0X5D, 0X69, 0X69, 0X69, 0X79, 0X79, 0X79, 
		0X90, 0X90, 0X90, 0XA4, 0XA4, 0XA4, 0XB3, 0XB3, 0XB3, 0XBB, 0XBB, 0XBB, 
		0XBF, 0XBF, 0XBF, 0XC0, 0XC0, 0XC0, 0XBF, 0XBF, 0XBF, 0XB3, 0XB3, 0XB3, 
		0X90, 0X90, 0X90, 0X67, 0X67, 0X67, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X60, 0X60, 0X60, 
		0X6B, 0X6B, 0X6B, 0X7E, 0X7E, 0X7E, 0X95, 0X95, 0X95, 0XA9, 0XA9, 0XA9, 
		0XB6, 0XB6, 0XB6, 0XBD, 0XBD, 0XBD, 0XBE, 0XBE, 0XBE, 0XB3, 0XB3, 0XB3, 
		0X90, 0X90, 0X90, 0X67, 0X67, 0X67, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X59, 0X59, 0X59, 0X63, 0X63, 0X63, 0X70, 0X70, 0X70, 0X85, 0X85, 0X85, 
		0X9B, 0X9B, 0X9B, 0XAD, 0XAD, 0XAD, 0XB8, 0XB8, 0XB8, 0XB1, 0XB1, 0XB1, 
		0X90, 0X90, 0X90, 0X67, 0X67, 0X67, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X5B, 0X5B, 0X5B, 0X67, 0X67, 0X67, 
		0X76, 0X76, 0X76, 0X8B, 0X8B, 0X8B, 0XA0, 0XA0, 0XA0, 0XA4, 0XA4, 0XA4, 
		0X8B, 0X8B, 0X8B, 0X67, 0X67, 0X67, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X5E, 0X5E, 0X5E, 0X68, 0X68, 0X68, 0X7A, 0X7A, 0X7A, 0X85, 0X85, 0X85, 
		0X78, 0X78, 0X78, 0X60, 0X60, 0X60, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X60, 0X60, 0X60, 0X63, 0X63, 0X63, 
		0X5B, 0X5B, 0X5B, 0X52, 0X52, 0X52, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X59, 0X59, 0X59, 
		0X51, 0X51, 0X51, 0X4B, 0X4B, 0X4B, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X59, 0X59, 0X59, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		};

	unsigned char rightArrow[] = {
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X91, 0X91, 0X91, 
		0X60, 0X60, 0X60, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0XCF, 0XCF, 0XCF, 
		0XCD, 0XCD, 0XCD, 0X9A, 0X9A, 0X9A, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0XC7, 0XC7, 0XC7, 
		0XD4, 0XD4, 0XD4, 0XD1, 0XD1, 0XD1, 0XBF, 0XBF, 0XBF, 0X81, 0X81, 0X81, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0XC1, 0XC1, 0XC1, 
		0XCE, 0XCE, 0XCE, 0XCA, 0XCA, 0XCA, 0XCB, 0XCB, 0XCB, 0XCA, 0XCA, 0XCA, 
		0XB5, 0XB5, 0XB5, 0X6D, 0X6D, 0X6D, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0XC0, 0XC0, 0XC0, 
		0XCA, 0XCA, 0XCA, 0XC5, 0XC5, 0XC5, 0XC5, 0XC5, 0XC5, 0XC8, 0XC8, 0XC8, 
		0XCB, 0XCB, 0XCB, 0XC8, 0XC8, 0XC8, 0XA6, 0XA6, 0XA6, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0XC0, 0XC0, 0XC0, 
		0XC9, 0XC9, 0XC9, 0XC3, 0XC3, 0XC3, 0XC1, 0XC1, 0XC1, 0XC3, 0XC3, 0XC3, 
		0XC5, 0XC5, 0XC5, 0XC9, 0XC9, 0XC9, 0XCC, 0XCC, 0XCC, 0XC3, 0XC3, 0XC3, 
		0X8D, 0X8D, 0X8D, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0XC0, 0XC0, 0XC0, 
		0XC9, 0XC9, 0XC9, 0XC3, 0XC3, 0XC3, 0XC0, 0XC0, 0XC0, 0XC0, 0XC0, 0XC0, 
		0XC1, 0XC1, 0XC1, 0XC3, 0XC3, 0XC3, 0XC6, 0XC6, 0XC6, 0XCA, 0XCA, 0XCA, 
		0XCD, 0XCD, 0XCD, 0XBF, 0XBF, 0XBF, 0X74, 0X74, 0X74, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0XC0, 0XC0, 0XC0, 
		0XC9, 0XC9, 0XC9, 0XC3, 0XC3, 0XC3, 0XC0, 0XC0, 0XC0, 0XC0, 0XC0, 0XC0, 
		0XC0, 0XC0, 0XC0, 0XC1, 0XC1, 0XC1, 0XC2, 0XC2, 0XC2, 0XC4, 0XC4, 0XC4, 
		0XC7, 0XC7, 0XC7, 0XCB, 0XCB, 0XCB, 0XCA, 0XCA, 0XCA, 0XB5, 0XB5, 0XB5, 
		0X5F, 0X5F, 0X5F, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0XC0, 0XC0, 0XC0, 
		0XC9, 0XC9, 0XC9, 0XC3, 0XC3, 0XC3, 0XC0, 0XC0, 0XC0, 0XC0, 0XC0, 0XC0, 
		0XC0, 0XC0, 0XC0, 0XC0, 0XC0, 0XC0, 0XC0, 0XC0, 0XC0, 0XC1, 0XC1, 0XC1, 
		0XC2, 0XC2, 0XC2, 0XC4, 0XC4, 0XC4, 0XC5, 0XC5, 0XC5, 0XC4, 0XC4, 0XC4, 
		0XBC, 0XBC, 0XBC, 0X8F, 0X8F, 0X8F, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0XC0, 0XC0, 0XC0, 
		0XC9, 0XC9, 0XC9, 0XC3, 0XC3, 0XC3, 0XC0, 0XC0, 0XC0, 0XC0, 0XC0, 0XC0, 
		0XC0, 0XC0, 0XC0, 0XC0, 0XC0, 0XC0, 0XC0, 0XC0, 0XC0, 0XBE, 0XBE, 0XBE, 
		0XBB, 0XBB, 0XBB, 0XB2, 0XB2, 0XB2, 0XA2, 0XA2, 0XA2, 0X8E, 0X8E, 0X8E, 
		0X7B, 0X7B, 0X7B, 0X75, 0X75, 0X75, 0X6E, 0X6E, 0X6E, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0XC0, 0XC0, 0XC0, 
		0XC9, 0XC9, 0XC9, 0XC3, 0XC3, 0XC3, 0XC0, 0XC0, 0XC0, 0XC0, 0XC0, 0XC0, 
		0XC0, 0XC0, 0XC0, 0XBF, 0XBF, 0XBF, 0XBB, 0XBB, 0XBB, 0XB2, 0XB2, 0XB2, 
		0XA0, 0XA0, 0XA0, 0X84, 0X84, 0X84, 0X66, 0X66, 0X66, 0X4E, 0X4E, 0X4E, 
		0X47, 0X47, 0X47, 0X56, 0X56, 0X56, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0XC0, 0XC0, 0XC0, 
		0XC9, 0XC9, 0XC9, 0XC3, 0XC3, 0XC3, 0XC0, 0XC0, 0XC0, 0XBF, 0XBF, 0XBF, 
		0XBD, 0XBD, 0XBD, 0XB5, 0XB5, 0XB5, 0XA5, 0XA5, 0XA5, 0X8B, 0X8B, 0X8B, 
		0X6D, 0X6D, 0X6D, 0X51, 0X51, 0X51, 0X40, 0X40, 0X40, 0X4D, 0X4D, 0X4D, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0XC0, 0XC0, 0XC0, 
		0XC9, 0XC9, 0XC9, 0XC3, 0XC3, 0XC3, 0XBF, 0XBF, 0XBF, 0XB8, 0XB8, 0XB8, 
		0XAA, 0XAA, 0XAA, 0X93, 0X93, 0X93, 0X75, 0X75, 0X75, 0X57, 0X57, 0X57, 
		0X43, 0X43, 0X43, 0X45, 0X45, 0X45, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0XC0, 0XC0, 0XC0, 
		0XC8, 0XC8, 0XC8, 0XC1, 0XC1, 0XC1, 0XAF, 0XAF, 0XAF, 0X9A, 0X9A, 0X9A, 
		0X7D, 0X7D, 0X7D, 0X5E, 0X5E, 0X5E, 0X46, 0X46, 0X46, 0X41, 0X41, 0X41, 
		0X56, 0X56, 0X56, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0XBF, 0XBF, 0XBF, 
		0XC5, 0XC5, 0XC5, 0XAB, 0XAB, 0XAB, 0X87, 0X87, 0X87, 0X67, 0X67, 0X67, 
		0X4B, 0X4B, 0X4B, 0X3F, 0X3F, 0X3F, 0X53, 0X53, 0X53, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0XBA, 0XBA, 0XBA, 
		0XAD, 0XAD, 0XAD, 0X79, 0X79, 0X79, 0X53, 0X53, 0X53, 0X40, 0X40, 0X40, 
		0X4A, 0X4A, 0X4A, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0XA4, 0XA4, 0XA4, 
		0X77, 0X77, 0X77, 0X4B, 0X4B, 0X4B, 0X44, 0X44, 0X44, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X83, 0X83, 0X83, 
		0X56, 0X56, 0X56, 0X57, 0X57, 0X57, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X5B, 0X5B, 0X5B, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 0X58, 
		};

		// create color map image
		wxBitmap CreateColorMapBitmap(s32 width, s32 height, SliceController* controller)
		{
			const u32 COLORMAP_RANGE = 256;
			const u32 COLORMAP_OFFSET = 12;
			if (width < COLORMAP_RANGE + COLORMAP_OFFSET) return wxBitmap(width, height);

			u8* buf = new u8[width * 1 * 3];
			memset(buf, 255, width * 1 * 3);
			const s32 start = COLORMAP_OFFSET;
			const s32 last = start + COLORMAP_RANGE;
			controller->SetColorMap(buf + start * 3, COLORMAP_RANGE);
			
			for (s32 i = 0; i < start-1; ++i)
			{
				buf[i*3 + 0] = buf[start*3 + 0];
				buf[i*3 + 1] = buf[start*3 + 1];
				buf[i*3 + 2] = buf[start*3 + 2];
			}

			for (s32 i = last+1; i < width; ++i)
			{
				buf[i*3 + 0] = buf[(last-1)*3 + 0];
				buf[i*3 + 1] = buf[(last-1)*3 + 1];
				buf[i*3 + 2] = buf[(last-1)*3 + 2];
			}

			wxImage image = wxImage(width, 1, buf, false);
			image.Rescale(width, height);

			return wxBitmap(image);
		}

		void EnsureSelectLevel(wxChoice* choice, s32 level)
		{
			wxString text = (level == -1) ? "Max" : wxString::Format("%i", level);
			if (!choice->SetStringSelection(text))
			{
				choice->Select(0);
			}
		}

		bool ParseLevelString(const wxString& text, s32* val)
		{
			if (text == "Max")
			{
				*val = -1;
				return true;
			}

			long level;
			if (text.ToLong(&level))
			{
				*val = (s32)level;
				return true;
			}

			return false;
		}

} // namespace 


namespace UI
{
	SliceCtrlDlg::SliceCtrlDlg(wxWindow *parent) : SliceCtrlDlgBase(parent)
	{
		unsigned char *limg = new unsigned char[20 * 20 * 3];
		unsigned char *rimg = new unsigned char[20 * 20 * 3];
		memcpy(limg, leftArrow, (20 * 20 * 3));
		memcpy(rimg, rightArrow, (20 * 20 * 3));
		
		wxImage laImg(20, 20, limg, false);
		wxImage raImg(20, 20, rimg, false);

		wxBitmap labImg(laImg);
		wxBitmap rabImg(raImg);

		m_pXPosUpBtn->SetBitmapLabel(rabImg);
		m_pYPosUpBtn->SetBitmapLabel(rabImg);
		m_pZPosUpBtn->SetBitmapLabel(rabImg);

		m_pXPosDownBtn->SetBitmapLabel(labImg);
		m_pYPosDownBtn->SetBitmapLabel(labImg);
		m_pZPosDownBtn->SetBitmapLabel(labImg);

		Fit();
		FitInside();
	}

	void SliceCtrlDlg::SetController(SliceController* controller)
	{
		m_ctrl = controller;
		if(!m_ctrl) return;

		m_bbox[0] = m_ctrl->GetBBoxMin();
		m_bbox[1] = m_ctrl->GetBBoxMax();
		
		for(int i = 0; i < 3; i++){
			m_slidePitch[i] = (m_bbox[1][i] - m_bbox[0][i]) / 100.0f;
			m_position[i] = m_ctrl->GetPosition(i);
			m_spinPitch[i] = m_ctrl->GetSpinPitch(i);
			SetSlidePos(i);
			SetPosTxt(i, m_ctrl->GetSlideUnit());
		}
		SetPitchTxt(m_ctrl->GetSlideUnit());

		if (!m_ctrl->CheckSupportSlideUnit(SliceController::SLIDE_IDX))
		{
			SetPitchTxt(SliceController::SLIDE_COORD);
			m_ctrl->SetSlideUnit(SliceController::SLIDE_COORD);
		}

		if (m_ctrl->GetSlideUnit() == SliceController::SLIDE_IDX) {
			m_pPolicyIdxBtn->SetValue(true);
			m_pPolicyCoordBtn->SetValue(false);
		} else {
			m_pPolicyIdxBtn->SetValue(false);
			m_pPolicyCoordBtn->SetValue(true);			
		}

		m_pPolicyIdxBtn->Enable(m_ctrl->CheckSupportSlideUnit(SliceController::SLIDE_IDX));
		m_pPolicyIdxLabel->Enable(m_ctrl->CheckSupportSlideUnit(SliceController::SLIDE_IDX));
		m_pPolicyCoordBtn->Enable(m_ctrl->CheckSupportSlideUnit(SliceController::SLIDE_COORD));
		m_pPolicyCoordLabel->Enable(m_ctrl->CheckSupportSlideUnit(SliceController::SLIDE_COORD));

		// ColorMap
		if (m_ctrl->CheckColorMapStatus())
		{
			m_pColorMapSizer->Show(true);
			wxBitmap colorMap = CreateColorMapBitmap(320, 4, m_ctrl);
			m_pColorMapImage->SetBitmap(colorMap);
			m_pColorMapRangeTxt->SetLabelText(wxString::Format("(%.2f - %.2f)", m_ctrl->GetColorMapRangeMin(), m_ctrl->GetColorMapRangeMax()));
			
			s32 minLevel, maxLevel;
			u32 selectMax;
			m_ctrl->GetColorMapRangeLevels(&minLevel, &maxLevel, &selectMax);
			wxArrayString levels;
			levels.reserve(selectMax);
			for (s32 i = selectMax; 0 <= i; --i)
			{
				levels.Add(wxString::Format("%i", i));
			}
			m_pColorMapRangeMinLevel->Clear();
			m_pColorMapRangeMinLevel->Append(levels);
			m_pColorMapRangeMinLevel->Select(minLevel);
			EnsureSelectLevel(m_pColorMapRangeMinLevel, minLevel);
			m_pColorMapRangeMaxLevel->Clear();
			m_pColorMapRangeMaxLevel->Append("Max");
			m_pColorMapRangeMaxLevel->Append(levels);
			m_pColorMapRangeMaxLevel->Select(maxLevel);
			EnsureSelectLevel(m_pColorMapRangeMaxLevel, maxLevel);

		} else {
			m_pColorMapSizer->Show(false);
		}

		this->Fit();
	}

	void SliceCtrlDlg:: UpdateControllerView()
	{
		if (m_ctrl)
		{
			SetController(m_ctrl);
		}
	}


	void SliceCtrlDlg::OnXSlideDrag( wxScrollEvent& event )
	{
		if(!m_ctrl) return;

		f32 slidePos = static_cast<f32>(m_pXSlider->GetValue());
		const int axis = 0;
		// Positionのアップデート
		m_ctrl->SetPositionDrag(axis, m_bbox[0][axis] + slidePos * m_slidePitch[axis]);
		m_position[axis] = m_ctrl->GetPosition(axis);
		// Positionテキストのアップデート
		SetPosTxt(axis, m_ctrl->GetSlideUnit());
		UI::vxWindow::GetTopWindow()->Draw();
	}

	void SliceCtrlDlg::OnXSlideScroll( wxScrollEvent& event )
	{
		if(!m_ctrl) return;

		f32 slidePos = static_cast<f32>(m_pXSlider->GetValue());
		const int axis = 0;
		// Positionのアップデート
		m_ctrl->SetPosition(axis, m_bbox[0][axis] + slidePos * m_slidePitch[axis]);
		m_position[axis] = m_ctrl->GetPosition(axis);
		// Positionテキストのアップデート
		SetPosTxt(axis, m_ctrl->GetSlideUnit());
		UI::vxWindow::GetTopWindow()->Draw();
	}

	void SliceCtrlDlg::OnYSlideDrag( wxScrollEvent& event )
	{
		if(!m_ctrl) return;

		f32 slidePos = static_cast<f32>(m_pYSlider->GetValue());
		const int axis = 1;
		// Positionのアップデート
		m_ctrl->SetPositionDrag(axis, m_bbox[0][axis] + slidePos * m_slidePitch[axis]);
		m_position[axis] = m_ctrl->GetPosition(axis);
		// Positionテキストのアップデート
		SetPosTxt(axis, m_ctrl->GetSlideUnit());
		UI::vxWindow::GetTopWindow()->Draw();
	}

	void SliceCtrlDlg::OnYSlideScroll( wxScrollEvent& event )
	{
		if(!m_ctrl) return;

		f32 slidePos = static_cast<f32>(m_pYSlider->GetValue());
		const int axis = 1;
		// Positionのアップデート
		m_ctrl->SetPosition(axis, m_bbox[0][axis] + slidePos * m_slidePitch[axis]);
		m_position[axis] = m_ctrl->GetPosition(axis);
		// Positionテキストのアップデート
		SetPosTxt(axis, m_ctrl->GetSlideUnit());
		UI::vxWindow::GetTopWindow()->Draw();
	}

	void SliceCtrlDlg::OnZSlideDrag( wxScrollEvent& event )
	{
		if(!m_ctrl) return;

		f32 slidePos = static_cast<f32>(m_pZSlider->GetValue());
		const int axis = 2;
		// Positionのアップデート
		m_ctrl->SetPositionDrag(axis, m_bbox[0][axis] + slidePos * m_slidePitch[axis]);
		m_position[axis] = m_ctrl->GetPosition(axis);
		// Positionテキストのアップデート
		SetPosTxt(axis, m_ctrl->GetSlideUnit());
		UI::vxWindow::GetTopWindow()->Draw();
	}

	void SliceCtrlDlg::OnZSlideScroll( wxScrollEvent& event )
	{
		if(!m_ctrl) return;

		f32 slidePos = static_cast<f32>(m_pZSlider->GetValue());
		const int axis = 2;
		// Positionのアップデート
		m_ctrl->SetPosition(axis, m_bbox[0][axis] + slidePos * m_slidePitch[axis]);
		m_position[axis] = m_ctrl->GetPosition(axis);
		// Positionテキストのアップデート
		SetPosTxt(axis, m_ctrl->GetSlideUnit());
		UI::vxWindow::GetTopWindow()->Draw();
	}

	// ダイアログのPosition変更
	void SliceCtrlDlg::OnPosTxt( wxCommandEvent& event )
	{
		if(!m_ctrl) return;

		u32 axis = 0;
		switch (event.GetId()) {
		default:
			// 指定したIDではない場合は終了
			return;
		case SLICE_CTRL_XPOS_TXT:
			axis = 0;
			break;
		case SLICE_CTRL_YPOS_TXT:
			axis = 1;
			break;
		case SLICE_CTRL_ZPOS_TXT:
			axis = 2;
			break;
		}
		// 値の保存
		SetPosValue(axis, m_ctrl->GetSlideUnit());
		
		// スライダーバーの位置をPositionに合わせる
		SetSlidePos(axis);

		// 位置の設定
		m_ctrl->SetPosition(axis, m_position[axis]);
		// 再描画
		UI::vxWindow::GetTopWindow()->Draw();
	}
	
	// 入力されたposition値を範囲内に収めた値に切り詰めて返す
	f32 SliceCtrlDlg::GetLimitedPos(f32 pos, u32 axis)
	{
		f32 newpos = pos;
		if (pos < m_bbox[0][axis]) newpos = m_bbox[0][axis];
		if (pos > m_bbox[1][axis]) newpos = m_bbox[1][axis];
		return newpos;
	}

	void SliceCtrlDlg::OnXPosUp( wxCommandEvent& event )
	{
		if(!m_ctrl) return;

		u32 axis = 0;
		// 新しいPositionの計算
		m_position[axis] = GetLimitedPos(m_position[axis] + m_spinPitch[axis], axis);

		// スライダー、Positionの位置を変更
		SetPosTxt(axis, m_ctrl->GetSlideUnit());
		SetSlidePos(axis);

		// 位置の設定
		m_ctrl->SetPosition(axis, m_position[axis]);
		m_position[axis] = m_ctrl->GetPosition(axis);
		// 再描画
		UI::vxWindow::GetTopWindow()->Draw();
	}

	void SliceCtrlDlg::OnXPosDown( wxCommandEvent& event )
	{
		if(!m_ctrl) return;

		u32 axis = 0;
		// 新しいPositionの計算
		m_position[axis] = GetLimitedPos(m_position[axis] - m_spinPitch[axis], axis);

		// スライダー、Positionの位置を変更
		SetPosTxt(axis, m_ctrl->GetSlideUnit());
		SetSlidePos(axis);

		// 位置の設定
		m_ctrl->SetPosition(axis, m_position[axis]);
		m_position[axis] = m_ctrl->GetPosition(axis);
		// 再描画
		UI::vxWindow::GetTopWindow()->Draw();
	}

	void SliceCtrlDlg::OnYPosUp( wxCommandEvent& event )
	{
		if(!m_ctrl) return;

		u32 axis = 1;
		// 新しいPositionの計算
		m_position[axis] = GetLimitedPos(m_position[axis] + m_spinPitch[axis], axis);

		// スライダー、Positionの位置を変更
		SetPosTxt(axis, m_ctrl->GetSlideUnit());
		SetSlidePos(axis);

		// 位置の設定
		m_ctrl->SetPosition(axis, m_position[axis]);
		m_position[axis] = m_ctrl->GetPosition(axis);
		// 再描画
		UI::vxWindow::GetTopWindow()->Draw();
	}

	void SliceCtrlDlg::OnYPosDown( wxCommandEvent& event )
	{
		if(!m_ctrl) return;

		u32 axis = 1;
		// 新しいPositionの計算
		m_position[axis] = GetLimitedPos(m_position[axis] - m_spinPitch[axis], axis);

		// スライダー、Positionの位置を変更
		SetPosTxt(axis, m_ctrl->GetSlideUnit());
		SetSlidePos(axis);

		// 位置の設定
		m_ctrl->SetPosition(axis, m_position[axis]);
		m_position[axis] = m_ctrl->GetPosition(axis);
		// 再描画
		UI::vxWindow::GetTopWindow()->Draw();
	}

	void SliceCtrlDlg::OnZPosUp( wxCommandEvent& event )
	{
		if(!m_ctrl) return;

		u32 axis = 2;
		// 新しいPositionの計算
		m_position[axis] = GetLimitedPos(m_position[axis] + m_spinPitch[axis], axis);

		// スライダー、Positionの位置を変更
		SetPosTxt(axis, m_ctrl->GetSlideUnit());
		SetSlidePos(axis);

		// 位置の設定
		m_ctrl->SetPosition(axis, m_position[axis]);
		m_position[axis] = m_ctrl->GetPosition(axis);
		// 再描画
		UI::vxWindow::GetTopWindow()->Draw();
	}

	void SliceCtrlDlg::OnZPosDown( wxCommandEvent& event )
	{
		if(!m_ctrl) return;

		u32 axis = 2;
		// 新しいPositionの計算
		m_position[axis] = GetLimitedPos(m_position[axis] - m_spinPitch[axis], axis);

		// スライダー、Positionの位置を変更
		SetPosTxt(axis, m_ctrl->GetSlideUnit());
		SetSlidePos(axis);

		// 位置の設定
		m_ctrl->SetPosition(axis, m_position[axis]);
		m_position[axis] = m_ctrl->GetPosition(axis);
		// 再描画
		UI::vxWindow::GetTopWindow()->Draw();
	}

	// ダイアログのPitchの変更
	void SliceCtrlDlg::OnPitchTxt( wxCommandEvent& event )
	{
		if(!m_ctrl) return;

		switch (event.GetId()) {
		default:
			// 指定したIDではない場合は終了
			return;
		case SLICE_CTRL_XPITCH_TXT:
			SetPitchValue(0, m_ctrl->GetSlideUnit());
			break;
		case SLICE_CTRL_YPITCH_TXT:
			SetPitchValue(1, m_ctrl->GetSlideUnit());
			break;
		case SLICE_CTRL_ZPITCH_TXT:
			SetPitchValue(2, m_ctrl->GetSlideUnit());
			break;
		}
	}

	// radio buttonの設定
	void SliceCtrlDlg::OnRadioButton( wxCommandEvent& event )
	{
		if(!m_ctrl) return;

		switch (event.GetId()) {
		default:
			// 指定したIDではない場合は終了
			return;
		// 格子単位で移動
		case SLICE_CTRL_IDX_RADIO:
			if (m_ctrl->CheckSupportSlideUnit(SliceController::SLIDE_IDX)) {

				if (m_ctrl->GetSlideUnit() == SliceController::SLIDE_COORD) {
					// pitchの表示を切り替える
					SetPitchTxt(SliceController::SLIDE_IDX);
				}
				// 移動の単位を格子単位に変更
				m_ctrl->SetSlideUnit(SliceController::SLIDE_IDX);
			}
			break;
		// 座標位置で移動
		case SLICE_CTRL_COORD_RADIO:
			if (m_ctrl->CheckSupportSlideUnit(SliceController::SLIDE_COORD)) {
				if (m_ctrl->GetSlideUnit() == SliceController::SLIDE_IDX) {
					// pitchの表示を切り替える
					SetPitchTxt(SliceController::SLIDE_COORD);
				}
				// 移動の単位を座標単位に変更
				m_ctrl->SetSlideUnit(SliceController::SLIDE_COORD);
			}
			break;
		}

		// Positionの値を変更する
		for (int i = 0; i < 3; i++) {
			m_ctrl->SetPosition(i, m_position[i]);
			m_position[i] = m_ctrl->GetPosition(i);
			SetPosTxt(i, m_ctrl->GetSlideUnit());
		}
		// 再描画
		UI::vxWindow::GetTopWindow()->Draw();

	}

	void SliceCtrlDlg::OnColorMapMinLevelChoice(wxCommandEvent& event)
	{
		s32 min, max;
		if (!ParseLevelString(m_pColorMapRangeMinLevel->GetStringSelection(), &min)) return;
		if (!ParseLevelString(m_pColorMapRangeMaxLevel->GetStringSelection(), &max)) return;
		m_ctrl->SetColorMapRangeLevels(min, max);
		m_pColorMapRangeTxt->SetLabelText(wxString::Format("(%.2f - %.2f)", m_ctrl->GetColorMapRangeMin(), m_ctrl->GetColorMapRangeMax()));

		UI::vxWindow::GetTopWindow()->Draw();
	}

	void SliceCtrlDlg::OnColorMapMaxLevelChoice(wxCommandEvent& event)
	{
		s32 min, max;
		if (!ParseLevelString(m_pColorMapRangeMinLevel->GetStringSelection(), &min)) return;
		if (!ParseLevelString(m_pColorMapRangeMaxLevel->GetStringSelection(), &max)) return;
		m_ctrl->SetColorMapRangeLevels(min, max);
		m_pColorMapRangeTxt->SetLabelText(wxString::Format("(%.2f - %.2f)", m_ctrl->GetColorMapRangeMin(), m_ctrl->GetColorMapRangeMax()));

		UI::vxWindow::GetTopWindow()->Draw();
	}


	// m_positionの値からスライドバーの位置を設定
	void SliceCtrlDlg::SetSlidePos(u32 axis)
	{
		if(!m_ctrl) return;

		wxSlider *slider = 0; // 設定するテキストコントロール
		switch (axis) {
		default:
			// 指定した軸ではない場合は終了
			break;
		case 0: // X軸
			slider = m_pXSlider;
			break;
		case 1: // Y軸
			slider = m_pYSlider;
			break;
		case 2: // Z軸
			slider = m_pZSlider;
			break;
		}
		int slidePos = static_cast<int>((m_position[axis] - m_bbox[0][axis]) / m_slidePitch[axis]);
		slider->SetValue(slidePos);
	}

	// m_positionの値からポジションテキストを設定
	void SliceCtrlDlg::SetPosTxt(u32 axis, u32 unit)
	{
		if(!m_ctrl) return;

		wxTextCtrl *text_ctrl = 0; // 設定するテキストコントロール
		switch (axis) {
		default:
			// 指定した軸ではない場合は終了
			break;
		case 0: // X軸
			text_ctrl = m_pXPosTxt;
			break;
		case 1: // Y軸
			text_ctrl = m_pYPosTxt;
			break;
		case 2: // Z軸
			text_ctrl = m_pZPosTxt;
			break;
		}

		if (unit == SliceController::SLIDE_IDX) {
			// インデックス番号で管理する場合は座標をインデックス番号に変換する
			int idx_pos = ConvertPosCoord2Idx(m_position[axis], axis);
			text_ctrl->SetValue(wxString::Format("%d", idx_pos));
		} else {
			// 座標値で管理する場合はそのまま書く
			text_ctrl->SetValue(wxString::Format("%.6f", m_position[axis]));
		}

	}

	// Positionの値からいろいろ設定
	void SliceCtrlDlg::SetPosValue(u32 axis, u32 unit)
	{
		if(!m_ctrl) return;

		wxTextCtrl *text_ctrl = 0; // 設定するテキストコントロール
		switch (axis) {
		default:
			// 指定した軸ではない場合は終了
			break;
		case 0: // X軸
			text_ctrl = m_pXPosTxt;
			break;
		case 1: // Y軸
			text_ctrl = m_pYPosTxt;
			break;
		case 2: // Z軸
			text_ctrl = m_pZPosTxt;
			break;
		}
		if (unit == SliceController::SLIDE_IDX) {
			// idxを座標値に変更
			long val = 1;
			if (!text_ctrl->GetValue().ToLong(&val)) {
				// 値がおかしい場合は元の値に戻す
				text_ctrl->SetValue(wxString::Format("%d", ConvertPosCoord2Idx(m_position[axis], axis)));
			}
			s32 idx_pos = static_cast<int>(val);
			// 範囲制限
			s32 range_max = static_cast<s32>((m_bbox[1][axis] - m_bbox[0][axis]) / m_ctrl->GetPitch(axis));
			if (idx_pos <= 0) idx_pos = 0;
			if (idx_pos >= range_max) idx_pos = range_max - 1;
			// 値を入れなおす
			text_ctrl->SetValue(wxString::Format("%d", idx_pos));

			m_position[axis] = ConvertPosIdx2Coord(idx_pos, axis);
		} else {
			// そのまま代入する
			double val;
			if (!text_ctrl->GetValue().ToDouble(&val)) {
				// 値がおかしい場合は元の値に戻す
				text_ctrl->SetValue(wxString::Format("%.6f", m_position[axis]));
			}
			f32 coord_pos = static_cast<f32>(val);
			// 範囲制限
			if (coord_pos < m_bbox[0][axis]) coord_pos = m_bbox[0][axis];
			if (coord_pos > m_bbox[1][axis]) coord_pos = m_bbox[1][axis];
			// 値を入れなおす
			text_ctrl->SetValue(wxString::Format("%.6f", coord_pos));
			m_position[axis] = coord_pos;
		}		
	}

	// Pitchテキストからm_spinPitchのを設定
	void SliceCtrlDlg::SetPitchValue(u32 axis, u32 unit)
	{
		if(!m_ctrl) return;

		wxTextCtrl *text_ctrl = 0; // 設定するテキストコントロール
		switch (axis) {
		default:
			// 指定した軸ではない場合は終了
			break;
		case 0: // X軸
			text_ctrl = m_pXPitchTxt;
			break;
		case 1: // Y軸
			text_ctrl = m_pYPitchTxt;
			break;
		case 2: // Z軸
			text_ctrl = m_pZPitchTxt;
			break;
		}
		if (unit == SliceController::SLIDE_IDX) {
			// idxを座標値に変更
			long val = 1;
			if (!text_ctrl->GetValue().ToLong(&val)) {
				// 値がおかしい場合は元の値に戻す
				text_ctrl->SetValue(wxString::Format("%d", ConvertPitchCoord2Idx(m_spinPitch[axis], axis)));
			}
			s32 idx_pitch = static_cast<s32>(val);
			// 範囲制限
			s32 range_max = static_cast<s32>((m_bbox[1][axis] - m_bbox[0][axis]) / m_ctrl->GetPitch(axis));
			if (idx_pitch <= 0) idx_pitch = 1;
			if (idx_pitch > range_max) idx_pitch = range_max;
			// 値を入れなおす
			text_ctrl->SetValue(wxString::Format("%d", idx_pitch));

			m_spinPitch[axis] = ConvertPitchIdx2Coord(idx_pitch, axis);
		} else {
			// そのまま代入する
			double val;
			if (!text_ctrl->GetValue().ToDouble(&val)) {
				// 値がおかしい場合は元の値に戻す
				text_ctrl->SetValue(wxString::Format("%.6f", m_spinPitch[axis]));
			}
			f32 coord_pitch = static_cast<f32>(val);
			// 範囲制限
			if (coord_pitch < 0) coord_pitch = 0;
			if (coord_pitch > (m_bbox[1][axis] - m_bbox[0][axis])) coord_pitch = m_bbox[1][axis] - m_bbox[0][axis];
			// 値を入れなおす
			text_ctrl->SetValue(wxString::Format("%.6f", coord_pitch));
			m_spinPitch[axis] = coord_pitch;
		}
		m_ctrl->SetSpinPitch(m_spinPitch[axis], axis);
	}

	// Pitchの表示を切り替える
	void SliceCtrlDlg::SetPitchTxt(u32 newunit)
	{
		if(!m_ctrl) return;

		if (newunit == SliceController::SLIDE_IDX) {
			// 座標値をidx値に変更
			u32 newp[3];
			for (int i = 0; i < 3; i++) {
				newp[i] = ConvertPitchCoord2Idx(m_spinPitch[i], i);
				m_spinPitch[i] = ConvertPitchIdx2Coord(newp[i], i);
			}
			m_pXPitchTxt->SetValue(wxString::Format("%d", newp[0]));
			m_pYPitchTxt->SetValue(wxString::Format("%d", newp[1]));
			m_pZPitchTxt->SetValue(wxString::Format("%d", newp[2]));
		} else {
			// 座標値で管理する場合はそのまま書く
			for (int i = 0; i < 3; i++) {
				m_spinPitch[i] = m_ctrl->GetSpinPitch(i);
			}
			m_pXPitchTxt->SetValue(wxString::Format("%.6f", m_spinPitch[0]));
			m_pYPitchTxt->SetValue(wxString::Format("%.6f", m_spinPitch[1]));
			m_pZPitchTxt->SetValue(wxString::Format("%.6f", m_spinPitch[2]));
		}
	}

	// 座標値で持っているpitchの値をIndex値に変更
	u32 SliceCtrlDlg::ConvertPitchCoord2Idx(f32 pitch, u32 axis)
	{
		s32 idx_pitch = static_cast<s32>(pitch / m_ctrl->GetPitch(axis));
		// 範囲制限
		s32 range_max = static_cast<s32>((m_bbox[1][axis] - m_bbox[0][axis]) / m_ctrl->GetPitch(axis));
		if (idx_pitch <= 0) idx_pitch = 1;
		if (idx_pitch > range_max) idx_pitch = range_max;
		return idx_pitch;
	}

	// Index値で持っているpitchの値を座標値に変更
	f32 SliceCtrlDlg::ConvertPitchIdx2Coord(u32 pitch, u32 axis)
	{
		f32 coord_pitch = static_cast<f32>(pitch * m_ctrl->GetPitch(axis));
		// 範囲制限
		if (coord_pitch < 0) coord_pitch = 0;
		if (coord_pitch > (m_bbox[1][axis] - m_bbox[0][axis])) coord_pitch = m_bbox[1][axis] - m_bbox[0][axis];
		return coord_pitch;
	}

	// 座標値で持っているPositionの値をIndex値に変更
	u32 SliceCtrlDlg::ConvertPosCoord2Idx(f32 pos, u32 axis)
	{
		return m_ctrl->GetIdxPosition(axis, pos);
	}

	// Index値で持っているPositionの値を座標値に変更
	f32 SliceCtrlDlg::ConvertPosIdx2Coord(u32 pos, u32 axis)
	{
		return m_ctrl->GetCoordPosition(axis, pos) + m_ctrl->GetPitch(axis) * 0.5f;
	}

	// セル幅(座標値)を取得
	f32 SliceCtrlDlg::GetCoordPitch(const u32 axis) const 
	{
		return m_ctrl->GetPitch(axis);
	}
} // namespace U

