/*
 *
 * MainWindow.cpp
 * 
 */

//
// You can't use "../VX/VX.h". because vxnew conflicts with wx.
//

#include "DialogUtil.h"
#include <wx/wx.h>
#include <wx/clipbrd.h>
#include <wx/colordlg.h>

#include "vxWindow.h"
#include "fxgenGUIExportSTLDlg.h"
#include "DomainCartDlg.h"
#include "DomainBCMDlg.h"
#include "SliceCtrlDlg.h"

#include "fxgenGUIFreeRotateDlg.h"
#include "fxgenGUIBCMediumManagerDlgBase.h"
#include "fxgenGUIBCMediumManagerDlgBase2.h"
#include "fxgenGUILocalBCSettingDlgBase.h"
#include "fxgenGUILocalBCSettingDlgBase2.h"
#include "fxgenGUIOuterBCSettingDlgBase.h"
#include "fxgenGUISubdomainSettingDlgBase.h"

#include "fxgenGUICoordinateViewerFrame.h"


#include "fxgenGUISDImportDlgBase.h"//modal
#include "fxgenGUISD_Frame.h"


#define APPLICATION "FXgen"
#define VERSION     "1.2.13"
#define BUILD       "2015/10/27"
//#define COPYRIGHT   "(c) 2015 University of Tokyo,\nInstitute of Industrial Science"
#define COPYRIGHT   "\n(c) 2012-2015 University of Tokyo,\nInstitute of Industrial Science\n(c) 2014-2015 RIKEN,\nAdvanced Institute for Computational Science"


//#include "vxNodeDialog.h"
//
namespace 
{

	UI::fxgenGUIExportSTLDlg  *exportSTLDlg      = NULL;
	UI::fxgenGUIFreeRotateDlg *freeRotateDlg     = NULL;
	UI::DomainCartDlg         *domainCartDlg     = NULL;
	UI::DomainBCMDlg          *domainBCMDlg      = NULL;
	UI::SliceCtrlDlg		  *sliceCtrlDlg      = NULL;
	fxgenGUISD_Frame          *voxelCartesianDlg = NULL;
} // namespace;

// Dialogs for V-Xgen
namespace UI
{

	wxWindow* GetTopWnd()
	{
		wxWindow* wnd = UI::vxWindow::GetTopWindow()->GetWX();
		assert(wnd!=NULL);
		return wnd;
	}

	bool FileOpenDlg(std::string& filename, const std::string& extstr,const std::string& defaultpath)
	{
		wxString wxfilename = wxFileSelector(wxT("Open File"), defaultpath.c_str(), wxT(""), wxT(""),
											 extstr.c_str(), wxFD_OPEN,GetTopWnd());


		if( wxfilename.IsEmpty() )
		{
			return false;
		}

		filename = wxfilename;
		return true;

	}

	bool MultipleFileOpenDlg(std::string& filename, const std::string& extstr,std::vector<std::string>& files)
	{
		files.clear();
		wxArrayString paths, filenames;

		wxFileDialog openFileDialog(GetTopWnd(),wxT("Open file"), wxT(""), wxT(""),extstr.c_str(),
		                  wxFD_OPEN|wxFD_MULTIPLE);



        if(openFileDialog.ShowModal() == wxID_OK)
        {
            openFileDialog.GetPaths(paths);
            openFileDialog.GetFilenames(filenames);
			for(int i=0;i<filenames.size();i++){
				files.push_back(paths.Item(i).ToStdString());
			}
			return true;
		}
		else
			return false;
	}

	bool FileSaveDlg(std::string& filename, const std::string& extstr)
	{
		wxString wxfilename = wxFileSelector(wxT("Save File"), wxT(""), wxT(""), wxT(""),
											 extstr.c_str(), wxFD_SAVE|wxFD_OVERWRITE_PROMPT,GetTopWnd());
		if( wxfilename.IsEmpty() )
		{
			return false;
		}
		
		filename = wxfilename;
		return true;
	}


	bool DirOpenDlg(std::string& dirname,const std::string& defaultPath)
	{
		const wxString& dir = wxDirSelector("Choose a folder",
											defaultPath,
											wxDD_DEFAULT_STYLE,
											wxDefaultPosition,
											GetTopWnd());
		if ( dir.empty() )
		{
			return false;
		}
		
		dirname = dir;
		return true;
	}

	bool DirSaveDlg(std::string& dirname,const std::string& defaultPath)
	{
		const wxString& dir = wxDirSelector("Choose a save folder",
											defaultPath,
											wxDD_DEFAULT_STYLE,
											wxDefaultPosition,
											GetTopWnd()
											);
		if ( dir.empty() )
		{
			return false;
		}
		
		dirname = dir;
		return true;
	}

	void ShowInfoDlg()
	{
		char title[128];
		char contents[256];
#ifdef MACOS
		sprintf(title, "%s", APPLICATION);
		sprintf(contents, "Version:%s  Build:%s\n\nCopyright %s", VERSION, BUILD, COPYRIGHT);
#else
		sprintf(title, "About %s", APPLICATION);
		sprintf(contents, "%s\n\nVersion:%s  Build:%s\n\nCopyright %s", APPLICATION, VERSION, BUILD, COPYRIGHT);
#endif
		wxMessageBox(contents, title, wxOK|wxICON_INFORMATION,GetTopWnd());
	}

	void MessageDlg(const char* msg)
	{
		wxMessageBox(msg,"Message",wxOK, GetTopWnd() );
	}
	void ErrMessageDlg(const char* msg)
	{
		wxMessageBox(msg, "Error", wxOK|wxICON_ERROR, GetTopWnd());
	}
		
	bool ConfirmDlg(const char* msg)
	{
		if (wxMessageBox(msg, "Confirm", wxOK|wxCANCEL|wxCANCEL_DEFAULT|wxCENTRE,GetTopWnd()) == wxOK)
        
            return true;
		else
			return false;
	}

	void Exit()
	{
		//wxApp::GetInstance()->Exit();
		exit(0);
	}
	
	void ExportSTLDlg(vxWindow* win, bool show, bool modeDir, fxgenCore* core, GUIController* gui)
	{
		if(!exportSTLDlg && !show){
			return;
		}

		if(!exportSTLDlg && show){
			exportSTLDlg = new UI::fxgenGUIExportSTLDlg(win->GetWX(), core, gui);
		}
		exportSTLDlg->SetModeDir(modeDir);
		exportSTLDlg->Show(show);
	}

	void FreeRotateDlg(vxWindow* win, bool show, ViewController* vctrl)
	{
		if(!freeRotateDlg && !show){
			return;
		}

		if(!freeRotateDlg && show){
			freeRotateDlg = new UI::fxgenGUIFreeRotateDlg(win->GetWX(), vctrl);
		}
		freeRotateDlg->Show(show);
	}

	void DomainCartesianDlg(vxWindow* win, DomainCartesianEditor *editor, bool show)
	{
		if(!domainCartDlg && !show){
			return;
		}

		if(!domainCartDlg && show){
			domainCartDlg = new UI::DomainCartDlg(win->GetWX());
		}
		domainCartDlg->SetEditor(editor);		
		domainCartDlg->Show(show);
	}

	void NotifySelectionPick()
	{
		if(voxelCartesianDlg!=NULL && voxelCartesianDlg->IsShown()){
			voxelCartesianDlg->NotifySelectionPick();
		}
	}

	void Notify_EyeStatus()
	{
		if(voxelCartesianDlg!=NULL && voxelCartesianDlg->IsShown()){
			voxelCartesianDlg->Notify_EyeStatus();
		}

	}


	void VoxelCartesian_Dlg(		vxWindow* win, 
								VoxelCartesianEditor *editor, 
								GUIController* gui,
								fxgenCore* core,
								bool show)
	{
		if(!voxelCartesianDlg && !show){
			return;
		}

		if(!voxelCartesianDlg && show){
			voxelCartesianDlg = new fxgenGUISD_Frame(win->GetWX(), gui,core);
		}
		voxelCartesianDlg->SetEditor(editor);
		voxelCartesianDlg->Show(show);
	}

	void Domain_BCM_Dlg(vxWindow* win, DomainBCMEditor *editor, GUIController* gui, bool show)
	{
		if(!domainBCMDlg && !show){
			return;
		}

		if(!domainBCMDlg && show){
			domainBCMDlg = new UI::DomainBCMDlg(win->GetWX(), gui);
		}
		domainBCMDlg->SetEditor(editor);
		domainBCMDlg->Show(show);
	}

	void SliceControllerDlg(vxWindow* win, SliceController *controller, bool show)
	{
		if(!sliceCtrlDlg && !show){
			return;
		}
		if(!sliceCtrlDlg && show){
			sliceCtrlDlg = new UI::SliceCtrlDlg(win->GetWX());
		}
		sliceCtrlDlg->SetController(controller);
		UpdateSliceControllerDlg();
		sliceCtrlDlg->Show(show);
	}


	int BCMedDlgModal(vxWindow* win, BCMedFunc& func)
	{
		fxgenGUIBCMediumManagerDlgBase* bcmedManagerDlg = new fxgenGUIBCMediumManagerDlgBase(win->GetWX(), func);
		int ret = bcmedManagerDlg->ShowModal();
		bcmedManagerDlg->Destroy();
		return ret;
	}

	b8 BCMedDlgModal2(vxWindow* win, BCMedFunc& func, fxgenCore* core)
	{
		fxgenGUIBCMediumManagerDlgBase2* bcmedManagerDlg = new fxgenGUIBCMediumManagerDlgBase2(win->GetWX(), func, core);
		b8 ret = bcmedManagerDlg->Show();
		return ret;
	}

	b8 ColorDlg(vxWindow* win,  VX::Math::vec4& color)
	{
		wxColourDialog coldlg(win->GetWX());
		
		wxColour col(color.r * 255.0f ,color.g*255.0f, color.b*255.0f,color.a*255.0f);

	
		coldlg.GetColourData().SetChooseFull(true);
		coldlg.GetColourData().GetColour() = col;
		coldlg.SetTitle(_T("Choose color.."));
		if( coldlg.ShowModal() != wxID_OK ){
			return false;
		}
		wxColourData cdata = coldlg.GetColourData();
		wxColour rcol = cdata.GetColour();

		color.r =rcol.Red()/255.0f; 
		color.g =rcol.Green()/255.0f;
		color.b =rcol.Blue()/255.0f;
		color.a =rcol.Alpha()/255.0f;

		return true;
	}

	b8 CoordinateViewer_DlgModal(vxWindow* win, fxgenCore* core, ViewController* vctrl, GUIController* gui)
	{
		fxgenGUICoordinateViewerFrame* coordinateViewerDlg = new fxgenGUICoordinateViewerFrame(win->GetWX(),core,vctrl,gui);
		b8 ret = coordinateViewerDlg->Show(true);
		return ret;
	}

	///////////////////////////////////////////////////////

	int SDImportDlgModal(vxWindow* win, VoxelCartesianParam& info)
	{
		fxgenGUISDImportDlgBase* dlg = new fxgenGUISDImportDlgBase(win->GetWX());
		int ret = dlg->ShowModal();
		info = dlg->GetResult();
		dlg->Destroy();
		return ret;
	}

/******************************************************************************/
/*! @brief open BC setting dialog
******************************************************************************/
	int LocalBCSettingDlgModal(vxWindow* win, BCMedFunc& func)
	{
		const int lbc = func.GetLocalBCNum();
		const int med = func.GetMediumNum();
		if (lbc + med == 0) {
			UI::MessageDlg("No LocalBC/Medium.");
			return 0;
		}
		fxgenGUILocalBCSettingDlgBase* localbcDlg = new fxgenGUILocalBCSettingDlgBase(win->GetWX(), func);
		int ret = localbcDlg->ShowModal();
		localbcDlg->Destroy();
		return ret;
	}
	
	b8 LocalBCSettingDlgModal2(vxWindow* win, BCMedFunc& func, fxgenCore* core, GUIController* gui)
	{
		const int lbc = func.GetLocalBCNum();
		const int med = func.GetMediumNum();
		if (lbc + med == 0) {
			UI::MessageDlg("No LocalBC/Medium.");
			return 0;
		}
		fxgenGUILocalBCSettingDlgBase2* localbcDlg = new fxgenGUILocalBCSettingDlgBase2(win->GetWX(), func, core , gui);
		b8 ret = localbcDlg->Show();
		return ret;
	}

	int OuterBCSettingDlgModal(vxWindow* win, DomainEditor *editor)
	{
		fxgenGUIOuterBCSettingDlgBase* outerbcDlg = new fxgenGUIOuterBCSettingDlgBase(win->GetWX(), editor);
		int ret = outerbcDlg->ShowModal();
		outerbcDlg->Destroy();
		return ret;
	}
	
	int SubdomainSettingDlgModal(vxWindow* win, UI::SubdomainFunc& func)
	{
		fxgenGUISubdomainSettingDlgBase* subDlg = new fxgenGUISubdomainSettingDlgBase(win->GetWX(), func);
		int ret = subDlg->ShowModal();
		subDlg->Destroy();
		return ret;
	}

	//clipboard
	void SetClipboard(const std::string& msg)
	{

		  // Write some text to the clipboard
		if (wxTheClipboard->Open())
		{
			// This data objects are held by the clipboard,
			// so do not delete them in the app.
			wxString wstr( msg );

			wxTheClipboard->SetData( new wxTextDataObject(wstr) );
			wxTheClipboard->Close();
		}
	}

	std::string GetClipboard()
	{
		std::string ret="";
		if (!wxTheClipboard->Open()){
			return ret;
		}

		if (wxTheClipboard->IsSupported( wxDF_TEXT ))
		{
			wxTextDataObject data;
			wxTheClipboard->GetData( data );
			wxString wxs =  data.GetText() ;
			ret = wxs.ToStdString();
		}
		wxTheClipboard->Close();
		return ret;
	}

	void UpdateSliceControllerDlg()
	{
		if (!sliceCtrlDlg || !sliceCtrlDlg->IsVisible()) return;
		sliceCtrlDlg->UpdateControllerView();
	}

} // namespace VX
