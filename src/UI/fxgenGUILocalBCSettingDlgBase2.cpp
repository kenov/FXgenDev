#include "fxgenGUILocalBCSettingDlgBase2.h"

#include "DialogUtil.h"
#include "../VX/SG/LocalBC.h"
#include "../VX/SG/LocalBCClass.h"
#include "../VX/SG/Medium.h"
#include "../Core/fxgenCore.h"
#include "../Core/GUIController.h"


#include <algorithm>

namespace 
{

	wxColour getWXColor(const VX::Math::vec4& col)
	{
		return wxColour(col.r*255, col.g*255, col.b*255);
	}
	
	
	struct BCMedList
	{
		BCMedList(const std::string& alias_, const wxColor& col_, int id_, const std::string& kind_, const std::string& type_, void* ptr_)
		{
			alias = alias_;
			col   = col_;
			id    = id_;
			kind  = kind_;
			type  = type_;
			ptr   = ptr_;
		}
		bool operator<(const BCMedList& r) const
		{
			return id < r.id;
		}
		std::string alias;
		wxColour col;
		int id;
		std::string kind;
		std::string type;
		void* ptr;
	};
};


namespace UI
{
fxgenGUILocalBCSettingDlgBase2::fxgenGUILocalBCSettingDlgBase2( wxWindow* parent, UI::BCMedFunc& func ,fxgenCore* core , GUIController* gui)
:
LocalBCSettingDlgBase2( parent ),
m_core(core), m_gui(gui)
{
	fxgenBCMedFunc& p = static_cast<fxgenBCMedFunc&>(func);
	// copy
	// , due to modeless windows , not to be called destrctor of func from caller function.
	m_func = p;

	m_pBCMedGrid->SetSelectionMode(wxGrid::wxGridSelectRows);
	m_pBCMedGrid->SetSelectionForeground(wxColour(204,102,0));
	m_pBCMedGrid->SetSelectionBackground(wxColour(72,31,0));

	std::vector<BCMedList> bcmlist;
	
	const int lbc = func.GetLocalBCNum();
	for (int i = 0; i < lbc; ++i){
		bcmlist.push_back(BCMedList(func.GetLocalBC(i)->GetAlias().c_str(),getWXColor(func.GetLocalBC(i)->GetColor()), func.GetLocalBC(i)->GetID(), "LocalBC", func.GetLocalBC(i)->GetClass()->GetLabel(), static_cast<void*>(const_cast<VX::SG::LocalBC*>(func.GetLocalBC(i)))));
	}
	const int med = func.GetMediumNum();
	for (int i = 0; i < med; ++i){
		bcmlist.push_back(BCMedList(func.GetMedium(i)->GetLabel().c_str(),getWXColor(func.GetMedium(i)->GetColor()), func.GetMedium(i)->GetID(), "Medium", func.GetMedium(i)->GetMediumType(), static_cast<void*>(const_cast<VX::SG::Medium*>(func.GetMedium(i)))));
	}
	
	std::sort(bcmlist.begin(), bcmlist.end());
	for (size_t i = 0; i < bcmlist.size(); ++i)
	{
		Append(bcmlist[i].alias, bcmlist[i].col, bcmlist[i].id, bcmlist[i].kind, bcmlist[i].type, bcmlist[i].ptr);
	}
/*	const int lbc = func.GetLocalBCNum();
	for (int i = 0; i < lbc; ++i){
		Append(func.GetLocalBC(i)->GetAlias().c_str(),getWXColor(func.GetLocalBC(i)->GetColor()), func.GetLocalBC(i)->GetID(), "LocalBC", static_cast<void*>(const_cast<VX::SG::LocalBC*>(func.GetLocalBC(i))));
	}
	const int med = func.GetMediumNum();
	for (int i = 0; i < med; ++i){
		Append(func.GetMedium(i)->GetLabel().c_str(),getWXColor(func.GetMedium(i)->GetColor()), func.GetMedium(i)->GetID(), "Medium", static_cast<void*>(const_cast<VX::SG::Medium*>(func.GetMedium(i))));
	}*/
	m_pBCMedGrid->SelectRow(0);
	this->Fit();
	m_pOKBtn->SetDefault();

	hydeIDcol();
}

/**
*	@brief hyde id colum
*/
void fxgenGUILocalBCSettingDlgBase2::hydeIDcol()
{
//#ifndef _DEBUG
	m_pBCMedGrid->HideCol(2);
//#endif
}

void fxgenGUILocalBCSettingDlgBase2::Append(const std::string& alias, const wxColour& col, const int ID, const std::string& kind, const std::string& type, void* ptr)
{
	m_pBCMedGrid->AppendRows();
	const int lastrow = m_pBCMedGrid->GetRows() - 1;
		
	// Color
	m_pBCMedGrid->SetReadOnly(lastrow, 1, true);
	m_pBCMedGrid->SetCellBackgroundColour(lastrow, 1, col);
	m_pBCMedGrid->SetCellValue(lastrow, 1, "");
	
	m_pBCMedGrid->SetCellValue(lastrow, 0, alias);
	m_pBCMedGrid->SetCellValue(lastrow, 2, wxString::Format("%d",ID));
	m_pBCMedGrid->SetCellValue(lastrow, 3, kind);
	m_pBCMedGrid->SetCellValue(lastrow, 4, type);
	
	m_nodelist.push_back(ptr);
	
/*	for(int i = 0; i < 4; i++)
	{
		if (i == 1)
			continue; // col
		
		wxGridCellAttr* attr = new wxGridCellAttr();
		attr->SetEditor(celeditor[i]);
		m_pBCMedGrid->SetColAttr(i, attr);
	}*/
}

void fxgenGUILocalBCSettingDlgBase2::OnBCMListRangeSelect( wxGridRangeSelectEvent& event )
{
	//printf("RANGE SELECT\n");
	wxArrayInt ar = m_pBCMedGrid->GetSelectedRows();
	const int n = static_cast<int>(ar.GetCount());
	if (n > 1) {
		const int selrow = ar[0];
		m_pBCMedGrid->ClearSelection();
		m_pBCMedGrid->SelectRow(selrow);
	}
}

void fxgenGUILocalBCSettingDlgBase2::OnBCMedChoice( wxCommandEvent& event )
{
// TODO: Implement OnBCMedChoice
}

void fxgenGUILocalBCSettingDlgBase2::OnOKBtn( wxCommandEvent& event )
{
	wxArrayInt ar = m_pBCMedGrid->GetSelectedRows();
	const int n = static_cast<int>(ar.GetCount());
	if (!n){
		UI::MessageDlg("select row in grid");
		//assert(0);
		return;
	}
	const int selrow = ar[0];
	
	void* p = static_cast<wxGridCellTextEditor*>(m_nodelist[selrow]);
	VX::SG::Node* node = static_cast<VX::SG::Node*>(p);
	if (node) {
		node->Ref();
	}

	m_func.ClearLocalBC();
	m_func.ClearMedium();
	m_func.ClearOuterBC();
	if (node) {
		if (node->GetType() == VX::SG::NODETYPE_LOCALBC)
			m_func.AddLocalBC(static_cast<VX::SG::LocalBC*>(node));
		else if (node->GetType() == VX::SG::NODETYPE_MEDIUM)
			m_func.AddMedium(static_cast<VX::SG::Medium*>(node));
		node->Unref();
	}
	
	//not close
	//EndDialog(1);

	ApplyToMainWnd();

}

void fxgenGUILocalBCSettingDlgBase2::OnCancelBtn( wxCommandEvent& event )
{
	EndDialog(0);
	Destroy();
}

void fxgenGUILocalBCSettingDlgBase2::ApplyToMainWnd()
{

	const int lbc = m_func.GetLocalBCNum();
	const int med = m_func.GetMediumNum();
	if (lbc) {
		m_core->SelectionSetID(m_func.GetLocalBC(0)->GetID());
	} else if (med) {
		m_core->SelectionSetID(m_func.GetMedium(0)->GetID());
	}

	repaintMainWnd();
}

void fxgenGUILocalBCSettingDlgBase2::repaintMainWnd()
{
	// tree icon update
	m_gui->UpdateTree(false);
}

}