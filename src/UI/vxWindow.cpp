/*
 *
 * vxWindow.cpp
 * 
 * vxWindow and MainView & MainFrame implimentation
 *
 * vxWindow is wrapper class for wxWidgets
 * MainFrame has MainView.
 */

//
// You can't use "../VX/VX.h". because vxnew conflicts with wx.
//

#include "../UILogic/BaseLogic.h"
#include "../VX/Log.h"

#include <wx/wx.h>
#include <wx/glcanvas.h>

#include <string>
#include <map>
#include "vxWindow.h"



// ----------------------------------------
// MainView
// ----------------------------------------
	
class MainView : public wxGLCanvas
{
public:
	MainView(wxWindow *parent, wxSize size);
	~MainView();
	void Draw();
	
	void OnPaint(wxPaintEvent &pe);
	void OnSize(wxSizeEvent &se);
	void OnMouse(wxMouseEvent &me);
	void OnIdle(wxIdleEvent &ie);
	void OnErase(wxEraseEvent &ee);
	void OnKeyDown(wxKeyEvent &ke);
	void OnKeyUp(wxKeyEvent &ke);

	void StartIdleloop();
	
	void SetContext();
	void SetLogic(BaseLogic* logic);
	
private:
	
	wxGLContext* m_context;
	BaseLogic*   m_baseLogic;
	
	int m_mouse_btn;
	int m_mouse_x;
	int m_mouse_y;
	
	DECLARE_EVENT_TABLE()
};

BEGIN_EVENT_TABLE(MainView, wxGLCanvas)
	EVT_SIZE(MainView::OnSize)
	EVT_PAINT(MainView::OnPaint)
	EVT_ERASE_BACKGROUND(MainView::OnErase)
	EVT_MOUSE_EVENTS(MainView::OnMouse)
	EVT_KEY_DOWN(MainView::OnKeyDown)
	EVT_KEY_UP(MainView::OnKeyUp)
	//EVT_IDLE(MainView::OnIdle)
END_EVENT_TABLE()

MainView::MainView(wxWindow *parent, wxSize size)
    : wxGLCanvas(parent, wxID_ANY, NULL, wxDefaultPosition, 
				 size, wxDEFAULT_FRAME_STYLE, wxT("GLCanvas"))
{
	m_mouse_btn = 0;
	m_baseLogic = 0;

	m_context = new wxGLContext(this);
	//SetCurrent(*m_context);
	this->Connect( wxEVT_IDLE, wxIdleEventHandler( MainView::OnIdle ), NULL, this );
	this->Disconnect( wxEVT_IDLE, wxIdleEventHandler( MainView::OnIdle ), NULL, this );
}

MainView::~MainView()
{
	//delete m_context;
}


void MainView::SetContext()
{
	if(!IsShownOnScreen()) return;
	if(!m_context) return;
	SetCurrent(*m_context);
}

void MainView::SetLogic(BaseLogic* logic)
{
	wxSize size = GetClientSize();
	m_baseLogic = logic;
	if (m_baseLogic)
	{
		m_baseLogic->OnResize(size.x, size.y);
		m_baseLogic->Draw();
		SwapBuffers();
	}
}

void MainView::Draw()
{
	if(!IsShownOnScreen()) return;
	SetCurrent(*m_context);
	if (m_baseLogic)
	{
		m_baseLogic->Draw();
		SwapBuffers();
	}
}

void MainView::OnSize(wxSizeEvent& WXUNUSED(event))
{
	wxSize cs = GetClientSize();
	if (m_baseLogic)
		m_baseLogic->OnResize(cs.GetX(), cs.GetY());
	Draw();
}

void MainView::OnPaint(wxPaintEvent& WXUNUSED(event))
{
	wxPaintDC dc(this);
	Draw();
}

void MainView::OnErase(wxEraseEvent& WXUNUSED(event))
{
	return; /* to do nothing */
}

void MainView::OnKeyDown(wxKeyEvent &ke)
{
    VXLogD("MainView::OnKeyDown ke.GetKeyCode[%d]\n",ke.GetKeyCode());
    
	if (m_baseLogic)
		m_baseLogic->OnKeyDown(ke.GetKeyCode());
	Draw();
}

void MainView::OnKeyUp(wxKeyEvent &ke)
{
	if (m_baseLogic)
		m_baseLogic->OnKeyUp(ke.GetKeyCode());
	Draw();
}


void MainView::OnMouse(wxMouseEvent &me)
{
	wxPoint pt = me.GetPosition();
	const int x = pt.x;
	const int y = pt.y;

	// Move
	if (m_mouse_x != x || m_mouse_x != y)
	{
		if (m_baseLogic)
			m_baseLogic->OnMouseMove(x, y);
	}
	m_mouse_x = x;
	m_mouse_x = y;

	// Down
	if (me.LeftDown() || me.LeftDClick())
	{
		m_mouse_btn |= BaseLogic::MOUSE_LEFT;
		if (m_baseLogic)
			m_baseLogic->OnMouseLeftDown(x, y);
	}
	if (me.RightDown())
	{
		m_mouse_btn |= BaseLogic::MOUSE_RIGHT;
		if (m_baseLogic)
			m_baseLogic->OnMouseRightDown(x, y);
	}
	if (me.MiddleDown())
	{
		m_mouse_btn |= BaseLogic::MOUSE_MIDDLE;
		if (m_baseLogic)
			m_baseLogic->OnMouseMiddleDown(x, y);
	}
	if (m_mouse_btn)
	{
		if (!HasCapture())
			CaptureMouse();
	}
	
	// Up
	if (me.LeftUp())
		m_mouse_btn &= ~BaseLogic::MOUSE_LEFT;
	if (me.RightUp())
		m_mouse_btn &= ~BaseLogic::MOUSE_RIGHT;
	if (me.MiddleUp())
		m_mouse_btn &= ~BaseLogic::MOUSE_MIDDLE;
	
	// CaptureRelease
	if (!m_mouse_btn)
	{
		if (HasCapture())
			ReleaseMouse();
	}

	// Up Event
	if (me.LeftUp())
	{
		if (m_baseLogic)
			m_baseLogic->OnMouseLeftUp(x, y);
	}
	if (me.RightUp())
	{
		if (m_baseLogic)
			m_baseLogic->OnMouseRightUp(x, y);
	}
	if (me.MiddleUp())
	{
		if (m_baseLogic)
			m_baseLogic->OnMouseMiddleUp(x, y);
	}
	
	
	if (m_baseLogic)
	{
		m_baseLogic->OnMouse(x, y, m_mouse_btn);
		if (me.LeftDClick())
			m_baseLogic->OnMouseLeftDblClick(x, y);
		if (me.RightDClick())
			m_baseLogic->OnMouseRightDblClick(x, y);
		if (me.MiddleDClick())
			m_baseLogic->OnMouseMiddleDblClick(x, y);
	
		const int w = me.GetWheelRotation();
		if (w != 0)
			m_baseLogic->OnMouseWheel(x, y, w / me.GetWheelDelta());
	}
	Draw();
}

void MainView::OnIdle(wxIdleEvent &ie)
{
	if (m_baseLogic->OnIdle()) {
		Draw();
		ie.RequestMore(true);// force to call idleFunction
	} else {
		this->Disconnect( wxEVT_IDLE, wxIdleEventHandler( MainView::OnIdle ), NULL, this ); // Disable Idle
	}
}

void MainView::StartIdleloop()
{
	this->Connect( wxEVT_IDLE, wxIdleEventHandler( MainView::OnIdle ), NULL, this ); // Enable Idle
}


// ----------------------------------------
// MainFrame
// ----------------------------------------


class MainFrame : public wxFrame
{
public:
	MainFrame(const std::string& title,
	          wxPoint p = wxDefaultPosition,
	          wxSize size = wxDefaultSize,
			  long style = wxDEFAULT_FRAME_STYLE );
	~MainFrame();
	
	void AddMenu(const std::string& menu_name);
	wxMenu* GetMenu(const std::string& menu_name);
	void SetLogic(BaseLogic* logic);
	void StartIdleloop();
	
	bool Show(bool show = true);

	void Draw();

	void MouseCaptureRelease();
	
private:
	MainView*  m_mainView;
	BaseLogic* m_logic;
	
	std::map<std::string, wxMenu*> m_menus;
	
	void onMenuFunction(wxCommandEvent& event);
	
	DECLARE_EVENT_TABLE()
};


const int vxMENUID_BASE = 10000;
const int maxMenuNum    = 10000;

BEGIN_EVENT_TABLE(MainFrame, wxFrame)
EVT_MENU_RANGE(vxMENUID_BASE, vxMENUID_BASE + maxMenuNum, MainFrame::onMenuFunction)
END_EVENT_TABLE()

MainFrame::MainFrame(const std::string& title, wxPoint p, wxSize size, long style)
: wxFrame(0, -1, title.c_str(), p, size, style)
{
	m_logic = 0;
	m_mainView = new MainView(this, size);
	m_mainView->SetFocus();

#if defined (_WIN32) || defined (_WIN64)
	SetIcon(wxIcon(wxT("FXgen.exe;0"), wxBITMAP_TYPE_ICO));
#endif
	//Show(true);
	SetClientSize(size.x, size.y);
}

void MainFrame::StartIdleloop()
{
	VXLogD("StartIdleloop\n");
    // this cause wrong issue. [Q] key lost.
	m_mainView->SetContext();
	
    m_mainView->StartIdleloop();
}

bool MainFrame::Show(bool show)
{
	if( wxFrame::Show(show) ){
		if( show ){
			m_mainView->SetContext();
		}
		return true;
	}
	return false;
}

void MainFrame::onMenuFunction(wxCommandEvent& event)
{
	int id = event.GetId();
	VXLogD("menu id=%d\n",id);
	if (id >= vxMENUID_BASE && m_logic)
		m_logic->OnMenu(id - vxMENUID_BASE);
	m_mainView->Draw();
}

wxMenu* MainFrame::GetMenu(const std::string& menu_name)
{
	return m_menus[menu_name];
}

void MainFrame::AddMenu(const std::string& menu_name)
{
	wxMenuBar *menuBar = GetMenuBar();
	if (!menuBar)
		menuBar = new wxMenuBar;
	
	wxMenu* newMenu = new wxMenu();
	menuBar->Append(newMenu, menu_name.c_str());
	SetMenuBar(menuBar);
	
	m_menus[menu_name] = newMenu;
}

void MainFrame::SetLogic(BaseLogic* logic)
{
	m_logic = logic;
	m_mainView->SetLogic(logic);
}

void MainFrame::Draw()
{
	m_mainView->Draw();
}

MainFrame::~MainFrame()
{



	m_mainView->Destroy();
}


void MainFrame::MouseCaptureRelease()
{
	if (m_mainView->HasCapture())
		m_mainView->ReleaseMouse();
}


// ----------------------------------------
// vxWindow
// ----------------------------------------
namespace UI {

vxWindow::vxWindow(const std::string& title, int sx, int sy, int w, int h)
{
	const int default_w = 1024;
	const int default_h = 600;
	if (w == -1)
		w = default_w;
	if (h == -1)
		h = default_h;
	m_mainframe = new MainFrame(title, wxPoint(sx, sy), wxSize(w,h)); // maybe autorelease
}

vxWindow::~vxWindow()
{
}

void vxWindow::SetLogic(BaseLogic *logic)
{
	m_mainframe->SetLogic(logic);
}


void vxWindow::AddMenu(const std::string& menu_name)
{
	m_mainframe->AddMenu(menu_name);
}

void vxWindow::AddSubMenu(const std::string& menu_name, int sub_menu_id, const std::string& sub_menu_name)
{
	wxMenu* menu = m_mainframe->GetMenu(menu_name);
	if (menu)
		menu->Append(vxMENUID_BASE + sub_menu_id, sub_menu_name);
}

void vxWindow::AddSubCheckMenu(const std::string& menu_name, int sub_menu_id, const std::string& sub_menu_name)
{
	wxMenu* menu = m_mainframe->GetMenu(menu_name);
	if (menu)
		menu->AppendCheckItem(vxMENUID_BASE + sub_menu_id, sub_menu_name);
}

void vxWindow::AddSubMenuSeparator(const std::string& menu_name)
{
	wxMenu* menu = m_mainframe->GetMenu(menu_name);
	if (menu)
		menu->AppendSeparator();	
}
	
void vxWindow::CheckMenu(const std::string& menu_name, int sub_menu_id, bool check)
{
	wxMenu* menu = m_mainframe->GetMenu(menu_name);
	if (menu)
		menu->Check(vxMENUID_BASE + sub_menu_id, check);
}
	
void vxWindow::EnableMenu(const std::string& menu_name, int sub_menu_id, bool enable)
{
	wxMenu* menu = m_mainframe->GetMenu(menu_name);
	if (menu)
		menu->Enable(vxMENUID_BASE + sub_menu_id, enable);
}


void vxWindow::Draw()
{
	m_mainframe->Draw();
}

void vxWindow::SetTopWindow()
{
	wxTheApp->SetTopWindow(m_mainframe);
	m_mainframe->Show();
	m_top = this;
}

void vxWindow::StartIdleloop()
{
	m_mainframe->StartIdleloop();
}
	
wxWindow* vxWindow::GetWX()
{
	return m_mainframe;
}

//
// static methods
//
vxWindow* vxWindow::m_top = 0;

vxWindow* vxWindow::GetTopWindow()
{
	return m_top;
}
	
} // namespace UI

