#include "fxgenGUISD_Frame.h"

#include "fxgenGUISD_ClippingPanel.h"
#include "fxgenGUISD_SelectPanel.h"
#include "fxgenGUISD_GlobalPanel.h"
#include "fxgenGUISD_LocalPanel.h"
#include "fxgenGUISD_BcflagPanel.h"

#include "../Core/GUIController.h"
#include "vxWindow.h"

#include "../VX/VX.h"


fxgenGUISD_Frame::fxgenGUISD_Frame( 
	wxWindow *parent, GUIController *gui,fxgenCore	*core )
	:SD_Frame( parent ), m_editor(NULL), m_gui(gui),m_core(core),
	m_ClippingPanel(NULL),m_SelectPanel(NULL),m_GlobalPanel(NULL),m_LocalPanel(NULL),m_BcflagPanel(NULL)
{


	//コンストラクタ  
	createPages(); 
}

void fxgenGUISD_Frame::m_notebookOnNotebookPageChanging( wxNotebookEvent& event )
{
	//何もしない
}

void fxgenGUISD_Frame::m_notebookOnNotebookPageChanged( wxNotebookEvent& event )
{

    // this function(getcurrentPage) is bug wrong . in mac , previous page is returned .
    //                          in win , now page is returned.
    //
	//wxWindow* w = m_notebook->GetCurrentPage();// <= NG!!
    

    int selectindex = event.GetSelection();
    wxWindow* w = m_notebook->GetPage(selectindex);
	//macosではページチェンジ時にページが表示されないバグがあるため明示的にshowする
    w->Show(true);
}


void fxgenGUISD_Frame::CreateMedBcflgGridTable()
{
	m_BcflagPanel->CreateMedBcflgGridTable();
}

void fxgenGUISD_Frame::ClearMedBcflgGridTable()
{
	m_BcflagPanel->ClearMedBcflgGridTable();
}

wxString fxgenGUISD_Frame::FloatFormat(const float& value)
{
	//計算したfloat値を
	//GUIの直接入力の６ケタ（暫定）に合わせて記載
	return wxString::Format("%.6f", value);
	//return wxString::Format("%.2f", value);

}


const std::vector<VX::SG::VoxelCartesianL*>& fxgenGUISD_Frame::Get_staging_locals()const
{
	return m_SelectPanel->Get_staging_locals();
}



void fxgenGUISD_Frame::NotifySelectionPick()
{
	
	m_ClippingPanel->NotifySelectionPick();
	m_SelectPanel->NotifySelectionPick();
	m_GlobalPanel->NotifySelectionPick();
	m_LocalPanel->NotifySelectionPick();
	m_BcflagPanel->NotifySelectionPick();

}

void fxgenGUISD_Frame::updateMainWnd()
{
	// tree icon update
	m_gui->UpdateTree(false);
	UI::vxWindow::GetTopWindow()->Draw();
}

void fxgenGUISD_Frame::createPages()
{

	//Frameのサイズ
	this->SetSize(410,750);

	//1. topの領域に　clip panelを設定
	{
		m_pTopSizer->Clear(true);
		m_ClippingPanel = new fxgenGUISD_ClippingPanel( this, m_gui,m_core,this );
		m_pTopSizer->Add( m_ClippingPanel, 1, wxALL|wxEXPAND,
						10 // 大き目にborderを指定 
						);
	}

	//プロパティページの設定
	//ページをクリア

	m_notebook->DeleteAllPages();

	//1ページ目（selectionの作成）
	{
		m_SelectPanel = new fxgenGUISD_SelectPanel(m_notebook,m_gui,m_core,this );
		m_notebook->AddPage( m_SelectPanel, wxT("Selection"), true );
	}
	//２ページ目(global / local の作成)
	{

		wxPanel* page = new wxPanel( m_notebook, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );

		wxBoxSizer* pageSizer =new wxBoxSizer( wxVERTICAL );

		// panelの構築 global / local
		m_GlobalPanel		= new fxgenGUISD_GlobalPanel(page,m_gui,m_core,this);
		m_LocalPanel		= new fxgenGUISD_LocalPanel(page,m_gui,m_core,this);

		pageSizer->Add( m_GlobalPanel, 0, wxEXPAND | wxALL, 5 );
		pageSizer->Add( m_LocalPanel, 0, wxEXPAND | wxALL, 5 );
	
		page->SetSizer( pageSizer );
		page->Layout();
		pageSizer->Fit( page );

		m_notebook->AddPage( page, wxT("Plane"), false );

	}
	//３ページ目の作成
	{
		m_BcflagPanel = new fxgenGUISD_BcflagPanel(m_notebook,m_gui,m_core,this );
		m_notebook->AddPage( m_BcflagPanel, wxT("Bcflag"), false );
	}

	m_notebook->ChangeSelection(0);

}

void fxgenGUISD_Frame::Notify_EyeStatus()
{
	//GUI resetting from SG node 
	// ex. check box status change on Clip,Global,Local
	m_ClippingPanel->Notify_EyeStatus();
	m_GlobalPanel->Notify_EyeStatus();
	m_LocalPanel->Notify_EyeStatus();

}

void fxgenGUISD_Frame::Show_inner()
{
	//data set event

	m_ClippingPanel->Show_inner();
	m_SelectPanel->Show_inner();
	m_GlobalPanel->Show_inner();
	m_LocalPanel->Show_inner();
	m_BcflagPanel->Show_inner();

}

 fxgenGUISD_Frame::~fxgenGUISD_Frame()
{

}


// Handlers for SD_Frame events.
void fxgenGUISD_Frame::OnActivate( wxActivateEvent& event )
{

	VXLogI("fxgenGUISD_Frame::OnActivate\n");
	event.Skip();
}

void fxgenGUISD_Frame::OnActivateApp( wxActivateEvent& event )
{

	VXLogI("fxgenGUISD_Frame::OnActivateApp\n");
	event.Skip();
}

void fxgenGUISD_Frame::OnClose( wxCloseEvent& event )
{

	VXLogI("fxgenGUISD_Frame::OnClose\n");
	//隠すだけ
	this->Show(false);

}

void fxgenGUISD_Frame::m_pCloseBtnOnButtonClick( wxCommandEvent& event )
{
	VXLogI("fxgenGUISD_Frame::m_pCloseBtnOnButtonClick\n");
	//TODO:fuchi skipが悪影響がないか後でチェック

	this->Show(false);

}

bool fxgenGUISD_Frame::Show(bool show)
{

	if(show)
	{
		Show_inner();
	}
	else
	{
		m_editor = NULL;
		stopTimers();
	}

	return wxFrame::Show(show);
}

void fxgenGUISD_Frame::stopTimers()
{
	m_ClippingPanel->stopTimers() ;
	m_GlobalPanel->stopTimers();
	m_LocalPanel->stopTimers();
}

void fxgenGUISD_Frame::SetEditor(VoxelCartesianEditor *editor)
{
	m_editor = editor;
}

int fxgenGUISD_Frame::GetFrameRate()const
{
	int v = m_playSpeedspinCtrl->GetValue();
	return v;
}

void fxgenGUISD_Frame::m_playSpeedspinCtrlOnSpinCtrl( wxSpinEvent& event )
{

}

void fxgenGUISD_Frame::m_playSpeedspinCtrlOnSpinCtrlText( wxCommandEvent& event )
{

}

bool fxgenGUISD_Frame::GetTextCtrlIndex( wxTextCtrl* text_ctrl , int& outval ,const bool& offset)
{
	long val = 1;
	if (!text_ctrl->GetValue().ToLong(&val)) {
		return false;
	}
	int OFFSET = (offset)?1:0;
	int index = static_cast<int>(val);	

	outval = index - OFFSET;
	return true;
}

void fxgenGUISD_Frame::SetTextCtrlIndex( wxTextCtrl* text_ctrl , const int& index, const bool& offset)
{
	int OFFSET = (offset)?1:0;
	int gui_idx_pos = index + OFFSET;
	text_ctrl->SetValue(wxString::Format("%d", gui_idx_pos));
}


