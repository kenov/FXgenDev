/*
 *
 * DialogUtil.h
 * 
 */

// Dialogs for V-Xgen

#ifndef _VX_DIALOG_UTIL_H_
#define _VX_DIALOG_UTIL_H_

#include <string>
#include "BCMedFunc.h"
#include "SubdomainFunc.h"
#include "NodePropertyFunc.h"
#include "../VX/Type.h"

#include <vector>

#include "../VX/Math.h"


class DomainCartesianEditor;
class DomainBCMEditor;
class VoxelCartesianEditor;
class DomainEditor;
class SliceController;
class GUIController;
class fxgenCore;
class VoxelCartesianParam;
class ViewController;
class fxgenCore;
class wxWindow;
namespace VX{
	namespace SG{
		class LocalBC;
		class OuterBC;
		class LocalBCClass;
		class OuterBCClass;
		class Medium;
	}
} // namespace VX

namespace UI{
	class vxWindow;
	
}

namespace UI
{
	wxWindow* GetTopWnd();
	bool FileOpenDlg(std::string& filename, const std::string& extstr,	const std::string& default_path = "");	
	bool MultipleFileOpenDlg(std::string& filename, const std::string& extstr,	std::vector<std::string>& files);
	bool FileSaveDlg(std::string& filename, const std::string& extstr);	

	bool DirOpenDlg(std::string& dirname,const std::string& defaultPath);
	bool DirSaveDlg(std::string& dirname,const std::string& defaultPath);
	void ShowInfoDlg();
	void MessageDlg(const char* msg);
	void ErrMessageDlg(const char* msg);
	bool ConfirmDlg(const char* msg);
	void Exit();

	void ExportSTLDlg(vxWindow* win, bool show, bool modeDir, fxgenCore* core, GUIController* gui);
	void FreeRotateDlg(vxWindow* win, bool show, ViewController* vctrl);

	void SDGlobalImportDlg(vxWindow* win,bool show = true);
	void DomainCartesianDlg(vxWindow* win, DomainCartesianEditor *editor, bool show = true);
	void Domain_BCM_Dlg(vxWindow* win, DomainBCMEditor* editor, GUIController* gui, bool show = true);
	void VoxelCartesian_Dlg(vxWindow* win, VoxelCartesianEditor* editor, GUIController* gui,fxgenCore* core, bool show = true);
	void SliceControllerDlg(vxWindow* win, SliceController* controller, bool show = true);
	void NotifySelectionPick();
	
	void Notify_EyeStatus();


	int BCMedDlgModal(vxWindow* win, BCMedFunc& func);
	b8 BCMedDlgModal2(vxWindow* win, BCMedFunc& func, fxgenCore* core );
	b8 ColorDlg(vxWindow* win,  VX::Math::vec4& color);

	b8 CoordinateViewer_DlgModal(vxWindow* win,  fxgenCore* core, ViewController* vctrl, GUIController* gui);

	int SDImportDlgModal(vxWindow* win,VoxelCartesianParam& func);

	int LocalBCSettingDlgModal(vxWindow* win, BCMedFunc& func);
	b8 LocalBCSettingDlgModal2(vxWindow* win, BCMedFunc& func, fxgenCore* core, GUIController* gui);

	int OuterBCSettingDlgModal(vxWindow* win, DomainEditor* editor);

	int SubdomainSettingDlgModal(vxWindow* win, SubdomainFunc& func);
	
	void NodePropertyDlg(vxWindow* win, NodePropertyFunc& func);
	
	//clipboard
	void SetClipboard(const std::string& msg);
	std::string GetClipboard();

	void UpdateSliceControllerDlg();

} // namespace VX

#endif // _VX_DIALOG_UTIL_H_
