#include "fxgenGUISD_SelectPanel.h"
#include "../Core/fxgenCore.h"
#include "../VX/SG/VoxelCartesianL.h"
#include "../VX/SG/VoxelCartesianG.h"
#include "../VX/SG/HitVoxelCartesianL.h"
#include "../VX/SGPickerElm.h"
#include "../Core/SliceController.h"
#include "vxWindow.h"
#include "../Core/GUIController.h"
#include "../FileIO/VoxelCartesianScanner.h"


#include <vector>

#include "fxgenGUISDSettingUtil.h"
#include "fxgenGUISD_Frame.h"

#include "DialogUtil.h"


//Grid の列の位置
// 0列目 load フラグ
// 1列目 coordinate information




using namespace VX::SG;

// namespace 

fxgenGUISD_SelectPanel::fxgenGUISD_SelectPanel( 
		wxWindow* parent, 
	 GUIController *gui,fxgenCore* core,
	 fxgenGUISD_Frame* dlg)
:
SD_SelectPanel( parent ), m_core(core), m_gui(gui)	, b_creating_grid(false),m_dlg(dlg)
{

}

fxgenGUISD_SelectPanel::~fxgenGUISD_SelectPanel()
{

}

void fxgenGUISD_SelectPanel::Show_inner()
{
	setGuiData();
}

bool fxgenGUISD_SelectPanel::Show(bool show )
{
	return wxWindow::Show(show);
}

/**
*	@brief データをテーブルに設定する
*/
void fxgenGUISD_SelectPanel::setGuiData()
{
	//select cell
	// 行選択モード
	wxGrid::wxGridSelectionModes mode = wxGrid::wxGridSelectRows;//wxGrid::wxGridSelectCells;
	m_pSubDomainGrid->SetSelectionMode( mode );

	//選択時の行の色と背景色
	m_pSubDomainGrid->SetSelectionForeground(wxColour(204,102,0));
	m_pSubDomainGrid->SetSelectionBackground(wxColour(72,31,0));

	//staging対象を取得する
	m_staging_locals.clear();
	getSelectedLocalsFromSG( m_staging_locals );

	// previous grid data clear
	int row = m_pSubDomainGrid->GetRows();
	if(row>0){
		m_pSubDomainGrid->DeleteRows(0,row);
	}
	
	m_pSubDomainGrid->EnableEditing(false);
	b_creating_grid = true;
	m_pSubDomainGrid->AppendRows(m_staging_locals.size());
	for (s32 i = 0; i < m_staging_locals.size(); i++) {
		//grid　に行追加
		AddRow(i,m_staging_locals.at(i));
	}
	b_creating_grid = false;
	m_pSubDomainGrid->EnableEditing(true);
}

/**
*	@brief SGノード中、selectionされているlocalを取得
*/
void fxgenGUISD_SelectPanel::getSelectedLocalsFromSG(  std::vector<VX::SG::VoxelCartesianL*>& staging)
{
	VX::SG::VoxelCartesianG* g = m_core->GetVoxelCartesianG();
	const std::vector<NodeRefPtr<VX::SG::VoxelCartesianL> >& locals = g->GetLocals();

	staging.clear();
	for(int i=0; i<locals.size();i++){
		// selectedされているものだけを選択
		if(locals.at(i)->GetSelection()){
			staging.push_back(locals.at(i));
		}
	}

}

void fxgenGUISD_SelectPanel::AddRow(const int& row, VX::SG::VoxelCartesianL* local )
{
	wxString name		= local->GetName();
	u32 id				= local->Getid();

	b8 isLoad = local->IsLoad();


	// def attr
	// colum editor
	//col0, Load Flag
	wxGridCellAttr* attr_load = new wxGridCellAttr();
	attr_load->SetEditor(	new wxGridCellBoolEditor());
	attr_load->SetRenderer( new wxGridCellBoolRenderer());
	m_pSubDomainGrid->SetAttr(row ,0, attr_load);


	/////////////////////////////
	// cell setting	

	//col0, Load Flag
	wxString loadStr = (isLoad)?"1":"";
	m_pSubDomainGrid->SetCellValue(row, COL_LOAD, loadStr);

	//col1, Coord
	m_pSubDomainGrid->SetCellValue(row, COL_COORD, name);
	m_pSubDomainGrid->SetReadOnly(row, COL_COORD, true);

	//フォント指定
	//wxFont f = m_pSubDomainGrid->GetCellFont(row, COL_COORD);
	//m_pSubDomainGrid->SetCellFont(row, COL_COORD, f);


}


void fxgenGUISD_SelectPanel::updateMainWnd()
{
	m_dlg->updateMainWnd();
	// tree icon update
}

/**
*	@brief grid行の選択イベント。CrossPlaceのActiveになるlocal subdomainを変更する
*/
void fxgenGUISD_SelectPanel::m_pSubDomainGridOnGridSelectCell( wxGridEvent& event )
{
	//テーブルを作成中は変更をキャンセルする。
	if(b_creating_grid){
		return;
	}
	
	int row = event.GetRow();
    int col = event.GetCol();
	if(col != COL_COORD || row<0 ){
		return;
	}
	//行をカレントにする
	changeActiveLocalOnGrid(row,false);
}

void fxgenGUISD_SelectPanel::changeSelectLocalVoxel()
{
	VX::SG::VoxelCartesianG* g = m_core->GetVoxelCartesianG();
	VX::SG::VoxelCartesianL* local = g->GetCrossPlaneActiveLocal();
	assert(local);
	//ローカルの断面操作の対象を変更する
	m_core->changeSelectedLocalVoxel(local);

}

VX::SG::VoxelCartesianL* fxgenGUISD_SelectPanel::GetNodeByRowIndex(const int& row)
{
	assert(row < m_staging_locals.size());

	assert(m_staging_locals.size() == m_pSubDomainGrid->GetRows());


	VX::SG::VoxelCartesianL* local = m_staging_locals.at(row);
	assert(local!=NULL);

	return local;

}

/**
*	@brief 適用ボタン
*/
void fxgenGUISD_SelectPanel::m_button_subdomainloadOnButtonClick( wxCommandEvent& event )
{
	//Gridの選択状態に合わせてロード、Visiableを指定する
	wxBusyCursor wait;

	int rows = m_pSubDomainGrid->GetRows();
	assert(m_staging_locals.size() == rows);

	
	for (int i = 0; i < rows; ++i) {
		//AttrEditorを参照すると解放がgridの管轄になるのでセルの値だけを取得する
		std::string errMsg;
		//ロードフラグのチェック
		wxString v = m_pSubDomainGrid->GetCellValue(i, COL_LOAD);
		b8 isLoad = wxGridCellBoolEditor::IsTrueValue(v);

		//指定
		VX::SG::VoxelCartesianL* local = m_staging_locals.at(i);
		local->SetLoad(isLoad,errMsg);

		if(errMsg.size()>0){
			if(rows==1){
				UI::MessageDlg(errMsg.c_str());
			}else{
				//エラーがあればメッセージを出して処理を続けるかユーザに聞く
				std::string msg  = errMsg + std::string("\nDo you continue?\n");
				if( !UI::ConfirmDlg(msg.c_str())){
					break;
				}
			}
		}
	}

	//ステージングされたボクセルをスキャンする
	scanVoxel(m_staging_locals);

	//断面の再描画
	repaintCrossPlane();

	//選択のローカルのクリア（ステージングしたものが煩わしいため）
	clearSelectionAllLocals();
	
	//グローバル断面を可視化する
	// 意図的にグローバルを非表示にしている場合もあるので、明示的に可視化しない
	changeEyeStatusInit();
	
	//MedBcflgテーブルの構築
	m_dlg->CreateMedBcflgGridTable();


	// update 
	updateMainWnd();
}

/**
*	@brief 適用ボタンを押下された場合のeyeの状態の初期設定
*	       clip   close
*		   global open
*          local  close
*/
void fxgenGUISD_SelectPanel::changeEyeStatusInit()
{
	changeEyeStatus(false,true,false);
}

void fxgenGUISD_SelectPanel::changeEyeStatus(const bool& b_clip_eye_open , const bool& b_global_eye_open , const bool& b_local_eye_open)
{
	VX::SG::VoxelCartesianG* g = m_core->GetVoxelCartesianG();
	g->GetClipNode()->SetVisible(b_clip_eye_open);
	g->GetGlobalGroup()->SetVisible(b_global_eye_open);
	g->GetLocalGroup()->SetVisible(b_local_eye_open);

	m_dlg->Notify_EyeStatus();

	m_dlg->updateMainWnd();
	// tree icon update
}

/**
*	@brief Clearボタンを押下された場合のeyeの状態の初期設定
*	       clip   open
*		   global close
*          local  open
*/
void fxgenGUISD_SelectPanel::changeEyeStatusClear()
{
	changeEyeStatus(true,false,true);
}

/**
*	@brief スキャンして定義されている値の候補を参照する
*/
void fxgenGUISD_SelectPanel::scanVoxel(const std::vector<VX::SG::VoxelCartesianL*>& locals	)
{

	//スキャンして結果を格納する
	VoxelCartesianScanner scan(m_core);
	scan.Scan(locals );

}

void fxgenGUISD_SelectPanel::clearSelectionAllLocals()
{
	VX::SG::VoxelCartesianG* g = m_core->GetVoxelCartesianG();
	const std::vector<NodeRefPtr<VoxelCartesianL> >& locals = g->GetLocals();
	for(int i=0;i<locals.size();i++){
		locals[i]->Select(false);
	}
}



/**
*	@brief 断面を再描画する
*/
void fxgenGUISD_SelectPanel::repaintCrossPlane()
{
	//TODO:fuchi 初回の断面がグレイなので、repaintできれば良い

	VX::SG::VoxelCartesianG* g = m_core->GetVoxelCartesianG();
	g->RepaintSlicePlane();

	const std::vector<NodeRefPtr<VoxelCartesianL> >& locals = g->GetLocals();
	for(int i=0;i<locals.size();i++){
		locals[i]->RepaintSlicePlane();
	}

}



/**
*	@brief grid で選択行（複数）を削除する。
*		
*
*/
void fxgenGUISD_SelectPanel::m_button_clearOnButtonClick( wxCommandEvent& event )
{
	wxBusyCursor wait;

	clearStagingGridData();
	
	//断面の再描画
	repaintCrossPlane();

	//選択のローカルのクリア（ステージングしたものが煩わしいため）
	clearSelectionAllLocals();
	
	//グローバルをclose. clip/local をopen
	changeEyeStatusClear();


	//MedBcflgテーブルの破棄
	m_dlg->ClearMedBcflgGridTable();

	// update 
	updateMainWnd();
}

void fxgenGUISD_SelectPanel::clearStagingGridData()
{
	//指定行のlocalをとりだし、強制的にselectionを解除する
	//その後、gridテーブルを再構築する
	for(int i=0;i<m_staging_locals.size();i++){
		std::string errMsg;//クリアの時はエラーメッセージは無いが指定するのみ
		VoxelCartesianL* local = m_staging_locals[i];
		local->SetLoad(false,errMsg);//データクリア
		local->SetVisible(true);//見えるようにしておく。デフォルト
	}	
	m_staging_locals.clear();

	// previous grid data clear
	int row = m_pSubDomainGrid->GetRows();
	if(row>0){
		m_pSubDomainGrid->DeleteRows(0,row);
	}
}

/**
*	@brief SGのノードからＧＵＩ情報を再構築する
*/
void fxgenGUISD_SelectPanel::CreateGuiBySGNode()
{
	setGuiData();
}

VX::SG::VoxelCartesianL* fxgenGUISD_SelectPanel::getPickedLocalInMainWnd()
{
	VX::SG::VoxelCartesianL* local = NULL;
	const VX::SGPickerElm& pick = m_core->GetPickInformation();
	bool hasLocal = false;
	const VX::SG::HitVoxelCartesianL& hit = pick.GetLocal(hasLocal);
	if(hasLocal){
		local = hit.m_dom;
	}
	return local;
}

bool fxgenGUISD_SelectPanel::isStagingLocalOnGrid(const VX::SG::VoxelCartesianL* local, int& rowIndexOnGrid)const
{
	rowIndexOnGrid = 0;

	for(int i=0;i<m_staging_locals.size();i++){
		//オブジェクト同じかチェック
		if( m_staging_locals.at(i) == local){
			
			rowIndexOnGrid = i;
			return true;
		}
	}

	return false;
}

void fxgenGUISD_SelectPanel::changeActiveLocalOnGrid( const int& row,const bool& callSelectRow)
{
	if(callSelectRow){
		m_pSubDomainGrid->SelectRow(row);
	}

	//セレクトイベント発行
	// ３列目の部分がクリックされると、アクティブを切り替える。
	VoxelCartesianL* local = GetNodeByRowIndex(row);
	assert(local!=NULL);
	m_core->GetVoxelCartesianG()->SetCrossPlaneActiveLocal(local);

	// アクティブなローカルの枠線を強調するので再描画
	//repaint main wnd
	updateMainWnd();
	
	//change current local voxel
	// ローカルの断面アクティブノードが変更されたので通知
	changeSelectLocalVoxel();
}

void fxgenGUISD_SelectPanel::NotifySelectionPick()
{
	VoxelCartesianL* local = getPickedLocalInMainWnd();
	if(local!=NULL){		
		int row = 0;
		if(isStagingLocalOnGrid(local,row)){
			//ステージング対象のローカルドメインのピック
			changeActiveLocalOnGrid(row,true);

		}
	}
}

/**
*	@brief ステージング決定
*			現在ステージングされているものを解除（クリアボタンと同じ解放）を行い。
*			SGで選択されているノードを行に追加する。
*/
void fxgenGUISD_SelectPanel::m_button_stagingOnButtonClick(wxCommandEvent& event)
{
	wxBusyCursor wait;

	//現在のステージングをデータクリア
	clearStagingGridData();

	// GUIテーブル再構築
	CreateGuiBySGNode();

	//チェックボックスを明示的にＯＮにする（押すのが面倒のため）
	setCheckON_AllCell();
}

void fxgenGUISD_SelectPanel::setCheckON_AllCell()
{

	int rows = m_pSubDomainGrid->GetRows();
	for(int i=0;i<rows;i++){
		//col1, Show Flag
		wxString checkON = "1";

		m_pSubDomainGrid->SetCellValue(i, COL_LOAD,    checkON);

	}

}

/**
*	@brief 再選択用の[SelectAll]ボタン
*/
void fxgenGUISD_SelectPanel::m_button_reselectionOnButtonClick( wxCommandEvent& event )
{
	//明示的には指定しない
	//m_core->SelectionMode(fxgenCore::MODE_DOMAIN);
	
	//目のアイコンを更新
	changeEyeStatus(true,true,true);

	setSelectionOnStagingLocalDomains(m_staging_locals);

}

const std::vector<VX::SG::VoxelCartesianL*>& fxgenGUISD_SelectPanel::Get_staging_locals()const
{
	return m_staging_locals;
}

void fxgenGUISD_SelectPanel::setSelectionOnStagingLocalDomains( const std::vector<VX::SG::VoxelCartesianL*>& staging)
{
	//ステージングされているのを再度
	for(int i=0; i<staging.size();i++){
		// selectにする
		staging.at(i)->Select(true);
	}

	// refresh main scene graph
	// repaint 
	updateMainWnd();
}
