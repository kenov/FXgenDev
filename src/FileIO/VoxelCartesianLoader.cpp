#include "VoxelCartesianLoader.h"

#include "../VX/VX.h"
#include "../VX/SG/SceneGraph.h"

#include "FileIOUtil.h"
#include "../VOX/BitVoxel.h"
#include "../VOX/VOXFileCommon.h"
#include "VoxelCartesianDfiLoader.h"
#include "TextParser.h"
#include "VoxelCartesianBlockLoader.h"
#include "VoxelCartesianScanner.h"

#include "../VX/RLE.h"

#define MODULE_NAME "Voxel Cartesian Load"


VoxelCartesianLoader::VoxelCartesianLoader()
{

}

VoxelCartesianLoader::~VoxelCartesianLoader()
{

}

VX::SG::Group* VoxelCartesianLoader::Load(const VoxelCartesianParam& param,fxgenCore* core,std::string& errMsg)
{
    return Load_Dfi( param,core,errMsg);
	
}

VX::SG::Group* VoxelCartesianLoader::Load_Dfi(const VoxelCartesianParam& param,fxgenCore* core,std::string& errMsg)
{
	using namespace std;
	using namespace VX::SG;

	// local を作成
	VoxelCartesianDfiLoader dfiLoader(param);
	if( !dfiLoader.IsOK() ){
		return NULL;
	}
	
	// グローバルの幾何パラメータ
	VX::Math::vec3 g_origin	= dfiLoader.GetOrigin();
	VX::Math::vec3 g_region	= dfiLoader.GetRegion();
	VX::Math::idx3 g_voxSize	= dfiLoader.GetNumVoxels();
	VX::Math::idx3 g_divs		= dfiLoader.GetNumDivs();
	
	//ダイレクトファイルモードであれば、グローバルの大きさをローカルに合わせる
	if( param.IsDirectMode()){
		if(dfiLoader.GetLocals().size()==1){
			VX::SG::VoxelCartesianL * local = dfiLoader.GetLocals().at(0);
			g_origin	= local->GetOrigin();
			g_region	= local->GetRegion();
			g_voxSize	= local->GetNumVoxels();
			g_divs		= local->GetNumDivs();

			// local のデータを強制ロード
			local->SetLoad(true,errMsg);

			// mediumをスキャン
			VoxelCartesianScanner scan(core);
			scan.Scan(local);

			// selectionを設定
			local->Select(true);

			//断面強制表示
			local->RepaintSlicePlane();

		}else{
			assert(false);
		}
	}


	//グローバルを作成
	VX::SG::VoxelCartesianG* global 
		= vxnew VX::SG::VoxelCartesianG(g_origin,
		                                g_region,
										g_voxSize,
										g_divs,
										dfiLoader.GetLocals(),
										dfiLoader.GetOneSubdomainBytesize()
										);


	return global;
}

/**
*	@brief ダイレクトにCellIDファイルを読む
*/
/*
VX::SG::Group* VoxelCartesianLoader::Load_bvxdirect(const std::string& direct_bvxfile)
{
	using namespace std;
	using namespace VX::SG;

	VX::Math::idx3 vSize(0,0,0);
	bool isNeedSwap = false;
	u32 gc = 0;

	bool ret = VoxelCartesianBlockLoader::ReadHeaderInfo( 
														direct_bvxfile,
		                                                vSize,
														isNeedSwap,
														gc);
	if(!ret){
		return NULL;
	}
	// subdomain毎に適切な値を振る
	u32 L_id						= 0;
	VX::Math::vec3 L_origin		= VX::Math::vec3(0,0,0);
    VX::Math::vec3 L_region		= VX::Math::vec3(vSize[0],vSize[1],vSize[2]);
	VX::Math::idx3 L_voxSize		= vSize;
	VX::Math::idx3 L_divs			= VX::Math::idx3(1,1,1); 
	VX::Math::idx3 L_headGIdx		= VX::Math::idx3(1,1,1); 
	std::string L_block_CellID_file = direct_bvxfile;
	std::string L_block_BCflagID_file = "";
	s32 L_cellID=-1;
	s32 L_bCflagID=VX::SG::VoxelCartesianL::BCFLAG_NONE;
	bool L_header_isNeedSwap = isNeedSwap;
	u32 L_header_gc = gc;

	VX::SG::VoxelCartesianL * local = vxnew VX::SG::VoxelCartesianL(
							L_id,
							L_origin,
                            L_region,
							L_voxSize,
							L_divs,
							L_headGIdx,
							L_block_CellID_file,
							L_block_BCflagID_file,
							L_cellID,
							L_bCflagID,
							L_header_isNeedSwap,
							L_header_gc	);
	//global もlocalと同じ。localが１つのケースを想定している。
	VX::Math::vec3 origin = L_origin;
	VX::Math::vec3 region = L_region;
	VX::Math::idx3 voxSize = L_voxSize;
	VX::Math::idx3 divs = L_divs;
	std::vector<VoxelCartesianL*> locals;
	std::string cid_dir = "";
	std::string bcf_dir = "";
	u32 one_subdomainbytesize = 0;
	
	// local のデータを強制ロード
	local->SetLoad(true);
	// selectionを設定
	local->Select(true);

	locals.push_back(local);

	VX::SG::VoxelCartesianG* mesh = vxnew VX::SG::VoxelCartesianG(
		origin,
        region,
		voxSize,
		divs,
		locals,
		cid_dir,
		bcf_dir,
		one_subdomainbytesize);


	return mesh;
}
 */
