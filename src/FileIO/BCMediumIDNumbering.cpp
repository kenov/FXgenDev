
#include "BCMediumIDNumbering.h"
#include <assert.h>

BCMediumIDNumbering* BCMediumIDNumbering::m_myself = NULL;

BCMediumIDNumbering::BCMediumIDNumbering()
{

}

BCMediumIDNumbering::~BCMediumIDNumbering()
{

}

BCMediumIDNumbering* BCMediumIDNumbering::GetInstance()
{
	if(m_myself==0){
		m_myself = new BCMediumIDNumbering();
	}
	return m_myself;
}


std::string BCMediumIDNumbering::GetNameByID(const u32& id)
{
	if(id==0){
		return "";// non setting
	}
	std::map<u32, std::string>::iterator  ret = m_names.find(id);
	if(ret != m_names.end()){
		return ret->second;
	}
	// not found
#ifdef _DEBUG
	assert(false);
#endif
	return "";
}

bool BCMediumIDNumbering::HasID(const u32& id) const
{
	std::map<u32, std::string>::const_iterator  ret = m_names.find(id);
	return (ret != m_names.end());
}

void BCMediumIDNumbering::AddID(const u32& id,std::string& name) 
{
	assert(HasID(id)==false);

	assert(id>0);
	assert(name.size()>0);

	m_names.insert( std::map<u32, std::string>::value_type( id, name ) );
}

/**
*	@brief 名前がなかったら自動的にＩＤを採番（空き番からサーチ）して設定する
*/
u32 BCMediumIDNumbering::GenerateIDByName(const std::string& name )
{
	if(name.size()==0){
		assert(false);
		return 0;
	}

	//std::map<u32, std::string>::iterator  ret = m_names.find("hoge");
	std::map<u32, std::string>::iterator it = m_names.begin();
	while( it != m_names.end() )
	{
		if(name == (*it).second){
			return (*it).first;
		}
		++it;
	}
	//candidattion
	u32 id = NumberingNextID();
	//check for empty
	std::map<u32, std::string>::iterator  ret = m_names.find(id);
	if(ret != m_names.end()){
		assert(false);
	}

	m_names.insert( std::map<u32, std::string>::value_type( id, name ) );
	return id;
}


/**
*	@brief 1から順番に空き番を見つける
*/
u32 BCMediumIDNumbering::NumberingNextID() const
{	
	//std::map<u32, std::string>::iterator it = m_names.begin();
	u32 START = 1;
	
	for( u32 i = START ; ; i++){
		std::map<u32, std::string>::const_iterator  ret = m_names.find( i );
		if(ret == m_names.end()){
			return i;//not found id
		}
	}
	return 0;
}

void BCMediumIDNumbering::Replace(const std::vector<std::pair<u32,std::string> > & data)
{
	
	m_names.clear();

	for(u32 i=0;i<data.size();i++){
		// index start from 1
		u32 id = data.at(i).first;
		assert(id>0);
		
		m_names.insert( std::map<u32, std::string>::value_type( id, data.at(i).second ) );
	}
	
}


