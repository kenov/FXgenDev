//
//  STLBinaryLoader.cpp
//

#include "../VX/VX.h"
#include "../VX/SG/SceneGraph.h"
#include "../VX/Stream.h"

#include <string>
#include <set>

#include "STLBinaryLoader.h"


namespace  {
	std::string withOutExt(const std::string& fname)
	{
		size_t p = fname.rfind(".");
		if (p != std::string::npos) {
			std::string t = fname;
			t = t.substr(0, p);
			return t;
		}
		return fname;
	}
}

STLBinaryLoader::STLBinaryLoader(){
}
STLBinaryLoader::~STLBinaryLoader(){
}
	
	
VX::SG::Node* STLBinaryLoader::Load(const VX::Stream* st)
{	
	using namespace VX::Math;
	using namespace VX::SG;
	
	const s8* data = static_cast<const s8*>(st->GetData());
	data += 80; //skip header
	
	const u32 triCount = *reinterpret_cast<const u32*>(&data[0]); data+=sizeof(u32);
	const u32 vertexCount = triCount * 3;
	const u32 indexCount  = triCount * 3;

	// alloc and store
	GeometryBVH* g = vxnew GeometryBVH();
	g->SetName(withOutExt(st->GetFileName()));
	g->Alloc(vertexCount, indexCount);
	Geometry::VertexFormat* vertex = g->GetVertex();
	Index* index = g->GetIndex();
	
	Geometry::VertexFormat* vertexptr = vertex;
	u32* idb = g->GetIDBuffer();
	for (u32 i = 0; i < vertexCount; i+=3)
	{
		const u32 normal[] = {
			*reinterpret_cast<const u32*>(&data[0]),
			*reinterpret_cast<const u32*>(&data[4]),
			*reinterpret_cast<const u32*>(&data[8])
		};
		// id
		u16 idx = *reinterpret_cast<const u16*>(&data[48]);
		if (idx > 32) // invalid ID
			idx = 0;
		idb[i/3] = (idx<<8);
		u32 initCol = VX::initColor;
		*reinterpret_cast<u32*>(&vertexptr->pos.x) = *reinterpret_cast<const u32*>(&data[12]);
		*reinterpret_cast<u32*>(&vertexptr->pos.y) = *reinterpret_cast<const u32*>(&data[16]);
		*reinterpret_cast<u32*>(&vertexptr->pos.z) = *reinterpret_cast<const u32*>(&data[20]);
		vertexptr->col = initCol;
		*reinterpret_cast<u32*>(&vertexptr->normal.x) = normal[0];
		*reinterpret_cast<u32*>(&vertexptr->normal.y) = normal[1];
		*reinterpret_cast<u32*>(&vertexptr->normal.z) = normal[2];
		vertexptr++;
		*reinterpret_cast<u32*>(&vertexptr->pos.x) = *reinterpret_cast<const u32*>(&data[24]);
		*reinterpret_cast<u32*>(&vertexptr->pos.y) = *reinterpret_cast<const u32*>(&data[28]);
		*reinterpret_cast<u32*>(&vertexptr->pos.z) = *reinterpret_cast<const u32*>(&data[32]);
		vertexptr->col = initCol;
		*reinterpret_cast<u32*>(&vertexptr->normal.x) = normal[0];
		*reinterpret_cast<u32*>(&vertexptr->normal.y) = normal[1];
		*reinterpret_cast<u32*>(&vertexptr->normal.z) = normal[2];
		vertexptr->normal = normalize(vertexptr->normal);
		vertexptr++;
		*reinterpret_cast<u32*>(&vertexptr->pos.x) = *reinterpret_cast<const u32*>(&data[36]);
		*reinterpret_cast<u32*>(&vertexptr->pos.y) = *reinterpret_cast<const u32*>(&data[40]);
		*reinterpret_cast<u32*>(&vertexptr->pos.z) = *reinterpret_cast<const u32*>(&data[44]);
		vertexptr->col = initCol;
		*reinterpret_cast<u32*>(&vertexptr->normal.x) = normal[0];
		*reinterpret_cast<u32*>(&vertexptr->normal.y) = normal[1];
		*reinterpret_cast<u32*>(&vertexptr->normal.z) = normal[2];
		vertexptr->normal = normalize(vertexptr->normal);
		vertexptr++;
		
		if (normal[0] == 0 && normal[1] == 0 && normal[2] == 0)
		{
			const vec3 v0 = vertexptr[-3].pos;
			const vec3 v1 = vertexptr[-2].pos;
			const vec3 v2 = vertexptr[-1].pos;
			const vec3 N  = normalize(cross(v2 - v0, v1 - v0));
			vertexptr[-3].normal = N;
			vertexptr[-2].normal = N;
			vertexptr[-1].normal = N;
		}
		
		data += 50;
	}
	for (u32 i = 0; i < indexCount; i++)
	{
		index[i] = i;
	}

	g->CalcBounds();
	
	VXLogD("Loaded STLBinary: Triangle=%d\n", triCount);
	
	return g;
}
