/*
 *
 * FileIO/FileIOUtil.h
 *
 */

#ifndef __FILE_IO_UTIL_H__
#define __FILE_IO_UTIL_H__

#include "../VX/Type.h"
#include "../VX/Math.h"
#include <algorithm>
#include <vector>
#include <string>
#include <sstream>

#include <cctype>
#include <cstdio>

class TextParser;
namespace VX {
	namespace SG {
		class Node;
		class Group;
		class OuterBC;
		class LocalBC;
	}
}

struct ToUpper{
    char operator()(char c) { return toupper(c); }
};

/////////////// Color - Text Converter ///////////////////
inline
u32 colorCvt_str2int( const std::string& color_str, bool& status )
{
	u32 color = 0;
	if(color_str.length() != 8){ status = false; return 0; }

	const char *col_str = color_str.c_str();

	int i = 7;
	while(*col_str != '\0'){
		if('0' <= *col_str && *col_str <= '9'){
			color += (*col_str - '0') << (i * 4);
		}
		else if('a' <= *col_str && *col_str <= 'f'){
			color += (*col_str - 'a' + 10) << (i * 4);
		}
		else if('A' <= *col_str && *col_str <= 'F'){
			color += (*col_str - 'A' + 10) << (i * 4);
		}
		else {
			status = false;
			return 0;
		}
		i--;
		col_str += 1;
	}

	status = true;
	return color;
}

inline
bool colorCvt_int2str( const u32 color, std::string& color_str )
{
	char col_str[9] = {0};
	
	for(int i = 0; i < 4; i++){
		unsigned char c = (color >> (i * 8)) & 0x000000FF;
		unsigned char lc = c        & 0x0F;
		unsigned char uc = (c >> 4) & 0x0F;
		col_str[7 - i*2] = lc < 10 ? '0' + lc : 'A' + (lc-10);
		col_str[6 - i*2] = uc < 10 ? '0' + uc : 'A' + (uc-10);
	}
	col_str[8] = '\0';

	color_str = col_str;

	return true;
}

inline
VX::Math::vec4 colorCvt_str2vec4( const std::string& color_str, bool& status )
{
	VX::Math::vec4 color = VX::Math::vec4(1, 1, 1, 1);

	u32 iColor = colorCvt_str2int( color_str, status );
	if( !status ){ return color; }

	color.a = static_cast<float>((iColor >>  0) & 0x000000FF) / 255.0f;
	color.b = static_cast<float>((iColor >>  8) & 0x000000FF) / 255.0f;
	color.g = static_cast<float>((iColor >> 16) & 0x000000FF) / 255.0f;
	color.r = static_cast<float>((iColor >> 24) & 0x000000FF) / 255.0f;

	status = true;
	return color;
}

inline
bool colorCvt_vec42str( const VX::Math::vec4& color, std::string& color_str )
{
	u32 iColor = 0;
	iColor += ( (static_cast<u32>(color.a * 255.0f) & 0x000000FF) <<  0 );
	iColor += ( (static_cast<u32>(color.b * 255.0f) & 0x000000FF) <<  8 );
	iColor += ( (static_cast<u32>(color.g * 255.0f) & 0x000000FF) << 16 );
	iColor += ( (static_cast<u32>(color.r * 255.0f) & 0x000000FF) << 24 );
	
	return colorCvt_int2str(iColor, color_str);
}


/////////////////// String Compare ///////////////////////
inline
int CompStr( const std::string& str1, const std::string& str2, bool ignorecase=true )
{
	std::string lstr1 = str1;
	std::string lstr2 = str2;
	if( ignorecase ){
		std::transform(lstr1.begin(), lstr1.end(), lstr1.begin(), ::tolower);
		std::transform(lstr2.begin(), lstr2.end(), lstr2.begin(), ::tolower);
	}

	return lstr1.compare(lstr2);
}

////////////////// File Path Utility //////////////////////
inline
std::string convertPath(const std::string& path)
{
	std::string r = path;
	size_t p = r.find("\\");
	while (p != std::string::npos)
	{
		*(r.begin() + p) = '/';
		p = r.find("\\");
	}
	return r;
}

inline	
std::string getExt(const std::string& path)
{
	size_t p = path.rfind(".");
	std::string ext;
	if (p != std::string::npos)
	{
		ext = path.substr(p, path.size());
		std::transform(ext.begin(), ext.end(), ext.begin(), ToUpper()); 
	}
	return ext;
}

/**
*	@brief get directory path
*	@param[in] path
*	@ret path without "/" in last
*/
inline
std::string GetDirectory(const std::string& path)
{
	std::string cpath = convertPath(path);

	std::string dir;
	size_t p = cpath.rfind("/");
	if(p != std::string::npos)
	{

		dir = cpath.substr(0, p+1);
/*		
		// without "/" in last charactor 

		dir = cpath.substr(0, p);
		*/
	}
	return dir;
}

inline
std::string GetFilePrefix(const std::string& path)
{
	std::string cpath = convertPath(path);
	std::string filename = cpath.substr(cpath.rfind("/")+1);
	
	return filename.substr(0, filename.rfind("."));
}

inline std::string FixDirectoryPath(const std::string& dir)
{
	if( dir == std::string("") ){
		return std::string("./");
	}else if( dir.rfind("/") != dir.length()-1 ){
		return dir + std::string("/");
	}else{
		return dir;
	}
}

/////////////////// Endian Swapper //////////////////////////
/// for 2bytes
static inline void BSwap16(void* a){
	u16* x = (u16*)a;
	*x = (u16)( ((((*x) & 0xff00) >> 8 ) | (((*x) & 0x00ff) << 8)) );
}

/// for 4bytes
static inline void BSwap32(void* a){
	u32* x = (u32*)a;
	*x = ( (((*x) & 0xff000000) >> 24) | (((*x) & 0x00ff0000) >> 8 ) |
	       (((*x) & 0x0000ff00) <<  8) | (((*x) & 0x000000ff) << 24) );
}

/// for 8bytes
static inline void BSwap64(void* a){
	u64* x = (u64*)a;
	*x=( (((*x) & 0xff00000000000000ull) >> 56) | (((*x) & 0x00ff000000000000ull) >> 40) |
	     (((*x) & 0x0000ff0000000000ull) >> 24) | (((*x) & 0x000000ff00000000ull) >>  8) |
	     (((*x) & 0x00000000ff000000ull) <<  8) | (((*x) & 0x0000000000ff0000ull) << 24) |
	     (((*x) & 0x000000000000ff00ull) << 40) | (((*x) & 0x00000000000000ffull) << 56) );
}


//////////////////// Read Vector Parameter for TextParser format /////////////////
int ReadVec4( TextParser* tp, const std::string& label, VX::Math::vec4& v );
int ReadVec3( TextParser* tp, const std::string& label, VX::Math::vec3& v );
int ReadVec3( TextParser* tp, const std::string& label, VX::Math::idx3& v );
int ReadVec3( TextParser* tp, const std::string& label, u32 v[3]);


//////////////////// TextParser format File Writer //////////////////////
b8 isBCMFile( const std::string& filename );

#endif // __BC_FILE_UTIL_H__

