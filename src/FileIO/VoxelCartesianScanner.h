/*
 *
 * FileIO/VoxelCartesianScanner.h
 *
 *
 */

#ifndef __VoxelCartesianScanner_H__
#define __VoxelCartesianScanner_H__

#include <vector>
#include "../VX/Type.h"

class fxgenCore;
namespace VX {
	namespace SG {
		class VoxelCartesianL;
		class VoxelCartesianL_Scan;
		class Medium;
		class LocalBC;
		class LocalBCClass;
	}
}

class VoxelCartesianScanner
{
public:
	VoxelCartesianScanner(fxgenCore* core);
	~VoxelCartesianScanner();
	
	//static void test_loadMeduim(fxgenCore* core);
	void Scan(VX::SG::VoxelCartesianL* local);
	void Scan(const std::vector<VX::SG::VoxelCartesianL*>& locals);
private:
	void setMedium(VX::SG::VoxelCartesianL_Scan& cellid);
	void setBCflg(VX::SG::VoxelCartesianL_Scan& bcflg);
	void getMeduimListOnCore(std::vector<VX::SG::Medium*>& meds);
	void getLocalBCListOnCore(std::vector<VX::SG::LocalBC*>& bcs);
	void getBCCListOnCore(std::vector<VX::SG::LocalBCClass*>& bccs);

	void addNewMeduim( const u8& med_id );
	void addNewBCflg( const u8& bcf_id );


	bool hasID(const std::vector<VX::SG::Medium*>& meds, const u32& id)const;
	bool hasID(const std::vector<VX::SG::LocalBC*>& bcf, const u32& id)const;


	fxgenCore* m_core;
};


#endif // __MESH_CARTESIAN_LOADER_H__

