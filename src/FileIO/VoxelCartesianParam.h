/*
 *
 * VoxelCartesianParam.h
 * 
 */

#ifndef _VoxelCartesianParam_H_
#define _VoxelCartesianParam_H_

#include <string>

class fxgenCore;


class VoxelCartesianParam
{

public:

	enum MODE{
		FUNC_NONE =0 ,
		FUNC_DIR ,
		FUNC_DIRECT_FILE_CELLID,
		FUNC_DIRECT_FILE_BCFLG
	};

	VoxelCartesianParam();
	virtual ~VoxelCartesianParam();

	//copy constructor
    VoxelCartesianParam(const VoxelCartesianParam& obj);
    VoxelCartesianParam& operator=(const VoxelCartesianParam& obj);


	std::string Get_basedir()const;
	void Set_basedir(const std::string& path);
	bool Has_basedir()const;

	void Set_Dfi_file(const std::string& path);
	std::string Get_Dfi_file()const;
	bool Has_Dfi_file()const;

	void Set_CellID_file(const std::string& path);
	std::string Get_CellID_file()const;
	bool Has_CellID_file()const;	

	void Set_CellID_dir(const std::string& path);
	std::string Get_CellID_dir()const;
	bool Has_CellID_dir()const;	

	void Set_BCflg_file(const std::string& path);
	std::string Get_BCflg_file()const;
	bool Has_BCflg_file()const;	

	void Set_BCflg_dir(const std::string& path);
	std::string Get_BCflg_dir()const;
	bool Has_BCflg_dir()const;	

	void Clear_CellID_file();	
	void Clear_CellID_dir();	
	void Clear_BCflg_file();	
	void Clear_BCflg_dir();	

	MODE GetMode() const;

	bool existFile(const std::string& path)const;
	bool existDir(const std::string& path)const;


	void Clear();
	bool isInputValid(std::string& errMsg)const;

	bool IsDirectMode()const;


private:
	std::string getDefaultPath();
	void assignCopy(const VoxelCartesianParam& obj);



private:

	std::string m_IndexDfiFile;//dfi index file;
	std::string m_CellID_file;
	std::string m_BCflag_file;
	std::string m_CellID_dir;
	std::string m_BCflag_dir;

};



#endif // _VX_DIALOG_UTIL_H_
