/*
 *
 *  FileIO/BCMediumIDNumbering.h
 *
 */

#ifndef __BC_BCMediumIDNumbering_H__
#define __BC_BCMediumIDNumbering_H__

using namespace std;

#include <string>
#include <map>
#include <vector>
#include "../VX/Type.h"

class BCMediumIDNumbering 
{

private	:
	BCMediumIDNumbering();
	~BCMediumIDNumbering();

public:
	static BCMediumIDNumbering* GetInstance();
	
	std::string GetNameByID(const u32& id);
	u32 GenerateIDByName(const std::string& name );


	void Replace(const std::vector<std::pair<u32,std::string> > & data);

	bool HasID(const u32& id) const;
	void AddID(const u32& id,std::string& name) ;


private:

	u32 NumberingNextID() const;
	static BCMediumIDNumbering* m_myself;
	std::map<u32, std::string> m_names;


};

#endif // __BC_BCMediumIDNumbering_H__

