#include "../VX/VX.h"
#include "../VX/SG/SceneGraph.h"

#include "BCClassSaver.h"

#include <vector>
#include <string>

#include <fstream>
#include <sstream>

BCClassSaver::BCClassSaver()
{

}

BCClassSaver::~BCClassSaver()
{

}

b8 BCClassSaver::Save(const std::string& filepath, const VX::SG::Group* setting)
{
	using namespace std;
	using namespace VX::SG;

	if( !setting ){
		return false;
	}
	
	vector<const OuterBCClass*> obcs;
	vector<const LocalBCClass*> lbcs;

	for(int i = 0; i < setting->GetChildCount(); i++){
		const Node *n = setting->GetChild(i);
		NODETYPE type = n->GetType();

		if(type == NODETYPE_LOCALBCCLASS)
		{
			lbcs.push_back(dynamic_cast<const LocalBCClass*>(n));
			continue;
		}
		else if (type == NODETYPE_OUTERBCCLASS)
		{
			obcs.push_back(dynamic_cast<const OuterBCClass*>(n));
		}
	}
	
	
	ostringstream os;

	if(obcs.size() != 0){
		os << "OuterBCClassTable {" << endl;
		for(int i = 0; i < obcs.size(); i++){
			os << "  Class[@] {" << endl;
			os << "    Label = \"" << obcs[i]->GetLabel() << "\"" << endl;
			if( obcs[i]->HasThermalOption() ){
				os << "    ThermalOption = \"" << obcs[i]->GetThermalOption() << "\"" << endl;
			}
			os << "  }" << endl;
		}
		os << "}" << endl;
	}
	os << endl;

	if(lbcs.size() != 0){
		os << "LocalBCClassTable {" << endl;
		for(int i = 0; i < lbcs.size(); i++){
			os << "  Class[@] {" << endl;
			os << "    Label = \"" << lbcs[i]->GetLabel() << "\"" << endl;
			os << "  }" << endl;
		}
		os << "}" << endl;
	}
	os << endl;

	ofstream ofs( filepath.c_str() );
	ofs << os.str();

	return true;
}
