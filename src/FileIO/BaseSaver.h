//
//  BaseSaver.h
//

#ifndef INCLUDE_VX_BASESAVER_H
#define INCLUDE_VX_BASESAVER_H

#include "../VX/Type.h"

namespace VX {
	
class Stream;
	namespace SG {
		class Node;
	} // namespace SG
}

class BaseSaver
{
protected:
	BaseSaver(){};
	virtual ~BaseSaver(){};
	
public:
	virtual b8 Save(VX::Stream* s, const VX::SG::Node* node, f32 scale = 0, b8 visibleOnly = true) = 0;
};


#endif // INCLUDE_VX_BASESAVER_H

