/*
 *
 * FileIO/FaceBCSaver.h
 *
 *
 */

#ifndef __OUTER_BOUNDARY_SAVER_H__
#define __OUTER_BOUNDARY_SAVER_H__

namespace VX {
	namespace SG {
		class Node;
	} // namespace SG
} // namespace VX

b8 WriteFaceBC( std::ostringstream& os, const VX::SG::Node* node);

class FaceBCSaver
{
public:
	FaceBCSaver();
	~FaceBCSaver();

	b8 Save(const std::string &filepath, const VX::SG::Node* node);
};

#endif // __OUTER_BOUNDARY_SAVER_H__

