#include "VoxelCartesianDfiLoader.h"

#include "../VOX/BitVoxel.h"
#include "../VOX/VOXFileCommon.h"
#include "../VX/SG/VoxelCartesianBlock.h"
#include "FileIOUtil.h"

#include "../VX/RLE.h"

#include <algorithm>
#include <cassert>
#include <functional>
#include <iostream>
#include <vector>

#include <algorithm>
#include <iostream>

VoxelCartesianDfiLoader::VoxelCartesianDfiLoader(const VoxelCartesianParam& param)
	: m_isOK(false),m_param(param)
{
	assert(m_param.Has_Dfi_file());
	std::string dfi_filepath  = m_param.Get_Dfi_file();

	m_rootDirectory = FixDirectoryPath(GetDirectory(dfi_filepath));
	if( !LoadIndex(dfi_filepath) ){
		return;
	}

	if( !LoadBlocks() ){
		Clear();
		return;
	}
	// set file size
	SetOneSubdomainBytesize();

	m_isOK = true;
}

VoxelCartesianDfiLoader::~VoxelCartesianDfiLoader()
{
	// Do not Clear (Blocks) in here
}

bool VoxelCartesianDfiLoader::getRankNoByFileName(const std::string& file , u32& rankNo)
{
	if(file.size()==0){
		return false;
	}

    int pos = (int)file.rfind('_');
    if(pos>=0){
        std::string rank = file.substr(pos+1,6);
        int r = std::atoi(rank.c_str());
        assert(r>=0);
        rankNo = r;
        return true;
	}
	return false;
}


void VoxelCartesianDfiLoader::Clear()
{
	using namespace VX::SG;

	for(std::vector<VoxelCartesianL*>::iterator it = m_locals.begin();
		it != m_locals.end(); ++it)
	{
		if( *it != NULL )
			vxdelete(*it);
	}
	m_locals.clear();

}

const b8 VoxelCartesianDfiLoader::IsOK() const 
{ 
	return m_isOK; 
}

const VX::Math::vec3& VoxelCartesianDfiLoader::GetOrigin()    const 
{ 
	return m_origin;       
}

const VX::Math::vec3& VoxelCartesianDfiLoader::GetRegion()    const 
{ 
	return m_region;  
}

const VX::Math::idx3& VoxelCartesianDfiLoader::GetNumDivs()   const 
{ 
	return m_numDivision;   
}

const VX::Math::idx3& VoxelCartesianDfiLoader::GetNumVoxels() const 
{ 
	return m_numVoxel;
}
std::vector<VX::SG::VoxelCartesianL*>& VoxelCartesianDfiLoader::GetLocals()  
{
	return m_locals;
}

s32 VoxelCartesianDfiLoader::GetOneSubdomainBytesize() const
{
	return m_one_subdomain_bytesize;
}

void VoxelCartesianDfiLoader::SetOneSubdomainBytesize()
{
	m_one_subdomain_bytesize = 0;

	if(m_locals.size()>0){
		m_one_subdomain_bytesize = (s32)m_locals[0]->GetByteSize();
	}
	
}

b8 VoxelCartesianDfiLoader::LoadDomain(TextParser* tp, VX::Math::vec3& org, VX::Math::vec3& rgn,
	VX::Math::idx3& vox, VX::Math::idx3& div,
	std::string& activeSubdomainFile)
{
	using namespace std;
	bool hasOrigin = false;
	bool hasRegion = false;
	bool hasVoxel  = false;
	bool hasDivision = false;
	vector<string> lbls;
	tp->getLabels(lbls);
	for(vector<string>::iterator it = lbls.begin(); it != lbls.end(); ++it){
		string valStr;
		tp->getValue(*it, valStr);

		if( CompStr(*it, "GlobalOrigin") == 0 ){
			if( ReadVec3(tp, *it, org ) == TP_NO_ERROR ){ hasOrigin = true; }
			continue;
		}
		if( CompStr(*it, "GlobalRegion") == 0 ){
			if( ReadVec3(tp, *it, rgn ) == TP_NO_ERROR ){ hasRegion = true; }
			continue;
		}
		if( CompStr(*it, "GlobalVoxel") == 0 ){
			if( ReadVec3(tp, *it, vox ) == TP_NO_ERROR ){ hasVoxel = true; }
			continue;
		}
		if( CompStr(*it, "GlobalDivision") == 0 ){
			if( ReadVec3(tp, *it, div ) == TP_NO_ERROR ){ hasDivision = true; }
			continue;
		}
		if( CompStr(*it, "ActiveSubdomainFile") == 0 ){
			activeSubdomainFile = valStr;
			continue;
		}
				
	}
	if(!hasOrigin || !hasRegion || !hasVoxel || !hasDivision){
		return false;
	}
	return true;
}

bool VoxelCartesianDfiLoader::strcmp(std::string lhs, std::string rhs)
{
    // 小文字に変換してから比較
    //std::back_inserter(out),(int (*)(int))std::toupper);
    //std::transform(lhs.begin(), lhs.end(), lhs.begin(), std::tolower);
    //std::transform(rhs.begin(), rhs.end(), rhs.begin(), std::tolower);
    std::transform(lhs.begin(), lhs.end(), lhs.begin(), (int (*)(int))std::tolower);
    std::transform(rhs.begin(), rhs.end(), rhs.begin(), (int (*)(int))std::tolower);
    return lhs.compare(rhs)==0;
}    


// Load index.dfi
b8 VoxelCartesianDfiLoader::LoadIndex(const std::string& filepath)
{
	using namespace std;
	TextParser *tp = new TextParser;

	if( tp->read(filepath) != TP_NO_ERROR ){
		VXLogE("Read File Error (%s) [%s:%d] \n", filepath.c_str(), __FILE__, __LINE__);
		delete tp;
		return false;
	}

	tp->changeNode("/FileInfo");
	{
		bool hasDir    = false;
		bool hasPrefix = false;
		bool hasGC     = false;
		vector<string> lbls;
		tp->getLabels(lbls);
		for(vector<string>::iterator it = lbls.begin(); it != lbls.end(); ++it){
			string valStr;
			tp->getValue(*it, valStr);

			if( CompStr(*it, "DirectoryPath") == 0 ){
				m_ib.dir = FixDirectoryPath(valStr);
				hasDir   = true;
				continue;
			}
			if( CompStr(*it, "Endian") == 0 ){
				bool swap = false;
				if(strcmp(valStr,"big")){
					swap = true;
				}else if(strcmp(valStr,"little")){
					swap = false;
				}else if(strcmp(valStr,"default")){
					// same as platform 
					swap = false;
				}else{
					//known 
					assert(false);
				}

				m_ib.isNeedSwap = swap ;
				
				continue;
			}
			if( CompStr(*it, "Prefix") == 0 ){
				m_ib.prefix = valStr;
				hasPrefix = true;
				continue;
			}
			if( CompStr(*it, "GuideCell") == 0 ){
				m_ib.gc = atoi(valStr.c_str());
				hasGC = true;
				continue;
			}
		}
		if(!hasDir || !hasPrefix){
			VXLogE("Read File Error (%s) [%s:%d] \n", filepath.c_str(), __FILE__, __LINE__);
			delete tp;
			return false;
		}
	}

	tp->changeNode("/FilePath");
	{
		bool hasProcFile = false;
		vector<string> lbls;
		tp->getLabels(lbls);
		for(vector<string>::iterator it = lbls.begin(); it != lbls.end(); ++it){
			string valStr;
			tp->getValue(*it, valStr);

			if( CompStr(*it, "Process") == 0 ){
				m_procFilename = valStr;
				hasProcFile = true;
			}
		}
		if(!hasProcFile){
			VXLogE("Read File Error (%s) [%s:%d] \n", filepath.c_str(), __FILE__, __LINE__);
			delete tp;
			return false;
		}
	}

	delete tp;	
	return true;
}


b8 VoxelCartesianDfiLoader::LoadProcRank(TextParser* tp, int &id, VX::Math::idx3& voxelSize, VX::Math::idx3& headIndex, VX::Math::idx3& tailIndex,
				int &CellID,int &BCflagID )
{
	using namespace std;
	bool hasID = false;
	bool hasVoxelSize = false;
	bool hasHeadIndex = false;
	bool hasTailIndex = false;
	bool hasCellID = false;
	bool hasBCflagID = false;// option

	id = 0;
	CellID = 0;
	BCflagID = 0;

	vector<string> lbls;
	tp->getLabels(lbls);
	for(vector<string>::iterator it = lbls.begin(); it != lbls.end(); ++it){
		string valStr;
		tp->getValue(*it, valStr);

		if( CompStr(*it, "ID") == 0 ){
			id = atoi(valStr.c_str());
			hasID = true;
			continue;
		}
		if( CompStr(*it, "VoxelSize") == 0 ){
			if( ReadVec3(tp, *it, voxelSize ) == TP_NO_ERROR ){ hasVoxelSize = true; }
			continue;
		}
		if( CompStr(*it, "HeadIndex") == 0 ){
			if( ReadVec3(tp, *it, headIndex ) == TP_NO_ERROR ){ hasHeadIndex = true; }
			continue;
		}
		if( CompStr(*it, "TailIndex") == 0 ){
			if( ReadVec3(tp, *it, tailIndex ) == TP_NO_ERROR ){ hasTailIndex = true; }
			continue;
		}
		if( CompStr(*it, "CellID") == 0 ){
			CellID = atoi(valStr.c_str());
			hasCellID = true;
			continue;
		}
		if( CompStr(*it, "BCflagID") == 0 ){
			BCflagID = atoi(valStr.c_str());
			hasBCflagID = true;
			continue;
		}				
	}

	if(hasBCflagID==false){
		BCflagID = VX::SG::VoxelCartesianL::BCFLAG_NONE;
	}

	if(!hasID || !hasVoxelSize || !hasHeadIndex || !hasTailIndex 
		|| !hasCellID 
		//this is option
		//|| !hasBCflagID  
				
		){
		assert(false);
		return false;
	}
	return true;
}



std::string VoxelCartesianDfiLoader::getProcFile()
{
	std::string proc = m_procFilename;

	unsigned int loc = (unsigned int)proc.find( "./", 0 );
    if( loc == 0 ){
		proc = proc.substr(2,proc.size()-2);
	}

	proc = m_rootDirectory + proc;
	
	return proc;
}

// Load Proc.dif and Block files
b8 VoxelCartesianDfiLoader::LoadBlocks()
{
	using namespace std;
	using namespace VX::SG;
	using namespace VX::Math;
	TextParser *tp = new TextParser;

	string filepath = getProcFile();

	if( tp->read(filepath) != TP_NO_ERROR ){
		VXLogE("Read File Error (%s) [%s:%d] \n", filepath.c_str(), __FILE__, __LINE__);
		delete tp;
		return false;
	}
			
	tp->changeNode("/Domain");
	{

		if( !LoadDomain(tp,m_origin, m_region, m_numVoxel, m_numDivision,m_activeSubdomainFile) )
		{
			VXLogE("Read File Error (%s) [%s:%d] \n", filepath.c_str(), __FILE__, __LINE__);
			delete tp;
			return false;
		}
	}
			
	size_t numProcs = 0;
	tp->changeNode("/MPI");
	{
		vector<string> lbls;
		tp->getLabels(lbls);
		for(vector<string>::iterator it = lbls.begin(); it != lbls.end(); ++it){
			string valStr;
			tp->getValue(*it, valStr);
			if( CompStr(*it, "NumberOfRank") == 0){
				numProcs = atoi(valStr.c_str());
				continue;
			}
		}
	}
			
	std::vector<MetaBlock> metas;

	tp->changeNode("/Process");
	{
		vector<string> nodes;
		tp->getNodes(nodes, 1);
		if( nodes.size() != numProcs )
		{
			VXLogW("it is different to NumberOfRank and Process of Rank count\n");
		}
				
		metas.reserve(nodes.size());
		for(vector<string>::iterator nit = nodes.begin(); nit != nodes.end(); ++nit){
			if( CompStr(nit->substr(0, 4), "Rank") == 0 ){
				tp->changeNode(*nit);
				int id=0;
				int CellID=0;
				int BCflagID=0;
				idx3 size, hIdx, tIdx;

				if( !LoadProcRank(tp, id, size, hIdx, tIdx , CellID , BCflagID ) ){
					VXLogE("Read File Error (%s) [%s:%d] \n", filepath.c_str(), __FILE__, __LINE__);
					delete tp;
					return false;
				}
				metas.push_back(MetaBlock(id, size, hIdx, tIdx,CellID,BCflagID));

				tp->changeNode("../");
			}
		}
	}
	delete tp;


	m_locals.clear();
	VoxelCartesianParam::MODE mode = m_param.GetMode();
	bool ret = false;
	if(mode == VoxelCartesianParam::FUNC_DIR){
		//ディレクトリでロード
		ret = loadByDir(metas , m_locals );
	}else if(mode == VoxelCartesianParam::FUNC_DIRECT_FILE_CELLID || mode == VoxelCartesianParam::FUNC_DIRECT_FILE_BCFLG ){
		ret = loadByFileDirect(metas , m_locals );	
	}else{
		assert(false);
		VXLogE("Read File Error (%s) [%s:%d] \n", filepath.c_str(), __FILE__, __LINE__);
		return false;
	}
	return ret;

}
		
bool VoxelCartesianDfiLoader::loadByFileDirect( const std::vector<MetaBlock>& metas , std::vector<VX::SG::VoxelCartesianL*>& locals )
{

	bool isCellIDType = m_param.GetMode() == VoxelCartesianParam::FUNC_DIRECT_FILE_CELLID ; 
	std::string direct_file;
	if(isCellIDType){
		assert(m_param.Has_CellID_file());
	}else{
		assert(m_param.Has_BCflg_file());
	}

	direct_file = (isCellIDType)? m_param.Get_CellID_file(): m_param.Get_BCflg_file();
	u32 rankNo =0;
	bool ret = getRankNoByFileName(direct_file, rankNo);
	if(!ret){
		assert(false);
		VXLogE("Read File Error .file name does't have Rank number (%s) [%s:%d] \n", direct_file.c_str(), __FILE__, __LINE__);
		return false;
	}

	const MetaBlock* pmeta = NULL;
	for(s32 i = 0; i < static_cast<s32>(metas.size()); i++){
		const MetaBlock& meta = metas[i];
		if(meta.id == rankNo){
			pmeta = &meta;
			break;
		}
	}
	if(pmeta==NULL){
		VXLogE("Read File Error .file name does't have Rank number in index.dfi (%s) [%s:%d] \n", direct_file.c_str(), __FILE__, __LINE__);
		return false;
	}

	//////////////////////////////////
	// subdomain毎に適切な値を振る
	VX::Math::vec3 l_origin ;
    VX::Math::vec3 l_region ;
	VX::Math::idx3 l_divs ;
	VX::Math::idx3 l_voxSize ;
	getSubDomainParam(*pmeta,l_origin,l_region,l_divs,l_voxSize);
		
    // file fullpath
    std::string file_CellID		;
    std::string file_BCflagID	;
    //固定値の設定
	s32 CellID;
	s32 BCflagID ;
	if(isCellIDType){
		file_CellID		=   m_param.Get_CellID_file();
		file_BCflagID	= "";
		CellID = -1;
		BCflagID = VX::SG::VoxelCartesianL::BCFLAG_NONE;
	}else{
		file_CellID		= "";
		file_BCflagID	= m_param.Get_BCflg_file();
		//固定値の設定
		CellID = -1;
		BCflagID = -1;
	}

	VX::SG::VoxelCartesianL * local = vxnew VX::SG::VoxelCartesianL(
		pmeta->id, l_origin,l_region,l_voxSize,l_divs, 
		pmeta->hIdx,
		file_CellID, 
		file_BCflagID,
		CellID,
		BCflagID,
		m_ib.isNeedSwap,//共通部のデータ
		m_ib.gc,
		get_digit_rank(),
		get_digit_grid()
		);



	locals.push_back( local );
	return true;
}

int VoxelCartesianDfiLoader::get_digit_rank() const
{
	//m_numDivisionの桁数 ランク総数
	int digit = m_numDivision[0]*m_numDivision[1]*m_numDivision[2];

	char dummy[100];
	int ret = sprintf(dummy, "%d",digit); 
	return ret;
}
int VoxelCartesianDfiLoader::get_digit_grid() const
{
	//m_numVoxelのxyzの内で最大の桁数
	int max = (std::max)((std::max)(m_numVoxel[0],m_numVoxel[1]),m_numVoxel[2]);

	char dummy[100];
	int ret = sprintf(dummy, "%d",max); 
	return ret;

}

bool VoxelCartesianDfiLoader::loadByDir(const std::vector<MetaBlock>& metas , std::vector<VX::SG::VoxelCartesianL*>& locals )
{
	//CellID dir か BCFlg dirのどちらかは存在するはず
	assert(m_param.Has_CellID_dir() || m_param.Has_BCflg_dir());

	std::string cid_dir = m_param.Get_CellID_dir();
	std::string bcf_dir = m_param.Get_BCflg_dir();

	// Load Block Files	 _CellID
	locals.clear();
	locals.assign(metas.size(),NULL);

	//#pragma omp parallel for
	for(s32 i = 0; i < static_cast<s32>(metas.size()); i++)
	{
		
		const MetaBlock& meta = metas[i];

		//////////////////////////////////
		// subdomain毎に適切な値を振る
		VX::Math::vec3 l_origin ;
        VX::Math::vec3 l_region ;
		VX::Math::idx3 l_divs ;
		VX::Math::idx3 l_voxSize ;
		getSubDomainParam(meta,l_origin,l_region,l_divs,l_voxSize);
		
        // file fullpath
        char cid_name[100];
        sprintf(cid_name, "cid_%06d.bvx", metas[i].id);
        std::string file_CellID =  cid_dir +"/" + cid_name;
        char bcf_name[100];
        sprintf(bcf_name, "bcf_%06d.bvx", metas[i].id);
        std::string file_BCflagID;
		file_BCflagID = bcf_dir +"/" + bcf_name;
   
		//固定値のチェック
		if(meta.CellID == -1){
			assert(file_CellID.size()>0);
		}else{
			//ロードしないようにパスを消す
			file_CellID = "";
		}
		//CIDのディレクトリが指定されてない場合（BCFlagのみの場合）cellIDを読まないようにパスをクリアする
		if(cid_dir.size()==0){
			file_CellID = "";
		}

		if(meta.BCflagID == -1){
			assert(file_BCflagID.size()>0);
		}else if(meta.BCflagID == VX::SG::VoxelCartesianL::BCFLAG_NONE){
			//定義自体が無い
			//ロードしないようにパスを消す
			file_BCflagID = "";
		}else{
			//ロードしないようにパスを消す
			file_BCflagID = "";
		}

		VX::SG::VoxelCartesianL * local = vxnew VX::SG::VoxelCartesianL(
			meta.id, l_origin,l_region,l_voxSize,l_divs, 
			meta.hIdx,
			file_CellID, 
			file_BCflagID,
			meta.CellID,
			meta.BCflagID,
			m_ib.isNeedSwap,//共通部のデータ
			m_ib.gc,
			get_digit_rank(),
			get_digit_grid()
			);

		locals[i] = local;
		// cellID , flag ファイルはロードしない。
	}
	return true;
}

void VoxelCartesianDfiLoader::getSubDomainParam(const MetaBlock& meta,
		VX::Math::vec3& l_origin , VX::Math::vec3& l_region ,	VX::Math::idx3& l_divs ,VX::Math::idx3& l_voxSize )
{
	VX::Math::vec3 g_origin		= GetOrigin();
    VX::Math::vec3 g_region		= GetRegion();
	VX::Math::idx3 g_voxSize	= GetNumVoxels();
	VX::Math::idx3 g_divs		= GetNumDivs();

	VX::Math::vec3 pitch;
	for(int i=0;i<3;i++){
		pitch[i] = g_region[i] / g_voxSize[i];

		l_origin[i] = g_origin[i] + ( (meta.hIdx[i] - 1) * pitch[i] ); 
		l_region[i] = pitch[i] * meta.size[i];

	}

	// voxel sum
	l_voxSize = meta.size;

	//fix in case of local
	l_divs.x = 1;
	l_divs.y = 1;
	l_divs.z = 1;

}