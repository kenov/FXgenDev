//
//  STLAsciiLoader.h
//

#ifndef INCLUDE_VX_STLASCIILOADER_H
#define INCLUDE_VX_STLASCIILOADER_H

#include "../VX/Type.h"
#include "BaseLoader.h"

namespace VX {
	namespace SG{
		class Node;
	}
}
	
class STLAsciiLoader : BaseLoader
{
public:
	STLAsciiLoader();
	~STLAsciiLoader();
	
	VX::SG::Node* Load(const VX::Stream* st);
};
	
#endif // INCLUDE_VX_STLASCIILOADER_H

