#include "../VX/VX.h"
#include "../VX/SG/SceneGraph.h"

#include "FaceBCLoader.h"

#include "TextParser.h"

#define MODULE_NAME "FaceBC Load"

namespace {

	inline
	int CompStr( const std::string& str1, const std::string& str2, bool ignorecase=true )
	{
		std::string lstr1 = str1;
		std::string lstr2 = str2;
		if( ignorecase ){
			std::transform(lstr1.begin(), lstr1.end(), lstr1.begin(), ::tolower);
			std::transform(lstr2.begin(), lstr2.end(), lstr2.begin(), ::tolower);
		}

		return lstr1.compare(lstr2);
	}

} // namespace


FaceBCLoader::FaceBCLoader()
{

}

FaceBCLoader::~FaceBCLoader()
{

}

b8 FaceBCLoader::Load(const std::string& filepath, const VX::SG::Group* setting, VX::SG::Domain* domain)
{
	using namespace std;
	using namespace VX::SG;
	
	if(!setting){ VXLogE("Setting Node is NULL.\n"); return false; }
	if(!domain) { VXLogE("Domain Node is NULL.\n");  return false; }

	int err = 0;
	TextParser *tp = new TextParser();
	
	tp->remove();

	err = tp->read(filepath);
	if(err != 0){ delete tp; return false; }	
	
	bool readFlag[6] = {false, false, false, false, false, false};
	string sFlowBCs[6];
	//string sMediums[6];
	
	map<int, const char*> dirTag;
	dirTag.insert( std::pair<int, const char*>(0, "Xminus") );
	dirTag.insert( std::pair<int, const char*>(1, "Xplus")  );
	dirTag.insert( std::pair<int, const char*>(2, "Yminus") );
	dirTag.insert( std::pair<int, const char*>(3, "Yplus")  );
	dirTag.insert( std::pair<int, const char*>(4, "Zminus") );
	dirTag.insert( std::pair<int, const char*>(5, "Zplus")  );

	// read from TextParser
	err = tp->changeNode("/BCTable/OuterBoundary/FaceBC");

	// medium have relationship inside OuterBoundary

	if( err != TP_NO_ERROR ){
		return false;	
	}

	vector<string> lbls;
	tp->getLabels(lbls);
	for(vector<string>::iterator it = lbls.begin(); it != lbls.end(); ++it){
		string attr_key = *it;
		string attr_val;
		tp->getValue(*it, attr_val);
	
		for(map<int, const char*>::iterator tagIt = dirTag.begin(); tagIt != dirTag.end(); ++tagIt){
			
			if(CompStr(attr_key, tagIt->second) == 0){
				int index = tagIt->first;
				sFlowBCs[index] = attr_val;
				readFlag[index] = true;
				break;
			}
		}
	}

	const OuterBC* pFlowBCs[6]    = { NULL, NULL, NULL, NULL, NULL, NULL };
	//const Medium*  pMeds[6]       = { NULL, NULL, NULL, NULL, NULL, NULL };
	
	// check read results
	for(int i = 0; i < 6; i++){
		if(!readFlag[i]){ delete tp; return false; }
		
		bool isFindFbc = false;
		//bool isFindMed = false;
		for(int j = 0; j < setting->GetChildCount(); j++) {
			const NODETYPE type = setting->GetChild(j)->GetType();
			if( type == NODETYPE_OUTERBC ) {
				const OuterBC* bc = dynamic_cast<const OuterBC*>(setting->GetChild(j));
				if( CompStr(bc->GetAlias(), sFlowBCs[i]) == 0 ){
					pFlowBCs[i] = bc;
					isFindFbc = true;
					break;
					//continue;
				}
			}
			/*
			else if ( type == NODETYPE_MEDIUM ) {
				const Medium* med = dynamic_cast<const Medium*>(setting->GetChild(j));
				if( CompStr(med->GetLabel(), sMediums[i]) == 0 ){
					pMeds[i] = med;
					isFindMed = true;
					continue;
				}
			}
			if( isFindFbc && isFindMed ) break;
			*/
			
		}

		if(pFlowBCs[i] == NULL){
			delete tp;
			return false;
		}
		/*
		if(pMeds[i] == NULL){
			delete tp;
			return false;
		}
		*/
	}

	
	// Set BC/Medium to domain
	for(int i = 0; i < 6; i++){
		
		assert(pFlowBCs[i]!=NULL);

		domain->SetFlowBC(static_cast<Domain::BC_DIRECTION>(i), pFlowBCs[i]);

		//domain->SetGuideCellMedium(static_cast<Domain::BC_DIRECTION>(i), pMeds[i]);

		std::string name = pFlowBCs[i]->GetMedium();
		const Medium* pMed = NULL;
		for(int j = 0; j < setting->GetChildCount(); j++) {
			const NODETYPE type = setting->GetChild(j)->GetType();
			if ( type == NODETYPE_MEDIUM ) {
				const Medium* med = dynamic_cast<const Medium*>(setting->GetChild(j));
				if( CompStr(med->GetLabel(), name) == 0 ){
					pMed = med;
					break;
				}
			}
		}
		if(pMed!=NULL){
			domain->SetGuideCellMedium(static_cast<Domain::BC_DIRECTION>(i), pMed);
		}else{
			assert(false);
		}

	}
	
	delete tp;
	return true;
}

