/*
 *
 * FileIO/VoxelCartesianDfiLoader.h
 *
 *
 */

#ifndef __VoxelCartesianDfiLoader_H__
#define __VoxelCartesianDfiLoader_H__

#include "../VX/VX.h"
#include "../VX/Math.h"
#include <string>
#include <vector>
#include "../VX/SG/SceneGraph.h"
#include "TextParser.h"
#include "VoxelCartesianParam.h"


class VoxelCartesianDfiLoader
	{
	public:
        VoxelCartesianDfiLoader(const VoxelCartesianParam& param);
		~VoxelCartesianDfiLoader();
		void Clear();
		const b8 IsOK() const;
		const VX::Math::vec3& GetOrigin()    const;
		const VX::Math::vec3& GetRegion()    const; 
		const VX::Math::idx3& GetNumDivs()   const;
		const VX::Math::idx3& GetNumVoxels() const ;
		std::vector<VX::SG::VoxelCartesianL*>& GetLocals()  ;
		s32 GetOneSubdomainBytesize() const;
		void SetOneSubdomainBytesize();

	private:
		struct idxBlock
		{
			std::string dir;
			std::string prefix;
			bool isNeedSwap;
			u32         gc;
			idxBlock() : gc(0),isNeedSwap(false){}
		};

		struct MetaBlock
		{
			u32 id;
			s32 CellID;
			s32 BCflagID;

			VX::Math::idx3 size;
			VX::Math::idx3 hIdx;
			VX::Math::idx3 tIdx;
	
			MetaBlock(const u32 _id, const VX::Math::idx3& _size, 
				const VX::Math::idx3& _hIdx, const VX::Math::idx3& _tIdx,
						const s32 _CellID,const s32 _BCflagID)
			 : id(_id), size(_size), hIdx(_hIdx), tIdx(_tIdx), CellID(_CellID), BCflagID(_BCflagID) { }
		};
	private:
		int get_digit_rank() const;
		int get_digit_grid() const;

		b8 LoadDomain(TextParser* tp, VX::Math::vec3& org, VX::Math::vec3& rgn,
			VX::Math::idx3& vox, VX::Math::idx3& div,	std::string& activeSubdomainFile);

		// Load index.dfi
		b8 LoadIndex(const std::string& filepath);

		b8 LoadProcRank(TextParser* tp, int &id, VX::Math::idx3& voxelSize, 
			VX::Math::idx3& headIndex, VX::Math::idx3& tailIndex,
						int &CellID,int &BCflagID);



		void getSubDomainParam(const MetaBlock& meta,
					VX::Math::vec3& l_origin , VX::Math::vec3& l_region ,VX::Math::idx3& l_divs ,VX::Math::idx3& l_voxSize );

		// Load Proc.dif and Block files
		b8 LoadBlocks();
		
		std::string getProcFile();
		bool strcmp(std::string lhs, std::string rhs);
        
        bool getRankNoByFileName(const std::string& file , u32& rankNo);
        bool IsDicectFileBCFlg_Mode(u32& rankNo);

		bool loadByDir(const std::vector<MetaBlock>& metas , std::vector<VX::SG::VoxelCartesianL*>& locals );
		bool loadByFileDirect( const std::vector<MetaBlock>& metas , std::vector<VX::SG::VoxelCartesianL*>& locals );


	private:
		b8 m_isOK;

		std::string		m_rootDirectory;
		VoxelCartesianParam	m_param;

		std::string		m_procFilename;
		idxBlock		m_ib;

		int				m_numProcs;
		VX::Math::vec3	m_origin;
		VX::Math::vec3	m_region;
		VX::Math::idx3	m_numVoxel;
		VX::Math::idx3	m_numDivision;
		std::string		m_activeSubdomainFile;
		std::vector<VX::SG::VoxelCartesianL*> m_locals;
		s32				m_one_subdomain_bytesize;

};


#endif // __VoxelCartesianDfiLoader_H__