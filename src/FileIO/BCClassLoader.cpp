#include "../VX/VX.h"
#include "../VX/SG/SceneGraph.h"

#include "BCClassLoader.h"
#include "../FileIO/FileIOUtil.h"
#include "TextParser.h"

#include <vector>
#include <string>

#define MODULE_NAME "BC Class Load"

namespace {
} // namespace

BCClassLoader::BCClassLoader()
{

}


BCClassLoader::~BCClassLoader()
{

}

b8 BCClassLoader::Load(const std::string& filepath, VX::SG::Group* setting )
{
	using namespace std;
	using namespace VX::SG;

	int err = 0;
	TextParser *tp = new TextParser();
	
	// remove old data;
	tp->remove();

	err = tp->read(filepath);
	if(err != 0) { delete tp; return false; }

	vector<Node*> bcclassList;

	if( tp->changeNode("/OuterBCClassTable") == TP_NO_ERROR ){
		vector<string> nodes;
		tp->getNodes(nodes, 1);
		for(vector<string>::iterator nit = nodes.begin(); nit != nodes.end(); ++nit){
			if( CompStr(nit->substr(0, 5), "class") == 0) {
				tp->changeNode(*nit);

				vector<string> labels;
				if( tp->getLabels(labels) != TP_NO_ERROR ){
					VXLogW("[%s] Failed to parse OuterBCClass %s \n", MODULE_NAME, nit->c_str());
					continue;
				}
				string name;
				string thermalOption;
				b8 hasThermalOption = false;
				for(vector<string>::iterator lit = labels.begin(); lit != labels.end(); ++lit){
					string valstr;
					tp->getValue(*lit, valstr);
					if( CompStr( *lit, "label" ) == 0 ){
						name = valstr;
					}
					if( CompStr( *lit, "ThermalOption") == 0 ){
						thermalOption = valstr;
						hasThermalOption = true;
					}
				}

				if( name.length() == 0 ){ 
					VXLogW("[%s] OuterBCClass %s has no label\n", MODULE_NAME, nit->c_str());
					continue;
				}
				OuterBCClass* bcclass;
				if( hasThermalOption ){
					bcclass = vxnew OuterBCClass(name, thermalOption);
				}else{
					bcclass = vxnew OuterBCClass(name);
				}

				bcclassList.push_back(bcclass);
				tp->changeNode("../");
			}else{
				continue;
			}
		}
	}else{
		VXLogW("[%s] No OuterBCClass\n", MODULE_NAME);
	}

	if( tp->changeNode("/LocalBCClassTable") == TP_NO_ERROR ){
		vector<string> nodes;
		tp->getNodes(nodes, 1);
		//b8 err = false;
		for(vector<string>::iterator nit = nodes.begin(); nit != nodes.end(); ++nit){
			if( CompStr(nit->substr(0, 5), "class") == 0) {
				tp->changeNode(*nit);

				vector<string> labels;
				if( tp->getLabels(labels, 1) != TP_NO_ERROR ){
					VXLogW("[%s] Failed to parse LocalBCClass %s \n", MODULE_NAME, nit->c_str());
					continue;
				}

				string name;
				string thermalOption;
				//b8 hasThermalOption = false;
				for(vector<string>::iterator lit = labels.begin(); lit != labels.end(); ++lit){
					string valstr;
					tp->getValue(*lit, valstr);
					if( CompStr( *lit, "label" ) == 0 ){
						name = valstr;
					}
				}

				if( name.length() == 0){
					VXLogW("[%s] LocalBCClass %s has no label\n", MODULE_NAME, nit->c_str());
					continue;
				}
				LocalBCClass* bcclass = vxnew LocalBCClass(name);
				bcclassList.push_back(bcclass);

				tp->changeNode("../");
			}else{
				continue;
			}
		}
	}else{
		VXLogW("[%s] No LocalBCClass\n", MODULE_NAME);
	}

	tp->remove();

	if( bcclassList.size() == 0 ){
		delete tp;
		return false;
	}

	// Delete old BC Class
	for(int i = setting->GetChildCount()-1; i >=0; i--){
		Node* n = setting->GetChild(i);
		NODETYPE type = n->GetType();
		if( type == NODETYPE_LOCALBCCLASS || type == NODETYPE_OUTERBCCLASS )
		{
			setting->RemoveChild(n);
		}
	}

	// Add new BC Class
	for(int i = 0; i < bcclassList.size(); i++){
		Node* n = bcclassList[i];
		NODETYPE type = n->GetType();
		if( type == NODETYPE_LOCALBCCLASS || type == NODETYPE_OUTERBCCLASS )
		{
			setting->AddChild(n);
		}
	}

	delete tp;
	return true;
}


