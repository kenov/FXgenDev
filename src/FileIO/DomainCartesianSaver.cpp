#include "../VX/VX.h"
#include "../VX/Math.h"
#include "../VX/SG/SceneGraph.h"

#include "DomainCartesianSaver.h"
#include "FileIOUtil.h"

#include <fstream>
#include <sstream>

namespace {

	b8 SaveSubdomain(const std::string& path, const u32 x, const u32 y, const u32 z, const u8 *subdomain)
	{
		FILE *fp = fopen(path.c_str(), "wb");
		if(!fp){
			VXLogE("File Open Error. (%s)\n", path.c_str());
			return false;
		}
	
		u32 FMT = ('S' | ('B' << 8) | ('D' << 16) | ('M' << 24));
		if( fwrite(&FMT, sizeof(u32), 1, fp) != 1)
		{
			VXLogE("File Write Error. (%s)\n", path.c_str());
			fclose(fp);
			return false;
		}
	
		const u32 size[3] = { x, y, z };
		if( fwrite(size, sizeof(u32), 3, fp) != 3 )
		{
			VXLogE("File Write Error. (%s)\n", path.c_str());
			fclose(fp);
			return false;
		}
	
		if( fwrite(subdomain, sizeof(u8), x * y * z, fp) !=  x * y * z )
		{
			VXLogE("File Write Error. (%s)\n", path.c_str());
			fclose(fp);
			return false;
		}
	
		fclose(fp);
		return true;
	}


} // namespace




b8 WriteDomainCartesian( std::ostringstream& os, const VX::SG::Node* node, const std::string& writeDir, const std::string& subdomainFilename)
{
	using namespace std;
	using namespace VX::SG;
	using namespace VX::Math;

	if( !node ){ return false; }
	if( node->GetType() != NODETYPE_BBOX_DOMAIN_CARTESIAN )
	{
		VXLogE("Node type is not Cartesian domain\n");
		return false; 
	}

	const DomainCartesian* domain = dynamic_cast<const DomainCartesian*>(node);

	vec3 min, max;

	VX::Math::idx3 vox, div;

	bool hasVox = true;
	bool hasDiv = true;

	domain->GetBBoxMinMax(min, max);
	domain->GetVoxelCount(vox);
	domain->GetDivCount(div);

	if( vox[0] == 1 && vox[1] == 1 && vox[2] == 1 )
		hasVox = false;
	
	if( div[0] == 1 && div[1] == 1 && div[2] == 1 )
		hasDiv = false;

	const vec3 org = min;
	const vec3 rgn = max-min;

	vec3 pth;
	if(hasVox){
		for(int i = 0; i < 3; i++){
			pth[i] = rgn[i] / static_cast<float>(vox[i]);
		}
	}

	const std::string& unit = domain->GetUnit();

	os << "DomainInfo {" << endl;
	os << "  " << "UnitOfLength        =";
	os << " \"" << unit << "\"" << endl;
	os << "  " << "GlobalOrigin        =";
	os << " (" << org[0] << ", " << org[1] << ", " << org[2] << ")" << endl;
	os << "  " << "GlobalRegion        =";
	os << " (" << rgn[0] << ", " << rgn[1] << ", " << rgn[2] << ")" << endl;

	if(hasVox){
		if(domain->GetExportPolicy() == VX::SG::DomainCartesian::EXP_VOXEL_PITCH){
			os << "  " << "GlobalPitch         =";
			os << " (" << pth[0] << ", " << pth[1] << ", " << pth[2] << ")" << endl;
		}else{
			os << "  " << "GlobalVoxel         =";
			os << " (" << vox[0] << ", " << vox[1] << ", " << vox[2] << ")" << endl;
		}
	}
	

	if(hasDiv){
		os << "  " << "GlobalDivision      =";
		os << " (" << div[0] << ", " << div[1] << ", " << div[2] << ")" << endl;

		os << "  " << "ActiveSubDomainFile = \"" << subdomainFilename << "\"" << endl;

	}
	os << "}" << endl; // DomainInfo

	if(hasDiv){
		const std::string subdomainAbsoluteFilepath = writeDir + "/" + subdomainFilename;
		if(!SaveSubdomain(subdomainAbsoluteFilepath, div[0], div[1], div[2], domain->GetSubdomain())){
			VXLogE("Write Subdomain Error. (%s)\n", subdomainAbsoluteFilepath.c_str());
			return false;
		}
	}

	return true;
}


DomainCartesianSaver::DomainCartesianSaver()
{

}

DomainCartesianSaver::~DomainCartesianSaver()
{

}

b8 DomainCartesianSaver::Save(const std::string& filepath, const VX::SG::Node* node)
{

	std::ostringstream os;
	os.setf(std::ios::scientific);
	os.precision(6);


	const std::string subdomainFilename = GetFilePrefix(filepath) + ".asd";
	
	if( !WriteDomainCartesian( os, node, GetDirectory(filepath), subdomainFilename ) ){
		VXLogE("[Domain Cartesian Save] Failed to write domain\n");
		return false;
	}

	std::ofstream ofs( filepath.c_str() );
	if( !ofs ){
		VXLogE("File open error.(%s)\n", filepath.c_str());
		return false;
	}
	ofs << os.str();

	return true;
}

