/*
 *
 * FileIO/DomainLoader.h
 *
 */

#ifndef __DOMAIN_CARTESIAN_LOADER_H__
#define __DOMAIN_CARTESIAN_LOADER_H__

namespace VX {
	namespace SG {
		class Node;
	} // namespace SG
} // namespace VX

class DomainCartesianLoader
{
public:
	DomainCartesianLoader();
	~DomainCartesianLoader();

	VX::SG::Node* Load(const std::string& filepath);
};

#endif // __DOMAIN_CARTESIAN_LOADER_H__

