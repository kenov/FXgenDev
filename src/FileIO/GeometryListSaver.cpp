
#include "../VX/VX.h"
#include "../VX/SG/SceneGraph.h"

#include "GeometryListSaver.h"
#include "DomainCartesianSaver.h"
#include "DomainBCMSaver.h"
#include "BCMediumListSaver.h"
#include "FaceBCSaver.h"
#include "FileIOUtil.h"

#include <vector>
#include <string>

#include <fstream>
#include <sstream>

#define MODULE_NAME "Geometry List Save"

#define FLOAT_RRECISION_ORDER (6)
/*
GeometryList {
	BaseDir = "./"
	Type = "auto"
	File[@] {
         path = "a1.stl"
	}
	File[@] {
         path = "geometry_list/a/a1.stl"
	}
	File[@] {
         path = "geometry_list/a/a2.stl"
	}
	File[@] {
         path = "geometry_list/b/b1.stl"
	}
	File[@] {
         path = "geometry_list/b/b2.stl"
	}
}
*/
b8 GeometryListSaver::Save(const std::string& filepath, const VX::SG::Node* sgroot)
{
	using namespace std;
	using namespace VX::SG;

	VX::SG::Node* node = findSTLRoot(sgroot);
	if(node ==NULL){
		return false;
	}

	ostringstream os;
	os.setf(std::ios::scientific);
	// precision 
	os.precision(FLOAT_RRECISION_ORDER);

	os << "GeometryList {" << endl;
	os << std::endl;
	


	bool visibleOnly = true;

	std::string basedir = "./"; 
	os << "	BaseDir = \"" << basedir << "\"" <<endl;
	os << endl;

	//再帰オプションはデフォルトＯＦＦ
	os << "	Recursive = \"false\"" <<endl;
	os << endl;
	

	//STL root 配下をループ
	Group* g = static_cast<Group*>(node);
	const s32 n = g->GetChildCount();
	for (s32 i = 0; i < n; i++){
		//STL フォルダの第一子をループ
		std::string filepath_wk = "";
		dumpTree(g->GetChild(i),  os, visibleOnly,filepath_wk);
	}

	os << "}" << endl;
	os << endl;

	//////////////////
	// check if possible to open file that I created.
	ofstream ofs( filepath.c_str() );
	if( !ofs ){
		VXLogE("[%s] File open error.(%s)\n", MODULE_NAME, filepath.c_str());
		return false;
	}

	ofs << os.str();

	return true;
}

VX::SG::Node* GeometryListSaver::findSTLRoot(const VX::SG::Node* sgroot)
{
	using namespace VX::SG;
	//"/root/Model/"
	VX::SG::Node* nd = const_cast<VX::SG::Node*>(sgroot);
	Group* g = static_cast<Group*>(nd);
	const s32 n = g->GetChildCount();
	for (s32 i = 0; i < n; i++){
		// find "STL"
		if( g->GetChild(i)->GetName() == "Model"){
			return g->GetChild(i);
		}
	}
	return NULL;
}


void GeometryListSaver::dumpTree( VX::SG::Node* node, std::ostringstream& os , const b8& visibleOnly,const std::string& filepath 
									)
{
	if (!node)
		return;
		
	if (node->GetIntermediate())
		return;
		
	if (visibleOnly && !node->GetVisible())
		return;

	std::string name = node->GetName();
	std::string self_path;
	if(filepath.size()!=0){
		self_path = filepath + "/";
	}
	
	self_path += name;
	

	// self dump
	using namespace VX::SG;
	using namespace VX::Math;

	NODETYPE t = node->GetType();
	
	if (t == NODETYPE_GEOMETRY)
	{
		// write myself
		self_path += ".stl";
		using namespace VX::Math;
		Geometry* g = static_cast<Geometry*>(node);
		writeNode(g,os,self_path);
		return;
	}

	// call children
	if (t == NODETYPE_GROUP || t == NODETYPE_TRANSFORM)
	{
		Group* g = static_cast<Group*>(node);
		const s32 n = g->GetChildCount();
		for (s32 i = 0; i < n; i++){
			dumpTree(g->GetChild(i),  os, visibleOnly,self_path);
		}
	}	
}



void GeometryListSaver::writeNode( VX::SG::Geometry* node, std::ostringstream& os , 
									const std::string& stl_filepath )
{

	using namespace std;

	os << "	File[@] {" <<endl;
	os << "         path = \"" << stl_filepath << "\"" <<endl;
	os << "	}" << endl;
	os << std::endl;

}