/*
 *
 * FileIO/DomainBCMSaver.h
 *
 */

#ifndef __DOMAIN_BCM_SAVER_H__
#define __DOMAIN_BCM_SAVER_H__

namespace VX {
	namespace SG {
		class Node;
	} // namespace SG
} // namespace VX

b8 WriteDomainBCM( std::ostringstream& os, const VX::SG::Node* node, 
                   const std::string& writeDir, const std::string& octreeFilename = std::string("tree.oct") );

class DomainBCMSaver
{
public:
	DomainBCMSaver();
	~DomainBCMSaver();

	b8 Save(const std::string& filepath, const VX::SG::Node* node);
};

#endif // __DOMAIN_CARTESIAN_SAVER_H__

