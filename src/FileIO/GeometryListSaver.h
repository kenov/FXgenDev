/*
 *
 * FileIO/GeometryListSaver.h
 *
 */

#ifndef __GeometryListSaver_H__
#define __GeometryListSaver_H__

namespace VX {
	namespace SG {
		class Domain;
		class Group;
	}
}

class GeometryListSaver
{
public:
	GeometryListSaver(){ }
	~GeometryListSaver(){ }

	b8 Save(const std::string &filepath, const VX::SG::Node* sgroot);

private:
	void dumpTree( VX::SG::Node* node, std::ostringstream& os , const b8& visibleOnly,const std::string& filepath );
	void writeNode( VX::SG::Geometry* node, std::ostringstream& os , 
									const std::string& stl_filepath );
	VX::SG::Node* findSTLRoot(const VX::SG::Node* sgroot);

};

#endif // __ALL_PARAMETER_SAVER__

