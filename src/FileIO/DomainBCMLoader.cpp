#include "../VX/VX.h"
#include "../VX/SG/SceneGraph.h"

#include "TextParser.h"

#include "DomainBCMLoader.h"
#include "FileIOUtil.h"
#include "../VOX/VOXFileCommon.h"

#include "../VOX/Pedigree.h"

#include <string>
#include <vector>

#define MODULE_NAME "Domain BCM Load"

namespace {
	std::string GetOctreeFilepath(const std::string& domainPath, const std::string& octreeFilename )
	{
		if( octreeFilename.at(0) == '/' )
		{
			return octreeFilename;
		}

		std::string dir = GetDirectory(domainPath);
		
		return dir + "/" + octreeFilename;
	}
	
	class OctreeFileLoader {
	public:
		OctreeFileLoader(const std::string& filepath)
		{
			VOX::OctHeader header;
			m_isOK = VOX::LoadOctreeFile(filepath, header, m_pedigrees);

			if( !m_isOK ){
				return;
			}
			
			for(int i = 0; i < 3; i++){
				m_origin[i] = static_cast<float>(header.org[i]);
				m_region[i] = static_cast<float>(header.rgn[i]);
				m_rootDims[i] = header.rootDims[i];
			}
		}
		
		~OctreeFileLoader(){ }

		const VX::Math::vec3& GetOrigin() const { return m_origin; }
		const VX::Math::vec3& GetRegion() const { return m_region; }
		const VX::Math::idx3& GetRootDims() const { return m_rootDims; }
		const std::vector<VOX::Pedigree>& GetPedigrees() const { return m_pedigrees; }
		b8 IsOK() const { return m_isOK; }
		
	private:
		std::vector<VOX::Pedigree> m_pedigrees;
		b8 m_isOK;

		VX::Math::vec3 m_origin;
		VX::Math::vec3 m_region;
		VX::Math::idx3 m_rootDims;
		//s32 m_maxLevel;
	};

	VX::SG::DomainBCM::RegionScope* ReadRegionScope(TextParser* tp, std::string type, f32 ratio, f32 cell)
	{
		std::vector<std::string> lbls;
		tp->getLabels(lbls);

		bool hasOrigin = false;
		bool hasRegion = false;
		bool hasLevel  = false;
		
		VX::SG::DomainBCM::RegionScope* scope = vxnew VX::SG::DomainBCM::RegionScope();
		scope->type = type;
		scope->ratio = ratio;
		scope->cell = cell;

		for(std::vector<std::string>::iterator it = lbls.begin(); it != lbls.end(); ++it)
		{
			std::string valStr;
			tp->getValue(*it, valStr);

			if( CompStr(*it, "Origin") == 0 ){
				if( ReadVec3(tp, *it, scope->origin) == TP_NO_ERROR ) hasOrigin = true;
				continue;
			}

			if( CompStr(*it, "Region") == 0 ){
				if( ReadVec3(tp, *it, scope->region) == TP_NO_ERROR ) hasRegion = true;
				continue;
			}

			if( CompStr(*it, "Level") == 0 ){
				s32 val = atoi(valStr.c_str());
				if( val >= 0 || val < 16)
				{
					scope->level = val;
					hasLevel = true;
				}
				continue;
			}

			if( CompStr(*it, "Margin") == 0 ){
				if (CompStr(valStr, "Cell") == 0 || CompStr(valStr, "Ratio") == 0 ) {
					scope->type = valStr;
				}
				continue;				
			}

			if( CompStr(*it, "MarginRatio") == 0 ){
				f32 val = (f32)atof(valStr.c_str());
				scope->ratio = static_cast<f32>(val);
				continue;
			}

			if( CompStr(*it, "MarginCell") == 0 ){
				f32 val = (f32)atof(valStr.c_str());
				scope->cell = static_cast<f32>(val);
				continue;
			}
		}

		if( !hasOrigin || !hasRegion || !hasLevel )
		{
			vxdelete(scope);
			return NULL;
		}

		return scope;
	}

	VX::SG::DomainBCM::GeometryScope* ReadGeometryScope(TextParser* tp, std::string type, f32 ratio, f32 cell)
	{
		std::vector<std::string> lbls;
		tp->getLabels(lbls);
		
		VX::SG::DomainBCM::GeometryScope* scope = vxnew VX::SG::DomainBCM::GeometryScope();

		bool hasName  = false;
		bool hasLevel = false;
		scope->type = type;
		scope->ratio = ratio;
		scope->cell = cell;

		for(std::vector<std::string>::iterator it = lbls.begin(); it != lbls.end(); ++it)
		{
			std::string valStr;
			tp->getValue(*it, valStr);

			if( CompStr(*it, "Name") == 0 ){
				scope->name = valStr;
				hasName = true;
				continue;
			}

			if( CompStr(*it, "Level") == 0 ){
				s32 val = atoi(valStr.c_str());
				if( val >= 0 || val < 16)
				{
					scope->level = static_cast<u32>(val);
					hasLevel = true;
				}
				continue;
			}

			if( CompStr(*it, "Margin") == 0 ){
				if (CompStr(valStr, "Cell") == 0 || CompStr(valStr, "Ratio") == 0 ) {
					scope->type = valStr;
				}
				continue;				
			}

			if( CompStr(*it, "MarginRatio") == 0 ){
				f32 val = (f32)atof(valStr.c_str());
				scope->ratio = static_cast<f32>(val);
				continue;
			}

			if( CompStr(*it, "MarginCell") == 0 ){
				f32 val = (f32)atof(valStr.c_str());
				scope->cell = static_cast<f32>(val);
				continue;
			}
		}

		if( !hasName || !hasLevel )
		{
			vxdelete(scope);
			return NULL;
		}

		return scope;
	}

	b8 ReadDomainParameter(TextParser *tp, VX::Math::vec3& org, VX::Math::vec3& rgn)
	{
		if( !tp ) return false;

		if( tp->changeNode("/domain") != TP_NO_ERROR )
		{
			return false;
		}

		b8 hasOrigin = false;
		b8 hasRegion = false;

		std::vector<std::string> lbls;
		tp->getLabels(lbls);
		for(std::vector<std::string>::iterator it = lbls.begin(); it != lbls.end(); ++it)
		{
			std::string valStr;
			tp->getValue(*it, valStr);

			if( CompStr(*it, "GlobalOrigin") == 0 ){
				if( ReadVec3(tp, *it, org) == TP_NO_ERROR ) hasOrigin = true;
				continue;
			}

			if( CompStr(*it, "GlobalRegion") == 0 ){
				if( ReadVec3(tp, *it, rgn) == TP_NO_ERROR ) hasRegion = true;
				continue;
			}
		}

		if( !hasOrigin || !hasRegion )
		{
			tp->changeNode("/");
			return false;
		}
		
		tp->changeNode("/");
		return true;
	}

	b8 ReadTreeParameter(TextParser*                                     tp,
	                     VX::Math::idx3&                                 rootDims,
	                     u32&                                            baseLevel,
						 u32&                                            minLevel,
						 VX::SG::DomainBCM::LeafOrdering&                leafOrdering,
						 std::vector<VX::SG::DomainBCM::RegionScope*>&   rgnScopeList,
						 std::vector<VX::SG::DomainBCM::GeometryScope*>& geoScopeList )
	{
		if( !tp ) return false;

		if( tp->changeNode("/Tree") != TP_NO_ERROR )
		{
			return false;
		}
		
		// Read Tree ( other than MeshDensity )
		std::vector<std::string> lbls;
		tp->getLabels(lbls);
		for(std::vector<std::string>::iterator it = lbls.begin(); it != lbls.end(); ++it)
		{
			std::string valStr;
			tp->getValue(*it, valStr);

			if( CompStr(*it, "RootDims") == 0 ){
				ReadVec3(tp, *it, rootDims);
				continue;
			}

			if( CompStr(*it, "BaseLevel") == 0 ){
				s32 val = atoi(valStr.c_str());
				if( val >= 0 || val < 16 ) baseLevel = static_cast<u32>(val);
				continue;
			}

			if( CompStr(*it, "MinLevel") == 0 ){
				s32 val = atoi(valStr.c_str());
				if( val >= 0 || val < 16 ) minLevel = static_cast<u32>(val);
				continue;
			}
			
			if( CompStr(*it, "Ordering") == 0 ){
				if( CompStr(valStr, "Z") == 0 ){
					leafOrdering = VX::SG::DomainBCM::ORDER_Z;
				}
				if( CompStr(valStr, "Hilbert") == 0 ){
					leafOrdering = VX::SG::DomainBCM::ORDER_HILBERT;
				}
				continue;
			}

		}

		// Read Tree/MeshDensity
		if( tp->changeNode("MeshDensity") == TP_NO_ERROR )
		{
			std::string type = "Cell";
			f32 ratio = 0.0f;
			f32 cell = 1.0f;

			std::vector<std::string> marginParams;
			tp->getLabels(marginParams);

			for(std::vector<std::string>::iterator it = marginParams.begin(); it != marginParams.end(); ++it)
			{
				std::string valStr;
				tp->getValue(*it, valStr);

				if( CompStr(*it, "Margin") == 0 ){
					if ( CompStr(valStr, "Cell") == 0 || CompStr(valStr, "Ratio") == 0 ) {
						type = valStr;
					}
					continue;				
				}

				if( CompStr(*it, "MarginRatio") == 0 ){
					f32 val = (f32)atof(valStr.c_str());
					ratio = static_cast<f32>(val);
					continue;
				}

				if( CompStr(*it, "MarginCell") == 0 ){
					f32 val = (f32)atof(valStr.c_str());
					cell = static_cast<f32>(val);
					continue;
				}
			}

			VX::SG::DomainBCM::RegionScope *scopeMesh = new VX::SG::DomainBCM::RegionScope();
			scopeMesh->type = type;
			scopeMesh->ratio = ratio;
			scopeMesh->cell = cell;

			std::vector<std::string> nodes;
			tp->getNodes(nodes, 1);
			for(std::vector<std::string>::iterator nit = nodes.begin(); nit != nodes.end(); ++nit)
			{
				if( CompStr(nit->substr(0, 4), "BBox") == 0 ){
					tp->changeNode(*nit);

					VX::SG::DomainBCM::RegionScope* scope = ReadRegionScope(tp, type, ratio, cell);
					if( scope ) rgnScopeList.push_back(scope);

					tp->changeNode("../");	
				}

				if( CompStr(nit->substr(0, 7), "Polygon") == 0 ){
					tp->changeNode(*nit);

					VX::SG::DomainBCM::GeometryScope* scope = ReadGeometryScope(tp, type, ratio, cell);
					if( scope ) geoScopeList.push_back(scope);

					tp->changeNode("../");
				}
			}
			tp->changeNode("../");
			rgnScopeList.push_back(scopeMesh);
		}

		tp->changeNode("/");
		return true;
	}

	b8 ReadOctree(TextParser* tp, const std::string& dir, OctreeFileLoader* &octLoader)
	{
		if(!tp) return false;
		
		if( tp->changeNode("/BCMTree") != TP_NO_ERROR )
		{
			return false;
		}

		std::vector<std::string> lbls;
		tp->getLabels(lbls);
		for(std::vector<std::string>::iterator it = lbls.begin(); it != lbls.end(); ++it)
		{
			std::string valStr;
			tp->getValue(*it, valStr);
			if( CompStr(*it, "TreeFile") == 0 ){
				std::string octreeFilepath = GetOctreeFilepath(dir, valStr);
				octLoader = new OctreeFileLoader(octreeFilepath);
				if( !octLoader->IsOK() )
				{
					delete octLoader;
					octLoader = NULL;
				}
				continue;
			}
		}

		tp->changeNode("/");
		if( !octLoader ) return false;

		return true;
	}

	b8 ReadLeafBlock(TextParser* tp, VX::Math::idx3& blockSize, std::string& unit)
	{
		if(!tp) return false;
		
		if( tp->changeNode("/LeafBlock") != TP_NO_ERROR )
		{
			return false;
		}

		std::vector<std::string> lbls;
		tp->getLabels(lbls);
		for(std::vector<std::string>::iterator it = lbls.begin(); it != lbls.end(); ++it)
		{
			std::string valStr;
			tp->getValue(*it, valStr);
			
			if( CompStr(*it, "Size") == 0 ){
				ReadVec3(tp, *it, blockSize);
				continue;
			}

			if( CompStr(*it, "Length") == 0 ){
				unit = valStr;
				continue;
			}
		}

		tp->changeNode("/");
		return true;
	}


} // namespace 

DomainBCMLoader::DomainBCMLoader()
{

}

DomainBCMLoader::~DomainBCMLoader()
{

}

VX::SG::Node* DomainBCMLoader::Load(const std::string& filepath)
{

	VX::SG::DomainBCM* domain = NULL;

	std::string ext = getExt(filepath);
	if( ext == ".TP")
	{
		TextParser *tp = new TextParser;
		if( tp->read(filepath) != TP_NO_ERROR )
		{
			VXLogE("[%s] Load Domain Error (%s).\n", MODULE_NAME, filepath.c_str());
			delete tp;
			return NULL;
		}
		// Parameter File

		VX::Math::vec3 org;
		VX::Math::vec3 rgn;
		VX::Math::idx3 rootDims(1, 1, 1);
		u32 baseLevel = 0;
		u32 minLevel  = 0;
		VX::SG::DomainBCM::LeafOrdering leafOrdering = VX::SG::DomainBCM::ORDER_Z;

		std::vector<VX::SG::DomainBCM::RegionScope*>   rgnScopeList;
		std::vector<VX::SG::DomainBCM::GeometryScope*> geoScopeList;

		VX::Math::idx3 blockSize(1, 1, 1);
		std::string unit = "NonDimensional";

		VX::SG::DomainBCM::ExportPolicy exportPolicy = VX::SG::DomainBCM::EXP_PARAMETER;
		OctreeFileLoader* octLoader = NULL;

		// Read /Domain
		if( !ReadDomainParameter(tp, org, rgn) )
		{
			VXLogE("[%s] Load Domain Error (%s).\n", MODULE_NAME, filepath.c_str());
			delete tp;
			return NULL;
		}

		// Read /Tree
		ReadTreeParameter(tp, rootDims, baseLevel, minLevel, leafOrdering, rgnScopeList, geoScopeList);

		// Read /BCMTree
		if( ReadOctree(tp, GetDirectory(filepath), octLoader) )
		{
			rootDims = octLoader->GetRootDims();
			exportPolicy = VX::SG::DomainBCM::EXP_OCTREE;
		}

		// Read /LeafBlock
		ReadLeafBlock(tp, blockSize, unit );

		domain = vxnew VX::SG::DomainBCM();

		// Set Parameters
		VX::Math::vec3 min, max;
		min = org;
		max = org + rgn;
		domain->SetBBoxMinMax(min, max);
		domain->SetRootDims(rootDims);
		domain->SetBaseLevel(baseLevel);
		domain->SetMinLevel(minLevel);
		domain->SetBlockSize(blockSize);
		domain->SetLeafOrdering(leafOrdering);
//		domain->setMarginValue(rgnScopeList.back());
//		rgnScopeList.pop_back();

		std::vector<VX::SG::DomainBCM::RegionScope*>& _rgnScopeList = domain->GetRegionScopeList();
		_rgnScopeList.reserve(rgnScopeList.size());
		for(std::vector<VX::SG::DomainBCM::RegionScope*>::iterator it = rgnScopeList.begin(); it != rgnScopeList.end(); ++it)
		{
			_rgnScopeList.push_back(*it);
		}

		std::vector<VX::SG::DomainBCM::GeometryScope*>& _geoScopeList = domain->GetGeometryScopeList();
		_geoScopeList.reserve(geoScopeList.size());
		for(std::vector<VX::SG::DomainBCM::GeometryScope*>::iterator it = geoScopeList.begin(); it != geoScopeList.end(); ++it)
		{
			_geoScopeList.push_back(*it);
		}
		
		if( octLoader )
		{
			domain->CreateOctree(octLoader->GetPedigrees());
			exportPolicy = VX::SG::DomainBCM::EXP_OCTREE;
		}

		domain->SetExportPolicy(exportPolicy);

		if( octLoader ){
			delete octLoader;
		}
		delete tp;
	}
	else
	{
		// Octree File
		OctreeFileLoader octLoader(filepath);
		if( !octLoader.IsOK() )
		{
			return NULL;
		}
		domain = vxnew VX::SG::DomainBCM(octLoader.GetOrigin(), octLoader.GetRegion(), octLoader.GetRootDims(), octLoader.GetPedigrees());
		domain->SetExportPolicy(VX::SG::DomainBCM::EXP_OCTREE);
	}

	return domain;
}


