//
//  ObjLoader.cpp
//

#include "../VX/VX.h"
#include "../VX/SG/SceneGraph.h"
#include "../VX/Stream.h"

#include <string>
#include <set>

#include "ObjLoader.h"

#define IGNORE_NORMAL

namespace  {
    inline b8 keyncmp(const s8* a, const s8* b, s32 n)
    {
        for (s32 i = 0; i < n; i++) {
            if (a[i] != b[i])
                return false;
        }
        return true;
    }
    inline s32 searchReturn(const s8* a)
    {
        s32 i = 0;
        while(a[i] != 0x0A) i++; // LF
        return i;
    }
    inline s32 searchSpace(const s8* a)
    {
        s32 i = 0;
        while(a[i] != 0x20) ++i; // SPACE 
        return i;
    }
	inline s32 searchSlash(const s8* a)
    {
        s32 i = 0;
        while(a[i] != '/') ++i;
        return i;
    }
	
	inline s32 skipSpace(const s8* a)
    {
        s32 i = 0;
        while(a[i] == 0x20) ++i; // SPACE 
        return i;
    }
	

	inline s32 searchSpaceReturn(const s8* a)
    {
        s32 i = 0;
        while(a[i] != 0x20 && a[i] != 0x0A) ++i; // SPACE  or LF
        return i;
    }

	inline s32 spaceCount(const s8* a, s32* cnt)
    {
		(*cnt) = 0;
        s32 i = 0;
        while(a[i] != 0x0A) {
			if (a[i++] == 0x20)
				(*cnt)++;
		}
        return i;
    }
	
	inline s32 faceindexCount(const s8* a, s32* cnt)
	{
		(*cnt) = 0;
        s32 i = 0;
        while(a[i] != 0x0A) {
			if (a[i++] == 0x20 && isdigit(a[i]))
				(*cnt)++;
		}
        return i;
	}
	
	inline b8 endFile(u32 i, u32 n)
	{
		if (i > n)
			return true;
		else
			return false;
	}
	std::string withOutExt(const std::string& fname)
	{
		size_t p = fname.rfind(".");
		if (p != std::string::npos) {
			std::string t = fname;
			t = t.substr(0, p);
			return t;
		}
		return fname;
	}

	
	u32 astep_atoi (const char *p, s32* val)
	{
		const char* start_p = p;
		s32 sign = 1, value = 0;
		
		// skip white space
		while (white_space(*p))
			p += 1;
		
		// sign
		if (*p == '-') {
			sign = -1;
			p += 1;
		} else if (*p == '+') {
			p += 1;
		}
		
		while (valid_digit(*p)) {
			value = value * 10 + (*p - '0');
			p += 1;
		}
		
		*val = value * sign;
		return static_cast<u32>(p - start_p);
	}
	
	class DualIndex
	{
	public:
		VX::SG::Index vtx;
		VX::SG::Index nrm;
		DualIndex() {}
		DualIndex(VX::SG::Index vtx_, VX::SG::Index nrm_)
		{
			vtx = vtx_;
			nrm = nrm_;
		}
		/*bool operator< (const DualIndex& t) const
		{
			const u64 m  = (static_cast<u64>(vtx) << 32)   | nrm; 
			const u64 tm = (static_cast<u64>(t.vtx) << 32) | t.nrm; 
			return m < tm;
		}*/
	};
	class DualIndexIndex
	{
	public:
		u64 key;
		DualIndex* dualindexPtr;
		DualIndexIndex(DualIndex* idx)
		{
			key = (static_cast<u64>(idx->vtx) << 32) | idx->nrm;
			dualindexPtr = idx;
		}
		bool operator< (const DualIndexIndex& t) const
		{
			return key < t.key;
		}
		VX::SG::Index vertex() const
		{
			return static_cast<VX::SG::Index>(key >> 32);
		}
		VX::SG::Index normal() const
		{
			return static_cast<VX::SG::Index>(key & 0xFFFFFFFF);
		}
	};
}

	
ObjLoader::ObjLoader(){
}
ObjLoader::~ObjLoader(){
}
	
	
VX::SG::Node* ObjLoader::Load(const VX::Stream* st)
{	
	using namespace VX::Math;
	using namespace VX::SG;
	
	const s8* data = static_cast<const s8*>(st->GetData());
    u32 triCount   = 0;
	u32 vertexCount = 0;
	u32 normalCount = 0;
	u32 texCount    = 0;
	u32 n = st->GetSize();
	
	// scan
	for (u32 i = 0; i < n; i++) {
		if (data[i] == 'f'){
			i++;// 'f'
			i += skipSpace(&data[i]);
			s32 cnt = 0;
			i += faceindexCount(&data[i], &cnt);
			triCount += (cnt - 1);
	    }else if (keyncmp(&data[i],"vn", 2)){
#ifndef IGNORE_NORMAL
			normalCount++;
#endif
            i += searchReturn(&data[i]);
        }else if (keyncmp(&data[i],"vt", 2)){
			texCount++;
            i += searchReturn(&data[i]);
		}else if (data[i] == 'v'){
			vertexCount++;
			i += searchReturn(&data[i]);
		}
		else if (data[i] == 'g')                { i += searchReturn(&data[i]); }
		else if (data[i] == 'o')                { i += searchReturn(&data[i]); }
        else if (data[i] == '#')                { i += searchReturn(&data[i]); }
        else if (data[i] == '$')                { i += searchReturn(&data[i]); }
		else if (keyncmp(&data[i],"mtllib", 6)) { i += searchReturn(&data[i]); }
        else if (keyncmp(&data[i],"usemtl", 6)) { i += searchReturn(&data[i]); }
	}
	
	const u32 indexCount = triCount * 3;

	
	// alloc and store
	GeometryBVH* g = 0;
	s32 fcount = 0;
	s32 vcount = 0;
	s32 ncount = 0;
	Geometry::VertexFormat* vertexBuf   = 0;
	Geometry::VertexFormat* vertexBuf_v = 0;
	Geometry::VertexFormat* vertexBuf_n = 0;
	DualIndex* indexBuf = 0;
	Index* indexBuf_v = 0;
	Index* indexBuf_n = 0;
	u32 indexStride = 1;
	if (normalCount) {
		u32 maxcount = max(vertexCount, normalCount);
		vertexBuf = vxnew Geometry::VertexFormat[maxcount];
		vertexBuf_v = vertexBuf;
		vertexBuf_n = vertexBuf;
		indexBuf = vxnew DualIndex[indexCount];
		indexStride = 2;
		indexBuf_v = &(indexBuf->vtx);
		indexBuf_n = &(indexBuf->nrm);
	} else {
		g = vxnew GeometryBVH();
		g->SetName(withOutExt(st->GetFileName()));
		g->Alloc(vertexCount, indexCount);
		vertexBuf_v = g->GetVertex();
		vertexBuf_n = g->GetVertex();
		indexBuf_v = g->GetIndex();
		indexBuf_n = 0;
	}
	
	DualIndex tfs[32] = {};
	for (u32 i = 0; i < n; i++){
		if (data[i] == 'f'){
			i++;// 'f'
			i += skipSpace(&data[i]);
			// format is v/vt/vn format only
			s32 spccount = 0;
			//const char* cp = &data[i];
			faceindexCount(&data[i], &spccount);
			for (s32 fi = 0; fi < spccount + 1; fi++) {
				s32 tn;
				i += astep_atoi(&data[i], &tn);
				//assert(tn);
				if (tn < 0) // for negative index
					tfs[fi].vtx = vcount - tn;
				else
					tfs[fi].vtx = tn;
				if (data[i] == ' ')	continue; // VV pattern
				
				if (data[i] == '/') {
					i++;
					if (data[i] == '/') { 
						i++;// '/'
						i += astep_atoi(&data[i], &tn); tfs[fi].nrm = tn;
						continue; // VV//NN pattern
					} else {
						i += astep_atoi(&data[i], &tn);
						if (data[i] == ' ') continue; // VV/TT pattern
						i += astep_atoi(&data[i], &tn); tfs[fi].nrm = tn; // VV/TT/NN pattern
					}
				}
				i += searchSpaceReturn(&data[i]);
			}
			
			for (s32 fi = 0; fi < spccount - 1; fi++) {
				*indexBuf_v = tfs[0   ].vtx - 1; indexBuf_v += indexStride;
				*indexBuf_v = tfs[fi+1].vtx - 1; indexBuf_v += indexStride;
				*indexBuf_v = tfs[fi+2].vtx - 1; indexBuf_v += indexStride;
				if (indexBuf_n) {
					*indexBuf_n = tfs[0   ].nrm - 1; indexBuf_n += indexStride;
					*indexBuf_n = tfs[fi+1].nrm - 1; indexBuf_n += indexStride;
					*indexBuf_n = tfs[fi+2].nrm - 1; indexBuf_n += indexStride;
				}
				fcount++;
			}
			
			if (endFile(i,n)) // for exception
				break;
			
	    }else if (keyncmp(&data[i],"vn", 2)){
            i += 2;
#ifndef IGNORE_NORMAL
            i += skipSpace(&data[i]);
			vec3& nor = vertexBuf_n->normal;
			i += step_fatof(&data[i], &nor.x);
			i += step_fatof(&data[i], &nor.y);
			i += step_fatof(&data[i], &nor.z);
			vertexBuf_n++;
			ncount++;
#else
			i += searchReturn(&data[i]);
#endif
        }else if (keyncmp(&data[i],"vt", 2)){
            i += 2;
            i += searchReturn(&data[i]);
		}else if (data[i] == 'v'){
			i++;
			VX::Math::vec3& pos = vertexBuf_v->pos;
			vertexBuf_v->col = VX::initColor;
			i += VX::Math::step_fatof(&data[i], &pos.x);
			i += VX::Math::step_fatof(&data[i], &pos.y);
			i += VX::Math::step_fatof(&data[i], &pos.z);
#ifdef IGNORE_NORMAL
			vertexBuf_n->normal = vec3(0,0,0);
			vertexBuf_n++;
#endif
			vertexBuf_v++;
			vcount++;
	    }else if (data[i] == 'g'){
            i ++;
            i += searchReturn(&data[i]);
            // TODO
		}else if (data[i] == 'o'){
            i ++;
            i += searchReturn(&data[i]);
            // TODO			
        }else if (keyncmp(&data[i],"mtllib", 6)) {
            i += 6;
            i += searchReturn(&data[i]);
            // TODO
        }else if (keyncmp(&data[i],"usemtl", 6)) {
            i += 6;
            i += searchReturn(&data[i]);
            // TODO
        }else if (data[i] == '#'){ // comment
            i++;
            i += searchReturn(&data[i]);
        }else if (data[i] == '$'){ // comment
			i++;
			i += searchReturn(&data[i]);
		}

	}
	
	
//	assert(fcount == triCount);
	assert(vcount == vertexCount);
	assert(ncount == normalCount);

	// dupulicate vertecies
	if (!g) {
		g = vxnew GeometryBVH();
		g->SetName(withOutExt(st->GetFileName()));
		
		std::multiset<DualIndexIndex> didx;
		/*
		DualIndex* sortedidx = vxnew DualIndex[indexCount];
		memcpy(sortedidx, indexBuf, indexCount * sizeof(DualIndex));
		std::sort(sortedidx, sortedidx + indexCount);
		u32 maxcount = max(vertexCount, normalCount);// tmp
		*/
		DualIndex* ep = indexBuf + indexCount;
		for (DualIndex* p = indexBuf; p != ep; p++) {
			didx.insert(p);
		}

		// count new vertex num
		Index vertexcnt = 0;
		Index tmp_idx = 0xFFFFFFFF;
		std::multiset<DualIndexIndex>::iterator it, eit = didx.end();
		for (it = didx.begin(); it != eit; ++it) {
			if (tmp_idx != it->dualindexPtr->vtx) {
				vertexcnt++;
				tmp_idx = it->dualindexPtr->vtx;
			}
		}
		
		// generate new VB
		Index vptr = 0;
		Geometry::VertexFormat* new_vertexBuf = vxnew Geometry::VertexFormat[vertexcnt];
		tmp_idx = 0;
		for (it = didx.begin(); it != eit; ++it) {
			if (tmp_idx != it->dualindexPtr->vtx) {
				vptr++;
				tmp_idx = it->dualindexPtr->vtx;
			}
			new_vertexBuf[vptr].pos    = vertexBuf[it->vertex()].pos; 
			new_vertexBuf[vptr].normal = vertexBuf[it->normal()].normal;
//			new_vertexBuf[vptr].idx    = 0;
		}
		vxdeleteArray(vertexBuf);
		vertexBuf = new_vertexBuf;
		
		// ReIndexing
		vptr = 0;
		tmp_idx = 0;
		for (it = didx.begin(); it != eit; ++it) {
			if (tmp_idx != it->dualindexPtr->vtx) {
				vptr++;
				tmp_idx = it->dualindexPtr->vtx;
			}
			it->dualindexPtr->vtx = vptr;
		}

		VXLogD("vertex = %d index = %d\n", vertexcnt, indexCount);
		g->Alloc(vertexcnt, indexCount);
		memcpy(g->GetVertex(), vertexBuf, vertexcnt * sizeof(Geometry::VertexFormat));
		Index* vb = g->GetIndex();
		for (u32 i = 0; i < indexCount; i++)
			vb[i] = indexBuf[i].vtx;
			   
		vxdeleteArray(indexBuf);
		vxdeleteArray(vertexBuf);
	}
	
	// calc normals
	if (ncount == 0) {
		Geometry::VertexFormat* v = g->GetVertex();
		const Index* ip = g->GetIndex();
		for (u32 i = 0; i < indexCount; i+=3) {
			const Index* face = &ip[i];
			
			const Index idx1 = face[0];
			const Index idx2 = face[1];
			const Index idx3 = face[2];
			const vec3& a = v[idx1].pos;
			const vec3& b = v[idx2].pos;
			const vec3& c = v[idx3].pos;
			vec3 facetNormal = cross(b - a, c - a);// (b - a).cross(c - a);
			
			v[face[0]].normal += facetNormal;
			v[face[1]].normal += facetNormal;
			v[face[2]].normal += facetNormal;
		}
		for (u32 i = 0; i < vertexCount; i++)
			v[i].normal = normalize(v[i].normal);
	}
	
	g->CalcBounds();
	
	return g;
}



