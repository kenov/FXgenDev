/*
 *
 *  FileIO/BCClassSaver.h
 *
 */

#ifndef __BC_CLASS_SAVER_H__
#define __BC_CLASS_SAVER_H__

#include "BaseSettingSaver.h"

namespace VX {
	namespace SG {
		class Group;
	} // namespace SG
} // namespace VX

class BCClassSaver : public BaseSettingSaver
{
public:
	BCClassSaver();
	~BCClassSaver();

	b8 Save(const std::string& filepath, const VX::SG::Group* setting);
};

#endif // __BC_CLASS_SAVER_H__

