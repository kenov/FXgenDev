#include "FileIOUtil.h"
#include "../VX/SG/SceneGraph.h"

#include "TextParser.h"

namespace {

}

int ReadVec4( TextParser* tp, const std::string& label, VX::Math::vec4& v )
{
	using namespace std;

	if(!tp){ return TP_ERROR; }

	int err = TP_NO_ERROR;
	string valStr;
	vector<string> vec_valStr;
	if( (err = tp->getValue(label, valStr)) != TP_NO_ERROR){ return err; }
	tp->splitVector(valStr, vec_valStr);

	VX::Math::vec4 ret_v(0,0,0,0);
	for( u32 i = 0; i < vec_valStr.size(); i++){
		ret_v[i] = tp->convertFloat(vec_valStr[i], &err);
		if( err != TP_NO_ERROR) return err;
	}

	v = ret_v;

	return TP_NO_ERROR;
}

int ReadVec3( TextParser* tp, const std::string& label, VX::Math::vec3& v )
{
	using namespace std;

	if(!tp){ return TP_ERROR; }

	int err = TP_NO_ERROR;
	string valStr;
	vector<string> vec_valStr;
	if( (err = tp->getValue(label, valStr)) != TP_NO_ERROR){ return err; }
	tp->splitVector(valStr, vec_valStr);

	VX::Math::vec3 ret_v;
	for( u32 i = 0; i < vec_valStr.size(); i++){
		ret_v[i] = tp->convertFloat(vec_valStr[i], &err);
		if( err != TP_NO_ERROR) return err;
	}

	v = ret_v;

	return TP_NO_ERROR;
}

int ReadVec3( TextParser* tp, const std::string& label, VX::Math::idx3& v )
{
	using namespace std;

	if(!tp){ return TP_ERROR; }

	int err = TP_NO_ERROR;
	string valStr;
	vector<string> vec_valStr;
	if( (err = tp->getValue(label, valStr)) != TP_NO_ERROR){ return err; }
	tp->splitVector(valStr, vec_valStr);

	VX::Math::idx3 ret_v;
	for( u32 i = 0; i < vec_valStr.size(); i++){
		ret_v[i] = tp->convertInt(vec_valStr[i], &err);
		if( err != TP_NO_ERROR) return err;
	}

	v = ret_v;

	return TP_NO_ERROR;
}

int ReadVec3( TextParser* tp, const std::string& label, u32 v[3])
{
	using namespace std;

	if(!tp){ return TP_ERROR; }

	int err = TP_NO_ERROR;
	string valStr;
	vector<string> vec_valStr;

	if( (err = tp->getValue(label, valStr)) != TP_NO_ERROR){ return err; }
	tp->splitVector(valStr, vec_valStr);

	int ret_v[3] = {0, 0, 0};

	for( size_t i = 0; i < vec_valStr.size(); i++){
		ret_v[i] = tp->convertInt(vec_valStr[i], &err);
		if( err != TP_NO_ERROR) return err;
	}

	v[0] = ret_v[0];
	v[1] = ret_v[1];
	v[2] = ret_v[2];

	return TP_NO_ERROR;
}

b8 isBCMFile( const std::string& filename )
{
	TextParser *tp = new TextParser();

	if( (tp->read(filename) != TP_NO_ERROR ) )
	{
		delete tp;
		return false;
	}
	
	b8 hasTree    = false;
	b8 hasBCMTree = false;
	std::vector<std::string> nodes;
	tp->getNodes(nodes, 1);
	for(std::vector<std::string>::iterator nit = nodes.begin(); nit != nodes.end(); ++nit)
	{
		if( CompStr((*nit), "BCMTree") == 0 ){
			hasBCMTree = true;
			continue;
		}
		if( CompStr((*nit), "Tree") == 0 ){
			hasTree = true;
			continue;
		}
	}

	delete tp;
	if( hasTree || hasBCMTree ){
		return true;
	}
	
	return false;
}


