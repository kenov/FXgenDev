#include "VoxelCartesianBlockLoader.h"


#include "../VOX/BitVoxel.h"
#include "../VOX/VOXFileCommon.h"
#include "../VX/SG/VoxelCartesianBlock.h"
#include "FileIOUtil.h"

#include "../VX/RLE.h"
#include <algorithm>
#include <cassert>
#include <functional>
#include <iostream>
#include <vector>

namespace VX {
namespace SG {

VoxelCartesianBlockLoader::VoxelCartesianBlockLoader()
{

}

VoxelCartesianBlockLoader::~VoxelCartesianBlockLoader()
{

}


bool VoxelCartesianBlockLoader::ReadHeaderInfo( 
														const std::string& filepath,
		                                                VX::Math::idx3& size,
														bool& header_isNeedSwap,
														u32& header_gc)
{
	
	using namespace std;
	using namespace VX::SG;
	using namespace VX::Math;

	FILE *fp = NULL;
	if( (fp = fopen(filepath.c_str(), "rb")) == NULL ){
		VXLogE("File Open Error (%s) [%s:%d]\n", filepath.c_str(), __FILE__, __LINE__);
		return false;
	}

	VOX::LBHeader header;
	VOX::CellIDCapsule cc;

	bool isNeedSwap = false;
	if( !Load_LeafBlock_Header(fp, header, isNeedSwap) ){
		VXLogE("File Read Error (%s) [%s:%d]\n", filepath.c_str(), __FILE__, __LINE__);
		fclose(fp);
		return false;
	}
	if( !Load_LeafBlock_CellIDHeader(fp, cc.header, isNeedSwap) ){
		VXLogE("File Read Error (%s) [%s:%d]\n", filepath.c_str(), __FILE__, __LINE__);
		fclose(fp);
		return false;
	}

	fclose(fp);

	// check value
	if(header.kind != static_cast<unsigned char>(VOX::LB_CELLID)){
		VXLogE("%s is not Grid file [%s:%d]\n", filepath.c_str(), __FILE__, __LINE__);	
		return false;
	}
	if(header.bitWidth < 1 && header.bitWidth > 5) {
		VXLogE("%s is not Grid file [%s:%d]\n", filepath.c_str(), __FILE__, __LINE__);
		return false;
	}
			
	size[0] = header.size[0];
	size[1] = header.size[1];
	size[2] = header.size[2];
	header_gc = header.vc;
	header_isNeedSwap = isNeedSwap;

	return true;
}

VX::SG::VoxelCartesianBlock* VoxelCartesianBlockLoader::Load( const RunMode& mode, 
														const std::string& filepath,
		                                                const VX::Math::idx3& _size,
														const bool& header_isNeedSwap,
														const u32& header_gc,
														std::string& errMsg
														)
{
	

	using namespace std;
	using namespace VX::SG;
	using namespace VX::Math;

	if(filepath.size()==0){
		return NULL;
	}

	FILE *fp = NULL;
	if( (fp = fopen(filepath.c_str(), "rb")) == NULL ){
		VXLogE("File Open Error (%s) [%s:%d]\n", filepath.c_str(), __FILE__, __LINE__);
		//ファイルが無いのは想定内のためエラーにしない
		//errMsg = std::string("File Open Error.") + filepath;
		return NULL;
	}

	VOX::LBHeader header;
	VOX::CellIDCapsule cc;

	bool isNeedSwap;
	if( !Load_LeafBlock_Header(fp, header, isNeedSwap) ){
		VXLogE("File Read Error (%s) [%s:%d]\n", filepath.c_str(), __FILE__, __LINE__);
		fclose(fp);
		errMsg = std::string("File Read Error.[Load_LeafBlock_Header].") + filepath;
		return NULL;
	}

	if( !Load_LeafBlock_CellIDHeader(fp, cc.header, isNeedSwap) ){
		VXLogE("File Read Error (%s) [%s:%d]\n", filepath.c_str(), __FILE__, __LINE__);
		fclose(fp);
		errMsg = std::string("File Read Error.[Load_LeafBlock_CellIDHeader].") + filepath;
		return NULL;
	}

	if(header.kind != static_cast<unsigned char>(VOX::LB_CELLID)){
		VXLogE("%s is not Grid file [%s:%d]\n", filepath.c_str(), __FILE__, __LINE__);
		fclose(fp);
		errMsg = std::string("File Read Error.[header.kind].") + filepath;
		return NULL;
	}

	short bitWidth = header.bitWidth;

	if(bitWidth < 1 && bitWidth > 5) {
		VXLogE("%s is not Grid file [%s:%d]\n", filepath.c_str(), __FILE__, __LINE__);
		fclose(fp);
		errMsg = std::string("File Read Error.[header.bitWidth].") + filepath;
		return NULL;
	}
			
	if(header.size[0] != _size.x || header.size[1] != _size.y || header.size[2] != _size.z)
	{
		VXLogE("File Read Error (%s) [%s:%d] header size is illegal, in bvx=[%d,%d,%d],in dfi=[%d,%d,%d]\n", filepath.c_str(), __FILE__, __LINE__,
			header.size[0],header.size[1],header.size[2],_size.x,_size.y,_size.z
			);
		fclose(fp);
		
		std::stringstream ss;
		ss << "File Read Error.[header.size is different between dfi[" << _size.x << "," << _size.y << "," << _size.z
			<< "] and bvx[" << header.size[0] <<"," << header.size[1] << "," << header.size[2] << "]. ";
		std::string str = ss.str();


		errMsg = ss.str() + filepath;

		return NULL;
	}

	if(header.vc != header_gc){
		VXLogE("File Read Error.index_dfi_guildCell[%d],cellID_header_gc[%d] (%s) [%s:%d]\n",header_gc, header.vc ,filepath.c_str(), __FILE__, __LINE__);
		//fclose(fp);
		//return NULL;

	}

	if(isNeedSwap != header_isNeedSwap){
		VXLogE("File Read Error (%s) [%s:%d]\n", filepath.c_str(), __FILE__, __LINE__);
		fclose(fp);
		errMsg = std::string("File Read Error.[header.isNeedSwap is different].") + filepath;
		return NULL;
	}

	if( !Load_LeafBlock_CellIDData(fp, &cc.data, header, cc.header, isNeedSwap) ) {
		VXLogE("File Read Error (%s) [%s:%d]\n", filepath.c_str(), __FILE__, __LINE__);
		fclose(fp);
		errMsg = std::string("File Read Error.[Load_LeafBlock_CellIDData].") + filepath;
		return NULL;
	}

	//
	// TODO: BCFlagはbitwidth=5であるがボクセル毎に32bit消費しているので、bitwidth=32が正解ではないか？要確認
	//
	if(mode == VoxelCartesianBlockLoader::MODE_BCFLAG){
		bitWidth = 32;
	}

	//VXLogI("cc.header.numBlock : %ld, cc.header.compSize : %ld\n", cc.header.numBlock, cc.header.compSize);
	//for(int i = 0; i < cc.header.compSize; i++){
	//	VXLogI("cc.data[%d] : %d\n", i, cc.data[i]);
	//}
			
	fclose(fp);

	u8* rleBuf = NULL;
	size_t rleSize = 0;

	if( header.vc != 0 ){
		// Reshape

		////////////////////////
		// this is decomplesser.

		unsigned char* voxel = DecompCellIDData( header, cc );
		idx3 bsz( header.size[0], header.size[1], header.size[2] );
		int vc = header.vc;

		u8* block = new u8[bsz.x * bsz.y * bsz.z];
		size_t fbsize = (bsz.x + vc*2) * (bsz.y + vc*2) * (bsz.z + vc*2);

		for(u32 z = 0; z < header.size[2]; z++){
			for(u32 y = 0; y < header.size[1]; y++){
				size_t bloc  = 0    + ( y     +  z     *  bsz.y         ) *  bsz.x;
				size_t fbloc = 0+vc + ((y+vc) + (z+vc) * (bsz.y+(vc*2)) ) * (bsz.x+(vc*2));
				memcpy(&block[bloc], &voxel[fbloc], sizeof(unsigned char) * bsz.x);
			}
		}
				
		size_t bitVoxelSize = 0;
		//
		//  Bitvoxel encode
		//
		VOX::bitVoxelCell* bitVoxel = VOX::CompressBitVoxel(&bitVoxelSize, bsz.x * bsz.y * bsz.z, block, static_cast<u8>(bitWidth));
		size_t bvs = bitVoxelSize * sizeof(VOX::bitVoxelCell);
				
		//
		//  RLE encode
		//
		rleBuf = VX::rleEncode<VOX::bitVoxelCell, u8>(bitVoxel, bvs, &rleSize);
		delete [] block;
		delete [] bitVoxel;
	}
	else if( cc.header.compSize == 0 ){
		
		size_t bitVoxelSize = VOX::GetBitVoxelSize(header.size[0] * header.size[1] * header.size[2], static_cast<u8>(bitWidth));
		size_t bvs = bitVoxelSize * sizeof(VOX::bitVoxelCell);
				
		//
		//  RLE encode
		//
		rleBuf = VX::rleEncode<VOX::bitVoxelCell, u8>(reinterpret_cast<VOX::bitVoxelCell*>(cc.data), bvs, &rleSize); 
		delete [] cc.data;

	}
	else
	{
		rleBuf = cc.data;
		rleSize = cc.header.compSize;

	}
	size_t bitVoxelSize = VOX::GetBitVoxelSize((_size.x * _size.y * _size.z), static_cast<u8>(bitWidth));
	u8* rleData = new u8[rleSize];
	memcpy(rleData, rleBuf, sizeof(u8) * rleSize);
	delete [] rleBuf;

	VoxelCartesianBlock *cblock = 
			vxnew VoxelCartesianBlock(  rleData, rleSize, bitVoxelSize, static_cast<u8>(bitWidth));

	/*
#if defined(_WIN32) && (_DEBUG)

	VOX::bitVoxelCell* bitVoxel = cblock->GetBitVoxel();
	
	const u8 bw = cblock->GetBitWidth();
	for(int z=0;z<_size.z;z++){
		for(int y=0;y<_size.y;y++){
			for(int x=0;x<_size.x;x++){
				VX::Math::idx3 bPos(x,y,z);
				u32 id = GetCellID(bitVoxel, bw, _size, bPos);
				//VXLogD ("(%d,%d,%d)=%d\n", x,y,z,id);
				if(id!=0){
					int i=0;
				}
			}
		}
	}
	delete [] bitVoxel; // !! Do not use vxnew !!
#endif
	*/

	return cblock;
}

void VoxelCartesianBlockLoader::UnLoad(VX::SG::VoxelCartesianBlock* block)
{
	assert(block!=NULL);
	vxdelete(block);
}

}} // namespace VX::SG




