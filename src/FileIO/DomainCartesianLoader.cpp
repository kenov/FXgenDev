
#include "../VX/VX.h"
#include "../VX/SG/SceneGraph.h"

#include "DomainCartesianLoader.h"
#include "FileIOUtil.h"
#include "TextParser.h"

#define SUBDOMAIN_IDENTIFIER ('S' | ('B' << 8) | ('D' << 16) | ('M' << 24))

#define MODULE_NAME "Domain Cartesian Load"

namespace {

	std::string GetActiveSubdomainFilepath(const std::string& domainPath, const std::string& subdomainFilename )
	{
		if( subdomainFilename.at(0) == '/' ){
			return subdomainFilename;
		}
		std::string dir = GetDirectory(domainPath);
		
		return dir + "/" + subdomainFilename;
	}

	struct Subdomain
	{
		u32 x, y, z;
		u8  *contents;

		Subdomain(u32 _x, u32 _y, u32 _z){
			x = _x;
			y = _y;
			z = _z;
			contents = vxnew u8[x * y * z];
		}

		~Subdomain(){
			vxdeleteArray(contents);
		}
	};

	Subdomain* ReadActiveSubdomain(const std::string& filepath)
	{
		FILE *fp = fopen(filepath.c_str(), "rb");
		if(!fp){
			VXLogW("Missing Subdomain File. (%s)", filepath.c_str());
			return NULL;
		}

		u32 FMT = 0;
		if( fread(&FMT, sizeof(u32), 1, fp) != 1){
			VXLogE("File Read Error. (%s)", filepath.c_str());
			return NULL;
		}

		bool isNeedSwap = false;
		// Format Check
		if( FMT != SUBDOMAIN_IDENTIFIER ) 
		{
			BSwap32(&FMT);
			if( FMT != SUBDOMAIN_IDENTIFIER ){
				VXLogE("File Format Error. (%s)", filepath.c_str());
				return NULL;
			}
			isNeedSwap = true;
		}

		u32 size[3] = {0, 0, 0};
		if( fread(size, sizeof(u32), 3, fp) != 3 ){
			VXLogE("File Read Error. (%s)", filepath.c_str());
			return NULL;
		}
		if( isNeedSwap ){
			BSwap32(&size[0]);
			BSwap32(&size[1]);
			BSwap32(&size[2]);
		}

		if( size[0] == 0 || size[1] == 0 || size[2] == 0 ){
			VXLogE("Data is invalid. (%s)", filepath.c_str());
			return NULL;
		}

		Subdomain *sb = vxnew Subdomain(size[0], size[1], size[2]);
		
		if( fread(sb->contents, sizeof(u8), size[0]*size[1]*size[2], fp) != size[0] * size[1] * size[2] )
		{
			VXLogE("Data is invalid. (%s)", filepath.c_str());
			vxdelete(sb);
			return NULL;
		}

		fclose(fp);

		return sb;
	}

};

DomainCartesianLoader::DomainCartesianLoader()
{

}

DomainCartesianLoader::~DomainCartesianLoader()
{

}

VX::SG::Node* DomainCartesianLoader::Load(const std::string& filepath)
{
	using namespace std;
	using namespace VX::SG;
	using namespace VX::Math;

	int err = 0;
	TextParser *tp = new TextParser();

	tp->remove();

	err = tp->read(filepath);
	if(err != TP_NO_ERROR){ delete tp; return NULL; }
	
	err = tp->changeNode("/domaininfo");
	if(err != TP_NO_ERROR){
		VXLogW("[%s] No such DomainInfo in file [%s].\n", MODULE_NAME, filepath.c_str());
		delete tp;
		return NULL;
	}

	vec3 origin;
	vec3 region;
	vec3 pitch;
	idx3 vox;
	idx3 div;
	Subdomain* sb = NULL;

	bool hasOrigin    = false;
	bool hasRegion    = false;
	bool hasPitch     = false;
	bool hasVox       = false;
	bool hasDiv       = false;
	bool hasSubdomain = false;

	std::string unit = "NonDimensional";

	vector<string> lbls;
	tp->getLabels(lbls);
	for(vector<string>::iterator it = lbls.begin(); it != lbls.end(); ++it){
		string valStr;
		tp->getValue(*it, valStr);
	
		if( CompStr(*it, "UnitOfLength") == 0 ){
			unit = valStr;
			continue;
		}

		if( CompStr(*it, "GlobalOrigin") == 0 ){
			if( ReadVec3(tp, *it, origin) == TP_NO_ERROR ) { hasOrigin = true; }
			continue;
		}

		if( CompStr(*it, "GlobalRegion") == 0 ){
			if( ReadVec3(tp, *it, region) == TP_NO_ERROR ){ hasRegion = true; }
			continue;
		}

		if( CompStr(*it, "GlobalPitch") == 0 ){
			if( ReadVec3(tp, *it, pitch) == TP_NO_ERROR ){ hasPitch = true; }
			continue;
		}

		if( CompStr(*it, "GlobalVoxel") == 0 ){
			if( ReadVec3(tp, *it, vox) == TP_NO_ERROR ){ hasVox = true; }
			continue;
		}

		if( CompStr(*it, "GlobalDivision") == 0){
			if( ReadVec3(tp, *it, div) == TP_NO_ERROR ){ hasDiv = true; }
			continue;
		}
		
		if( CompStr(*it, "ActiveSubDomainFile") == 0){
			string valStr;
			tp->getValue(*it, valStr);
			string sbfilepath = GetActiveSubdomainFilepath(filepath, valStr);
			sb = ReadActiveSubdomain(sbfilepath);
			if( sb == NULL ){
				VXLogW("[%s] Load ActiveSubdmainFile Error (%s).\n", MODULE_NAME, sbfilepath.c_str());
			}
			hasSubdomain = true;
			continue;
		}
	}

	DomainCartesian* domain = vxnew DomainCartesian("Domain Boundary", origin, region);
	domain->SetUnit(unit);

	if( !hasOrigin || !hasRegion ){
		VXLogE("[%s] %s has no GlobalOrigin / GlobalRegion.\n", MODULE_NAME);
		vxdelete(sb); vxdelete(domain); delete tp; return NULL;
	}

	if( hasPitch ){
		for(u32 i = 0; i < 3; i++){
			vox[i]    = static_cast<s32>(ceil(region[i] / pitch[i]));
			region[i] = static_cast<float>(vox[i]) * pitch[i];
		}

		if( !domain->SetBBoxMinMax(origin, (origin+region)) ) {
			VXLogE("[%s] Origin / Region is invalid value.\n", MODULE_NAME);
			vxdelete(sb); vxdelete(domain); delete tp; return NULL;
		}
		if( !domain->SetVoxelCount(vox)  ) {
			VXLogE("[%s] Voxel / Pitch is invalid value.\n", MODULE_NAME);
			vxdelete(sb); vxdelete(domain); delete tp; return NULL;
		}

		domain->SetExportPolicy(VX::SG::DomainCartesian::EXP_VOXEL_PITCH);

	}else if( hasVox ){
		if( !domain->SetVoxelCount(vox)  ) { 
			VXLogE("[%s] Voxel / Pitch is invalid value.\n", MODULE_NAME);
			vxdelete(sb); vxdelete(domain); delete tp; return NULL;
		}

		domain->SetExportPolicy(VX::SG::DomainCartesian::EXP_VOXEL_COUNT);
	}

	if( hasDiv ){
		if( !domain->SetDivCount(div) ){
			VXLogE("[%s] Division is invalid value.\n", MODULE_NAME);
			vxdelete(sb); vxdelete(domain); delete tp; return NULL;
		}
	}

	if( hasSubdomain && (sb!=NULL) ){
		if( div[0] != sb->x || div[1] != sb->y || div[2] != sb->z ) { 
			VXLogE("[%s] ActiveSubdomainFile's division is not same GlobalDivision", MODULE_NAME);
			vxdelete(sb); vxdelete(domain); delete tp; return NULL;
		}
		if( !domain->SetSubdomain(sb->contents) ) {
			VXLogE("[%s] ActiveSubdomainFile is invalid\n", MODULE_NAME);
			vxdelete(sb); vxdelete(domain); delete tp; return NULL;
		}		
	}

	domain->UpdateGeometry();
	
	vxdelete(sb);
	delete tp;
	return domain;
}

