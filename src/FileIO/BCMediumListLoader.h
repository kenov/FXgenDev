/*
 *
 *  FileIO/BCMediumListLoader.h
 *
 */

#ifndef __BC_MEDIUM_LIST_LOADER_H__
#define __BC_MEDIUM_LIST_LOADER_H__

#include <string>
#include "BaseSettingLoader.h"

class BCMediumListLoader : public BaseSettingLoader
{
public:
	BCMediumListLoader();
	~BCMediumListLoader();

	b8 Load(const std::string& filename, VX::SG::Group* setting);
};

#endif // __BC_MEDIUM_LIST_LOADER_H__

