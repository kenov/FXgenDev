//
//  STLAsciiSaver.cpp
//

#include "../VX/VX.h"
#include "STLAsciiSaver.h"
#include "../VX/Stream.h"
#include "../VX/SG/SceneGraph.h"

#include <stdio.h>

STLAsciiSaver::STLAsciiSaver(){}
STLAsciiSaver::~STLAsciiSaver(){}

class DumpNodeToSTLAsciiVisitor
{
public:
	DumpNodeToSTLAsciiVisitor(VX::Stream* st, const u32& blocksize, f32 scale = 1)
	{
		m_st = st;
		m_blockSize = blocksize;
		m_modelScale = scale;
		facetstr    = "facet normal ";
		endfacetstr  = "endfacet\n";
		outerstr    = "outer loop\n";
		endouterstr = "endloop\n";
		vertexstr   = "vertex ";
	}
	void operator()(const VX::SG::Geometry* g) const
	{
		std::string blockstr;

		char linebuf[512] = "";
		for (u32 i=0; i < g->GetIndexCount(); i += 3)
		{
			const u32 index_0 = g->GetIndex()[i];
			const u32 index_1 = g->GetIndex()[i+1];
			const u32 index_2 = g->GetIndex()[i+2];

			const VX::Math::vec3& vert_0 = g->GetVertex()[index_0].pos * m_modelScale;
			const VX::Math::vec3& vert_1 = g->GetVertex()[index_1].pos * m_modelScale;
			const VX::Math::vec3& vert_2 = g->GetVertex()[index_2].pos * m_modelScale;

			blockstr.append(facetstr);
			VX::Math::vec3 normal = VX::Math::normalize(VX::Math::cross((vert_1 - vert_0), (vert_2 - vert_0)));
			
			sprintf(linebuf,"%f %f %f\n", normal.x, normal.y, normal.z);
			blockstr.append(linebuf);

			blockstr.append(outerstr); 
			sprintf(linebuf,"%s %f %f %f\n", vertexstr.c_str(), vert_0.x, vert_0.y, vert_0.z);
			blockstr.append(linebuf);
			sprintf(linebuf,"%s %f %f %f\n", vertexstr.c_str(), vert_1.x, vert_1.y, vert_1.z);
			blockstr.append(linebuf);
			sprintf(linebuf,"%s %f %f %f\n", vertexstr.c_str(), vert_2.x, vert_2.y, vert_2.z);
			blockstr.append(linebuf);
			
			blockstr.append(endouterstr);
			blockstr.append(endfacetstr);

			if (blockstr.size() > m_blockSize)
			{
				m_st->Write(blockstr.c_str(), static_cast<u32>(blockstr.size() * sizeof(char)), 0); 
				blockstr.clear();
			}
		}

		if (!blockstr.empty())
			m_st->Write(blockstr.c_str(),static_cast<u32>(blockstr.size() * sizeof(char)), 0); 
	}
private:
	VX::Stream* m_st;	
	u32 m_blockSize;
	f32 m_modelScale;
	std::string solidstr;
	std::string endsolidstr;
	std::string facetstr;
	std::string endfacetstr;
	std::string outerstr;
	std::string endouterstr;
	std::string vertexstr;
	
};

b8 STLAsciiSaver::Save(VX::Stream* st, const VX::SG::Node* node, f32 scale, b8 visibleOnly)
{
	if (!st || !node)
		return false;

	std::string tagbuf("solid ");
	tagbuf.append(st->GetFileName());
	tagbuf.append("\n");

	st->Write(tagbuf.c_str(), static_cast<u32>(tagbuf.size()), 0);

	// write down per 5MB
	DumpNodeToSTLAsciiVisitor visitor(st, 5 * 1024 * 1024, scale);
	VX::SG::VisitAllGeometry(node, visitor, visibleOnly);
	
	tagbuf = "endsolid\n";
	st->Write(tagbuf.c_str(), static_cast<u32>(tagbuf.size()), 0);

	return true;
}

