/*
 *
 * FileIO/MeshCartesianLoader.h
 *
 *
 */

#ifndef __MESH_CARTESIAN_LOADER_H__
#define __MESH_CARTESIAN_LOADER_H__

#include <string>

namespace VX {
	namespace SG {
		class Group;
	}
}

class MeshCartesianLoader
{
public:
	MeshCartesianLoader();
	~MeshCartesianLoader();

	VX::SG::Group* Load(const std::string& filepath);
};


#endif // __MESH_CARTESIAN_LOADER_H__

