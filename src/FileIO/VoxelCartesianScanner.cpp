#include "VoxelCartesianScanner.h"

#include <assert.h>
#include "../Core/fxgenCore.h"
#include "../VX/SG/Node.h"
#include "../VX/SG/Group.h"
#include "../VX/SG/GUISettings.h"
#include "../VX/SG/Medium.h"
#include "../VX/SG/VoxelCartesianL.h"
#include "../FileIO/BCMediumIDNumbering.h"
#include "../VX/SG/LocalBCClass.h"
#include "../VX/SG/LocalBC.h"

VoxelCartesianScanner::VoxelCartesianScanner(fxgenCore* core):m_core(core)
{

}

VoxelCartesianScanner::~VoxelCartesianScanner()
{

}

void VoxelCartesianScanner::getBCCListOnCore(std::vector<VX::SG::LocalBCClass*>& bccs)
{
	bccs.clear();

	VX::SG::Group* setting = m_core->GetSettingNode();

	for(int i = 0; i < setting->GetChildCount(); i++){
		VX::SG::Node* n = setting->GetChild(i);
		VX::SG::NODETYPE type = n->GetType();
		if( type == VX::SG::NODETYPE_LOCALBCCLASS) 
		{
			VX::SG::LocalBCClass* m = dynamic_cast<VX::SG::LocalBCClass*>(n);
			assert(m!=NULL);
			bccs.push_back(m);
		}
	}
}

void VoxelCartesianScanner::getMeduimListOnCore(std::vector<VX::SG::Medium*>& meds)
{
	meds.clear();

	VX::SG::Group* setting = m_core->GetSettingNode();

	for(int i = 0; i < setting->GetChildCount(); i++){
		VX::SG::Node* n = setting->GetChild(i);
		VX::SG::NODETYPE type = n->GetType();
		if( type == VX::SG::NODETYPE_MEDIUM) 
		{
			VX::SG::Medium* m = dynamic_cast<VX::SG::Medium*>(n);
			assert(m!=NULL);
			meds.push_back(m);
		}
	}
}

void VoxelCartesianScanner::getLocalBCListOnCore(std::vector<VX::SG::LocalBC*>& bcs)
{
	bcs.clear();

	VX::SG::Group* setting = m_core->GetSettingNode();

	for(int i = 0; i < setting->GetChildCount(); i++){
		VX::SG::Node* n = setting->GetChild(i);
		VX::SG::NODETYPE type = n->GetType();
		if( type == VX::SG::NODETYPE_LOCALBC) 
		{
			VX::SG::LocalBC* m = dynamic_cast<VX::SG::LocalBC*>(n);
			assert(m!=NULL);
			bcs.push_back(m);
		}
	}
}

void VoxelCartesianScanner::addNewMeduim( const u8& med_id )
{
	if(med_id==0){
#ifdef _DEBUG
		assert(false);
#endif
		return;
	}

	VX::SG::Group* setting = m_core->GetSettingNode();
	VX::SG::GUISettings* colormap = m_core->GetGuiSettingNode();
	size_t color_size = colormap->GetColor().size();
	assert(color_size>0);

	char mediumName[64] = {};
		
	sprintf(mediumName, "medium%d", med_id);
	std::string medium = mediumName;
		
	//色の決定
	VX::Math::vec4 color = colormap->GetColor().at((med_id -1 ) % color_size);
		
	VX::SG::Medium* m =vxnew VX::SG::Medium(medium,med_id,color);
	setting->AddChild(m);

	//採番テーブルに追加
	if(!BCMediumIDNumbering::GetInstance()->HasID(med_id)){
		BCMediumIDNumbering::GetInstance()->AddID(med_id,medium);
	}
}

void VoxelCartesianScanner::addNewBCflg( const u8& bcf_id )
{
	if(bcf_id==0){
#ifdef _DEBUG
		assert(false);
#endif
		return;
	}

	VX::SG::Group* setting = m_core->GetSettingNode();
	VX::SG::GUISettings* colormap = m_core->GetGuiSettingNode();
	size_t color_size = colormap->GetColor().size();
	assert(color_size>0);

	char Alias[64] = {};
	sprintf(Alias, "BCflg%d", bcf_id);
	std::string alias = Alias;
		
	VX::Math::vec4 color = colormap->GetColor().at((bcf_id -1 ) % color_size);
						
	//Meduimとのリンク
	std::string mediumName = "";
	std::vector<VX::SG::Medium*> meds;
	getMeduimListOnCore(meds);
	if(meds.size()>0){
		//デフォルトで最初のmediumにリンクする
		mediumName = meds[0]->GetName();
	}

	//BCCとのリンク
	VX::SG::LocalBCClass* bcclass = NULL;
	std::vector<VX::SG::LocalBCClass*> bccs;
	getBCCListOnCore(bccs);
	if(bccs.size()>0){
		//デフォルトで最初のBCCにリンクする
		bcclass = bccs[0];
	}
	assert(bcclass!=NULL);

	VX::SG::LocalBC* b =vxnew VX::SG::LocalBC(alias,bcclass,bcf_id,color,mediumName);
	
	setting->AddChild(b);

	//採番テーブルに追加
	if(!BCMediumIDNumbering::GetInstance()->HasID(bcf_id)){
		BCMediumIDNumbering::GetInstance()->AddID(bcf_id,alias);
	}
}


bool VoxelCartesianScanner::hasID(const std::vector<VX::SG::Medium*>& meds, const u32& id)const
{
	for(int i=0;i<meds.size();i++){
		if( meds.at(i)->GetID() == id ){
			return true;
		}
	}
	return false;
}

bool VoxelCartesianScanner::hasID(const std::vector<VX::SG::LocalBC*>& bcf, const u32& id)const
{
	for(int i=0;i<bcf.size();i++){
		if( bcf.at(i)->GetID() == id ){
			return true;
		}
	}
	return false;
}

/**
*	@brief medium値を無ければ自動追加
*/
void VoxelCartesianScanner::setMedium(VX::SG::VoxelCartesianL_Scan& cellid)
{
	//現在のノードの medium/BCflgのデータ IDが錯綜している場合もあるので両方チェックする
	std::vector<VX::SG::Medium*> meds;
	getMeduimListOnCore(meds);

	std::vector<VX::SG::LocalBC*> bcf;
	getLocalBCListOnCore(bcf);

	//32個のmeduim値から有効値かつ、既存のmeduiumリストに無いものは追加する
	for(s32 i=0;i<cellid.GetSize();i++){

		// index = 0 は予約されたindexで、idは１から採番のため無視
		if(i==0){
			continue;
		}

		if( !cellid.IsEnable(i)){
			continue;
		}

		//インデックスがmeduimＩＤと同値
		u32 medID = i;
		if(hasID(meds, medID)){
			continue;
		}
		//TODO: 要小野先生確認
		//万一　mediumのＩＤがBCflgに設定されているときは、既存のBCFlagを優先させるため処理を無視する
		/*
		if(hasID(bcf, medID)){
			//mediumと同じ値なので無視
			continue;
		}
		*/

		//そのmedium値を追加
		const u8 med_id = static_cast<u8>(medID);
		addNewMeduim( med_id);
	}
}

/**
*	@brief bcflg値を無ければ自動追加
*/
void VoxelCartesianScanner::setBCflg(VX::SG::VoxelCartesianL_Scan& bcflg)
{

	std::vector<VX::SG::Medium*> meds;
	getMeduimListOnCore(meds);

	std::vector<VX::SG::LocalBC*> bcf;
	getLocalBCListOnCore(bcf);

	//32個のmeduim値から有効値かつ、既存のmeduiumリストに無いものは追加する
	for(s32 i=0;i<bcflg.GetSize();i++){

		// index = 0 は予約されたindexで、idは１から採番のため無視
		if(i==0){
			continue;
		}

		if( !bcflg.IsEnable(i)){
			continue;
		}

		//インデックスがmeduimＩＤと同値
		u32 bcfID = i;
		if(hasID(bcf, bcfID)){
			continue;
		}
		//TODO: 要小野先生確認
		//万一　bcflgのＩＤがmediumに設定されているときは、既存のmediumを優先させるため処理を無視する
		/*
		if(hasID(meds, bcfID)){
			continue;
		}
		*/

		//そのmedium値を追加
		const u8 bcflg_id = static_cast<u8>(bcfID);
		addNewBCflg( bcflg_id);
	}
}

/**
*	@brief 指定したサブドメイン内をスキャンしてテーブルに追加する
*/
void VoxelCartesianScanner::Scan(const std::vector<VX::SG::VoxelCartesianL*>& locals)
{

	VX::SG::VoxelCartesianL_Scan cellid , bcflg;

	//全てのローカルでサーチする
	for(int i=0;i<locals.size();i++){
		VX::SG::VoxelCartesianL* local = locals.at(i);
		local->ScanCellID(cellid);
		local->ScanBCflg(bcflg);
	}

	//Settingノードに自動設定
	setMedium(cellid);
	setBCflg(bcflg);

	//更新
	m_core->UpdateIDColorTable();
}
void VoxelCartesianScanner::Scan(VX::SG::VoxelCartesianL* local)
{
	std::vector<VX::SG::VoxelCartesianL*> locals;
	locals.push_back(local);
	Scan(locals);
}

