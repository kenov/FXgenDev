/*
 *
 * BaseSettingSaver.h
 *
 */

#ifndef INCLUDE_FILEIO_BASE_SETTING_SAVER_H
#define INCLUDE_FILEIO_BASE_SETTING_SAVER_H

#include "../VX/Type.h"
#include <string>

namespace VX {
	namespace SG {
		class Group;
	} // namespace SG
} // namespace VX

class BaseSettingSaver
{
protected:
	BaseSettingSaver(){};
	virtual ~BaseSettingSaver(){};

public:
	virtual b8 Save(const std::string& filename, const VX::SG::Group* setting) = 0;
};


#endif // INCLUDE_FILEIO_BASE_SETTING_SAVER_H
