/*
 *
 * FileIO/PolylibFileSaver.h
 *
 */

#ifndef __PolylibFileSaver_H__
#define __PolylibFileSaver_H__

#include "../VX/Type.h"

#include <string>
#include <fstream>
#include <sstream>

namespace VX {
	namespace SG {
		class Node;
		class Group;
		class Geometry;
	}
}

class PolylibFileSaver
{
public:
	PolylibFileSaver(){ }
	~PolylibFileSaver(){ }

	b8 Save(const std::string& filepath,const VX::SG::Node* node,const VX::SG::Group* setting, const b8& visibleOnly);

private:
	void dumpTree( VX::SG::Node* node, std::ostringstream& os , const b8& visibleOnly,
									const std::string& filepath );
	
	void writeNode( VX::SG::Geometry* node, std::ostringstream& os , 
									const std::string& filepath );

	void GetInfo(VX::SG::Geometry* g , std::string& medium , std::string& localBC);
	b8 GetNameByID( const int& id , int& nt , std::string& name );

	VX::SG::Group* m_setting;
};

#endif // __PolylibFileSaver_H__

