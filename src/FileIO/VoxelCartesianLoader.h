/*
 *
 * FileIO/VoxelCartesianLoader.h
 *
 *
 */

#ifndef __VoxelCartesianLoader_H__
#define __VoxelCartesianLoader_H__

#include <string>

namespace VX {
	namespace SG {
		class Group;
	}
}
class VoxelCartesianParam;
class fxgenCore;
class VoxelCartesianLoader
{
public:
	VoxelCartesianLoader();
	~VoxelCartesianLoader();

    VX::SG::Group* Load(const VoxelCartesianParam& param,fxgenCore* core,std::string& errMsg);

private:
    VX::SG::Group* Load_Dfi(const VoxelCartesianParam& param,fxgenCore* core , std::string& errMsg);
	//VX::SG::Group* Load_bvxdirect(const std::string& direct_bvxfile);

};


#endif // __MESH_CARTESIAN_LOADER_H__

