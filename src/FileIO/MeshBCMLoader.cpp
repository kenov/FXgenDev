#include "../VX/VX.h"
#include "../VX/SG/SceneGraph.h"
#include "../VX/SG/MeshUtil.h"

#include "MeshBCMLoader.h"
#include "FileIOUtil.h"
#include "../VOX/BitVoxel.h"
#include "../VOX/VOXFileCommon.h"
#include "../VOX/PartitionMapper.h"

#include "TextParser.h"

#include "../VX/RLE.h"

#define MODULE_NAME "Mesh BCM Load"

namespace {
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	bool Load_LeafBlock_CellID( const std::string&               dir, 
	                            const VOX::IdxBlock*             ib, 
							    VOX::PartitionMapper*            pmapper,
							    VOX::LBHeader&                   header, 
							    std::vector<VOX::CellIDCapsule>& cidCapsules )
	{
		using namespace std;
		using namespace VOX;

		cidCapsules.clear();
		
		vector<PartitionMapper::FDIDList> fdidlists;
		pmapper->GetFDIDLists(0, fdidlists);

		cidCapsules.reserve(fdidlists.size());
		
		bool err = false;

		for(vector<PartitionMapper::FDIDList>::const_iterator file = fdidlists.begin(); file != fdidlists.end(); ++file){
			char filename[128];
			sprintf(filename, "%s_%06d.%s", ib->prefix.c_str(), file->FID, ib->extension.c_str());
			string filepath = dir + string(filename);

			FILE *fp = NULL;
			if( (fp = fopen(filepath.c_str(), "rb")) == NULL ){
				VXLogE("file open error \"%s\" [%s:%d]\n", filepath.c_str(), __FILE__, __LINE__);
				err = true;
				break;
			}

			bool isNeedSwap = false;
			
			LBHeader hdr;

			if( !Load_LeafBlock_Header(fp, hdr, isNeedSwap) ){
				VXLogE("%s is not leafBlock file [%s:%d]\n", filepath.c_str(), __FILE__, __LINE__);
				fclose(fp); err = true; break;
			}

			if(hdr.kind != static_cast<unsigned char>(LB_CELLID)){
				VXLogE("%s is not Grid file [%s:%d]\n", filepath.c_str(), __FILE__, __LINE__);
				fclose(fp); err = true; break;
			}
			if(hdr.bitWidth < 1 && header.bitWidth > 5) {
				VXLogE("%s is not Grid file [%s:%d]\n", filepath.c_str(), __FILE__, __LINE__);
				fclose(fp); err = true; break;
			}

			CellIDCapsule cc;
			if( !Load_LeafBlock_CellIDHeader(fp, cc.header, isNeedSwap) ){
				fclose(fp); err = true; break;
			}
			
			Load_LeafBlock_CellIDData(fp, &cc.data, hdr, cc.header, isNeedSwap);

			cidCapsules.push_back(cc);
			header = hdr;
			fclose(fp);
		}

		if( err ){
			VXLogE("Clear cidCapsules [%s:%d]\n", __FILE__, __LINE__);
			// データロードに失敗したためロード済みのデータを破棄
			for(vector<CellIDCapsule>::iterator it = cidCapsules.begin(); it != cidCapsules.end(); ++it){
				delete [] it->data; // !! Do not use vxnew !!
			}
			cidCapsules.clear();
			return false;
		}

		return true;
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	bool Load_LeafBlock_CellID_Gather( const std::string&               dir, 
	                                   const VOX::IdxBlock*             ib, 
									   VOX::PartitionMapper*            pmapper,
							           VOX::LBHeader&                   header, 
									   std::vector<VOX::CellIDCapsule>& cidCapsules )
	{
		using namespace std;
		using namespace VOX;

		cidCapsules.clear();

		// ファイルロード
		char filename[128];
		sprintf(filename, "%s.%s", ib->prefix.c_str(), ib->extension.c_str());
		string filepath = dir + string(filename);

		FILE *fp = NULL;
		if( (fp = fopen(filepath.c_str(), "rb")) == NULL ){
			VXLogE("err : file open error \"%s\" [%s:%d]\n", filepath.c_str(), __FILE__, __LINE__);
			return false;
		}

		bool isNeedSwap = false;
		
		LBHeader hdr;

		if( !Load_LeafBlock_Header(fp, hdr, isNeedSwap) ){
			VXLogE("%s is not leafBlock file [%s:%d]\n", filepath.c_str(), __FILE__, __LINE__);
			fclose(fp); return false;
		}

		if(hdr.kind != static_cast<unsigned char>(LB_CELLID)){
			VXLogE("%s is not Grid file [%s:%d]\n", filepath.c_str(), __FILE__, __LINE__);
			fclose(fp); return false;
		}

		if(hdr.bitWidth < 1 && header.bitWidth > 5) {
			VXLogE("%s is not Grid file [%s:%d]\n", filepath.c_str(), __FILE__, __LINE__);
			fclose(fp); return false;
		}

		header = hdr;

		uint64_t wnp = 0;
		fread(&wnp, sizeof(uint64_t), 1, fp);
		if( isNeedSwap ){
			BSwap64(&wnp);
		}

		if( wnp != pmapper->GetWriteProcs() ){
			VXLogE("err : %s's write procs is invalid  [%s:%d]\n", filepath.c_str(), __FILE__, __LINE__);
			fclose(fp); return false;
		}

		vector<LBCellIDHeader> chs(wnp);
		for(int i = 0; i < wnp; i++){
			if( !Load_LeafBlock_CellIDHeader(fp, chs[i], isNeedSwap) ){
				fclose(fp); return false;
			}
		}

		vector<unsigned char*> contents(wnp);

		for(int i = 0; i < wnp; i++){
			contents[i] = NULL;
			Load_LeafBlock_CellIDData( fp, &contents[i], hdr, chs[i], isNeedSwap );
		}

		fclose(fp);

		std::vector<bool> freeMask(wnp);
		for( int i = 0; i < wnp; i++){ freeMask[i] = true; }

		vector<PartitionMapper::FDIDList> fdidlists;
		pmapper->GetFDIDLists(0, fdidlists);
		for(vector<PartitionMapper::FDIDList>::iterator file = fdidlists.begin(); file != fdidlists.end(); ++file){
			CellIDCapsule cc;
			cc.header = chs[file->FID];
			cc.data   = contents[file->FID];
			cidCapsules.push_back(cc);
			freeMask[file->FID] = false;
		}
		
		// 不要なデータを解放
		for(int i = 0; i < wnp; i++){
			if(freeMask[i]) delete [] contents[i]; // !! Do not use vxnew !!
		}

		return true;
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	class BCMLoader
	{
	public:
		BCMLoader(const std::string& filepath) : m_isOK(false)
		{
			m_rootDirectory = FixDirectoryPath(GetDirectory(filepath));
			if( !LoadIndex(filepath) ){
				return;
			}

			VOX::OctHeader header;
			if( !VOX::LoadOctreeFile(m_octFilepath, header, m_pedigrees) )
			{
				return;
			}

			for(int i = 0; i < 3; i++)
			{
				if( fabs( m_origin[i] - static_cast<float>(header.org[i]) ) > 0.001f ){ return; } // for Check
				if( fabs( m_region[i] - static_cast<float>(header.rgn[i]) ) > 0.001f ){ return; } // for Check
				m_rootDims[i] = header.rootDims[i];
			}
			VOX::PartitionMapper pmapper(m_numProcs, 1, static_cast<u32>(m_pedigrees.size()));
			if( !LoadLeafBlockFile(&m_idxBlock, &pmapper) )
			{
				return;
			}

			m_isOK = true;
		}

		~BCMLoader()
		{
			// Do not Clear in here
		}

		void Clear()
		{
			for(std::vector<VX::SG::Block*>::iterator it = m_blocks.begin(); it != m_blocks.end(); ++it)
			{
				if( *it != NULL ){
					delete *it; // !! Do not use vxnew !!
				}
			}
		}

		const b8 IsOK() const { return m_isOK; }
		const VX::Math::vec3& GetOrigin() const { return m_origin; }
		const VX::Math::vec3& GetRegion() const { return m_region; }
		const VX::Math::idx3& GetRootDims() const { return m_rootDims; }
		const VX::Math::idx3& GetBlockSize() const { return m_blockSize; }
		u8 GetBitWidth() const { return static_cast<u8>(m_idxBlock.bitWidth); }
		const std::vector<VOX::Pedigree>& GetPedigrees() const { return m_pedigrees; }
		const std::vector<VX::SG::Block*>& GetBlocks() const { return m_blocks; }


	private:
		b8 LoadLeafBlockFile( const VOX::IdxBlock* ib, VOX::PartitionMapper* pmapper )
		{
			using namespace VOX;
			if( ib->kind != LB_CELLID ) { return false; }

			LBHeader header;
			std::vector<CellIDCapsule> cidCapsules;
			VXPrint("Load LeafBlock File(s) Start : ");
			b8 status = true;
			if( ib->isGather )
			{
				status = Load_LeafBlock_CellID_Gather(ib->rootDir + ib->dataDir, ib, pmapper, header, cidCapsules);
			}
			else
			{
				status = Load_LeafBlock_CellID(ib->rootDir + ib->dataDir, ib, pmapper, header, cidCapsules );
			}
			VXPrint("Complate\n");

			if( !status )
			{
				VXLogE("Read File Error [%s:%d] \n", __FILE__, __LINE__);
				for(std::vector<CellIDCapsule>::iterator it = cidCapsules.begin(); it != cidCapsules.end(); ++it)
				{
					delete [] it->data; // !! Do not use vxnew !!
				}
				return false;
			}

			if( header.bitWidth != ib->bitWidth || 
			    header.size[0] != m_blockSize.x || header.size[1] != m_blockSize.y || header.size[2] != m_blockSize.z )
			{
				VXLogE("Read File Error [%s:%d] \n", __FILE__, __LINE__);
				for(std::vector<CellIDCapsule>::iterator it = cidCapsules.begin(); it != cidCapsules.end(); ++it)
				{
					delete [] it->data; // !! Do not use vxnew !!
				}
				return false;
			}
			
			u32 numBlocks = pmapper->GetEnd(0) - pmapper->GetStart(0);
			
			u32 did = 0;
			u32 fid = 0;
			std::vector<PartitionMapper::FDIDList> fdidlists;
			pmapper->GetFDIDLists(0, fdidlists);

			m_blocks.resize(numBlocks);
			for(std::vector<VX::SG::Block*>::iterator it = m_blocks.begin(); it != m_blocks.end(); ++it)
			{
				*it = NULL;
			}

			u64* fdidSizeTable = new u64[fdidlists.size()];
			for(size_t i = 0; i < fdidlists.size(); i++){
				fdidSizeTable[i] = fdidlists[i].FDIDs.size();
			}

			VXPrint("Reconstructing LeafBlock to Internal Format\n[START] "); fflush(stdout);

			for(size_t i = 0; i < fdidlists.size(); i++){
				u64 iLoc = 0;
				for(size_t j = 0; j < i; j++){
					iLoc += fdidSizeTable[j];
				}

				unsigned char* voxels = DecompCellIDData( header, cidCapsules[i] );
				
				for(u64 j = 0; j < fdidSizeTable[i]; j++)
				{
					VX::Math::idx3 bsz( header.size[0], header.size[1], header.size[2] );
					u32 vc = header.vc;
					unsigned char* blockBuf = new unsigned char[bsz.x * bsz.y * bsz.z];
					size_t fbsize = (bsz.x + vc*2) * (bsz.y + vc*2) * (bsz.z + vc*2);
					const unsigned char* pvox = &voxels[ fbsize * j ];

					for(u32 z = 0; z < header.size[2]; z++){
						for(u32 y = 0; y < header.size[1]; y++){
							size_t bloc  = 0    + ( y     +  z     *  bsz.y         ) *  bsz.x;
							size_t fbloc = 0+vc + ((y+vc) + (z+vc) * (bsz.y+(vc*2)) ) * (bsz.x+(vc*2));
							memcpy(&blockBuf[bloc], &pvox[fbloc], sizeof(unsigned char) * bsz.x);
						}
					}

					size_t bitVoxelSize = 0;
					bitVoxelCell* bitVoxel = CompressBitVoxel(&bitVoxelSize, bsz.x * bsz.y * bsz.z, blockBuf, 
					                                          static_cast<u32>(ib->bitWidth));
					size_t rleSize = 0;
					u8* rleBuf = VX::rleEncode<VOX::bitVoxelCell, u8>(bitVoxel, bitVoxelSize * sizeof(VOX::bitVoxelCell), &rleSize);
					u8* rleData = new u8[rleSize];
					memcpy(rleData, rleBuf, sizeof(u8) * rleSize);
					
					VX::SG::Block* block = new VX::SG::Block(rleData, rleSize, bitVoxelSize); // !! Do not use vxnew !!
					m_blocks[j + iLoc] = block;

					delete [] bitVoxel;
					delete [] blockBuf;
					delete [] rleBuf;
				}
				delete [] voxels; // !! Do not use vxnew !!
				VXPrint("*"); fflush(stdout);
			}
			VXPrint(" [END]\n");

			delete [] fdidSizeTable;
			return true;
		}

		b8 LoadIndexProc(const std::string& filepath, std::vector<VOX::IdxProc>& procList)
		{
			using namespace std;
			TextParser *tp = new TextParser;

			if( tp->read(filepath) != TP_NO_ERROR ){
				VXLogE("Read File Error (%s) [%s:%d] \n", filepath.c_str(), __FILE__, __LINE__);
				delete tp;
				return false;
			}

			{
				tp->changeNode("/MPI");
				string valStr;
				if( tp->getValue("NumberOfRank", valStr) != TP_NO_ERROR ){
					VXLogE("Read File Error (%s) [%s:%d] \n", filepath.c_str(), __FILE__, __LINE__);
					delete tp;
					return false;
				}
				m_numProcs = atoi(valStr.c_str());
			}

			tp->changeNode("/process");
			vector<string>nodes;
			tp->getNodes(nodes, 1);
			for(vector<string>::iterator nit = nodes.begin(); nit != nodes.end(); ++nit)
			{
				if( CompStr(nit->substr(0, 4), "Rank") == 0 )
				{
					tp->changeNode(*nit);
					vector<string> lbls;
					tp->getLabels(lbls);

					VOX::IdxProc proc;
					for(vector<string>::iterator it = lbls.begin(); it != lbls.end(); ++it)
					{
						string valStr;
						tp->getValue(*it, valStr);
						if( CompStr(*it, "ID") == 0 )
						{
							proc.rank = atoi(valStr.c_str());
							continue;
						}
						if( CompStr(*it, "BlockRange") == 0 )
						{
							double range[3];
							tp->splitRange(valStr, &range[0], &range[1], &range[2]);
							proc.rangeMin = static_cast<unsigned int>(range[0]);
							proc.rangeMax = static_cast<unsigned int>(range[1]);
							continue;
						}
					}
					procList.push_back(proc);
					tp->changeNode("../");
				}
			}

			if( procList.size() != m_numProcs )
			{
				VXLogE("Read File Error (%s) [%s:%d] \n", filepath.c_str(), __FILE__, __LINE__);
				procList.clear();
				delete tp;
				return false;
			}
			
			delete tp;
			return true;
		}

		b8 LoadIndexCellID(TextParser *tp, VOX::IdxBlock& ib)
		{
			using namespace std;
			vector<string>lbls;
			tp->getLabels(lbls);

			bool hasName       = false;
			bool hasBitWidth   = false;
			bool hasPrefix     = false;
			bool hasExtension  = false;
			bool hasGatherMode = false;

			for(vector<string>::iterator it = lbls.begin(); it != lbls.end(); ++it)
			{
				string valStr;
				tp->getValue(*it, valStr);

				if( CompStr(*it, "name") == 0 ){
					ib.name = valStr;
					hasName = true;
					continue;
				}

				if( CompStr(*it ,"BitWidth") == 0 ){
					ib.bitWidth = atoi(valStr.c_str());
					ib.kind     = VOX::LB_CELLID;
					ib.dataType = VOX::LB_UINT8;
					hasBitWidth = true;
					continue;
				}

				if( CompStr(*it, "VirtualCellSize") == 0 ){
					ib.vc = atoi(valStr.c_str());
					continue;
				}

				if( CompStr(*it, "Prefix") == 0 ){
					ib.prefix = valStr;
					hasPrefix = true;
					continue;
				}

				if( CompStr(*it, "Extension") == 0 ){
					ib.extension = valStr;
					hasExtension = true;
					continue;
				}

				if( CompStr(*it, "DirectoryPath") == 0 ){
					ib.dataDir = FixDirectoryPath(valStr);
					continue;
				}

				if( CompStr(*it, "GatherMode") == 0 ){
					if( CompStr(valStr, "distributed") == 0 ){
						ib.isGather = false;
						hasGatherMode = true;
					}else if( CompStr(valStr, "gathered") == 0 ){
						ib.isGather = true;
						hasGatherMode = true;
					}else{
						VXLogE("value (%s) of Keyword [GatherMode] is invalid.\n", valStr.c_str());
						return false;
					}
					continue;
				}
			}

			if( !hasName || !hasBitWidth || !hasPrefix || !hasExtension || !hasGatherMode )
			{
				VXLogE("Read File Error [%s:%d] \n", __FILE__, __LINE__);
				return false;
			}

			return true;
		}


		b8 LoadIndex(const std::string& filepath)
		{
			using namespace std;
			TextParser *tp = new TextParser;

			string octFilename;
			string procFilename;

			VX::Math::vec3 globalOrigin;
			VX::Math::vec3 globalRegion;
			VX::Math::idx3 blockSize;

			std::vector<VOX::IdxProc> idxProcList;

			//std::string dir = FixDirectoryPath(GetDirectory(filepath));
			
			if( tp->read(filepath) != TP_NO_ERROR ){
				VXLogE("Read File Error (%s) [%s:%d] \n", filepath.c_str(), __FILE__, __LINE__);
				delete tp;
				return false;
			}
			
			if( tp->changeNode("/BCMTree") != TP_NO_ERROR ){
				VXLogE("Read File Error (%s) [%s:%d] \n", filepath.c_str(), __FILE__, __LINE__);
				delete tp;
				return false;
			}
			// Read Octree Filename
			if( tp->getValue("TreeFile", octFilename) != TP_NO_ERROR ){
				VXLogE("Read File Error (%s) [%s:%d] \n", filepath.c_str(), __FILE__, __LINE__);
				delete tp;
				return false;
			}

			// Read Proc Filename
			if( tp->getValue("ProcFile", procFilename) != TP_NO_ERROR ){
				VXLogE("Read File Error (%s) [%s:%d] \n", filepath.c_str(), __FILE__, __LINE__);
				delete tp;
				return false;
			}
			
			string procFilepath = m_rootDirectory + procFilename;
			if( !LoadIndexProc(procFilepath, idxProcList ) ){
				VXLogE("Read File Error (%s) [%s:%d] \n", filepath.c_str(), __FILE__, __LINE__);
				delete tp;
				return false;
			}
			
			// Read Domain
			tp->changeNode("/Domain");
			{
				bool hasOrigin = false;
				bool hasRegion = false;
				vector<string> lbls;
				tp->getLabels(lbls);
				for(vector<string>::iterator it = lbls.begin(); it != lbls.end(); ++it)
				{
					string valStr;
					tp->getValue(*it, valStr);
					if( CompStr(*it, "GlobalOrigin") == 0 )
					{
						if( ReadVec3(tp, *it, globalOrigin) == TP_NO_ERROR ){ hasOrigin = true; }
						continue;
					}
					if( CompStr(*it, "GlobalRegion") == 0 )
					{
						if( ReadVec3(tp, *it, globalRegion) == TP_NO_ERROR ){ hasRegion = true; }
						continue;
					}
				}
				if( !hasOrigin || !hasRegion )
				{
					VXLogE("Read File Error (%s) [%s:%d] \n", filepath.c_str(), __FILE__, __LINE__);
					delete tp;
					return false;
				}
			}

			std::vector<VOX::IdxBlock> idxBlockList;
			tp->changeNode("/LeafBlock");
			{
				vector<string> nodes;
				tp->getNodes(nodes, 1);
				for(vector<string>::iterator nit = nodes.begin(); nit != nodes.end(); ++nit)
				{
					if( CompStr(nit->substr(0, 6), "CellID") == 0 )
					{
						tp->changeNode(*nit);
						VOX::IdxBlock ib;
						if( LoadIndexCellID(tp, ib) ){
							ib.rootDir = m_rootDirectory;
							idxBlockList.push_back(ib);
						}
						tp->changeNode("../");
						continue;
					}
				}
				if( idxBlockList.size() == 0 )
				{
					VXLogE("Read File Error (%s) [%s:%d] \n", filepath.c_str(), __FILE__, __LINE__);
					delete tp;
					return false;
				}

				if( ReadVec3(tp, "size", blockSize) != TP_NO_ERROR )
				{
					VXLogE("Read File Error (%s) [%s:%d] \n", filepath.c_str(), __FILE__, __LINE__);
					delete tp;
					return false;
				}
			}
			delete tp;
			
			bool hasCellIDBlock = false;
			for(std::vector<VOX::IdxBlock>::iterator it = idxBlockList.begin(); it != idxBlockList.end(); it++){
				if( it->kind == VOX::LB_CELLID ){
					m_idxBlock = (*it);
					hasCellIDBlock = true;
					break;
				}
			}

			if( !hasCellIDBlock )
			{
				return false;
			}

			
			m_origin    = globalOrigin;
			m_region    = globalRegion;
			m_blockSize = blockSize;

			m_octFilepath = m_rootDirectory + octFilename;

			return true;
		}

		b8 m_isOK;

		std::string m_rootDirectory;
		std::string m_octFilepath;
		
		VOX::IdxBlock m_idxBlock; // CellID Block Meta Information
		u32 m_numProcs;

		VX::Math::vec3 m_origin;
		VX::Math::vec3 m_region;
		VX::Math::idx3 m_rootDims;
		VX::Math::idx3 m_blockSize;

		std::vector<VOX::Pedigree> m_pedigrees;
		std::vector<VX::SG::Block*> m_blocks;
	};

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

} // namespace 

MeshBCMLoader::MeshBCMLoader()
{

}

MeshBCMLoader::~MeshBCMLoader()
{

}

VX::SG::Group* MeshBCMLoader::Load(const std::string& filepath)
{
	using namespace std;
	using namespace VX::SG;

	VXLogD("Load BCM File [Start] \n");
	BCMLoader bcmLoader(filepath);
	if( !bcmLoader.IsOK() ){
		bcmLoader.Clear();
		return NULL;
	}
	VXLogD("Load BCM File [End] \n");
	
	VXLogD("Create VX::SG::MeshBCM [Start] \n");
	VX::SG::MeshBCM* mesh = vxnew VX::SG::MeshBCM( bcmLoader.GetOrigin(),
	                                               bcmLoader.GetRegion(),
												   bcmLoader.GetRootDims(),
												   bcmLoader.GetBlockSize(),
												   bcmLoader.GetBitWidth(),
												   bcmLoader.GetPedigrees(),
												   bcmLoader.GetBlocks() );
	VXLogD("Create VX::SG::MeshBCM [End] \n");
	mesh->SetName("BCM CellID Mesh");
	return mesh;
}


