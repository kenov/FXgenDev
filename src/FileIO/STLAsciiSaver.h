//
//  STLAsciiSaver.h
//

#ifndef INCLUDE_VX_STL_ASCII_SAVER_H
#define INCLUDE_VX_STL_ASCII_SAVER_H

#include "../VX/Type.h"
#include "BaseSaver.h"

namespace VX {
	namespace SG{
		class Node;
	}
}
	
class STLAsciiSaver : BaseSaver
{
public:
	STLAsciiSaver();
	~STLAsciiSaver();
	
	b8 Save(VX::Stream* st, const VX::SG::Node* node, f32 scale = 1, b8 visibleOnly = true);
};
	
#endif // INCLUDE_VX_STL_ASCII_SAVER_H

