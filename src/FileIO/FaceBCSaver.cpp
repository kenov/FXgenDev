#include "../VX/VX.h"
#include "../VX/Math.h"
#include "../VX/SG/SceneGraph.h"

#include "FaceBCSaver.h"
#include "FileIOUtil.h"

#include <fstream>
#include <sstream>

namespace {

} // namespace

b8 WriteFaceBC( std::ostringstream& os, const VX::SG::Node* node)
{
	using namespace std;
	using namespace VX::SG;
	
	if( !node ){ return false; }
	if( node->GetType() != NODETYPE_BBOX_DOMAIN &&
		node->GetType() != NODETYPE_BBOX_DOMAIN_CARTESIAN &&
	    node->GetType() != NODETYPE_BBOX_DOMAIN_BCM )
	{
		VXLogE("Node type is not Domain\n");
		return false;
	}

	const Domain* domain = dynamic_cast<const Domain*>(node);

	const char* dirTag[6] = {
		"Xminus", "Xplus",
		"Yminus", "Yplus",
		"Zminus", "Zplus"
	};
	
	vector<const OuterBC*> bcs(6);
	vector<const Medium*> meds(6);

	for(int i = 0; i < 6; i++){
		const OuterBC* bc = domain->GetFlowBC(static_cast<Domain::BC_DIRECTION>(i));
		if( bc == NULL ) { 
			VXLogE("OuterBoundary of Face (%s) is missing.\n", dirTag[i]);
			return false;
		}
		if( bc->GetRef() < 2 )
		{
			VXLogE("OuterBoundary of Face (%s) is missing.\n", dirTag[i]);
			return false;
		}

		bcs[i] = bc;
		
		const Medium* med = domain->GetGuideCellMedium(static_cast<Domain::BC_DIRECTION>(i));
		if( med == NULL ) {
			VXLogE("Guide Cell Medium of Face (%s) is missing.\n", dirTag[i]);
			return false;
		}
		if( med->GetRef() < 2 )
		{
			VXLogE("Guide Cell Medium of Face (%s) is missing.\n", dirTag[i]);
			return false;
		}

		meds[i] = med;
	}
	
	os << "  OuterBC {" << endl;
	for(int i = 0; i < 6; i++){
		os << "      " << dirTag[i] << " =\"" << bcs[i]->GetAlias() << "\"" << endl;
		std::string medium = meds[i]->GetLabel();
	}
	os << "  }" << endl;// OuterBC

	return true;
}

FaceBCSaver::FaceBCSaver()
{

}

FaceBCSaver::~FaceBCSaver()
{

}

b8 FaceBCSaver::Save(const std::string& filepath, const VX::SG::Node* node)
{
	
	std::ostringstream os;
	
	os << "BCTable {" << std::endl << "  OuterBoundary {" << std::endl;

	if( !WriteFaceBC( os, node ) ){
		VXLogE("[FaceBC Save] Failed to write FaceBC\n");
		return false;
	}

	os << "  } // OuterBoundary" << std::endl;
	os << "} // BCTable" << std::endl;
	
	std::ofstream ofs( filepath.c_str() );
	if( !ofs ){
		VXLogE("File open error.(%s)\n", filepath.c_str());
		return false;
	}

	ofs << os.str();

	return true;
}

