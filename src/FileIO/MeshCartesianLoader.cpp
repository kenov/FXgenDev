#include "../VX/VX.h"
#include "../VX/SG/SceneGraph.h"

#include "MeshCartesianLoader.h"
#include "FileIOUtil.h"
#include "../VOX/BitVoxel.h"
#include "../VOX/VOXFileCommon.h"

#include "TextParser.h"

#include "../VX/RLE.h"

#define MODULE_NAME "Mesh Cartesian Load"

namespace {

	class DFILoader
	{
	public:
		DFILoader(const std::string& filepath) : m_isOK(false)
		{
			m_rootDirectory = FixDirectoryPath(GetDirectory(filepath));
			if( !LoadIndex(filepath) ){
				return;
			}

			if( !LoadBlocks() ){
				Clear();
				return;
			}
			m_isOK = true;
		}

		~DFILoader()
		{
			// Do not Clear (Blocks) in here
		}

		void Clear()
		{
			using namespace VX::SG;
			for(std::vector<MeshCartesian::CartesianBlock*>::iterator it = m_blocks.begin(); it != m_blocks.end(); ++it)
			{
				if( *it != NULL )
					vxdelete(*it);
			}
			m_blocks.clear();
		}

		const b8 IsOK() const { return m_isOK; }

		const VX::Math::vec3& GetOrigin()    const { return m_origin;        }
		const VX::Math::vec3& GetRegion()    const { return m_region;        }
		const VX::Math::idx3& GetNumDivs()   const { return m_numDivision;   }
		const VX::Math::idx3& GetNumVoxels() const { return m_numVoxel;      }
		const std::vector<VX::SG::MeshCartesian::CartesianBlock*>& GetBlocks() const { return m_blocks; }
	
	private:
		struct idxBlock
		{
			std::string dir;
			std::string prefix;
			u32         gc;

			idxBlock() : gc(0){}
		};

		struct MetaBlock
		{
			u32 id;
			VX::Math::idx3 size;
			VX::Math::idx3 hIdx;
			VX::Math::idx3 tIdx;
			MetaBlock(const u32 _id, const VX::Math::idx3& _size, const VX::Math::idx3& _hIdx, const VX::Math::idx3& _tIdx)
			 : id(_id), size(_size), hIdx(_hIdx), tIdx(_tIdx) { }
		};

		b8 LoadDomain(TextParser* tp, VX::Math::vec3& org, VX::Math::vec3& rgn, VX::Math::idx3& vox, VX::Math::idx3& div)
		{
			using namespace std;
			bool hasOrigin = false;
			bool hasRegion = false;
			bool hasVoxel  = false;
			bool hasDivision = false;
			vector<string> lbls;
			tp->getLabels(lbls);
			for(vector<string>::iterator it = lbls.begin(); it != lbls.end(); ++it){
				string valStr;
				tp->getValue(*it, valStr);

				if( CompStr(*it, "GlobalOrigin") == 0 ){
					if( ReadVec3(tp, *it, org ) == TP_NO_ERROR ){ hasOrigin = true; }
					continue;
				}
				if( CompStr(*it, "GlobalRegion") == 0 ){
					if( ReadVec3(tp, *it, rgn ) == TP_NO_ERROR ){ hasRegion = true; }
					continue;
				}
				if( CompStr(*it, "GlobalVoxel") == 0 ){
					if( ReadVec3(tp, *it, vox ) == TP_NO_ERROR ){ hasVoxel = true; }
					continue;
				}
				if( CompStr(*it, "GlobalDivision") == 0 ){
					if( ReadVec3(tp, *it, div ) == TP_NO_ERROR ){ hasDivision = true; }
					continue;
				}
			}
			if(!hasOrigin || !hasRegion || !hasVoxel || !hasDivision){
				return false;
			}
			return true;
		}

		// Load index.dfi
		b8 LoadIndex(const std::string& filepath)
		{
			using namespace std;
			TextParser *tp = new TextParser;

			if( tp->read(filepath) != TP_NO_ERROR ){
				VXLogE("Read File Error (%s) [%s:%d] \n", filepath.c_str(), __FILE__, __LINE__);
				delete tp;
				return false;
			}

			tp->changeNode("/Domain");
			if( !LoadDomain(tp, m_origin, m_region, m_numVoxel, m_numDivision) )
			{
				VXLogE("Read File Error (%s) [%s:%d] \n", filepath.c_str(), __FILE__, __LINE__);
				delete tp;
				return false;
			}

			tp->changeNode("/FileInfo");
			{
				bool hasDir    = false;
				bool hasPrefix = false;
				bool hasGC     = false;
				vector<string> lbls;
				tp->getLabels(lbls);
				for(vector<string>::iterator it = lbls.begin(); it != lbls.end(); ++it){
					string valStr;
					tp->getValue(*it, valStr);

					if( CompStr(*it, "DirectoryPath") == 0 ){
						m_ib.dir = FixDirectoryPath(valStr);
						hasDir   = true;
						continue;
					}
					if( CompStr(*it, "Prefix") == 0 ){
						m_ib.prefix = valStr;
						hasPrefix = true;
						continue;
					}
					if( CompStr(*it, "GuideCell") == 0 ){
						m_ib.gc = atoi(valStr.c_str());
						hasGC = true;
						continue;
					}
				}
				if(!hasDir || !hasPrefix){
					VXLogE("Read File Error (%s) [%s:%d] \n", filepath.c_str(), __FILE__, __LINE__);
					delete tp;
					return false;
				}
			}

			tp->changeNode("/FilePath");
			{
				bool hasProcFile = false;
				vector<string> lbls;
				tp->getLabels(lbls);
				for(vector<string>::iterator it = lbls.begin(); it != lbls.end(); ++it){
					string valStr;
					tp->getValue(*it, valStr);

					if( CompStr(*it, "Process") == 0 ){
						m_procFilename = valStr;
						hasProcFile = true;
					}
				}
				if(!hasProcFile){
					VXLogE("Read File Error (%s) [%s:%d] \n", filepath.c_str(), __FILE__, __LINE__);
					delete tp;
					return false;
				}
			}

			delete tp;	
			return true;
		}

		b8 LoadProcRank(TextParser* tp, int &id, VX::Math::idx3& voxelSize, VX::Math::idx3& headIndex, VX::Math::idx3& tailIndex)
		{
			using namespace std;
			bool hasID = false;
			bool hasVoxelSize = false;
			bool hasHeadIndex = false;
			bool hasTailIndex = false;

			vector<string> lbls;
			tp->getLabels(lbls);
			for(vector<string>::iterator it = lbls.begin(); it != lbls.end(); ++it){
				string valStr;
				tp->getValue(*it, valStr);

				if( CompStr(*it, "ID") == 0 ){
					id = atoi(valStr.c_str());
					hasID = true;
					continue;
				}
				if( CompStr(*it, "VoxelSize") == 0 ){
					if( ReadVec3(tp, *it, voxelSize ) == TP_NO_ERROR ){ hasVoxelSize = true; }
					continue;
				}
				if( CompStr(*it, "HeadIndex") == 0 ){
					if( ReadVec3(tp, *it, headIndex ) == TP_NO_ERROR ){ hasHeadIndex = true; }
					continue;
				}
				if( CompStr(*it, "TailIndex") == 0 ){
					if( ReadVec3(tp, *it, tailIndex ) == TP_NO_ERROR ){ hasTailIndex = true; }
					continue;
				}
			}
			if(!hasID || !hasVoxelSize || !hasHeadIndex || !hasTailIndex ){
				return false;
			}
			return true;
		}

		VX::SG::MeshCartesian::CartesianBlock* LoadBlockFile( const int id,
		                                                      const VX::Math::idx3& size,
		                                                      const VX::Math::idx3& headIndex,
															  const VX::Math::idx3& tailIndex )
		{
			//VXLogI("id(%3d) size(%3d, %3d %3d) hIdx(%3d %3d %3d) tIdx(%3d %3d %3d)\n", id,
			//         size.x, size.y, size.z, headIndex.x, headIndex.y, headIndex.z, tailIndex.x, tailIndex.y, tailIndex.z);
			
			using namespace std;
			using namespace VX::SG;
			using namespace VX::Math;

			string dir = m_rootDirectory + m_ib.dir;
			char filename[128];
			sprintf(filename, "%s_%06d.lb", m_ib.prefix.c_str(), id);
			string filepath = dir + filename;

			FILE *fp = NULL;
			if( (fp = fopen(filepath.c_str(), "rb")) == NULL ){
				VXLogE("File Open Error (%s) [%s:%d]\n", filepath.c_str(), __FILE__, __LINE__);
				return NULL;
			}

			VOX::LBHeader header;
			VOX::CellIDCapsule cc;

			bool isNeedSwap;
			if( !Load_LeafBlock_Header(fp, header, isNeedSwap) ){
				VXLogE("File Read Error (%s) [%s:%d]\n", filepath.c_str(), __FILE__, __LINE__);
				fclose(fp);
				return NULL;
			}

			if( !Load_LeafBlock_CellIDHeader(fp, cc.header, isNeedSwap) ){
				VXLogE("File Read Error (%s) [%s:%d]\n", filepath.c_str(), __FILE__, __LINE__);
				fclose(fp);
				return NULL;
			}

			if(header.kind != static_cast<unsigned char>(VOX::LB_CELLID)){
				VXLogE("%s is not Grid file [%s:%d]\n", filepath.c_str(), __FILE__, __LINE__);
				fclose(fp);
				return NULL;
			}

			if(header.bitWidth < 1 && header.bitWidth > 5) {
				VXLogE("%s is not Grid file [%s:%d]\n", filepath.c_str(), __FILE__, __LINE__);
				fclose(fp);
				return NULL;
			}
			
			if(header.size[0] != size.x || header.size[1] != size.y || header.size[2] != size.z)
			{
				VXLogE("File Read Error (%s) [%s:%d]\n", filepath.c_str(), __FILE__, __LINE__);
				fclose(fp);
				return NULL;
			}

			if(header.vc != m_ib.gc){
				VXLogE("File Read Error (%s) [%s:%d]\n", filepath.c_str(), __FILE__, __LINE__);
				fclose(fp);
				return NULL;
			}

			if( !Load_LeafBlock_CellIDData(fp, &cc.data, header, cc.header, isNeedSwap) ) {
				VXLogE("File Read Error (%s) [%s:%d]\n", filepath.c_str(), __FILE__, __LINE__);
				fclose(fp);
				return NULL;
			}

			//VXLogI("cc.header.numBlock : %ld, cc.header.compSize : %ld\n", cc.header.numBlock, cc.header.compSize);
			//for(int i = 0; i < cc.header.compSize; i++){
			//	VXLogI("cc.data[%d] : %d\n", i, cc.data[i]);
			//}
			
			fclose(fp);

			u8* rleBuf = NULL;
			size_t rleSize = 0;

			if( header.vc != 0 ){
				// Reshape

				////////////////////////
				// this is decomplesser.

				unsigned char* voxel = DecompCellIDData( header, cc );
				idx3 bsz( header.size[0], header.size[1], header.size[2] );
				int vc = header.vc;

				u8* block = new u8[bsz.x * bsz.y * bsz.z];
				size_t fbsize = (bsz.x + vc*2) * (bsz.y + vc*2) * (bsz.z + vc*2);

				for(u32 z = 0; z < header.size[2]; z++){
					for(u32 y = 0; y < header.size[1]; y++){
						size_t bloc  = 0    + ( y     +  z     *  bsz.y         ) *  bsz.x;
						size_t fbloc = 0+vc + ((y+vc) + (z+vc) * (bsz.y+(vc*2)) ) * (bsz.x+(vc*2));
						memcpy(&block[bloc], &voxel[fbloc], sizeof(unsigned char) * bsz.x);
					}
				}
				
				size_t bitVoxelSize = 0;
				//
				//  Bitvoxel encode
				//
				VOX::bitVoxelCell* bitVoxel = VOX::CompressBitVoxel(&bitVoxelSize, bsz.x * bsz.y * bsz.z, block, static_cast<u8>(header.bitWidth));
				size_t bvs = bitVoxelSize * sizeof(VOX::bitVoxelCell);
				
				//
				//  RLE encode
				//
				rleBuf = VX::rleEncode<VOX::bitVoxelCell, u8>(bitVoxel, bvs, &rleSize);
				delete [] block;
				delete [] bitVoxel;
			}
			else if( cc.header.compSize == 0 ){
				size_t bitVoxelSize = VOX::GetBitVoxelSize(header.size[0] * header.size[1] * header.size[2], static_cast<u8>(header.bitWidth));
				size_t bvs = bitVoxelSize * sizeof(VOX::bitVoxelCell);
				
				//
				//  RLE encode
				//
				rleBuf = VX::rleEncode<VOX::bitVoxelCell, u8>(reinterpret_cast<VOX::bitVoxelCell*>(cc.data), bvs, &rleSize); 
				delete [] cc.data;
			}
			else
			{
				rleBuf = cc.data;
				rleSize = cc.header.compSize;
			}
			size_t bitVoxelSize = VOX::GetBitVoxelSize((size.x * size.y * size.z), static_cast<u8>(header.bitWidth));
			u8* rleData = new u8[rleSize];
			memcpy(rleData, rleBuf, sizeof(u8) * rleSize);
			delete [] rleBuf;

			MeshCartesian::CartesianBlock *cblock = 
			      vxnew MeshCartesian::CartesianBlock(size, headIndex, rleData, rleSize, bitVoxelSize, static_cast<u8>(header.bitWidth));

			return cblock;
		}

		// Load Proc.dif and Block files
		b8 LoadBlocks()
		{
			using namespace std;
			using namespace VX::SG;
			using namespace VX::Math;
			TextParser *tp = new TextParser;

			string filepath = m_rootDirectory + m_procFilename;
			if( tp->read(filepath) != TP_NO_ERROR ){
				VXLogE("Read File Error (%s) [%s:%d] \n", filepath.c_str(), __FILE__, __LINE__);
				delete tp;
				return false;
			}
			
			tp->changeNode("/Domain");
			{
				VX::Math::vec3 org;
				VX::Math::vec3 rgn;
				VX::Math::idx3 vox;
				VX::Math::idx3 div;
				if( !LoadDomain(tp, org, rgn, vox, div) )
				{
					VXLogE("Read File Error (%s) [%s:%d] \n", filepath.c_str(), __FILE__, __LINE__);
					delete tp;
					return false;
				}
				if( m_numVoxel != vox || m_numDivision != div )
				{
					VXLogE("Read File Error (%s) [%s:%d] \n", filepath.c_str(), __FILE__, __LINE__);
					delete tp;
					return false;
				}
			}
			
			size_t numProcs = 0;
			tp->changeNode("/MPI");
			{
				vector<string> lbls;
				tp->getLabels(lbls);
				for(vector<string>::iterator it = lbls.begin(); it != lbls.end(); ++it){
					string valStr;
					tp->getValue(*it, valStr);
					if( CompStr(*it, "NumberOfRank") == 0){
						numProcs = atoi(valStr.c_str());
						continue;
					}
				}
			}
			
			std::vector<MetaBlock> metas;

			tp->changeNode("/Process");
			{
				vector<string> nodes;
				tp->getNodes(nodes, 1);
				if( nodes.size() != numProcs )
				{
					VXLogW("it is different to NumberOfRank and Process of Rank count\n");
				}
				
				metas.reserve(nodes.size());
				for(vector<string>::iterator nit = nodes.begin(); nit != nodes.end(); ++nit){
					if( CompStr(nit->substr(0, 4), "Rank") == 0 ){
						tp->changeNode(*nit);
						int id;
						idx3 size, hIdx, tIdx;
						if( !LoadProcRank(tp, id, size, hIdx, tIdx) ){
							VXLogE("Read File Error (%s) [%s:%d] \n", filepath.c_str(), __FILE__, __LINE__);
							delete tp;
							return false;
						}
						metas.push_back(MetaBlock(id, size, hIdx, tIdx));

						tp->changeNode("../");
					}
				}
			}
			delete tp;

			// Load Block Files	
			m_blocks.resize(metas.size());
			for(vector<MeshCartesian::CartesianBlock*>::iterator it = m_blocks.begin(); it != m_blocks.end(); ++it ){
				*it = NULL;
			}
			bool ret = true;
			//#pragma omp parallel for
			for(s32 i = 0; i < static_cast<s32>(metas.size()); i++)
			{
				m_blocks[i] = LoadBlockFile(metas[i].id, metas[i].size, metas[i].hIdx, metas[i].tIdx);
				if( m_blocks[i] == NULL){
					VXLogE("Read File Error (%s) [%s:%d] \n", filepath.c_str(), __FILE__, __LINE__);
					ret = false;
				}
			}

			return ret;
		}

	private:
		b8 m_isOK;

		std::string m_rootDirectory;

		std::string m_procFilename;
		idxBlock m_ib;

		int m_numProcs;
		VX::Math::vec3 m_origin;
		VX::Math::vec3 m_region;
		VX::Math::idx3 m_numVoxel;
		VX::Math::idx3 m_numDivision;
		std::vector<VX::SG::MeshCartesian::CartesianBlock*> m_blocks;
	};

} // namespace 

MeshCartesianLoader::MeshCartesianLoader()
{

}

MeshCartesianLoader::~MeshCartesianLoader()
{

}

VX::SG::Group* MeshCartesianLoader::Load(const std::string& filepath)
{
	using namespace std;
	using namespace VX::SG;

	DFILoader dfiLoader(filepath);
	if( !dfiLoader.IsOK() ){
		return NULL;
	}
	
	VX::SG::MeshCartesian* mesh = vxnew VX::SG::MeshCartesian(dfiLoader.GetOrigin(),
		                                                      dfiLoader.GetRegion(),
															  dfiLoader.GetNumVoxels(),
															  dfiLoader.GetNumDivs(),
															  dfiLoader.GetBlocks());
	mesh->SetName("Cartesian CellID Mesh");

	return mesh;
}


