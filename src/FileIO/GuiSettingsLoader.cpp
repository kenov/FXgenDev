#include "../VX/VX.h"
#include "../VX/SG/SceneGraph.h"

#include "GuiSettingsLoader.h"
#include "FileIOUtil.h"
#include "TextParser.h"

#include <vector>
#include <string>

#define MODULE_NAME "GuiSettings Load"

namespace {
} // namespace

GuiSettingsLoader::GuiSettingsLoader()
{

}


GuiSettingsLoader::~GuiSettingsLoader()
{

}

bool GuiSettingsLoader::splitToFloat(const std::string& val, float color[4])
{
	return true;
}

b8 GuiSettingsLoader::Load(const std::string& filepath, VX::SG::Group* setting )
{
	using namespace std;
	using namespace VX::SG;

	int err = 0;
	TextParser *tp = new TextParser();
	
	// remove old data;
	tp->remove();

	err = tp->read(filepath);
	if(err != 0) { 
		VXLogE("[%s]. TP errNo=[%d]. Failed to parse %s \n", MODULE_NAME,err, filepath.c_str());

		delete tp; return false; 
	}

	GUISettings* guiset = vxnew GUISettings();

	if( tp->changeNode("/ColorTable") == TP_NO_ERROR ){
		vector<string> nodes;
		tp->getNodes(nodes, 1);
		for(vector<string>::iterator nit = nodes.begin(); nit != nodes.end(); ++nit){
			if( CompStr(nit->substr(0, 5), "color") == 0) {
				tp->changeNode(*nit);

				vector<string> labels;
				if( tp->getLabels(labels) != TP_NO_ERROR ){
					VXLogW("[%s] Failed to parse OuterBCClass %s \n", MODULE_NAME, nit->c_str());
					continue;
				}
				string name;
				for(vector<string>::iterator lit = labels.begin(); lit != labels.end(); ++lit){
					string valstr;
					tp->getValue(*lit, valstr);
					if( CompStr( *lit, "rgba" ) == 0 ){
						name = valstr;
						if( name.length() == 0 ){ 
							VXLogW("[%s] AutoColorTable %s has no rgba\n", MODULE_NAME, nit->c_str());
							continue;
						}

						VX::Math::vec4 v;
						if( ReadVec4(tp, *lit, v) == TP_NO_ERROR ) { 
							
							// default is only red color
							VX::Math::vec4 e;
							e.r=v[0];
							e.g=v[1];
							e.b=v[2];
							e.a=v[3];

							guiset->Append(e);

						}
					}

				}

				tp->changeNode("../");
			}else{
				continue;
			}
		}
	}else{
		VXLogW("[%s] No AutoColorTable\n", MODULE_NAME);
		delete tp; return false;
	}

	if( tp->changeNode("/BCflg") == TP_NO_ERROR ){

		vector<string> labels;
		if( tp->getLabels(labels) != TP_NO_ERROR ){
			VXLogW("Failed to parse BCflg %s \n", MODULE_NAME);
			delete tp; return false;
		}

		for(vector<string>::iterator lit = labels.begin(); lit != labels.end(); ++lit){
			string valstr;
			tp->getValue(*lit, valstr);
			if( CompStr( *lit, "TriOffsetScaleRatio" ) == 0 ){				
				float radio = (float)atof(valstr.c_str());
				if(0<=radio&&radio<1){
					//static �ɐݒ�
					VoxelCartesianBCflag::SetBCflgTriOffsetScale(radio);
				}
				break;
			}

		}
	}else{
		VXLogW("[%s] No BCflg\n", MODULE_NAME);
		delete tp; return false;
	}



	tp->remove();

	if( guiset->GetColor().size()==0){
		delete tp; return false;
	}

	// Delete old 
	for(int i = setting->GetChildCount()-1; i >=0; i--){
		Node* n = setting->GetChild(i);
		NODETYPE type = n->GetType();
		if( type == NODETYPE_GUI_SETTINGS  )
		{
			setting->RemoveChild(n);
		}
	}

	// Add new 
	setting->AddChild(guiset);

	delete tp;
	return true;
}


