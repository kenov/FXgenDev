//
//  STLBinarySaver.cpp
//

#include "../VX/VX.h"
#include "STLBinarySaver.h"

#include "../VX/Stream.h"
#include "../VX/SG/SceneGraph.h"
	
#include <stdio.h>

STLBinarySaver::STLBinarySaver(){}
STLBinarySaver::~STLBinarySaver(){}
	
class CheckIndexCountVisitor
{
public:
	CheckIndexCountVisitor(u32* vertcounter)
	{
		m_counter = vertcounter;
	}
	void operator()(const VX::SG::Geometry* g) const
	{
		*m_counter += g->GetIndexCount();
	}

private:
	u32* m_counter;
};

class DumpNodeToSTLBinaryVisitor
{
public:
	DumpNodeToSTLBinaryVisitor(VX::Stream* st, u32 blockNum, f32 scale = 1)
	{
		m_st = st;
		m_blockNum = blockNum;
		m_modelScale = scale;
	}
	void operator()(const VX::SG::Geometry* g) const
	{
		s8* blockbuf = vxnew s8[50 * m_blockNum];
		memset(blockbuf, 0, 50 * m_blockNum);

		char* p = blockbuf;
		unsigned int writesize = 0;

		for (u32 i=0; i<g->GetIndexCount(); i+=3)
		{
			const u32 index_0 = g->GetIndex()[i];
			const u32 index_1 = g->GetIndex()[i+1];
			const u32 index_2 = g->GetIndex()[i+2];

			const VX::Math::vec3& vert_0 = g->GetVertex()[index_0].pos * m_modelScale;
			const VX::Math::vec3& vert_1 = g->GetVertex()[index_1].pos * m_modelScale;
			const VX::Math::vec3& vert_2 = g->GetVertex()[index_2].pos * m_modelScale;
			VX::Math::vec3 normal = VX::Math::normalize(VX::Math::cross((vert_1 - vert_0), (vert_2 - vert_0)));

			memcpy(p, reinterpret_cast<const void*>(&normal), sizeof(f32) * 3);
			p += sizeof(f32) * 3;
			memcpy(p, reinterpret_cast<const void*>(&vert_0), sizeof(f32) * 3);
			p += sizeof(f32) * 3;
			memcpy(p, reinterpret_cast<const void*>(&vert_1), sizeof(f32) * 3);
			p += sizeof(f32) * 3;
			memcpy(p, reinterpret_cast<const void*>(&vert_2), sizeof(f32) * 3);
			p += sizeof(f32) * 3;
			
			u32 idx = ((g->GetIDBuffer()[i/3] >> 8) & 0xFF);
			const u16 bid = idx;
			memcpy(p, reinterpret_cast<const void*>(&bid), sizeof(u16));
			p += 2;

			writesize += 50;

			if (writesize >= m_blockNum * 50)
			{
				m_st->Write(blockbuf, writesize, 0);
				memset(blockbuf, 0, 50 * m_blockNum);
				writesize = 0;
				p = blockbuf;
			}
		}
		if (writesize != 0)
			m_st->Write(reinterpret_cast<void*>(blockbuf), writesize, 0);
		
		vxdeleteArray(blockbuf);

	}
private:
	VX::Stream* m_st;
	u32 m_blockNum;
	f32 m_modelScale;
	
};


b8 STLBinarySaver::Save(VX::Stream* st, const VX::SG::Node* node, f32 scale, b8 visibleOnly)
{
	if (!st || !node)
		return false;

	// header
	char header[80];
	memset(header, 0, 80);
	sprintf(header, "This file is exported from vxGen v4.");
	st->Write(header, 80, 0);

	// check vert num
	u32 vnum = 0;
	CheckIndexCountVisitor vcvisitor(&vnum);
	VX::SG::VisitAllGeometry(node, vcvisitor, visibleOnly);

	vnum /= 3;

	st->Write(reinterpret_cast<void*>(&vnum), sizeof(u32), 0);

	
	// write down per 256K triangles
	DumpNodeToSTLBinaryVisitor visitor(st, 1024 * 256, scale);
	VX::SG::VisitAllGeometry(node, visitor,false);


	return true;
}

