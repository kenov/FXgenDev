
#include "PolylibFileSaver.h"
#include "../VX/VX.h"
#include "../VX/SG/SceneGraph.h"


#include "FileIOUtil.h"


#define MODULE_NAME "Polylib Save"

#define FLOAT_RRECISION_ORDER (6)

/* example

Polylib {

  Duct {
    class_name = "PolygonGroup"
    filepath ="../STL/CurvedDuct.stl"
    movable = "false"
    label = "VirtualMetal"  <= medium
    type = "Obstacle"     <= outer
  }

  Inlet {
    class_name = "PolygonGroup"
    filepath ="../STL/inflow.stl"
    movable = "false"
    label = "VirtualMetal"
    type = "SpecifiedVelocity"
  }

}
*/

b8 PolylibFileSaver::Save(const std::string& filepath,
							const VX::SG::Node* node, 
							const VX::SG::Group* setting,
							const b8& visibleOnly)
{
	using namespace std;
	using namespace VX::SG;

	m_setting = const_cast<VX::SG::Group*>(setting);

	std::ostringstream os;
	os.setf(std::ios::scientific);
	// precision 
	os.precision(FLOAT_RRECISION_ORDER);

	os << "Polylib {" << endl;
	os << std::endl;
	
	VX::SG::Node* nd = const_cast<VX::SG::Node*>(node);

	std::string filepath_wk = ".."; 

	dumpTree( nd ,os,visibleOnly ,filepath_wk );
		
	os << "}" << endl;
	os << endl;

	//////////////////
	// check if possible to open file that I created.
	ofstream ofs( filepath.c_str() );
	if( !ofs ){
		VXLogE("[%s] File open error.(%s)\n", MODULE_NAME, filepath.c_str());
		return false;
	}

	ofs << os.str();

	return true;
}

void PolylibFileSaver::dumpTree( VX::SG::Node* node, std::ostringstream& os , const b8& visibleOnly,const std::string& filepath 
									)
{
	if (!node)
		return;
		
	if (node->GetIntermediate())
		return;
		
	if (visibleOnly && !node->GetVisible())
		return;

	std::string name = node->GetName();
	std::string self_path = filepath + "/" + name;

	// self dump
	using namespace VX::SG;
	using namespace VX::Math;

	NODETYPE t = node->GetType();
	
	if (t == NODETYPE_GEOMETRY)
	{
		// write myself
		self_path += ".stl";
		using namespace VX::Math;
		Geometry* g = static_cast<Geometry*>(node);
		writeNode(g,os,self_path);
		return;
	}

	// call children
	if (t == NODETYPE_GROUP || t == NODETYPE_TRANSFORM)
	{
		Group* g = static_cast<Group*>(node);
		const s32 n = g->GetChildCount();
		for (s32 i = 0; i < n; i++){
			dumpTree(g->GetChild(i),  os, visibleOnly,self_path);
		}
	}	
}

/**
* ex.
  Duct {
    class_name = "PolygonGroup"         <= fix char
    filepath ="../STL/CurvedDuct.stl"  <= stl file path
    movable = "false"                  <= ? fix false
    label = "VirtualMetal"             <= medium
    type = "Obstacle"                  <= outer
  }
*/
void PolylibFileSaver::writeNode( VX::SG::Geometry* node, std::ostringstream& os , 
									const std::string& stl_filepath )
{

	using namespace std;
	using namespace VX::SG;

	std::string nodeName = node->GetName();
	std::string class_name = "PolygonGroup";
	std::string filepath = stl_filepath;
	std::string movable = "false";
	std::string meduim	 ;
	std::string localbc	;

	GetInfo(node , meduim, localbc);

	os << "  " << nodeName << " {" <<endl;
	os << "    class_name = \"" << class_name << "\"" <<endl;
	os << "    filepath = \"" << filepath << "\"" << endl;
	os << "    movable = \"" << movable << "\"" <<endl;
	os << "    label = \"" << meduim << "\"" <<endl;
	os << "    type = \"" << localbc << "\"" <<endl;
	os << "  }" << endl;
	os << std::endl;

}

void PolylibFileSaver::GetInfo(VX::SG::Geometry* g , std::string& medium , std::string& localBC)
{

	// the first triangle 's medium value become the color of Node icon
	u32* idb = g->GetIDBuffer();


	// TODO :  select first polygon 's medium and facebc value
	const u32 n		= g->GetIndexCount()/3;
	// 4th byte is selection byte
	int color_id = 0; // 3th byte for medium 
	int color_id2 = 0;// 2th byte for localBC ?  this is pending
	
	if(n>0){
		//get color id byte of first polygon
		color_id = (idb[0] >> 8) & 0xFF;
		//TODO:fuchi
		color_id2 = (idb[0] >> 16) & 0xFF;
	}

	// get color by colorID()

	int type;
	std::string name ;
	if( GetNameByID( color_id , type , name ) ){
		if(type == VX::SG::NODETYPE_LOCALBC){
			localBC = name;
		}else if(type == VX::SG::NODETYPE_MEDIUM ){
			medium = name;
		}
	}

}

b8 PolylibFileSaver::GetNameByID( const int& id , int& nt , std::string& name )
{

	const s32 n = m_setting->GetChildCount();
	int oid = 0;
	for (s32 i = 0; i < n; ++i) {
		nt = m_setting->GetChild(i)->GetType();

		if (nt == VX::SG::NODETYPE_LOCALBCCLASS){
			const VX::SG::LocalBCClass* cls = static_cast<const VX::SG::LocalBCClass*>(m_setting->GetChild(i));
			;
		}
		else if (nt == VX::SG::NODETYPE_OUTERBCCLASS){
			const VX::SG::OuterBCClass* cls = static_cast<const VX::SG::OuterBCClass*>(m_setting->GetChild(i));
			;
		}
		else if (nt == VX::SG::NODETYPE_LOCALBC){
			const VX::SG::LocalBC* bc = static_cast<const VX::SG::LocalBC*>(m_setting->GetChild(i));
			if( bc->GetID() == id ){
				name = bc->GetAlias();
				return true;
			}
		}
		else if (nt == VX::SG::NODETYPE_OUTERBC){
			const VX::SG::OuterBC* bc = static_cast<const VX::SG::OuterBC*>(m_setting->GetChild(i));
			;
		}
		else if (nt == VX::SG::NODETYPE_MEDIUM){
			const VX::SG::Medium* m = static_cast<const VX::SG::Medium*>(m_setting->GetChild(i));
			if( m->GetID() == id){
				name = m->GetLabel();
				return true;
			}
		}
	}

	return false;
}

