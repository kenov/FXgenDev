//
//  ObjSaver.cpp
//

#include "../VX/VX.h"
#include "ObjSaver.h"

#include "../VX/Stream.h"
#include "../VX/SG/SceneGraph.h"

#include <stdio.h>

ObjSaver::ObjSaver(){}
ObjSaver::~ObjSaver(){}

class DumpNodeToOBJVisitor
{
public:
	DumpNodeToOBJVisitor(VX::Stream* st, const s32& mode, const u32& blocksize)
	{
		m_st = st;
		m_mode = mode;
		m_blockSize = blocksize;
	}
	void operator()(const VX::SG::Geometry* g) const
	{
		char linebuf[512];
		memset(linebuf, 0, 512);
		std::string blockstr;

		switch(m_mode)
		{
			case 0:
			{
				for (u32 v=0; v < g->GetVertexCount(); ++v)
				{
					const VX::SG::Geometry::VertexFormat& vert = g->GetVertex()[v];
					sprintf(linebuf, "v %f %f %f\n",vert.pos[0], vert.pos[1], vert.pos[2]);
					blockstr.append(linebuf);
					if (blockstr.size() > m_blockSize)
					{
						m_st->Write(blockstr.c_str(), static_cast<u32>(blockstr.size() * sizeof(char)), 0);
						blockstr.clear();
					}
				}
				break;
			}
			case 1:
			{
				for (u32 v=0; v < g->GetVertexCount(); ++v)
				{
					const VX::SG::Geometry::VertexFormat& vert = g->GetVertex()[v];
					sprintf(linebuf, "vn %f %f %f\n",vert.normal[0], vert.normal[1], vert.normal[2]);
					blockstr.append(linebuf);
					if (blockstr.size() > m_blockSize)
					{
						m_st->Write(blockstr.c_str(), static_cast<u32>(blockstr.size() * sizeof(char)), 0);
						blockstr.clear();
					}
				}
				break;
			}
			case 2:
			{

				for (u32 i=0; i < g->GetIndexCount(); i += 3)
				{
					u32 index_0 = g->GetIndex()[i]	 + 1; // obj face index start from 1
					u32 index_1 = g->GetIndex()[i+1] + 1;
					u32 index_2 = g->GetIndex()[i+2] + 1;
					sprintf(linebuf, "f %d//%d %d//%d %d//%d\n", index_0,index_0, index_1,index_1, index_2, index_2);
					blockstr.append(linebuf);
					if (blockstr.size() > m_blockSize)
					{
						m_st->Write(blockstr.c_str(), static_cast<u32>(blockstr.size() * sizeof(char)), 0);
						blockstr.clear();
					}
				}
			}
			default:
				break;
		}

		if (!blockstr.empty())
			m_st->Write(blockstr.c_str(), static_cast<u32>(blockstr.size()), 0);

	}
private:
	VX::Stream* m_st;	
	s32 m_mode;
	u32 m_blockSize;
};


b8 ObjSaver::Save(VX::Stream* st, const VX::SG::Node* node, f32 scale, b8 visibleOnly)
{
	if (!st || !node)
		return false;

	std::string header("# This file is exported from vxGen v4.\n");
	st->Write(header.c_str(), static_cast<u32>(header.size()), 0);

	// write down per 5MB
	DumpNodeToOBJVisitor vvisitor(st, 0, 5 * 1024 * 1024);
	VX::SG::VisitAllGeometry(node, vvisitor, false);
	DumpNodeToOBJVisitor nvisitor(st, 1, 5 * 1024 * 1024);
	VX::SG::VisitAllGeometry(node, nvisitor, false);
	DumpNodeToOBJVisitor fvisitor(st, 2, 5 * 1024 * 1024);
	VX::SG::VisitAllGeometry(node, fvisitor, false);

	return true;
}

