
#include "../VX/VX.h"
#include "../VX/SG/SceneGraph.h"

#include "AllParameterSaver.h"
#include "DomainCartesianSaver.h"
#include "DomainBCMSaver.h"
#include "BCMediumListSaver.h"
#include "FaceBCSaver.h"
#include "FileIOUtil.h"

#include <vector>
#include <string>

#include <fstream>
#include <sstream>

#define MODULE_NAME "All Parameter Save"

#define FLOAT_RRECISION_ORDER (6)

b8 AllParameterSaver::Save(const std::string& filepath, const VX::SG::Group* setting, const VX::SG::Domain* domain)
{
	using namespace std;
	using namespace VX::SG;

	ostringstream os;
	os.setf(std::ios::scientific);
	// precision 
	os.precision(FLOAT_RRECISION_ORDER);
	VXLogI("AllParameterSaver::Save=>(%s)\n", filepath.c_str());
	/* ex.
(1)	 
BCTable {
  LocalBoundary {
    XXXX {
      Class           = "Obstacle"
      Medium          = "VirtualMetal"
	   COLOR = "FF0000FF"
    }
	...
  }

  OuterBoundary {
    XXXX {
      Class   = "Wall"
      Type    = "Fixed"
      Medium  = "Virtualmetal"
    }
	..
  
    FaceBC {
      Xminus = "outerwall"
      Xplus  = "outerwall"
      Yminus = "outerwall"
      Yplus  = "tfree"
      Zminus = "outerwall"
      Zplus  = "outerwall"
    }
  }
}
(2)
DomainInfo {
  UnitOfLength   = "NonDimensional"
  GlobalOrigin   = (-7.600000e+00, -6.000000e-01, -1.000000e-01)
  GlobalRegion   = (1.200000e+01, 1.000000e+01, 1.200000e+00)
  GlobalPitch    = (1.250000e-02, 1.250000e-02, 1.250000e-02)

  //GlobalVoxel    = (960   , 800   , 96   )
  //GlobalDivision = (1    , 1    , 4    )

  ActiveSubDomainFile = "hoge"

}
(3)
MediumTable {
  XXXX {
    State               = "Fluid"
	COLOR = "FF0000FF"
  }
  ..
}
*/

	os << endl;
	os << "ApplicationControl {" << endl;
	os << "  ChecckParameter     = \"%%\"" << endl;
	os << "  Operator            = \"%%\"" << endl;
	os << "}" << endl;
	os << endl;
	os << endl;

	//////////////
	// (1) Local / Outer and FaceBC

	if( setting != NULL ){
		os << "BCTable {" << endl;
		os << endl;
		os << "  Boundary {" << endl;

		///////
		// Local Block
		if( !WriteLocalBCList(os, setting) ){
			VXLogE("[%s] Failed to write LocalBC List.\n", MODULE_NAME);
			return false;
		}
		os << std::endl;
	
		//////
		// Get Outer Elements 
		vector<const OuterBC*> bcs;
		for(int i = 0; i < setting->GetChildCount(); i++){
			const Node* n = setting->GetChild(i);
			NODETYPE type = n->GetType();
	
			if(type == NODETYPE_OUTERBC)
			{
				bcs.push_back(dynamic_cast<const OuterBC*>(n));
				continue;
			}
		}
		
		//////
		// Outer block
		if( bcs.size() != 0 ){
			
			//////////
			// Outer Elements
			
			if( !WriteOuterBCList(os, bcs) ){
				VXLogE("[%s] Failed to write OuterBC List.\n", MODULE_NAME);
				return false;
			}
			os << "  }" << endl;

			/////////
			// FaceBC block
			os << endl;
			if( domain != NULL ){
				if( !WriteFaceBC( os, domain ) ){
				//	VXLogE("[%s] Failed to write FaceBC.\n", MODULE_NAME);
				//	return false;
				}
			}
		}
		os << "}" << endl; // BCTable
		os << endl;
	}


	//////////////////
	// (2)  Domain Info

	// TODO Auto change Cartesian / BCM ? <= what is this ? I should ask predecessor

	if( domain != NULL ){
		if( domain->GetType() == NODETYPE_BBOX_DOMAIN_CARTESIAN ){
			const std::string subdomainFilename = GetFilePrefix(filepath) + ".asd";
			if( !WriteDomainCartesian( os, domain, GetDirectory(filepath), subdomainFilename ) ){
				VXLogE("[%s] Failed to write domain.\n", MODULE_NAME);
				return false;
			}
		}else if (domain->GetType() == NODETYPE_BBOX_DOMAIN_BCM ){
			const std::string octreeFilename = GetFilePrefix(filepath) + ".oct";
			if( !WriteDomainBCM( os, domain, GetDirectory(filepath), octreeFilename ) ){
				VXLogE("[%s] Failed to write domain.\n", MODULE_NAME);
				return false;
			}
		}
		os << endl;
	}

	/////////////////////
	// (2.5) FillHint
	os << "FillHint {" << endl;
	os << "  %% {" << endl;
	os << "    Kind       = \"%%\"" << endl;
	os << "    Direction  = \"%%\"" << endl;
	os << "    Coordinate = " << "(";
	os << "%%" << ", " << "%%" << ", " << "%%" << ")" << endl;
	os << "    Medium     = \"%%\"" << endl;
	os << "}" << endl;
	os << endl;


	/////////////////////
	// (3) Medium table
	if( setting != NULL ){
		os << endl;
		if( !WriteMediumList(os, setting) ){
			VXLogE("[%s] Failed to write Medium List.\n", MODULE_NAME);
			return false;
		}
	}

	//////////////////
	// check if possible to open file that I created.
	ofstream ofs( filepath.c_str() );
	if( !ofs ){
		VXLogE("[%s] File open error.(%s)\n", MODULE_NAME, filepath.c_str());
		return false;
	}

	ofs << os.str();

	return true;
}

/* old format

b8 AllParameterSaver::Save(const std::string& filepath, const VX::SG::Group* setting, const VX::SG::Domain* domain)
{
	using namespace std;
	using namespace VX::SG;

	ostringstream os;
	os.setf(std::ios::scientific);
	os.precision(6);

	
	// TODO Auto change Cartesian / BCM
	if( domain != NULL ){
		if( domain->GetType() == NODETYPE_BBOX_DOMAIN_CARTESIAN ){
			const std::string subdomainFilename = GetFilePrefix(filepath) + ".asd";
			if( !WriteDomainCartesian( os, domain, GetDirectory(filepath), subdomainFilename ) ){
				VXLogE("[%s] Failed to write domain.\n", MODULE_NAME);
				return false;
			}
		}else if (domain->GetType() == NODETYPE_BBOX_DOMAIN_BCM ){
			const std::string octreeFilename = GetFilePrefix(filepath) + ".oct";
			if( !WriteDomainBCM( os, domain, GetDirectory(filepath), octreeFilename ) ){
				VXLogE("[%s] Failed to write domain.\n", MODULE_NAME);
				return false;
			}
		}
		os << endl;
	}
	
	if( setting != NULL ){
		if( !WriteMediumList(os, setting) ){
			VXLogE("[%s] Failed to write Medium List.\n", MODULE_NAME);
			return false;
		}
	
		os << endl;
		os << "BCTable {" << endl;
		if( !WriteLocalBCList(os, setting) ){
			VXLogE("[%s] Failed to write LocalBC List.\n", MODULE_NAME);
			return false;
		}
		os << std::endl;
	
		vector<const OuterBC*> bcs;
		for(int i = 0; i < setting->GetChildCount(); i++){
			const Node* n = setting->GetChild(i);
			NODETYPE type = n->GetType();
	
			if(type == NODETYPE_OUTERBC)
			{
				bcs.push_back(dynamic_cast<const OuterBC*>(n));
				continue;
			}
		}
		
		if( bcs.size() != 0 ){
			os << "  OuterBoundary {" << endl;
			if( !WriteOuterBCList(os, bcs) ){
				VXLogE("[%s] Failed to write OuterBC List.\n", MODULE_NAME);
				return false;
			}
			os << endl;

			if( domain != NULL ){
				if( !WriteFaceBC( os, domain ) ){
				//	VXLogE("[%s] Failed to write FaceBC.\n", MODULE_NAME);
				//	return false;
				}
			}

			os << "  } // OuterBoundary" << endl;
		}
		os << "} // BCTable" << endl;
		os << endl;
	}

	ofstream ofs( filepath.c_str() );
	if( !ofs ){
		VXLogE("[%s] File open error.(%s)\n", MODULE_NAME, filepath.c_str());
		return false;
	}

	ofs << os.str();

	return true;
}
*/