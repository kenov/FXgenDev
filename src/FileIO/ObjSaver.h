//
//  ObjSaver.h
//

#ifndef INCLUDE_VX_OBJSAVER_H
#define INCLUDE_VX_OBJSAVER_H

#include "../VX/Type.h"
#include "BaseSaver.h"

namespace VX {
	namespace SG{
		class Node;
	}
}
	
class ObjSaver : BaseSaver
{
public:
	ObjSaver();
	~ObjSaver();
	
	b8 Save(VX::Stream* st, const VX::SG::Node* node, f32 scale = 1, b8 visibleOnly = true);
};
	
#endif // INCLUDE_VX_OBJSAVER_H

