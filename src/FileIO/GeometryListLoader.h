/*
 *
 *  FileIO/GeometryListLoader.h
 *
 */

#ifndef __GeometryListLoader_H__
#define __GeometryListLoader_H__

#include <string>

namespace VX {
	namespace SG {
		class Node;
	} // namespace SG
} // namespace VX
class fxgenCore;

class GeometryListLoader 
{
public:
	GeometryListLoader();
	~GeometryListLoader();

	b8 Load(const std::string& filepath, VX::SG::Node* sgroot,fxgenCore* core);
private:

	std::string getAbs_BaseDir(const std::string& baseDir , const std::string& tpfile_path );

	void addSGRecursible(VX::SG::Node* stlTree, const std::string& baseDir);
	b8 registToNode( VX::SG::Node* sgroot,const std::string& baseDir ,const std::vector<std::string>& stlvec,const bool& bRecursible);
	VX::SG::Node* findSTLRoot(const VX::SG::Node* sgroot);
	void addSafeAddInSG( VX::SG::Node* stlTree, const std::string& baseDir, const std::string& stlPath);
	VX::SG::Node* makeFolderPathInSG( VX::SG::Node* stlTree, const std::string& baseDir, const std::string& stlPath);
	VX::SG::Group* GetChildByName( VX::SG::Group* parent,const std::string& name);
	VX::SG::Group* AppendDirChildByName( VX::SG::Group* parent,const std::string& name);
	b8 split(const std::string& str, const char delimiter, std::vector<std::string>& list);
	std::string getDirNameOnly(const std::string& name);

	fxgenCore* m_core;
};


#endif // __BC_CLASS_LOADER_H__
