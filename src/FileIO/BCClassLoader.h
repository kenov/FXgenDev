/*
 *
 *  FileIO/BCClassLoader.h
 *
 */

#ifndef __BC_CLASS_LOADER_H__
#define __BC_CLASS_LOADER_H__

#include <string>
#include "BaseSettingLoader.h"

namespace VX {
	namespace SG {
		class Group;
	} // namespace SG
} // namespace VX

class BCClassLoader : public BaseSettingLoader
{
public:
	BCClassLoader();
	~BCClassLoader();

	b8 Load(const std::string& filepath, VX::SG::Group* setting);
};


#endif // __BC_CLASS_LOADER_H__
