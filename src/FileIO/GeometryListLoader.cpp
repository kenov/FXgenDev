#include "../VX/VX.h"
#include "../VX/SG/SceneGraph.h"
#include "../Core/fxgenCore.h"

#include "GeometryListLoader.h"
#include "FileIOUtil.h"
#include "TextParser.h"


#include <vector>
#include <string>
#include <wx/string.h>
#include <wx/file.h>
#include <wx/dir.h>


#define MODULE_NAME "GeometryList Load"

namespace {
} // namespace

GeometryListLoader::GeometryListLoader():m_core(NULL)
{

}


GeometryListLoader::~GeometryListLoader()
{

}
/*
GeometryList {
	BaseDir = "./"
	Recursive = "false"
	File[@] {
         path = "a1.stl"
	}
	File[@] {
         path = "geometry_list/a/a1.stl"
	}
	File[@] {
         path = "geometry_list/a/a2.stl"
	}
	File[@] {
         path = "geometry_list/b/b1.stl"
	}
	File[@] {
         path = "geometry_list/b/b2.stl"
	}
}
*/

b8 GeometryListLoader::registToNode( VX::SG::Node* sg_root,
									const std::string& baseDir ,
									const std::vector<std::string>& stlvec,
									const bool& bRecursible)
{
	// get STL node
	using namespace VX::SG;
	Group* sgroot = static_cast<Group*>(sg_root);
	Node* stlTree = findSTLRoot(sgroot);
	if(stlTree==NULL){
		stlTree = vxnew Group();
		stlTree->SetName("Model");
		sgroot->AddChild(stlTree);
	}

	if(bRecursible){
		//再帰ロード
		addSGRecursible(stlTree, baseDir);
	}else{
		//個別ロード
		for(int i=0;i<stlvec.size();i++){
			addSafeAddInSG( stlTree, baseDir, stlvec.at(i));
		}
	}

	return true;
}

void GeometryListLoader::addSGRecursible(VX::SG::Node* stlTree, const std::string& baseDir)
{
	VX::SG::Node* dir = m_core->LoadDirModel(baseDir.c_str());

	//directory name
	std::string dirName = getDirNameOnly(dir->GetName());
	dir->SetName(dirName);

	VX::SG::Group* g = static_cast<VX::SG::Group*>(stlTree);
	g->AddChild(dir);

}

std::string GeometryListLoader::getDirNameOnly(const std::string& name)
{
	//stl ファイルのフルパスを分割
	std::vector<std::string> list;
	split(name, '/',list);
	
	//相対パスの場合は、そのまま文字で格納されるため名前だけを取ります
	//最後の文字をディレクトリ名として採用
	std::string dirName;
	if(list.size()>2){
		dirName = list.at(list.size()-1);
	}else{
		//そのまま
		dirName = name;
	}
	return dirName;
}

void GeometryListLoader::addSafeAddInSG( VX::SG::Node* stlTree, const std::string& baseDir, const std::string& stlPath)
{
	std::string stlfile		=  baseDir + stlPath;

	//ファイルが存在しない場合は無視
	if(!wxFile::Exists(stlfile)){
		VXLogE("no exist file:%s\n", stlfile.c_str());
		return;
	}

	VX::SG::Node* stlNode = m_core->LoadModel(stlfile.c_str());
	//stlの途中フォルダがなければ追加する
	VX::SG::Node* parent = makeFolderPathInSG( stlTree, baseDir, stlPath);

	VX::SG::Group* g = static_cast<VX::SG::Group*>(parent);
	g->AddChild(stlNode);
}

VX::SG::Node* GeometryListLoader::makeFolderPathInSG(	VX::SG::Node* _node, 
													const std::string& baseDir,
													const std::string& stlPath
													)
{
	using namespace VX::SG;

	Group* node = static_cast<Group*>(_node);

	//stl ファイルのフルパスを分割
	std::vector<std::string> list;
	split(stlPath, '/',list);
	VX::SG::Group* current = node;
	for(int i=0;i<list.size();i++){

		if(i== list.size()-1){
			//stlファイル名なのでパスに無関係のため無視
			continue;
		}
		//次のパス場所へ
		std::string dirName = list.at(i);
		//カレントノードから該当する子ノードが無いかをチェック
		VX::SG::Group* dirNode = GetChildByName( current,dirName);
		if(dirNode==NULL){
			//無ければ作る
			dirNode = AppendDirChildByName(current,dirName);
		}
		//カレントを切り替え
		current=dirNode;
	}

	return current;
}

b8 GeometryListLoader::split(const std::string& str, const char delimiter, std::vector<std::string>& list)
{
	list.clear();

	std::string istr = str;

	size_t p = 0;
	while( (p = istr.find(delimiter)) != std::string::npos )
	{
		if(p != 0){
			list.push_back( istr.substr(0, p) );
		}
		istr.erase(istr.begin(), istr.begin() + p + 1);
	}

	if( istr.length() != 0 ) list.push_back(istr);

	return true;
}

VX::SG::Group* GeometryListLoader::AppendDirChildByName( VX::SG::Group* parent,const std::string& dirname)
{
	using namespace VX::SG;
	Transform* trs = vxnew Transform();
	trs->SetName(dirname);
	parent->AddChild(trs);
	return trs;
}

VX::SG::Group* GeometryListLoader::GetChildByName( VX::SG::Group* parent,const std::string& name)
{
	using namespace VX::SG;
	const s32 n = parent->GetChildCount();
	for (s32 i = 0; i < n; i++){
		if( parent->GetChild(i)->GetName() == name){
			Node* node = parent->GetChild(i);
			if(node->GetType()==NODETYPE_TRANSFORM){
				Group* g = static_cast<Group*>(node);
				return g;
			}
		}
	}
	return NULL;
}

VX::SG::Node* GeometryListLoader::findSTLRoot(const VX::SG::Node* sgroot)
{
	using namespace VX::SG;
	//"/root/STL/"
	VX::SG::Node* nd = const_cast<VX::SG::Node*>(sgroot);
	Group* g = static_cast<Group*>(nd);
	const s32 n = g->GetChildCount();
	for (s32 i = 0; i < n; i++){
		// find "STL"
		if( g->GetChild(i)->GetName() == "Model"){
			return g->GetChild(i);
		}
	}
	return NULL;
}

std::string GeometryListLoader::getAbs_BaseDir(const std::string& _baseDir , const std::string& _tpfile_path )
{
	//  ./aaa/bbb/  なら./を削り、先頭にtpfileのディレクトリを付ける
	
	std::string baseDir = _baseDir;
	std::string tpfile_path = _tpfile_path;
	// change \ => /
	std::replace( baseDir.begin(), baseDir.end(), '\\', '/' );
	std::replace( tpfile_path.begin(), tpfile_path.end(), '\\', '/' );
	
	std::string path = baseDir;

	if(baseDir.find("./",0)==0){
		int pos = tpfile_path.find_last_of('/');
		if(pos != std::string::npos){
			path = tpfile_path.substr(0,pos) + "/" + baseDir.substr(2) ;
		}
	}

	//最後が/で終わってない場合は付与する
	const char* tdta = path.c_str();
	int tlen = path.length();
	if(tdta[tlen-1]!='/'){
		path += "/";
	}

	return path;
}


b8 GeometryListLoader::Load(const std::string& filepath, VX::SG::Node* sgroot,fxgenCore* core )
{
	using namespace std;
	using namespace VX::SG;

	m_core = core;

	int err = 0;
	TextParser tx;
	TextParser* tp = &tx;
	
	err = tp->read(filepath);
	if(err != 0) { 
		VXLogE("[%s]. TP errNo=[%d]. Failed to parse %s \n", MODULE_NAME,err, filepath.c_str());
		return false; 
	}



	std::string baseDir;
	std::vector<std::string> stlvec;
	bool bAuto = false;
	if( tp->changeNode("/GeometryList") == TP_NO_ERROR ){
		
		// BaseDir in label
		int err=0;
		tp->getValue("BaseDir",baseDir,&err);

		if(err){
			VXLogW("[%s] Failed to parse BaseDir %s \n", MODULE_NAME, "BaseDir");
			return false;
		}
		
		//base dir ,convert path from rel to abs, 
		baseDir = getAbs_BaseDir (baseDir , filepath);

		std::string typeval;
		tp->getValue("Recursive",typeval,&err);
		if(typeval == "true"){
			bAuto = true;
		}

		// File
		vector<string> nodes;
		tp->getNodes(nodes, 1);

		for(vector<string>::iterator nit = nodes.begin(); nit != nodes.end(); ++nit){
			if( CompStr(nit->substr(0, 4), "File") == 0) {
				tp->changeNode(*nit);

				int err=0;
				std::string path;
				tp->getValue("path",path,&err);

				stlvec.push_back(path);

				tp->changeNode("../");

			}
		}


	}else{
		return false;
	}

	tp->remove();


	bool ret = registToNode( sgroot , baseDir ,stlvec,bAuto);

	return ret;
}




