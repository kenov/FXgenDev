/*
 *
 * FileIO/DomainBCMLoader.h
 *
 *
 */

#ifndef __DOMAIN_BCM_LOADER_H__
#define __DOMAIN_BCM_LOADER_H__

#include <string>

namespace VX {
	namespace SG {
		class Node;
	}
}

class DomainBCMLoader
{
public:
	DomainBCMLoader();
	~DomainBCMLoader();

	VX::SG::Node* Load(const std::string& filepath);
};


#endif // __DOMAIN_BCM_LOADER_H__

