/*
 *
 * BaseSettingLoder.h
 *
 */

#ifndef INCLUDE_FILEIO_BASE_SETTING_LOADER_H
#define INCLUDE_FILEIO_BASE_SETTING_LOADER_H

#include "../VX/Type.h"
#include <string>

namespace VX {
	namespace SG {
		class Group;
	} // namespace SG
} // namespace VX

class BaseSettingLoader
{
protected:
	BaseSettingLoader(){};
	virtual ~BaseSettingLoader(){};

public:
	virtual b8 Load(const std::string& filename, VX::SG::Group* setting ) = 0;
};


#endif // INCLUDE_FILEIO_BASE_SETTING_LOADER_H
