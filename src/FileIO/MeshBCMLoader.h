/*
 *
 * FileIO/MeshBCMLoader.h
 *
 *
 */

#ifndef __MESH_BCM_LOADER_H__
#define __MESH_BCM_LOADER_H__

#include <string>

namespace VX {
	namespace SG {
		class Group;
	}
}

class MeshBCMLoader
{
public:
	MeshBCMLoader();
	~MeshBCMLoader();

	VX::SG::Group* Load(const std::string& filepath);
};


#endif // __MESH_CARTESIAN_LOADER_H__

