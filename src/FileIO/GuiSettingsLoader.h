/*
 *
 *  FileIO/GuiSettingsLoader.h
 *
 */

#ifndef __GuiSettingsLoader_H__
#define __GuiSettingsLoader_H__

#include <string>
#include "BaseSettingLoader.h"

namespace VX {
	namespace SG {
		class Group;
	} // namespace SG
} // namespace VX

class GuiSettingsLoader : public BaseSettingLoader
{
public:
	GuiSettingsLoader();
	~GuiSettingsLoader();

	b8 Load(const std::string& filepath, VX::SG::Group* setting);

	bool splitToFloat(const std::string& val, float color[4]); 
};


#endif // __BC_CLASS_LOADER_H__
