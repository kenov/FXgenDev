//
//  STLBinarySaver.h
//

#ifndef INCLUDE_VX_STL_BINARY_SAVER_H
#define INCLUDE_VX_STL_BINARY_SAVER_H

#include "../VX/Type.h"
#include "BaseSaver.h"

namespace VX {
	namespace SG{
		class Node;
	}
}
	
class STLBinarySaver : BaseSaver
{
public:
	STLBinarySaver();
	~STLBinarySaver();
	
	b8 Save(VX::Stream* st, const VX::SG::Node* node, f32 scale = 1, b8 visibleOnly = true);
};
	
#endif // INCLUDE_VX_STL_BINARY_SAVER_H

