//
//  STLBinaryLoader.h
//
//  Created by kioku on 11/09/16.
//  Copyright 2011 System K. All rights reserved.
//

#ifndef INCLUDE_VX_STLBINARYLOADER_H
#define INCLUDE_VX_STLBINARYLOADER_H

#include "../VX/Type.h"
#include "BaseLoader.h"

namespace VX {
	namespace SG{
		class Node;
	}
}
	
class STLBinaryLoader : BaseLoader
{
public:
	STLBinaryLoader();
	~STLBinaryLoader();
	
	VX::SG::Node* Load(const VX::Stream* st);
};
	
#endif // INCLUDE_VX_STLBINARYLOADER_H

