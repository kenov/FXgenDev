//
//  BaseLoader.h
//

#ifndef INCLUDE_VX_BASELOADER_H
#define INCLUDE_VX_BASELOADER_H

#include "../VX/Type.h"

namespace VX {
	
class Stream;
	namespace SG {
		class Node;
	} // namespace SG
	
	static const u32 initColor = 0xFF000000;
}

class BaseLoader
{
protected:
	BaseLoader(){};
	virtual ~BaseLoader(){};
	
public:
	virtual VX::SG::Node* Load(const VX::Stream* s) = 0;
};


#endif // INCLUDE_VX_BASELOADER_H

