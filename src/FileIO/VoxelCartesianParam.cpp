#include "VoxelCartesianParam.h"
#include <assert.h>
#include "../Core/fxgenCore.h"


#include "../VX/SG/Node.h"
#include "../VX/SG/Group.h"
#include "../VX/SG/GUISettings.h"
#include "../VX/SG/Medium.h"
#include "../FileIO/BCMediumIDNumbering.h"
#include <wx/string.h>
#include <wx/file.h>
#include <wx/dir.h>


VoxelCartesianParam::VoxelCartesianParam()
{

}

VoxelCartesianParam::~VoxelCartesianParam()
{

}

//	コピーコンストラクタ
VoxelCartesianParam::VoxelCartesianParam( const VoxelCartesianParam& obj )
{
	assignCopy(obj);
}

VoxelCartesianParam& VoxelCartesianParam::operator=(const VoxelCartesianParam& obj)
{
	if(this == &obj){
		return *this;   //自己代入
	}
	assignCopy(obj);
	return(*this);
}

void VoxelCartesianParam::assignCopy(const VoxelCartesianParam& obj)
{

	m_IndexDfiFile	= obj.m_IndexDfiFile;
	m_CellID_file	= obj.m_CellID_file;
	m_BCflag_file	= obj.m_BCflag_file;
	m_CellID_dir	= obj.m_CellID_dir;
	m_BCflag_dir	= obj.m_BCflag_dir;

}

bool VoxelCartesianParam::isInputValid(std::string& errMsg)const
{
	// dfi file is mandatry. flg dir is option.
	if(	!Has_Dfi_file()){
		errMsg = "set index.dfi file";
		return false;
	}

	if( Has_CellID_file() ||  Has_BCflg_file()){
		return true;//ダイレクトファイルモード
	}

	if( Has_CellID_dir() || Has_BCflg_dir()){
		return true;//ディレクトリモード
	}

	errMsg = "set CellID or BCflag";

	return false;
}
std::string VoxelCartesianParam::getDefaultPath()
{

	char* dir=NULL;
#ifdef _WIN32	
	dir = getenv( "USERPROFILE" );
#else
	dir = getenv( "HOME" );
#endif
	if(dir==NULL){
		assert(false);
		return "";	
	}

	return std::string(dir);
}

void VoxelCartesianParam::Set_Dfi_file(const std::string& path)
{
	assert(existFile(path));
	m_IndexDfiFile = path;
}

std::string VoxelCartesianParam::Get_Dfi_file()const
{
	return m_IndexDfiFile;
}

void VoxelCartesianParam::Set_CellID_file(const std::string& path)
{
	assert(existFile(path));
	m_CellID_file = path;
}

std::string VoxelCartesianParam::Get_CellID_file()const
{
	return m_CellID_file;
}

void VoxelCartesianParam::Set_CellID_dir(const std::string& path)
{
	assert(existDir(path));
	m_CellID_dir = path;
}


std::string VoxelCartesianParam::Get_CellID_dir()const
{
	return m_CellID_dir;
}

void VoxelCartesianParam::Set_BCflg_file(const std::string& path)
{
	assert(existFile(path));
	m_BCflag_file = path;
}

std::string VoxelCartesianParam::Get_BCflg_file()const
{
	return m_BCflag_file;
}

void VoxelCartesianParam::Clear()
{
	m_IndexDfiFile="";
	m_CellID_file="";
	m_BCflag_file="";
	m_CellID_dir="";
	m_BCflag_dir="";
}
void VoxelCartesianParam::Set_BCflg_dir(const std::string& path)
{
	assert(existDir(path));
	m_BCflag_dir = path;
}

std::string VoxelCartesianParam::Get_BCflg_dir()const
{
	return m_BCflag_dir;
}

bool VoxelCartesianParam::existFile(const std::string& path)const
{
	if(path.size()==0){
		return false;
	}
	return wxFile::Exists(path);
}

bool VoxelCartesianParam::existDir(const std::string& path)const
{
	if(path.size()==0){
		return false;
	}
	return wxDir::Exists(path);
}

bool VoxelCartesianParam::Has_Dfi_file()const
{
	return existFile(m_IndexDfiFile);
}


bool VoxelCartesianParam::Has_CellID_file()const
{
	return existFile(m_CellID_file);
}

bool VoxelCartesianParam::Has_CellID_dir()const
{
	return existDir(m_CellID_dir);
}

bool VoxelCartesianParam::Has_BCflg_file()const
{
	return existFile(m_BCflag_file);
}

bool VoxelCartesianParam::Has_BCflg_dir()const
{
	return existDir(m_BCflag_dir);
}

void VoxelCartesianParam::Clear_CellID_file()
{
	m_CellID_file = "";
}

void VoxelCartesianParam::Clear_CellID_dir()
{
	m_CellID_dir = "";
}

void VoxelCartesianParam::Clear_BCflg_file()
{
	m_BCflag_file = "";
}

void VoxelCartesianParam::Clear_BCflg_dir()
{
	m_BCflag_dir = "";
}

bool VoxelCartesianParam::IsDirectMode()const
{
	VoxelCartesianParam::MODE mode = GetMode();
	return ( mode == FUNC_DIRECT_FILE_CELLID || mode == FUNC_DIRECT_FILE_BCFLG);
}

VoxelCartesianParam::MODE VoxelCartesianParam::GetMode()const
{
	//dfiは必須
	if(Has_Dfi_file()){
		
		if(Has_CellID_dir()){
			//BCFlgはオプションなので無視
			return FUNC_DIR;
		}
		if(Has_BCflg_dir()){
			//BCFlgだけの場合もある
			return FUNC_DIR;
		}
		if(Has_CellID_file()){
			//直ファイル指定
			return FUNC_DIRECT_FILE_CELLID;
		}
		if(Has_BCflg_file()){
			//直ファイル指定
			return FUNC_DIRECT_FILE_BCFLG;
		}
		//
	}
	//その他は
	assert(false);//unknown
	return FUNC_NONE;

}


