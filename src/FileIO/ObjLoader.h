//
//  ObjLoader.h
//


#ifndef INCLUDE_VX_OBJLOADER_H
#define INCLUDE_VX_OBJLOADER_H

#include "../VX/Type.h"
#include "BaseLoader.h"

namespace VX {
	namespace SG{
		class Node;
	}
}
	
class ObjLoader : BaseLoader
{
public:
	ObjLoader();
	~ObjLoader();
	
	VX::SG::Node* Load(const VX::Stream* st);
};
	
#endif // INCLUDE_VX_OBJLOADER_H

