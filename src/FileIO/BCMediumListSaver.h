/*
 *
 *  FileIO/BCMediumListSaver.h
 *
 */

#ifndef __BC_MEDIUM_LIST_SAVER_H__
#define __BC_MEDIUM_LIST_SAVER_H__

#include "BaseSettingSaver.h"

namespace VX {
	namespace SG {
		class Group;
	} // namespace SG
} // namespace VX

b8 WriteMediumList( std::ostringstream& os, const VX::SG::Group* setting );
b8 WriteOuterBCList( std::ostringstream& os, const VX::SG::Group* setting );
b8 WriteOuterBCList( std::ostringstream& os, const std::vector<const VX::SG::OuterBC*>& bcs);
b8 WriteLocalBCList( std::ostringstream& os, const VX::SG::Group* setting );
b8 WriteLocalBCList( std::ostringstream& os, const std::vector<const VX::SG::LocalBC*>& bcs);

class BCMediumListSaver : public BaseSettingSaver
{
public:
	BCMediumListSaver();
	~BCMediumListSaver();

	b8 Save(const std::string& filepath, const VX::SG::Group* setting);
};

#endif // __BC_MEDIUM_LIST_SAVER_H__

