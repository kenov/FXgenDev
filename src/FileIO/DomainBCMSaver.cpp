#include "../VX/VX.h"
#include "../VX/Math.h"
#include "../VX/SG/SceneGraph.h"

#include "DomainBCMSaver.h"
#include "FileIOUtil.h"

#include "../VOX/Node.h"
#include "../VOX/Pedigree.h"
#include "../VOX/BCMOctree.h"
#include "../VOX/RootGrid.h"
#include "../VOX/VOXFileCommon.h"

#include <fstream>
#include <sstream>

namespace {

	b8 SaveOctree(const std::string& path, const VX::Math::vec3& origin, const VX::Math::vec3& region, const VOX::BCMOctree* octree)
	{
		FILE *fp = NULL;
		if( (fp = fopen(path.c_str(), "wb")) == NULL )
		{
			VXLogE("[Domain BCM Save] Failed to Open file (%s). [%s:%d]\n", path.c_str(), __FILE__, __LINE__);
			return false;
		}
		
		VOX::OctHeader header;
		header.identifier = OCTREE_FILE_IDENTIFIER;

		const VOX::RootGrid* rootGrid = octree->getRootGrid();

		header.org[0]      = origin.x;
		header.org[1]      = origin.y;
		header.org[2]      = origin.z;
		header.rgn[0]      = region.x;
		header.rgn[1]      = region.y;
		header.rgn[2]      = region.z;
		header.rootDims[0] = rootGrid->getSizeX();
		header.rootDims[1] = rootGrid->getSizeY();
		header.rootDims[2] = rootGrid->getSizeZ();

		header.numLeaf = octree->getNumLeafNode();
		
		const std::vector<VOX::Node*> nodes = octree->getLeafNodeArray();
		std::vector<VOX::Pedigree> pedigrees;
		pedigrees.reserve(nodes.size());

		int maxLevel = 0;
		for(std::vector<VOX::Node*>::const_iterator it = nodes.begin(); it != nodes.end(); ++it)
		{
			const VOX::Node* n = *it;
			maxLevel = n->getLevel() > maxLevel ? n->getLevel() : maxLevel;
			pedigrees.push_back(n->getPedigree());
		}

		header.maxLevel = maxLevel;

		fwrite(&header, sizeof(header), 1, fp);
		fwrite(&pedigrees[0], sizeof(VOX::Pedigree), pedigrees.size(), fp);

		fclose(fp);

		return true;
	}


} // namespace




b8 WriteDomainBCM( std::ostringstream& os, const VX::SG::Node* node, 
                   const std::string& writeDir, const std::string& octreeFilename )
{
	using namespace std;
	using namespace VX::SG;
	using namespace VX::Math;

	if( !node )
		return false;
	
	if( node->GetType() != NODETYPE_BBOX_DOMAIN_BCM )
	{
		VXLogE("Node Type is not BCM domain\n");
		return false;
	}

	const DomainBCM* domain = dynamic_cast<const DomainBCM*>(node);

	const vec3& org = domain->GetOrigin();
	const vec3& rgn = domain->GetRegion();

	const VOX::BCMOctree* octree = NULL;
	octree = domain->GetOctree();

	os << "Domain {" << endl;
	os << "  " << "GlobalOrigin = ";
	os << " (" << org[0] << ", " << org[1] << "," << org[2] << ")" << endl;
	os << "  " << "GlobalRegion = ";
	os << " (" << rgn[0] << ", " << rgn[1] << "," << rgn[2] << ")" << endl;
	os << "}" << endl << endl;

	if( octree && domain->GetExportPolicy() == DomainBCM::EXP_OCTREE )
	{
		os << "BCMTree {" << endl;
		os << "  TreeFile = \"" << octreeFilename << "\"" << endl;
		os << "}" << endl << endl;

		const std::string octreeAbsoluteFilepath = writeDir + "/" + octreeFilename;
		if( !SaveOctree(octreeAbsoluteFilepath, org, rgn, octree) )
		{
			VXLogE("Write Octree Error (%s)\n", octreeAbsoluteFilepath.c_str());
			return false;
		}
	}
	else
	{
		if( domain->GetExportPolicy() == DomainBCM::EXP_OCTREE )
		{
			VXLogW("[Domain BCM Save] Export Policy is Octree but octree is not created yet. So write Parameter.\n");
		}
		
		const VX::Math::idx3& rootDims = domain->GetRootDims();
		const u32 minLevel  = domain->GetMinLevel();
		const u32 baseLevel = domain->GetBaseLevel();

		const std::vector<DomainBCM::RegionScope*>&   rgnList = domain->GetRegionScopeList();
		const std::vector<DomainBCM::GeometryScope*>& geoList = domain->GetGeometryScopeList();

		const char* odrStr[2] = {"Z", "Hilbert"};
		const string ordering = domain->GetLeafOrdering() == DomainBCM::ORDER_Z ?  odrStr[0] : odrStr[1];
		os << "Tree {" << endl;
		os << "  Ordering  = \"" << ordering << "\"" << endl;
		os << "  RootDims  = ("  << rootDims[0] << ", " << rootDims[1] << ", " << rootDims[2] << ")" << endl;
		os << "  BaseLevel = " << baseLevel << endl;
		os << "  MinLevel  = " << minLevel  << endl;
		
		if( rgnList.size() != 0 || geoList.size() != 0 ){
			os << "  MeshDensity {" << endl;
			
			os << "    Margin  = \"" << domain->getMarginType() << "\"" << endl;
			os << "    MarginRatio = "  << domain->getMarginRatio() << endl;
			os << "    MarginCell  = "  << domain->getMarginCell() << endl;
		}

		for(std::vector<DomainBCM::RegionScope*>::const_iterator it = rgnList.begin(); it != rgnList.end(); ++it)
		{
			os << "    BBox[@] {" << endl;
			os << "      Origin = (" << (*it)->origin.x << ", " << (*it)->origin.y << ", " << (*it)->origin.z << ")" << endl;
			os << "      Region = (" << (*it)->region.x << ", " << (*it)->region.y << ", " << (*it)->region.z << ")" << endl;
			os << "      Level  = "  << (*it)->level << endl;
			os << "      Margin  = \""  << (*it)->type << "\"" << endl;
			os << "      MarginRatio = "  << (*it)->ratio << endl;
			os << "      MarginCell  = "  << (*it)->cell << endl;
			os << "    }" << endl << endl;
		}

		for(std::vector<DomainBCM::GeometryScope*>::const_iterator it = geoList.begin(); it != geoList.end(); ++it)
		{
			os << "    Polygon[@] { " << endl;
			os << "      Name  = \"" << (*it)->name << "\"" << endl;
			os << "      Level = "   << (*it)->level << endl;
			os << "      Margin  = \""  << (*it)->type << "\"" << endl;
			os << "      MarginRatio = "  << (*it)->ratio << endl;
			os << "      MarginCell  = "  << (*it)->cell << endl;
			os << "    }" << endl << endl;
		}

		if( rgnList.size() != 0 || geoList.size() != 0 ){
			os << "  }" << endl << endl;
		}

		os << "}" << endl << endl;

	}

	const VX::Math::idx3& size = domain->GetBlockSize();
	os << "LeafBlock {" << endl;
	os << "  " << "Size = ";
	os << " (" << size[0] << ", " << size[1] << ", " << size[2] << ")" << endl;
	os << endl;
	os << "  Unit {" << endl;
	os << "    Length = \"" << domain->GetUnit() << "\"" << endl;
	os << "  }" << endl << endl;
	os << "}" << endl << endl;

	return true;
}


DomainBCMSaver::DomainBCMSaver()
{

}

DomainBCMSaver::~DomainBCMSaver()
{

}

b8 DomainBCMSaver::Save(const std::string& filepath, const VX::SG::Node* node)
{

	std::ostringstream os;
	os.setf(std::ios::scientific);
	os.precision(6);
	
	const std::string octreeFilename = GetFilePrefix(filepath) + ".oct";
	
	if( !WriteDomainBCM( os, node, GetDirectory(filepath), octreeFilename ) ){
		VXLogE("[Domain BCM Save] Failed to write domain\n");
		return false;
	}

	std::ofstream ofs( filepath.c_str() );
	if( !ofs ){
		VXLogE("File open error.(%s)\n", filepath.c_str());
		return false;
	}
	ofs << os.str();

	return true;
}
