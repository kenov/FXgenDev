/*
 *
 * FileIO/AllParameterSaver.h
 *
 */

#ifndef __ALL_PARAMETER_SAVER_H__
#define __ALL_PARAMETER_SAVER_H__

namespace VX {
	namespace SG {
		class Domain;
		class Group;
	}
}

class AllParameterSaver
{
public:
	AllParameterSaver(){ }
	~AllParameterSaver(){ }

	b8 Save(const std::string &filepath, const VX::SG::Group* setting, const VX::SG::Domain* domain);
};

#endif // __ALL_PARAMETER_SAVER__

