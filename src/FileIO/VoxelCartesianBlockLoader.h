/*
 * VX/SG/CellIDVoxelCartesian.h
 *
 * SceneGraphLibrary
 *
 */

#ifndef INCLUDE_VoxelCartesianBlockLoader_H
#define INCLUDE_VoxelCartesianBlockLoader_H

#include "../VX/VX.h"
#include "../VX/Math.h"
#include <string>
#include <vector>
#include "../VX/SG/SceneGraph.h"

namespace VX
{
	namespace SG
	{

		class VoxelCartesianBlockLoader
		{
		public:

			enum RunMode{
				MODE_CELLID,
				MODE_BCFLAG
			};

			VoxelCartesianBlockLoader();

			~VoxelCartesianBlockLoader();

			static VX::SG::VoxelCartesianBlock* Load(	const RunMode& mode, 
														const std::string& filepath,
														const VX::Math::idx3& size,
														const bool& header_isNeedSwap,
														const u32& header_gc,
														std::string& errMsg
															  );

			static void UnLoad(VX::SG::VoxelCartesianBlock* block);


			static bool ReadHeaderInfo( 
														const std::string& filepath,
		                                                VX::Math::idx3& size,
														bool& header_isNeedSwap,
														u32& header_gc);


		private:

		};

	} // namespace SG

} // namespace VX

#endif // INCLUDE_VXSCENEGRAPH_MESH_CARTESIAN_H

