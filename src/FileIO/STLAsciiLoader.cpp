//
//  STLAsciiLoader.cpp
//

#include "../VX/VX.h"
#include "../VX/SG/SceneGraph.h"
#include "../VX/Stream.h"

#include <string>
#include <set>
#include <string.h>

#include "STLAsciiLoader.h"

namespace {
	inline s32 searchReturn(const s8* a)
    {
        s32 i = 0;
        while(a[i] != 0x0A) i++; // LF
        return ++i;
    }
    inline s32 searchSpace(const s8* a)
    {
        s32 i = 0;
        while(a[i] != 0x20 && a[i] != 0x09) ++i; // SPACE, TAB
        return ++i;
    }
	inline s32 skipSpace(const s8* a)
    {
        s32 i = 0;
        while(a[i] == 0x20 || a[i] == 0x09) ++i; // SPACE, TAB
        return i;
    }
	
	inline s32 searchSpaceReturn(const s8* a)
    {
        s32 i = 0;
        while(a[i] != 0x20 && a[i] != 0x09 && a[i] != 0x0A) ++i; // SPACE,TAB  or LF
        return i;
    }
	
	std::string withOutExt(const std::string& fname)
	{
		size_t p = fname.rfind(".");
		if (p != std::string::npos) {
			std::string t = fname;
			t = t.substr(0, p);
			return t;
		}
		return fname;
	}

}
	
STLAsciiLoader::STLAsciiLoader(){
}
STLAsciiLoader::~STLAsciiLoader(){
}
	
	
VX::SG::Node* STLAsciiLoader::Load(const VX::Stream* st)
{	
	using namespace VX::Math;
	using namespace VX::SG;
	
	const s8* data = static_cast<const s8*>(st->GetData());
	s32 byte_size = st->GetSize();

	const char* solidstr    = "solid";
	const char* endsolidstr = "endsolid";
	const char* facetstr    = "facet normal";
	const char* endfacestr  = "endfacet";
	const char* outerstr    = "outer loop";
	const char* endouterstr = "endloop";
	const char* vertexstr   = "vertex";
	
	Geometry::VertexFormat vtx;
	std::vector<Geometry::VertexFormat> vertices;
	size_t i = 0;
	
	while (true)
	{

		i += skipSpace(&data[i]);

		if(i >= byte_size){
			break;
		}

		if (strncmp(&data[i], solidstr, strlen(solidstr)) == 0)
		{
			i += searchReturn(&data[i]);
		}
		else if (strncmp(&data[i], facetstr, strlen(facetstr)) == 0)
		{
			i += strlen(facetstr);
			vec3 normal;
			i += step_fatof(&data[i], &normal.x);
			i += step_fatof(&data[i], &normal.y);
			i += step_fatof(&data[i], &normal.z);
			vtx.normal = normal;
			i += searchReturn(&data[i]);
		}
		else if (strncmp(&data[i], vertexstr, strlen(vertexstr)) == 0)
		{
			i += strlen(vertexstr);
			vec3 pos;
			i += step_fatof(&data[i], &pos.x);
			i += step_fatof(&data[i], &pos.y);
			i += step_fatof(&data[i], &pos.z);
			vtx.pos = pos;
			vtx.col = VX::initColor;
//			vtx.idx = 0;
			vertices.push_back(vtx);
			
			i += searchReturn(&data[i]);
		}
		else if (strncmp(&data[i], outerstr, strlen(outerstr)) == 0)
		{
			i += searchReturn(&data[i]);
		}
		else if (strncmp(&data[i], endouterstr, strlen(endouterstr)) == 0)
		{
			i += searchReturn(&data[i]);
		}		
		else if (strncmp(&data[i], endfacestr, strlen(endfacestr)) == 0)
		{
			i += searchReturn(&data[i]);
		}
		else if (strncmp(&data[i], endsolidstr, strlen(endsolidstr)) == 0)
		{
			//break;

			// there are many solids
			// cotinue
			i += searchReturn(&data[i]);
		}
		else // unknown line
		{
			i += searchReturn(&data[i]);
		}
	}
	
	const u32 vertexCount = static_cast<u32>(vertices.size());
	const u32 indexCount  = vertexCount;
	
	GeometryBVH* g = vxnew GeometryBVH();
	g->SetName(withOutExt(st->GetFileName()));
	g->Alloc(vertexCount, indexCount);
	
	memcpy(g->GetVertex(), &vertices[0], sizeof(Geometry::VertexFormat) * vertexCount); // vertex copy
	vertices.clear();
	std::vector<Geometry::VertexFormat> size0;
	vertices.swap(size0); // free std::vector
	
	Index* index = vxnew Index[indexCount];
	for (u32 i = 0; i < indexCount; i++)
		index[i] = i;
	memcpy(g->GetIndex(), index, sizeof(Index) * indexCount); // index copy
	vxdeleteArray(index);
	
	g->CalcBounds();
	
	return g;
}



