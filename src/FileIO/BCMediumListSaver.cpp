#include "../VX/VX.h"
#include "../VX/SG/SceneGraph.h"

#include "BCMediumListSaver.h"
#include "FileIOUtil.h"

#include <vector>
#include <string>

#include <fstream>
#include <sstream>

b8 WriteMediumList( std::ostringstream& os, const VX::SG::Group* setting)
{
	using namespace std;
	using namespace VX::SG;
	using namespace VX::Math;

	if( !setting ){
		return false;
	}

	vector<const Medium*> meds;

	for(int i = 0; i < setting->GetChildCount(); i++){
		const Node *n = setting->GetChild(i);
		NODETYPE type = n->GetType();

		if(type == NODETYPE_MEDIUM)
		{
			meds.push_back(dynamic_cast<const Medium*>(n));
			continue;
		}
	}

	if(meds.size() != 0 ){
		os << "MediumTable {" << endl;
		for(int i = 0; i < meds.size(); i++){
			string color_str;
			if( !colorCvt_vec42str(meds[i]->GetColor(), color_str) ){ 
				return false; 
			}
			string label = meds[i]->GetLabel();
			string type = meds[i]->GetMediumType();

			os << "  "<< label << " {"  << endl;
			os << "    State               = \"" << type << "\"" << endl;
			os << "    MassDensity         = " << "%%" << endl;
			os << "    SpecificHeat        = " << "%%" << endl;
			os << "    ThermalConductivity = " << "%%" << endl;
			if(type == "Fluid") {
				os << "    KinematicViscosity  = " << "%%" << endl;
				os << "    Viscosity           = " << "%%" << endl;
				os << "    SpeedOFSound        = " << "%%" << endl;
				os << "    VolumeExpansion     = " << "%%" << endl;
			}

/*			old format
			os << "  Medium[@] {"  << endl;
			os << "    Type  = \"" << meds[i]->GetMediumType() << "\"" << endl;
			os << "    Label = \"" << meds[i]->GetLabel() << "\"" << endl;
			os << "    ID    = "   << meds[i]->GetID() << endl;
			os << "    COLOR = \"" << color_str << "\"" << endl;
*/

			os << "  }" << endl;
		}
		os << "}" << endl;// MediumTable
		os << endl;
	}

	return true;
}

b8 WriteOuterBCList( std::ostringstream& os, const VX::SG::Group* setting)
{
	using namespace std;
	using namespace VX::SG;

	if( !setting ){
		return false;
	}

	vector<const OuterBC*> bcs;

	for(int i = 0; i < setting->GetChildCount(); i++){
		const Node *n = setting->GetChild(i);
		NODETYPE type = n->GetType();

		if(type == NODETYPE_OUTERBC)
		{
			bcs.push_back(dynamic_cast<const OuterBC*>(n));
			continue;
		}
	}

	bool status = true;
	if( bcs.size() != 0){
		status = WriteOuterBCList(os, bcs);
	}

	return status;
}

b8 WriteOuterBCList( std::ostringstream& os, const std::vector<const VX::SG::OuterBC*>& bcs)
{
	using namespace std;

	for(int i = 0; i < bcs.size(); i++){
		string color_str;
		string class_str;
		if( !bcs[i]->GetClass() ) { return false; }
		class_str = bcs[i]->GetClass()->GetLabel();
		string node_name = bcs[i]->GetAlias();
		string medium_str = bcs[i]->GetMedium();

		os << "    "<< node_name << " {" << endl;
		os << "      Kind                   = \""   << "Outer" << "\""<< endl;
		os << "      Class                  = \""   << class_str << "\"" << endl;
		os << "      Medium                 = \""   << medium_str << "\"" << endl;
		if(class_str == "Wall") {
		} else if(class_str == "Wall") {
			os << "      Type                   = \""   << "%%" << "\"" << endl;
		} else if(class_str == "Outflow") {
			os << "      PressureType           = \"" << "%%" << "\"" << endl;
			os << "      PrsValue               = "   << "%%" << endl;
		} else if(class_str == "SpecifiedVelocity") {
			os << "      Profile                = \"" << "%%" << "\"" << endl;
			os << "      OrientationVector      = " << "(";
			os << "%%" << ", " << "%%" << ", " << "%%" << ")" << endl;
			os << "      Velocity               = "   << "%%" << endl;
			os << "      Temperature            = "   << "%%" << endl;
			os << "      Amplitude              = "   << "%%" << endl;
			os << "      Frequency              = "   << "%%" << endl;
			os << "      InitialPhase           = "   << "%%" << endl;
			os << "      ConstantBias           = "   << "%%" << endl;
		} else if(class_str == "Symmetric") {
		} else if(class_str == "Periodic") {
			string Mode_str = "%%";
			os << "      Mode                   = \"" << Mode_str << "\"" << endl;
			if(Mode_str == "SimpleCopy") {
			} else if(Mode_str == "Directional") {
				os << "      FlowDirection          = \"" << "%%" << "\"" << endl;
				os << "      PressureDifference     = "   << "%%" << endl;
			} else if(Mode_str == "Driver") {
				os << "      DriverDirection        = \"" << "%%" << "\"" << endl;
			}
		} else if(class_str == "TractionFree") {
		} else if(class_str == "FarField") {
		}

		if( bcs[i]->GetClass()->HasThermalOption() ) {
			string thermalOption_str = bcs[i]->GetClass()->GetThermalOption();
			os << "      ThermalOption          = \"" << thermalOption_str << "\"" << endl;
			if(thermalOption_str == "Adiabatic") {
				os << "      Type                   = \"" << "%%" << "\"" << endl;
			} else if(thermalOption_str == "HeatFlux") {
				os << "      Flux                   = " << "%%" << endl;
			} else if(thermalOption_str == "HeatTransferS") {
				os << "      SurfaceTemperature     = " << "%%" << endl;
				os << "      CoefOfHeatTransfer     = " << "%%" << endl;
			} else if(thermalOption_str == "HeatTransferSN") {
				os << "      SurfaceTemperature     = " << "%%" << endl;
				os << "      VerticalLaminarAlpha   = " << "%%" << endl;
				os << "      VerticalLaminarBeta    = " << "%%" << endl;
				os << "      VerticalTurbulentAlpha = " << "%%" << endl;
				os << "      VerticalTurbulentBeta  = " << "%%" << endl;
				os << "      VerticalRaCritial      = " << "%%" << endl;
				os << "      LowerLaminarAlpha      = " << "%%" << endl;
				os << "      LowerLaminarBeta       = " << "%%" << endl;
				os << "      LowerTurbulentAlpha    = " << "%%" << endl;
				os << "      LowerTurbulentBeta     = " << "%%" << endl;
				os << "      LowerRaCritial         = " << "%%" << endl;
			} else if(thermalOption_str == "HeatTransferSF") {
				os << "      SurfaceTemperature     = " << "%%" << endl;
				os << "      alpha                  = " << "%%" << endl;
				os << "      beta                   = " << "%%" << endl;
				os << "      gamma                  = " << "%%" << endl;
			} else if(thermalOption_str == "IsoThermal") {
				os << "      Temperature            = " << "%%" << endl;
			}
		}
		os << "    }"   << endl;
		os << endl;

		/* old format
		os << "    BasicBCs[@] {" << endl;
		os << "      Alias = \""   << bcs[i]->GetAlias() << "\"" << endl;
		os << "      Class = \""   << class_str << "\"" << endl;
		if( bcs[i]->GetClass()->HasThermalOption() ){
			os << "      ThermalOption = \"" << bcs[i]->GetClass()->GetThermalOption() << "\"" << endl;
		}
		os << "    }" << endl;
		*/
	}
	return true;
}


b8 WriteLocalBCList( std::ostringstream& os, const VX::SG::Group* setting)
{
	using namespace std;
	using namespace VX::SG;

	if( !setting ){
		return false;
	}

	vector<const LocalBC*> bcs;

	for(int i = 0; i < setting->GetChildCount(); i++){
		const Node *n = setting->GetChild(i);
		NODETYPE type = n->GetType();

		if(type == NODETYPE_LOCALBC)
		{
			bcs.push_back(dynamic_cast<const LocalBC*>(n));
			continue;
		}
	}
	bool status = true;
	if( bcs.size() != 0){


		status = WriteLocalBCList(os, bcs);
	}

	return status;
}

b8 WriteLocalBCList( std::ostringstream& os, const std::vector<const VX::SG::LocalBC*>& bcs)
{
	using namespace std;

	for(int i = 0; i < bcs.size(); i++){
		
		string color_str;
		if( !colorCvt_vec42str(bcs[i]->GetColor(), color_str) ){ 
			return false; 
		}
		if( !bcs[i]->GetClass() ) { 
			return false; 
		}
		string class_str	= bcs[i]->GetClass()->GetLabel();
		string medium_str	= bcs[i]->GetMedium();
		string node_name	= bcs[i]->GetAlias();

		os << "    "<< node_name << " {" << endl;
		os << "      Kind                   = \"" << "Inner" << "\"" << endl;
		os << "      Class                  = \"" << class_str << "\"" << endl;
		os << "      Filepath               = \"" << "%%" << "\""<<endl;
		os << "      Medium                 = \"" << medium_str << "\"" << endl;
		if(class_str == "SpecifiedVelocity") {
			os << "      OrientationVector      = " << "(";
			os << "%%" << ", " << "%%" << ", " << "%%" << ")" << endl;
			os << "      Type                   = \"" << "%%" << "\"" << endl;
			os << "      Profile                = \"" << "%%" << "\"" << endl;
			os << "      Velocity               = "   << "%%" << endl;
			os << "      FluidDirection         = "   << "%%" << endl;
			os << "      Temperature            = "   << "%%" << endl;
		} else if(class_str == "Outflow") {
			os << "      PressureType           = \"" << "%%" << "\"" << endl;
			os << "      PrsValue               = "   << "%%" << endl;
		} else if(class_str == "Periodic") {
		} else if(class_str == "Forcing") {
		} else if(class_str == "PressureLoss") {
		} else if(class_str == "Fan") {
		} else if(class_str == "Darcy") {
		} else if(class_str == "CellMonitor") {
		} else if(class_str == "Inactive") {
		} else if(class_str == "Adiabatic") {
		} else if(class_str == "DirectHeatFlux") {
			os << "      Heartflux              = " << "%%" << endl;
		} else if(class_str == "HeatTransferB") {
		} else if(class_str == "HeatTransferN") {
		} else if(class_str == "HeatTransferS") {
			os << "      SurfaceTemperature     = " << "%%" << endl;
			os << "      CoefOfHeatTransfer     = " << "%%" << endl;
		} else if(class_str == "HeatTransferSF") {
			os << "      Temperature            = " << "%%" << endl;
		} else if(class_str == "HeatTransferSN") {
			os << "      SurfaceTemperature     = " << "%%" << endl;
			os << "      RefTempMode            = \"" << class_str << "\"" << endl;
			os << "      VerticalLaminarAlpha   = " << "%%" << endl;
			os << "      VerticalLaminarBeta    = " << "%%" << endl;
			os << "      VerticalTurbulentAlpha = " << "%%" << endl;
			os << "      VerticalTurbulentBeta  = " << "%%" << endl;
			os << "      VerticalRaCritial      = " << "%%" << endl;
			os << "      LowerLaminarAlpha      = " << "%%" << endl;
			os << "      LowerLaminarBeta       = " << "%%" << endl;
			os << "      LowerTurbulentAlpha    = " << "%%" << endl;
			os << "      LowerTurbulentBeta     = " << "%%" << endl;
			os << "      LowerRaCritial         = " << "%%" << endl;
		} else if(class_str == "IsoThermal") {
			os << "      Temperature            = " << "%%" << endl;
		} else if(class_str == "Radiation") {
		} else if(class_str == "HeatSource") {
			os << "      HeatReleaseValue       = " << "%%" << endl;
		} else if(class_str == "SpecifiedTemprature") {
			os << "      Temperature            = " << "%%" << endl;
		} else if(class_str == "Obstacle") {
		}
		os << "    }" << endl;
		os << endl;


		/* old type
		os << "    BC[@] {" << endl;
		os << "      Alias = \"" << bcs[i]->GetAlias() << "\"" << endl;
		os << "      Class = \"" << class_str << "\"" << endl;
		os << "      ID    = "   << bcs[i]->GetID() << endl;
		os << "      COLOR = \"" << color_str << "\"" << endl;
		os << "    }" << endl;
		*/

	}
	
	return true;
}

BCMediumListSaver::BCMediumListSaver()
{

}

BCMediumListSaver::~BCMediumListSaver()
{

}

b8 BCMediumListSaver::Save(const std::string& filepath, const VX::SG::Group* setting)
{
	std::ostringstream os;
	
	if(!WriteMediumList(os, setting)){
		VXLogE("[BC/Medium List Save] Failed to write Medium\n");
		return false;
	}

	os << "BCTable {" << std::endl;
	if(!WriteLocalBCList(os, setting)){
		VXLogE("[BC/Medium List Save] Failed to write LocalBC\n");
		return false;
	}
	os << std::endl;
	if(!WriteOuterBCList(os, setting)){
		VXLogE("[BC/Medium List Save] Failed to Write OuterBC\n");
		return false;
	}
	os << "}" << std::endl << std::endl; // BCTable


	std::ofstream ofs( filepath.c_str() );
	ofs << os.str();

	return true;
}
