/*
 *
 * FileIO/FaceBCLoader.h
 *
 */

#ifndef __OUTER_BOUNDARY_LOADER_H__
#define __OUTER_BOUNDARY_LOADER_H__


namespace VX {
	namespace SG {
		class Node;
		class Group;
		class Domain;
	} // namespace SG
} // namespace VX

class FaceBCLoader
{
public:
	FaceBCLoader();
	~FaceBCLoader();

	b8 Load(const std::string& filepath, const VX::SG::Group* setting, VX::SG::Domain* domain);
};

#endif // __OUTER_BOUNDARY_LOADER__

