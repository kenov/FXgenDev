#include "VX.h"
#include "Math.h"
#include "SG/SceneGraph.h"
#include "SGPickerHelper.h"
#include "MatrixStack.h"
#include "../FileIO/BCMediumIDNumbering.h"
#include <sstream>
#include <string>

namespace VX {
	
SGPickerHelper::SGPickerHelper()
{

}
	
SGPickerHelper::~SGPickerHelper()
{

}

std::string SGPickerHelper::getVoxelPickInformation(const VX::SG::VoxelCartesianG* g , const VX::SG::HitGeometry* hg)
{


	VX::SG::Index  i = hg->m_index;
	u32 idx = ((hg->m_geo->GetIDBuffer()[i/3] >> 8) & 0xFF);
	std::string mediumName = BCMediumIDNumbering::GetInstance()->GetNameByID(idx);

	std::ostringstream stream;
	
	stream << "ID=";
	if(mediumName.size()>0){
		stream << mediumName;
	}else{
		stream << idx;//medium / outer id
	}
	stream << ",";

	// 重心点
	/*
	ピックしたポリゴンの３点のグローバル座標値が取得できてますので
	３点の重心の点を求めて、その重心点の属するローカルとグローバルのインデックスを計算予定です。
	Origin/Regionおよびサブドメインの分割数を見れば可能と思っております。問題無ければ
	「PickしたLocalBC名称・ローカルインデックス・グローバルインデックス・ピックしたポリゴンの重心座標」
	の情報をステータスバーに表示予定です。
	表示例：LocalBCName1, LocalIndex=(5,5,5),GlobalIndex=(100,100,100),GPos=(x,y,z)
	 */

	VX::Math::vec3 coord_pos= (hg->m_v0 + hg->m_v1 + hg->m_v2) / 3;
	VX::Math::vec3 org		= g->GetOrigin();
	VX::Math::vec3 pitch	= g->GetPitch();
	VX::Math::idx3 voxSize	= g->GetNumVoxels();
	VX::Math::idx3 divSize  = g->GetNumDivs();
	//1サブドメインあたりのボクセルのサイズ
	VX::Math::idx3 voxSizeForOneSD ( static_cast<u32>(voxSize.x/divSize.x),
									 static_cast<u32>(voxSize.y/divSize.y),
									 static_cast<u32>(voxSize.z/divSize.z));

	VX::Math::idx3 posIdx_G,posIdx_L;
	for(int i=0;i<3;i++){
		posIdx_G[i] = static_cast<u32>((coord_pos[i] - org[i]) / pitch[i]);
		posIdx_G[i] = static_cast<s32>(posIdx_G[i]) >= voxSize[i] ? voxSize[i]-1 : posIdx_G[i];
		//globalを元にしてlocalを算出する
		// 剰余の余り
		posIdx_L[i] = static_cast<u32>(posIdx_G[i] % voxSizeForOneSD[i]);
		posIdx_L[i] = static_cast<s32>(posIdx_L[i]) >= voxSizeForOneSD[i] ? voxSizeForOneSD[i]-1 : posIdx_L[i];
	}

	stream << "LocalIndex=(" << posIdx_L.x <<"," <<posIdx_L.y << "," << posIdx_L.z << "),";
	stream << "GlobalIndex=(" << posIdx_G.x <<"," <<posIdx_G.y << "," << posIdx_G.z << "),";
	stream << "GPos=(" << coord_pos.x <<"," <<coord_pos.y << "," << coord_pos.z << ")";

	std::string result = stream.str();
	return result;
}

/**
*	@brief pick information of tri
*	@param[in] root node
*	@param[in] hg pick target node
*/
std::string SGPickerHelper::getTriPickInformation(const VX::SG::Node* node , const VX::SG::HitGeometry* hg)
{
	// fuchi
	std::string outPath;
	getPath( node , hg->m_geo ,  "" ,  outPath  );
	if(outPath.size()==0){
		//assert(false);
		//return "";
		VXLogW("no path in getPickInformation\n");
	}

	VX::SG::Index  i = hg->m_index;

	// 
	//VX::SG::Index tri_index = hg->m_index / 3;
	// stl order on file . start is 1.
	//tri_index = tri_index + 1;
	u32 idx = ((hg->m_geo->GetIDBuffer()[i/3] >> 8) & 0xFF);
	std::string mediumName = BCMediumIDNumbering::GetInstance()->GetNameByID(idx);

	// msg format. e.g.   /root/aaa/bbb[triID=777]
	
	std::ostringstream stream;
	
	stream << outPath;
	stream << " [ID=";
	if(mediumName.size()>0){
		stream << mediumName;
	}else{
		stream << idx;//medium / outer id
	}

	stream << "] vertex[(" << hg->m_v0.x <<"," << hg->m_v0.y << "," << hg->m_v0.z << ")"
															<< "(" << hg->m_v1.x <<"," << hg->m_v1.y << "," << hg->m_v1.z << ")"
															<< "(" << hg->m_v2.x <<"," << hg->m_v2.y << "," << hg->m_v2.z << ")]"
															;
	
	std::string result = stream.str();
	return result;

}

void SGPickerHelper::getPath(const VX::SG::Node* self,const VX::SG::Geometry* geo ,  const std::string& dir , std::string& outPath  )
{

	using namespace VX::SG;
	using namespace VX::Math;
	const NODETYPE t = self->GetType();
	std::string selfdir = dir + "/" + self->GetName();
	if(t == NODETYPE_GEOMETRY && self == geo){
		outPath = selfdir;
		return ;
	}
	
	//call children
	if (t == NODETYPE_GROUP  || t == NODETYPE_TRANSFORM) {
	
		VX::SG::Node* s_self = const_cast<VX::SG::Node*>(self);
		Group* grp = static_cast<Group*>(s_self);
		s32 n = grp->GetChildCount();
		for (s32 i = 0; i < n; i++) {
			std::string parent_dir = selfdir;
			getPath( grp->GetChild(i),  geo ,  parent_dir , outPath  );
			if(outPath.size()>0){
				//end
				return ;
			}
		}
	}
	
}

	
} // namespace VX

