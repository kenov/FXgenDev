
#include "VX.h"
#include "Math.h"

#include <vector>

namespace VX {
class Graphics;
class ProgramObject;
	
class RenderDevice
{
public:
	
	class CommandArg
	{
	public:
		CommandArg(){}
		std::string m_name;
		//union {
			VX::Math::vec4   m_vec;
			VX::Math::matrix m_mat;
			struct {
				u32 m_vb;
				u32 m_ib;
				u32 m_indexCount;
				u32 m_texname;
			} triarg;
			const VX::Math::vec4* m_vec4array;
			s32 m_vecnum;
		//};
		void TriangleLineArg(u32 vb, u32 ib, u32 indexcount, u32 texname = 0)
		{
			triarg.m_vb = vb;
			triarg.m_ib = ib;
			triarg.m_indexCount = indexcount;
			triarg.m_texname = texname;
		}
		void FloatArg(const s8* name, const float f)
		{
			m_name  = name;
			m_vec.x = f;
		}
		void IntArg(const s8* name, const int i)
		{
			m_name = name;
			m_vec.x = static_cast<float>(i);
		}
		void Vec4Arg(const s8* name, const VX::Math::vec4& v)
		{
			m_name = name;
			m_vec  = v;
		}
		void Matrix4Arg(const s8* name, const VX::Math::matrix& m)
		{
			m_name = name;
			m_mat  = m;
		}
		void Vec4ArrayArg(const s8* name, const VX::Math::vec4* v, s32 vecnum)
		{
			m_name = name;
			m_vec4array = v;
			m_vecnum = vecnum;
		}

	};

	
	class RenderCommand
	{
	public:
		enum TYPE_COMMAND
		{
			CMD_RENDER_TRIANGLE = 1,
			CMD_RENDER_TRIANGLE_LINE,
			CMD_RENDER_TRIANGLE_TEXTURE,
			CMD_RENDER_LINES,
			CMD_RENDER_POINTS,
			CMD_MATRIX,
			CMD_FLOAT,
			CMD_INT,
			CMD_VEC4,
			CMD_VEC4ARRAY,
		};
		RenderCommand(TYPE_COMMAND cmd_, CommandArg* arg_, ProgramObject* prg_, u32 cid_)
		{
			cmd = cmd_;
			arg = arg_;
			prg = prg_;
			cid = cid_;
		}
		
		b8 operator<(const RenderCommand& t) const
		{
			return prg < t.prg || (prg <= t.prg && cid < t.cid);
		}
		TYPE_COMMAND cmd;
		CommandArg*  arg;
		ProgramObject* prg;
		u32 cid;
	};
	
	
	RenderDevice(VX::Graphics* vg);
	~RenderDevice();
	
	// ----- Create Commands ------
	u32  WriteBuffer(const void* vertex, u32 vertex_num, const void* index, u32 index_num, const u32* idBuffer, u32 bufferID = 0);
	u32  WriteTexture(const void* texImage, const VX::Math::idx2& size, u32 bufferID = 0);
	void DeleteBuffer(u32 bufferId);
	void DeleteTexture(u32 bufferId);
	
	// ----- Queue commands ---------
	void QRenderTriangle(u32 bufferId);
	void QRenderTriangle(u32 bufferID, u32 textureId);
	void QRenderTriangleLine(u32 bufferId);
	void QRenderLines(u32 bufferId);
	void QRenderPoints(u32 bufferId);
	void FlashCommands();
	
	//void SetViewMat(const VX::Math::matrix4x4& mat);
	//void SetProjMat(const VX::Math::matrix4x4& mat);
	
	void QMatrix(const s8* name, const VX::Math::matrix4x4& mat);
	void QFloat(const s8* name, const float f);
	void QInt(const s8* name, const int i);
	void QVec4(const s8* name, const VX::Math::vec4& v);
	void QVec4Array(const s8* name, const VX::Math::vec4* v, s32 vecnum);
	void QBindShader  (VX::ProgramObject* prg);
	void QUnbindShader();
	
private:
		
	static const u32 batchVertexSize = 65000;
	static const u32 MAX_COMMANDARG_SIZE = 30000;
	
	VX::Graphics* g;
	ProgramObject* m_bindingShader;
//	ProgramObject* m_triprg;
//	ProgramObject* m_lineprg;
	
//	VX::Math::matrix4x4 m_proj;
//	VX::Math::matrix4x4 m_view;

	class VBIB
	{
	public:
		VBIB()
		{
			vbib[0] = 0;
			vbib[1] = 0;
			indexCount = 0;
		}
		VBIB(u32 vb, u32 ib, u32 indexcount)
		{
			vbib[0] = vb;
			vbib[1] = ib;
			indexCount = indexcount;
		}
		u32 vbib[2];
		u32 indexCount;
	};
	std::map<s32, std::vector<VBIB> > m_buffers;
	u32 m_bufferID; // !!! Limitation : max recreate buffer is u32 size !!!

	class TEXID
	{
	public:
		TEXID() : texname(0), size(VX::Math::idx2(0, 0)) { }
		TEXID(const u32 texname_, const VX::Math::idx2& size_) : texname(texname_), size(size_){ }
		u32 texname;
		VX::Math::idx2 size;
	};
	std::map<u32, TEXID> m_textures;

	std::vector<RenderCommand> m_cmd;
	u32 m_cmdcounter;
	std::vector<CommandArg> m_cmdarg;
	u32 m_cmdargCnt;
	CommandArg* GetCommandArg();
	
	VX::SG::Geometry::VertexFormat* m_tempVB;
	VX::SG::Index* m_tempIB;
};

} // namespace VX

