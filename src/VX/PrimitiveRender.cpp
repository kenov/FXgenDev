#include "../VX/VX.h"
#include "../VX/Graphics.h"
#include "../VX/Math.h"
#include "ShaderProgramObject.h"

#include "PrimitiveRender.h"

#include <assert.h>

namespace  {
	
	static const char* cvVShader = SHADER_INLINE(
		uniform mat4 proj;
		uniform mat4 view;
	    attribute vec3 position;
		attribute vec4 color;
		varying vec4 col;
		void main(void)
		{
			col = color;
			gl_Position = proj * view * vec4(position,1);
		}
	);
	
	static const char* cvFShader = SHADER_INLINE(
		varying vec4 col;
		void main(void)
		{
			if (col.a == 0.0)
				discard;
			gl_FragColor = col;
		}
	);
	
	static const char* cnvVShader = SHADER_INLINE(
		uniform mat4 proj;
		uniform mat4 view;
		attribute vec3 position;
		attribute vec4 color;
		attribute vec3 normal;										 
		varying vec4 col;
		varying vec3 nor;
		void main(void)
		{
			nor = (proj * view * vec4(normal,0)).xyz;
			col = color;
			gl_Position = proj * view * vec4(position,1.0);
		}
	);
	
	static const char* cnvFShader = SHADER_INLINE(
		varying vec4 col;
		varying vec3 nor;
		void main(void)
		{
			if (col.a == 0.0)
				discard;
			
			const vec3 L = vec3(0.0,0.0,-1.0);
			float sh = (1.0 - dot(normalize(nor),L)) * 3.141592;//-PI - PI
			gl_FragColor.rgb = col.rgb * (cos(sh) * 0.5 + 0.5);
			gl_FragColor.a   = col.a;
		}
	);

	static const char* uvvVShader = SHADER_INLINE(
		attribute vec3 position;
		attribute vec4 color;
		attribute vec2 uv;
		uniform mat4 proj;
		uniform mat4 view;
		varying vec4 col;
		varying vec2 vtx_uv;
		void main(void)
		{
			col = color;
			vtx_uv = uv;
			gl_Position = proj * view * vec4(position,1);
		}
	);
	
	static const char* uvvFShader = SHADER_INLINE(
		uniform sampler2D texture;
		varying vec4 col;
		varying vec2 vtx_uv;
		void main(void)
		{
			gl_FragColor = col * texture2D(texture, vtx_uv);
		}
	);
} // namespace

namespace VX {
using namespace Math;

PrimitiveRender::PrimitiveRender(Graphics* mg)
{
	g = mg;
	m_mode = MODE_UNKNOWN;
	m_col = 0xFFFFFFFF;
	m_uv  = vec2(0.0f,0.0f);
	m_tex = 0;
	m_needupdate = false;
	
	m_projMat = Identity();
	m_viewMat = Identity();
	
	ShaderObject cvVS(g),cvFS(g), cnvVS(g),cnvFS(g), uvvVS(g), uvvFS(g);
	if (!cvVS.LoadFromMemory(cvVShader, ShaderObject::VERTEX_SHADER)){
		VXLogE("Shader compile failed.[%s:%d]", __FILE__, __LINE__);
	}
	if (!cvFS.LoadFromMemory(cvFShader, ShaderObject::FRAGMENT_SHADER)){
		VXLogE("Shader compile failed.[%s:%d]", __FILE__, __LINE__);
	}
	if (!cnvVS.LoadFromMemory(cnvVShader, ShaderObject::VERTEX_SHADER)){
		VXLogE("Shader compile failed.[%s:%d]", __FILE__, __LINE__);
	}
	if (!cnvFS.LoadFromMemory(cnvFShader, ShaderObject::FRAGMENT_SHADER)){
		VXLogE("Shader compile failed.[%s:%d]", __FILE__, __LINE__);
	}
	if (!uvvVS.LoadFromMemory(uvvVShader, ShaderObject::VERTEX_SHADER)){
		VXLogE("Shader compile failed.[%s:%d]", __FILE__, __LINE__);
	}
	if (!uvvFS.LoadFromMemory(uvvFShader, ShaderObject::FRAGMENT_SHADER)){
		VXLogE("Shader compile failed.[%s:%d]", __FILE__, __LINE__);
	}
	
	m_prg    = vxnew ProgramObject(g);
	m_prgnor = vxnew ProgramObject(g);
	m_prgtex = vxnew ProgramObject(g);
	
	if (!m_prg->Link(cvVS, cvFS)){
		vxdelete(m_prg);
		m_prg = 0;
	}
	if (!m_prgnor->Link(cnvVS, cnvFS)){
		vxdelete(m_prgnor);
		m_prgnor = 0;
	}
	if (!m_prgtex->Link(uvvVS, uvvFS)){
		vxdelete(m_prgtex);
		m_prgtex = 0;
	}

	g->GenBuffers(3, m_vb);
}

PrimitiveRender::~PrimitiveRender()
{
	if (m_prg)
		vxdelete(m_prg);
	if (m_prgnor)
		vxdelete(m_prgnor);
	if (m_prgtex)
		vxdelete(m_prgtex);
	
}
	
void PrimitiveRender::Clear()
{
	m_needupdate = true;
	m_tex = 0;
	m_points.clear();
	m_lines[0].clear();
	m_lines[1].clear();
	m_tris[0].clear();
	m_tris[1].clear();
	m_tris_uv[0].clear();
	m_tris_uv[1].clear();
}

void PrimitiveRender::SetTexture(TextureObject* tex)
{
	m_tex = tex;
}

void PrimitiveRender::Begin(PrimitiveRender::MODE mode)
{
	m_mode = mode | MODE_FIRST_FLAG;
}

void PrimitiveRender::End()
{
	m_mode = MODE_UNKNOWN;
}

void PrimitiveRender::Vertex(const vec3& v)
{
	m_needupdate = true;
	const b8 firstflag = (m_mode & MODE_FIRST_FLAG ? true : false);
	m_mode &= ~MODE_FIRST_FLAG;
	
	if (m_mode < MODE_TRIANGLES)
	{
		std::vector<CVertex>* vtype[] = {0, &m_points, &m_lines[0], &m_lines[1]};
		if (firstflag && vtype[m_mode]->size() > 0) {
			if (m_mode == MODE_TRIANGLE_STRIP) {
				// add triangle contraction
				vtype[m_mode]->push_back((*vtype[m_mode])[vtype[m_mode]->size() - 1]);
				vtype[m_mode]->push_back(CVertex(v, m_col));
			}
			if (m_mode == MODE_LINE_STRIP) {
				CVertex nv((*vtype[m_mode])[vtype[m_mode]->size() - 1]);
				nv.col = 0;
				vtype[m_mode]->push_back(nv);
				nv.pos = v;
				vtype[m_mode]->push_back(nv);
			}
		}
			
		if (vtype[m_mode])
			vtype[m_mode]->push_back(CVertex(v, m_col));
	}
	else
	{
		std::vector<CNVertex>* vtype[] = {0, 0, 0, 0,  &m_tris[0], &m_tris[1]};
		if (firstflag && vtype[m_mode]->size() > 0) {
			if (m_mode == MODE_TRIANGLE_STRIP) {
				// add triangle contraction
				vtype[m_mode]->push_back((*vtype[m_mode])[vtype[m_mode]->size() - 1]);
				vtype[m_mode]->push_back(CNVertex(v, m_col, m_nor));
			}
		}
		
		if (vtype[m_mode])
			vtype[m_mode]->push_back(CNVertex(v, m_col, m_nor));		
	}
}

void PrimitiveRender::Color(const vec4& col)
{
	m_col =
		((0xFF & static_cast<u32>(0xFF * col.a)) << 24) |
		((0xFF & static_cast<u32>(0xFF * col.b)) << 16) |  
		((0xFF & static_cast<u32>(0xFF * col.g)) << 8 ) |  
		((0xFF & static_cast<u32>(0xFF * col.r))      );  
}

void PrimitiveRender::Normal(const Math::vec3 &n)
{
	m_nor = n;
}
	
void PrimitiveRender::Texcoord(const vec2& uv)
{
	m_uv = uv;
}

void PrimitiveRender::Rect(const vec3& start, const vec3& end)
{
	Begin(MODE_TRIANGLE_STRIP);
	Vertex(vec3(start.x,start.y, start.z));
	Vertex(vec3(end.x  ,start.y, start.z));
	Vertex(vec3(start.x  ,end.y, start.z));
	Vertex(vec3(end.x  ,end.y  , start.z));
	End();	
}

void PrimitiveRender::DrawAll()
{
	const u32 pn = static_cast<u32>(m_points.size());
	const u32 tn = static_cast<u32>(m_tris[0].size());
	const u32 ln = static_cast<u32>(m_lines[0].size());
	const u32 ts = static_cast<u32>(m_tris[1].size());
	const u32 ls = static_cast<u32>(m_lines[1].size());
	const u32 numTriArray[]       = {tn, ts};
	const u32 numArray[]          = {ln, ls, pn};
	const VGenum primTriType[]    = {VG_TRIANGLES, VG_TRIANGLE_STRIP};
	const VGenum primType[]       = {VG_LINES, VG_LINE_STRIP, VG_POINTS };
	
	const u32 allcount = pn + ln + ls;
	const u32 alltricount = tn + ts;
//	assert(allcount < 32768);// MAX limitation
//	assert(alltricount < 32768);// MAX limitation
	
	if (!tn && !pn && !ln && !ts && !ls)
		return;
	
	if (tn || ts)
	{
		// ---- Tris ---------
		const CNVertex* vtxTriArray[] = {tn ? &m_tris[0][0] : 0, ts ? &m_tris[1][0] : 0};
		m_prgnor->Bind();
		m_prgnor->SetUniformMatrix4x4("view", 1, VG_FALSE, &m_viewMat.f[0]);
		m_prgnor->SetUniformMatrix4x4("proj", 1, VG_FALSE, &m_projMat.f[0]);
		
		const s32 atn = static_cast<s32>(sizeof(numTriArray)/sizeof(u32));
		g->BindBuffer(VG_ARRAY_BUFFER, m_vb[1]);
		if (m_needupdate) {
			g->BufferData(VG_ARRAY_BUFFER, alltricount * sizeof(CNVertex), 0, VG_DYNAMIC_DRAW);
			
			u32 noffset = 0;
			for (s32 i = 0; i < atn; i++) {
				const u32 vn = numTriArray[i];
				if (vn) {
					const u32 vsize = vn * sizeof(CNVertex);
					g->BufferSubData(VG_ARRAY_BUFFER, noffset, vsize, vtxTriArray[i]);
					noffset += vsize;
				}
			}
		}
		
		s32 pos_att = m_prgnor->GetAttribLocation("position");
		s32 col_att = m_prgnor->GetAttribLocation("color");
		s32 nor_att = m_prgnor->GetAttribLocation("normal");
		g->EnableVertexAttribArray(pos_att);
		g->VertexAttribPointer(pos_att, 3, VG_FLOAT, VG_FALSE, sizeof(CNVertex), 0);
		g->EnableVertexAttribArray(col_att);
		g->VertexAttribPointer(col_att, 4, VG_UNSIGNED_BYTE, VG_TRUE, sizeof(CNVertex), (const void*)(sizeof(f32)*3));
		g->EnableVertexAttribArray(nor_att);
		g->VertexAttribPointer(nor_att, 3, VG_FLOAT, VG_FALSE, sizeof(CNVertex), (const void*)(sizeof(f32)*4));
		
		u32 noffset_v = 0;
		for (s32 i = 0; i < atn; i++) {
			const u32 vn = numTriArray[i];
			if (vn)	{
				//TODO:fuchi
				g->DrawArrays(primTriType[i], noffset_v, vn);
				noffset_v += vn;
			}
		}
		m_prgnor->Unbind();
		g->DisableVertexAttribArray(nor_att);
		g->BindBuffer(VG_ARRAY_BUFFER, 0);
	}
	
	if (pn || ln || ls)
	{
		// Lines / Points
		const CVertex* vtxArray[]     = {ln ? &m_lines[0][0] : 0, ls ? &m_lines[1][0] : 0, pn ? &m_points[0] : 0};
		m_prg->Bind();
		m_prg->SetUniformMatrix4x4("view", 1, VG_FALSE, &m_viewMat.f[0]);
		m_prg->SetUniformMatrix4x4("proj", 1, VG_FALSE, &m_projMat.f[0]);
		
		const s32 an = static_cast<s32>(sizeof(numArray)/sizeof(u32));
		g->BindBuffer(VG_ARRAY_BUFFER, m_vb[0]);
		if (m_needupdate) {
			g->BufferData(VG_ARRAY_BUFFER, allcount * sizeof(CVertex), 0, VG_DYNAMIC_DRAW);
			u32 offset = 0;
			for (s32 i = 0; i < an; i++) {
				const u32 vn = numArray[i];
				if (vn) {
					const u32 vsize = vn * sizeof(CVertex);
					g->BufferSubData(VG_ARRAY_BUFFER, offset, vsize, vtxArray[i]);
					offset += vsize;
				}
			}
		}
		//todo : fuchi this is rendering place
		s32 pos_att = m_prg->GetAttribLocation("position");
		s32 col_att = m_prg->GetAttribLocation("color");
		g->EnableVertexAttribArray(pos_att);
		g->VertexAttribPointer(pos_att, 3, VG_FLOAT, VG_FALSE, sizeof(CVertex), 0);
		g->EnableVertexAttribArray(col_att);
		g->VertexAttribPointer(col_att, 4, VG_UNSIGNED_BYTE, VG_TRUE, sizeof(CVertex), (const void*)(sizeof(f32)*3));
		
		u32 offset_v = 0;
		for (s32 i = 0; i < an; i++) {
			const u32 vn = numArray[i];
			if (vn)	{
				g->DrawArrays(primType[i], offset_v, vn);
				offset_v += vn;
			}
		}
		m_prg->Unbind();
		g->BindBuffer(VG_ARRAY_BUFFER, 0);
	}
		m_needupdate = false;
}


} // namespace
