//
//  Dir.h
//

#ifndef INCLUDE_VX_DIR_H
#define INCLUDE_VX_DIR_H

#include "Type.h"
#include <vector>
#include <string>

namespace VX {
class Dir
{
public:
	Dir(const s8* dirname);

	u32 GetFileCount() const;
	u32 GetDirCount() const;
	const std::string& GetFile(u32 i) const;
	const std::string& GetDir(u32 i) const;
	std::string GetFilePath(const s8* filename) const;
	b8 IsOpened() const;
	const std::string& GetPath() const;
	const std::string& GetName() const;
	b8 CreateDir(const s8* dirname);
	
private:
	std::string m_dirname;
	std::string m_dirpath;
	b8 m_opened;
	std::vector<std::string> m_files;
	std::vector<std::string> m_dirs;
};
	
} // namespace VX

#endif // INCLUDE_VX_DIR_H
