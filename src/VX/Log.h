/*
 *  VX.h
 *
 * Basic VX lib header
 * 
 */

#ifndef INCLUDE_VX_LOG_H
#define INCLUDE_VX_LOG_H



void VXLogE  (const char *format, ...);
void VXLogW  (const char *format, ...);
void VXLogI  (const char *format, ...);
void VXLogD  (const char *format, ...);
void VXPrint (const char *format, ...);

#endif // INCLUDE_VX_LOG_H
