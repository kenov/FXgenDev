/*
 *
 * SGRender.h
 * 
 */


#ifndef __VX_SG_RENDER_H__
#define __VX_SG_RENDER_H__

#include "Type.h"


namespace VX {
	class Graphics;
	class RenderDevice;
	class MatrixStack;
	class ProgramObject;
	namespace SG{
		class Node;
	}

class SGRender
{
public:
	SGRender(VX::Graphics* vg);
	~SGRender();
	
	void Draw(const VX::SG::Node* node);
	void RenderAll();
	
	void SetWorldViewMat(const VX::Math::matrix4x4& mat);
	void SetProjMat(const VX::Math::matrix4x4& mat);

	void ClearCache();
	//void SetSceneGraph(const VX::SG::Node* node);
	
	void ShowWire(b8 enable);
	b8   IsShowWire() const;

	void ShowDomainWire(b8 enable);
	b8   IsShowDomainWire() const;
	
	void ShowMeshGrid(b8 enable);
	b8   IsShowMeshGrid() const;

	void ShowPolygon(b8 enable);
	b8   IsShowPolygon() const;
	
	void SetIDColorTable(int i, const VX::Math::vec4& col);
	VX::Math::vec4 GetIDColorTable(int i);
private:
	void drawNode(const VX::SG::Node* node);
	void drawGeometry(const VX::SG::Geometry* geo);
	void drawBBox(const VX::SG::BBox* bbox);
	void drawDomain(const VX::SG::DomainCartesian* bbox);
	void drawVoxelCartesianSliceG(const VX::SG::VoxelCartesianSliceG* slice);
	void drawVoxelCartesianSliceL(const VX::SG::VoxelCartesianSliceL* slice);
	void drawVoxelCartesianSliceC(const VX::SG::VoxelCartesianSliceC* slice);
	void drawVoxelCartesianBCflag(const VX::SG::VoxelCartesianBCflag* flg);
	
	void drawVoxelCartesianG(const VX::SG::VoxelCartesianG* node);
	void drawVoxelCartesianL(const VX::SG::VoxelCartesianL* node);
	void drawVoxelCartesianC(const VX::SG::VoxelCartesianC* node);

	void drawMeshCartesianSlice(const VX::SG::MeshCartesianSlice* slice);
	void drawMeshBCMSlice(const VX::SG::MeshBCMSlice* slice);
	void drawDistanceFieldSlice(const VX::SG::DistanceFieldSlice* slice);

	//u32 m_screenWidth;
	//u32 m_screenHeight;
	VX::Math::matrix m_view,m_proj;
	VX::ProgramObject* m_triprg;
	VX::ProgramObject* m_textriprg;
	VX::ProgramObject* m_lineprg;
	VX::ProgramObject* m_transtriprg;
	VX::ProgramObject* m_colormapprg;
	
	VX::Graphics* g;
	
	const VX::SG::Node* m_sg;
	
	MatrixStack* m_stack;
	
	class DrawQueue;
	DrawQueue* m_que;
	
	RenderDevice* m_render;
	
	SG::Material* m_defaultMaterial;
	
	b8 m_enableWire;
	b8 m_enableDomainWire;
	b8 m_enablePolygon;
	b8 m_enableMeshGrid;

	static const s32 MAX_IDCOLOR_NUM = 32;
	VX::Math::vec4 m_colors[MAX_IDCOLOR_NUM];
};
	
} // namespace VX

#endif // __VX_SG_RENDER_H__

