#include "VX.h"
#include "Math.h"
#include "SG/SceneGraph.h"
#include "SGPicker.h"
#include "MatrixStack.h"


#include "../FileIO/BCMediumIDNumbering.h"
#include <sstream>
#include <string>

namespace VX {
	
SGPicker::SGPicker()
{
	m_proj = Math::Identity();
	m_view = Math::Identity();
	m_width  = 0;
	m_height = 0;
	m_stack = vxnew MatrixStack();
}
	
SGPicker::~SGPicker()
{
	vxdelete( m_stack );
}

void SGPicker::SetViewMat(const VX::Math::matrix4x4& mat)
{
	m_view = mat;
}
void SGPicker::SetProjMat(const VX::Math::matrix4x4& mat)
{
	m_proj = mat;
}
	
void SGPicker::Resize(const u32 width, const u32 height)
{
	m_width  = width;
	m_height = height;
}

b8 SGPicker::EndPick(VX::SG::Node* node, f32 win_x, f32 win_y, TYPE_TRAVERSAL type)
{
	using namespace Math;
	const vec3 nwinpos(win_x, m_height - win_y, 0.0f);
	const vec3 fwinpos(win_x, m_height - win_y, 1.0f);
	const s32 viewport[] = {0, 0, static_cast<s32>(m_width), static_cast<s32>(m_height)};
	vec3 org, tar;
	UnProject(nwinpos, m_view, m_proj, viewport, &org);
	UnProject(fwinpos, m_view, m_proj, viewport, &tar);
	
	const vec3 dir = normalize(tar - org);
	vec3 orgdir[] = {org, dir};

	endPick(node, orgdir, type);
	return true;
}

const VX::SGPickerElm& SGPicker::GetPickInformation() const
{
	return m_pickElm;
}

b8 SGPicker::Pick(VX::SG::Node* node, f32 win_x, f32 win_y, b8 selection, TYPE_TRAVERSAL type )
{
	
	m_stack->Clear();
	
	using namespace Math;
	const vec3 nwinpos(win_x, m_height - win_y, 0.0f);
	const vec3 fwinpos(win_x, m_height - win_y, 1.0f);
	const s32 viewport[] = {0, 0, static_cast<s32>(m_width), static_cast<s32>(m_height)};
	vec3 org, tar;
	UnProject(nwinpos, m_view, m_proj, viewport, &org);
	UnProject(fwinpos, m_view, m_proj, viewport, &tar);
	
	const vec3 dir = normalize(tar - org);
	//printf("org=(%f,%f,%f) tar=(%f,%f,%f) dir=(%f,%f,%f)\n",org.x, org.y, org.z, tar.x, tar.y, tar.z, dir.x, dir.y, dir.z);
	vec3 orgdir[] = {org, dir};
	
#if 0
	return pick(node, orgdir);
#else

	m_pickElm.Clear();

	std::vector<VX::SG::HitGeometry>		hits;
	std::vector<VX::SG::HitDomain>			hdoms;
	std::vector<VX::SG::HitVoxelCartesianL> hlocals;
	b8 h = pick(node, orgdir, hits, hdoms, hlocals, type);
	if (h) {
		{
			std::vector<VX::SG::HitGeometry>::iterator hg = std::min_element(hits.begin(), hits.end());
			if (hg != hits.end()) {
				const VX::SG::Index min_idx = hg->m_index;
				u32* idb = hg->m_geo->GetIDBuffer();
				const u32 minidxid = min_idx/3;
				if (selection) {
					if (!(idb[minidxid] & 0x000000FF)){
						idb[minidxid] |= 0x000000FF;
						hg->m_geo->EnableNeedUpdate();
						hg->m_geo->SetSelection(true, true);
					}
				} else {
					if (idb[minidxid] & 0x000000FF) {
						idb[minidxid] &= 0xFFFFFF00;
						hg->m_geo->EnableNeedUpdate();
						hg->m_geo->CheckSelection();
					}
				}

				// get tri pick information
				m_pickElm.SetGeo(*hg);

			}
		}
		{
			std::vector<VX::SG::HitDomain>::iterator hd = std::min_element(hdoms.begin(), hdoms.end());
			if (hd != hdoms.end()) {
				const u32 idx = hd->m_index;
				if (hd->m_dom->GetType() == VX::SG::NODETYPE_BBOX_DOMAIN_CARTESIAN) {
					VX::SG::DomainCartesian* dc = static_cast<VX::SG::DomainCartesian*>(hd->m_dom);
					if (selection) {
						dc->SelectSubdomain(idx, true);
					} else {
						dc->SelectSubdomain(idx, false);
					}
				}
			}
		}
		{
			std::vector<VX::SG::HitVoxelCartesianL>::iterator hl = std::min_element(hlocals.begin(), hlocals.end());
			if (hl != hlocals.end()) {
				const u32 idx = hl->m_index;
				if (hl->m_dom->GetType() == VX::SG::NODETYPE_VOXEL_CARTESIAN_L) {
					VX::SG::VoxelCartesianL* dc = static_cast<VX::SG::VoxelCartesianL*>(hl->m_dom);
					if (selection) {
						dc->SelectSubdomain(idx, true);
					} else {
						dc->SelectSubdomain(idx, false);
					}
				}

				// get pick information
				m_pickElm.SetLocal(*hl);
			}
		}
	}
	return h;
#endif
	
}
	
b8 SGPicker::Pick(VX::SG::Node* node, f32 win_sx, f32 win_sy, f32 win_ex, f32 win_ey, b8 selection, TYPE_TRAVERSAL type)
{
	m_stack->Clear();
	
	using namespace Math;
	const s32 viewport[] = {0, 0, static_cast<s32>(m_width), static_cast<s32>(m_height)};
	const f32 sx = win_sx < win_ex ? win_sx : win_ex;
	const f32 ex = win_sx > win_ex ? win_sx : win_ex;
	const f32 sy = win_sy > win_ey ? win_sy : win_ey;
	const f32 ey = win_sy < win_ey ? win_sy : win_ey;
	const vec3 snwinpos(sx, m_height - sy, -1.0f);
	const vec3 sfwinpos(sx, m_height - sy,  1.0f);
	const vec3 enwinpos(ex, m_height - ey, -1.0f);
	const vec3 efwinpos(ex, m_height - ey,  1.0f);

	vec3 from0, from1, to0, to1;
	matrix finalMatrix = m_proj;
	finalMatrix = Inverse(finalMatrix);
	UnProject(snwinpos, finalMatrix, viewport, &from0);
	UnProject(sfwinpos, finalMatrix, viewport, &to0);
	UnProject(enwinpos, finalMatrix, viewport, &from1);
	UnProject(efwinpos, finalMatrix, viewport, &to1);
	
	vec3 nr[4],fr[4];
	nr[0] = vec3(from0.x, from0.y, from0.z);
	nr[1] = vec3(from1.x, from0.y, from0.z);
	nr[2] = vec3(from1.x, from1.y, from0.z);
	nr[3] = vec3(from0.x, from1.y, from0.z);
	fr[0] = vec3(to0.x, to0.y, to0.z);
	fr[1] = vec3(to1.x, to0.y, to0.z);
	fr[2] = vec3(to1.x, to1.y, to0.z);
	fr[3] = vec3(to0.x, to1.y, to0.z);
	
	vec4 plane_normals[4];//positive dir is outer
	if (m_proj._44 > 0.0f){ // Ortho
		plane_normals[0] = vec4(cross( normalize(fr[0]-nr[0]), normalize(nr[1] - nr[0]) ), to0.y);
		plane_normals[1] = vec4(cross( normalize(fr[1]-nr[1]), normalize(nr[2] - nr[1]) ),-to1.x);
		plane_normals[2] = vec4(cross( normalize(fr[2]-nr[2]), normalize(nr[3] - nr[2]) ),-to1.y);
		plane_normals[3] = vec4(cross( normalize(fr[3]-nr[3]), normalize(nr[0] - nr[3]) ), to0.x);
	} else { // Perspective
		plane_normals[0] = vec4(cross( normalize(fr[0]), normalize(nr[1] - nr[0]) ),0.0f);
		plane_normals[1] = vec4(cross( normalize(fr[1]), normalize(nr[2] - nr[1]) ),0.0f);
		plane_normals[2] = vec4(cross( normalize(fr[2]), normalize(nr[3] - nr[2]) ),0.0f);
		plane_normals[3] = vec4(cross( normalize(fr[3]), normalize(nr[0] - nr[3]) ),0.0f);
	}
	/*printf("From0(%f,%f,%f), From1(%f,%f,%f)\n",
		   from0.x, from0.y, from0.z,
		   from1.x, from1.y, from1.z);
	printf("To0(%f,%f,%f), To1(%f,%f,%f)\n",
		   to0.x, to0.y, to0.z,
		   to1.x, to1.y, to1.z);

	printf("N0(%f,%f,%f,%f) N1(%f,%f,%f,%f) N2(%f,%f,%f,%f) N3(%f,%f,%f,%f)\n",
		   plane_normals[0].x,plane_normals[0].y,plane_normals[0].z,plane_normals[0].w,
		   plane_normals[1].x,plane_normals[1].y,plane_normals[1].z,plane_normals[1].w,
		   plane_normals[2].x,plane_normals[2].y,plane_normals[2].z,plane_normals[2].w,
		   plane_normals[3].x,plane_normals[3].y,plane_normals[3].z,plane_normals[3].w	);*/
	return pick(node, plane_normals, selection, type);

}

b8 SGPicker::pick(VX::SG::Node* node, const Math::vec4* frustum, b8 selection, TYPE_TRAVERSAL type)
{
	if (!node || !node->GetVisible())
		return false;
	
	b8 hit = false;
	using namespace VX::SG;
	using namespace VX::Math;
	const NODETYPE t = node->GetType();
	if (t == NODETYPE_GROUP) {
		Group* grp = static_cast<Group*>(node);
		s32 n = grp->GetChildCount();
		for (s32 i = 0; i < n; i++) {
			hit |= pick(grp->GetChild(i), frustum, selection, type);
		}
	} else if (t == NODETYPE_GEOMETRY && (type & TYPE_GEOMETRY)) {
		GeometryBVH* geo = static_cast<GeometryBVH*>(node);
		hit |= geo->IntersectFrustum(frustum, m_view * m_stack->GetMatrix(), selection);
	} else if (t == NODETYPE_BBOX) {
		BBox* dom = static_cast<BBox*>(node);
		hit |= dom->IntersectFrustum(frustum, m_view * m_stack->GetMatrix(), selection);
	} else if (t == NODETYPE_BBOX_DOMAIN_CARTESIAN && (type & TYPE_DOMAIN)) {
		DomainCartesian* dom = static_cast<DomainCartesian*>(node);
		hit |= dom->IntersectFrustum(frustum, m_view * m_stack->GetMatrix(), selection);

	} else if (t == NODETYPE_VOXEL_CARTESIAN_G) {
		const VoxelCartesianG* g = static_cast<VoxelCartesianG*>(node);
		const std::vector<NodeRefPtr<VoxelCartesianL> >& locals = g->GetLocals();
		for (s32 i = 0; i < locals.size(); i++) {
			hit |= pick(locals.at(i), frustum, selection, type);
		}
	} else if (t == NODETYPE_VOXEL_CARTESIAN_L && (type & TYPE_DOMAIN)) {

		//pick for subdomain
		VoxelCartesianL* dom = static_cast<VoxelCartesianL*>(node);
		assert(dom!=NULL);
		hit |= dom->IntersectFrustum(frustum, m_view * m_stack->GetMatrix(), selection);

	} else if (t == NODETYPE_VOXEL_CARTESIAN_L && (type & TYPE_GEOMETRY)) {

		VoxelCartesianL* local = static_cast<VoxelCartesianL*>(node);
		if(local->GetBCflagNode()){
			hit |= pick(local->GetBCflagNode()->GetGridGeometry(), frustum, selection, type);
		}

	} else if (t == NODETYPE_TRANSFORM) {
		Transform* trs = static_cast<Transform*>(node);
		const matrix& m = trs->GetMatrix();
		b8 push = false;
		if (m != Identity())
			push = true;
		
		if (push)
			m_stack->Push(m);
		s32 n = trs->GetChildCount();
		for (s32 i = 0; i < n; i++) {
			hit |= pick(trs->GetChild(i), frustum, selection, type);
		}
		if (push)
			m_stack->Pop();
	}

	return hit;
}

b8 SGPicker::pick(	VX::SG::Node* node, 
					const VX::Math::vec3* orgdir, 
					std::vector<VX::SG::HitGeometry>& hits,
					std::vector<VX::SG::HitDomain>& hdoms,
					std::vector<VX::SG::HitVoxelCartesianL>& ldoms, 
					TYPE_TRAVERSAL type)
{

	if (!node || !node->GetVisible())
		return false;
	
	b8 hit = false;
	using namespace VX::SG;
	using namespace VX::Math;
	const NODETYPE t = node->GetType();
	if (t == NODETYPE_GROUP) {
		Group* grp = static_cast<Group*>(node);
		s32 n = grp->GetChildCount();
		for (s32 i = 0; i < n; i++) {
			hit |= pick(grp->GetChild(i), orgdir, hits, hdoms,ldoms, type);
		}
	} else if (t == NODETYPE_GEOMETRY && (type & TYPE_GEOMETRY)) {
		GeometryBVH* geo = static_cast<GeometryBVH*>(node);
		if(!geo->GetSelectable()) return false;
		//const matrix invWorld = Inverse(m_stack->GetMatrix());
		const matrix world = m_stack->GetMatrix();
		const matrix invWorld = Inverse(world);
		const vec3 orgdir_t[] = {(invWorld * vec4(orgdir[0],1.0f)).xyz(), normalize((Transpose(invWorld) * vec4(orgdir[1],0.0f)).xyz())};
		hit |= geo->Intersect(orgdir_t, hits);
	
	} else if (t == NODETYPE_BBOX_DOMAIN_CARTESIAN && (type & TYPE_DOMAIN)) {
		DomainCartesian* dom = static_cast<DomainCartesian*>(node);
		const matrix world = m_stack->GetMatrix();
		const matrix invWorld = Inverse(world);
		const vec3 orgdir_t[] = {(invWorld * vec4(orgdir[0],1.0f)).xyz(), normalize((Transpose(invWorld) * vec4(orgdir[1],0.0f)).xyz())};
		hit |= dom->IntersectFrustum(orgdir_t, hdoms);

	} else if (t == NODETYPE_VOXEL_CARTESIAN_G) {
		const VoxelCartesianG* g = static_cast<VoxelCartesianG*>(node);

		//TODO:fuchi clippingが有効であれば考慮して無視する
		const VX::SG::VoxelCartesianC* clipNode	= g->GetClipNode();
		const VX::SG::Group*		localGroup	= g->GetLocalGroup();	
		const std::vector<VX::SG::NodeRefPtr<VX::SG::VoxelCartesianL> >& locals	= g->GetLocals();	
		bool isEnableClipping = clipNode->GetVisible();
		//スライスポジション
		VX::Math::vec3 slicePos;
		slicePos.x = clipNode->GetSlicePosition(0);
		slicePos.y = clipNode->GetSlicePosition(1);
		slicePos.z = clipNode->GetSlicePosition(2);

		if(localGroup->GetVisible()){
			int size = (int)locals.size();
			for (s32 i = 0; i < size; i++) {
				// call local domains
				VX::SG::VoxelCartesianL* local = locals.at(i);
				if(!isEnableClipping || local->IsVisibleByClippint(slicePos)){
					hit |= pick(local, orgdir, hits, hdoms,ldoms, type);
				}
			}
		}

	}else if (t == NODETYPE_VOXEL_CARTESIAN_L && (type & TYPE_DOMAIN)) {
		//for subdomain
		VoxelCartesianL* dom = static_cast<VoxelCartesianL*>(node);

		const matrix world = m_stack->GetMatrix();
		const matrix invWorld = Inverse(world);
		const vec3 orgdir_t[] = {(invWorld * vec4(orgdir[0],1.0f)).xyz(), normalize((Transpose(invWorld) * vec4(orgdir[1],0.0f)).xyz())};
		
		hit |= dom->IntersectFrustum(orgdir_t, ldoms);

	} else if (t == NODETYPE_VOXEL_CARTESIAN_L && (type & TYPE_GEOMETRY)) {
		VoxelCartesianL* local = static_cast<VoxelCartesianL*>(node);
		
		if(local->GetBCflagNode()){
			hit |= pick(local->GetBCflagNode()->GetGridGeometry(), orgdir, hits, hdoms,ldoms, type);
		}
	} else if (t == NODETYPE_TRANSFORM) {
		Transform* trs = static_cast<Transform*>(node);
		const matrix& m = trs->GetMatrix();
		b8 push = false;
		if (m != Identity())
			push = true;
		
		if (push)
			m_stack->Push(m);
		s32 n = trs->GetChildCount();
		for (s32 i = 0; i < n; i++) {
			hit |= pick(trs->GetChild(i), orgdir, hits, hdoms,ldoms, type);
		}
		if (push)
			m_stack->Pop();
	}
	
	return hit;
}
	
b8 SGPicker::endPick(VX::SG::Node* node, const VX::Math::vec3* orgdir, TYPE_TRAVERSAL type)
{
	if (!node || !node->GetVisible())
		return false;
	
	b8 hit = false;
	using namespace VX::SG;
	using namespace VX::Math;
	const NODETYPE t = node->GetType();
	if (t == NODETYPE_GROUP) {
		Group* grp = static_cast<Group*>(node);
		s32 n = grp->GetChildCount();
		for (s32 i = 0; i < n; i++) {
			hit |= endPick(grp->GetChild(i), orgdir, type);
		}
	} else if (t == NODETYPE_GEOMETRY && (type & TYPE_GEOMETRY)) {
		/*GeometryBVH* geo = static_cast<GeometryBVH*>(node);
		const matrix invWorld = Inverse(m_stack->GetMatrix());
		const matrix world = m_stack->GetMatrix();
		const matrix invWorld = Inverse(world);
		const vec3 orgdir_t[] = {(invWorld * vec4(orgdir[0],1.0f)).xyz(), normalize((Transpose(invWorld) * vec4(orgdir[1],0.0f)).xyz())};
		hit |= geo->Intersect(orgdir_t, hits);*/
	} else if (t == NODETYPE_BBOX_DOMAIN_CARTESIAN && (type & TYPE_DOMAIN)) {
		DomainCartesian* dom = static_cast<DomainCartesian*>(node);
		//const matrix world = m_stack->GetMatrix();
		//const matrix invWorld = Inverse(world);
		//const vec3 orgdir_t[] = {(invWorld * vec4(orgdir[0],1.0f)).xyz(), normalize((Transpose(invWorld) * vec4(orgdir[1],0.0f)).xyz())};
		//hit |= dom->IntersectFrustum(orgdir_t, hdoms);
		dom->EndPick();
	} else if (t == NODETYPE_VOXEL_CARTESIAN_G) {

		const VoxelCartesianG* g = static_cast<VoxelCartesianG*>(node);
		const std::vector<NodeRefPtr<VoxelCartesianL> >& locals = g->GetLocals();
		for (s32 i = 0; i < locals.size(); i++) {
			hit |= endPick(locals.at(i), orgdir, type);
		}

	} else if (t == NODETYPE_VOXEL_CARTESIAN_L && (type & TYPE_DOMAIN)) {

		//pick for subdomain
		VoxelCartesianL* dom = static_cast<VoxelCartesianL*>(node);
		assert(dom!=NULL);
		dom->EndPick();

	} else if (t == NODETYPE_TRANSFORM) {
		Transform* trs = static_cast<Transform*>(node);
		const matrix& m = trs->GetMatrix();
		b8 push = false;
		if (m != Identity())
			push = true;
		
		if (push)
			m_stack->Push(m);
		s32 n = trs->GetChildCount();
		for (s32 i = 0; i < n; i++) {
			hit |= endPick(trs->GetChild(i), orgdir, type);
		}
		if (push)
			m_stack->Pop();
	}
	
	return hit;
}
	
} // namespace VX

