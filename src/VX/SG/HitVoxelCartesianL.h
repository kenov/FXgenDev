/*
 * SceneGraphLibrary
 *
 */

#ifndef INCLUDE_HitVoxelCartesianL_H
#define INCLUDE_HitVoxelCartesianL_H

#include "../Type.h"

namespace VX
{
	namespace SG
	{

		class VoxelCartesianL;

		class HitVoxelCartesianL
		{

		public:
			HitVoxelCartesianL(VoxelCartesianL* dom, u32 index, f32 hit_t)
			{
				m_dom   = dom;
				m_index = index;
				m_hit_t = hit_t;
			}
			b8 operator<(const HitVoxelCartesianL& t) const
			{
				return m_hit_t < t.m_hit_t;
			}
				
			VoxelCartesianL* m_dom;
			u32 m_index;
			f32 m_hit_t;

			virtual ~HitVoxelCartesianL(){

			}
		
			HitVoxelCartesianL(){

			}
		
			//	コピーコンストラクタ
			HitVoxelCartesianL( const HitVoxelCartesianL& obj )
			{
				assignCopy(obj);
			}

			HitVoxelCartesianL& operator=(const HitVoxelCartesianL& obj)
			{
				if(this == &obj){
					return *this;   //自己代入
				}
				assignCopy(obj);
				return(*this);
			}	

		private:
			void assignCopy(const HitVoxelCartesianL& obj)
			{
				m_dom	= obj.m_dom;
				m_index	= obj.m_index;
				m_hit_t	= obj.m_hit_t;
			}

		};

	} // namespace SG
} // namespace VX

#endif // INCLUDE_HitVoxelCartesianL_H

