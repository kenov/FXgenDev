/*
 * Group.h
 *
 * SceneGraph Library
 *
 */

#ifndef INCLUDE_VXSCENEGRAPH_GROUP_H
#define INCLUDE_VXSCENEGRAPH_GROUP_H

#include "../Type.h"
#include "Node.h"
#include "Geometry.h"

#include <vector>
#include <string>

namespace VX
{
	namespace SG
	{
		
		class Group : public Node
		{
		public:
			Group(NODETYPE nt = NODETYPE_GROUP) : Node(nt) {
				b8 m_diaphaDraw = false;
			}
			~Group(){
			}
			
			void AddChild(Node* child)
			{
				if (!child)
					return;
				
				child->SetParent(this);
				m_childs.push_back(child);
			}
/*			void AddChildFirst(Node* child)
			{
				if (!child)
					return;
				
				m_childs.insert(m_childs.begin(), child);
			}*/

			void RemoveChild(Node* child)
			{
				if (!child)
					return;

				std::vector< NodeRefPtr<Node> >::iterator it, eit = m_childs.end();
				for (it = m_childs.begin(); it != eit; ++it)
				{
					if (*it == child)
					{
						m_childs.erase(it);
						break;
					}
				}
			}
			s32 GetChildCount() const
			{
				return static_cast<s32>(m_childs.size());
			}
			Node* GetChild(s32 i)
			{
				if (static_cast<s32>(m_childs.size()) <= i)
					return 0;
				return m_childs[i];
			}
			const Node* GetChild(s32 i) const
			{
				if (static_cast<s32>(m_childs.size()) <= i)
					return 0;
				return m_childs[i];
			}
			
			b8 GetSelection() const
			{
				b8 select = false;
				std::vector< NodeRefPtr<Node> >::const_iterator it, eit = m_childs.end();
				for (it = m_childs.begin(); it != eit; ++it)
				{
					select |= (*it)->GetSelection();
				}
				return select;
			}
			void SetSelection(b8 select, b8 flagonly = false)
			{
				std::vector< NodeRefPtr<Node> >::iterator it, eit = m_childs.end();
				for (it = m_childs.begin(); it != eit; ++it)
				{
					(*it)->SetSelection(select, flagonly);
				}
			}

			void clearLocalBCOuterBCMedium();

			void SetDiapha(b8 diapha)
			{
				this->Node::SetDiapha(diapha);
				s32 n = this->GetChildCount();
				for (s32 i = 0; i < n; i++) {
					Node* child = this->GetChild(i);
					if (child->GetType() == NODETYPE_GROUP) {
						Group* grp = static_cast<Group*>(child);
						grp->SetDiaphaDraw(GetDiapha() || GetDiaphaDraw());
					} else if (child->GetType() == NODETYPE_GEOMETRY) {
						Geometry* geo = static_cast<Geometry*>(child);
						geo->SetDiaphaDraw(GetDiapha() || GetDiaphaDraw());
					}
				}

			}

			void SetDiaphaDraw(b8 diaphaDraw)
			{
				m_diaphaDraw = diaphaDraw;
				s32 n = this->GetChildCount();
				for (s32 i = 0; i < n; i++) {
					Node* child = this->GetChild(i);
					if (child->GetType() == NODETYPE_GROUP) {
						Group* grp = static_cast<Group*>(child);
						grp->SetDiaphaDraw(GetDiapha() || GetDiaphaDraw());
					} else if (child->GetType() == NODETYPE_GEOMETRY) {
						Geometry* geo = static_cast<Geometry*>(child);
						geo->SetDiaphaDraw(GetDiapha() || GetDiaphaDraw());
					}
				}
			}

			b8 GetDiaphaDraw() const
			{
				return m_diaphaDraw;
			}

		private:
			std::vector< NodeRefPtr<Node> > m_childs;
			b8 m_diaphaDraw;
		};
		
    } // namespace SG
} // namespace VX

#endif // INCLUDE_VXSCENEGRAPH_GROUP_H
