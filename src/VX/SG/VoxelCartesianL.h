/*
 * VX/SG/CellIDVoxelCartesian.h
 *
 * SceneGraphLibrary
 *
 */

#ifndef INCLUDE_VXSCENEGRAPH_VoxelCartesianL_H
#define INCLUDE_VXSCENEGRAPH_VoxelCartesianL_H

#include "../Type.h"
#include "../Math.h"

#include "Node.h"
#include "Group.h"
#include "Geometry.h"


#include "MeshUtil.h"
#include "VoxelCartesianL_Scan.h"


#include <string>
#include <vector>

namespace VX
{
	namespace SG
	{

		class VoxelCartesianSliceL;
		class VoxelCartesianBCflag;
		class VoxelCartesianBlock;
		class Geometry;
		class VoxelCartesianC;
		class VoxelCartesianL_SelectionHelper;
		class HitVoxelCartesianL;



		class VoxelCartesianL : public Group
		{

			friend class VoxelCartesianL_SelectionHelper;  // Declare a friend class

		public:
			
			VoxelCartesianL( const u32& id,
							const VX::Math::vec3& origin,
			               const VX::Math::vec3& region,
						   const VX::Math::idx3& voxSize,
						   const VX::Math::idx3& divs,
						    const VX::Math::idx3& headGIdx,
							const std::string& block_CellID_file,
							  const std::string& block_BCflagID_file,
							  const s32& cellID,
							  const s32& bCflagID,
							  
							  const bool& header_isNeedSwap,
							  const u32& header_gc,
							  const u32& digit_rank,
							  const u32& digit_grid
							  );


			~VoxelCartesianL();


			const VX::Math::idx3& GetHeadIdx_G() const;
			const VX::Math::idx3& GetHeadIdx_L() const;
			const VX::Math::idx3& GetNumVoxels() const;
			const VX::Math::idx3& GetNumDivs() const;
			const VX::Math::vec3& GetOrigin() const;
			const VX::Math::vec3& GetRegion() const;
			const VX::Math::vec3& GetMin() const;	
			const VX::Math::vec3 GetMax() const;		
			const VX::Math::vec3& GetPitch() const;
			const VoxelCartesianBlock* GetBlock_CellID() const;
			const VoxelCartesianBlock* GetBlock_BCflagID() const;
			void GetBlocksOnSlice(const u32 axis, const size_t idx, std::vector<const VoxelCartesianBlock*> &blockList) const;
			void SetSlicePosition(const u32 axis, const f32 pos, const u32 index_coord, const b8 boundaryOnly);
			f32 GetSlicePosition(const u32 axis) const ;
			// 座標値からインデックス番号を取得する
			u32 GetIdxPosition(const u32 axis, const f32 coord_pos) const;
			// インデックス番号から座標値を取得する
			f32 GetCoordPosition(const u32 axis, const u32 idx_pos) const;
			b8 GetBBoxMinMax(VX::Math::vec3& minval, VX::Math::vec3& maxval) const;

			size_t GetByteSize();
			u32	Getid()const;
			std::string GetNameOnGridCell()const;

			b8 IsLoad()const;
			void SetLoad(const b8& b,std::string& errMsg);

	
			void LoadBlockFiles(std::string& errMsg);
			void UnLoadBlockFiles();

			s32 GetConst_CellID()const;
			s32 GetConst_BCflagID()const;

			//for BCflag
			void ChangeShowBCFlag(const std::vector<u8>& BCflags);
			VoxelCartesianBCflag* GetBCflagNode();
			const VoxelCartesianBCflag* GetBCflagNode()const;
			///////////////////////////////////////////
			// for pick , clip selection
			void Select(b8 select);
			void SelectSubdomain(u32 idx, b8 select);
			b8 IntersectFrustum(const Math::vec3* orgdir, std::vector<VX::SG::HitVoxelCartesianL>& hdoms);
			b8 IntersectFrustum(const Math::vec4* frustum, const Math::matrix& mat, b8 selection = true);
			b8 EndPick();
			const VX::SG::Geometry* GetGeometry() const;
			const VX::SG::Geometry* GetClipGeometry() const;
			const VX::SG::Geometry* GetBoxGeometry() const;
			b8 GetSelection() const;
			void SetSelection(b8 select, b8 flagonly = false);

			// clipping check
			b8 IsVisibleByClippint(const VX::Math::vec3& slicePos)const;
			void RepaintSlicePlane();

            // for scan
            
            void ScanCellID(  VoxelCartesianL_Scan& vals);
            void ScanBCflg(  VoxelCartesianL_Scan& vals);
            
			const VoxelCartesianL_Scan& GetScanResult_bcf()const;
			const VoxelCartesianL_Scan& GetScanResult_cel()const;


			static const int BCFLAG_NONE;
		private:
			void setDispName();
		
			void AddBCflagNode();

			void load_voxdata(std::string& errMsg);
			void clear_voxdata();

		private:
			u32				m_id;
			VX::Math::vec3	m_org;
			VX::Math::vec3	m_rgn;
			VX::Math::vec3	m_pitch;
			VX::Math::idx3	m_voxSize;
			VX::Math::idx3	m_divs;

			VX::Math::idx3		m_headIdx_G;
			VX::Math::idx3		m_headIdx_L;//(1,1,1)固定　コンストラクタで定義
			s32					m_CellID;//固定値
			s32					m_BCflagID;//固定値
			std::string			m_block_CellID_file;
			std::string			m_block_BCflagID_file;
			VoxelCartesianBlock* m_block_CellID;
			VoxelCartesianBlock* m_block_BCflagID;
			bool				m_header_isNeedSwap;//ドメイン共通定義のスワップフラグ
			u32					m_header_gc;// ドメイン共通定義の ガイドセル

			b8				m_load;
	
			u32 m_digit_rank ;//ランクの最大桁数
			u32 m_digit_grid ;//セルの最大桁数

			//ボクセルのスキャン結果
			VoxelCartesianL_Scan m_cel_scan_result;


			NodeRefPtr<VoxelCartesianSliceL> m_slices[3];

			//外枠の線。不要かもしれない
			//NodeRefPtr<Geometry> m_boundary_geo;

			u64 m_numUseBlockMemory;

			// for bcflag
			NodeRefPtr<VoxelCartesianBCflag> m_BCflag;

			// for selection
			VoxelCartesianL_SelectionHelper* m_SelectionHelper;


		};// end of class 
	} // namespace SG

} // namespace VX

#endif // INCLUDE_VXSCENEGRAPH_VoxelCartesianL_H

