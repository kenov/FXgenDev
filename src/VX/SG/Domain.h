/*
 * VX/SG/Domain.h
 *
 * SceneGraphLibrary
 *
 */

#ifndef INCLUDE_VXSCENEGRAPH_DOMAIN_H
#define INCLUDE_VXSCENEGRAPH_DOMAIN_H

#include "../Type.h"
#include "../Math.h"

#include "Node.h"
#include "OuterBC.h"
#include "Medium.h"

#include <vector>
#include <string>

namespace VX
{
	namespace SG
	{
		class OuterBC;
		class Medium;
		class Domain;
		
		class HitDomain
		{
		public:
			HitDomain(Domain* dom, u32 index, f32 hit_t)
			{
				m_dom   = dom;
				m_index = index;
				m_hit_t = hit_t;
			}
			b8 operator<(const HitDomain& t) const
			{
				return m_hit_t < t.m_hit_t;
			}
				
			Domain* m_dom;
			u32 m_index;
			f32 m_hit_t;
		};
			

				
		class Domain : public Node
		{
		public:
			enum BC_DIRECTION{
				X_NEGATIVE = 0,
				X_POSITIVE = 1,
				Y_NEGATIVE = 2,
				Y_POSITIVE = 3,
				Z_NEGATIVE = 4,
				Z_POSITIVE = 5,
				DIR_UNDEFINED
			};


			Domain(const NODETYPE ntype = NODETYPE_BBOX_DOMAIN,
			       const std::string name = "Domain",
				   const std::string unit = "NonDimensional") 
			: Node(ntype)
			{
				SetName(name);
				

				InitUnitList();

				if(!SetUnit(unit)){
					m_unit = "NonDimensional";
				}

				for(int i = 0; i < 6; i++){
					m_flowBC[i]    = NULL;
					m_medium[i]    = NULL;
				}
			}
			
			virtual ~Domain()
			{

			}

			void SetFlowBC(const BC_DIRECTION dir, const OuterBC* bc)
			{
				m_flowBC[dir] = bc;
			}

			void SetGuideCellMedium(const BC_DIRECTION dir, const Medium* med)
			{
				m_medium[dir] = med;
			}

			const OuterBC* GetFlowBC(const BC_DIRECTION dir ) const
			{
				return m_flowBC[dir];
			}

			const Medium* GetGuideCellMedium( const BC_DIRECTION dir ) const
			{
				return m_medium[dir];
			}

			const std::string& GetUnit( ) const
			{
				return m_unit;
			}

			b8 SetUnit( const std::string& unit )
			{
				if( !IsCorrectUnit(unit) ){
					return false;
				}
				m_unit = unit;
				return true;
			}

			const std::vector<std::string>& GetUnitList() const
			{
				return m_unitList;
			}

			virtual void SelectedSubdomainStatus(b8 active) = 0;
			
		private:
			void InitUnitList()
			{
				m_unitList.reserve(4);
				m_unitList.push_back("NonDimensional");
				m_unitList.push_back("m");
				m_unitList.push_back("cm");
				m_unitList.push_back("mm");
			}

			b8 IsCorrectUnit(const std::string& unit)
			{
				for(int i = 0; i < m_unitList.size(); i++){
					if(unit == m_unitList[i]){
						return true;
					}
				}
				return false;
			}


		// Member
		private:
			NodeRefPtr<const OuterBC> m_flowBC[6];
			NodeRefPtr<const Medium>  m_medium[6];
			
			std::string m_unit;

			std::vector<std::string> m_unitList;

		};

		class DomainSlice :public Node
		{
		public:
			DomainSlice(NODETYPE type) : Node(type) {}
			virtual const Geometry* GetBoundaryGeometry() const = 0;
			virtual const Geometry* GetBlockGeometry() const = 0;
			virtual const Geometry* GetFaceGeometry() const = 0;
		};

	} // namespace SG
} // namespace VX

#endif // INCLUDE_VXSCENEGRAPH_DOMAIN_H

