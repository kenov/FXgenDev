/*
 * VX/SG/GeometryUtil.cpp
 *
 */
#include "GeometryUtil.h"
#include <cfloat>
#include <cmath>
#include <algorithm>

using namespace VX::Math;

namespace {
	
inline void minmax(f32 a, f32 b, f32 c, f32& min, f32& max)
{
	min = (a < b ? (a < c ? a : c) : (b < c ? b : c));
	max = (a > b ? (a > c ? a : c) : (b > c ? b : c));
}

inline vec3 project(const vec3& v, const vec3* basis)
{
	//m = transpose(mat3(basis0,1,2)) -- basis is orthonormal basis
	//return m * v;
	return vec3(
		basis[0][0] * v.x + basis[0][1] * v.y + basis[0][2] * v.z,
		basis[1][0] * v.x + basis[1][1] * v.y + basis[1][2] * v.z,
		basis[2][0] * v.x + basis[2][1] * v.y + basis[2][2] * v.z);
}

} // namespace


// 
b8 TestIntersectionAABBTriangle(
	const vec3& center,
	const vec3& halfextent,
	const vec3* triangle)
{
	const vec3 v0 = triangle[0] - center;
	const vec3 v1 = triangle[1] - center;
	const vec3 v2 = triangle[2] - center;

	for (int i = 0; i < 3; ++i)
	{
		f32 min, max;
		minmax(v0[i], v1[i], v2[i], min, max);
		const f32 r = halfextent[i];
		if (min > r || max < -r) return false;
	}

	const vec3 e0 = v1 - v0;
	const vec3 e1 = v2 - v1;
	const vec3 e2 = v0 - v2;
	{
		const vec3 n = cross(e0, e1);
		// project box onto triangle normal
		const f32 r = std::abs(n.x) * halfextent.x + std::abs(n.y) * halfextent.y + std::abs(n.z) * halfextent.z;
		const f32 d = dot(n, v0);
		if (d - r > 0 || d + r < 0) return false;
	}
	
	const vec3 faces[] = { vec3(1, 0, 0), vec3(0, 1, 0), vec3(0, 0, 1) };
	const vec3 e[3] = { e0, e1, e2 };
	for (int j = 0; j < 3; ++j)
	{
		for (int i = 0; i < 3; ++i)
		{
			const vec3 a = cross(faces[i], e[j]);
			f32 min, max;
			minmax(dot(a, v0), dot(a, v1), dot(a, v2), min, max);
			// project box onto axis-a. It can be considered as a "radius"
			const f32 r = std::abs(a.x) * halfextent.x + std::abs(a.y) * halfextent.y + std::abs(a.z) * halfextent.z;
			if (min > r || max < -r) return false;
		}
	}

	return true;
}

b8 TestIntersectionOBBTriangle(
	const vec3& center,
	const vec3& halfextent,
	const vec3* basis,
	const vec3* triangle)
{
	// not tested

	const vec3 v0 = project(triangle[0] - center, basis);
	const vec3 v1 = project(triangle[1] - center, basis);
	const vec3 v2 = project(triangle[2] - center, basis);

	for (int i = 0; i < 3; ++i)
	{
		f32 min, max;
		minmax(v0[i], v1[i], v2[i], min, max);
		const f32 r = halfextent[i];
		if (min > r || max < -r) return false;
	}

	const vec3 e0 = v1 - v0;
	const vec3 e1 = v2 - v1;
	const vec3 e2 = v0 - v2;
	{
		const vec3 n = cross(e0, e1);
		// project box onto triangle normal
		const f32 r = std::abs(n.x) * halfextent.x + std::abs(n.y) * halfextent.y + std::abs(n.z) * halfextent.z;
		const f32 d = dot(n, v0);
		if (d - r > 0 || d + r < 0) return false;
	}
	
	const vec3 e[3] = { e0, e1, e2 };
	for (int j = 0; j < 3; ++j)
	{
		for (int i = 0; i < 3; ++i)
		{
			const vec3 a = cross(basis[i], e[j]);
			f32 min, max;
			minmax(dot(a, v0), dot(a, v1), dot(a, v2), min, max);
			// project box onto axis-a. It can be considered as a "radius"
			const f32 r = std::abs(a.x) * halfextent.x + std::abs(a.y) * halfextent.y + std::abs(a.z) * halfextent.z;
			if (min > r || max < -r) return false;
		}
	}

	return true;
}

f32 DistanceSqPointAABB(
	const VX::Math::vec3& point,
	const VX::Math::vec3& center,
	const VX::Math::vec3& halfextent,
	f32 cutoff,
	VX::Math::vec3& closest)
{
	const vec3 v = point - center;
	const vec3 axis[] = { vec3(1, 0, 0), vec3(0, 1, 0), vec3(0, 0, 1) };

	f32 ret = 0;
	vec3 c;
	for (int i = 0; i < 3; ++i)
	{
		c[i] = dot(v, axis[i]);
		if (c[i] < -halfextent[i])
		{
			f32 delta = c[i] + halfextent[i];
			ret += delta * delta;
		}
		else if (halfextent[i] < c[i])
		{
			f32 delta = c[i] - halfextent[i];
			ret += delta * delta;
		}
	}
	if (ret < cutoff)
	{
		closest = center + c;
	}
	return ret;
}

// http://www.geometrictools.com/Documentation/DistancePoint3Triangle3.pdf
f32 DistanceSqPointTriangle(
	const vec3& point,
	const vec3* triangle,
	f32 cutoff,
	vec3& closest)
{
	const vec3 v0 = triangle[0] - point;
	const vec3 e0 = triangle[1] - triangle[0];
	const vec3 e1 = triangle[2] - triangle[0];

	const f32 a00 = dot(e0, e0);
	const f32 a01 = dot(e0, e1);
	const f32 a11 = dot(e1, e1);
	const f32 b0 = dot(v0, e0);
	const f32 b1 = dot(v0, e1);
	const f32 c = dot(v0, v0);
	const f32 det = std::abs(a00*a11 - a01*a01);
	f32 s = a01*b1 - a11*b0; // = numerator of (a01 * b1 - a11 * b0) / det
	f32 t = a01*b0 - a00*b1; // = numerator of (a01 * b0 - a00 * b1) / det

	// squared distance function: Q
//#define DISTANCE_SQ(s, t) ((a00) * s*s + (2 * a01) * s*t + (a11) * t*t + (2 * b0) * s + (2.f * b1) * t + c)
#define DISTANCE_SQ(s, t) (s * (a00*s + a01*t + 2*b0) + t * (a01*s + a11*t + 2*b1) + c)
	// gradient of Q
	// dQ(s, t) = (2*a00*s + 2*a01*t + 2*b0, 2*a11*t + 2*a01*s + 2*b1)

	f32 d = FLT_MAX;
	if (s + t <= det)
	{
		if (s < 0 && t < 0) // region 4
		{
			// s=0 or t=0
			if (b0 < 0)
			{
				s = Clamp(- b0 / a00, 0, 1);
				t = 0;
				d = DISTANCE_SQ(s, 0);
			}
			else
			{
				s = 0;
				t = Clamp(- b1 / a11, 0, 1);
				d = DISTANCE_SQ(0, t);
			}
		}
		else if (s < 0) // region 3
		{
			s = 0;
			t = Clamp(- b1 / a11, 0, 1);
			d = DISTANCE_SQ(0, t);
		}
		else if (t < 0) // region 5
		{
			s = Clamp(- b0 / a00, 0, 1);
			t = 0;
			d = DISTANCE_SQ(s, 0);
		}
		else // region 0
		{
			s = s / det;
			t = t / det;
			d = DISTANCE_SQ(s, t);
		}
	}
	else
	{
		if (s < 0) // region 2
		{
			// s=0 or s+t=1
			// (0,-1) . dQ(0,1) = - 2*a11 - 2*b1
			// (1,-1) . dQ(0,1) = 2*a01 + 2*b0 - 2*a11 - 2*b1
			f32 numer = a11 + b1 - a01 - b0; // with t
			f32 denom = a00 - 2 * a01 + a11;
			if (0 < numer)
			{
				s = Clamp(numer / denom, 0, 1); // solve Q(s, 1-s)/ds = 0
				t = 1 - s;
				d = DISTANCE_SQ(s, t);
			}
			else
			{
				s = 0;
				t = Clamp(- b1 / a11, 0, 1);
				d = DISTANCE_SQ(0, t);
			}
		}
		else if (t < 0) // region 6
		{
			// t=0 or s+t=1
			// (-1,0) . dQ(1,0) = - 2*a00 - 2*b0
			// (1,-1) . dQ(1,0) = 2*a00 + 2*b0 - 2*a01 - 2*b1
			f32 numer = a00 + b0 - a01 - b1;
			f32 denom = a00 - 2 * a01 + a11;
			if (0 < numer)
			{
				t = Clamp(numer / denom, 0, 1); // solve Q(1-t, t)/dt = 0
				s = 1 - t;
				d = DISTANCE_SQ(s, t);
			}
			else
			{
				s = Clamp(- b0 / a00, 0, 1);
				t = 0;
				d = DISTANCE_SQ(s, 0);
			}
		}
		else // region 1
		{
			// s+t=1
			f32 numer = a11 + b1 - a01 - b0; // solve Q(s, 1-s)/ds = 0
			f32 denom = a00 - 2*a01 + a11;
			s = Clamp(numer / denom, 0, 1);
			t = 1 - s;
			d = DISTANCE_SQ(s, t);
		}
	}

	if (d < cutoff)
	{
		closest = triangle[0] + e0 * s + e1 * t;
	}

	// account for numerical round-off error
	if (d < 0)
	{
		d = 0; 
	}
	return d;
}

void UpdateBounding(
	const vec3& center,
	const vec3& halfextent,
	const vec3* basis,
	vec3& aabb_min,
	vec3& aabb_max)
{
	vec3 e;
	for (int i = 0; i < 3; ++i)
	{
		e[i] = Abs(basis[0][i]) * halfextent.x + Abs(basis[1][i]) * halfextent.y + Abs(basis[2][i]) * halfextent.z;
	}
	aabb_min = center - e;
	aabb_max = center + e;
}


f32 DistanceSqPointPoints(
	const vec3& point,
	const vec3* points, u32 count,
	f32 cutoff,
	vec3& closest)
{
	f32 ret = cutoff;
	for (u32 i = 0; i < count; ++i)
	{
		const vec3 diff = points[i] - point;
		const f32 d = dot(diff, diff);
		if (d < ret)
		{
			ret = d;
			closest = points[i];
		}
	}
	return ret;
}
