#include "VoxelCartesianC.h"

#include "../Type.h"
#include "../Math.h"

#include "Node.h"
#include "Group.h"
#include "Geometry.h"
#include "Texture.h"
#include "VoxGeometryUtil.h"
#include "VoxelCartesianBlock.h"
#include "VoxelCartesianSliceC.h"
#include "VoxelCartesianSliceL.h"
#include "VoxelCartesianL.h"

#include "MeshUtil.h"

#include <string>
#include <vector>

#include<stdio.h>
#include <limits.h>

/*
namespace {
	//order is ABGR 
	const static u32 boundaryColor = 0xFF00FFFF;

} // namespace 
*/

namespace VX {
namespace SG {


//====================== VoxelCartesianC ===========================
VoxelCartesianC::VoxelCartesianC( const VX::Math::vec3& origin,
                              const VX::Math::vec3& region,
							  const VX::Math::idx3& voxSize,
							  const VX::Math::idx3& divs)
	: m_org(origin),
	  m_rgn(region),
	  m_voxSize(voxSize),
	  m_divs(divs),
      Group(NODETYPE_VOXEL_CARTESIAN_C)
	 
{
	m_pitch = VX::Math::vec3( m_rgn.x / float(m_voxSize.x),
	                          m_rgn.y / float(m_voxSize.y),
							  m_rgn.z / float(m_voxSize.z) );


	m_numUseBlockMemory = 0;


	for(int i = 0; i < 3; i++){
		VoxelCartesianSliceC* slice = vxnew VoxelCartesianSliceC(i, this);
		this->AddChild(slice);
		m_slices[i] = slice;
	}

	// add region scope
	//AddRegionScopeGeometry(m_org, m_rgn);


	setDispName();
}

VoxelCartesianC::~VoxelCartesianC()
{

	//for(std::vector<VoxelCartesianL*>::iterator it = m_locals.begin(); it != m_locals.end(); ++it){
		//vxdelete(*it);
	//}

	//Memory::SubExternalSize(m_numUseBlockMemory); // TODO
}

void VoxelCartesianC::setDispName()
{
	
	char name[100];
	
	sprintf(name, "Clipping");

	SetName(name);
}


const VX::Math::idx3& VoxelCartesianC::GetNumVoxels() const
{
	return m_voxSize;
}

const VX::Math::idx3& VoxelCartesianC::GetNumDivs() const
{
	return m_divs;
}

const VX::Math::vec3& VoxelCartesianC::GetOrigin() const
{
	return m_org;
}

const VX::Math::vec3& VoxelCartesianC::GetRegion() const
{
	return m_rgn;
}

const VX::Math::vec3& VoxelCartesianC::GetMin() const 
{
	return m_org;
}
			
const VX::Math::vec3 VoxelCartesianC::GetMax() const
{
	return (m_org + m_rgn);
}
			
const VX::Math::vec3& VoxelCartesianC::GetPitch() const 
{
	return m_pitch;
}

void VoxelCartesianC::SetSlicePosition(const u32 axis, const f32 pos, const u32 index_coord, const b8 boundaryOnly)
{
	m_slices[axis]->SetPosition(pos, index_coord, boundaryOnly);
}

f32 VoxelCartesianC::GetSlicePosition(const u32 axis) const 
{
	return m_slices[axis]->GetPosition();
}

// 座標値からインデックス番号を取得する
u32 VoxelCartesianC::GetIdxPosition(const u32 axis, const f32 coord_pos) const
{
	u32 posIdx = static_cast<u32>((coord_pos - m_org[axis]) / m_pitch[axis]);
	posIdx = static_cast<s32>(posIdx) >= m_voxSize[axis] ? m_voxSize[axis]-1 : posIdx;
	return posIdx;
}

// インデックス番号から座標値を取得する
f32 VoxelCartesianC::GetCoordPosition(const u32 axis, const u32 idx_pos) const
{
	f32 posCoord = m_org[axis] + m_pitch[axis] * idx_pos;
	return posCoord;
}


b8 VoxelCartesianC::GetBBoxMinMax(VX::Math::vec3& minval, VX::Math::vec3& maxval) const
{
	minval = m_org;
	maxval = minval + m_rgn;
	return true;
}
/*
void VoxelCartesianC::AddRegionScopeGeometry(const VX::Math::vec3& origin, const VX::Math::vec3& region)
{
	using namespace VX::SG;

	Geometry *geo = vxnew Geometry();
	geo->SetUseVertexLineColor(true);
	geo->SetDrawMode(Geometry::MODE_LINE);
	geo->SetName("Region Scope");
	geo->SetSelectable(false);
	geo->Alloc(8, 24);

	BoxSetter(geo->GetVertex(), geo->GetIndex(), origin, region,boundaryColor);
	geo->CalcBounds();
	geo->EnableNeedUpdate();

	this->AddChild(geo);

}
*/

} // namespace SG
} // namespace VX



