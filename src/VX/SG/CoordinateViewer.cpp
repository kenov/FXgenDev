/*
 *
 * CoordinateViewer.cpp
 * 
 */
#include "../VX.h"
#include "../SG/SceneGraph.h"
#include "../SG/GeometryBVH.h"



#include "../Stream.h"

#include <string>
#include <set>
#include <string.h>

#include "../../FileIO/FileIOUtil.h"
#include "CoordinateViewer.h"

PolygonGenerator::PolygonGenerator(){
}
PolygonGenerator::~PolygonGenerator(){
}

VX::SG::Node* PolygonGenerator::Load(const wxString& posX_p, const wxString& posY_p,const wxString& posZ_p,const wxString& size_p)
{
	VX::SG::GeometryBVH* g = vxnew VX::SG::GeometryBVH();
	load_inner_p( posX_p,posY_p,posZ_p,size_p, g );
	return g;
}

void PolygonGenerator::Update(const wxString& posX_p, const wxString& posY_p,const wxString& posZ_p,const wxString& size_p,VX::SG::GeometryBVH* g/*IO*/)
{
	load_inner_p( posX_p,posY_p,posZ_p,size_p, g );
}
void PolygonGenerator::load_inner_p(const wxString& posX_p, const wxString& posY_p,const wxString& posZ_p,const wxString& size_p,
										VX::SG::GeometryBVH* g //io
										)
{
	using namespace VX::Math;
	using namespace VX::SG;

	Geometry::VertexFormat vtx;
	std::vector<Geometry::VertexFormat> vertices;
    //size_t i = 0;

	//point
	Geometry::VertexFormat center;
	float length;

	float pos_x = atof(posX_p);
	float pos_y = atof(posY_p);
	float pos_z = atof(posZ_p);
	float size = atof(size_p);

	//input_data
	center.pos.x = pos_x;
	center.pos.y = pos_y;
	center.pos.z = pos_z;
	length = size;
	static const u32 color = VX::initColor;

	//output_data

	Geometry::VertexFormat tmp_vtx[8];
	//頂点1
	tmp_vtx[0].pos.x = center.pos.x + length / 2;
	tmp_vtx[0].pos.y = center.pos.y + (-1) * length / 2;
	tmp_vtx[0].pos.z = center.pos.z + length / 2;

	//頂点2
	tmp_vtx[1].pos.x = center.pos.x + length / 2;
	tmp_vtx[1].pos.y = center.pos.y + length / 2;
	tmp_vtx[1].pos.z = center.pos.z + length / 2;

	//頂点3
	tmp_vtx[2].pos.x = center.pos.x + length / 2;
	tmp_vtx[2].pos.y = center.pos.y + length / 2;
	tmp_vtx[2].pos.z = center.pos.z + (-1) * length / 2;

	//頂点4
	tmp_vtx[3].pos.x = center.pos.x + length / 2;
	tmp_vtx[3].pos.y = center.pos.y + (-1) * length / 2;
	tmp_vtx[3].pos.z = center.pos.z + (-1) * length / 2;

	//頂点5
	tmp_vtx[4].pos.x = center.pos.x + (-1) * length / 2;
	tmp_vtx[4].pos.y = center.pos.y + (-1) * length / 2;
	tmp_vtx[4].pos.z = center.pos.z + length / 2;

	//頂点6
	tmp_vtx[5].pos.x = center.pos.x + (-1) * length / 2;
	tmp_vtx[5].pos.y = center.pos.y + length / 2;
	tmp_vtx[5].pos.z = center.pos.z + length / 2;

	//頂点7
	tmp_vtx[6].pos.x = center.pos.x + (-1) * length / 2;
	tmp_vtx[6].pos.y = center.pos.y + length / 2;
	tmp_vtx[6].pos.z = center.pos.z + (-1) * length / 2;

	//頂点8
	tmp_vtx[7].pos.x = center.pos.x + (-1) * length / 2;
	tmp_vtx[7].pos.y = center.pos.y + (-1) * length / 2;
	tmp_vtx[7].pos.z = center.pos.z + (-1) * length / 2;

	//+x方向の生成
	//法線方向
	vtx.normal.x=1.0;
	vtx.normal.y=0.0;
	vtx.normal.z=0.0;

	vtx.pos = tmp_vtx[0].pos;
	vtx.col = color;
	vertices.push_back(vtx);	

	vtx.pos = tmp_vtx[3].pos;
	vtx.col = color;
	vertices.push_back(vtx);

	vtx.pos = tmp_vtx[2].pos;
	vtx.col = color;
	vertices.push_back(vtx);	

	vtx.normal.x=1.0;
	vtx.normal.y=0.0;
	vtx.normal.z=0.0;

	vtx.pos = tmp_vtx[0].pos;
	vtx.col = color;
	vertices.push_back(vtx);	

	vtx.pos = tmp_vtx[2].pos;
	vtx.col = color;
	vertices.push_back(vtx);	

	vtx.pos = tmp_vtx[1].pos;
	vtx.col = color;
	vertices.push_back(vtx);

	//-x方向の生成
	//法線方向
	vtx.normal.x=-1.0;
	vtx.normal.y=0.0;
	vtx.normal.z=0.0;

	vtx.pos = tmp_vtx[4].pos;
	vtx.col = color;
	vertices.push_back(vtx);	

	vtx.pos = tmp_vtx[5].pos;
	vtx.col = color;
	vertices.push_back(vtx);	

	vtx.pos = tmp_vtx[6].pos;
	vtx.col = color;
	vertices.push_back(vtx);	

	vtx.normal.x=-1.0;
	vtx.normal.y=0.0;
	vtx.normal.z=0.0;

	vtx.pos = tmp_vtx[4].pos;
	vtx.col = color;
	vertices.push_back(vtx);	

	vtx.pos = tmp_vtx[6].pos;
	vtx.col = color;
	vertices.push_back(vtx);	

	vtx.pos = tmp_vtx[7].pos;
	vtx.col = color;
	vertices.push_back(vtx);

	//+y方向の生成
	//法線方向
	vtx.normal.x=0.0;
	vtx.normal.y=1.0;
	vtx.normal.z=0.0;

	vtx.pos = tmp_vtx[1].pos;
	vtx.col = color;
	vertices.push_back(vtx);	

	vtx.pos = tmp_vtx[2].pos;
	vtx.col = color;
	vertices.push_back(vtx);	

	vtx.pos = tmp_vtx[5].pos;
	vtx.col = color;
	vertices.push_back(vtx);	

	vtx.normal.x=0.0;
	vtx.normal.y=1.0;
	vtx.normal.z=0.0;

	vtx.pos = tmp_vtx[5].pos;
	vtx.col = color;
	vertices.push_back(vtx);	

	vtx.pos = tmp_vtx[2].pos;
	vtx.col = color;
	vertices.push_back(vtx);	

	vtx.pos = tmp_vtx[6].pos;
	vtx.col = color;
	vertices.push_back(vtx);

	//-y方向の生成
	//法線方向
	vtx.normal.x=0.0;
	vtx.normal.y=-1.0;
	vtx.normal.z=0.0;

	vtx.pos = tmp_vtx[0].pos;
	vtx.col = color;
	vertices.push_back(vtx);	

	vtx.pos = tmp_vtx[4].pos;
	vtx.col = color;
	vertices.push_back(vtx);	

	vtx.pos = tmp_vtx[7].pos;
	vtx.col = color;
	vertices.push_back(vtx);	

	vtx.normal.x=0.0;
	vtx.normal.y=-1.0;
	vtx.normal.z=0.0;

	vtx.pos = tmp_vtx[0].pos;
	vtx.col = color;
	vertices.push_back(vtx);	

	vtx.pos = tmp_vtx[7].pos;
	vtx.col = color;
	vertices.push_back(vtx);	

	vtx.pos = tmp_vtx[3].pos;
	vtx.col = color;
	vertices.push_back(vtx);

	//+z方向の生成
	//法線方向
	vtx.normal.x=0.0;
	vtx.normal.y=0.0;
	vtx.normal.z=1.0;

	vtx.pos = tmp_vtx[0].pos;
	vtx.col = color;
	vertices.push_back(vtx);

	vtx.pos = tmp_vtx[1].pos;
	vtx.col = color;
	vertices.push_back(vtx);	

	vtx.pos = tmp_vtx[5].pos;
	vtx.col = color;
	vertices.push_back(vtx);	

	vtx.normal.x=0.0;
	vtx.normal.y=0.0;
	vtx.normal.z=1.0;

	vtx.pos = tmp_vtx[5].pos;
	vtx.col = color;
	vertices.push_back(vtx);	

	vtx.pos = tmp_vtx[4].pos;
	vtx.col = color;
	vertices.push_back(vtx);	

	vtx.pos = tmp_vtx[0].pos;
	vtx.col = color;
	vertices.push_back(vtx);

	//-z方向の生成
	//法線方向
	vtx.normal.x=0.0;
	vtx.normal.y=0.0;
	vtx.normal.z=-1.0;

	vtx.pos = tmp_vtx[7].pos;
	vtx.col = color;
	vertices.push_back(vtx);	

	vtx.pos = tmp_vtx[6].pos;
	vtx.col = color;
	vertices.push_back(vtx);	

	vtx.pos = tmp_vtx[2].pos;
	vtx.col = color;
	vertices.push_back(vtx);	

	vtx.normal.x=0.0;
	vtx.normal.y=0.0;
	vtx.normal.z=1.0;

	vtx.pos = tmp_vtx[7].pos;
	vtx.col = color;
	vertices.push_back(vtx);	

	vtx.pos = tmp_vtx[2].pos;
	vtx.col = color;
	vertices.push_back(vtx);	

	vtx.pos = tmp_vtx[3].pos;
	vtx.col = color;
	vertices.push_back(vtx);


	const u32 vertexCount = static_cast<u32>(vertices.size());
	const u32 indexCount  = vertexCount;
	
	//GeometryBVH* g = vxnew GeometryBVH();
	//連続領域の再構築
	g->Alloc(vertexCount, indexCount);
	
	memcpy(g->GetVertex(), &vertices[0], sizeof(Geometry::VertexFormat) * vertexCount); // vertex copy
	vertices.clear();
	std::vector<Geometry::VertexFormat> size0;
	vertices.swap(size0); // free std::vector
	
	Index* index = vxnew Index[indexCount];
	for (u32 i = 0; i < indexCount; i++)
		index[i] = i;
	memcpy(g->GetIndex(), index, sizeof(Index) * indexCount); // index copy
	vxdeleteArray(index);
	
	g->CalcBounds();
	
}

VX::SG::Node* PolygonGenerator::Load(const wxString& posX,const wxString& posY,const wxString& posZ,const wxString& width,const wxString& depth,
							const wxString& height,const wxString& nor_vecX,const wxString& nor_vecY,const wxString& nor_vecZ,
							const wxString& dir_vecX,const wxString& dir_vecY,const wxString& dir_vecZ)
{
	VX::SG::GeometryBVH* g = vxnew VX::SG::GeometryBVH();
	load_inner_r( posX,posY,posZ,width,
			depth,height,nor_vecX,nor_vecY,nor_vecZ,
			dir_vecX,dir_vecY,dir_vecZ, g );
	return g;
}

void PolygonGenerator::Update(const wxString& posX,const wxString& posY,const wxString& posZ,const wxString& width,
					const wxString& depth,const wxString& height,const wxString& nor_vecX,const wxString& nor_vecY,const wxString& nor_vecZ,
					const wxString& dir_vecX,const wxString& dir_vecY,const wxString& dir_vecZ,
					VX::SG::GeometryBVH* g)
{
	load_inner_r( posX,posY,posZ,width,
			depth,height,nor_vecX,nor_vecY,nor_vecZ,
			dir_vecX,dir_vecY,dir_vecZ, g );
}

void PolygonGenerator::load_inner_r(const wxString& posX,const wxString& posY,const wxString& posZ,const wxString& width,
					const wxString& depth,const wxString& height,const wxString& nor_vecX,const wxString& nor_vecY,const wxString& nor_vecZ,
					const wxString& dir_vecX,const wxString& dir_vecY,const wxString& dir_vecZ,
					VX::SG::GeometryBVH* g //io
										)
{
	using namespace VX::Math;
	using namespace VX::SG;

	Geometry::VertexFormat vtx;
	std::vector<Geometry::VertexFormat> vertices, cross;
    //size_t i = 0;

	//point
	Geometry::VertexFormat center;
	Geometry::VertexFormat nor_vec;
	Geometry::VertexFormat dir_vec;
	Geometry::VertexFormat cross_vec;


	float pos_x = atof(posX);
	float pos_y = atof(posY);
	float pos_z = atof(posZ);
	float width_r = atof(width);
	float depth_r = atof(depth);
	float height_r = atof(height);
	float nor_x = atof(nor_vecX);
	float nor_y = atof(nor_vecY);
	float nor_z = atof(nor_vecZ);
	float dir_x = atof(dir_vecX);
	float dir_y = atof(dir_vecY);
	float dir_z = atof(dir_vecZ);


	//input_data
	center.pos.x = pos_x;
	center.pos.y = pos_y;
	center.pos.z = pos_z;

	nor_vec.pos.x = nor_x;
	nor_vec.pos.y = nor_y;
	nor_vec.pos.z = nor_z;

	float nor_norm = sqrt(nor_vec.pos.x * nor_vec.pos.x +
							nor_vec.pos.y * nor_vec.pos.y + 
							nor_vec.pos.z * nor_vec.pos.z);

	dir_vec.pos.x = dir_x;
	dir_vec.pos.y = dir_y;
	dir_vec.pos.z = dir_z;

	float dir_norm = sqrt(dir_vec.pos.x * dir_vec.pos.x +
						dir_vec.pos.y * dir_vec.pos.y + 
						dir_vec.pos.z * dir_vec.pos.z);

	cross_vec.pos.x = nor_y * dir_z - nor_z * dir_y;
	cross_vec.pos.y = nor_z * dir_x - nor_x * dir_z;
	cross_vec.pos.z = nor_x * dir_y - nor_y * dir_x;

	float cross_norm = sqrt(cross_vec.pos.x * cross_vec.pos.x +
						cross_vec.pos.y * cross_vec.pos.y + 
						cross_vec.pos.z * cross_vec.pos.z);

	static const u32 color = VX::initColor;

	//output_data

	Geometry::VertexFormat tmp1_vtx[8],tmp2_vtx[8],tmp_vtx[8];
	//頂点1
	/*
	tmp_vtx[0].pos.x = center.pos.x + width_r / 2;
	tmp_vtx[0].pos.y = center.pos.y + (-1) * depth_r / 2;
	tmp_vtx[0].pos.z = center.pos.z + height_r / 2;
	*/
	tmp1_vtx[0].pos = center.pos + ((height_r/2) / nor_norm) * nor_vec.pos ;
	tmp2_vtx[0].pos = tmp1_vtx[0].pos + (-1) * ((width_r/2) / dir_norm) * dir_vec.pos;
	tmp_vtx[0].pos = tmp2_vtx[0].pos + ((depth_r/2) / cross_norm) * cross_vec.pos;

	//頂点2
	/*
	tmp_vtx[1].pos.x = center.pos.x + width_r / 2;
	tmp_vtx[1].pos.y = center.pos.y + depth_r / 2;
	tmp_vtx[1].pos.z = center.pos.z + height_r / 2;
	*/
	tmp1_vtx[1].pos = center.pos + ((height_r/2) / nor_norm) * nor_vec.pos ;
	tmp2_vtx[1].pos = tmp1_vtx[1].pos + ((width_r/2) / dir_norm) * dir_vec.pos;
	tmp_vtx[1].pos = tmp2_vtx[1].pos + ((depth_r/2) / cross_norm) * cross_vec.pos;


	//頂点3
	/*
	tmp_vtx[2].pos.x = center.pos.x + width_r / 2;
	tmp_vtx[2].pos.y = center.pos.y + depth_r / 2;
	tmp_vtx[2].pos.z = center.pos.z + (-1) * height_r / 2;
	*/
	tmp1_vtx[2].pos = center.pos + ((height_r/2) / nor_norm) * nor_vec.pos ;
	tmp2_vtx[2].pos = tmp1_vtx[2].pos + ((width_r/2) / dir_norm) * dir_vec.pos;
	tmp_vtx[2].pos = tmp2_vtx[2].pos + (-1) * ((depth_r/2) / cross_norm) * cross_vec.pos;

	//頂点4
	/*
	tmp_vtx[3].pos.x = center.pos.x + width_r / 2;
	tmp_vtx[3].pos.y = center.pos.y + (-1) * depth_r / 2;
	tmp_vtx[3].pos.z = center.pos.z + (-1) * height_r / 2;
	*/
	tmp1_vtx[3].pos = center.pos + ((height_r/2) / nor_norm) * nor_vec.pos ;
	tmp2_vtx[3].pos = tmp1_vtx[3].pos + (-1) * ((width_r/2) / dir_norm) * dir_vec.pos;
	tmp_vtx[3].pos = tmp2_vtx[3].pos + (-1) * ((depth_r/2) / cross_norm) * cross_vec.pos;

	//頂点5
	/*
	tmp_vtx[4].pos.x = center.pos.x + (-1) * width_r / 2;
	tmp_vtx[4].pos.y = center.pos.y + (-1) * depth_r / 2;
	tmp_vtx[4].pos.z = center.pos.z + height_r / 2;
	*/
	tmp1_vtx[4].pos = center.pos + (-1) * ((height_r/2) / nor_norm) * nor_vec.pos ;
	tmp2_vtx[4].pos = tmp1_vtx[4].pos + (-1) * ((width_r/2) / dir_norm) * dir_vec.pos;
	tmp_vtx[4].pos = tmp2_vtx[4].pos + ((depth_r/2) / cross_norm) * cross_vec.pos;

	//頂点6
	/*
	tmp_vtx[5].pos.x = center.pos.x + (-1) * width_r / 2;
	tmp_vtx[5].pos.y = center.pos.y + depth_r / 2;
	tmp_vtx[5].pos.z = center.pos.z + height_r / 2;
	*/
	tmp1_vtx[5].pos = center.pos + (-1) * ((height_r/2) / nor_norm) * nor_vec.pos ;
	tmp2_vtx[5].pos = tmp1_vtx[5].pos + ((width_r/2) / dir_norm) * dir_vec.pos;
	tmp_vtx[5].pos = tmp2_vtx[5].pos + ((depth_r/2) / cross_norm) * cross_vec.pos;

	//頂点7
	/*
	tmp_vtx[6].pos.x = center.pos.x + (-1) * width_r / 2;
	tmp_vtx[6].pos.y = center.pos.y + depth_r / 2;
	tmp_vtx[6].pos.z = center.pos.z + (-1) * height_r / 2;
	*/
	tmp1_vtx[6].pos = center.pos + (-1) * ((height_r/2) / nor_norm) * nor_vec.pos ;
	tmp2_vtx[6].pos = tmp1_vtx[6].pos + ((width_r/2) / dir_norm) * dir_vec.pos;
	tmp_vtx[6].pos = tmp2_vtx[6].pos + (-1) * ((depth_r/2) / cross_norm) * cross_vec.pos;

	//頂点8
	/*
	tmp_vtx[7].pos.x = center.pos.x + (-1) * width_r / 2;
	tmp_vtx[7].pos.y = center.pos.y + (-1) * depth_r / 2;
	tmp_vtx[7].pos.z = center.pos.z + (-1) * height_r / 2;
	*/
	tmp1_vtx[7].pos = center.pos + (-1) * ((height_r/2) / nor_norm) * nor_vec.pos ;
	tmp2_vtx[7].pos = tmp1_vtx[7].pos + (-1) * ((width_r/2) / dir_norm) * dir_vec.pos;
	tmp_vtx[7].pos = tmp2_vtx[7].pos + (-1) * ((depth_r/2) / cross_norm) * cross_vec.pos;

	//+x方向の生成
	//法線方向
	vtx.normal.x= dir_vec.pos.x;
	vtx.normal.y= dir_vec.pos.y;
	vtx.normal.z= dir_vec.pos.z;


	vtx.pos = tmp_vtx[0].pos;
	vtx.col = color;
	vertices.push_back(vtx);	

	vtx.pos = tmp_vtx[3].pos;
	vtx.col = color;
	vertices.push_back(vtx);

	vtx.pos = tmp_vtx[2].pos;
	vtx.col = color;
	vertices.push_back(vtx);	

	vtx.normal.x= dir_vec.pos.x;
	vtx.normal.y= dir_vec.pos.y;
	vtx.normal.z= dir_vec.pos.z;

	vtx.pos = tmp_vtx[0].pos;
	vtx.col = color;
	vertices.push_back(vtx);	

	vtx.pos = tmp_vtx[2].pos;
	vtx.col = color;
	vertices.push_back(vtx);	

	vtx.pos = tmp_vtx[1].pos;
	vtx.col = color;
	vertices.push_back(vtx);

	//-x方向の生成
	//法線方向
	vtx.normal.x= (-1) * dir_vec.pos.x;
	vtx.normal.y= (-1) * dir_vec.pos.y;
	vtx.normal.z= (-1) * dir_vec.pos.z;

	vtx.pos = tmp_vtx[4].pos;
	vtx.col = color;
	vertices.push_back(vtx);	

	vtx.pos = tmp_vtx[5].pos;
	vtx.col = color;
	vertices.push_back(vtx);	

	vtx.pos = tmp_vtx[6].pos;
	vtx.col = color;
	vertices.push_back(vtx);	

	vtx.normal.x= (-1) * dir_vec.pos.x;
	vtx.normal.y= (-1) * dir_vec.pos.y;
	vtx.normal.z= (-1) * dir_vec.pos.z;

	vtx.pos = tmp_vtx[4].pos;
	vtx.col = color;
	vertices.push_back(vtx);	

	vtx.pos = tmp_vtx[6].pos;
	vtx.col = color;
	vertices.push_back(vtx);	

	vtx.pos = tmp_vtx[7].pos;
	vtx.col = color;
	vertices.push_back(vtx);

	//+y方向の生成
	//法線方向
	
	vtx.normal.x=cross_vec.pos.x;
	vtx.normal.y=cross_vec.pos.y;
	vtx.normal.z=cross_vec.pos.z;

	vtx.pos = tmp_vtx[1].pos;
	vtx.col = color;
	vertices.push_back(vtx);	

	vtx.pos = tmp_vtx[2].pos;
	vtx.col = color;
	vertices.push_back(vtx);	

	vtx.pos = tmp_vtx[5].pos;
	vtx.col = color;
	vertices.push_back(vtx);	

	vtx.normal.x=cross_vec.pos.x;
	vtx.normal.y=cross_vec.pos.y;
	vtx.normal.z=cross_vec.pos.z;

	vtx.pos = tmp_vtx[5].pos;
	vtx.col = color;
	vertices.push_back(vtx);	

	vtx.pos = tmp_vtx[2].pos;
	vtx.col = color;
	vertices.push_back(vtx);	

	vtx.pos = tmp_vtx[6].pos;
	vtx.col = color;
	vertices.push_back(vtx);

	//-y方向の生成
	//法線方向
	vtx.normal.x=(-1) * cross_vec.pos.x;
	vtx.normal.y=(-1) * cross_vec.pos.y;
	vtx.normal.z=(-1) * cross_vec.pos.z;

	vtx.pos = tmp_vtx[0].pos;
	vtx.col = color;
	vertices.push_back(vtx);	

	vtx.pos = tmp_vtx[4].pos;
	vtx.col = color;
	vertices.push_back(vtx);	

	vtx.pos = tmp_vtx[7].pos;
	vtx.col = color;
	vertices.push_back(vtx);	

	vtx.normal.x=(-1) * cross_vec.pos.x;
	vtx.normal.y=(-1) * cross_vec.pos.y;
	vtx.normal.z=(-1) * cross_vec.pos.z;

	vtx.pos = tmp_vtx[0].pos;
	vtx.col = color;
	vertices.push_back(vtx);	

	vtx.pos = tmp_vtx[7].pos;
	vtx.col = color;
	vertices.push_back(vtx);	

	vtx.pos = tmp_vtx[3].pos;
	vtx.col = color;
	vertices.push_back(vtx);

	//+z方向の生成
	//法線方向
	vtx.normal.x=nor_vec.pos.x;
	vtx.normal.y=nor_vec.pos.y;
	vtx.normal.z=nor_vec.pos.z;

	vtx.pos = tmp_vtx[0].pos;
	vtx.col = color;
	vertices.push_back(vtx);

	vtx.pos = tmp_vtx[1].pos;
	vtx.col = color;
	vertices.push_back(vtx);	

	vtx.pos = tmp_vtx[5].pos;
	vtx.col = color;
	vertices.push_back(vtx);	

	vtx.normal.x=nor_vec.pos.x;
	vtx.normal.y=nor_vec.pos.y;
	vtx.normal.z=nor_vec.pos.z;

	vtx.pos = tmp_vtx[5].pos;
	vtx.col = color;
	vertices.push_back(vtx);	

	vtx.pos = tmp_vtx[4].pos;
	vtx.col = color;
	vertices.push_back(vtx);	

	vtx.pos = tmp_vtx[0].pos;
	vtx.col = color;
	vertices.push_back(vtx);

	//-z方向の生成
	//法線方向
	vtx.normal.x=(-1) * nor_vec.pos.x;
	vtx.normal.y=(-1) * nor_vec.pos.y;
	vtx.normal.z=(-1) * nor_vec.pos.z;

	vtx.pos = tmp_vtx[7].pos;
	vtx.col = color;
	vertices.push_back(vtx);	

	vtx.pos = tmp_vtx[6].pos;
	vtx.col = color;
	vertices.push_back(vtx);	

	vtx.pos = tmp_vtx[2].pos;
	vtx.col = color;
	vertices.push_back(vtx);	

	vtx.normal.x=(-1) * nor_vec.pos.x;
	vtx.normal.y=(-1) * nor_vec.pos.y;
	vtx.normal.z=(-1) * nor_vec.pos.z;

	vtx.pos = tmp_vtx[7].pos;
	vtx.col = color;
	vertices.push_back(vtx);	

	vtx.pos = tmp_vtx[2].pos;
	vtx.col = color;
	vertices.push_back(vtx);	

	vtx.pos = tmp_vtx[3].pos;
	vtx.col = color;
	vertices.push_back(vtx);


	const u32 vertexCount = static_cast<u32>(vertices.size());
	const u32 indexCount  = vertexCount;
	
//	GeometryBVH* g = vxnew GeometryBVH();
	g->Alloc(vertexCount, indexCount);
	
	memcpy(g->GetVertex(), &vertices[0], sizeof(Geometry::VertexFormat) * vertexCount); // vertex copy
	vertices.clear();
	std::vector<Geometry::VertexFormat> size0;
	vertices.swap(size0); // free std::vector
	
	Index* index = vxnew Index[indexCount];
	for (u32 i = 0; i < indexCount; i++)
		index[i] = i;
	memcpy(g->GetIndex(), index, sizeof(Index) * indexCount); // index copy
	vxdeleteArray(index);
	
	g->CalcBounds();
	
}

VX::SG::Node* PolygonGenerator::Load(const wxString& posX,const wxString& posY,const wxString& posZ,const wxString& depth,
									const wxString& fanRad,const wxString& bossRad,const wxString& nor_vecX,const wxString& nor_vecY,
									const wxString& nor_vecZ)
{
	VX::SG::GeometryBVH* g = vxnew VX::SG::GeometryBVH();
	load_inner_c( posX, posY, posZ, depth, fanRad, bossRad, nor_vecX, nor_vecY, nor_vecZ, g );
	return g;
}

void PolygonGenerator::Update(const wxString& posX,const wxString& posY,const wxString& posZ,const wxString& depth,
		const wxString& fanRad,const wxString& bossRad,const wxString& nor_vecX,const wxString& nor_vecY,const wxString& nor_vecZ,
		VX::SG::GeometryBVH* g)
{
	load_inner_c( posX, posY, posZ, depth, fanRad, bossRad, nor_vecX, nor_vecY, nor_vecZ, g );
}

void PolygonGenerator::load_inner_c(const wxString& posX,const wxString& posY,const wxString& posZ,const wxString& depth,
		const wxString& fanRad,const wxString& bossRad,const wxString& nor_vecX,const wxString& nor_vecY,const wxString& nor_vecZ,
		VX::SG::GeometryBVH* g )
{
	using namespace VX::Math;
	using namespace VX::SG;

	Geometry::VertexFormat vtx;
	std::vector<Geometry::VertexFormat> vertices ,vtx_top_out ,vtx_top_in ,vtx_bottom_out ,vtx_bottom_in;
	
	//point
	Geometry::VertexFormat center;
	Geometry::VertexFormat nor_vec;
	Geometry::VertexFormat dir_vec;
	Geometry::VertexFormat p_start;
	

	float pos_x = atof(posX);
	float pos_y = atof(posY);
	float pos_z = atof(posZ);
	float depth_c = atof(depth);
	float fanRad_c = atof(fanRad);
	float bossRad_c = atof(bossRad);
	float nor_x = atof(nor_vecX);
	float nor_y = atof(nor_vecY);
	float nor_z = atof(nor_vecZ);

	
	//input_data
	center.pos.x = pos_x;
	center.pos.y = pos_y;
	center.pos.z = pos_z;

	nor_vec.pos.x = nor_x;
	nor_vec.pos.y = nor_y;
	nor_vec.pos.z = nor_z;

	float nor_norm = sqrt(nor_vec.pos.x * nor_vec.pos.x +
					nor_vec.pos.y * nor_vec.pos.y + 
					nor_vec.pos.z * nor_vec.pos.z);

	//Z軸と円筒軸方向の外積をとる
	
	if((nor_vec.pos.x==0.0F) && (nor_vec.pos.y==0.0F))
	{
		dir_vec.pos.x = 0;
		dir_vec.pos.y = (-1) * nor_vec.pos.z;
		dir_vec.pos.z = nor_vec.pos.y;
	}
	else{
		dir_vec.pos.x = (-1) * nor_vec.pos.y;
		dir_vec.pos.y = nor_vec.pos.x;
		dir_vec.pos.z = 0;
	}
//上面外側の始点p0を抽出

	float dir_norm = sqrt(dir_vec.pos.x * dir_vec.pos.x +
						dir_vec.pos.y * dir_vec.pos.y + 
						dir_vec.pos.z * dir_vec.pos.z);

	//上面の中心ベクトル
	Geometry::VertexFormat center_top;
	center_top.pos = center.pos + ((depth_c / 2) / nor_norm) * nor_vec.pos;
	
	//p0ベクトル
	p_start.pos = center_top.pos + ( fanRad_c / dir_norm ) * dir_vec.pos;


	//回転マトリックス
	#define CYLYNDER_CIRCLE_SUM (36)

	double deg = 360 / CYLYNDER_CIRCLE_SUM ;     /* 角度（度数）*/
	double rad = deg * PI / 180;   /* ラジアン値に変換*/

		VX::Math::matrix4x4 rotMat = rotateAxis( nor_vec, rad);


	//p0周りを採取
	Geometry::VertexFormat next;

	for(int i=0; i< CYLYNDER_CIRCLE_SUM ; i++){
		if(i==0){
			vtx_top_out.push_back(p_start);
			continue;
		}

		next.pos = vtx_top_out.at(i - 1).pos - center_top.pos; 
		//１つ前の点を使い、マトリックスを使いまわす
		next = convert(rotMat, next);
		next.pos = next.pos + center_top.pos;
		//next.Multi(mat);
		//next.Plus(center_point);
		vtx_top_out.push_back(next);
	}

//上面内側の始点p0を抽出

	//上面の中心ベクトル
	center_top.pos = center.pos + ((depth_c / 2) / nor_norm) * nor_vec.pos;
	
	//p0ベクトル
	p_start.pos = center_top.pos + ( bossRad_c / dir_norm ) * dir_vec.pos;

	//p0周りを採取
	for(int i=0; i< CYLYNDER_CIRCLE_SUM ; i++){
		if(i==0){
			vtx_top_in.push_back(p_start);
			continue;
		}

		next.pos = vtx_top_in.at(i - 1).pos - center_top.pos; 
		//１つ前の点を使い、マトリックスを使いまわす
		next = convert(rotMat, next);
		next.pos = next.pos + center_top.pos;
		//next.Multi(mat);
		//next.Plus(center_point);
		vtx_top_in.push_back(next);
	}

//下面外側の頂点を設定（上面からマッピング）

	Geometry::VertexFormat bottom_out[CYLYNDER_CIRCLE_SUM];
	
	for(int i=0; i< CYLYNDER_CIRCLE_SUM ; i++){

		bottom_out[i].pos = vtx_top_out.at(i).pos + (-1) * depth_c * nor_vec.pos / nor_norm;
		vtx_bottom_out.push_back(bottom_out[i]);
	}


//下面内側の頂点を設定（上面からマッピング）

	Geometry::VertexFormat bottom_in[CYLYNDER_CIRCLE_SUM];
	
	for(int i=0; i< CYLYNDER_CIRCLE_SUM ; i++){

		bottom_in[i].pos = vtx_top_in.at(i).pos + (-1) * depth_c * nor_vec.pos / nor_norm;
		vtx_bottom_in.push_back(bottom_in[i]);
	}

	
	static const u32 color = VX::initColor;

	//output_data

	//円筒上面の生成1

	for(int i=1; i< CYLYNDER_CIRCLE_SUM+1 ; i++){

		//法線方向
		vtx.normal= dir_vec.pos;

		vtx.pos = vtx_top_in.at(i-1).pos;
		vtx.col = color;
		vertices.push_back(vtx);	

		vtx.pos = vtx_top_out.at(i-1).pos;
		vtx.col = color;
		vertices.push_back(vtx);

		vtx.pos = vtx_top_in.at(((CYLYNDER_CIRCLE_SUM)+i)%((CYLYNDER_CIRCLE_SUM))).pos;
		vtx.col = color;
		vertices.push_back(vtx);	
	}

	//円筒上面の生成2

	for(int i=1; i< CYLYNDER_CIRCLE_SUM+1 ; i++){

		//法線方向
		vtx.normal= dir_vec.pos;

		vtx.pos = vtx_top_out.at(i-1).pos;
		vtx.col = color;
		vertices.push_back(vtx);	

		vtx.pos = vtx_top_out.at(((CYLYNDER_CIRCLE_SUM)+i)%((CYLYNDER_CIRCLE_SUM))).pos;
		vtx.col = color;
		vertices.push_back(vtx);

		vtx.pos = vtx_top_in.at(((CYLYNDER_CIRCLE_SUM)+i)%((CYLYNDER_CIRCLE_SUM))).pos;
		vtx.col = color;
		vertices.push_back(vtx);	
	}


	//円筒下面の生成1
	//Geometry::VertexFormat center_bottom;
	//center_bottom.pos = center_top.pos + (-1) * depth_c * nor_vec.pos / nor_norm;

	for(int i=1; i< CYLYNDER_CIRCLE_SUM+1 ; i++){
		//法線方向
		vtx.normal= (-1) *dir_vec.pos;

		vtx.pos = vtx_bottom_in.at(i-1).pos;
		vtx.col = color;
		vertices.push_back(vtx);	

		vtx.pos = vtx_bottom_out.at(i-1).pos;
		vtx.col = color;
		vertices.push_back(vtx);

		vtx.pos = vtx_bottom_in.at(((CYLYNDER_CIRCLE_SUM)+i)%((CYLYNDER_CIRCLE_SUM))).pos;
		vtx.col = color;
		vertices.push_back(vtx);
		
	}
	//円筒下面の生成2

	for(int i=1; i< CYLYNDER_CIRCLE_SUM+1 ; i++){

		//法線方向
		vtx.normal= (-1) * dir_vec.pos;

		vtx.pos = vtx_bottom_out.at(i-1).pos;
		vtx.col = color;
		vertices.push_back(vtx);	

		vtx.pos = vtx_bottom_out.at(((CYLYNDER_CIRCLE_SUM)+i)%((CYLYNDER_CIRCLE_SUM))).pos;
		vtx.col = color;
		vertices.push_back(vtx);

		vtx.pos = vtx_bottom_in.at(((CYLYNDER_CIRCLE_SUM)+i)%((CYLYNDER_CIRCLE_SUM))).pos;
		vtx.col = color;
		vertices.push_back(vtx);
	}

	//円筒外側面の生成1
	Geometry::VertexFormat tmp_vtx1,tmp_vtx2,tmp_vtx3,tmp_vec1,tmp_vec2;

	for(int i=1; i< CYLYNDER_CIRCLE_SUM+1 ; i++){
		//法線方向　外積計算

		tmp_vtx1.pos = vtx_top_out.at(i-1).pos;
		tmp_vtx2.pos = vtx_bottom_out.at(i-1).pos;
		tmp_vtx3.pos = vtx_top_out.at(((CYLYNDER_CIRCLE_SUM)+i)%((CYLYNDER_CIRCLE_SUM))).pos;

		tmp_vec1.pos = tmp_vtx2.pos-tmp_vtx1.pos;
		tmp_vec2.pos = tmp_vtx3.pos-tmp_vtx1.pos;

		vtx.normal.x = tmp_vec1.pos.y * tmp_vec2.pos.z - tmp_vec1.pos.z * tmp_vec2.pos.y;
		vtx.normal.y = tmp_vec1.pos.z * tmp_vec2.pos.x - tmp_vec1.pos.x * tmp_vec2.pos.z;
		vtx.normal.z = tmp_vec1.pos.x * tmp_vec2.pos.y - tmp_vec1.pos.y * tmp_vec2.pos.x;

		vtx.pos = tmp_vtx1.pos;
		vtx.col = color;
		vertices.push_back(vtx);
		
		vtx.pos = tmp_vtx2.pos;
		vtx.col = color;
		vertices.push_back(vtx);
				
		vtx.pos = tmp_vtx3.pos;
		vtx.col = color;
		vertices.push_back(vtx);	
	}

	//円筒外側面の生成2
	for(int i=1; i< CYLYNDER_CIRCLE_SUM+1 ; i++){
		//法線方向　外積計算

		tmp_vtx1.pos = vtx_bottom_out.at(i-1).pos;
		tmp_vtx2.pos = vtx_top_out.at(((CYLYNDER_CIRCLE_SUM)+i)%((CYLYNDER_CIRCLE_SUM))).pos;
		tmp_vtx3.pos = vtx_bottom_out.at(((CYLYNDER_CIRCLE_SUM)+i)%((CYLYNDER_CIRCLE_SUM))).pos;

		tmp_vec1.pos = tmp_vtx2.pos-tmp_vtx1.pos;
		tmp_vec2.pos = tmp_vtx3.pos-tmp_vtx1.pos;

		vtx.normal.x = tmp_vec1.pos.y * tmp_vec2.pos.z - tmp_vec1.pos.z * tmp_vec2.pos.y;
		vtx.normal.y = tmp_vec1.pos.z * tmp_vec2.pos.x - tmp_vec1.pos.x * tmp_vec2.pos.z;
		vtx.normal.z = tmp_vec1.pos.x * tmp_vec2.pos.y - tmp_vec1.pos.y * tmp_vec2.pos.x;

		vtx.pos = tmp_vtx1.pos;
		vtx.col = color;
		vertices.push_back(vtx);
		
		vtx.pos = tmp_vtx2.pos;
		vtx.col = color;
		vertices.push_back(vtx);
				
		vtx.pos = tmp_vtx3.pos;
		vtx.col = color;
		vertices.push_back(vtx);	
	}
	
	//円筒内側面の生成1
	for(int i=1; i< CYLYNDER_CIRCLE_SUM+1 ; i++){
		//法線方向　外積計算

		tmp_vtx1.pos = vtx_top_in.at(i-1).pos;
		tmp_vtx2.pos = vtx_bottom_in.at(i-1).pos;
		tmp_vtx3.pos = vtx_top_in.at(((CYLYNDER_CIRCLE_SUM)+i)%((CYLYNDER_CIRCLE_SUM))).pos;

		tmp_vec1.pos = tmp_vtx2.pos-tmp_vtx1.pos;
		tmp_vec2.pos = tmp_vtx3.pos-tmp_vtx1.pos;

		vtx.normal.x = tmp_vec1.pos.y * tmp_vec2.pos.z - tmp_vec1.pos.z * tmp_vec2.pos.y;
		vtx.normal.y = tmp_vec1.pos.z * tmp_vec2.pos.x - tmp_vec1.pos.x * tmp_vec2.pos.z;
		vtx.normal.z = tmp_vec1.pos.x * tmp_vec2.pos.y - tmp_vec1.pos.y * tmp_vec2.pos.x;

		vtx.pos = tmp_vtx1.pos;
		vtx.col = color;
		vertices.push_back(vtx);
		
		vtx.pos = tmp_vtx2.pos;
		vtx.col = color;
		vertices.push_back(vtx);
				
		vtx.pos = tmp_vtx3.pos;
		vtx.col = color;
		vertices.push_back(vtx);	
	}

	//円筒内側面の生成2
	for(int i=1; i< CYLYNDER_CIRCLE_SUM+1 ; i++){
		//法線方向　外積計算

		tmp_vtx1.pos = vtx_bottom_in.at(i-1).pos;
		tmp_vtx2.pos = vtx_top_in.at(((CYLYNDER_CIRCLE_SUM)+i)%((CYLYNDER_CIRCLE_SUM))).pos;
		tmp_vtx3.pos = vtx_bottom_in.at(((CYLYNDER_CIRCLE_SUM)+i)%((CYLYNDER_CIRCLE_SUM))).pos;

		tmp_vec1.pos = tmp_vtx2.pos-tmp_vtx1.pos;
		tmp_vec2.pos = tmp_vtx3.pos-tmp_vtx1.pos;

		vtx.normal.x = tmp_vec1.pos.y * tmp_vec2.pos.z - tmp_vec1.pos.z * tmp_vec2.pos.y;
		vtx.normal.y = tmp_vec1.pos.z * tmp_vec2.pos.x - tmp_vec1.pos.x * tmp_vec2.pos.z;
		vtx.normal.z = tmp_vec1.pos.x * tmp_vec2.pos.y - tmp_vec1.pos.y * tmp_vec2.pos.x;

		vtx.pos = tmp_vtx1.pos;
		vtx.col = color;
		vertices.push_back(vtx);
		
		vtx.pos = tmp_vtx2.pos;
		vtx.col = color;
		vertices.push_back(vtx);
				
		vtx.pos = tmp_vtx3.pos;
		vtx.col = color;
		vertices.push_back(vtx);	
	}


	const u32 vertexCount = static_cast<u32>(vertices.size());
	const u32 indexCount  = vertexCount;
	
//	GeometryBVH* g = vxnew GeometryBVH();
	g->Alloc(vertexCount, indexCount);
	
	memcpy(g->GetVertex(), &vertices[0], sizeof(Geometry::VertexFormat) * vertexCount); // vertex copy
	vertices.clear();
	std::vector<Geometry::VertexFormat> size0;
	vertices.swap(size0); // free std::vector
	
	Index* index = vxnew Index[indexCount];
	for (u32 i = 0; i < indexCount; i++)
		index[i] = i;
	memcpy(g->GetIndex(), index, sizeof(Index) * indexCount); // index copy
	vxdeleteArray(index);
	
	g->CalcBounds();
	
}

/**
 *	@brief	軸指定回転マトリックス計算
 *
 *	@param[in]	axis	軸ベクトル
 *	@param[in]	rad		回転角度(radian)
 *
 *	@retval	計算結果
 */
VX::Math::matrix4x4 PolygonGenerator::rotateAxis( const VX::SG::Geometry::VertexFormat axis, const float rad )
{
	VX::Math::matrix4x4 	result;
	VX::SG::Geometry::VertexFormat	normal = axis ;

	float normal_n = sqrt(normal.pos.x * normal.pos.x  + normal.pos.y * normal.pos.y +normal.pos.z * normal.pos.z);

	normal.pos = normal.pos / normal_n;

	float sin_val = sin( rad );
	float cos_val = cos( rad );

	float inv_cos_val = 1 - cos_val;

	float x = normal.pos[0];
	float y = normal.pos[1];
	float z = normal.pos[2];

	float xsq = x * x;
	float ysq = y * y;
	float zsq = z * z;

	result._11 = ( inv_cos_val * xsq ) + cos_val;
	result._12 = ( inv_cos_val * x * y ) - ( sin_val * z );
	result._13 = ( inv_cos_val * x * z ) + ( sin_val * y );
	result._14 = 0;

	result._21 = ( inv_cos_val * x * y ) + ( sin_val * z );
	result._22 = ( inv_cos_val * ysq ) + cos_val;
	result._23 = ( inv_cos_val * y * z ) - ( sin_val * x );
	result._24 = 0;

	result._31 = ( inv_cos_val * x * z ) - ( sin_val * y );
	result._32 = ( inv_cos_val * y * z ) + ( sin_val * x );
	result._33 = ( inv_cos_val * zsq ) + cos_val;
	result._34 = 0;

	result._41 = 0;
	result._42 = 0;
	result._43 = 0;
	result._44 = 1;

	return result;
};
/**
 *	@brief	座標変換
 *
 *	@param[in]	m	マトリックス
 *	@param[in]	v	座標値
 *
 *	@retval	変換結果
 */
VX::SG::Geometry::VertexFormat PolygonGenerator::convert( const VX::Math::matrix4x4& m, const VX::SG::Geometry::VertexFormat& v )
{
	float r[4] ;
	r[0] = m._11 * v.pos.x + m._12 * v.pos.y + m._13 * v.pos.z;
	r[1] = m._21 * v.pos.x + m._22 * v.pos.y + m._23 * v.pos.z;
	r[2] = m._31 * v.pos.x + m._32 * v.pos.y + m._33 * v.pos.z;
	r[3] = 0;

	VX::SG::Geometry::VertexFormat	result;

	result.pos[ 0] = r[ 0];
	result.pos[ 1] = r[ 1];
	result.pos[ 2] = r[ 2];

	return result;
};