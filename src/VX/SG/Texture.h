/*
 * VX/SG/Texture.h
 *
 * SceneGraph Library
 *
 */

#ifndef INCLUDE_VXSCENEGRAPH_TEXTURE_H
#define INCLUDE_VXSCENEGRAPH_TEXTURE_H

#include "../Type.h"
#include "../Math.h"
#include "Node.h"

#include <string>
#include <cstring>

namespace VX
{
	namespace SG
	{
		class Texture : public Node
		{
		public:
			static s32 MAX_SIZE;
			
			Texture(VX::Math::idx2 size = VX::Math::idx2(0, 0))
			 : Node(NODETYPE_TEXTURE)
			{
				m_img = NULL;
				if(size.x != 0 && size.y != 0){
					Resize(size);
				}
				m_needUpdate = false;
			}

			~Texture()
			{
				vxdeleteArray(m_img);
			}

			b8 Resize(VX::Math::idx2& size)
			{
				m_size = size;
				vxdeleteArray(m_img);
				
				m_img = vxnew u32[m_size.x * m_size.y];
				memset(m_img, 0, m_size.x * m_size.y * sizeof(u32));

				return true;
			}

			const u32* GetTexImage() const 
			{
				return m_img;
			}

			const VX::Math::idx2& GetSize() const
			{
				return m_size;
			}

			u32* GetImagePtr()
			{
				return m_img;
			}

			b8 GetSelection() const { return false; }
			void SetSelection(b8 select, b8 flagonly = false) { /* to do nothing */ }

			b8 GetNeedUpdate() const
			{
				return m_needUpdate;
			}
			
			void DisableNeedUpdate()
			{
				m_needUpdate = false;
			}
			void EnableNeedUpdate()
			{
				m_needUpdate = true;
			}

		protected:
			b8 m_needUpdate;

		private:
			VX::Math::idx2 m_size;
			u32 *m_img;
		};

	} // namespace SG

} // namespace VX


#endif // INCLUDE_VXSCENEGRAPH_TEXTURE_H

