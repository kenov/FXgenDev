#include "VoxelCartesianBlock.h"

namespace VX {
namespace SG {

/// @note bitVoxelSize = VOX::GetbitVoxelSize( ... )
VoxelCartesianBlock::VoxelCartesianBlock(
				//const VX::Math::idx3 &size, 
				//const VX::Math::idx3 &headIdx, 
				u8* rleBuf, const size_t rleSize, const size_t bitVoxelSize, const u8 bitWidth)
	// m_size(size), 
	//m_headIdx(headIdx), 
	:m_bitWidth(bitWidth)
{
	m_block = vxnew Block(rleBuf, rleSize, bitVoxelSize);
}

VoxelCartesianBlock::~VoxelCartesianBlock()
{
	vxdelete(m_block);
}


VOX::bitVoxelCell* VoxelCartesianBlock::GetBitVoxel() const
{
	return m_block->GetBitVoxel();
}
				
/*
const VX::Math::idx3& VoxelCartesianBlock::GetSize() const 
{ 
	return m_size; 
}

const VX::Math::idx3& VoxelCartesianBlock::GetHeadIdx() const 
{ 
	return m_headIdx;  
}
*/

const u8 VoxelCartesianBlock::GetBitWidth()const 
{ 
	return m_bitWidth; 
}

const unsigned char* VoxelCartesianBlock::GetRleBuf() const
{ 
	return m_block->GetRleBuf(); 
}

const size_t VoxelCartesianBlock::GetRleSize() const 
{ 
	return m_block->GetRleSize(); 
}
const size_t VoxelCartesianBlock::GetbitVoxelSize() const 
{ 
	return m_block->GetBitVoxelSize(); 
}



} // namespace SG
} // namespace VX



