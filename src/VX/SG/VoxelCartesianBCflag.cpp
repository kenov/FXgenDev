#include "VoxelCartesianBCflag.h"

#include "VoxelCartesianL.h"

#include "Node.h"
#include "Group.h"
#include "GeometryBVH.h"
#include "VoxGeometryUtil.h"
#include "VoxelCartesianUtil.h"
#include "VoxelCartesianBlock.h"

#include "MeshUtil.h"
#include "../Type.h"
#include "../Math.h"
#include <string>
#include <vector>
#include <limits.h>

namespace VX {
namespace SG {


float VoxelCartesianBCflag::m_bcflgTriOffsetScale = 0;

VoxelCartesianBCflag::VoxelCartesianBCflag(const VoxelCartesianL* local )
  : Node(NODETYPE_VOXEL_CARTESIAN_BCFLAG), m_local(local),m_geo(NULL)
{	
	const char* nName = "BCflag";
	SetName(nName);
	
}

VoxelCartesianBCflag::~VoxelCartesianBCflag()
{
	vxdelete(m_geo);
}

float VoxelCartesianBCflag::GetBCflgTriOffsetScale()
{
	assert(m_bcflgTriOffsetScale>=0 && m_bcflgTriOffsetScale<1);
	return m_bcflgTriOffsetScale;
}

void VoxelCartesianBCflag::SetBCflgTriOffsetScale(const float& scale)
{
	assert(scale>=0 && scale<1);
	m_bcflgTriOffsetScale = scale;
}

void VoxelCartesianBCflag::UnLoad()
{
	if( IsLoaded() ){
		vxdelete(m_geo);
		m_geo = NULL;
	}
	
}

bool VoxelCartesianBCflag::IsLoaded()const
{
	return (m_geo != NULL);
}

bool VoxelCartesianBCflag::isConstCellIDValueDef(s32& cellid)
{
	cellid = 0;
	cellid = m_local->GetConst_BCflagID();
	if(cellid !=-1){
		return true;
	}
	return false;
}
const VoxelCartesianL_Scan& VoxelCartesianBCflag::GetScanResult()const
{
	return m_bcf_scan_result;
}
void VoxelCartesianBCflag::Scan( VoxelCartesianL_Scan& result)
{
    //自分のスキャン結果
	m_bcf_scan_result.Clear();

    const VoxelCartesianBlock* bcflag = m_local->GetBlock_BCflagID();
    if(bcflag == NULL){
        return;
    }
        
    VOX::bitVoxelCell* bitVoxel = bcflag->GetBitVoxel();
    if(bitVoxel==NULL){
        return ;
    }
    const VX::Math::idx3 bSize3 = m_local->GetNumVoxels();

    VX::Math::idx3 bPos;
     
    BoundaryFlag t;
    for( s32 z=0; z< bSize3[2]; z++ ){
        for( s32 y=0; y< bSize3[1]; y++ ){
            for( s32 x=0; x< bSize3[0]; x++ ){
                bPos.x = x;
                bPos.y = y;
                bPos.z = z;
                    
                u32 id = GetBCflg(bitVoxel, bSize3, bPos);
                if(id==0){
                    continue;
                }
                    
                t.fromUInt32(id);
                    
                s8 v = 0;
                    
                v = t.BC_Face_T;
				m_bcf_scan_result.SetEnable(v);

                v = t.BC_Face_B;
                m_bcf_scan_result.SetEnable(v);

                v = t.BC_Face_N;
                m_bcf_scan_result.SetEnable(v);

                v = t.BC_Face_S;
                m_bcf_scan_result.SetEnable(v);

                v = t.BC_Face_E;
                m_bcf_scan_result.SetEnable(v);

                v = t.BC_Face_W;
                m_bcf_scan_result.SetEnable(v);
                    
            }
        }
    }
        
    delete [] bitVoxel; // !! Do not use vxnew !!

	//結果を呼び出し元に反映
	for( int i=0; i< m_bcf_scan_result.GetSize();i++){
		if(m_bcf_scan_result.IsEnable(i)){
			result.SetEnable(i);
		}
	}
}
    
void VoxelCartesianBCflag::genTris(const std::vector<u8>& BCflags , std::vector<BcTri>& triVec)
{

	if(BCflags.size()==0){
		return;
	}

	const VoxelCartesianBlock* bcflag = m_local->GetBlock_BCflagID();
	if(bcflag == NULL){
		return;
	}

	VOX::bitVoxelCell* bitVoxel = bcflag->GetBitVoxel();
	if(bitVoxel==NULL){
		return ;
	}
	const VX::Math::idx3 bSize3 = m_local->GetNumVoxels();
	const VX::Math::vec3 origin	= m_local->GetOrigin();
	const VX::Math::vec3 pitch	= m_local->GetPitch();


    
	VX::Math::idx3 bPos;
	const u8 bw = bcflag->GetBitWidth();

	//最大容量をリザーブ
	// ボクセル数×６面×２三角形
    size_t maxTri = bSize3[0]*bSize3[1]*bSize3[2];//*12;
	triVec.reserve(maxTri);
    
	BoundaryFlag bcdata;
	for( s32 z=0; z< bSize3[2]; z++ ){
		for( s32 y=0; y< bSize3[1]; y++ ){
			for( s32 x=0; x< bSize3[0]; x++ ){
				bPos.x = x;
				bPos.y = y;
				bPos.z = z;
				//ボクセル値を取得
				u32 id = GetBCflg(bitVoxel, bSize3, bPos);

				// idは圧縮値なのでBCFlagが読めるデータ構造に解凍する
				bcdata.fromUInt32(id);
				
				//三角形の存在を６面体でチェックし登録する
				addTriByEachVoxel( BCflags, bcdata ,origin, pitch, bPos,triVec);				
			}
		}
	}

	delete [] bitVoxel; // !! Do not use vxnew !!

}

b8 VoxelCartesianBCflag::GetSelection() const 
{ 
	if(m_geo!=NULL){
		return m_geo->GetSelection();
	}
	return false; 
}

void VoxelCartesianBCflag::SetSelection(b8 select, b8 flagonly) 
{ 
	if(m_geo!=NULL){
		m_geo->SetSelection(select,flagonly);
	}
}

void VoxelCartesianBCflag::addTriByEachVoxel(const std::vector<u8>& guiBCs,
												const BoundaryFlag& f ,
												const VX::Math::vec3& origin,
												const VX::Math::vec3& pitch,
												const VX::Math::idx3& bPos,
												std::vector<BcTri>& triVec)
{

	//除去ロジック
	bool hit = false;
	for(int i=0;i<guiBCs.size();i++){
		const u8& b = guiBCs[i];
		if(f.BC_Face_T == b || f.BC_Face_B == b || f.BC_Face_N == b ||
			f.BC_Face_S == b || f.BC_Face_E == b || f.BC_Face_W == b
			)
		{
			hit=true;
			break;
		}
	}

	if(!hit){
		//該当無し
		return;
	}

	//該当するので、ポリゴンの座標値を求める
	/*
	 　　　　　　　(上)
	                z  y(奥)
	                | /
	                |/
	                 -----> x(右)
                   

	            右ネジ
								p7---- p6
								|      |
				   				| North|   
					    		|      |
		         				p4 --- p5
			   /     /
              / TOP /
			p3---- p2
	    West|      |East                 
			|South |
			|      | /
	     	p0 --- p1
			 BOTTOM
	   
	*/
	// pitchの100分の１
	// 少し内側に寄せる
	static const f32 OFFSET_VALUE = GetBCflgTriOffsetScale();
	VX::Math::vec3 OFFSET = pitch*(OFFSET_VALUE);

	VX::Math::vec3 p0(	origin.x+(pitch.x*bPos.x),	origin.y+(pitch.y*bPos.y),	origin.z+(pitch.z*bPos.z));
	VX::Math::vec3 p1(p0.x + pitch.x,	p0.y,				p0.z);
	VX::Math::vec3 p2(p0.x + pitch.x,	p0.y,				p0.z + pitch.z);
	VX::Math::vec3 p3(p0.x,				p0.y,				p0.z + pitch.z);
	VX::Math::vec3 p4(p0.x,				p0.y + pitch.y,		p0.z);
	VX::Math::vec3 p5(p0.x + pitch.x,	p0.y + pitch.y,		p0.z);
	VX::Math::vec3 p6(p0.x + pitch.x,	p0.y + pitch.y,		p0.z + pitch.z);
	VX::Math::vec3 p7(p0.x,				p0.y + pitch.y,		p0.z + pitch.z);

	//offset 考慮した変更
	p0.x += OFFSET.x;	p0.y += OFFSET.y;	p0.z += OFFSET.z;
	p1.x -= OFFSET.x;	p1.y += OFFSET.y;	p1.z += OFFSET.z;
	p2.x -= OFFSET.x;	p2.y += OFFSET.y;	p2.z -= OFFSET.z;
	p3.x += OFFSET.x;	p3.y += OFFSET.y;	p3.z -= OFFSET.z;
	p4.x += OFFSET.x;	p4.y -= OFFSET.y;	p4.z += OFFSET.z;
	p5.x -= OFFSET.x;	p5.y -= OFFSET.y;	p5.z += OFFSET.z;
	p6.x -= OFFSET.x;	p6.y -= OFFSET.y;	p6.z -= OFFSET.z;
	p7.x += OFFSET.x;	p7.y -= OFFSET.y;	p7.z -= OFFSET.z;

	BcTri t;
	for(int i=0;i<guiBCs.size();i++){
		const u8& b = guiBCs.at(i);
		if(f.BC_Face_T == b){
			//top
			// p267,p273
			t.SetVal(p2,p6,p7,b);
			triVec.push_back(t);
			t.SetVal(p2,p7,p3,b);
			triVec.push_back(t);
		}
		if(f.BC_Face_B == b){
			//bottom
			// p045,051
			t.SetVal(p0,p4,p5,b);
			triVec.push_back(t);
			t.SetVal(p0,p5,p1,b);
			triVec.push_back(t);
		}
		if(f.BC_Face_N == b){
			//north
			//p476,p465
			t.SetVal(p4,p7,p6,b);
			triVec.push_back(t);
			t.SetVal(p4,p6,p5,b);
			triVec.push_back(t);
		}
		if(f.BC_Face_S == b){
			//south
			//p012,p023
			t.SetVal(p0,p1,p2,b);
			triVec.push_back(t);
			t.SetVal(p0,p2,p3,b);
			triVec.push_back(t);
		}
		if(f.BC_Face_E == b){
			//east
			//p156,p162
			t.SetVal(p1,p5,p6,b);
			triVec.push_back(t);
			t.SetVal(p1,p6,p2,b);
			triVec.push_back(t);
		}
		if(f.BC_Face_W == b){
			//west
			//p037,074
			t.SetVal(p0,p3,p7,b);
			triVec.push_back(t);
			t.SetVal(p0,p7,p4,b);
			triVec.push_back(t);
		}
	}
}

GeometryBVH* VoxelCartesianBCflag::createGeoByTri(const std::vector<BcTri>& triVec)
{


	//GLSLのTriを生成する
	GeometryBVH* g = vxnew GeometryBVH();
	
	using namespace VX::Math;
	using namespace VX::SG;
		
	const u32 triCount = (u32)triVec.size();
	const u32 vertexCount = triCount * 3;
	const u32 indexCount  = triCount * 3;

	// alloc and store
	const char* nName = "BCflag_geo";
	g->SetName(nName);
	g->Alloc(vertexCount, indexCount);
	Geometry::VertexFormat* vertex = g->GetVertex();
	Index* index = g->GetIndex();
	
	Geometry::VertexFormat* vertexptr = vertex;
	u32* idb = g->GetIDBuffer();
	for (u32 t = 0; t < triCount; t++)
	{
		const BcTri& tri = triVec[t];
		const vec3& v0 = tri.v0;
		const vec3& v1 = tri.v1;
		const vec3& v2 = tri.v2;
		const vec3 N  = normalize(cross(v2 - v0, v1 - v0));

		// id
		u16 idx = tri.mBcflag;
		if (idx > 32) // invalid ID
			idx = 0;
		//medimum値をインデックスバッファに格納
		idb[t] = (idx<<8);

		static const u32 initCol = 0xFF000000;

		vertexptr->pos.x =  v0.x;
		vertexptr->pos.y =  v0.y;
		vertexptr->pos.z =  v0.z;
		vertexptr->col = initCol;
		vertexptr->normal.x = N[0];
		vertexptr->normal.y = N[1];
		vertexptr->normal.z = N[2];
		vertexptr++;

		vertexptr->pos.x =  v1.x;
		vertexptr->pos.y =  v1.y;
		vertexptr->pos.z =  v1.z;
		vertexptr->col = initCol;
		vertexptr->normal.x = N[0];
		vertexptr->normal.y = N[1];
		vertexptr->normal.z = N[2];
		vertexptr++;

		vertexptr->pos.x =  v2.x;
		vertexptr->pos.y =  v2.y;
		vertexptr->pos.z =  v2.z;
		vertexptr->col = initCol;
		vertexptr->normal.x = N[0];
		vertexptr->normal.y = N[1];
		vertexptr->normal.z = N[2];
		vertexptr++;
		
	}

	for (u32 i = 0; i < indexCount; i++)
	{
		index[i] = i;
	}

	g->CalcBounds();
	
	VXLogD("Loaded BCflag: Triangle=%d\n", triCount);
	
	g->Build();// Build BVH
	
	return g;
}

/**
*	@brief GUIでローカルがロードにチェックが入っている
*/
void VoxelCartesianBCflag::ChangeShowBCFlag(const bool& islocal_load, const bool& islocal_visible, const std::vector<u8>& BCflags)
{
	//前回分を破棄
	UnLoad();
	//ステージングされて、かつ、ロード済みであれば電波する
	if(islocal_load && islocal_visible && BCflags.size()>0){
		Load(BCflags);
	}
}

void VoxelCartesianBCflag::Load(const std::vector<u8>& BCflags)
{
	assert(!IsLoaded());

	//ボクセルからTriを取得する
	std::vector<BcTri> triVec;
	genTris(BCflags ,triVec);

	VXLogI("tri sim=%d\n",triVec.size());

	m_geo = createGeoByTri(triVec);
	m_geo->EnableNeedUpdate();
}

} // namespace SG
} // namespace VX



