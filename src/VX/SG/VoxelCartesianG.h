/*
 * VX/SG/CellIDVoxelCartesian.h
 *
 * SceneGraphLibrary
 *
 */

#ifndef INCLUDE_VXSCENEGRAPH_VoxelCartesianG_H
#define INCLUDE_VXSCENEGRAPH_VoxelCartesianG_H

#include "../Type.h"
#include "../Math.h"

#include "Node.h"
#include "Group.h"
#include "Geometry.h"

#include "MeshUtil.h"

#include <string>
#include <vector>

namespace VX
{
	namespace SG
	{

		class VoxelCartesianSliceG;
		class VoxelCartesianBlock;
		class VoxelCartesianL;
		class VoxelCartesianC;

		class VoxelCartesianG : public Group
		{
		public:
			
			VoxelCartesianG( const VX::Math::vec3& origin,
			               const VX::Math::vec3& region,
						   const VX::Math::idx3& voxSize,
						   const VX::Math::idx3& divs,
						   std::vector<VoxelCartesianL*>& locals,
						   const u32& subdomainbytesize
						   );

			~VoxelCartesianG();

			const std::string& GetCidDir() const;
			const std::string& GetBcfDir() const;
			const VX::Math::idx3& GetNumVoxels() const;
			const VX::Math::idx3& GetNumDivs() const;
			const VX::Math::vec3& GetOrigin() const;
			const VX::Math::vec3& GetRegion() const;
			const VX::Math::vec3& GetMin() const;	
			const VX::Math::vec3 GetMax() const;		
			const VX::Math::vec3& GetPitch() const;
			

			VoxelCartesianL* GetCrossPlaneActiveLocal();
			void SetCrossPlaneActiveLocal(VoxelCartesianL* local);

			const std::vector<NodeRefPtr<VoxelCartesianL> >& GetLocals() const;

			void GetBlocksOnSlice(const u32 axis, const size_t idx, std::vector<const VoxelCartesianL*> &blockList) const;
			void SetSlicePosition(const u32 axis, const f32 pos, const u32 index_coord, const b8 boundaryOnly);
			f32 GetSlicePosition(const u32 axis) const ;
			// 座標値からインデックス番号を取得する
			u32 GetIdxPosition(const u32 axis, const f32 coord_pos) const;
			// インデックス番号から座標値を取得する
			f32 GetCoordPosition(const u32 axis, const u32 idx_pos) const;
			b8 GetBBoxMinMax(VX::Math::vec3& minval, VX::Math::vec3& maxval) const;

			// サブドメインのファイルのサイズ
			u32 GetOneSubDomainByteSize() const;
			void SetOneSubDomainByteSize(const u32& size);

			b8 IsShowTreeNode()const;
			void SetShowTreeNode(const b8& show);


			const VoxelCartesianC* GetClipNode()const;
			VoxelCartesianC* GetClipNode();

			const Group* GetGlobalGroup() const;
			Group* GetGlobalGroup();
			const Group* GetLocalGroup() const;
			Group* GetLocalGroup();

			//GUIで表示するBCFlagの選択値
			void ChangeShowBCFlag(const std::vector<u8>& BCflags);
			void RepaintSlicePlane();
		private:
			void setDispName();
			//void AddRegionScopeGeometry(const VX::Math::vec3& origin, const VX::Math::vec3& region);
			void createClipNode();

		private:
			VX::Math::vec3 m_org;
			VX::Math::vec3 m_rgn;
			VX::Math::vec3 m_pitch;
			VX::Math::idx3 m_voxSize;
			VX::Math::idx3 m_divs;



			std::vector<NodeRefPtr<VoxelCartesianL> > m_locals;
			VoxelCartesianL* m_CrossPlaneActiveLocal;

			NodeRefPtr<VoxelCartesianSliceG> m_slices[3];

			u64 m_numUseBlockMemory;
			u32 m_one_subdomainbytesize;
			b8  m_isShowTreeNode;

			NodeRefPtr<VoxelCartesianC> m_clipNode;

			NodeRefPtr<Group> m_localGroup;
			NodeRefPtr<Group> m_globalGroup;

		};

	} // namespace SG

} // namespace VX

#endif // INCLUDE_VXSCENEGRAPH_VoxelCartesianG_H

