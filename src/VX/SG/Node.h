/*
 * VX/SG/Node.h
 *
 * SceneGraph Library
 *
 */

#ifndef INCLUDE_VXSCENEGRAPH_NODE_H
#define INCLUDE_VXSCENEGRAPH_NODE_H

#include "../VX.h"
#include "NodeType.h"

#include <string>
#include <assert.h>

namespace VX
{
	namespace SG
	{
		class Node;
		class RefPtr
		{
		protected:
			RefPtr()           { m_ref = 0; }
			virtual ~RefPtr()  {};
		public:
			s32 Ref()    const { return ++m_ref; }
			s32 GetRef() const { return m_ref;   }
			s32 Unref()  const {
				const s32 ref = --m_ref;
				assert(ref>=0);
				
				if (!ref)
					vxdelete(reinterpret_cast<Node*>(const_cast<RefPtr*>(this)));
				return ref;
			}
			
		private:
			mutable s32 m_ref;
		};
		
		class Node : public RefPtr
		{
		protected:
			Node(const NODETYPE type) : m_type(type), m_visible(true), m_intermediate(false), RefPtr()
			{
				m_parent = NULL;
				m_diapha = false;
			}
		public:
			virtual ~Node(){}

			NODETYPE GetType() const { return m_type; };
			void SetName(const std::string& name)
			{
				m_name = name;
			}
			const std::string& GetName() const
			{
				return m_name;
			}
			void SetParent(Node* parent)
			{
				m_parent = parent;
			}
			Node* GetParent() const
			{
				return m_parent;
			}
			void SetVisible(b8 visible)
			{
				m_visible = visible;
			}
			b8 GetVisible() const
			{
				return m_visible;
			}
			void SetIntermediate(b8 intermediate)
			{
				m_intermediate = intermediate;
			}
			b8 GetIntermediate() const
			{
				return m_intermediate;
			}
			void SetDiapha(b8 diapha)
			{
				m_diapha = diapha;
			}
			b8 GetDiapha() const
			{
				return m_diapha;
			}
			virtual b8 GetSelection() const = 0;
			virtual void SetSelection(b8 select, b8 flagonly = false) = 0;

		private:
			const NODETYPE m_type;
			Node* m_parent;
			std::string m_name;
			b8 m_visible;
			b8 m_intermediate;
			b8 m_diapha;
		};
		
		template<class T>
		class NodeRefPtr
		{
		public:
			NodeRefPtr()
			{
				m_p = 0;
			}
			NodeRefPtr(T* s)
			{
				m_p = s;
				if (m_p)
					m_p->Ref();
			}
			explicit NodeRefPtr(const NodeRefPtr& p)
			{
				m_p = p;
				if (m_p)
					m_p->Ref();
			}
			~NodeRefPtr()
			{
				if (m_p)
					m_p->Unref();
			}
			
			T* Get() const
			{
				return m_p;
			}
			
			operator T*() const
			{
				return m_p;
			}
			T& operator *() const
			{
				assert(m_p);
				return *m_p;
			}
			T* operator->() const
			{
				assert(m_p);
				return m_p;
			}
			bool operator !() const
			{
				return m_p == 0;
			}
			bool operator ==(T* p) const
			{
				return m_p == p;
			}
			bool operator !=(T* p) const
			{
				return m_p != p;
			}
			bool operator <(T* p) const // for STL container
			{
				return m_p < p;
			}
				
			// equal operator
			T* operator =(T* p)
			{
				if (p)
					p->Ref();
				if (m_p)
					m_p->Unref();
				m_p = p;
				return m_p;
			}
				
			const NodeRefPtr& operator =(const NodeRefPtr& r)
			{
				if (this->m_p == r.m_p)
					return *this;
				
				if (r.m_p)
					r.m_p->Ref();
				if (m_p)
					m_p->Unref();
				m_p = r.m_p;
				
				return *this;
			}
			
		private:
			T* m_p;
		};
		
	} // namespace SG
    
} // namespace VX

#endif // INCLUDE_VXSCENEGRAPH_NODE_H
