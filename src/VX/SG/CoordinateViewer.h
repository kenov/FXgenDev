//
//  CoordinateViewer.h
//

#ifndef _COORDINATE_VIEWER_H
#define _COORDINATE_VIEWER_H

//#include "../Type.h"
//#include "BaseLoader.h"


#include "../../VX/Type.h"
#include "../../FileIO/BaseLoader.h"
#include <wx/string.h>

namespace VX {
	namespace SG{
		class Node;
		class GeometryBVH;
	}
}

class PolygonGenerator
{
public:
	PolygonGenerator();
	~PolygonGenerator();
	
	void Update(const wxString& posX_p, const wxString& posY_p,const wxString& posZ_p,const wxString& size_p,VX::SG::GeometryBVH* g/*IO*/);
	void Update(const wxString& posX,const wxString& posY,const wxString& posZ,const wxString& width,const wxString& depth,
					const wxString& height,const wxString& nor_vecX,const wxString& nor_vecY,const wxString& nor_vecZ,
					const wxString& dir_vecX,const wxString& dir_vecY,const wxString& dir_vecZ,VX::SG::GeometryBVH* g/*IO*/);
	void Update(const wxString& posX,const wxString& posY,const wxString& posZ,const wxString& depth,
		const wxString& fanRad,const wxString& bossRad,const wxString& nor_vecX,const wxString& nor_vecY,const wxString& nor_vecZ,
		VX::SG::GeometryBVH* g);

	VX::SG::Node* Load(const wxString& posX_p, const wxString& posY_p,const wxString& posZ_p,const wxString& size_p);
	VX::SG::Node* Load(const wxString& posX,const wxString& posY,const wxString& posZ,const wxString& width,const wxString& depth,
					const wxString& height,const wxString& nor_vecX,const wxString& nor_vecY,const wxString& nor_vecZ,
					const wxString& dir_vecX,const wxString& dir_vecY,const wxString& dir_vecZ);
	VX::SG::Node* Load(const wxString& posX,const wxString& posY,const wxString& posZ,const wxString& depth,const wxString& fanRad,
					const wxString& bossRad,const wxString& nor_vecX,const wxString& nor_vecY,const wxString& nor_vecZ);

	VX::Math::matrix4x4 rotateAxis( const VX::SG::Geometry::VertexFormat axis, const float rad );
	VX::SG::Geometry::VertexFormat convert( const VX::Math::matrix4x4& m, const VX::SG::Geometry::VertexFormat& v );

private:
	void load_inner_p(const wxString& posX_p, const wxString& posY_p,const wxString& posZ_p,const wxString& size_p,
					VX::SG::GeometryBVH* g //io
										);
	void load_inner_r(const wxString& posX,const wxString& posY,const wxString& posZ,const wxString& width,
					const wxString& depth,const wxString& height,const wxString& nor_vecX,const wxString& nor_vecY,const wxString& nor_vecZ,
					const wxString& dir_vecX,const wxString& dir_vecY,const wxString& dir_vecZ,
					VX::SG::GeometryBVH* g //io
										);
	void load_inner_c(const wxString& posX,const wxString& posY,const wxString& posZ,const wxString& depth,
					const wxString& fanRad,const wxString& bossRad,const wxString& nor_vecX,const wxString& nor_vecY,const wxString& nor_vecZ,
					VX::SG::GeometryBVH* g //io
										);


};
	
#endif // _COORDINATE_VIEWER_H