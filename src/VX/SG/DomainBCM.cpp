/*
 * VX/SG/DomainBCM.cpp
 *
 * SceneGraphLibrary
 *
 */

#include "DomainBCM.h"

#include "../../VOX/Node.h"
#include "../../VOX/BCMOctree.h"
#include "../../VOX/RootGrid.h"
#include "../../VOX/Partition.h"
#include "../../VOX/VOXUtil.h"
#include "VoxGeometryUtil.h"
#include <sstream>
#include <cmath>

#include <stdio.h>

namespace {
	const static u32 boundaryColor = 0xFF3F55FF;
	const static u32 lineColor     = 0xFF050505;

	inline u32 HSV2RGB(const f32 H, const f32 S, const f32 V)
	{
		s32 Hi;
		f32 f, p, q, t;

		Hi = static_cast<s32>( H / 60.0f) % 6;
		f  = ( H / 60.0f ) - static_cast<f32>(Hi);
		p  = V * (1.0f - S);
		q  = V * (1.0f - f * S);
		t  = V * (1.0f - (1.0f - f) * S);

		u32 R, G, B;

		switch(Hi){
			case 0:
				R = static_cast<u32>(V * 255.0f);
				G = static_cast<u32>(t * 255.0f);
				B = static_cast<u32>(p * 255.0f);
				break;
			case 1:
				R = static_cast<u32>(q * 255.0f);
				G = static_cast<u32>(V * 255.0f);
				B = static_cast<u32>(p * 255.0f);
				break;
			case 2:
				R = static_cast<u32>(p * 255.0f);
				G = static_cast<u32>(V * 255.0f);
				B = static_cast<u32>(t * 255.0f);
				break;
			case 3:
				R = static_cast<u32>(p * 255.0f);
				G = static_cast<u32>(q * 255.0f);
				B = static_cast<u32>(V * 255.0f);
				break;
			case 4:
				R = static_cast<u32>(t * 255.0f);
				G = static_cast<u32>(p * 255.0f);
				B = static_cast<u32>(V * 255.0f);
				break;
			case 5:
				R = static_cast<u32>(V * 255.0f);
				G = static_cast<u32>(p * 255.0f);
				B = static_cast<u32>(q * 255.0f);
				break;
			default:
				break;
		}
		u32 A = 255;
		return (((R & 255) << 0) | ((G & 255) << 8) | ((B & 255) << 16) | ((A & 255) << 24));
	}
	

	inline void GetPedigreeBBox(const VX::Math::vec3& rootOrigin, const VX::Math::vec3& rootRegion, const VOX::Pedigree& p,
	                     VX::Math::vec3& origin, VX::Math::vec3& region)
	{
		s32 lv = p.getLevel();
		VX::Math::vec3 ploc(static_cast<f32>(p.getX()), static_cast<f32>(p.getY()), static_cast<f32>(p.getZ()));

		region = VX::Math::vec3( rootRegion.x / (1 << lv), rootRegion.y / (1 << lv), rootRegion.z / (1 << lv) );
		origin = VX::Math::vec3( rootOrigin.x + region.x * ploc.x,
		                         rootOrigin.y + region.y * ploc.y,
								 rootOrigin.z + region.z * ploc.z );
	}

	class InformationCollector
	{
	public:
		explicit InformationCollector(u32 maxLevel)
		{
			VX::SG::DomainBCM::BCMOctreeInformation::Initialize(m_information, maxLevel);
		}
		
		void operator() (const VOX::Node* node)
		{
			m_information.totalNodeCount++;
			m_information.levels[node->getLevel()].nodeCount++;
			if (node->isLeafNode())
			{
				m_information.totalLeafCount++;
				m_information.levels[node->getLevel()].leafCount++;
			}
		}

		const VX::SG::DomainBCM::BCMOctreeInformation& GetResult() const { return m_information; }

	private:
		VX::SG::DomainBCM::BCMOctreeInformation m_information;
	};

	class DomainBCMDFSlice : public VX::SG::DistanceFieldSlice
	{
	public:
		DomainBCMDFSlice(u32 axis, VX::SG::ColorMap* colormap, const VX::SG::DomainBCM* domain)
			: VX::SG::DistanceFieldSlice(axis, colormap), m_domain(domain)
		{
		}

	protected:
		void GetClippingBBOX(VX::Math::vec3& min, VX::Math::vec3& max) const
		{
			using VX::Math::vec3;
			const vec3& org = m_domain->GetOrigin();
			const vec3& rgn = m_domain->GetRegion();
			min = org;
			max = org + rgn;
		}

	private:
		const VX::SG::DomainBCM* m_domain;
	};

	// look-up distance from subdivision margins
	f32 DistanceFromLevel(VX::SG::DomainBCM* domain, s32 level)
	{
		using std::min; using std::max;
		typedef std::vector<VX::SG::DomainBCM::SubdivisionMargin> Margins;
		Margins& margins = domain->GetSubdivisionMargins();
		if (margins.empty()) return 0;

		if (level == VX::SG::DomainBCM::SubdivisionMargin::MAX_LEVEL)
		{
			// choose minimum
			f32 ret = FLT_MAX;
			for (Margins::const_iterator i = margins.begin(); i != margins.end(); ++i)
			{
				ret = min(ret, i->margin);
			}
			return ret;
		}
		else
		{
			// choose maximum
			f32 ret = 0;
			for (Margins::const_iterator i = margins.begin(); i != margins.end(); ++i)
			{
				if (i->level == VX::SG::DomainBCM::SubdivisionMargin::MAX_LEVEL || level <= i->level)
				{
					ret = max(ret, i->margin);
				}
			}
			return ret;
		}
	}

} // namespace

namespace VX {
namespace SG {

DomainBCMSlice::DomainBCMSlice( const u32 axis, const DomainBCM* domain )
 : DomainSlice(NODETYPE_BBOX_DOMAIN_SLICE), m_domain(domain), m_axis(axis), m_selecting(false)
{
	const char* sliceName[3] = {"Slice X", "Slice Y", "Slice Z"};
	SetName(sliceName[m_axis]);

	m_maxLevel = domain->GetMaxLevel();
	
	const VOX::BCMOctree* octree = domain->GetOctree();
	const VOX::RootGrid* rootGrid = octree->getRootGrid();
	
	m_rootUVW = VX::Math::idx3(
	    axis == 0 ? rootGrid->getSizeY() : rootGrid->getSizeX(),
		axis == 2 ? rootGrid->getSizeY() : rootGrid->getSizeZ(),
		axis == 0 ? rootGrid->getSizeX() : axis == 1 ? rootGrid->getSizeY() : rootGrid->getSizeZ() );
	
	m_maxBlocks = static_cast<u32>(SearchBCMMaxBlockCount( m_axis, octree, m_maxLevel ));

	m_boundary = vxnew Geometry();
	m_boundary->SetUseVertexLineColor(true);
	m_boundary->SetDrawMode(Geometry::MODE_LINE);
	m_boundary->Alloc(4, 8);
	
	m_block = vxnew Geometry();
	m_block->SetUseVertexLineColor(true);
	m_block->SetDrawMode(Geometry::MODE_LINE);
	m_block->Alloc( 4 * m_maxBlocks, 8 * m_maxBlocks);

	m_face = vxnew Geometry();
	m_face->SetUseVertexLineColor(true);
	m_face->SetDrawMode(Geometry::MODE_POLYGON);
	m_face->Alloc( 4 * m_maxBlocks, 6 * m_maxBlocks);

	f32 pos = domain->GetOrigin()[m_axis] + domain->GetRegion()[m_axis] * 0.5f;
	SetPosition(pos, 0);
}

DomainBCMSlice::~DomainBCMSlice()
{

}



f32 DomainBCMSlice::GetPosition() const
{
	return m_boundary->GetVertex()[0].pos[m_axis];
}

void DomainBCMSlice::SetPosition( const f32 pos, const u32 index_coord, const b8 boundaryOnly )
{
	using namespace VX::Math;

	const vec3 gorg = m_domain->GetOrigin();
	const vec3 grgn = m_domain->GetRegion();
	const vec3 pitch = m_domain->GetPitch();

	if( pos < gorg[m_axis] || pos > (gorg[m_axis] + grgn[m_axis]) )
	{
		return;
	}

	
	const VOX::BCMOctree* octree   = m_domain->GetOctree();
	const VOX::RootGrid*  rootGrid = octree->getRootGrid();

	const idx3 rootDims( rootGrid->getSizeX(), rootGrid->getSizeY(), rootGrid->getSizeZ() );
	const u32  numIdx = m_rootUVW[2] * ( 1 << m_maxLevel );

	u32 posIdx = m_domain->GetIdxPosition(m_axis, pos);	
	//VXLogD("posIdx: %d gorg: %f grgn: %f pitch: %f pos: %f\n", posIdx, gorg[m_axis], grgn[m_axis], pitch[m_axis], pos);

	float position = pos;
	if(index_coord == 0){
		position = gorg[m_axis] + static_cast<f32>(posIdx) * pitch[m_axis] + pitch[m_axis] * .5f;
	}

	// Boundary
	{
		vec3 org = gorg;
		org[m_axis] = position;
		BoundarySetter(m_boundary->GetVertex(), m_boundary->GetIndex(), m_axis, org, grgn, boundaryColor, 0);
		m_boundary->EnableNeedUpdate();
	}
	if( boundaryOnly ){
		return;
	}

	// Max Number of Blocks within RootGrid
	const u32 numBlocksWR = 1 << m_maxLevel; // Max Number of Blocks within RootGrid
	const u32 rPos = posIdx / numBlocksWR;   // RootNode Position
	const u32 nPos = posIdx % numBlocksWR;   // Node Position within RootGrid
	
	std::vector<const VOX::Node*> leafNodes;
	leafNodes.reserve(m_maxBlocks);
	// Collect leafNodes which crossed SlicePosition from Octree
	for(s32 v = 0; v < m_rootUVW[1]; v++){
		for(s32 u = 0; u < m_rootUVW[0]; u++){
			u32 x = m_axis == 0 ? rPos : u;
			u32 y = m_axis == 1 ? rPos : m_axis == 0 ? u : v;
			u32 z = m_axis == 2 ? rPos : v;
			const VOX::Node* root = octree->getRootNode(rootGrid->index2rootID(x, y, z));
			GetBCMNodesOnPlane(m_axis, root, nPos, m_maxLevel, leafNodes);
		}
	}
	//VXLogD("NumBlocks[%d] : %ld m_maxBlocks : %ld\n", m_axis, leafNodes.size(), m_maxBlocks);
	
	vec3 rootRgn = vec3( grgn.x / f32(rootDims.x), grgn.y / f32(rootDims.y), grgn.z / f32(rootDims.z) );

	Geometry::VertexFormat* bvtx = m_block->GetVertex();
	Geometry::VertexFormat* fvtx = m_face->GetVertex();
	Index *bidx = m_block->GetIndex();
	Index *fidx = m_face->GetIndex();

	VOX::Partition partition(m_domain->GetNumDivs(), octree->getNumLeafNode());

	m_block->SetIndexCount( static_cast<u32>(leafNodes.size() * 8) );
	m_face->SetIndexCount( static_cast<u32>(leafNodes.size() * 6) );

	for(s32 i = 0; i < (s32)(leafNodes.size()); i++){
		s32 id = leafNodes[i]->getBlockID();
		const VOX::Pedigree p = leafNodes[i]->getPedigree();

		vec3 rootIdx( f32(rootGrid->rootID2indexX(p.getRootID())), 
			          f32(rootGrid->rootID2indexY(p.getRootID())), 
					  f32(rootGrid->rootID2indexZ(p.getRootID())) );

		vec3 rootOrg( gorg.x + rootRgn.x * rootIdx.x,
		              gorg.y + rootRgn.y * rootIdx.y,
					  gorg.z + rootRgn.z * rootIdx.z );

		vec3 org, rgn;
		GetPedigreeBBox(rootOrg, rootRgn, p, org, rgn);
		org[m_axis] = position;
		BoundarySetter(&bvtx[i * 4], &bidx[i * 8], m_axis, org, rgn, lineColor, (u32)(i * 4));
		FaceSetter(&fvtx[i * 4], &fidx[i * 6], m_axis, org, rgn, m_domain->GetDivColor(partition.getRank(id)), (u32)(i * 4));

/*
#ifdef _DEBUG
		{
			u32 _color = m_domain->GetDivColor(partition.getRank(id));
			u8 r = (_color >> 0 ) & 255;
			u8 b = (_color >> 8 ) & 255;
			u8 g = (_color >> 16) & 255;
			u8 a = (_color >> 24) & 255;
			VXPrint("         R : %7d G : %7d B %7d A : %7d\n", r, g, b, a);
		}
#endif // _DEBUG
*/
	}


	m_block->EnableNeedUpdate();
	m_face->EnableNeedUpdate();
}

////////////////////////////////////////////////////////////////////////////////////////////////////

DomainBCM::DomainBCM(const std::string& name)
  : Domain(NODETYPE_BBOX_DOMAIN_BCM, name), m_octree(NULL), m_lvColorTbl(NULL), m_divColorTbl(NULL)
{
	m_slices[0] = NULL;
	m_slices[1] = NULL;
	m_slices[2] = NULL;

	ColorMap* cm = vxnew ColorMap();
	ColorMap::HSVRedToBlue(*cm);
	cm->EnableNeedUpdate();
	m_dfSlices[0] = vxnew DomainBCMDFSlice(0, cm, this);
	m_dfSlices[1] = vxnew DomainBCMDFSlice(1, cm, this);
	m_dfSlices[2] = vxnew DomainBCMDFSlice(2, cm, this);
	m_dfColorMap = cm;
	SetDistanceSliceRange(0, 0.01f);
	SetColorMapRangeLevels(0, SubdivisionMargin::MAX_LEVEL);
	
	m_org      = VX::Math::vec3(0.0f, 0.0f, 0.0f);
	m_rgn      = VX::Math::vec3(1.0f, 1.0f, 1.0f);
	m_rootDims = VX::Math::idx3(1, 1, 1);
	m_baseLevel    = 0;
	m_minLevel     = 0;
	m_blockSize    = VX::Math::idx3(1, 1, 1);
	m_ordering     = ORDER_Z;
	m_exportPolicy = EXP_PARAMETER;

	m_maxLevel = 0;

	m_useDistanceBasedMethod = false;
	m_subdivisionMargins.push_back(SubdivisionMargin(0, SubdivisionMargin::MAX_LEVEL));

	SetIntermediate(true);

	m_divs = 1;
	InitLvColorTable(m_lvColorTbl, 16);

	BCMOctreeInformation::Initialize(m_information, 0);
}

DomainBCM::DomainBCM(const VX::Math::vec3& org, const VX::Math::vec3& rgn, const VX::Math::idx3& rootDims, 
                     const std::vector<VOX::Pedigree>& pedigrees, const std::string& name)
 : Domain(NODETYPE_BBOX_DOMAIN_BCM, name), m_octree(NULL), m_lvColorTbl(NULL), m_divColorTbl(NULL)
{
	InitLvColorTable(m_lvColorTbl,  16);
	
	m_slices[0] = NULL;
	m_slices[1] = NULL;
	m_slices[2] = NULL;

	ColorMap* cm = vxnew ColorMap();
	ColorMap::Jet(*cm, true);
	cm->EnableNeedUpdate();
	m_dfSlices[0] = vxnew DomainBCMDFSlice(0, cm, this);
	m_dfSlices[1] = vxnew DomainBCMDFSlice(1, cm, this);
	m_dfSlices[2] = vxnew DomainBCMDFSlice(2, cm, this);
	m_dfColorMap = cm;
	SetDistanceSliceRange(0, 0.01f);

	m_baseLevel    = 0;
	m_minLevel     = 0;
	m_blockSize    = VX::Math::idx3(1, 1, 1);
	m_ordering     = ORDER_Z;
	m_exportPolicy = EXP_PARAMETER;

	m_org = org;
	m_rgn = rgn;
	m_rootDims = rootDims;
	
	m_maxLevel  = 0;
	
	m_useDistanceBasedMethod = false;
	m_subdivisionMargins.push_back(SubdivisionMargin(0, SubdivisionMargin::MAX_LEVEL));

	CreateOctree(pedigrees);

	SetIntermediate(true);
	
	m_divs = 1;
	InitLvColorTable(m_lvColorTbl, 16);
	
	BCMOctreeInformation::Initialize(m_information, 0);
}

DomainBCM::~DomainBCM()
{
	DeleteOctree();

	vxdelete(m_lvColorTbl);
	vxdelete(m_divColorTbl);
	
	ClearGeometryScopeList();
	ClearRegionScopeList();
}

// Init Color Table for Level Block
void DomainBCM::InitLvColorTable(u32* &colorTable, const u32 max)
{
	vxdeleteArray(colorTable);
	colorTable = vxnew u32[max];

	const static f32 PI = 3.1415926535f;
	for(u32 i = 0; i < max; i++){
		f32 H = (f32)(i) / (f32)(max);
		colorTable[i] = HSV2RGB(H * 360.0f, 1.0f, 0.7f);
	}
}

// Init Color Table for Parallel Divided Block
void DomainBCM::InitDivColorTable(u32* &colorTable, const u32 max)
{
	vxdeleteArray(colorTable);
	colorTable = vxnew u32[max];
	
	const static f32 PI = 3.1415926535f;
	const static u32 nh = 30;
	const static u32 ns = 100;
	const static u32 nv = 100;

	const f32 h_coff = 270.0f;
	const f32 s_coff = 0.7f;
	const f32 v_coff = 0.8f;

	if( max < nh ){
		for(u32 i = 0; i < max; i++){
			f32 H = (f32)(i) / (f32)(max);
			//colorTable[i] = HSV2RGB(H * h_coff, 0.3f * s_coff, 0.5f * v_coff);
			colorTable[i] = HSV2RGB(H * h_coff, s_coff, v_coff);
		}
	}else{
		for(u32 i = 0; i < max; i++){
			u32 ih = i % nh;
			u32 is = ns - ((i / nh) % ns);
			u32 iv = nv - ((i / nh) / nv);
			f32 h = ((f32)(ih) / (f32)(nh)) * h_coff;
			f32 s = ((f32)(is) / (f32)(ns)) * s_coff;
			f32 v = ((f32)(iv) / (f32)(nv)) * v_coff;
			colorTable[i] = HSV2RGB( h, s, v);
		}
	}
/*
#ifdef _DEBUG
	VXPrint("======== ColorTable =========\n");
	for(u32 i = 0; i < max; i++){
		VXPrint("[i %4d] R : %7d G : %7d B %7d A : %7d\n", i, 
		       (colorTable[i] >> 0 ) & 255, (colorTable[i] >> 8) & 255, (colorTable[i] >> 16) & 255, (colorTable[i] >> 24) & 255);
	
	}
	VXPrint("=============================\n");
#endif // _DEBUG
*/

}

b8 DomainBCM::SetNumDivs( const u32 divs )
{
	if( divs > static_cast<u32>(m_octree->getNumLeafNode()) )
	{
		return false;
	}

	m_divs = divs;
	InitDivColorTable(m_divColorTbl, m_divs);
	for(int i = 0; i < 3; i++){
		if(m_slices[i]){
			SetSlicePosition(i, GetSlicePosition(i), 1, false);
		}
	}
	return true;
}

u32 DomainBCM::GetNumBlocks() const
{
	return m_octree->getNumLeafNode();
}

b8 DomainBCM::GetPedigreeList(std::vector<VOX::Pedigree>& pedigrees) const
{
	if( !m_octree )
		return false;

	pedigrees.clear();

	const std::vector<VOX::Node*> nodes = m_octree->getLeafNodeArray();
	pedigrees.resize(nodes.size());

	for(size_t i = 0; i < nodes.size(); i++){
		pedigrees[i] = nodes[i]->getPedigree();
	}

	return true;
}

void DomainBCM::CreateTreeGeometry()
{
	using namespace std;
	using namespace VX::Math;

	vec3 rootRgn = vec3( m_rgn.x / f32(m_rootDims.x), m_rgn.y / f32(m_rootDims.y), m_rgn.z / f32(m_rootDims.z) );

	m_levelBoxes.clear();

	vector<VOX::Node*> ns = m_octree->getLeafNodeArray();
	vector< vector<VOX::Node*> > lnodes;
	lnodes.resize(m_maxLevel+1);
	for(vector<VOX::Node*>::iterator it = ns.begin(); it != ns.end(); ++it)
	{
		const VOX::Pedigree p = (*it)->getPedigree();
		int lv = (*it)->getLevel();
		lnodes[lv].push_back(*it);
	}

	const VOX::RootGrid* rootGrid = m_octree->getRootGrid();

	for(u32 l = 0; l < m_maxLevel+1; l++){
		size_t nb = static_cast<s32>(lnodes[l].size());

		char name[128];
		sprintf(name, "Level : %d (%ld blocks)", l, lnodes[l].size());

		Geometry* geo = vxnew Geometry(NODETYPE_BBOX_DOMAIN_BCM_GRID);
		geo->SetUseVertexLineColor(true);
		geo->SetDrawMode(Geometry::MODE_LINE);
		geo->Alloc(u32(nb * 8), u32(nb * 24));
		geo->SetName(name);
		geo->SetSelectable(true);
		geo->SetSelection(false);

		m_levelBoxes.push_back(geo);
		
		Geometry::VertexFormat *vertex = m_levelBoxes[l]->GetVertex();
		Index *index = m_levelBoxes[l]->GetIndex();
		for(s32 i = 0; i < nb; i++){
			const VOX::Pedigree p = lnodes[l][i]->getPedigree();
			vec3 rootIdx( f32(rootGrid->rootID2indexX(p.getRootID())), 
				          f32(rootGrid->rootID2indexY(p.getRootID())), 
						  f32(rootGrid->rootID2indexZ(p.getRootID())) );

			vec3 rootOrg( m_org.x + rootRgn.x * rootIdx.x,
			              m_org.y + rootRgn.y * rootIdx.y,
						  m_org.z + rootRgn.z * rootIdx.z );

			vec3 org, rgn;
			GetPedigreeBBox(rootOrg, rootRgn, p, org, rgn);
			
			Geometry::VertexFormat *vtx = &vertex[i * 8];
			Index *idx = &index[i * 24];
			
			BoxSetter(vtx, idx, org, rgn, m_lvColorTbl[l], u32(i * 8));
		}
		m_levelBoxes[l]->CalcBounds();
		m_levelBoxes[l]->EnableNeedUpdate();
	}

	for( int i = 0; i < 3; i++){
		m_slices[i] = vxnew DomainBCMSlice(i, this);
		m_dfSlices[i] = NULL;
	}
	m_dfColorMap = NULL;
}

// for create from leaf nodes
void DomainBCM::CreateOctree(const std::vector<VOX::Pedigree>& pedigrees)
{
	DeleteOctree();
	DeleteTreeGeometry();

	VOX::RootGrid* rootGrid = new VOX::RootGrid(m_rootDims.x, m_rootDims.y, m_rootDims.z); // !! DO NOT USE vxnew to create RootGrid  !!
	m_octree = new VOX::BCMOctree(rootGrid, pedigrees);                                    // !! DO NOT USE vxnew to create BCMOctree !!
	
	m_maxLevel = 0;
	const std::vector<VOX::Node*> &nodes = m_octree->getLeafNodeArray();
	for(std::vector<VOX::Node*>::const_iterator it = nodes.begin(); it != nodes.end(); ++it)
	{
		const VOX::Node* n = *it;
		m_maxLevel = m_maxLevel < static_cast<u32>(n->getLevel()) ? static_cast<u32>(n->getLevel()) : m_maxLevel;
	}

	m_pitch = VX::Math::vec3( m_rgn.x / static_cast<f32>(m_rootDims.x) / static_cast<f32>( 1 << m_maxLevel ),
                              m_rgn.y / static_cast<f32>(m_rootDims.y) / static_cast<f32>( 1 << m_maxLevel ),
                              m_rgn.z / static_cast<f32>(m_rootDims.z) / static_cast<f32>( 1 << m_maxLevel ) );

	InformationCollector collector(m_maxLevel);
	for (int i = 0; i < rootGrid->getSize(); ++i)
	{
		const VOX::Node* root = m_octree->getRootNode(i);
		VisitAllBCMNode(root, collector);
	}
	m_information = collector.GetResult();
	Memory::AddExternalSize(m_information.totalNodeCount * sizeof(VOX::Node)); // TODO

	m_divs = 1;
	InitDivColorTable(m_divColorTbl, m_divs);

	CreateTreeGeometry();
}

// for create by build octree
void DomainBCM::CreateOctree(VOX::Divider* divider)
{
	VXLogD("[Create Octree] BEGIN\n");
	DeleteOctree();
	DeleteTreeGeometry();
	
	VOX::RootGrid* rootGrid = new VOX::RootGrid(m_rootDims.x, m_rootDims.y, m_rootDims.z); // !! DO NOT USE vxnew to create RootGrid  !!

	// TODO Setting Ordering
	VOX::BCMOctree::Ordering ordering = m_ordering == ORDER_Z ? VOX::BCMOctree::Z : VOX::BCMOctree::HILBERT;
	VXPrint("  Build Octree       | BEGIN ");
	m_octree = new VOX::BCMOctree(rootGrid, divider, ordering);
	VXPrint("END |\n");
	
	VXPrint("  Search Max Level   | BEGIN ");
	m_maxLevel = 0;
	const std::vector<VOX::Node*> &nodes = m_octree->getLeafNodeArray();
	for(std::vector<VOX::Node*>::const_iterator it = nodes.begin(); it != nodes.end(); ++it)
	{
		const VOX::Node* n = *it;
		m_maxLevel = m_maxLevel < static_cast<u32>(n->getLevel()) ? static_cast<u32>(n->getLevel()) : m_maxLevel;
	}
	VXPrint("END |\n");

	m_pitch = VX::Math::vec3( m_rgn.x / static_cast<f32>(m_rootDims.x) / static_cast<f32>( 1 << m_maxLevel ),
                              m_rgn.y / static_cast<f32>(m_rootDims.y) / static_cast<f32>( 1 << m_maxLevel ),
                              m_rgn.z / static_cast<f32>(m_rootDims.z) / static_cast<f32>( 1 << m_maxLevel ) );
	VXPrint("  Collect Octree Information | BEGIN ");
	InformationCollector collector(m_maxLevel);
	for (int i = 0; i < rootGrid->getSize(); ++i)
	{
		const VOX::Node* root = m_octree->getRootNode(i);
		VisitAllBCMNode(root, collector);
	}
	m_information = collector.GetResult();
	Memory::AddExternalSize(m_information.totalNodeCount * sizeof(VOX::Node)); // TODO
	VXPrint("END |\n");

	m_divs = 1;
	InitDivColorTable(m_divColorTbl, m_divs);
	
	VXPrint("  Reset Geometry     | BEGIN ");
	CreateTreeGeometry();
	VXPrint("END |\n");
	VXLogD("[Create Octree] END\n");
}

void DomainBCM::DeleteOctree()
{
	if( !m_octree ) return;

	delete m_octree;
	m_octree = NULL;
	BCMOctreeInformation::Initialize(m_information, 0);

	Memory::SubExternalSize(m_information.totalNodeCount * sizeof(VOX::Node)); // TODO
	DeleteTreeGeometry();
	
	m_divs = 1;
	InitDivColorTable(m_divColorTbl, m_divs);
}

void DomainBCM::DeleteTreeGeometry()
{
	m_levelBoxes.clear();

	ColorMap* cm = vxnew ColorMap();
	ColorMap::Jet(*cm, true);
	cm->EnableNeedUpdate();
	cm->SetRangeMin(m_dfRangeMin);
	cm->SetRangeMax(m_dfRangeMax);
	m_dfColorMap = cm;

	for(int i = 0; i < 3; ++i) {
		m_slices[i] = NULL;
		m_dfSlices[i] = vxnew DomainBCMDFSlice(i, cm, this);
	}
}

void DomainBCM::SetDistanceSliceRange(f32 min, f32 max)
{
	if (max <= min)
	{
		max = min + 0.01f;
	}
	m_dfRangeMin = min;
	m_dfRangeMax = max;
	if (m_dfColorMap) {
		m_dfColorMap->SetRangeMin(m_dfRangeMin);
		m_dfColorMap->SetRangeMax(m_dfRangeMax);
	}
}

void DomainBCM::SetColorMapRangeLevels(s32 min, s32 max)
{
	m_dfRangeMinLevel = min;
	m_dfRangeMaxLevel = max;
	UpdateColorMapRange();
}

void DomainBCM::UpdateColorMapRange()
{
	SetDistanceSliceRange(DistanceFromLevel(this, m_dfRangeMaxLevel), DistanceFromLevel(this, m_dfRangeMinLevel));
}


u32 DomainBCM::ComputeMaxLevel() const
{
	u32 maxLevel = 0;
	maxLevel = GetMinLevel()  > maxLevel ? GetMinLevel()  : maxLevel;
	maxLevel = GetBaseLevel() > maxLevel ? GetBaseLevel() : maxLevel;
	const std::vector<GeometryScope*>& glist = GetGeometryScopeList();
	for(std::vector<GeometryScope*>::const_iterator it = glist.begin(); it != glist.end(); ++it)
	{
		maxLevel = (*it)->level > maxLevel ? (*it)->level : maxLevel;
	}
	const std::vector<RegionScope*>& rlist = GetRegionScopeList();
	for(std::vector<RegionScope*>::const_iterator it = rlist.begin(); it != rlist.end(); ++it)
	{
		maxLevel = (*it)->level > maxLevel ? (*it)->level : maxLevel;
	}

	return maxLevel;
}


} // namespace SG
} // namespace VX



