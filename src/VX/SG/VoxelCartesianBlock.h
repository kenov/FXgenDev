/*
 * VX/SG/CellIDVoxelCartesian.h
 *
 * SceneGraphLibrary
 *
 */

#ifndef INCLUDE_VoxelCartesianBlock_H
#define INCLUDE_VoxelCartesianBlock_H

#include "../Type.h"
#include "../Math.h"
#include "MeshUtil.h"

namespace VX
{
	namespace SG
	{

		class VoxelCartesianBlock
		{
		public:
			/// @note bitVoxelSize = VOX::GetbitVoxelSize( ... )
			VoxelCartesianBlock(
							//const VX::Math::idx3 &size, const VX::Math::idx3 &headIdx, 
				            u8* rleBuf, const size_t rleSize, const size_t bitVoxelSize, const u8 bitWidth);

			~VoxelCartesianBlock();


			VOX::bitVoxelCell* GetBitVoxel() const;
				
			//const VX::Math::idx3& GetSize()    const ;
			//const VX::Math::idx3& GetHeadIdx() const ;
			const u8 GetBitWidth()             const ;

			const unsigned char* GetRleBuf() const ;
			const size_t GetRleSize() const ;
			const size_t GetbitVoxelSize() const;
				
		private:
			Block            *m_block;
			//VX::Math::idx3    m_size;
			//VX::Math::idx3    m_headIdx;
			u8                m_bitWidth;
		};

	} // namespace SG

} // namespace VX

#endif // INCLUDE_VXSCENEGRAPH_MESH_CARTESIAN_H

