#include "VoxelCartesianG.h"

#include "../Type.h"
#include "../Math.h"

#include "Node.h"
#include "Group.h"
#include "Geometry.h"
#include "Texture.h"
#include "VoxGeometryUtil.h"
#include "VoxelCartesianBlock.h"
#include "VoxelCartesianSliceG.h"
#include "VoxelCartesianSliceL.h"
#include "VoxelCartesianC.h"
#include "VoxelCartesianL.h"

#include "MeshUtil.h"

#include <string>
#include <vector>

#include <stdio.h>
#include <limits.h>


namespace {
	//order is ABGR 
	const static u32 boundaryColor = 0xFF00FFFF;

} // namespace 

namespace VX {
namespace SG {


//====================== VoxelCartesianG ===========================
VoxelCartesianG::VoxelCartesianG( const VX::Math::vec3& origin,
                              const VX::Math::vec3& region,
							  const VX::Math::idx3& voxSize,
							  const VX::Math::idx3& divs,
							  std::vector<VoxelCartesianL*>& locals,
							  const u32& one_subdomainbytesize)
	: m_org(origin),
	  m_rgn(region),
	  m_voxSize(voxSize),
	  m_divs(divs),
	  m_one_subdomainbytesize(one_subdomainbytesize),
	  m_isShowTreeNode(true),
	  m_clipNode(NULL),
	  m_CrossPlaneActiveLocal(NULL),
      Group(NODETYPE_VOXEL_CARTESIAN_G)
	 
{
	m_pitch = VX::Math::vec3( m_rgn.x / float(m_voxSize.x),
	                          m_rgn.y / float(m_voxSize.y),
							  m_rgn.z / float(m_voxSize.z) );

	assert(locals.size()>0);

	m_locals.resize(	locals.size());


	m_numUseBlockMemory = 0;
	// local group node
	m_localGroup = vxnew VX::SG::Group();
	m_globalGroup = vxnew VX::SG::Group();
	char name[128];
	sprintf(name, "Local:[%d]", locals.size());
	m_localGroup->SetName(name);
	sprintf(name, "Global");
	m_globalGroup->SetName(name);
	//m_globalGroup->SetVisible(false);//最初は可視化しない。


	for(size_t i = 0; i < locals.size(); i++){
		VoxelCartesianL* local = locals[i];
		m_locals[i] = local;
		//add child
		m_localGroup->AddChild(local);
		if(m_locals[i]->GetBlock_CellID()!=NULL){
			m_numUseBlockMemory += m_locals[i]->GetBlock_CellID()->GetRleSize();
		}
		if(m_locals[i]->GetBlock_BCflagID()!=NULL){
			m_numUseBlockMemory += m_locals[i]->GetBlock_BCflagID()->GetRleSize();
		}
	}

	//Memory::AddExternalSize(m_numUseBlockMemory); // TODO

	// local group node
	for(int i = 0; i < 3; i++){
		VoxelCartesianSliceG* slice = vxnew VoxelCartesianSliceG(i, this);
		m_globalGroup->AddChild(slice);
		m_slices[i] = slice;
	}

	// add region scope
	//AddRegionScopeGeometry(m_org, m_rgn);

	setDispName();

	//
	// for clipping plane to select local subdomains
	createClipNode();

	// tree view node regist
	this->AddChild(m_clipNode);
	this->AddChild(m_globalGroup);
	this->AddChild(m_localGroup);
}

VoxelCartesianG::~VoxelCartesianG()
{

	//for(std::vector<VoxelCartesianL*>::iterator it = m_locals.begin(); it != m_locals.end(); ++it){
		//vxdelete(*it);
	//}

	//Memory::SubExternalSize(m_numUseBlockMemory); // TODO
}

void VoxelCartesianG::setDispName()
{
	
	char name[100];
	
	sprintf(name, "Domain[%d],Voxel(%d,%d,%d)", 
		m_divs.x*m_divs.y*m_divs.z ,
		m_voxSize.x,m_voxSize.y,m_voxSize.z
		);

	SetName(name);
}

u32 VoxelCartesianG::GetOneSubDomainByteSize() const
{
	return m_one_subdomainbytesize;
}

void VoxelCartesianG::SetOneSubDomainByteSize(const u32& size)
{
	m_one_subdomainbytesize = size;
}

b8 VoxelCartesianG::IsShowTreeNode()const
{
	return m_isShowTreeNode;
}

void VoxelCartesianG::SetShowTreeNode(const b8& show)
{
	m_isShowTreeNode = show;
}

void VoxelCartesianG::createClipNode()
{
	if(m_clipNode!=NULL){
		//already exists. it should not call this method.
		assert(false);
		return ;
	}

	m_clipNode = vxnew VoxelCartesianC(m_org,m_rgn,m_voxSize,m_divs);
}
VoxelCartesianC* VoxelCartesianG::GetClipNode()
{
	if(m_clipNode==NULL){
		assert(false);
		return NULL;
	}
	return m_clipNode;
}

const VoxelCartesianC* VoxelCartesianG::GetClipNode()const
{
	return m_clipNode;
}

const Group* VoxelCartesianG::GetGlobalGroup() const
{
	return m_globalGroup;
}

Group* VoxelCartesianG::GetGlobalGroup()
{
	return m_globalGroup;
}

const Group* VoxelCartesianG::GetLocalGroup() const
{
	return m_localGroup;
}

Group* VoxelCartesianG::GetLocalGroup() 
{
	return m_localGroup;
}

void VoxelCartesianG::ChangeShowBCFlag(const std::vector<u8>& BCflags)
{
	for( int i=0;i< m_locals.size();i++){
		m_locals[i]->ChangeShowBCFlag(BCflags);
	}
}
void VoxelCartesianG::GetBlocksOnSlice(const u32 axis, const size_t idx, std::vector<const VoxelCartesianL*> &blockList) const
{
	blockList.clear();
	
	for(std::vector<NodeRefPtr<VoxelCartesianL> >::const_iterator it = m_locals.begin(); it != m_locals.end(); ++it)
	{
		//const VX::Math::idx3 hIdx = (*it)->GetBlock_CellID()->GetHeadIdx();
		//const VX::Math::idx3 size = (*it)->GetBlock_CellID()->GetSize();
		//ローカルの相対　
		const VX::Math::idx3 hIdx = (*it)->GetHeadIdx_G();
		const VX::Math::idx3 size = (*it)->GetNumVoxels();

		if( hIdx[axis] <= idx && idx < (hIdx[axis] + size[axis]))
		{
			blockList.push_back(*it);
		}
	}
}



const VX::Math::idx3& VoxelCartesianG::GetNumVoxels() const
{
	return m_voxSize;
}

const VX::Math::idx3& VoxelCartesianG::GetNumDivs() const
{
	return m_divs;
}

const VX::Math::vec3& VoxelCartesianG::GetOrigin() const
{
	return m_org;
}

const VX::Math::vec3& VoxelCartesianG::GetRegion() const
{
	return m_rgn;
}

const VX::Math::vec3& VoxelCartesianG::GetMin() const 
{
	return m_org;
}
			
const VX::Math::vec3 VoxelCartesianG::GetMax() const
{
	return (m_org + m_rgn);
}
			
const VX::Math::vec3& VoxelCartesianG::GetPitch() const 
{
	return m_pitch;
}

const std::vector<NodeRefPtr<VoxelCartesianL> >& VoxelCartesianG::GetLocals() const
{
	return m_locals;
}


/**
*	@brief 断面操作用のカレントのローカルドメインを設定
*/

VoxelCartesianL* VoxelCartesianG::GetCrossPlaneActiveLocal()
{
	return m_CrossPlaneActiveLocal;
}

void VoxelCartesianG::SetCrossPlaneActiveLocal(VoxelCartesianL* local)
{
	m_CrossPlaneActiveLocal = local;
}

void VoxelCartesianG::SetSlicePosition(const u32 axis, const f32 pos, const u32 index_coord, const b8 boundaryOnly)
{
	m_slices[axis]->SetPosition(pos, index_coord, boundaryOnly);
}

f32 VoxelCartesianG::GetSlicePosition(const u32 axis) const 
{
	return m_slices[axis]->GetPosition();
}
/**
*	@brief 断面を強制的に再描画する
*/
void VoxelCartesianG::RepaintSlicePlane()
{
	const u32 index_coord = 1;//座標値
	const b8 boundaryOnly = false;
	for(int i=0;i<3;i++){
		f32 pos = GetSlicePosition(i);
		SetSlicePosition(i,pos,index_coord,boundaryOnly);
	}
}
// 座標値からインデックス番号を取得する
u32 VoxelCartesianG::GetIdxPosition(const u32 axis, const f32 coord_pos) const
{
	u32 posIdx = static_cast<u32>((coord_pos - m_org[axis]) / m_pitch[axis]);
	posIdx = static_cast<s32>(posIdx) >= m_voxSize[axis] ? m_voxSize[axis]-1 : posIdx;
	return posIdx;
}

// インデックス番号から座標値を取得する
f32 VoxelCartesianG::GetCoordPosition(const u32 axis, const u32 idx_pos) const
{
	f32 posCoord = m_org[axis] + m_pitch[axis] * idx_pos;
	return posCoord;
}


b8 VoxelCartesianG::GetBBoxMinMax(VX::Math::vec3& minval, VX::Math::vec3& maxval) const
{
	minval = m_org;
	maxval = minval + m_rgn;
	return true;
}
/*
void VoxelCartesianG::AddRegionScopeGeometry(const VX::Math::vec3& origin, const VX::Math::vec3& region)
{
	using namespace VX::SG;

	Geometry *geo = vxnew Geometry();
	geo->SetUseVertexLineColor(true);
	geo->SetDrawMode(Geometry::MODE_LINE);
	geo->SetName("Region Scope");
	geo->SetSelectable(false);
	geo->Alloc(8, 24);

	BoxSetter(geo->GetVertex(), geo->GetIndex(), origin, region,boundaryColor);
	geo->CalcBounds();
	geo->EnableNeedUpdate();

	m_globalGroup->AddChild(geo);

}
*/

} // namespace SG
} // namespace VX



