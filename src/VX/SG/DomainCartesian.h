/*
 * VX/SG/Domain_Cartesian.h
 *
 * SceneGraphLibrary
 *
 */

#ifndef INCLUDE_VXSCENEGRAPH_DOMAIN_CARTESIAN_H
#define INCLUDE_VXSCENEGRAPH_DOMAIN_CARTESIAN_H

#include "../Type.h"
#include "../Math.h"
#include "Node.h"
#include "Group.h"
#include "Domain.h"
#include "VoxGeometryUtil.h"

#include <string>

namespace VX
{
	namespace SG
	{

		class DomainCartesian;

		class DomainCartesianSlice : public DomainSlice
		{
		public:
			DomainCartesianSlice( const u32 axis, const DomainCartesian* domain );
			~DomainCartesianSlice();
			
			const Geometry* GetBoundaryGeometry() const
			{
				return m_boundary;
			}

			const Geometry* GetBlockGeometry() const
			{
				return m_block;
			}

			const Geometry* GetFaceGeometry() const
			{
				return m_face;
			}

			void SetPosition( const f32 pos, const u32 index_coord, const b8 boundaryOnly = false );
			f32 GetPosition() const;

			b8 GetSelection() const { return m_selecting; }
			void SetSelection(b8 select, b8 flagonly = false)
			{
				m_selecting = select;
			}

		private:
			const DomainCartesian* m_domain;
			const u32        m_axis;

			u32 m_maxLevel;
			u32 m_maxBlocks;
			VX::Math::idx3 m_rootUVW;
			b8 m_selecting;

			NodeRefPtr<Geometry> m_boundary;
			NodeRefPtr<Geometry> m_block;
			NodeRefPtr<Geometry> m_face;
		};


		///////////////////////////////////////////////////////////////////////////
		class DomainCartesianGroup : public Group
		{
		public:
			DomainCartesianGroup() : Group(NODETYPE_BBOX_DOMAIN_CARTESIAN_GROUP){}
			~DomainCartesianGroup(){}
		};

		///////////////////////////////////////////////////////////////////////////

		class DomainCartesian : public Domain
		{
		public:
			enum ExportPolicy {
				EXP_VOXEL_PITCH = 0,
				EXP_VOXEL_COUNT = 1,
			};
	
			DomainCartesian(const std::string& name = "Domain Boundary",
			                const VX::Math::vec3 origin = VX::Math::vec3(0.0f, 0.0f, 0.0f),
							const VX::Math::vec3 region = VX::Math::vec3(1.0f, 1.0f, 1.0f) )
			: Domain(NODETYPE_BBOX_DOMAIN_CARTESIAN, name)
			{
				m_slices[0] = NULL;
				m_slices[1] = NULL;
				m_slices[2] = NULL;

				m_minval = origin;
				m_maxval = origin + region;
				m_clip[0] = m_minval;
				m_clip[1] = m_maxval;
				m_clipdir = VX::Math::vec3(1.0f,0.0f,0.0f);
				m_numVoxels[0] = m_numVoxels[1] = m_numVoxels[2] = 1;
				m_numDivs[0]   = m_numDivs[1]   = m_numDivs[2]   = 1;
				m_org = m_minval;
				m_rgn = m_maxval - m_minval;
				m_pitch = m_rgn / 64.f;

				m_exportPolicy = EXP_VOXEL_PITCH;

				m_selecting = false;
				
				m_pressingClipHandle = SELECT_NONE;
				
				const unsigned int active = 1;
				
				m_subdomain = vxnew u8[m_numDivs[0] * m_numDivs[1] * m_numDivs[2]];
				memset(m_subdomain, active, sizeof(u8) * m_numDivs[0] * m_numDivs[1] * m_numDivs[2]);
				m_selection = vxnew u8[m_numDivs[0] * m_numDivs[1] * m_numDivs[2]];
				memset(m_selection, 0, sizeof(u8) * m_numDivs[0] * m_numDivs[1] * m_numDivs[2]);

				const VX::Math::vec4 line1_normal(150/255.0f, 150/255.0f, 150/255.0f, 1.0f);
				const VX::Math::vec4 line1_active(152/255.0f, 74 /255.0f, 40 /255.0f, 1.0f);
				const VX::Math::vec4 line2_normal(150/255.0f, 150/255.0f, 150/255.0f, 1.0f);
				const VX::Math::vec4 line2_active(192/255.0f, 90 /255.0f, 42 /255.0f, 1.0f);

				m_line = vxnew Geometry();
				m_line->SetUseVertexLineColor(true);
				m_line->SetDrawMode(Geometry::MODE_LINE);
				m_line->SetLineActiveColor(line1_active);
				m_line->SetLineNormalColor(line1_normal);
				m_line->SetDiapha(true);
				
				m_box = vxnew Geometry();
				m_box->SetUseVertexLineColor(true);
				m_box->SetDrawMode(Geometry::MODE_POLYGON);
				m_box->SetLineActiveColor(line1_active);
				m_box->SetLineNormalColor(line1_normal);
				m_box->SetDiapha(true);

				
				m_clipgeo = vxnew Geometry();
				m_clipgeo->SetUseVertexLineColor(true);
				m_clipgeo->SetDrawMode(Geometry::MODE_LINE);
				m_clipgeo->SetDiapha(true);
				
				for (int i = 0; i < 3; i++) {
					m_slices[i] = vxnew DomainCartesianSlice(i, this);
				}
			}
	
			~DomainCartesian()
			{
				vxdeleteArray(m_subdomain);
				vxdeleteArray(m_selection);
			}
	
			
			b8 SetBBoxMinMax(const VX::Math::vec3& minval, const VX::Math::vec3& maxval, bool updateGeometry = false)
			{
				m_maxval = maxval;
				m_minval = minval;
				m_clip[0] = minval;
				m_clip[1] = maxval;
				m_org = minval;
				m_rgn = maxval - minval;
				m_pitch = VX::Math::vec3(m_rgn.x / m_numVoxels.x, 
										 m_rgn.y / m_numVoxels.y, 
										 m_rgn.z / m_numVoxels.z);

				if(updateGeometry) return UpdateGeometry();
				return true;
			}
			
			b8 SetVoxelCount( const VX::Math::idx3& vox, bool updateGeometry = false )
			{
				m_numVoxels = vox;
				SetDivCount(VX::Math::idx3(1, 1, 1));

				m_pitch = VX::Math::vec3(m_rgn.x / m_numVoxels.x, 
										 m_rgn.y / m_numVoxels.y, 
										 m_rgn.z / m_numVoxels.z);
				 
				if(updateGeometry) return UpdateGeometry();
				return true;
			}
	
			b8 SetDivCount( const VX::Math::idx3& div, bool updateGeometry = false )
			{
				if(m_numDivs[0] != div.x || m_numDivs[1] != div.y || m_numDivs[2] != div.z )
				{
					vxdeleteArray(m_subdomain);
					m_subdomain = vxnew u8[div.x * div.y * div.z];

					const unsigned char active = 1;
					memset(m_subdomain, active, sizeof(u8) * div.x * div.y * div.z);

					vxdeleteArray(m_selection);
					m_selection = vxnew u8[div.x * div.y * div.z];
					memset(m_selection, 0, sizeof(u8) * div.x * div.y * div.z);
				}

				m_numDivs = div;
				
				if(updateGeometry) return UpdateGeometry();
				return true;
			}

			b8 SetExportPolicy( ExportPolicy policy ){
				m_exportPolicy = policy;
				return true;
			}

			const ExportPolicy GetExportPolicy() const {
				return m_exportPolicy;
			}


			b8 GetBBoxMinMax(VX::Math::vec3& minval, VX::Math::vec3& maxval) const
			{
				minval = m_minval;
				maxval = m_maxval;
				return true;
			}

			b8 GetVoxelCount(VX::Math::idx3& vox) const
			{
				vox = m_numVoxels;
				return true;
			}

			b8 GetDivCount(VX::Math::idx3& div) const
			{
				div = m_numDivs;
				return true;
			}

			void SelectSubdomain(u32 idx, b8 select)
			{
				if (idx >= static_cast<u32>(m_numDivs[0] * m_numDivs[1] * m_numDivs[2]))
					return;
				
				const u8 state = (select ? 1 : 0);
				if (m_selection[idx] == state)
					return;
				
				m_selection[idx] = state;
				UpdateGeometry();
				if (select)
					SetSelection(true, true);
				else
					checkSelection();
			}
			
			// for File Loader
			b8 SetSubdomain( const u8* subdomain,  bool updateGeometry = false )
			{
				vxdeleteArray(m_subdomain);
				m_subdomain = vxnew u8[m_numDivs[0] * m_numDivs[1] * m_numDivs[2]];
				memcpy(m_subdomain, subdomain, sizeof(u8) * (m_numDivs[0] * m_numDivs[1] * m_numDivs[2]));

				vxdeleteArray(m_selection);
				m_selection = vxnew u8[m_numDivs[0] * m_numDivs[1] * m_numDivs[2]];
				memset(m_selection, 0, sizeof(u8) * (m_numDivs[0] * m_numDivs[1] * m_numDivs[2]));

				if(updateGeometry) return UpdateGeometry();
				return true;
			}

			const u8* GetSubdomain() const
			{
				return m_subdomain;
			}

			
			b8 GetSelection() const { return m_selecting; };
			void SetSelection(b8 select, b8 flagonly = false)
			{
				m_selecting = select;
				if (flagonly)
					return;
				const u8 sflag = (select ? 1 : 0);
				const s32 n = m_numDivs[0] * m_numDivs[1] * m_numDivs[2];
				for (s32 i = 0; i < n; ++i)
					m_selection[i] = sflag;

				UpdateGeometry();
			}
						
			const VX::Math::vec3& GetClipMin() const
			{
				return m_clip[0];
			}
			const VX::Math::vec3& GetClipMax() const
			{
				return m_clip[1];
			}
			void SetClip(const VX::Math::vec3& cmin, const VX::Math::vec3& cmax)
			{
				m_clip[0] = cmin;
				m_clip[1] = cmax;
				if (m_clip[1].x < m_clip[0].x) m_clip[0].x = m_clip[1].x;
				if (m_clip[1].y < m_clip[0].y) m_clip[0].y = m_clip[1].y;
				if (m_clip[1].z < m_clip[0].z) m_clip[0].z = m_clip[1].z;
				UpdateGeometry();
			}
		private:
			b8 isClip(const VX::Math::vec3& pos) const
			{
				if (pos.x < m_clip[0].x || pos.x > m_clip[1].x
				||  pos.y < m_clip[0].y || pos.y > m_clip[1].y
				||  pos.z < m_clip[0].z || pos.z > m_clip[1].z)
				{
					return true;
				}
				return false;
			}
			
			#define CLIPBOXSIZE 0.01f
			
			void makeClipBox(VX::SG::Geometry::VertexFormat* vtx, VX::SG::Index* idx, int axis)
			{
				using namespace VX::Math;
				f32 offset = length(m_clip[1] - m_clip[0]) * CLIPBOXSIZE;
				f32 sig = 1.0f;
				s32 clp = 0;
				if (axis >= 3) {
					sig = -1.0f;
					clp = 1;
				}
				offset *= sig;
				
				const float dd = offset * 0.05f;
				vtx[0].pos = m_clip[clp] + vec3(-dd  , -dd    , -dd   ); vtx[0].col = 0xFF0000FF; vtx[0].normal = vec3(1.0f,1.0f,1.0f);
				vtx[1].pos = m_clip[clp] + vec3(offset, -dd   , -dd   ); vtx[1].col = 0xFF0000FF; vtx[1].normal = vec3(1.0f,1.0f,1.0f);
				vtx[2].pos = m_clip[clp] + vec3(-dd   , -dd   , -dd   ); vtx[2].col = 0xFF00FF00; vtx[2].normal = vec3(1.0f,1.0f,1.0f);
				vtx[3].pos = m_clip[clp] + vec3(-dd   , offset, -dd   ); vtx[3].col = 0xFF00FF00; vtx[3].normal = vec3(1.0f,1.0f,1.0f);
				vtx[4].pos = m_clip[clp] + vec3(-dd   , -dd   , -dd   ); vtx[4].col = 0xFFFF0000; vtx[4].normal = vec3(1.0f,1.0f,1.0f);
				vtx[5].pos = m_clip[clp] + vec3(-dd   , -dd   , offset); vtx[5].col = 0xFFFF0000; vtx[5].normal = vec3(1.0f,1.0f,1.0f);
				const Index base = axis * 6;
				idx[0] = base  ;
				idx[1] = base+1;
				idx[2] = base+2;
				idx[3] = base+3;
				idx[4] = base+4;
				idx[5] = base+5;
			}
			void updateClipGeometry()
			{
				m_clipgeo->Alloc(6*6, 6*6);// 6Line
				Geometry::VertexFormat* vtx = m_clipgeo->GetVertex();
				Index* idx = m_clipgeo->GetIndex();
				for (s32 i = 0; i < 6; ++i)
					makeClipBox(&vtx[i*6], &idx[i*6], i);
				
				m_clipgeo->CalcBounds();
				m_clipgeo->EnableNeedUpdate();
			}

			void makeLines();
			
			enum AXISDIR{
				X_AXIS = 1,
				Y_AXIS = 2,
				Z_AXIS = 4,
				NEGATIVE_AXIS = 8,
				X_NEGATIVE = X_AXIS,
				Y_NEGATIVE = Y_AXIS,
				Z_NEGATIVE = Z_AXIS,
				X_POSITIVE = X_AXIS|NEGATIVE_AXIS,
				Y_POSITIVE = Y_AXIS|NEGATIVE_AXIS,
				Z_POSITIVE = Z_AXIS|NEGATIVE_AXIS
			};
			template<class T>
			void voxelIterator(T& func, const u8* voxel, u8 flag = 0)
			{
				// outer
				for (s32 y = 0; y < m_numDivs[1]; ++y) {
					for (s32 x = 0; x < m_numDivs[0]; ++x) {
						if (voxel[x + y * m_numDivs[0] + 0 * m_numDivs[0] * m_numDivs[1]] == flag)
							func(x,y,0,Z_NEGATIVE);
						if (voxel[x + y * m_numDivs[0] + (m_numDivs[2]-1) * m_numDivs[0] * m_numDivs[1]] == flag)
							func(x,y,m_numDivs[2]-1,Z_POSITIVE);
					}
				}
				for (s32 z = 0; z < m_numDivs[2]; ++z) {
					for (s32 x = 0; x < m_numDivs[0]; ++x) {
						if (voxel[x + 0 * m_numDivs[0] + z * m_numDivs[0] * m_numDivs[1]] == flag)
							func(x,0,z,Y_NEGATIVE);
						if (voxel[x + (m_numDivs[1]-1)  * m_numDivs[0] + z * m_numDivs[0] * m_numDivs[1]] == flag)
							func(x,m_numDivs[1]-1,z,Y_POSITIVE);
					}
				}
				for (s32 z = 0; z < m_numDivs[2]; ++z) {
					for (s32 y = 0; y < m_numDivs[1]; ++y) {
						if (voxel[0 + y * m_numDivs[0] + z * m_numDivs[0] * m_numDivs[1]] == flag)
							func(0,y,z,X_NEGATIVE);
						if (voxel[m_numDivs[0]-1 + y * m_numDivs[0] + z * m_numDivs[0] * m_numDivs[1]] == flag)
							func(m_numDivs[0]-1,y,z,X_POSITIVE);
					}
				}
				
				// inner
				for (s32 y = 0; y < m_numDivs[1]; ++y) {
					for (s32 x = 0; x < m_numDivs[0]; ++x) {
						for (s32 z = 0; z < m_numDivs[2]-1; ++z) {
							if (voxel[x + y * m_numDivs[0] + z     * m_numDivs[0] * m_numDivs[1]]
							!=  voxel[x + y * m_numDivs[0] + (z+1) * m_numDivs[0] * m_numDivs[1]]){
								func(x,y,z,Z_POSITIVE);
							}
						}
					}
				}
				for (s32 z = 0; z < m_numDivs[2]; ++z) {
					for (s32 y = 0; y < m_numDivs[1]; ++y) {
						for (s32 x = 0; x < m_numDivs[0]-1; ++x) {
							if (voxel[x   + y * m_numDivs[0] + z * m_numDivs[0] * m_numDivs[1]]
							!=  voxel[x+1 + y * m_numDivs[0] + z * m_numDivs[0] * m_numDivs[1]]){
								func(x,y,z,X_POSITIVE);
							}
						}
					}
				}
				for (s32 z = 0; z < m_numDivs[2]; ++z) {
					for (s32 x = 0; x < m_numDivs[0]; ++x) {
						for (s32 y = 0; y < m_numDivs[1]-1; ++y) {
							if (voxel[x + y     * m_numDivs[0] + z * m_numDivs[0] * m_numDivs[1]]
							!=  voxel[x + (y+1) * m_numDivs[0] + z * m_numDivs[0] * m_numDivs[1]]){
								func(x,y,z,Y_POSITIVE);
							}
						}
					}
				}

			}
			
			class faceCounter
			{
			public:
				faceCounter()    { m_cnt = 0; }
				void operator()(u32 x, u32 y, u32 z, u32 dir){ m_cnt++;   }
				u32 GetTriangleIndexCount(){ return m_cnt * 6; }
			private:
				u32 m_cnt;
			};
			
			class faceCreator
			{
			public:
				//faceCreator(VX::SG::Index* idx, u32* numDivs){
				faceCreator(VX::SG::Index* idx, const VX::Math::idx3& numDivs){
					m_idx = idx;
					m_cnt = 0;
					m_numDivs[0] = numDivs[0];
					m_numDivs[1] = numDivs[1];
					m_numDivs[2] = numDivs[2];
					m_offset = 0;
				}
				void SetVertexOffset(u32 offset)
				{
					m_offset = offset;
				}
				void operator()(u32 x, u32 y, u32 z, u32 dir){
					const s32 lXNum = m_numDivs[0] + 1;
					const s32 lYNum = m_numDivs[1] + 1;
					const s32 d = (dir & NEGATIVE_AXIS ? 1 : 0);
					if (dir & X_AXIS) {
						m_idx[m_cnt * 6    ] = x + d +  y    * lXNum +  z    * lYNum * lXNum + m_offset;
						m_idx[m_cnt * 6 + 1] = x + d + (y+1) * lXNum +  z    * lYNum * lXNum + m_offset;
						m_idx[m_cnt * 6 + 2] = x + d + (y+1) * lXNum + (z+1) * lYNum * lXNum + m_offset;
						m_idx[m_cnt * 6 + 3] = x + d +  y    * lXNum +  z    * lYNum * lXNum + m_offset;
						m_idx[m_cnt * 6 + 4] = x + d + (y+1) * lXNum + (z+1) * lYNum * lXNum + m_offset;
						m_idx[m_cnt * 6 + 5] = x + d +  y    * lXNum + (z+1) * lYNum * lXNum + m_offset;
					}
					if (dir & Y_AXIS) {
						m_idx[m_cnt * 6    ] = x   + (y+d) * lXNum +  z    * lYNum * lXNum + m_offset;
						m_idx[m_cnt * 6 + 1] = x+1 + (y+d) * lXNum +  z    * lYNum * lXNum + m_offset;
						m_idx[m_cnt * 6 + 2] = x+1 + (y+d) * lXNum + (z+1) * lYNum * lXNum + m_offset;
						m_idx[m_cnt * 6 + 3] = x   + (y+d) * lXNum +  z    * lYNum * lXNum + m_offset;
						m_idx[m_cnt * 6 + 4] = x+1 + (y+d) * lXNum + (z+1) * lYNum * lXNum + m_offset;
						m_idx[m_cnt * 6 + 5] = x   + (y+d) * lXNum + (z+1) * lYNum * lXNum + m_offset;
					}
					if (dir & Z_AXIS) {
						m_idx[m_cnt * 6    ] = x   +  y    * lXNum + (z+d) * lYNum * lXNum + m_offset;
						m_idx[m_cnt * 6 + 1] = x+1 +  y    * lXNum + (z+d) * lYNum * lXNum + m_offset;
						m_idx[m_cnt * 6 + 2] = x+1 + (y+1) * lXNum + (z+d) * lYNum * lXNum + m_offset;
						m_idx[m_cnt * 6 + 3] = x   +  y    * lXNum + (z+d) * lYNum * lXNum + m_offset;
						m_idx[m_cnt * 6 + 4] = x+1 + (y+1) * lXNum + (z+d) * lYNum * lXNum + m_offset;
						m_idx[m_cnt * 6 + 5] = x   + (y+1) * lXNum + (z+d) * lYNum * lXNum + m_offset;
					}
					
					m_cnt++;
				}
			private:
				u32 m_cnt;
				VX::SG::Index* m_idx;
				u32 m_numDivs[3];
				u32 m_offset;
			};
			void makeBox()
			{
				using namespace VX::Math;
				const s32 lXNum = m_numDivs[0] + 1;
				const s32 lYNum = m_numDivs[1] + 1;
				const s32 lZNum = m_numDivs[2] + 1;
				const u32 pointNum = lXNum * lYNum * lZNum;

				const u8 active = 1;
				
				faceCounter fc;
				voxelIterator(fc, m_subdomain,active);
				voxelIterator(fc, m_selection,1);
				u32 idxCount = fc.GetTriangleIndexCount();
				m_box->Alloc(pointNum*2, idxCount);
				
				// copy vtx
				Geometry::VertexFormat* boxvtx = m_box->GetVertex();
				Geometry::VertexFormat* linevtx = m_line->GetVertex();
				memcpy(boxvtx, linevtx, pointNum * sizeof(Geometry::VertexFormat));
				for (u32 v = 0; v < pointNum; ++v)
					boxvtx[v].col = 0x66FFFFFF;
				memcpy(&boxvtx[pointNum], linevtx, pointNum * sizeof(Geometry::VertexFormat));
				for (u32 v = 0; v < pointNum; ++v)
					boxvtx[v+pointNum].col = 0x663333FF;
				
				const vec3 maxval, minval;
				
				// Index
				Index* idx = m_box->GetIndex();
				memset(idx, 0, sizeof(Index)*idxCount);
				faceCreator fcr(idx, m_numDivs);
				fcr.SetVertexOffset(pointNum);
				voxelIterator(fcr,m_selection,1);
				fcr.SetVertexOffset(0);
				voxelIterator(fcr,m_subdomain,active);
				
				m_box->CalcBounds();
				m_box->EnableNeedUpdate();
			}
			VX::Math::vec3 clipedMinMaxValue(const VX::Math::vec3& clip)
			{
				VX::Math::vec3 c = clip;
				c.x = (c.x < m_minval.x ? m_minval.x : c.x);
				c.y = (c.y < m_minval.y ? m_minval.y : c.y);
				c.z = (c.z < m_minval.z ? m_minval.z : c.z);
				c.x = (c.x > m_maxval.x ? m_maxval.x : c.x);
				c.y = (c.y > m_maxval.y ? m_maxval.y : c.y);
				c.z = (c.z > m_maxval.z ? m_maxval.z : c.z);
				return c;
			}
			
		public:
			b8 UpdateGeometry()
			{
				makeLines();
				makeBox();
				updateClipGeometry();
				for (int i = 0; i < 3; i++) {
					if (m_slices[i]) {
						SetSlicePosition(i, m_org[i] + m_rgn[i] * 0.5f, 1, false);
						//GetSlicePosition(i)
					}
				}

				return true;
			}
			
			void ResetClipping()
			{
				m_clip[0] = m_minval;
				m_clip[1] = m_maxval;
				UpdateGeometry();
			}
			
			b8 IntersectFrustum(const Math::vec3* orgdir, std::vector<VX::SG::HitDomain>& hdoms)
			{
				if (m_pressingClipHandle)
				{
					const VX::Math::vec3 od = m_oldorgdir[1] * m_oldhit + m_oldorgdir[0];
					const VX::Math::vec3 nd = orgdir[1] * m_oldhit + orgdir[0];
					const VX::Math::vec3 md = nd - od;
					if (m_pressingClipHandle == SELECT_MIN)
						m_clip[0] += md;
					else if (m_pressingClipHandle == SELECT_MAX)
						m_clip[1] += md;
					
					m_clip[0] = clipedMinMaxValue(m_clip[0]);
					m_clip[1] = clipedMinMaxValue(m_clip[1]);
					m_oldorgdir[0] = orgdir[0];
					m_oldorgdir[1] = orgdir[1];
					UpdateGeometry();
					return true;
				}

				f32 hmin = 1.0E+16f;
				
				// Clipbox
				f32 offset = length(m_clip[1] - m_clip[0]) * CLIPBOXSIZE;
				VX::Math::vec3 cliphandle[] = {
					m_clip[1]+VX::Math::vec3(-offset,-offset,-offset),m_clip[1],
					m_clip[0],m_clip[0]+VX::Math::vec3( offset, offset, offset),
				};
				for (s32 i = 0; i < 2; ++i) {
					if (testAABB(orgdir, cliphandle[1+2*i], cliphandle[2*i], hmin)){
						m_pressingClipHandle = (i == 0 ? SELECT_MAX : SELECT_MIN);
						m_oldhit = hmin;
						m_oldorgdir[0] = orgdir[0];
						m_oldorgdir[1] = orgdir[1];
						return true;
					}
				}
									
				b8 hit = false;
				const VX::SG::Geometry::VertexFormat* v = m_line->GetVertex();
				
				const s32 lXNum = m_numDivs[0] + 1;
				const s32 lYNum = m_numDivs[1] + 1;
				for (s32 z = 0; z < m_numDivs[2]; ++z) {
					for (s32 y = 0; y < m_numDivs[1]; ++y) {
						for (s32 x = 0; x < m_numDivs[0]; ++x) {
							const VX::Math::vec3 aabb_min = v[x + y * lXNum + z * lXNum * lYNum].pos;
							const VX::Math::vec3 aabb_max = v[x+1 + (y+1) * lXNum + (z+1) * lXNum * lYNum].pos;
							if (isClip(aabb_max) || isClip(aabb_min))
								continue;
							
							b8 r = testAABB(orgdir, aabb_max, aabb_min, hmin);
							hit |= r;
							if (r) {
								VX::SG::HitDomain dom(this, x + y * m_numDivs[0] + z * m_numDivs[0] * m_numDivs[1], hmin);
								hdoms.push_back(dom);
							}
						}
					}
				}
					
				return hit;
			}
			b8 IntersectFrustum(const Math::vec4* frustum, const Math::matrix& mat, b8 selection = true)
			{
				VX::SG::Geometry::VertexFormat* v = m_line->GetVertex();
	
				b8 hit = false;
				const s32 frustum_num = 4;
				const u8 sflag = selection ? 1 : 0;
				const s32 lXNum = m_numDivs[0] + 1;
				const s32 lYNum = m_numDivs[1] + 1;
				// if you feel slow, you can optimize this.
				for (s32 z = 0; z < m_numDivs[2]; ++z) {
					for (s32 y = 0; y < m_numDivs[1]; ++y) {
						for (s32 x = 0; x < m_numDivs[0]; ++x) {
							s32 inner = 0;
							const VX::Math::vec4 tv[] = {
								VX::Math::vec4(v[x   +  y    * lXNum +  z    * lXNum * lYNum].pos, 1.0),
								VX::Math::vec4(v[x+1 +  y    * lXNum +  z    * lXNum * lYNum].pos, 1.0),
								VX::Math::vec4(v[x+1 + (y+1) * lXNum +  z    * lXNum * lYNum].pos, 1.0),
								VX::Math::vec4(v[x   + (y+1) * lXNum +  z    * lXNum * lYNum].pos, 1.0),
								VX::Math::vec4(v[x   +  y    * lXNum + (z+1) * lXNum * lYNum].pos, 1.0),
								VX::Math::vec4(v[x   + (y+1) * lXNum + (z+1) * lXNum * lYNum].pos, 1.0),
								VX::Math::vec4(v[x+1 +  y    * lXNum + (z+1) * lXNum * lYNum].pos, 1.0),
								VX::Math::vec4(v[x+1 + (y+1) * lXNum + (z+1) * lXNum * lYNum].pos, 1.0)
							};
							for (s32 p = 0; p < 8; ++p){
								if (isClip(tv[p].xyz()))
									break;
								
								const VX::Math::vec4 tvp = mat * tv[p];
								for (s32 f = 0; f < frustum_num; ++f)
								{
									const f32 d = dot(frustum[f], tvp);
									if (d <= 0)
										++inner;
								}
							}
							
							if (inner == frustum_num * 8)
							{
								m_selection[x + y * m_numDivs[0] + z * m_numDivs[0] * m_numDivs[1]] = sflag;
								hit = true;
							}
						}
					}
				}

				if (hit) {
					UpdateGeometry();
					m_line->EnableNeedUpdate();
					if (selection)
						SetSelection(true, true);
					else
						checkSelection();
				}
				return hit;
			}
			b8 EndPick()
			{
				m_pressingClipHandle = SELECT_NONE;
				return true;
			}
			const VX::SG::Geometry* GetGeometry() const
			{
				return m_line;
			}
			
			const VX::SG::Geometry* GetClipGeometry() const
			{
				return m_clipgeo;
			}
			const VX::SG::Geometry* GetBoxGeometry() const
			{
				return m_box;
			}
			
			
			const VX::SG::DomainCartesianSlice* GetSlice(const u32 i) const
			{
				return m_slices[i];
			}
						
			void EnableNeedUpdate()
			{
				m_line->EnableNeedUpdate();
			}
			
			const VX::Math::vec3& GetOrigin() const { return m_org; }
			const VX::Math::vec3& GetRegion() const { return m_rgn; }
			const VX::Math::vec3& GetPitch()  const { return m_pitch; }

			// for Slice Plane Control
			f32 GetSlicePosition(const u32 axis ) const
			{
				return m_slices[axis]->GetPosition();
			}
			void SetSlicePosition(const u32 axis, const f32 pos, const u32 index_coord, const b8 boundaryOnly)
			{
				m_slices[axis]->SetPosition(pos, index_coord, boundaryOnly);
			}

			u32 GetIdxPosition(const u32 axis, const f32 coord_pos) const
			{
				u32 numIdx = m_numVoxels[axis];
				u32 posIdx = static_cast<u32>((coord_pos - m_org[axis]) / m_pitch[axis]);
				posIdx = static_cast<s32>(posIdx) >= numIdx ? numIdx-1 : posIdx;
				return posIdx;
			}

			f32 GetCoordPosition(const u32 axis, const u32 idx_pos ) const
			{
				f32 posCoord = m_org[axis] + m_pitch[axis] * idx_pos;
				return posCoord;
			}
			
		private:
			void checkSelection()
			{
				const s32 n = m_numDivs[0] * m_numDivs[1] * m_numDivs[2];
				for (s32 i = 0; i < n; ++i) {
					if (m_selection[i]){
						m_selecting = true;
						return;
					}
				}
				m_selecting = false;
			}
			
			static inline bool intersect(const VX::Math::vec3* bb_minmax, const VX::Math::vec3& org,
										 const VX::Math::vec3& invDir, const u32 dirIsNeg[3], f32& hitmin) {
				// Check for ray intersection against $x$ and $y$ slabs
				float tmin =  (bb_minmax[    dirIsNeg[0]].x - org.x) * invDir.x;
				float tmax =  (bb_minmax[1 - dirIsNeg[0]].x - org.x) * invDir.x;
				const float tymin = (bb_minmax[    dirIsNeg[1]].y - org.y) * invDir.y;
				const float tymax = (bb_minmax[1 - dirIsNeg[1]].y - org.y) * invDir.y;
				if ((tmin > tymax) || (tymin > tmax))
					return false;
				if (tymin > tmin) tmin = tymin;
				if (tymax < tmax) tmax = tymax;
				
				// Check for ray intersection against $z$ slab
				const float tzmin = (bb_minmax[    dirIsNeg[2]].z - org.z) * invDir.z;
				const float tzmax = (bb_minmax[1 - dirIsNeg[2]].z - org.z) * invDir.z;
				if ((tmin > tzmax) || (tzmin > tmax))
					return false;
				if (tzmin > tmin)
					tmin = tzmin;
				if (tzmax < tmax)
					tmax = tzmax;
				b8 r = (tmin < hitmin) && (tmax > 0);
				hitmin = tmin;
				return r;
			}
			b8 testAABB(const VX::Math::vec3* orgdir, const VX::Math::vec3& aabb_max, const VX::Math::vec3& aabb_min, f32& hitmin) const
			{
				const VX::Math::vec3 invDir(1.0f / orgdir[1].x, 1.0f / orgdir[1].y, 1.0f / orgdir[1].z);
				u32 dirIsNeg[3] = { invDir.x < 0, invDir.y < 0, invDir.z < 0 };
				const VX::Math::vec3 bb_minmax[] = {aabb_min, aabb_max};
				return intersect(bb_minmax, orgdir[0], invDir, dirIsNeg, hitmin);
			}

			b8 CheckSubDomainRange( const u32* indexes, const u32 size )
			{
				u32 divCnt = m_numDivs[0] * m_numDivs[1] * m_numDivs[2];
				for(u32 i = 0; i < size; i++){
					if(divCnt <= indexes[i]) { return false; }
				}
				return true;
			}

			void SelectedSubdomainStatus(b8 active)
			{
				const u32 divCnt = m_numDivs[0] * m_numDivs[1] * m_numDivs[2];
				const u8 aflag = (active ? 1 : 0); // Active:1 Inactive:0
				for (u32 i = 0; i < divCnt; ++i){
					if (m_selection[i])
						m_subdomain[i] = aflag;
				}
				UpdateGeometry();
			}

		// member
		private:
			enum {
				m_lineSelectColor   = 0xFF3F55FF,
				m_lineInactiveColor = 0xFF444444,
				m_lineColor         = 0xFFFFFFFF
			};
			
			VX::Math::vec3 m_minval, m_maxval;
			VX::Math::idx3 m_numVoxels;
			VX::Math::idx3 m_numDivs;

			u8*            m_subdomain;
			u8*            m_selection;

			ExportPolicy  m_exportPolicy;
			
			//b8 m_hasVoxel;
			//b8 m_hasDivision;

			enum {
				SELECT_NONE,
				SELECT_MIN,
				SELECT_MAX
			} m_pressingClipHandle;
			f32 m_oldhit;
			VX::Math::vec3 m_oldorgdir[2];
			
			b8 m_selecting;
			
			VX::SG::NodeRefPtr<Geometry> m_line;
			VX::SG::NodeRefPtr<Geometry> m_clipgeo;
			VX::SG::NodeRefPtr<Geometry> m_box;
			
			VX::SG::NodeRefPtr<DomainCartesianSlice> m_slices[3];           //< Slice Planes

			VX::Math::vec3 m_clip[2];//min,max
			VX::Math::vec3 m_clipdir;

			VX::Math::vec3 m_org;        ///< Global Origin
			VX::Math::vec3 m_rgn;        ///< Global Region
			VX::Math::vec3 m_pitch;      ///< Pitch size for slice plane position
			
		};

	} // namespace SG
} // namespace VX

#endif // INCLUDE_VXSCENEGRAPH_DOMAIN_CARTESIAN_H
