/*
 * VX/SG/DomainBCM.h
 *
 * SceneGraphLibrary
 *
 */
/// @file DomainBCM.h
/// BCMドメインデータの管理クラス

#ifndef INCLUDE_VXSCENEGRAPH_DOMAIN_BCM_H
#define INCLUDE_VXSCENEGRAPH_DOMAIN_BCM_H

#include "../Type.h"
#include "../Math.h"

#include "Node.h"
#include "Geometry.h"
#include "Group.h"
#include "OuterBCClass.h"
#include "OuterBC.h"
#include "Medium.h"
#include "Domain.h"
#include "DistanceFieldSlice.h"

#include "../../VOX/Pedigree.h"

#include <list>
#include <vector>
#include <string>

namespace VOX {
	class BCMOctree;
	class Divider;
} // namespace VOX

namespace VX
{
	namespace SG
	{
		class DomainBCM;

		///////////////////////////////////////////////////////////////////////////
		class DomainBCMGroup : public Group
		{
		public:
			DomainBCMGroup() : Group(NODETYPE_BBOX_DOMAIN_BCM_GROUP){}
			~DomainBCMGroup(){}
		};

		///////////////////////////////////////////////////////////////////////////
		class DomainBCMSlice : public DomainSlice
		{
		public:
			DomainBCMSlice( const u32 axis, const DomainBCM* domain );
			~DomainBCMSlice();
			
			const Geometry* GetBoundaryGeometry() const
			{
				return m_boundary;
			}

			const Geometry* GetBlockGeometry() const
			{
				return m_block;
			}

			const Geometry* GetFaceGeometry() const
			{
				return m_face;
			}

			void SetPosition( const f32 pos, const u32 index_coord, const b8 boundaryOnly = false );
			f32 GetPosition() const;

			b8 GetSelection() const { return m_selecting; }
			void SetSelection(b8 select, b8 flagonly = false)
			{
				m_selecting = select;
			}

		private:
			const DomainBCM* m_domain;
			const u32        m_axis;
			b8               m_selecting;

			u32 m_maxLevel;
			u32 m_maxBlocks;
			VX::Math::idx3 m_rootUVW;

			NodeRefPtr<Geometry> m_boundary;
			NodeRefPtr<Geometry> m_block;
			NodeRefPtr<Geometry> m_face;
		};

		///////////////////////////////////////////////////////////////////////////
		class DomainBCM : public Domain
		{
		public:
			enum ExportPolicy {
				EXP_PARAMETER = 0,
				EXP_OCTREE    = 1,
			};

			enum LeafOrdering {
				ORDER_Z        = 0,
				ORDER_HILBERT  = 1,
			};
			/////////////////////////////////////////
			class GeometryScope
			{
			public:
				std::string type;
				f32 cell;
				f32 ratio;
				std::string name;
				u32 level;
				GeometryScope(){
					type = "Ratio";
					cell = 1;
					ratio = 0;
				}
				GeometryScope(const std::string& _name, const u32 _lv,
							  const std::string _type, const f32 _ratio, const f32 _cell)
				: name(_name), level(_lv)
				, type(_type), ratio(_ratio), cell(_cell)
				{
				}

				~GeometryScope()
				{

				}

				GeometryScope(const GeometryScope& obj)
				: name(obj.name), level(obj.level)
				, type(obj.type), cell(obj.cell), ratio(obj.ratio)
				{

				}

				GeometryScope& operator = (const GeometryScope& obj)
				{
					name = obj.name;
					level = obj.level;
					name = obj.name;
					level = obj.level;
					type = obj.type;
					cell = obj.cell;
					ratio = obj.ratio;
					return *this;
				}

				bool operator == (const GeometryScope& obj)
				{
					return (name == obj.name) && (level == obj.level) && (type == obj.type) && (cell == obj.cell) && (ratio == obj.ratio);
					
				}

				bool operator != (const GeometryScope& obj)
				{
					return (name != obj.name) || (level != obj.level) || (type != obj.type) || (cell != obj.cell) || (ratio != obj.ratio);
				
				}
			};

			/////////////////////////////////////////
			class RegionScope
			{
			public:
				std::string type;
				f32 cell;
				f32 ratio;
				VX::Math::vec3 origin, region;
				u32 level;
				RegionScope(){
					type = "Ratio";
					cell = 1;
					ratio = 0;
				}
				RegionScope(const VX::Math::vec3 _org, const VX::Math::vec3 _rgn, const u32 _lv,
							const std::string _type, const f32 _ratio, const f32 _cell)
				: origin(_org), region(_rgn), level(_lv)
				, type(_type), ratio(_ratio), cell(_cell)
				{
				}

				~RegionScope()
				{
				}

				RegionScope(const RegionScope& obj)
				: origin(obj.origin), region(obj.region), level(obj.level)
				, type(obj.type), cell(obj.cell), ratio(obj.ratio)
				{

				}

				RegionScope& operator = (const RegionScope& obj)
				{
					origin = obj.origin;
					region = obj.region;
					level  = obj.level;
					type = obj.type;
					cell = obj.cell;
					ratio = obj.ratio;
					return *this;
				}

				bool operator == (const RegionScope& obj)
				{
					return (origin == obj.origin) && (region == obj.region) && (level == obj.level) && (type == obj.type) && (cell == obj.cell) && (ratio == obj.ratio);
				}
				
				bool operator != (const RegionScope& obj)
				{
					return (origin != obj.origin) || (region != obj.region) || (level != obj.level) || (type != obj.type) || (cell != obj.cell) || (ratio != obj.ratio);
				}
			};
			
			/////////////////////////////////////////
			/// 距離に基づく細分化で使用する分割パラメータ.
			/// 
			class SubdivisionMargin
			{
			public:
				enum { MAX_LEVEL = -1 }; ///< 最大の分割レベル

			public:
				/// コンストラクタ
				SubdivisionMargin() : margin(0), level(MAX_LEVEL) {}

				/// コンストラクタ
				///
				/// @param[in] f32 margin 距離マージン
				/// @param[in] s32 level 分割レベル. 
				///
				SubdivisionMargin(f32 margin, s32 level) : margin(margin), level(level) {}

				f32 margin; ///< 分割判定の距離マージン
				s32 level;  ///< 分割レベル

				bool operator == (const SubdivisionMargin& rhs) const
				{
					return (margin == rhs.margin) && (level == rhs.level);
				}

				bool operator != (const SubdivisionMargin& rhs) const
				{
					return !this->operator==(rhs);
				}
			};

			/////////////////////////////////////////
			struct BCMOctreeInformation
			{
				u64 totalNodeCount;
				u64 totalLeafCount;
				u32 maxLevel;
				struct LevelInformation
				{
					u64 nodeCount;
					u64 leafCount;
					LevelInformation() : nodeCount(0), leafCount(0) {}
				};
				std::vector<LevelInformation> levels;

				static void Initialize(BCMOctreeInformation& information, u32 maxLevel)
				{
					information.totalNodeCount = 0;
					information.totalLeafCount = 0;
					information.maxLevel = maxLevel;
					information.levels.clear();
					information.levels.resize(maxLevel + 1);
				}
			};

			/////////////////////////////////////////
			DomainBCM(const std::string& name = "Domain BCM");

			DomainBCM(const VX::Math::vec3& org, const VX::Math::vec3& rgn, const VX::Math::idx3& rootDims,
					  const std::vector<VOX::Pedigree>& pedigrees,
			          const std::string& name = "Domain BCM");

			~DomainBCM();
			
			// for Geometry
			Geometry* GetLevelBox( const u32 level ) const
			{
				if ( level >= m_levelBoxes.size() )
				{
					return NULL;
				}
				return m_levelBoxes[level];
			}

			static void InitLvColorTable(u32* &colorTable, const u32 max);
			static void InitDivColorTable(u32* &colorTable, const u32 max);

			Node* GetSlice(const u32 i) const { return m_slices[i]; }
			Node* GetDistanceSlice(const u32 i) const { return m_dfSlices[i]; }

			// for File Save
			b8  GetPedigreeList(std::vector<VOX::Pedigree>& pedigrees) const;

			// for Octree Parameter
			u32 GetMaxLevel() const { return m_maxLevel; }
			u32 GetNumBlocks() const;
			const BCMOctreeInformation& GetOctreeInformation() const { return m_information; }

			const VOX::BCMOctree* GetOctree() const { return m_octree; }	
			const VX::Math::vec3& GetOrigin() const { return m_org; }
			const VX::Math::vec3& GetRegion() const { return m_rgn; }
			const VX::Math::idx3& GetRootDims() const { return m_rootDims; }
			
			// for create from leaf nodes
			void CreateOctree(const std::vector<VOX::Pedigree>& pedigrees);
			// for create by build octree
			void CreateOctree(VOX::Divider* divider);
			void DeleteOctree();

			b8 SetBBoxMinMax(const VX::Math::vec3& minval, const VX::Math::vec3& maxval)
			{
				m_org = minval;
				m_rgn = maxval - minval;
				//if( updateGeometry ) UpdateGlobalBoxGeometry();
				return true;
			}

			b8 SetRootDims(const VX::Math::idx3& rootDims)
			{
				m_rootDims = rootDims;
				//if( updateGeometry ) UpdateGlobalBoxGeometry();
				return true;
			}

			// for Slice Plane Control
			void SetSlicePosition(const u32 axis, const f32 pos, const u32 index_coord, const b8 boundaryOnly)
			{
				m_slices[axis]->SetPosition(pos, index_coord, boundaryOnly);
			}

			f32 GetSlicePosition(const u32 axis ) const
			{
				return m_slices[axis]->GetPosition();
			}

			void UpdateDistanceFieldSlice(u32 axis, f32 pos, const Group* sg, b8 boundaryOnly)
			{
				m_dfSlices[axis]->UpdatePosition(pos, sg, boundaryOnly);
			}

			f32 GetDistanceSlicePosition(u32 axis) const
			{
				return m_dfSlices[axis]->GetPosition();
			}

			const VX::Math::vec3& GetPitch() const
			{
				return m_pitch;
			}
			
			u32 GetIdxPosition(const u32 axis, const f32 coord_pos) const
			{
				u32 numIdx = m_rootDims[axis] * ( 1 << m_maxLevel);
				u32 posIdx = static_cast<u32>((coord_pos - m_org[axis]) / m_pitch[axis]);
				posIdx = static_cast<s32>(posIdx) >= numIdx ? numIdx-1 : posIdx;
				return posIdx;
			}

			f32 GetCoordPosition(const u32 axis, const u32 idx_pos ) const
			{
				f32 posCoord = m_org[axis] + m_pitch[axis] * idx_pos;
				return posCoord;
			}

			f32 GetDistanceSlicePitch(u32 axis) const
			{
				return m_rgn[axis] / 64;
			}

			void SetDistanceSliceRange(f32 min, f32 max);

			void GetDistanceSliceRange(f32* min, f32* max)
			{
				*min = m_dfRangeMin;
				*max = m_dfRangeMax;
			}

			// for Color Map scaling

			void GetColorMapRangeLevels(s32* min, s32* max, u32* selectMax) const
			{
				*min = m_dfRangeMinLevel;
				*max = m_dfRangeMaxLevel;
				*selectMax = ComputeMaxLevel();
			}

			void SetColorMapRangeLevels(s32 min, s32 max);
			void UpdateColorMapRange();
			
			///////////////////////////////////////////
			void SelectedSubdomainStatus(b8 active){ }
			b8 GetSelection() const { return false; }
			void SetSelection(b8 select, b8 flagonly = false) { }
			///////////////////////////////////////////

			// for Parallel Divide
			b8 SetNumDivs(const u32 divs);
			u32 GetNumDivs() const { return m_divs; }

			u32 GetDivColor(u32 i) const { return m_divColorTbl[i]; }

			// for Parameter
			void SetBaseLevel( const u32 lv )
			{
				m_baseLevel = lv;
			}

			void SetMinLevel( const u32 lv )
			{ 
				m_minLevel  = lv;
			}

			void SetBlockSize( const VX::Math::idx3& size)
			{
				m_blockSize = size;
			}

			void SetLeafOrdering( const LeafOrdering ordering)
			{
				m_ordering  = ordering;
			}

			u32 GetBaseLevel() const
			{
				return m_baseLevel;
			}

			u32 GetMinLevel() const
			{
				return m_minLevel;
			}

			const VX::Math::idx3& GetBlockSize() const
			{
				return m_blockSize;
			}

			LeafOrdering GetLeafOrdering() const
			{
				return m_ordering;
			}

			// for ExportPolicy
			void SetExportPolicy( const ExportPolicy policy )
			{
				m_exportPolicy = policy;
			}

			ExportPolicy GetExportPolicy() const
			{
				return m_exportPolicy;
			}

			// for Geometry Scope
			std::vector<GeometryScope*>& GetGeometryScopeList()
			{
				return m_geoScopeList;
			}

			const std::vector<GeometryScope*>& GetGeometryScopeList() const
			{
				return m_geoScopeList;
			}

			b8 ClearGeometryScopeList()
			{
				for(std::vector<GeometryScope*>::iterator it = m_geoScopeList.begin(); it != m_geoScopeList.end(); ++it)
				{
					vxdelete(*it);
				}
				m_geoScopeList.clear();
				return true;
			}

			// for Region Scope
			std::vector<RegionScope*>& GetRegionScopeList()
			{
				return m_rgnScopeList;
			}

			const std::vector<RegionScope*>& GetRegionScopeList() const
			{
				return m_rgnScopeList;
			}

			b8 ClearRegionScopeList()
			{
				for(std::vector<RegionScope*>::iterator it = m_rgnScopeList.begin(); it != m_rgnScopeList.end(); ++it)
				{
					vxdelete(*it);
				}
				m_rgnScopeList.clear();
				return true;
			}

			/// スコープ設定によるものを含めた分割レベルの最大値を計算する
			u32 ComputeMaxLevel() const;

			// for Distance-Based
			
			/// 距離に基づく細分化を行うかどうか
			///
			/// @retval true 距離に基づく細分化を行う
			/// @retval false 距離に基づく細分化を行わない(=交差に基づく細分化を行う)
			///
			b8 GetUseDistanceBasedMethod() const
			{
				return m_useDistanceBasedMethod;
			}

			/// 距離に基づく細分化を行うかどうかを設定する
			///
			/// @param[in] use 距離に基づく細分化を行う場合 true.
			///
			void SetUseDistanceMethod(b8 use)
			{
				m_useDistanceBasedMethod = use;
			}

			/// 距離に基づく細分化の分割パラメータ
			std::vector<SubdivisionMargin>& GetSubdivisionMargins()
			{
				return m_subdivisionMargins;
			}

			/// 距離に基づく細分化の分割パラメータ
			const std::vector<SubdivisionMargin>& GetSubdivisionMargins() const
			{
				return m_subdivisionMargins;
			}

			/// 分割パラメータをクリアする
			void ClearSubdivisionMargins()
			{
				m_subdivisionMargins.clear();
			}
			
			// Global margin Setting
			void setMarginValue(VX::SG::DomainBCM::RegionScope *margin)
			{
				m_marginType = margin->type;
				m_marginRatio = margin->ratio;
				m_marginCell = margin->cell;
			}

			const std::string getMarginType() const
			{
				return m_marginType;
			}
			
			const f32 getMarginRatio() const
			{
				return m_marginRatio;
			}
			
			const f32 getMarginCell() const
			{
				return m_marginCell;
			}

		private:
			void CreateTreeGeometry();
			void DeleteTreeGeometry();

			VOX::BCMOctree* m_octree;    ///< Octree (BCMTools/BCMOctree)
			
			VX::Math::vec3 m_org;        ///< Global Origin
			VX::Math::vec3 m_rgn;        ///< Global Region
			VX::Math::idx3 m_rootDims;   ///< Octree Root Division (Root Node Dims)

			/// for Visualize parameter
			VX::Math::vec3 m_pitch;      ///< Pitch size for slice plane position
			u32 m_maxLevel;              ///< Octree's Max level 
			u32 m_divs;                  ///< Parallel Division

			u32* m_lvColorTbl;           ///< level color Table (for Level Block)
			u32* m_divColorTbl;          ///< division color table (for slice plane)

			// Geometry
			NodeRefPtr<DomainBCMSlice> m_slices[3];           //< Slice Planes
			std::vector< NodeRefPtr<Geometry> > m_levelBoxes; //< Level BBoxes

			NodeRefPtr<DistanceFieldSlice> m_dfSlices[3];
			NodeRefPtr<ColorMap> m_dfColorMap;
			f32 m_dfRangeMin;
			f32 m_dfRangeMax;
			s32 m_dfRangeMinLevel;
			s32 m_dfRangeMaxLevel;
			
			///// Octree Build Parameters /////
			u32            m_baseLevel;                 ///< Base Octree Division Level
			u32            m_minLevel;                  ///< Minimum Octree Division Level
			VX::Math::idx3 m_blockSize;                 ///< Leaf Block Size
			std::vector<GeometryScope*> m_geoScopeList; ///< Geometry Scope List (name and level)
			std::vector<RegionScope*>   m_rgnScopeList; ///< Region Scope List   (BBox and level)
			LeafOrdering                m_ordering;     ///< Octree Leaf Node Ordering

			// Distance-Based parameter
			b8 m_useDistanceBasedMethod;                         ///< 距離に基づく細分化を行うか
			std::vector<SubdivisionMargin> m_subdivisionMargins; ///< 分割パラメータ

			ExportPolicy m_exportPolicy;               ///< File Export Policy (Paramter or Octree)

			BCMOctreeInformation m_information;        ///< 距離に基づく細分化の制御パラメータ
			
			//u64 m_numBCMOctreeNode;                    ///< for Monitoring Memory
			
			std::string m_marginType;					   ///< Magin Type (Cell or Ratio) Global Setting
			f32 m_marginRatio;							   ///< Magin Ratio Global Setting
			f32 m_marginCell;							   ///< Magin Cell Global Setting

		};

	} // namespace SG

} // namespace VX



#endif // INCLUDE_VX_SCENEGRAPH_DOMAIN_BCM

