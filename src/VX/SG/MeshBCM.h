/*
 * VX/SG/CellIDMeshBCM.h
 *
 * SceneGraphLibrary
 *
 */

#ifndef INCLUDE_VXSCENEGRAPH_MESH_BCM_H
#define INCLUDE_VXSCENEGRAPH_MESH_BCM_H

#include "../Type.h"
#include "../Math.h"

#include "Node.h"
#include "Group.h"
#include "Geometry.h"

#include "MeshUtil.h"
#include "../../VOX/Pedigree.h"

#include <string>
#include <vector>

namespace VOX {
	class BCMOctree;
}

namespace VX
{
	namespace SG
	{
		class MeshBCM;
		class MeshBCMSlice;		

		class MeshBCMSlice : public Node
		{
		public:
			MeshBCMSlice( const u32 axis, const MeshBCM* mesh );
			~MeshBCMSlice();

			const Geometry* GetFaceGeometry() const
			{
				return m_face;
			}

			const Geometry* GetBoundaryGeometry() const 
			{
				return m_boundary;
			}

			const Geometry* GetGridGeometry() const
			{
				return m_grid;
			}

			const Geometry* GetBlockGeometry() const
			{
				return m_block;
			}

			void SetPosition(const f32 pos, const u32 index_coord, const b8 boundaryOnly = false);
			f32 GetPosition() const;

			b8 GetSelection() const { return false; }
			void SetSelection(b8 select, b8 flagOnly = false) { }
		
		private:
			Texture* CreateTexture(const u32 maxBlocks, const VX::Math::idx2& blockUV);

			const MeshBCM* m_mesh;
			const u32      m_axis;
			
			NodeRefPtr<Geometry> m_face;
			NodeRefPtr<Geometry> m_block;
			NodeRefPtr<Geometry> m_grid;
			NodeRefPtr<Geometry> m_boundary;

			u32 m_maxBlocks;
		};

		class MeshBCM : public Group
		{
		public:
			MeshBCM( const VX::Math::vec3& origin,   const VX::Math::vec3& region,
					 const VX::Math::idx3& rootDims, const VX::Math::idx3& blockSize,
					 u8 bitWidth,
					 const std::vector<VOX::Pedigree>& pedigrees,
					 const std::vector<Block*>& blocks  );

			~MeshBCM();

			const VOX::BCMOctree* GetOctree() const 
			{
				return m_octree;
			}

			const VX::Math::idx3& GetBlockSize() const 
			{
				return m_blockSize;
			}

			const VX::Math::vec3& GetOrigin() const 
			{
				return m_org;
			}

			const VX::Math::vec3& GetRegion() const 
			{
				return m_rgn;
			}
			const VX::Math::vec3& GetPitch() const
			{
				return m_pitch;
			}

			u8 GetBitWidth() const
			{
				return m_bitWidth;
			}

			u32 GetMaxOctreeLevel() const
			{
				return m_maxLevel;
			}

			const VX::Math::vec3& GetMin() const 
			{
				return m_org;
			}
			
			const VX::Math::vec3 GetMax() const
			{
				return (m_org + m_rgn);
			}

			const Block* GetBlock(size_t i) const
			{
				return m_blocks[i];
			}

			u32 GetIdxPosition(const u32 axis, const f32 coord_pos) const
			{
				const u32  numIdx = m_rootDims[axis] * ( 1 << m_maxLevel ) * m_blockSize[axis];

				u32 posIdx = static_cast<u32>( (coord_pos - m_org[axis]) / m_pitch[axis] );
				posIdx = static_cast<s32>(posIdx) >= numIdx ? numIdx-1 : posIdx;

				return posIdx;
			}

			f32 GetCoordPosition(const u32 axis, const u32 idx_pos) const
			{
				f32 posCoord = m_org[axis] + m_pitch[axis] * idx_pos;
				return posCoord;
			}

			void SetSlicePosition(const u32 axis, const f32 pos, const u32 index_coord, const b8 boundaryOnly)
			{
				m_slices[axis]->SetPosition(pos, index_coord, boundaryOnly);
			}

			f32 GetSlicePosition(const u32 axis) const 
			{
				return m_slices[axis]->GetPosition();
			}


		private:
			const VX::Math::vec3 m_org;
			const VX::Math::vec3 m_rgn;
			const VX::Math::idx3 m_rootDims;
			const VX::Math::idx3 m_blockSize;
			const u8 m_bitWidth;

			VOX::BCMOctree* m_octree;
			VX::Math::vec3 m_pitch;
			u32 m_maxLevel;

			std::vector<Block*> m_blocks;

			NodeRefPtr<MeshBCMSlice> m_slices[3];	

			u64 m_numUseBlockMemory;
			u64 m_numBCMOctreeNode;
		};

	} // namespace SG

} // namespace VX

#endif // INCLUDE_VXSCENEGRAPH_MESH_BCM_H

