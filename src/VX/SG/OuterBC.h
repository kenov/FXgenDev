/*
 * VX/SG/OuterBC.h
 *
 * SceneGraph Library
 *
 */

#ifndef INCLUDE_VXSCENEGRAPH_OUTERBC_H
#define INCLUDE_VXSCENEGRAPH_OUTERBC_H

#include "../Type.h"
#include "../Math.h"
#include "Node.h"
#include "OuterBCClass.h"

#include <string>

namespace VX
{
	namespace SG
	{
		class OuterBCClass;
		
		class OuterBC : public Node
		{
		public:
			OuterBC(const std::string& alias = "defname",
					const OuterBCClass* bcclass = 0,
					const std::string& medium = "")
			: Node(NODETYPE_OUTERBC)
			{
				m_alias = alias;
				m_class = bcclass;
				m_medium = medium;
			}

			~OuterBC(){}
			
			void SetAlias(const std::string& alias)
			{
				m_alias = alias;
			}
			
			const std::string& GetAlias() const
			{
				return m_alias;
			}

			void SetMedium(const std::string& medium)
			{
				m_medium = medium;
			}
			
			const std::string& GetMedium() const
			{
				return m_medium;
			}

			const std::string GetAbsoluteName() const
			{
				if( m_class != 0 ){
					return m_alias + std::string(" [") + m_class->GetName() + std::string("]");
				}else{
					return GetAlias();
				}
			}
			
			void SetClass(VX::SG::OuterBCClass* bcclass)
			{
				m_class = bcclass;
			}
			
			const VX::SG::OuterBCClass* GetClass() const
			{
				return m_class;
			}
			
			b8 GetSelection() const { return false; };
			void SetSelection(b8 select, b8 flagonly = false){};
			
		private:
			
			std::string          m_alias;
			std::string			 m_medium;
			const OuterBCClass*  m_class;

		};
		
	} // namespace SG
		
} // namespace VX

#endif // INCLUDE_VXSCENEGRAPH_OUTERBC_H
