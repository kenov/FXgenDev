/*
 * VX/SG/OuterBCClass.h
 *
 * SceneGraph Library
 *
 */

#ifndef INCLUDE_VXSCENEGRAPH_OUTERBCCLASS_H
#define INCLUDE_VXSCENEGRAPH_OUTERBCCLASS_H

#include "../Type.h"
#include "Node.h"

#include <string>

namespace VX
{
	namespace SG
	{
		class OuterBCClass : public Node
		{
		public:
			OuterBCClass(const std::string& label)
			 : Node(NODETYPE_OUTERBCCLASS)
			{
				m_label = label;
				m_hasThermalOption = false;
			}
			
			OuterBCClass(const std::string& label, const std::string& thermalOption)
			 : Node(NODETYPE_OUTERBCCLASS)
			{
				m_label = label;
				m_thermalOption = thermalOption;
				m_hasThermalOption = true;
			}

			~OuterBCClass(){}
			
			const std::string GetName() const
			{
				if( m_hasThermalOption ){
					return m_label + std::string(" (") + m_thermalOption + std::string(") ");
				}
				else{
					return GetLabel();
				}
			}

			const std::string& GetLabel() const
			{
				return m_label;
			}

			const std::string& GetThermalOption() const
			{
				return m_thermalOption;
			}
			
			b8 HasThermalOption() const 
			{
				return m_hasThermalOption;
			}

			
			b8 GetSelection() const { return false; };
			void SetSelection(b8 select, b8 flagonly = false){};
			
		private:
			std::string m_label;
			std::string m_thermalOption;
			b8 m_hasThermalOption;
		};
		
	} // namespace SG
		
} // namespace VX

#endif // INCLUDE_VXSCENEGRAPH_OUTERBCCLASS_H
