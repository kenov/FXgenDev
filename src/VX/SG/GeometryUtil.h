/*
 * VX/SG/GeometryUtil.h
 *
 */
/// @file GeometryUtil.h
/// 距離計算, 交差判定の関数群.
///

#ifndef INCLUDE_VXSCENEGRAPH_GEOMETRY_UTIL_H
#define INCLUDE_VXSCENEGRAPH_GEOMETRY_UTIL_H

#include "../Type.h"
#include "../Math.h"

/// AABB と三角形間の交差を判定する
///
/// @param[in] center AABB の中心
/// @param[in] halfextent AABB の各軸方向のサイズ. 全体の大きさは halfextent * 2 となる
/// @param[in] triangle 三角形. 3要素の配列を指定する
/// @return 交差している場合 true
///
b8 TestIntersectionAABBTriangle(
	const VX::Math::vec3& center,
	const VX::Math::vec3& halfextent,
	const VX::Math::vec3* triangle);

/// OBB と三角形間の交差を判定する
///
/// 未テスト
///
/// @param[in] center OBB の中心
/// @param[in] halfextent OBB の各軸方向のサイズ
/// @param[in] basic OBB の各軸方向を表す正規化済みベクトル
/// @param[in] triangle 三角形. 3要素の配列を指定する
/// @return 交差している場合 true
///
b8 TestIntersectionOBBTriangle(
	const VX::Math::vec3& center,
	const VX::Math::vec3& halfextent,
	const VX::Math::vec3* basis,
	const VX::Math::vec3* triangle);

/// 点と AABB 間の距離の二乗を計算する.
///
/// @param[in] point 点
/// @param[in] center AABB の中心
/// @param[in] halfextent AABB の各軸方向のサイズ.
/// @param[in] cutoff 打ち切り距離の二乗. これ以上の距離については計算を行わない
/// @param[out] closest cutfoff 以下の点が存在する場合の最近点
/// @return 最近点までの距離の二乗. cutoff 以下の点が存在しない場合 cutoff を返す
///
f32 DistanceSqPointAABB(
	const VX::Math::vec3& point,
	const VX::Math::vec3& center,
	const VX::Math::vec3& halfextent,
	f32 cutoff,
	VX::Math::vec3& closest);

/// 点と三角形間の距離の二乗を計算する.
///
/// @param[in] point 点
/// @param[in] triangle 三角形. 3要素の配列を指定する
/// @param[in] cutoff 打ち切り距離の二乗. これ以上の距離については計算を行わない
/// @param[out] closest cutfoff 以下の点が存在する場合の最近点
/// @return 最近点までの距離の二乗. cutoff 以下の点が存在しない場合 cutoff を返す
///
f32 DistanceSqPointTriangle(
	const VX::Math::vec3& point,
	const VX::Math::vec3* triangle,
	f32 cutoff,
	VX::Math::vec3& closest);

/// 点と点群間の距離の二乗を計算する.
/// 
/// @param[in] point 点
/// @param[in] points 点群
/// @param[in] count 点群の数
/// @param[in] cutoff 打ち切り距離の二乗. これ以上の距離については計算を行わない
/// @param[out] closest cutfoff 以下の点が存在する場合の最近点
/// @return 最近点までの距離の二乗. cutoff 以下の点が存在しない場合 cutoff を返す
///
f32 DistanceSqPointPoints(
	const VX::Math::vec3& point,
	const VX::Math::vec3* points, u32 count,
	f32 cutoff,
	VX::Math::vec3& closest);

/// OBB を内包する最小の AABB を計算する.
///
/// @param[in] center OBB の中心
/// @param[in] halfextent OBB の各軸方向のサイズ
/// @param[in] basic OBB の各軸方向を表す正規化済みベクトル
/// @param[out] aabb_min OBB を内包する AABB の最小座標
/// @param[out] aabb_max OBB を内包する AABB の最大座標
///
void UpdateBounding(
	const VX::Math::vec3& center,
	const VX::Math::vec3& halfextent,
	const VX::Math::vec3* basis,
	VX::Math::vec3& aabb_min,
	VX::Math::vec3& aabb_max);

#endif // INCLUDE_VXSCENEGRAPH_GEOMETRY_UTIL_H
