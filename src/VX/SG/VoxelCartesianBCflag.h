/*
 * VX/SG/CellIDVoxelCartesian.h
 *
 * SceneGraphLibrary
 *
 */

#ifndef INCLUDE_VoxelCartesianBCflag_H
#define INCLUDE_VoxelCartesianBCflag_H

#include "Node.h"
#include "VoxelCartesianL_Scan.h"
#include "../Math.h"
#include <vector>
#include <map>
namespace VX
{
	namespace SG
	{


		#define FLG_MASK5 (0x1f)
		#define FLG_MASK1 (0x1)

		struct BoundaryFlag{
			u8 Active;
			u8 State;
			u8 BC_Face_T;
			u8 BC_Face_B;
			u8 BC_Face_N;
			u8 BC_Face_S;
			u8 BC_Face_E;
			u8 BC_Face_W;

			//³k
			u32 toUInt32(){
				u32 d=0;
				/*
	#ifdef _DEBUG
				int bitWidth = 5;
				unsigned char mask2 = 0;
				for(int i = 0; i < bitWidth; i++){
					mask2 += (1 << i);
				}
				assert(FLG_MASK5==mask2);
	#endif
			*/
				d += (BC_Face_W & FLG_MASK5) << 0;
				d += (BC_Face_E & FLG_MASK5) << 5;
				d += (BC_Face_S & FLG_MASK5) << 10;
				d += (BC_Face_N & FLG_MASK5) << 15;
				d += (BC_Face_B & FLG_MASK5) << 20;
				d += (BC_Face_T & FLG_MASK5) << 25;
				//1bit
				d += (State  & FLG_MASK1) << 30;
				d += (Active & FLG_MASK1) << 31;

				return d;
			};

			//ð
			void fromUInt32(const u32& d){
						
				BC_Face_W = (d >> 0) & FLG_MASK5;
				BC_Face_E = (d >> 5) & FLG_MASK5;
				BC_Face_S = (d >> 10) & FLG_MASK5;
				BC_Face_N = (d >> 15) & FLG_MASK5;
				BC_Face_B = (d >> 20) & FLG_MASK5;
				BC_Face_T = (d >> 25) & FLG_MASK5;
				State = (d >> 30) & FLG_MASK1;
				Active = (d >> 31) & FLG_MASK1;

			};

		};

		struct BcTri{
			public:
			VX::Math::vec3 v0;
			VX::Math::vec3 v1;
			VX::Math::vec3 v2;
			u8 mBcflag;// 0 - 31

			void SetVal(const VX::Math::vec3& p0,const VX::Math::vec3& p1,const VX::Math::vec3& p2,const u8& b ){
				v0 = p0;
				v1 = p1;
				v2 = p2;
				mBcflag = b;
			};
		};


		class VoxelCartesianL;
		class GeometryBVH;


		class VoxelCartesianBCflag : public Node
		{
		public:
			VoxelCartesianBCflag( const VoxelCartesianL* local );
			~VoxelCartesianBCflag();

			const GeometryBVH* GetGridGeometry() const 
			{
				return m_geo;
			}
			GeometryBVH* GetGridGeometry() 
			{
				return m_geo;
			}
			b8 GetSelection() const;
			void SetSelection(b8 select, b8 flagonly = false);


			void ChangeShowBCFlag(const bool& islocal_load, const bool& islocal_staging,const std::vector<u8>& BCflags);

			void Load(const std::vector<u8>& BCflags);
			void UnLoad();
			bool IsLoaded()const;
            void Scan( VoxelCartesianL_Scan& vals);
			const VoxelCartesianL_Scan& GetScanResult()const;

			static float GetBCflgTriOffsetScale();
			static void SetBCflgTriOffsetScale(const float& scale);

		private:

			static float m_bcflgTriOffsetScale;

        private:
			
			//GLSLp|Sì¬
			GeometryBVH* createGeoByTri(const std::vector<BcTri>& triVec);
			bool isConstCellIDValueDef(s32& cellid);
			void genTris(const std::vector<u8>& BCflags , std::vector<BcTri>& triVec);
			void addTriByEachVoxel(const std::vector<u8>& guiBCs,
												const BoundaryFlag& bcflg ,
												const VX::Math::vec3& origin,
												const VX::Math::vec3& pitch,
												const VX::Math::idx3& bPos,
												std::vector<BcTri>& triVec);
		private:
			const VoxelCartesianL*  m_local;
			VoxelCartesianL_Scan m_bcf_scan_result;

			GeometryBVH* m_geo;

		};

	} // namespace SG

} // namespace VX

#endif // INCLUDE_VXSCENEGRAPH_MESH_CARTESIAN_H

