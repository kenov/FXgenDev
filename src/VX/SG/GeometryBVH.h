/*
 * VX/SG/GeometryBVH.h
 *
 * SceneGraph Library
 *
 */

#ifndef INCLUDE_VXSCENEGRAPH_GEOMETRY_BVH_H
#define INCLUDE_VXSCENEGRAPH_GEOMETRY_BVH_H

#include "../Log.h"
#include "../Type.h"
#include "../Time.h"
#include "../Math.h"
#include "Geometry.h"
#include "GeometryUtil.h"
#include <algorithm>
#include <vector>
#include <cmath>

#define comp_less1(a_x,b_x)                 ((a_x) < (b_x))
#define comp_less2(a_x,a_y,b_x,b_y)         (comp_less1(a_x,b_x) || (!comp_less1(b_x,a_x) &&(a_y)<(b_y)))
#define comp_less3(a_x,a_y,b_x,b_y,a_z,b_z) (comp_less2(a_x,a_y,b_x,b_y) || (!comp_less2(b_x,b_y,a_x,a_y) && (a_z) < (b_z)))


namespace VX
{
	namespace SG
	{
		class HitGeometry
		{
		public:
			HitGeometry(Geometry* geo, Index index, f32 hit_t , const VX::Math::vec3& v0 , const VX::Math::vec3& v1, const VX::Math::vec3& v2)
			{
				m_geo   = geo;
				m_index = index;
				m_hit_t = hit_t;

				m_v0 = v0;
				m_v1 = v1;
				m_v2 = v2;

			}
			bool IsNull() const
			{
				return (m_geo==NULL);
			}
			b8 operator<(const HitGeometry& t) const
			{
				return m_hit_t < t.m_hit_t;
			}
			 
			Geometry* m_geo;
			Index m_index;
			f32 m_hit_t;
			VX::Math::vec3 m_v0;
			VX::Math::vec3 m_v1;
			VX::Math::vec3 m_v2;
			
			virtual ~HitGeometry(){

			}
		
			HitGeometry(){
				Clear();
			}
		
			void Clear(){
			
				m_geo=NULL;
				m_index = 0;
				m_hit_t= 0;
				m_v0.x = 0;
				m_v0.y = 0;
				m_v0.z = 0;
				m_v1.x = 0;
				m_v1.y = 0;
				m_v1.z = 0;
				m_v2.x = 0;
				m_v2.y = 0;
				m_v2.z = 0;

			}		
			//	コピーコンストラクタ
			HitGeometry( const HitGeometry& obj )
			{
				assignCopy(obj);
			}

			HitGeometry& operator=(const HitGeometry& obj)
			{
				if(this == &obj){
					return *this;   //自己代入
				}
				assignCopy(obj);
				return(*this);
			}	
			private:
			void assignCopy(const HitGeometry& obj)
			{
				m_geo	= obj.m_geo;
				m_index	= obj.m_index;
				m_hit_t	= obj.m_hit_t;
				m_v0	= obj.m_v0;
				m_v1	= obj.m_v1;
				m_v2	= obj.m_v2;
			}
			
		};
		
		class GeometryBVH : public Geometry
		{
		private:
			class TriID {
			public:
				VX::Math::vec3 barycentric; // 12
				u32 idx_id;                 // 4
			};
			class BVH {
			public:
				VX::Math::vec3 m_max, m_min; // 24
				VX::Math::vec4 m_sphere;     // 16
				
				enum {
					BRANCH_NODE = 0,
					LEAF_NODE   = (1<<4)
				};
				u32 m_leaf_kids;//1-4bit
				union {
					u32 m_idxid[4];// 16
					BVH* m_bvhid[2];//16
				};
				
				u32 debug_id;
			};
			BVH* m_bvh;
			
			static const b8 less_x(const TriID& a, const TriID& b) { return a.barycentric.x < b.barycentric.x; }
			static const b8 less_y(const TriID& a, const TriID& b) { return a.barycentric.y < b.barycentric.y; }
			static const b8 less_z(const TriID& a, const TriID& b) { return a.barycentric.z < b.barycentric.z; }
			u32 zsort(TriID* vec, u32 num, u32 axis) const
			{
				if (num <= 4)
					return 1;

				static const b8 (*cmpfunc[])(const TriID& a, const TriID& b) = {less_x, less_y, less_z};
				u32 hl = (num >> 1);
				std::nth_element(vec, vec + hl, vec + num, cmpfunc[axis]);

				axis = (axis + 1) % 3;
				const u32 c0 = zsort(vec     , hl      , axis);
				const u32 c1 = zsort(vec + hl, num - hl, axis);
				return c0 + c1 + 1;
			}

			BVH* buildBVH(BVH** bvh, TriID* vec, u32 num)
			{
				using namespace VX::Math;
				BVH* node = (*bvh)++; // depht first order
								
				// calc child bvh
				if (num <= 4)
				{
					vec3 bmax(-FLT_MAX, -FLT_MAX, -FLT_MAX);
					vec3 bmin( FLT_MAX,  FLT_MAX,  FLT_MAX);
					const VertexFormat* vertex = GetVertex();
					const Index* index = GetIndex();
					u32 i;
					u32 kids = 0;
					for (i = 0; i < num; ++i)
					{
						const u32 idxid = vec[i].idx_id;
						const vec3& v0 = vertex[index[idxid    ]].pos;
						const vec3& v1 = vertex[index[idxid + 1]].pos;
						const vec3& v2 = vertex[index[idxid + 2]].pos;
						bmax = VX::Math::Maximize(bmax, v0);
						bmax = VX::Math::Maximize(bmax, v1);
						bmax = VX::Math::Maximize(bmax, v2);
						bmin = VX::Math::Minimize(bmin, v0);
						bmin = VX::Math::Minimize(bmin, v1);
						bmin = VX::Math::Minimize(bmin, v2);
						node->m_idxid[i] = idxid;
						kids = (kids << 1) | 1;
					}
					while (i < 4)
					{
						node->m_idxid[i] = -1;
						++i;
					}
					node->m_max = bmax;
					node->m_min = bmin;
					node->m_sphere = vec4((bmax + bmin) * 0.5, length(bmax - bmin) * 0.5f);
					node->m_leaf_kids = kids | BVH::LEAF_NODE;
					return node;
				}
				else
				{
					const u32 hl = (num >> 1);
					BVH* b0 = buildBVH(bvh, vec     , hl      );
					BVH* b1 = buildBVH(bvh, vec + hl, num - hl);
					
					const vec3& omax = Maximize(b0->m_max, b1->m_max);
					const vec3& omin = Minimize(b0->m_min, b1->m_min);
					node->m_max = omax;
					node->m_min = omin;
					node->m_sphere = vec4((omax + omin) * 0.5, length(omax - omin) * 0.5f);
					node->m_bvhid[0] = b0;//static_cast<u32>(b0 - ownnode);
					node->m_bvhid[1] = b1;//static_cast<u32>(b1 - ownnode);
					node->m_leaf_kids = 3;
					return node;
				}					
			}
			
			/*
			void test_bvh_color(const BVH* bvh, u32 bvhnum)
			{
				VertexFormat* vertex = GetVertex();
				Index* index = GetIndex();
				if (bvh->m_leaf_kids & BVH::LEAF_NODE)
				{
					VX::Math::vec3 col = VX::Math::HSVtoRGB((bvh->debug_id/static_cast<double>(bvhnum)*360.0), 1, 1);
					const u32 tcol = static_cast<u32>(col.x * 255) | (static_cast<u32>(col.y * 255)<<8) | (static_cast<u32>(col.z * 255)<<16) | 0xFF000000;
					for (u32 i = 0; i < 4; ++i) {
						if ((bvh->m_leaf_kids >> i) & 1) {
							//							++tcnt;
							const u32 triid = bvh->m_idxid[i];
							vertex[index[triid  ]].col = tcol;
							vertex[index[triid+1]].col = tcol;
							vertex[index[triid+2]].col = tcol;
						}
					}
				}
				else // Branch
				{
					if (bvh->m_leaf_kids & 1)
						test_bvh_color(bvh->m_bvhid[0], bvhnum);
					if ((bvh->m_leaf_kids >> 1) & 1)
						test_bvh_color(bvh->m_bvhid[1], bvhnum);
				}
			}
			 */
			static inline bool intersect(const VX::Math::vec3* bb_minmax, const VX::Math::vec3& org,
										 const VX::Math::vec3& invDir, const u32 dirIsNeg[3], f32& hitmin) {
				// Check for ray intersection against $x$ and $y$ slabs
				float tmin =  (bb_minmax[    dirIsNeg[0]].x - org.x) * invDir.x;
				float tmax =  (bb_minmax[1 - dirIsNeg[0]].x - org.x) * invDir.x;
				const float tymin = (bb_minmax[    dirIsNeg[1]].y - org.y) * invDir.y;
				const float tymax = (bb_minmax[1 - dirIsNeg[1]].y - org.y) * invDir.y;
				if ((tmin > tymax) || (tymin > tmax))
					return false;
				if (tymin > tmin) tmin = tymin;
				if (tymax < tmax) tmax = tymax;
				
				// Check for ray intersection against $z$ slab
				const float tzmin = (bb_minmax[    dirIsNeg[2]].z - org.z) * invDir.z;
				const float tzmax = (bb_minmax[1 - dirIsNeg[2]].z - org.z) * invDir.z;
				if ((tmin > tzmax) || (tzmin > tmax))
					return false;
				if (tzmin > tmin)
					tmin = tzmin;
				if (tzmax < tmax)
					tmax = tzmax;
				b8 r = (tmin < hitmin) && (tmax > 0);
				hitmin = tmin;
				return r;
			}
			
			b8 testAABB(const VX::Math::vec3* orgdir, const VX::Math::vec3& aabb_max, const VX::Math::vec3& aabb_min, f32& hitmin) const
			{
				const VX::Math::vec3 invDir(1.0f / orgdir[1].x, 1.0f / orgdir[1].y, 1.0f / orgdir[1].z);
				u32 dirIsNeg[3] = { invDir.x < 0, invDir.y < 0, invDir.z < 0 };
				const VX::Math::vec3 bb_minmax[] = {aabb_min, aabb_max};
				return intersect(bb_minmax, orgdir[0], invDir, dirIsNeg, hitmin);
			}
			b8 testTriangle(const VX::Math::vec3& v0, const VX::Math::vec3& v1, const VX::Math::vec3& v2, const VX::Math::matrix& mat, const VX::Math::vec4* frustum) const
			{
				const VX::Math::vec4 tv0 = mat * VX::Math::vec4(v0, 1.0);
				const VX::Math::vec4 tv1 = mat * VX::Math::vec4(v1, 1.0);
				const VX::Math::vec4 tv2 = mat * VX::Math::vec4(v2, 1.0);
				const s32 frustum_num = 4;
				s32 inner = 0;
				for (s32 i = 0; i < frustum_num; ++i)
				{
					const f32 d0 = dot(frustum[i], tv0);
					const f32 d1 = dot(frustum[i], tv1);
					const f32 d2 = dot(frustum[i], tv2);
					if (d0 <= 0 && d1 <= 0 && d2 <=0)
						++inner;
				}
				if (inner == frustum_num)
					return true;
				else
					return false;
			}

			b8 testBBox(const VX::Math::vec3* A_max, const VX::Math::vec3* A_min, const VX::Math::vec3* B_max, const VX::Math::vec3* B_min) const
			{
				for(int i = 0; i < 3; i++){
					if( (*A_min)[i] > (*B_max)[i] || (*A_max)[i] < (*B_min)[i] ){
						return false;
					}
				}
				return true;
			}
		

			b8 intersect_rec(BVH* bvh, const VX::Math::vec3* orgdir, std::vector<Index>& tribuf) const
			{
				f32 hitmin = 10000.0f;
				if (!testAABB(orgdir, bvh->m_max, bvh->m_min, hitmin))
					return false;
				
				b8 hit = false;
				if (bvh->m_leaf_kids & BVH::LEAF_NODE)
				{
					for (u32 i = 0; i < 4; ++i) {
						if ((bvh->m_leaf_kids >> i) & 1) {
							const u32 idxid = bvh->m_idxid[i];
							tribuf.push_back(idxid);
						}
					}
					hit = true;
				}
				else // Branch
				{
					if (bvh->m_leaf_kids & 1)
						hit |= intersect_rec(bvh->m_bvhid[0], orgdir, tribuf);
					if ((bvh->m_leaf_kids >> 1) & 1)
						hit |= intersect_rec(bvh->m_bvhid[1], orgdir, tribuf);
				}
				return hit;
			}
			
			b8 intersect_frustum_rec(BVH* bvh, const VX::Math::vec4* frustum, const Math::matrix& mat, std::vector<Index>& tribuf, b8 including = false) const
			{
				s32 inner = 0;
				const s32 frustum_num = 4;
				if (including)
				{
					inner = frustum_num;
				}
				else
				{
					for (s32 i = 0; i < frustum_num; ++i)
					{
						VX::Math::vec4 sph = mat * VX::Math::vec4(bvh->m_sphere.xyz(),1.0f);
						sph.w = 1.0f;
						f32 d = dot(frustum[i], sph);
						if (d > m_bvh->m_sphere.w)
							return false;
						if (d <= -m_bvh->m_sphere.w)
							++inner;
					}
				}
				
				b8 hit = 0;
				if (bvh->m_leaf_kids & BVH::LEAF_NODE)
				{
					for (u32 i = 0; i < 4; ++i) {
						if (!((bvh->m_leaf_kids >> i) & 1))
							break;
						
						const VertexFormat* v = GetVertex();
						const Index* index = GetIndex();
						const u32 idxid = bvh->m_idxid[i];
						if (inner == frustum_num
						||  testTriangle(v[index[idxid]].pos, v[index[idxid+1]].pos, v[index[idxid+2]].pos, mat, frustum))
						{
							tribuf.push_back(idxid);
						}
					}
					hit = true;
				}
				else // Branch
				{
					including = (inner == frustum_num ? true : false);
					if (bvh->m_leaf_kids & 1)
						hit |= intersect_frustum_rec(bvh->m_bvhid[0], frustum, mat, tribuf, including);
					if ((bvh->m_leaf_kids >> 1) & 1)
						hit |= intersect_frustum_rec(bvh->m_bvhid[1], frustum, mat, tribuf, including);
				}
				return hit;
			}

			// for build Octree
			b8 intersect_bbox_rec(BVH* bvh, const VX::Math::vec3* max, const VX::Math::vec3* min) const
			{
				if ( !testBBox(max, min, &bvh->m_max, &bvh->m_min) ) // Not hit
					return false;
				
				b8 hit = false;
				if( (bvh->m_leaf_kids & BVH::LEAF_NODE) ) // LEAF
				{
					const Geometry::VertexFormat* v = GetVertex();
					const Index* index = GetIndex();
					for (u32 i = 0; i < 4; ++i) {
						if ((bvh->m_leaf_kids >> i) & 1) {
							const u32 idxid = bvh->m_idxid[i];

							const VX::Math::vec3& v0 = v[index[idxid + 0]].pos;
							const VX::Math::vec3& v1 = v[index[idxid + 1]].pos;
							const VX::Math::vec3& v2 = v[index[idxid + 2]].pos;
							VX::Math::vec3 bmax(-FLT_MAX, -FLT_MAX, -FLT_MAX);
							VX::Math::vec3 bmin( FLT_MAX,  FLT_MAX,  FLT_MAX);
							bmax = VX::Math::Maximize(bmax, v0);
							bmax = VX::Math::Maximize(bmax, v1);
							bmax = VX::Math::Maximize(bmax, v2);
							bmin = VX::Math::Minimize(bmin, v0);
							bmin = VX::Math::Minimize(bmin, v1);
							bmin = VX::Math::Minimize(bmin, v2);
							if (testBBox(&bmax, &bmin, max, min)){
								hit = true;
								break;
							}
						}
					}
				} else // BRANCH
				{
					if (bvh->m_leaf_kids & 1)
						hit |= intersect_bbox_rec(bvh->m_bvhid[0], max, min);
					if ((bvh->m_leaf_kids >> 1) & 1)
						hit |= intersect_bbox_rec(bvh->m_bvhid[1], max, min);
				}
				return hit;
			}

			// for build Octree (Distance-Based)
			b8 intersect_aabb_rec(BVH* bvh, const Math::vec3& bmin, const Math::vec3& bmax, const Math::vec3& center, const Math::vec3& halfextent) const
			{
				using Math::vec3;
				if (!testBBox(&bmax, &bmin, &bvh->m_max, &bvh->m_min)) return false;

				b8 hit = false;
				if (bvh->m_leaf_kids & BVH::LEAF_NODE)
				{
					const Geometry::VertexFormat* v = GetVertex();
					const Index* index = GetIndex();
					for (u32 i = 0; i < 4; ++i)
					{
						if ((bvh->m_leaf_kids >> i) & 1)
						{
							const u32 idxid = bvh->m_idxid[i];
							const vec3 tri[] = { v[index[idxid + 0]].pos, v[index[idxid + 1]].pos, v[index[idxid + 2]].pos };
							if (TestIntersectionAABBTriangle(center, halfextent, tri))
							{
								hit = true;
								break;
							}
						}
					}
				}
				else
				{
					if (!hit && ((bvh->m_leaf_kids >> 0) & 1))
						hit |= intersect_aabb_rec(bvh->m_bvhid[0], bmin, bmax, center, halfextent);
					if (!hit && ((bvh->m_leaf_kids >> 1) & 1))
						hit |= intersect_aabb_rec(bvh->m_bvhid[1], bmin, bmax, center, halfextent);
				}
				return hit;
			}

			// for build Octree (Distance-Based)
			b8 intersect_ellipsoid_rec(BVH* bvh, const Math::vec3& bmin, const Math::vec3& bmax, const Math::vec3& offset, const Math::vec3& scale) const
			{
				const f32 SQRT3 = 1.7320508075688772935274463415059f; // = sqrt(1^2 + 1^2 + 1^2)

				using Math::vec3;
				if (!testBBox(&bmax, &bmin, &bvh->m_max, &bvh->m_min)) return false;

				b8 hit = false;
				if (bvh->m_leaf_kids & BVH::LEAF_NODE)
				{
					const Geometry::VertexFormat* v = GetVertex();
					const Index* index = GetIndex();
					for (u32 i = 0; i < 4; ++i)
					{
						if ((bvh->m_leaf_kids >> i) & 1)
						{
							const u32 idxid = bvh->m_idxid[i];
							const vec3 tri[] = {
								(v[index[idxid + 0]].pos + offset) * scale,
								(v[index[idxid + 1]].pos + offset) * scale,
								(v[index[idxid + 2]].pos + offset) * scale };
							vec3 dummy;
							if (DistanceSqPointTriangle(vec3(0,0,0), tri, SQRT3, dummy) < SQRT3)
							{
								hit = true;
								break;
							}
						}
					}
				}
				else
				{
					if (!hit && ((bvh->m_leaf_kids >> 0) & 1))
						hit |= intersect_ellipsoid_rec(bvh->m_bvhid[0], bmin, bmax, offset, scale);
					if (!hit && ((bvh->m_leaf_kids >> 1) & 1))
						hit |= intersect_ellipsoid_rec(bvh->m_bvhid[1], bmin, bmax, offset, scale);
				}
				return hit;
			}

			// for build Octree (Distance-Based)
			b8 intersect_obb_rec(BVH* bvh, const Math::vec3& bmin, const Math::vec3& bmax, const Math::vec3& center, const Math::vec3& halfextent, const Math::vec3* basis) const
			{
				using Math::vec3;
				if (!testBBox(&bmax, &bmin, &bvh->m_max, &bvh->m_min)) return false;

				b8 hit = false;
				if (bvh->m_leaf_kids & BVH::LEAF_NODE)
				{
					const Geometry::VertexFormat* v = GetVertex();
					const Index* index = GetIndex();
					for (u32 i = 0; i < 4; ++i)
					{
						if ((bvh->m_leaf_kids >> i) & 1)
						{
							const u32 idxid = bvh->m_idxid[i];
							const vec3 tri[] = { v[index[idxid + 0]].pos, v[index[idxid + 1]].pos, v[index[idxid + 2]].pos };
							if (TestIntersectionOBBTriangle(center, halfextent, basis, tri))
							{
								hit = true;
								break;
							}
						}
					}
				}
				else
				{
					if (!hit && ((bvh->m_leaf_kids >> 0) & 1))
						hit |= intersect_obb_rec(bvh->m_bvhid[0], bmin, bmax, center, halfextent, basis);
					if (!hit && ((bvh->m_leaf_kids >> 1) & 1))
						hit |= intersect_obb_rec(bvh->m_bvhid[1], bmin, bmax, center, halfextent, basis);
				}
				return hit;
			}

			//
			f32 distance_sq_rec(BVH* bvh, const Math::vec3& point, f32 cutoff, Math::vec3& closest) const
			{
				using Math::vec3;

				vec3 center = (bvh->m_min + bvh->m_max) / 2;
				vec3 halfextent = (bvh->m_max - bvh->m_min) / 2;
				vec3 closestbbox;
				if (cutoff <= DistanceSqPointAABB(point, center, halfextent, cutoff, closestbbox))
				{
					return cutoff;
				}

				f32 ret = cutoff;
				if (bvh->m_leaf_kids & BVH::LEAF_NODE)
				{
					const Geometry::VertexFormat* v = GetVertex();
					const Index* index = GetIndex();
					for (u32 i = 0; i < 4; ++i)
					{
						if ((bvh->m_leaf_kids >> i) & 1)
						{
							const u32 idxid = bvh->m_idxid[i];
							const vec3 tri[] = { v[index[idxid + 0]].pos, v[index[idxid + 1]].pos, v[index[idxid + 2]].pos };
							vec3 closesttri;
							f32 d = DistanceSqPointTriangle(point, tri, ret, closesttri);
							//f32 d = DistanceSqPointPoints(point, tri, 3, ret, closesttri);
							if (d < ret)
							{
								ret = d;
								closest = closesttri;
							}
						}
					}
				}
				else
				{
					if ((bvh->m_leaf_kids >> 0) & 1)
					{
						f32 d = distance_sq_rec(bvh->m_bvhid[0], point, cutoff, closest);
						if (d < ret) ret = d;
					}
					if ((bvh->m_leaf_kids >> 1) & 1)
					{
						f32 d = distance_sq_rec(bvh->m_bvhid[1], point, cutoff, closest);
						if (d < ret) ret = d;
					}
				}
				return ret;
			}

			
		public:
			GeometryBVH() : Geometry()
			{
				m_bvh = 0;
			}
			~GeometryBVH()
			{
				vxdeleteArray(m_bvh);
			}
			
			// for build Octree
			b8 IntersectBBox(const Math::vec3* max, const Math::vec3* min) const
			{
				return intersect_bbox_rec(m_bvh, max, min);
			}

			// for build Octree (Distance-Based)

			/// AABB と形状の距離がしきい値以下かを判定する.
			///
			/// @param[in] min 判定する AABB の最小座標
			/// @param[in] max 判定する AABB の最大座標
			/// @return AABB への最近接距離がしきい値以下なら true
			///
			b8 TestDistanceAABB(const Math::vec3& min, const Math::vec3& max, f32 margin) const
			{
				using VX::Math::vec3;
				const vec3 m(margin, margin, margin);
				const vec3 bmin = min - m;
				const vec3 bmax = max + m;
				const vec3 center = (max + min) / 2;
				const vec3 halfextent = (max - min) / 2;

				// 近似距離判定
				#if 0
					// しきい値だけ拡大した AABB との交差判定
					//return intersect_aabb_rec(m_bvh, bmin, bmax, center, halfextent);
				#else
					// AABB を内包する楕円体との交差判定
					const f32 ratio = 1 + margin / length(halfextent);
					const vec3 offset = -center;
					const vec3 scale = 1 / (halfextent * ratio);
					return intersect_ellipsoid_rec(m_bvh, bmin, bmax, offset, scale);
				#endif
			}

			/// OBB と形状との交差を判定する.
			/// 未テスト
			/// 
			/// @param[in] center OBB の中心
			/// @param[in] halfextent OBB の各軸方向のサイズ
			/// @param[in] basic OBB の各軸方向を表す正規化済みベクトル. 3要素の配列を指定する
			/// @return 交差している場合 true
			///
			b8 IntersectOBB(const Math::vec3& center, const Math::vec3& halfextent, const Math::vec3* basis) const
			{
				VX::Math::vec3 min, max;
				UpdateBounding(center, halfextent, basis, min, max);
				return intersect_obb_rec(m_bvh, min, max, center, halfextent, basis);
			}

			// for distance field

			/// 点と形状間の距離を計算する.
			///
			/// @param[in] point 点の座標
			/// @return f32 形状への最近接距離
			///
			f32 DistanceFromPoint(const Math::vec3& point) const
			{
				Math::vec3 dummy;
				f32 distanceSq = distance_sq_rec(m_bvh, point, FLT_MAX, dummy);
				return std::sqrt(distanceSq);
			}

			/// 点と形状間の距離の二乗を計算する. 
			/// 距離しきい値内に形状が存在する場合は最近点を返す
			///
			/// @param[in] point 点の座標
			/// @param[in] cutoff 距離計算を打ち切るしきい値. 距離の二乗を指定する
			/// @param[out] closest cutfoff 以下の点が存在する場合に最近点を返す
			/// @return 形状への最近接距離. cutoff 以下の点が存在しない場合は cutoff を返す
			///
			f32 DistanceSqFromPoint(const Math::vec3& point, f32 cutoff, Math::vec3& closest) const
			{
				f32 distanceSq = distance_sq_rec(m_bvh, point, cutoff, closest);
				return distanceSq;
			}


			b8 IntersectFrustum(const Math::vec4* frustum, const Math::matrix& mat, b8 selection)
			{
				std::vector<Index> tribuf;
				b8 r = intersect_frustum_rec(m_bvh, frustum, mat, tribuf);

				const u32 tnum = static_cast<u32>(tribuf.size());
				u32* idb  = GetIDBuffer();
				if (selection) {
					for (u32 i = 0; i < tnum; ++i)
					{
						const Index idx = tribuf[i];
						idb[idx/3] |= 0x000000FF;
					}
				} else {
					for (u32 i = 0; i < tnum; ++i)
					{
						const Index idx = tribuf[i];
						idb[idx/3] &= 0xFFFFFF00;
					}
				}
				if (tnum) {
					m_needUpdate = true;
					CheckSelection();
				}
				return r;
			}

			// for multi geometry
			b8 Intersect(const VX::Math::vec3* orgdir, std::vector<HitGeometry>& hits)
			{
				std::vector<Index> tribuf;
				intersect_rec(m_bvh, orgdir, tribuf);
				
				b8 hit = false;
				const u32 tnum = static_cast<u32>(tribuf.size());
				const VertexFormat* v = GetVertex();
				const Index* index = GetIndex();
				f32 min_t     = FLT_MAX;
				Index min_idx = -1;
				VX::Math::vec3 mv0,mv1,mv2;
				for (u32 i = 0; i < tnum; ++i)
				{
					const Index idx = tribuf[i];
					const VX::Math::vec3& v0 = v[index[idx  ]].pos;
					const VX::Math::vec3& v1 = v[index[idx+1]].pos;
					const VX::Math::vec3& v2 = v[index[idx+2]].pos;
					f32 u,v, t = min_t;
					if (VX::Math::IntersectTriangle(orgdir[0], orgdir[1], v0, v1, v2, t, u, v) && min_t > t)
					{
						
						hit = true;
						min_t = t;
						min_idx = idx;

						mv0 = v0;
						mv1 = v1;
						mv2 = v2;
					}
				}
				
				if (hit)
					hits.push_back(HitGeometry(this, min_idx, min_t,mv0,mv1,mv2));
				
				return hit;
			}

			void Build()
			{
				const u32 indexnum = GetIndexCount();
				const u32 trinum = indexnum / 3;
				TriID* tri = vxnew TriID[trinum];

				VXLogD("triangle num = %d\n", trinum);
				
				using namespace VX::Math;
				const Geometry::VertexFormat* v = GetVertex();
				const Index* index = GetIndex();
				u32 tricount = 0;
				for (u32 i = 0; i < indexnum; i += 3)
				{
					const Geometry::VertexFormat& v1 = v[index[i  ]];
					const Geometry::VertexFormat& v2 = v[index[i+1]];
					const Geometry::VertexFormat& v3 = v[index[i+2]];
					tri[tricount].barycentric = (v1.pos + v2.pos + v3.pos) * (1.0f/3.0f);
					tri[tricount].idx_id = i;
					tricount++;
				}
				
				// first axis
				const vec3 blen = GetMax() - GetMin();
				u32 axis = 0;
				if      (blen.x < blen.y && blen.y > blen.z) { axis = 1; }
				else if (blen.x < blen.y && blen.y < blen.z) { axis = 2; }
				
				// sort
				f64 st_sort = VX::GetTimeCount();								
				const u32 bvh_nodenum = zsort(&tri[0], trinum, axis);
				f64 et_sort = VX::GetTimeCount();
						
				// Alloc BVH				
				vxdeleteArray(m_bvh);
				m_bvh = vxnew BVH[bvh_nodenum];
				for (u32 b = 0; b < bvh_nodenum; b++) // debug
				{
					m_bvh[b].debug_id = b;
				}

				// Build BVH
				BVH* bvhptr = m_bvh;
				buildBVH(&bvhptr, tri, trinum);
/*				u32 dif = static_cast<u32>(bvhptr - m_bvh);
				for (u32 b = 0; b < bvh_nodenum; b++) // debug
				{
					if (m_bvh[b].m_leaf_kids & BVH::LEAF_NODE)
						printf("ID=%6d: Tri(%d,%d,%d,%d)\n",m_bvh[b].debug_id, m_bvh[b].m_idxid[0],m_bvh[b].m_idxid[1],m_bvh[b].m_idxid[2],m_bvh[b].m_idxid[3]);
				}
*/
				
				f64 et_makebvh = VX::GetTimeCount();
				
				VXLogD("sorttime = %6.4lf[s] buildbvh = %6.4lf\n", et_sort - st_sort, et_makebvh - et_sort);
				VXLogD("bvh node num = %d\n", bvh_nodenum);
				
				vxdeleteArray(tri);
			
				// debug
#if 0				
				test_bvh_color(m_bvh, bvh_nodenum);
				
				Geometry::VertexFormat* lv = GetVertex();
				
				for (u32 t = 0; t < trinum; t++)
				{
					//u32 tcol = 0xFFFFFF * ( t / static_cast<double>(trinum));
					VX::Math::vec3 col = HSVtoRGB((t/static_cast<double>(trinum)*360.0), 1, 1);
					u32 tcol = static_cast<u32>(col.x * 255) | (static_cast<u32>(col.y * 255)<<8) | (static_cast<u32>(col.z * 255)<<16);
					tcol |= 0xFF000000;
					lv[index[3*tri[t].tri_id  ]].col = tcol;
					lv[index[3*tri[t].tri_id+1]].col = tcol;
					lv[index[3*tri[t].tri_id+2]].col = tcol;
				}
				
				for (u32 bi = 0; bi < bvhindex; bi++)
				{
					BVH* bvh = bvhleaf;
					//printf("bvh:%d,%d,%d,%d\n", bvh[bi].m_triid[0], bvh[bi].m_triid[1], bvh[bi].m_triid[2], bvh[bi].m_triid[3]);
					VX::Math::vec3 col = HSVtoRGB((bi/static_cast<double>(bvhindex)*360.0), 1, 1);
					u32 tcol = static_cast<u32>(col.x * 255) | (static_cast<u32>(col.y * 255)<<8) | (static_cast<u32>(col.z * 255)<<16);
					tcol |= 0xFF000000;
					for (u32 k = 0; k < bvhtrinum; k++)
					{
						const u32 triid = 3 * bvh[bi].m_triid[k];
						lv[index[triid  ]].col = tcol;
						lv[index[triid+1]].col = tcol;
						lv[index[triid+2]].col = tcol;
					}
				}
				
#endif
			}
		};
		
	} // namespace SG
} // namespace VX

#endif // INCLUDE_VXSCENEGRAPH_GEOMETRY_BVH_H
