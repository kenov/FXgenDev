/*
 * VX/SG/CellIDVoxelCartesian.h
 *
 * SceneGraphLibrary
 *
 */

#ifndef INCLUDE_VXSCENEGRAPH_VoxelCartesianL_Scan_H
#define INCLUDE_VXSCENEGRAPH_VoxelCartesianL_Scan_H

#include "../Type.h"
#include <assert.h>


#define MED_BC_MAX_SIZE (32)

namespace VX
{
	namespace SG
	{

		class VoxelCartesianL_Scan {

		public:
			VoxelCartesianL_Scan();
			virtual ~VoxelCartesianL_Scan();

			//copy constructor
			VoxelCartesianL_Scan(const VoxelCartesianL_Scan& obj);
			VoxelCartesianL_Scan& operator=(const VoxelCartesianL_Scan& obj);
			int GetSize()const;
			void SetEnable(const int& index);
			bool IsEnable(const int& index)const;
			void Clear();
		private:

			void assignCopy(const VoxelCartesianL_Scan& obj);	
			
			//0～31までの値が定義される
			bool vals[MED_BC_MAX_SIZE] ;




		};

	} // namespace SG

} // namespace VX

#endif // INCLUDE_VXSCENEGRAPH_VoxelCartesianL_H

