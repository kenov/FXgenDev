/*
 * VX/SG/Geometry.h
 *
 * SceneGraph Library
 *
 */

#ifndef INCLUDE_VXSCENEGRAPH_GEOMETRY_H
#define INCLUDE_VXSCENEGRAPH_GEOMETRY_H

#include "../Type.h"
#include "../Math.h"

#include "Node.h"
#include "Texture.h"
#include "ColorMap.h"
#include <string>
#include <float.h>

namespace VX
{
	namespace SG
	{
		typedef u32 Index;
		
		class Geometry : public Node
		{
		public:
			Geometry(NODETYPE ntype = NODETYPE_GEOMETRY) : Node(ntype){
				m_vertexBuffer = 0;
				m_indexBuffer  = 0;
				m_vertexCount  = 0;
				m_indexCount   = 0;
				m_maxIndexCount = 0;
				m_idbuffer	   = 0;

				m_alpha        = 0.25f;

				m_line_normal = VX::Math::vec4( 155.0f/255.0f,  38.0f/255.0f,   4.0f/255.0f, 1.0f);
				m_line_active = VX::Math::vec4( 155.0f/255.0f,  38.0f/255.0f,   4.0f/255.0f, 1.0f);
				m_poly_normal = VX::Math::vec4( 120.0f/255.0f, 120.0f/255.0f, 120.0f/255.0f, 1.0f);
				
				//transparent
				m_poly_active = VX::Math::vec4( 255.0f/255.0f,  85.0f/255.0f,  63.0f/255.0f, 1.0f);

				// �I�����A�������F�̒�`
				m_poly_active_diapha = VX::Math::vec4( 155.0f/255.0f,  38.0f/255.0f,   4.0f/255.0f, 1.0f * m_alpha);
				
				m_bbox[0] = m_bbox[1] = m_center = VX::Math::vec3(0,0,0);
				m_radius = 0;
				m_drawmode = MODE_POLYGON;
				
				m_useVertexLineColor = false;
				
				m_needUpdate = false;
				m_selecting = false;
				m_selectable = true;
				m_diaphaDraw = false;

				m_colormap = NULL;
			}
			~Geometry()
			{
				VXLogI("Geometry.~Geometry(vertexCount=%d,indexCount=%d)\n", m_vertexCount,m_indexCount);
				vxdeleteArray(m_vertexBuffer);
				vxdeleteArray(m_indexBuffer);
				vxdeleteArray(m_idbuffer);
			}
			
			class VertexFormat
			{
			public:
				Math::vec3 pos;
				Math::vec3 normal;
				u32        col;
				u32        pad;

				f32* u(){ return reinterpret_cast<f32*>(&col); }
				f32* v(){ return reinterpret_cast<f32*>(&pad); }
			};
			
			enum ALLOCOPTION {
				ALLOC_WITHOUT_IDBUFFER = 0,
				ALLOC_WITH_IDBUFFER = 1,
			};
			void Alloc(u32 vertexCount, Index indexCount, u32 option = ALLOC_WITH_IDBUFFER){

				VXLogI("Geometry.Alloc(vertexCount=%d,indexCount=%d,option=%d)\n", vertexCount,indexCount,option);

				if (m_vertexCount != vertexCount) {
					vxdeleteArray(m_vertexBuffer);
					m_vertexBuffer = vxnew VertexFormat[vertexCount];
					m_vertexCount  = vertexCount;
				}
				if (m_indexCount != indexCount) {
					vxdeleteArray(m_indexBuffer);
					m_indexBuffer  = vxnew Index[indexCount];
					m_indexCount   = indexCount;
					m_maxIndexCount = m_indexCount;
					vxdeleteArray(m_idbuffer);
					if (option == ALLOC_WITH_IDBUFFER)
					{
						const u32 idnum = indexCount/2+1;
						m_idbuffer     = vxnew u32[idnum];
						memset(m_idbuffer, 0, sizeof(u32) * idnum);
					}
				}
			//	EnableNeedUpdate();
			}
			void MinimizeIndex(u32 new_indexCount)
			{
				if (m_indexCount <= new_indexCount)
					return;
				
				m_indexCount = new_indexCount;
			}

			void SetIndexCount(u32 new_indexCount)
			{
				if (m_maxIndexCount < new_indexCount)
				{
					VXLogE("Max Over [%s]\n", GetName().c_str() );
					return;
				}
				
				m_indexCount = new_indexCount;
			}
			
			Index GetVertexCount() const {
				return m_vertexCount;
			}
			
			Index GetIndexCount() const {
				return m_indexCount;
			}
			
			const VertexFormat* GetVertex() const {
				return m_vertexBuffer;
			}
		
			VertexFormat* GetVertex() {
				return m_vertexBuffer;
			}
			
			Index* GetIndex(){
				return m_indexBuffer;
			}
			
			const Index* GetIndex() const {
				return m_indexBuffer;
			}

			u32* GetIDBuffer(){
				if(m_texture) 
					return 0;
				return m_idbuffer;
			}
			
			const u32* GetIDBuffer() const{
				if(m_texture) 
					return 0;
				return m_idbuffer;
			}

			void CalcBounds() {
				using namespace VX::Math;
				m_bbox[0] = vec3(-FLT_MAX,-FLT_MAX,-FLT_MAX);
				m_bbox[1] = vec3( FLT_MAX, FLT_MAX, FLT_MAX);
				for (u32 i = 0; i < m_vertexCount; i++) {
					for (u32 j = 0; j < 3; j++) {
						m_bbox[0][j] = (std::max)(m_bbox[0][j], m_vertexBuffer[i].pos[j]);
						m_bbox[1][j] = (std::min)(m_bbox[1][j], m_vertexBuffer[i].pos[j]);
					}
				}
				m_center = (m_bbox[0] + m_bbox[1]) * 0.5f;
				m_radius = length(m_bbox[0] - m_bbox[1]) * 0.5f;
			}
			
			const VX::Math::vec3& GetMax() const
			{
				return m_bbox[0];
			}
			
			const VX::Math::vec3& GetMin() const
			{
				return m_bbox[1];
			}
			
			f32 GetRadius() const
			{
				return m_radius;
			}
			
			const VX::Math::vec3& GetCenter() const
			{
				return m_center;
			}
			
			b8 GetNeedUpdate() const
			{
				return m_needUpdate;
			}
			
			void DisableNeedUpdate()
			{
				m_needUpdate = false;
			}
			void EnableNeedUpdate()
			{
				m_needUpdate = true;
			}
			
			const f32 GetAlpha() const { return m_alpha; }
			void SetAlpha(f32 alpha) { m_alpha = alpha; }

			const VX::Math::vec4& GetLineNormalColor() const { return m_line_normal; }
			const VX::Math::vec4& GetLineActiveColor() const { return m_line_active; }
			const VX::Math::vec4& GetPolyNormalColor() const { return m_poly_normal; }
			const VX::Math::vec4& GetPolyActiveColor() const { return m_poly_active; }
			const VX::Math::vec4& GetPolyActiveDiaphaColor() const { return m_poly_active_diapha; }

			void SetLineNormalColor(const VX::Math::vec4& col) { m_line_normal = col; }
			void SetLineActiveColor(const VX::Math::vec4& col) { m_line_active = col; }
			void SetPolyNormalColor(const VX::Math::vec4& col) { m_poly_normal = col; }
			void SetPolyActiveColor(const VX::Math::vec4& col) { m_poly_active = col; }
			void SetPolyActiveDiaphaColor(const VX::Math::vec4& col) { m_poly_active_diapha = col; }

			b8 GetUseVertexLineColor() const   { return m_useVertexLineColor; }
			void SetUseVertexLineColor(b8 use) { m_useVertexLineColor = use; }
			
			enum DRAWMODE {
				MODE_POINT           = 1,
				MODE_LINE            = 2,
				MODE_POLYGON         = 4,
			};
			void SetDrawMode(DRAWMODE mode){ m_drawmode = mode; }
			u32 GetDrawMode() const        { return m_drawmode; }

			void SetSelectable(b8 selectable) { m_selectable = selectable; }
			b8 GetSelectable() const { return m_selectable; }	
			
			b8 GetSelection() const { return m_selecting; }
			void SetSelection(b8 select, b8 flagonly = false) {
				if( !m_selectable ){
					m_selecting = false;
					return;
				}

				m_selecting = select;
				if (flagonly)
					return;

				const Index n = this->GetIndexCount()/3;
				if (select) {
					for (Index i = 0; i < n; ++i)
						m_idbuffer[i] |= 0x000000FF;
				} else {
					for (Index i = 0; i < n; ++i)
						m_idbuffer[i] &= 0xFFFFFF00;
				}
				EnableNeedUpdate();
			}
			void CheckSelection()
			{
				const Index n = this->GetIndexCount()/3;
				u32 flag = 0;
				for (Index i = 0; i < n; ++i)
					flag |= (m_idbuffer[i] & 0x000000FF);
				m_selecting = flag > 0;
			}

			const Texture* GetTexture() const
			{
				return m_texture;
			}

			Texture* GetTexture()
			{
				return m_texture;
			}

			void SetTexture(Texture* texture)
			{
				m_texture = texture;
			}

			const ColorMap* GetColorMap() const
			{
				return m_colormap;
			}

			ColorMap* GetColorMap()
			{
				return m_colormap;
			}

			void SetColorMap(ColorMap* colormap)
			{
				m_colormap = colormap;
			}

			void SetDiaphaDraw(b8 diaphaDraw)
			{
				m_diaphaDraw = diaphaDraw;
			}

			b8 GetDiaphaDraw() const
			{
				return m_diaphaDraw;
			}

		protected:
			b8 m_needUpdate;
			
		private:
			Index m_vertexCount;
			Index m_indexCount;
			Index m_maxIndexCount;
			
			VertexFormat* m_vertexBuffer;
			Index* m_indexBuffer;
			u32* m_idbuffer;// ID,SelectionFlag
			
			// Colors
			VX::Math::vec4 m_line_normal;
			VX::Math::vec4 m_line_active;
			VX::Math::vec4 m_poly_normal;
			VX::Math::vec4 m_poly_active;
			VX::Math::vec4 m_poly_active_diapha;

			f32 m_alpha;
			
			// BBox and BSphere
			VX::Math::vec3 m_bbox[2];// 0 - max, 1 - min
			VX::Math::vec3 m_center;
			f32 m_radius;
		
			b8 m_useVertexLineColor;
			u32 m_drawmode;
			b8 m_selecting;
			b8 m_selectable;
			b8 m_diaphaDraw;
			
			NodeRefPtr<Texture> m_texture;
			NodeRefPtr<ColorMap> m_colormap;
		};
		
	} // namespace SG
} // namespace VX

#endif // INCLUDE_VXSCENEGRAPH_GEOMETRY_H
