#include "VoxelCartesianL_Scan.h"


namespace VX {
namespace SG {



VoxelCartesianL_Scan::VoxelCartesianL_Scan()
{
	Clear();
}

VoxelCartesianL_Scan::~VoxelCartesianL_Scan()
{

}

//	コピーコンストラクタ
VoxelCartesianL_Scan::VoxelCartesianL_Scan( const VoxelCartesianL_Scan& obj )
{
	assignCopy(obj);
}

VoxelCartesianL_Scan& VoxelCartesianL_Scan::operator=(const VoxelCartesianL_Scan& obj)
{
	if(this == &obj){
		return *this;   //自己代入
	}
	assignCopy(obj);
	return(*this);
}

void VoxelCartesianL_Scan::Clear()
{
	for(int i=0;i<MED_BC_MAX_SIZE;i++){
		vals[i] = false;
	}
}

void VoxelCartesianL_Scan::SetEnable(const int& index)
{

	if(index<MED_BC_MAX_SIZE){
		vals[index] = true;
	}else{

		#ifdef _DEBUG
		assert(false);
		#endif

	}
}

bool VoxelCartesianL_Scan::IsEnable(const int& index)const
{
	#ifdef _DEBUG
	assert(index<MED_BC_MAX_SIZE);
	#endif
	return vals[index];
}


int VoxelCartesianL_Scan::GetSize()const
{
	return MED_BC_MAX_SIZE;
}

void VoxelCartesianL_Scan::assignCopy(const VoxelCartesianL_Scan& obj)
{

	for(int i=0;i<MED_BC_MAX_SIZE;i++){
		vals[i] = obj.vals[i];
	}

}


}} // end of namespace SG::VX



