/*
 * 
 * SceneGraphLibrary
 *
 */

#ifndef INCLUDE_VXSCENEGRAPH_VOX_GEOMETRY_UTIL_H
#define INCLUDE_VXSCENEGRAPH_VOX_GEOMETRY_UTIL_H

#include "../Log.h"
#include "../Type.h"

#include "../../VOX/Pedigree.h"
#include "../../VOX/Node.h"
#include "../../VOX/BCMOctree.h"
#include "../../VOX/RootGrid.h"

#include "Geometry.h"

#include <vector>

inline
void FaceTexSetter(VX::SG::Geometry::VertexFormat* vtx, VX::SG::Index* idx, 
                   const u32 axis, const VX::Math::vec3& org, const VX::Math::vec3& rgn, 
				   const VX::Math::vec2& texCoordLB, const VX::Math::vec2& texCoordRT, const u32 idxOffset = 0)
{
	using namespace VX::Math;
	using namespace VX::SG;
	if(axis == 0){
		vtx[0].pos    = vec3(org.x,        org.y,       org.z      );
		vtx[1].pos    = vec3(org.x,        org.y+rgn.y, org.z      );
		vtx[2].pos    = vec3(org.x,        org.y+rgn.y, org.z+rgn.z);
		vtx[3].pos    = vec3(org.x,        org.y,       org.z+rgn.z);
		for(int i = 0; i < 4; i++){ vtx[i].normal = vec3(1.0, 0.0, 0.0); }
	}else if(axis == 1){
		vtx[0].pos    = vec3(org.x,       org.y,        org.z      );
		vtx[1].pos    = vec3(org.x+rgn.x, org.y,        org.z      );
		vtx[2].pos    = vec3(org.x+rgn.x, org.y,        org.z+rgn.z);
		vtx[3].pos    = vec3(org.x,       org.y,        org.z+rgn.z);
		for(int i = 0; i < 4; i++){ vtx[i].normal = vec3(0.0, 1.0, 0.0); }
	}else{
		vtx[0].pos    = vec3(org.x,       org.y,       org.z       );
		vtx[1].pos    = vec3(org.x+rgn.x, org.y,       org.z       );
		vtx[2].pos    = vec3(org.x+rgn.x, org.y+rgn.y, org.z       );
		vtx[3].pos    = vec3(org.x,       org.y+rgn.y, org.z       );
		for(int i = 0; i < 4; i++){ vtx[i].normal = vec3(0.0, 0.0, 1.0); }
	}
	
	*vtx[0].u() = texCoordLB.x;  *vtx[0].v() = texCoordLB.y;
	*vtx[1].u() = texCoordRT.x;  *vtx[1].v() = texCoordLB.y;
	*vtx[2].u() = texCoordRT.x;  *vtx[2].v() = texCoordRT.y;
	*vtx[3].u() = texCoordLB.x;  *vtx[3].v() = texCoordRT.y;

	idx[0] = 0 + idxOffset;
	idx[1] = 1 + idxOffset;
	idx[2] = 2 + idxOffset;
	idx[3] = 2 + idxOffset;
	idx[4] = 3 + idxOffset;
	idx[5] = 0 + idxOffset;
}

inline
void FaceSetter(VX::SG::Geometry::VertexFormat* vtx, VX::SG::Index* idx, 
                const u32 axis, const VX::Math::vec3& org, const VX::Math::vec3& rgn, const u32 color, const u32 idxOffset = 0)
{
	using namespace VX::Math;
	using namespace VX::SG;
	if(axis == 0){
		vtx[0].pos    = vec3(org.x,        org.y,       org.z      );
		vtx[1].pos    = vec3(org.x,        org.y+rgn.y, org.z      );
		vtx[2].pos    = vec3(org.x,        org.y+rgn.y, org.z+rgn.z);
		vtx[3].pos    = vec3(org.x,        org.y,       org.z+rgn.z);
		for(int i = 0; i < 4; i++){ vtx[i].normal = vec3(1.0, 0.0, 0.0); vtx[i].col = color; }
	}else if(axis == 1){
		vtx[0].pos    = vec3(org.x,       org.y,        org.z      );
		vtx[1].pos    = vec3(org.x+rgn.x, org.y,        org.z      );
		vtx[2].pos    = vec3(org.x+rgn.x, org.y,        org.z+rgn.z);
		vtx[3].pos    = vec3(org.x,       org.y,        org.z+rgn.z);
		for(int i = 0; i < 4; i++){ vtx[i].normal = vec3(0.0, 1.0, 0.0); vtx[i].col = color; }
	}else{
		vtx[0].pos    = vec3(org.x,       org.y,       org.z       );
		vtx[1].pos    = vec3(org.x+rgn.x, org.y,       org.z       );
		vtx[2].pos    = vec3(org.x+rgn.x, org.y+rgn.y, org.z       );
		vtx[3].pos    = vec3(org.x,       org.y+rgn.y, org.z       );
		for(int i = 0; i < 4; i++){ vtx[i].normal = vec3(0.0, 0.0, 1.0); vtx[i].col = color; }
	}

	idx[0] = 0 + idxOffset;
	idx[1] = 1 + idxOffset;
	idx[2] = 2 + idxOffset;
	idx[3] = 2 + idxOffset;
	idx[4] = 3 + idxOffset;
	idx[5] = 0 + idxOffset;
}

inline
void BoundarySetter(VX::SG::Geometry::VertexFormat* vtx, VX::SG::Index* idx,
                    const u32 axis, const VX::Math::vec3& org, const VX::Math::vec3& rgn, 
					const u32 color, const u32 idxOffset = 0 )
{
	using namespace VX::Math;
	using namespace VX::SG;
	if(axis == 0){
		vtx[0].pos    = vec3(org.x,        org.y,       org.z      );
		vtx[1].pos    = vec3(org.x,        org.y+rgn.y, org.z      );
		vtx[2].pos    = vec3(org.x,        org.y+rgn.y, org.z+rgn.z);
		vtx[3].pos    = vec3(org.x,        org.y,       org.z+rgn.z);
		for(int i = 0; i < 4; i++){ vtx[i].normal = vec3(1.0, 0.0, 0.0); }
	}else if(axis == 1){
		vtx[0].pos    = vec3(org.x,       org.y,        org.z      );
		vtx[1].pos    = vec3(org.x+rgn.x, org.y,        org.z      );
		vtx[2].pos    = vec3(org.x+rgn.x, org.y,        org.z+rgn.z);
		vtx[3].pos    = vec3(org.x,       org.y,        org.z+rgn.z);
		for(int i = 0; i < 4; i++){ vtx[i].normal = vec3(0.0, 1.0, 0.0); }
	}else{
		vtx[0].pos    = vec3(org.x,       org.y,       org.z       );
		vtx[1].pos    = vec3(org.x+rgn.x, org.y,       org.z       );
		vtx[2].pos    = vec3(org.x+rgn.x, org.y+rgn.y, org.z       );
		vtx[3].pos    = vec3(org.x,       org.y+rgn.y, org.z       );
		for(int i = 0; i < 4; i++){ vtx[i].normal = vec3(0.0, 0.0, 1.0); }
	}

	for(int i = 0; i < 4; i++){ vtx[i].col = color; }

	idx[0] = 0 + idxOffset;
	idx[1] = 1 + idxOffset;
	idx[2] = 1 + idxOffset;
	idx[3] = 2 + idxOffset;
	idx[4] = 2 + idxOffset;
	idx[5] = 3 + idxOffset;
	idx[6] = 3 + idxOffset;
	idx[7] = 0 + idxOffset;
}

inline
void GridSetter(VX::SG::Geometry::VertexFormat* vtx, VX::SG::Index* idx, const u32 axis,
                const VX::Math::vec3& org, const VX::Math::vec3& rgn, const VX::Math::idx2 div, 
				const u32 color, const u32 idxOffset = 0 )
{
	using namespace VX::Math;
	using namespace VX::SG;

	if( axis == 0 ){
		u32 cnt = 0;
		for(int u = 0; u < div.x-1; u++){
			float pitch = rgn.y / (float)div.x;
			vtx[cnt*2 + 0].pos = vec3(org.x, org.y + pitch * (u+1), org.z        );
			vtx[cnt*2 + 1].pos = vec3(org.x, org.y + pitch * (u+1), org.z + rgn.z);
			vtx[cnt*2 + 0].col = color;
			vtx[cnt*2 + 1].col = color;
			idx[cnt*2 + 0] = cnt*2 + 0 + idxOffset;
			idx[cnt*2 + 1] = cnt*2 + 1 + idxOffset;
			cnt++;
		}
		for(int v = 0; v < div.y-1; v++){
			float pitch = rgn.z / (float)div.y;
			vtx[cnt*2 + 0].pos = vec3(org.x, org.y,         org.z + pitch * (v+1));
			vtx[cnt*2 + 1].pos = vec3(org.x, org.y + rgn.y, org.z + pitch * (v+1));
			vtx[cnt*2 + 0].col = color;
			vtx[cnt*2 + 1].col = color;
			idx[cnt*2 + 0] = cnt*2 + 0 + idxOffset;
			idx[cnt*2 + 1] = cnt*2 + 1 + idxOffset;
			cnt++;
		}
		return;
	}
	else if( axis == 1 ){
		u32 cnt = 0;
		for(int u = 0; u < div.x-1; u++){
			float pitch = rgn.x / (float)div.x;
			vtx[cnt*2 + 0].pos = vec3(org.x + pitch * (u+1), org.y, org.z        );
			vtx[cnt*2 + 1].pos = vec3(org.x + pitch * (u+1), org.y, org.z + rgn.z);
			vtx[cnt*2 + 0].col = color;
			vtx[cnt*2 + 1].col = color;
			idx[cnt*2 + 0] = cnt*2 + 0 + idxOffset;
			idx[cnt*2 + 1] = cnt*2 + 1 + idxOffset;
			cnt++;
		}
		for(int v = 0; v < div.y-1; v++){
			float pitch = rgn.z / (float)div.y;
			vtx[cnt*2 + 0].pos = vec3(org.x,         org.y, org.z + pitch * (v+1));
			vtx[cnt*2 + 1].pos = vec3(org.x + rgn.x, org.y, org.z + pitch * (v+1));
			vtx[cnt*2 + 0].col = color;
			vtx[cnt*2 + 1].col = color;
			idx[cnt*2 + 0] = cnt*2 + 0 + idxOffset;
			idx[cnt*2 + 1] = cnt*2 + 1 + idxOffset;
			cnt++;
		}
		return;
	}
	else if( axis == 2 ){
		u32 cnt = 0;
		for(int u = 0; u < div.x-1; u++){
			float pitch = rgn.x / (float)div.x;
			vtx[cnt*2 + 0].pos = vec3(org.x + pitch * (u+1), org.y,         org.z);
			vtx[cnt*2 + 1].pos = vec3(org.x + pitch * (u+1), org.y + rgn.y, org.z);
			vtx[cnt*2 + 0].col = color;
			vtx[cnt*2 + 1].col = color;
			idx[cnt*2 + 0] = cnt*2 + 0 + idxOffset;
			idx[cnt*2 + 1] = cnt*2 + 1 + idxOffset;
			cnt++;
		}
		for(int v = 0; v < div.y-1; v++){
			float pitch = rgn.y / (float)div.y;
			vtx[cnt*2 + 0].pos = vec3(org.x,         org.y + pitch * (v+1), org.z);
			vtx[cnt*2 + 1].pos = vec3(org.x + rgn.x, org.y + pitch * (v+1), org.z);
			vtx[cnt*2 + 0].col = color;
			vtx[cnt*2 + 1].col = color;
			idx[cnt*2 + 0] = cnt*2 + 0 + idxOffset;
			idx[cnt*2 + 1] = cnt*2 + 1 + idxOffset;
			cnt++;
		}
		return;
	}
}

inline
void BoxSetter(VX::SG::Geometry::VertexFormat* vtx, VX::SG::Index* idx,
               const VX::Math::vec3& org, const VX::Math::vec3& rgn, const u32 color, const u32 idxOffset = 0 )
{
	using namespace VX::Math;
	using namespace VX::SG;
	
	vtx[0].pos.x = org.x;         vtx[0].pos.y = org.y;         vtx[0].pos.z = org.z;         vtx[0].col = color;
	vtx[1].pos.x = org.x + rgn.x; vtx[1].pos.y = org.y;         vtx[1].pos.z = org.z;         vtx[1].col = color;
	vtx[2].pos.x = org.x + rgn.x; vtx[2].pos.y = org.y + rgn.y; vtx[2].pos.z = org.z;         vtx[2].col = color;
	vtx[3].pos.x = org.x;         vtx[3].pos.y = org.y + rgn.y; vtx[3].pos.z = org.z;         vtx[3].col = color;
	vtx[4].pos.x = org.x;         vtx[4].pos.y = org.y;         vtx[4].pos.z = org.z + rgn.z; vtx[4].col = color;
	vtx[5].pos.x = org.x + rgn.x; vtx[5].pos.y = org.y;         vtx[5].pos.z = org.z + rgn.z; vtx[5].col = color;
	vtx[6].pos.x = org.x + rgn.x; vtx[6].pos.y = org.y + rgn.y; vtx[6].pos.z = org.z + rgn.z; vtx[6].col = color;
	vtx[7].pos.x = org.x;         vtx[7].pos.y = org.y + rgn.y; vtx[7].pos.z = org.z + rgn.z; vtx[7].col = color;

	int idxTbl[24] = { 0, 1, 1, 2, 2, 3, 3, 0, 4, 5, 5, 6, 6, 7, 7, 4, 0, 4, 3, 7, 1, 5, 2, 6 };
	for(int i = 0; i < 24; i++){
		idx[i] = idxTbl[i] + idxOffset;
	}
}

inline
void GetBCMNodesOnPlane_rec(const u32 axis, const VOX::Node* node, const u64 planePosition, const u32 maxLevel, 
                            std::vector<const VOX::Node*>& leafNodes)
{
	if(node->isLeafNode() )
	{
		if( !node->isActive() )
		{
			return;
		}

		leafNodes.push_back(node);
		return;
	}

	u32 lv = node->getLevel();

	int cidTbl[2][4] = {0};
	if( axis == 0 )
	{
		cidTbl[0][0] = 0; cidTbl[0][1] = 2; cidTbl[0][2] = 4; cidTbl[0][3] = 6;
		cidTbl[1][0] = 1; cidTbl[1][1] = 3; cidTbl[1][2] = 5; cidTbl[1][3] = 7;
	}
	if( axis == 1 )
	{
		cidTbl[0][0] = 0; cidTbl[0][1] = 1; cidTbl[0][2] = 4; cidTbl[0][3] = 5;
		cidTbl[1][0] = 2; cidTbl[1][1] = 3; cidTbl[1][2] = 6; cidTbl[1][3] = 7;
	}
	if( axis == 2 )
	{
		cidTbl[0][0] = 0; cidTbl[0][1] = 1; cidTbl[0][2] = 2; cidTbl[0][3] = 3;
		cidTbl[1][0] = 4; cidTbl[1][1] = 5; cidTbl[1][2] = 6; cidTbl[1][3] = 7;
	}
	int high_low = (( planePosition >> (maxLevel - (lv+1)) ) & 1) == 1 ? 1 : 0;
	for(int i = 0; i < 4; i++){
		const VOX::Node* n = node->getChild(cidTbl[high_low][i]);
		GetBCMNodesOnPlane_rec(axis, n, planePosition, maxLevel, leafNodes);
	}
}

inline
void GetBCMNodesOnPlane(const u32 axis, const VOX::Node* root, const u64 planePosition, const u32 maxLevel, 
                        std::vector<const VOX::Node*>& leafNodes)
{
	GetBCMNodesOnPlane_rec(axis, root, planePosition, maxLevel, leafNodes);
}

inline
u64 SearchBCMMaxBlockCount(const u32 axis, const VOX::BCMOctree* octree, const u32 maxLevel)
{
	using namespace VX::Math;

	const VOX::RootGrid* rootGrid = octree->getRootGrid();
	idx3 rootDims( rootGrid->getSizeX(), rootGrid->getSizeY(), rootGrid->getSizeZ() );


	// Max Number of Blocks within RootGrid
	const u32 numBlocksWR = 1 << maxLevel;
	const u32 numSlice = rootDims[axis] * numBlocksWR;

	u64 maxBlocks = 0;

	for(u32 s = 0; s < numSlice; s++)
	{
		const u32 rPos = s / numBlocksWR;
		const u32 nPos = s % numBlocksWR;

		std::vector<const VOX::Node*> leafNodes;
		u32 nru = axis == 0 ? u64(rootDims[1]) : u64(rootDims[0]);
		u32 nrv = axis == 2 ? u64(rootDims[1]) : u64(rootDims[2]);
		for(u32 v = 0; v < nrv; v++){
			for(u32 u = 0; u < nru; u++){
				u32 x = axis == 0 ? rPos : u;
				u32 y = axis == 1 ? rPos : axis == 0 ? u : v;
				u32 z = axis == 2 ? rPos : v;
				const VOX::Node* root = octree->getRootNode(rootGrid->index2rootID(x, y, z));
				GetBCMNodesOnPlane(axis, root, nPos, maxLevel, leafNodes);
			}
		}
		maxBlocks = maxBlocks > leafNodes.size() ? maxBlocks : leafNodes.size();
	}
	return maxBlocks;
}

#endif // INCLUDE_VXSCENEGRAPH_VOX_GEOMETRY_UTIL_H
