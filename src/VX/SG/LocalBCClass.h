/*
 * VX/SG/LocalBCClass.h
 *
 * SceneGraph Library
 *
 */

#ifndef INCLUDE_VXSCENEGRAPH_LOCALBCCLASS_H
#define INCLUDE_VXSCENEGRAPH_LOCALBCCLASS_H

#include "../Type.h"
#include "Node.h"

#include <string>

namespace VX
{
	namespace SG
	{
		class LocalBCClass : public Node
		{
		public:
			LocalBCClass(const std::string& name)
			: Node(NODETYPE_LOCALBCCLASS)
			{
				m_label = name;
			}
			~LocalBCClass(){}
			/*
			void SetName(const std::string& name)
			{
				m_label = name;
			}
			*/
			
			const std::string GetName() const
			{
				return m_label;
			}

			const std::string& GetLabel() const
			{
				return m_label;
			}
			
			b8 GetSelection() const { return false; };
			void SetSelection(b8 select, b8 flagonly = false){};
			
		private:
			std::string    m_label;
		};
		
	} // namespace SG
		
} // namespace VX

#endif // INCLUDE_VXSCENEGRAPH_LOCALBCCLASS_H
