#include "VoxelCartesianSliceC.h"

#include "VoxelCartesianC.h"

#include "../Type.h"
#include "../Math.h"

#include "Node.h"
#include "Group.h"
#include "Geometry.h"
#include "Texture.h"
#include "VoxGeometryUtil.h"

#include "VoxelCartesianUtil.h"
#include "VoxelCartesianBlock.h"
#include "VoxelCartesianL.h"

#include "MeshUtil.h"

#include <string>
#include <vector>

#include <limits.h>

namespace {
	const static u32 boundaryColor = 0xFF00FF00;//0xFF3F55FF;
	const static u32 lineColor     = 0xFFFF0000;//0xFF050505;

} // namespace 

namespace VX {
namespace SG {

VoxelCartesianSliceC::VoxelCartesianSliceC(const u32 axis, const VoxelCartesianC* mesh )
  : Node(NODETYPE_VOXEL_CARTESIAN_SLICE_C), m_mesh(mesh), m_axis(axis)
{	
	const char* sliceName[3] = {"Slice X", "Slice Y", "Slice Z"};
	SetName(sliceName[m_axis]);
	
	const VX::Math::idx3 voxsize = mesh->GetNumVoxels();
	m_uvw[0] = axis == 0 ? voxsize[1] : voxsize[0];
	m_uvw[1] = axis == 2 ? voxsize[1] : voxsize[2];
	m_uvw[2] = axis == 0 ? voxsize[0] : axis == 1 ? voxsize[1] : voxsize[2];
	
	using namespace VX::SG;
	using namespace VX::Math;

	// Boundary
	m_boundary = vxnew Geometry();
	m_boundary->SetUseVertexLineColor(true);
	m_boundary->SetDrawMode(Geometry::MODE_LINE);
	m_boundary->Alloc(4, 8);

	// Grid
	m_grid = vxnew Geometry();
	m_grid->SetUseVertexLineColor(true);
	m_grid->SetDrawMode(Geometry::MODE_LINE);
	u32 gridStride = (m_uvw[0]-1) * 2 + (m_uvw[1]-1) * 2;
	m_grid->Alloc(gridStride, gridStride);

	// BlockBoundary
	const VX::Math::idx3 numDiv3 = mesh->GetNumDivs();
	m_div = VX::Math::idx2( axis == 0 ? numDiv3[1] : numDiv3[0], axis == 2 ? numDiv3[1] : numDiv3[2] );
	m_block = vxnew Geometry();
	m_block->SetUseVertexLineColor(true);
	m_block->SetDrawMode(Geometry::MODE_LINE);
	m_block->Alloc( 4 * (m_div.x * m_div.y), 8 * (m_div.x * m_div.y));

	std::vector< int > uSizeList;
	std::vector< int > vSizeList;

	//const int textureLimit = 32; // TODO Texture::MAX_SIZE
	//const int textureLimit = 512; // TODO Texture::MAX_SIZE
	const int textureLimit = Texture::MAX_SIZE;
	CalcDivision(m_uvw.x, textureLimit, uSizeList);
	CalcDivision(m_uvw.y, textureLimit, vSizeList);

	m_subFaces.resize(uSizeList.size() * vSizeList.size());
	idx2 hIdxCounter(0, 0);
	int cnt = 0;

	for(std::vector<int>::iterator vit = vSizeList.begin(); vit != vSizeList.end(); vit++){
		for(std::vector<int>::iterator uit = uSizeList.begin(); uit != uSizeList.end(); uit++){
			Texture* tex = vxnew Texture(idx2(*uit, *vit));
			m_subFaces[cnt].face = vxnew Geometry();
			m_subFaces[cnt].face->SetDrawMode(Geometry::MODE_POLYGON);
			m_subFaces[cnt].face->SetTexture(tex);
			m_subFaces[cnt].face->Alloc(4, 6);
			
			m_subFaces[cnt].headIndex = hIdxCounter;
			m_subFaces[cnt].size = idx2(*uit, *vit);
			hIdxCounter.x += *uit;
			cnt++;

		}
		hIdxCounter.y += *vit;
		hIdxCounter.x = 0;
	}
	/*
	VXLogD("Create subface[%d]\n", m_axis);
	for(std::vector<SubFace>::iterator it = m_subFaces.begin(); it != m_subFaces.end(); ++it){
		VXLogD("hIdx(%3d %3d) size(%3d %3d)\n", it->headIndex.x, it->headIndex.y, it->size.x, it->size.y);
	}
	*/

	vec3 org = mesh->GetOrigin();
	vec3 rgn = mesh->GetRegion();
	
	//クリップの初期値は中心位置ではなく
	//posが０の場所
	//org[m_axis] += rgn[m_axis] * 0.5f;

	SetPosition(org[m_axis], 0);
}

VoxelCartesianSliceC::~VoxelCartesianSliceC()
{

}

f32 VoxelCartesianSliceC::GetPosition() const
{
	return m_boundary->GetVertex()[0].pos[m_axis];
}
void VoxelCartesianSliceC::SetPosition( const f32 pos, const u32 index_coord, const b8 boundaryOnly )
{
	using namespace VX::Math;
	
	const vec3 gorg = m_mesh->GetOrigin();
	const vec3 grgn = m_mesh->GetRegion();
	const vec3 pitch = m_mesh->GetPitch();
	const idx3 voxSize = m_mesh->GetNumVoxels();
	
	if( pos < gorg[m_axis] || pos > (gorg[m_axis] + grgn[m_axis]) )
	{
		return;
	}

	u32 posIdx = m_mesh->GetIdxPosition(m_axis, pos);
	//VXLogD("posIdx : %ld gorg :%f grgn :%f pitch :%f pos :%f\n", posIdx, gorg[m_axis], grgn[m_axis], pitch[m_axis], pos);

	float position = pos;
	if(index_coord == 0){
		position = gorg[m_axis] + static_cast<f32>(posIdx) * pitch[m_axis] + pitch[m_axis] * .5f;
	}
	
	// Boundary Set
	{
		vec3 org = gorg;
		org[m_axis] = position;
		BoundarySetter(m_boundary->GetVertex(), m_boundary->GetIndex(), m_axis, org, grgn, boundaryColor);
		m_boundary->EnableNeedUpdate();
	}

	
	if( boundaryOnly ) {
		return;
	}


	// Update Grid
	{
		vec3 org = gorg;
		org[m_axis] = position;
		GridSetter(m_grid->GetVertex(), m_grid->GetIndex(), m_axis, org, grgn, idx2(m_uvw[0], m_uvw[1]), lineColor);
		m_grid->EnableNeedUpdate();
	}

}


} // namespace SG
} // namespace VX



