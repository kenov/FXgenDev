/*
 *  SG/Util.h
 *
 */

#ifndef INCLUDE_VX_SG_UTIL_H
#define INCLUDE_VX_SG_UTIL_H

#include <float.h>
#include "VoxelCartesianL.h"
#include "VoxelCartesianBCflag.h"



namespace VX {
namespace SG{
	inline b8 _getbbox(const VX::SG::Node* node, VX::Math::vec3& box_max, VX::Math::vec3& box_min, b8 selectonly, b8 ignoreDomain)
	{
		b8 r = false;
		NODETYPE t = node->GetType();
		if (t == NODETYPE_GROUP)
		{
			const Group* g = static_cast<const Group*>(node);
			const s32 n = g->GetChildCount();
			for (s32 i = 0; i < n; i++)
				r |= _getbbox(g->GetChild(i), box_max, box_min, selectonly, ignoreDomain);
		}
		else if (!ignoreDomain && t == NODETYPE_BBOX_DOMAIN_BCM_GROUP)
		{
			const Group* g = static_cast<const Group*>(node);
			const s32 n = g->GetChildCount();
			for (s32 i = 0; i < n; i++)
				r |= _getbbox(g->GetChild(i), box_max, box_min, selectonly, ignoreDomain);
		}
		else if (t == NODETYPE_TRANSFORM)
		{
			const Transform* trs = static_cast<const Transform*>(node);
			const s32 n = trs->GetChildCount();
			for (s32 i = 0; i < n; i++)
				r |= _getbbox(trs->GetChild(i), box_max, box_min, selectonly, ignoreDomain);
		}
		else if (t == NODETYPE_GEOMETRY)
		{
			using namespace VX::Math;
			const Geometry* g = static_cast<const Geometry*>(node);
			if (!selectonly) {
				for (u32 i = 0; i < 3; i++) {
					box_max[i] = max(g->GetMax()[i], box_max[i]);
					box_min[i] = min(g->GetMin()[i], box_min[i]);
				}
				return true;
			} else {
				const u32* idb = g->GetIDBuffer();
				const VX::SG::Index n = g->GetIndexCount();
				const VX::SG::Index* idx = g->GetIndex();
				const VX::SG::Geometry::VertexFormat* v = g->GetVertex();
				b8 hit = false;
				for (u32 i = 0; i < n; i+=3){
					if (idb[i/3] & 0x000000FF) {
						hit = true;
						for (u32 t = 0; t < 3; t++){
							for (u32 j = 0; j < 3; j++) {
								box_max[j] = max(v[idx[i+t]].pos[j], box_max[j]);
								box_min[j] = min(v[idx[i+t]].pos[j], box_min[j]);
							}
						}
					}
				}
				return hit;
			}
		}
		else if ( !selectonly && t == NODETYPE_MESH_CARTESIAN )
		{
			using namespace VX::Math;
			const MeshCartesian* g = static_cast<const MeshCartesian*>(node);
			for(u32 i = 0; i < 3; i++){
				box_max[i] = max(g->GetMax()[i], box_max[i]);
				box_min[i] = min(g->GetMin()[i], box_min[i]);
			}
			return true;
		}
		else if ( !selectonly && t == NODETYPE_MESH_BCM )
		{
			using namespace VX::Math;
			const MeshBCM* g = static_cast<const MeshBCM*>(node);
			for(u32 i = 0; i < 3; i++){
				box_max[i] = max(g->GetMax()[i], box_max[i]);
				box_min[i] = min(g->GetMin()[i], box_min[i]);
			}
			return true;
		}
		else if (!ignoreDomain && t == NODETYPE_BBOX_DOMAIN_CARTESIAN)
		{
			using namespace VX::Math;
			const DomainCartesian* d = static_cast<const DomainCartesian*>(node);
			if (selectonly && !d->GetSelection())
				return false;
			
			vec3 bmin, bmax;
			d->GetBBoxMinMax(bmin, bmax);
			for (u32 j = 0; j < 3; j++) {
				box_max[j] = max(bmax[j], box_max[j]);
				box_min[j] = min(bmin[j], box_min[j]);
			}
			return true;
		}
		
		else if (!ignoreDomain && t == NODETYPE_VOXEL_CARTESIAN_G)
		{
			
			using namespace VX::Math;
			const VoxelCartesianG* d = dynamic_cast<const VoxelCartesianG*>(node);
			
					
			//self box check
			if (selectonly ){

				const s32 n = d->GetChildCount();
				for (s32 i = 0; i < n; i++){
					//call local domain
					r |= _getbbox(d->GetChild(i), box_max, box_min, selectonly, ignoreDomain);
				}

			}else{
				// set global bbox			
				vec3 bmin, bmax;
				d->GetBBoxMinMax(bmin, bmax);
				for (u32 j = 0; j < 3; j++) {
					box_max[j] = max(bmax[j], box_max[j]);
					box_min[j] = min(bmin[j], box_min[j]);
				}
				r |= true;
			}
			
			
		}
	
		else if (!ignoreDomain && t == NODETYPE_VOXEL_CARTESIAN_L)
		{
			
			using namespace VX::Math;
			const VoxelCartesianL* d = dynamic_cast<const VoxelCartesianL*>(node);

			const s32 n = d->GetChildCount();
			for (s32 i = 0; i < n; i++){
				//call bcflag
				r |= _getbbox(d->GetChild(i), box_max, box_min, selectonly, ignoreDomain);
			}

			//self box check
			if (selectonly && !d->GetSelection()){
				;//do nothing
			}else{
				// set local bbox			
				vec3 bmin, bmax;
				d->GetBBoxMinMax(bmin, bmax);
				for (u32 j = 0; j < 3; j++) {
					box_max[j] = max(bmax[j], box_max[j]);
					box_min[j] = min(bmin[j], box_min[j]);
				}
				r |= true;
			}
		}
		else if (!ignoreDomain && t == NODETYPE_VOXEL_CARTESIAN_BCFLAG)
		{
			
			using namespace VX::Math;
			const VoxelCartesianBCflag* b = dynamic_cast<const VoxelCartesianBCflag*>(node);
			const GeometryBVH* geo = b->GetGridGeometry();
			if(geo!=NULL){
				//call bcflag
				r |= _getbbox(geo, box_max, box_min, selectonly, ignoreDomain);
			}
		}
		
		return r;
	}
	


	inline b8 GetBBox(const VX::SG::Node* node, VX::Math::vec3& box_max, VX::Math::vec3& box_min, b8 selectonly = false, b8 ignoreDomain = false)
	{
		box_max = VX::Math::vec3(-FLT_MAX,-FLT_MAX,-FLT_MAX);
		box_min = VX::Math::vec3( FLT_MAX, FLT_MAX, FLT_MAX);
		return _getbbox(node, box_max, box_min, selectonly, ignoreDomain);
	}


	template<class T>
	inline void visitAllGeometry(const VX::SG::Node* node, T& func, b8 visibleOnly)
	{
		if (!node)
			return;
		
		if (node->GetIntermediate())
			return;
		
		if (visibleOnly && !node->GetVisible())
			return;

		NODETYPE t = node->GetType();
		if (t == NODETYPE_GROUP || t == NODETYPE_TRANSFORM)
		{
			const Group* g = static_cast<const Group*>(node);
			const s32 n = g->GetChildCount();
			for (s32 i = 0; i < n; i++)
				visitAllGeometry(g->GetChild(i), func, visibleOnly);
		}
		else if (t == NODETYPE_GEOMETRY)
		{
			using namespace VX::Math;
			const Geometry* g = static_cast<const Geometry*>(node);
			func(g);
		}
	}
	
	template<class T>
	inline void VisitAllGeometry(const VX::SG::Node* node, T& func, b8 visibleOnly = true)
	{
		visitAllGeometry(node, func, visibleOnly);
	}

	template<class T>
	inline void visitAllGeometry(VX::SG::Node* node, T& func, b8 visibleOnly)
	{
		if (!node)
			return;
		
		if (node->GetIntermediate())
			return;
		
		if (visibleOnly && !node->GetVisible())
			return;
		
		NODETYPE t = node->GetType();
		if (t == NODETYPE_GROUP || t == NODETYPE_TRANSFORM)
		{
			Group* g = static_cast<Group*>(node);
			const s32 n = g->GetChildCount();
			for (s32 i = 0; i < n; i++)
				visitAllGeometry(g->GetChild(i), func, visibleOnly);
		}
		else if (t == NODETYPE_GEOMETRY)
		{
			using namespace VX::Math;
			Geometry* g = static_cast<Geometry*>(node);
			func(g);
		}
	}
	
	template<class T>
	inline void VisitAllGeometry(VX::SG::Node* node, T& func, b8 visibleOnly = true)
	{
		visitAllGeometry(node, func, visibleOnly);
	}
	
	// All Node
	template<class T>
	inline void visitAllNode(const VX::SG::Node* node, T& func)
	{
		if (!node)
			return;
		
		func(node);
		
		NODETYPE t = node->GetType();
		if (t == NODETYPE_GROUP || t == NODETYPE_TRANSFORM)
		{
			const Group* g = static_cast<const Group*>(node);
			const s32 n = g->GetChildCount();
			for (s32 i = 0; i < n; i++)
				visitAllNode(g->GetChild(i), func);
		}
	}
	
	template<class T>
	inline void VisitAllNode(const VX::SG::Node* node, T& func)
	{
		visitAllGeometry(node, func);
	}
	
	template<class T>
	inline void visitAllNode(VX::SG::Node* node, T& func)
	{
		if (!node)
			return;

		func(node);
		
		NODETYPE t = node->GetType();
		if (t == NODETYPE_GROUP || t == NODETYPE_TRANSFORM)
		{
			Group* g = static_cast<Group*>(node);
			const s32 n = g->GetChildCount();
			for (s32 i = 0; i < n; i++)
				visitAllNode(g->GetChild(i), func);
		}
	}
	
	template<class T>
	inline void VisitAllNode(VX::SG::Node* node, T& func)
	{
		visitAllNode(node, func);
	}
	
	
	
} // SG
} // VX

#endif // INCLUDE_VX_SG_UTIL_H
