/*
 * VX/SG/GUISettings.h
 *
 * SceneGraph Library
 *
 */

#ifndef INCLUDE_VXSCENEGRAPH_GUISettings_H
#define INCLUDE_VXSCENEGRAPH_GUISettings_H

#include "../Type.h"
#include "Node.h"

#include <string>
#include <vector>
namespace VX
{
	namespace SG
	{

		class GUISettings : public Node
		{

		public:
		
			GUISettings()
			 : Node(NODETYPE_GUI_SETTINGS)
			{

			}
			
			~GUISettings(){}
			
			
			b8 GetSelection() const { return false; };
			void SetSelection(b8 select, b8 flagonly = false){};
			
			const std::vector<VX::Math::vec4>& GetColor() const{
				return m_colors;
			};
			void Clear(){
				m_colors.clear();
			};
			void Append(const VX::Math::vec4& e){
				m_colors.push_back(e);
			};
			


		private:
			std::vector<VX::Math::vec4> m_colors;

		};
		
	} // namespace SG
		
} // namespace VX

#endif // INCLUDE_VXSCENEGRAPH_GUISettings_H
