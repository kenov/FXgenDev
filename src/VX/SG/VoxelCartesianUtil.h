/*
 * 
 * SceneGraphLibrary
 *
 */
#ifndef INCLUDE_VoxelCartesianUtil_H
#define INCLUDE_VoxelCartesianUtil_H

#ifdef _WIN32

#elif MACOS

#else

#include <limits.h>

#endif

namespace {


	inline 
	bool IsCross(const VX::Math::idx2& hIdxA, const VX::Math::idx2& szA,
	             const VX::Math::idx2& hIdxB, const VX::Math::idx2& szB)
	{
		for(int i = 0; i < 2; i++){
			if( (hIdxA[i] + szA[i]-1) < hIdxB[i] )
				return false;
		}
		for(int i = 0; i < 2; i++){
			if( (hIdxB[i] + szB[i]-1) < hIdxA[i] )
				return false;
		}
		return true;
	}

	inline 
	void CalcDivision(const int wholeSize, const int limitSize, std::vector<int>& sizeList)
	{
		int lsize = INT_MAX;
		int divCount = 0;
		while( lsize > limitSize ){
			divCount++;
			lsize = wholeSize / divCount + ( wholeSize % divCount != 0 ? 1 : 0 );
		}

		int rem = wholeSize % divCount;
		sizeList.clear();
		sizeList.resize(divCount);
		for(int i = 0; i < divCount; i++){
			sizeList[i] = wholeSize / divCount + (i < rem ? 1 : 0);
		}
	}

} // namespace 


#endif // INCLUDE_VXSCENEGRAPH_VOX_GEOMETRY_UTIL_H
