/*
 * SceneGraphLibrary
 *
 */

#ifndef INCLUDE_VoxelCartesianL_SelectionHelper_H
#define INCLUDE_VoxelCartesianL_SelectionHelper_H

#include "../Type.h"
#include "../Math.h"
#include "Geometry.h"

#include <vector>


namespace VX
{
	namespace SG
	{

		class VoxelCartesianL;
		class HitVoxelCartesianL;



		class VoxelCartesianL_SelectionHelper
		{
		public:
			public:
			enum ExportPolicy {
				EXP_VOXEL_PITCH = 0,
				EXP_VOXEL_COUNT = 1,
			};
	

			VoxelCartesianL_SelectionHelper(VoxelCartesianL* local);
			~VoxelCartesianL_SelectionHelper();

			//選択
			void SelectSubdomain(u32 idx, b8 select);
			//衝突チェック
			//for clip selection
			b8 IntersectFrustum(const Math::vec3* orgdir, std::vector<VX::SG::HitVoxelCartesianL>& hdoms);
			b8 IntersectFrustum(const Math::vec4* frustum, const Math::matrix& mat, b8 selection = true);
			b8 EndPick();
			b8 GetSelection() const;
			void SetSelection(b8 select, b8 flagonly = false);
			const VX::SG::Geometry* GetGeometry() const;
			const VX::SG::Geometry* GetClipGeometry() const;
			const VX::SG::Geometry* GetBoxGeometry() const;

		private:
			//引数なしコンストラクタ
			VoxelCartesianL_SelectionHelper();

			//枠の構築
			void createRegionScopeGeometry();

			void checkSelection();
			VX::Math::vec3 clipedMinMaxValue(const VX::Math::vec3& clip);
			b8 UpdateGeometry();
			b8 testAABB(const VX::Math::vec3* orgdir, const VX::Math::vec3& aabb_max, const VX::Math::vec3& aabb_min, f32& hitmin) const;
			b8 isClip(const VX::Math::vec3& pos) const;
			void makeLines();
			void makeBox();
			void updateClipGeometry();
			void makeClipBox(VX::SG::Geometry::VertexFormat* vtx, VX::SG::Index* idx, int axis);

			static bool intersect(const VX::Math::vec3* bb_minmax, const VX::Math::vec3& org,
								const VX::Math::vec3& invDir, const u32 dirIsNeg[3], f32& hitmin);

			//utility 関数
			VoxelCartesianL* getLocal();

		private:
			VoxelCartesianL* m_local;

			enum {
				m_lineSelectColor   = 0xFF3F55FF,
				m_lineInactiveColor = 0xFF444444,
				m_lineColor         = 0xFFFFFFFF
			};
			VX::Math::vec3 m_minval, m_maxval;
			VX::Math::idx3 m_numVoxels;
			VX::Math::idx3 m_numDivs;

			//for selection
			u8*            m_subdomain;
			u8*            m_selection;
			
			ExportPolicy  m_exportPolicy;

			enum {
				SELECT_NONE,
				SELECT_MIN,
				SELECT_MAX
			} m_pressingClipHandle;
			f32 m_oldhit;
			VX::Math::vec3 m_oldorgdir[2];
			
			b8 m_selecting;
			
			VX::SG::NodeRefPtr<Geometry> m_line;
			VX::SG::NodeRefPtr<Geometry> m_clipgeo;
			VX::SG::NodeRefPtr<Geometry> m_box;
			
			VX::Math::vec3 m_clip[2];//min,max
			VX::Math::vec3 m_clipdir;
		
			enum AXISDIR{
				X_AXIS = 1,
				Y_AXIS = 2,
				Z_AXIS = 4,
				NEGATIVE_AXIS = 8,
				X_NEGATIVE = X_AXIS,
				Y_NEGATIVE = Y_AXIS,
				Z_NEGATIVE = Z_AXIS,
				X_POSITIVE = X_AXIS|NEGATIVE_AXIS,
				Y_POSITIVE = Y_AXIS|NEGATIVE_AXIS,
				Z_POSITIVE = Z_AXIS|NEGATIVE_AXIS
			};

			template<class T>
			void voxelIterator(T& func, const u8* voxel, u8 flag = 0)
			{
				// outer
				for (s32 y = 0; y < m_numDivs[1]; ++y) {
					for (s32 x = 0; x < m_numDivs[0]; ++x) {
						if (voxel[x + y * m_numDivs[0] + 0 * m_numDivs[0] * m_numDivs[1]] == flag)
							func(x,y,0,Z_NEGATIVE);
						if (voxel[x + y * m_numDivs[0] + (m_numDivs[2]-1) * m_numDivs[0] * m_numDivs[1]] == flag)
							func(x,y,m_numDivs[2]-1,Z_POSITIVE);
					}
				}
				for (s32 z = 0; z < m_numDivs[2]; ++z) {
					for (s32 x = 0; x < m_numDivs[0]; ++x) {
						if (voxel[x + 0 * m_numDivs[0] + z * m_numDivs[0] * m_numDivs[1]] == flag)
							func(x,0,z,Y_NEGATIVE);
						if (voxel[x + (m_numDivs[1]-1)  * m_numDivs[0] + z * m_numDivs[0] * m_numDivs[1]] == flag)
							func(x,m_numDivs[1]-1,z,Y_POSITIVE);
					}
				}
				for (s32 z = 0; z < m_numDivs[2]; ++z) {
					for (s32 y = 0; y < m_numDivs[1]; ++y) {
						if (voxel[0 + y * m_numDivs[0] + z * m_numDivs[0] * m_numDivs[1]] == flag)
							func(0,y,z,X_NEGATIVE);
						if (voxel[m_numDivs[0]-1 + y * m_numDivs[0] + z * m_numDivs[0] * m_numDivs[1]] == flag)
							func(m_numDivs[0]-1,y,z,X_POSITIVE);
					}
				}
				
				// inner
				for (s32 y = 0; y < m_numDivs[1]; ++y) {
					for (s32 x = 0; x < m_numDivs[0]; ++x) {
						for (s32 z = 0; z < m_numDivs[2]-1; ++z) {
							if (voxel[x + y * m_numDivs[0] + z     * m_numDivs[0] * m_numDivs[1]]
							!=  voxel[x + y * m_numDivs[0] + (z+1) * m_numDivs[0] * m_numDivs[1]]){
								func(x,y,z,Z_POSITIVE);
							}
						}
					}
				}
				for (s32 z = 0; z < m_numDivs[2]; ++z) {
					for (s32 y = 0; y < m_numDivs[1]; ++y) {
						for (s32 x = 0; x < m_numDivs[0]-1; ++x) {
							if (voxel[x   + y * m_numDivs[0] + z * m_numDivs[0] * m_numDivs[1]]
							!=  voxel[x+1 + y * m_numDivs[0] + z * m_numDivs[0] * m_numDivs[1]]){
								func(x,y,z,X_POSITIVE);
							}
						}
					}
				}
				for (s32 z = 0; z < m_numDivs[2]; ++z) {
					for (s32 x = 0; x < m_numDivs[0]; ++x) {
						for (s32 y = 0; y < m_numDivs[1]-1; ++y) {
							if (voxel[x + y     * m_numDivs[0] + z * m_numDivs[0] * m_numDivs[1]]
							!=  voxel[x + (y+1) * m_numDivs[0] + z * m_numDivs[0] * m_numDivs[1]]){
								func(x,y,z,Y_POSITIVE);
							}
						}
					}
				}

			}

			class faceCreator
			{
			public:
				//faceCreator(VX::SG::Index* idx, u32* numDivs){
				faceCreator(VX::SG::Index* idx, const VX::Math::idx3& numDivs){
					m_idx = idx;
					m_cnt = 0;
					m_numDivs[0] = numDivs[0];
					m_numDivs[1] = numDivs[1];
					m_numDivs[2] = numDivs[2];
					m_offset = 0;
				}
				void SetVertexOffset(u32 offset)
				{
					m_offset = offset;
				}
				void operator()(u32 x, u32 y, u32 z, u32 dir){
					const s32 lXNum = m_numDivs[0] + 1;
					const s32 lYNum = m_numDivs[1] + 1;
					const s32 d = (dir & NEGATIVE_AXIS ? 1 : 0);
					if (dir & X_AXIS) {
						m_idx[m_cnt * 6    ] = x + d +  y    * lXNum +  z    * lYNum * lXNum + m_offset;
						m_idx[m_cnt * 6 + 1] = x + d + (y+1) * lXNum +  z    * lYNum * lXNum + m_offset;
						m_idx[m_cnt * 6 + 2] = x + d + (y+1) * lXNum + (z+1) * lYNum * lXNum + m_offset;
						m_idx[m_cnt * 6 + 3] = x + d +  y    * lXNum +  z    * lYNum * lXNum + m_offset;
						m_idx[m_cnt * 6 + 4] = x + d + (y+1) * lXNum + (z+1) * lYNum * lXNum + m_offset;
						m_idx[m_cnt * 6 + 5] = x + d +  y    * lXNum + (z+1) * lYNum * lXNum + m_offset;
					}
					if (dir & Y_AXIS) {
						m_idx[m_cnt * 6    ] = x   + (y+d) * lXNum +  z    * lYNum * lXNum + m_offset;
						m_idx[m_cnt * 6 + 1] = x+1 + (y+d) * lXNum +  z    * lYNum * lXNum + m_offset;
						m_idx[m_cnt * 6 + 2] = x+1 + (y+d) * lXNum + (z+1) * lYNum * lXNum + m_offset;
						m_idx[m_cnt * 6 + 3] = x   + (y+d) * lXNum +  z    * lYNum * lXNum + m_offset;
						m_idx[m_cnt * 6 + 4] = x+1 + (y+d) * lXNum + (z+1) * lYNum * lXNum + m_offset;
						m_idx[m_cnt * 6 + 5] = x   + (y+d) * lXNum + (z+1) * lYNum * lXNum + m_offset;
					}
					if (dir & Z_AXIS) {
						m_idx[m_cnt * 6    ] = x   +  y    * lXNum + (z+d) * lYNum * lXNum + m_offset;
						m_idx[m_cnt * 6 + 1] = x+1 +  y    * lXNum + (z+d) * lYNum * lXNum + m_offset;
						m_idx[m_cnt * 6 + 2] = x+1 + (y+1) * lXNum + (z+d) * lYNum * lXNum + m_offset;
						m_idx[m_cnt * 6 + 3] = x   +  y    * lXNum + (z+d) * lYNum * lXNum + m_offset;
						m_idx[m_cnt * 6 + 4] = x+1 + (y+1) * lXNum + (z+d) * lYNum * lXNum + m_offset;
						m_idx[m_cnt * 6 + 5] = x   + (y+1) * lXNum + (z+d) * lYNum * lXNum + m_offset;
					}
					
					m_cnt++;
				}
			private:
				u32 m_cnt;
				VX::SG::Index* m_idx;
				u32 m_numDivs[3];
				u32 m_offset;
			};


		};// end of class 


	} // namespace SG
} // namespace VX

#endif // INCLUDE_VoxelCartesianL_SelectionHelper_H

