/*
 * VX/SG/BBox.h
 *
 * SceneGraphLibrary
 *
 */

#ifndef INCLUDE_VXSCENEGRAPH_BBOX_H
#define INCLUDE_VXSCENEGRAPH_BBOX_H

#include "../Type.h"
#include "../Math.h"

#include "Node.h"

namespace VX
{
	namespace SG
	{
		class BBox : public Geometry
		{
		public:
			BBox(const NODETYPE ntype = NODETYPE_BBOX) : Geometry(ntype)
			{
			}

			virtual ~BBox()
			{
			}

			b8 SetBBoxMinMax(const VX::Math::vec3& minval, const VX::Math::vec3& maxval)
			{
				m_maxval = maxval;
				m_minval = minval;
				return true;
				
			}
			
			virtual b8 Make()
			{
				using namespace VX::Math;
				Alloc(8, 24);
				Geometry::VertexFormat* vtx = GetVertex();
				SetUseVertexLineColor(true);
				vtx[0].pos = vec3(m_minval.x, m_minval.y, m_minval.z); vtx[0].col = m_lineColor;
				vtx[1].pos = vec3(m_maxval.x, m_minval.y, m_minval.z); vtx[1].col = m_lineColor;
				vtx[2].pos = vec3(m_maxval.x, m_maxval.y, m_minval.z); vtx[2].col = m_lineColor;
				vtx[3].pos = vec3(m_minval.x, m_maxval.y, m_minval.z); vtx[3].col = m_lineColor;
				vtx[4].pos = vec3(m_minval.x, m_minval.y, m_maxval.z); vtx[4].col = m_lineColor;
				vtx[5].pos = vec3(m_maxval.x, m_minval.y, m_maxval.z); vtx[5].col = m_lineColor;
				vtx[6].pos = vec3(m_maxval.x, m_maxval.y, m_maxval.z); vtx[6].col = m_lineColor;
				vtx[7].pos = vec3(m_minval.x, m_maxval.y, m_maxval.z); vtx[7].col = m_lineColor;
				Index* idx = GetIndex();
				
				idx[0]  = 0; idx[1]  = 1;
				idx[2]  = 1; idx[3]  = 2;
				idx[4]  = 2; idx[5]  = 3;
				idx[6]  = 3; idx[7]  = 0;
				idx[8]  = 4; idx[9]  = 5;
				idx[10] = 5; idx[11] = 6;
				idx[12] = 6; idx[13] = 7;
				idx[14] = 7; idx[15] = 4;
				idx[16] = 0; idx[17] = 4;
				idx[18] = 1; idx[19] = 5;
				idx[20] = 2; idx[21] = 6;
				idx[22] = 3; idx[23] = 7;

				CalcBounds();
				return true;
			}

			virtual b8 IntersectFrustum(const Math::vec4* frustum, const Math::matrix& mat, b8 selection = true)
			{
				const u32 vn = GetVertexCount();
				const VX::SG::Geometry::VertexFormat* v = GetVertex();
				s32 inner = 0;
				const s32 frustum_num = 4;
				for (u32 i = 0; i < vn; ++i) {
					const VX::Math::vec4 tv = mat * VX::Math::vec4(v[i].pos, 1.0);
					for (s32 f = 0; f < frustum_num; ++f)
					{
						const f32 d = dot(frustum[f], tv);
						if (d <= 0)
							++inner;
					}
				}
				if (inner == vn * frustum_num) {
					SetSelection(selection);
					return true;
				}
				return false;
			}
			
			void SetSelection(b8 selection)
			{
				m_selection = selection;
				const u32 vn = GetVertexCount();
				VX::SG::Geometry::VertexFormat* v = GetVertex();
				if (selection) {
					for (u32 i = 0; i < vn; ++i)
						v[i].col = m_lineSelectColor;
				}
				else
				{
					for (u32 i = 0; i < vn; ++i)
						v[i].col = m_lineColor;
				}
				EnableNeedUpdate();
			}
			
			b8 IsSelection()
			{
				return m_selection;
			}

		// Member

		protected:
			static const u32  m_lineColor = 0xFFCCCC00;//0xFF00FFFF;
			static const u32  m_lineSelectColor = 0xFFCCCCFF;
			b8 m_selection;
			VX::Math::vec3 m_maxval, m_minval;
		};
		
	} // namespace SG
} // namespace VX

#endif // INCLUDE_VXSCENEGRAPH_BBOX_H

