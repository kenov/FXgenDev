/*
 * VX/SG/ColorMap.h
 *
 * Colormap attached to Geometry
 *
 */
/// @file ColorMap.h
/// カラーマップのパラメータ管理クラス
///

#ifndef INCLUDE_VXSCENEGRAPH_COLORMAP_H
#define INCLUDE_VXSCENEGRAPH_COLORMAP_H

#include "../Type.h"
#include "Node.h"
#include <vector>
#include <algorithm>


namespace VX
{
	namespace SG
	{
		///
		/// カラーマップのパラメータ管理クラス.
		/// スカラー値からカラーマップへの変換パラメータを管理する.
		///
		class ColorMap : public Node
		{
		public:
			/// コンストラクタ
			ColorMap()
				: Node(NODETYPE_COLORMAP)
			{
				m_min = 0;
				m_max = 1;
				m_colorTable.assign(1, Math::vec3(1, 1, 1));
				m_needUpdate = false;
			}

			/// 変換テーブルのサイズ
			u32 GetColorTableSize() const { return (u32)m_colorTable.size(); }

			/// 変換テーブル
			const std::vector<Math::vec3>& GetColorTable() const { return m_colorTable; }

			/// 変換テーブル
			std::vector<Math::vec3>& GetColorTable() { return m_colorTable; }

			/// 変換のスケーリングパラメータ
			f32 GetRangeMin() const { return m_min; }

			/// 変換のスケーリングパラメータを設定する
			void SetRangeMin(f32 min_) { m_min = min_; }

			/// 変換のスケーリングパラメータ
			f32 GetRangeMax() const { return m_max; }

			/// 変換のスケーリングパラメータを設定する
			void SetRangeMax(f32 max_) { m_max = max_; }

			/// 変換テーブルを BGRA 32bit 形式でメモリバッファに書き込む
			void WriteBufferBGRA32(void* out, u32 sizeBytes) const
			{
				u8* c = (u8*)out;
				for (size_t i = 0; i < m_colorTable.size(); ++i)
				{
					*c++ = (u8)(m_colorTable[i].z * 255); // B
					*c++ = (u8)(m_colorTable[i].y * 255); // G
					*c++ = (u8)(m_colorTable[i].x * 255); // R
					*c++ = 255;                       // A 
				}
			}

			/// 変換テーブルを RGB 24bit 形式でメモリバッファに書き込む
			void WriteBufferRGB24(void* out, u32 sizeBytes) const
			{
				u8* c = (u8*)out;
				for (size_t i = 0; i < m_colorTable.size(); ++i)
				{
					*c++ = (u8)(m_colorTable[i].x * 255); // R
					*c++ = (u8)(m_colorTable[i].y * 255); // G
					*c++ = (u8)(m_colorTable[i].z * 255); // B
				}
			}

			// for update graphic resource
			b8 GetNeedUpdate() const { return m_needUpdate; }
			void DisableNeedUpdate() { m_needUpdate = false; }
			void EnableNeedUpdate() { m_needUpdate = true; }

			b8 GetSelection() const { return false; }
			void SetSelection(b8 select, b8 flagonly = false) { /* to do nothing */ }

		public:
			/// JET カラーマップで変換テーブルを初期化する.
			/// MATLAB の JET カラーマップ互換形式で変換テーブルを初期化する.
			///
			/// @param[in] colormap 初期化するカラーマップ
			/// @param[in] reverse 変換テーブルを反転するか
			///
			static void Jet(ColorMap& colormap, bool reverse);

			/// HSV の赤～青で変換テーブルを初期化する
			///
			/// @param[in] colormap 初期化するカラーマップ
			///
			static void HSVRedToBlue(ColorMap& colormap)
			{
				const f32 H_BLUE = 2.0f/3.0f;

				std::vector<Math::vec3>& table = colormap.GetColorTable();
				table.resize(256);
				for (s32 i = 0; i < 256; ++i)
				{
					table[i] = hsv2rgb(i/255.0f * H_BLUE, 1, 1);
				}
			}

		private:
			/// convert hsv to rgb
			static inline Math::vec3 hsv2rgb(f32 h, f32 s, f32 v)
			{
				s32 i = (s32)(h * 6.0f);
				f32 f = h * 6.0f - i;
				f32 p = v * (1.0f - s);
				f32 q = v * (1.0f - s*f);
				f32 t = v * (1.0f - s*(1.0f - f));
				switch (i % 6)
				{
				case 0: return Math::vec3(v, t, p); break;
				case 1: return Math::vec3(q, v, p); break;
				case 2: return Math::vec3(p, v, t); break;
				case 3: return Math::vec3(p, q, v); break;
				case 4: return Math::vec3(t, p, v); break;
				case 5: return Math::vec3(v, p, q); break;
				default: return Math::vec3(0, 0, 0);
				}
			}

			f32 m_min;
			f32 m_max;
			std::vector<Math::vec3> m_colorTable; ///< 変換テーブル
			b8 m_needUpdate; ///< 描画リソースの更新が必要か
		};


		inline void ColorMap::Jet(ColorMap& colormap, bool reverse)
		{
			const f32 rgbs[256*3] = {
						0, 0, 0.500f,        0, 0, 0.516f,        0, 0, 0.531f,        0, 0, 0.547f,
						0, 0, 0.563f,        0, 0, 0.578f,        0, 0, 0.594f,        0, 0, 0.610f,
						0, 0, 0.625f,        0, 0, 0.641f,        0, 0, 0.657f,        0, 0, 0.673f,
						0, 0, 0.688f,        0, 0, 0.704f,        0, 0, 0.720f,        0, 0, 0.735f,
						0, 0, 0.751f,        0, 0, 0.767f,        0, 0, 0.782f,        0, 0, 0.798f,
						0, 0, 0.814f,        0, 0, 0.829f,        0, 0, 0.845f,        0, 0, 0.861f,
						0, 0, 0.876f,        0, 0, 0.892f,        0, 0, 0.908f,        0, 0, 0.924f,
						0, 0, 0.939f,        0, 0, 0.955f,        0, 0, 0.971f,        0, 0, 0.986f,
						0, 0.002f, 1,        0, 0.018f, 1,        0, 0.033f, 1,        0, 0.049f, 1,
						0, 0.065f, 1,        0, 0.080f, 1,        0, 0.096f, 1,        0, 0.112f, 1,
						0, 0.127f, 1,        0, 0.143f, 1,        0, 0.159f, 1,        0, 0.175f, 1,
						0, 0.190f, 1,        0, 0.206f, 1,        0, 0.222f, 1,        0, 0.237f, 1,
						0, 0.253f, 1,        0, 0.269f, 1,        0, 0.284f, 1,        0, 0.300f, 1,
						0, 0.316f, 1,        0, 0.331f, 1,        0, 0.347f, 1,        0, 0.363f, 1,
						0, 0.378f, 1,        0, 0.394f, 1,        0, 0.410f, 1,        0, 0.425f, 1,
						0, 0.441f, 1,        0, 0.457f, 1,        0, 0.473f, 1,        0, 0.488f, 1,
						0, 0.504f, 1,        0, 0.520f, 1,        0, 0.535f, 1,        0, 0.551f, 1,
						0, 0.567f, 1,        0, 0.582f, 1,        0, 0.598f, 1,        0, 0.614f, 1,
						0, 0.629f, 1,        0, 0.645f, 1,        0, 0.661f, 1,        0, 0.676f, 1,
						0, 0.692f, 1,        0, 0.708f, 1,        0, 0.724f, 1,        0, 0.739f, 1,
						0, 0.755f, 1,        0, 0.771f, 1,        0, 0.786f, 1,        0, 0.802f, 1,
						0, 0.818f, 1,        0, 0.833f, 1,        0, 0.849f, 1,        0, 0.865f, 1,
						0, 0.880f, 1,        0, 0.896f, 1,        0, 0.912f, 1,        0, 0.927f, 1,
						0, 0.943f, 1,        0, 0.959f, 1,        0, 0.975f, 1,        0, 0.990f, 1,
					0.006f, 1, 0.994f,   0.022f, 1, 0.978f,   0.037f, 1, 0.963f,   0.053f, 1, 0.947f,
					0.069f, 1, 0.931f,   0.084f, 1, 0.916f,   0.100f, 1, 0.900f,   0.116f, 1, 0.884f,
					0.131f, 1, 0.869f,   0.147f, 1, 0.853f,   0.163f, 1, 0.837f,   0.178f, 1, 0.822f,
					0.194f, 1, 0.806f,   0.210f, 1, 0.790f,   0.225f, 1, 0.775f,   0.241f, 1, 0.759f,
					0.257f, 1, 0.743f,   0.273f, 1, 0.727f,   0.288f, 1, 0.712f,   0.304f, 1, 0.696f,
					0.320f, 1, 0.680f,   0.335f, 1, 0.665f,   0.351f, 1, 0.649f,   0.367f, 1, 0.633f,
					0.382f, 1, 0.618f,   0.398f, 1, 0.602f,   0.414f, 1, 0.586f,   0.429f, 1, 0.571f,
					0.445f, 1, 0.555f,   0.461f, 1, 0.539f,   0.476f, 1, 0.524f,   0.492f, 1, 0.508f,
					0.508f, 1, 0.492f,   0.524f, 1, 0.476f,   0.539f, 1, 0.461f,   0.555f, 1, 0.445f,
					0.571f, 1, 0.429f,   0.586f, 1, 0.414f,   0.602f, 1, 0.398f,   0.618f, 1, 0.382f,
					0.633f, 1, 0.367f,   0.649f, 1, 0.351f,   0.665f, 1, 0.335f,   0.680f, 1, 0.320f,
					0.696f, 1, 0.304f,   0.712f, 1, 0.288f,   0.727f, 1, 0.273f,   0.743f, 1, 0.257f,
					0.759f, 1, 0.241f,   0.775f, 1, 0.225f,   0.790f, 1, 0.210f,   0.806f, 1, 0.194f,
					0.822f, 1, 0.178f,   0.837f, 1, 0.163f,   0.853f, 1, 0.147f,   0.869f, 1, 0.131f,
					0.884f, 1, 0.116f,   0.900f, 1, 0.100f,   0.916f, 1, 0.084f,   0.931f, 1, 0.069f,
					0.947f, 1, 0.053f,   0.963f, 1, 0.037f,   0.978f, 1, 0.022f,   0.994f, 1, 0.006f,
						1, 0.990f, 0,        1, 0.975f, 0,        1, 0.959f, 0,        1, 0.943f, 0,
						1, 0.927f, 0,        1, 0.912f, 0,        1, 0.896f, 0,        1, 0.880f, 0,
						1, 0.865f, 0,        1, 0.849f, 0,        1, 0.833f, 0,        1, 0.818f, 0,
						1, 0.802f, 0,        1, 0.786f, 0,        1, 0.771f, 0,        1, 0.755f, 0,
						1, 0.739f, 0,        1, 0.724f, 0,        1, 0.708f, 0,        1, 0.692f, 0,
						1, 0.676f, 0,        1, 0.661f, 0,        1, 0.645f, 0,        1, 0.629f, 0,
						1, 0.614f, 0,        1, 0.598f, 0,        1, 0.582f, 0,        1, 0.567f, 0,
						1, 0.551f, 0,        1, 0.535f, 0,        1, 0.520f, 0,        1, 0.504f, 0,
						1, 0.488f, 0,        1, 0.473f, 0,        1, 0.457f, 0,        1, 0.441f, 0,
						1, 0.425f, 0,        1, 0.410f, 0,        1, 0.394f, 0,        1, 0.378f, 0,
						1, 0.363f, 0,        1, 0.347f, 0,        1, 0.331f, 0,        1, 0.316f, 0,
						1, 0.300f, 0,        1, 0.284f, 0,        1, 0.269f, 0,        1, 0.253f, 0,
						1, 0.237f, 0,        1, 0.222f, 0,        1, 0.206f, 0,        1, 0.190f, 0,
						1, 0.175f, 0,        1, 0.159f, 0,        1, 0.143f, 0,        1, 0.127f, 0,
						1, 0.112f, 0,        1, 0.096f, 0,        1, 0.080f, 0,        1, 0.065f, 0,
						1, 0.049f, 0,        1, 0.033f, 0,        1, 0.018f, 0,        1, 0.002f, 0,
						0.986f, 0, 0,        0.971f, 0, 0,        0.955f, 0, 0,        0.939f, 0, 0,
						0.924f, 0, 0,        0.908f, 0, 0,        0.892f, 0, 0,        0.876f, 0, 0,
						0.861f, 0, 0,        0.845f, 0, 0,        0.829f, 0, 0,        0.814f, 0, 0,
						0.798f, 0, 0,        0.782f, 0, 0,        0.767f, 0, 0,        0.751f, 0, 0,
						0.735f, 0, 0,        0.720f, 0, 0,        0.704f, 0, 0,        0.688f, 0, 0,
						0.673f, 0, 0,        0.657f, 0, 0,        0.641f, 0, 0,        0.625f, 0, 0,
						0.610f, 0, 0,        0.594f, 0, 0,        0.578f, 0, 0,        0.563f, 0, 0,
						0.547f, 0, 0,        0.531f, 0, 0,        0.516f, 0, 0,        0.500f, 0, 0,
			};

			std::vector<Math::vec3>& table = colormap.GetColorTable();
			table.resize(256);
			for (u32 i = 0; i < 256; ++i)
			{
				table[i] = Math::vec3(rgbs[i*3+0], rgbs[i*3+1], rgbs[i*3+2]);
			}
			if (reverse)
			{
				std::reverse(table.begin(), table.end());
			}
		}


	} // namespace SG
} // namespace VX


#endif // INCLUDE_VXSCENEGRAPH_COLORMAP_H