/*
 * VX/SG/Material.h
 *
 * SceneGraph Library
 *
 */

#ifndef INCLUDE_VXSCENEGRAPH_MATERIAL_H
#define INCLUDE_VXSCENEGRAPH_MATERIAL_H

#include "../Type.h"
#include "../Math.h"
#include "Node.h"

#include <map>
#include <string>

namespace VX
{
	namespace SG
	{
			
		class Material : public Node
		{
		public:
			Material()  : Node(NODETYPE_MATERIAL)
			{
				m_color = Math::vec4(0.8f, 0.8f, 0.8f, 1.0f);
				//m_vec4["color"] = Math::vec4(0.8f, 0.8f, 0.8f, 1.0f);
			}
			~Material(){}
			
			void SetShaderName(const std::string& shadername)
			{
				m_shadername = shadername;
			}
			
			const std::string& GetShaderName() const
			{
				return m_shadername;
			}
			
			const VX::Math::vec4& GetColor() const
			{
				return m_color;
			}
			
			void SetColor(const VX::Math::vec4& color)
			{
				m_color = color;
			}

			/*
			void Add(const std::string& name, const Math::matrix4x4& mat)
			{
				m_mat[name] = mat;
			}
			void Add(const std::string& name, const Math::vec4& vec4) 
			{
				m_vec4[name] = vec4;
			}
			
			const Math::matrix4x4& GetMatrix(const std::string& name) const
			{
				static Math::matrix E = Math::Identity();
				std::map<std::string, VX::Math::matrix>::const_iterator it = m_mat.find(name);
				if (it != m_mat.end())
					return it->second;
				else
					return E;
			}
			
			const Math::vec4& GetVec4(const std::string& name) const
			{
				static Math::vec4 e = Math::vec4(0,0,0,0);
				std::map<std::string, VX::Math::vec4>::const_iterator it = m_vec4.find(name);
				if (it != m_vec4.end())
					return it->second;
				else
					return e;
			}
			*/
			b8 GetSelection() const { return false; };
			void SetSelection(b8 select, b8 flagonly = false){};
			

		private:
			//std::map<std::string, VX::Math::matrix4x4> m_mat;
			//std::map<std::string, VX::Math::vec4>      m_vec4;
			VX::Math::vec4 m_color;
			std::string m_shadername;
		};
		
	} // namespace SG
		
} // namespace VX

#endif // INCLUDE_VXSCENEGRAPH_MATERIAL_H
