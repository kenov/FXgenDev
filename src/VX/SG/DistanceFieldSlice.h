/*
 * VX/SG/DistanceFieldSlice.h
 *
 * Distance field slice
 *
 */
/// @file
/// 距離場スライス情報の構築, 管理クラス
///

#ifndef INCLUDE_VXSCENEGRAPH_DISTANCE_FIELD_SLICE_H
#define INCLUDE_VXSCENEGRAPH_DISTANCE_FIELD_SLICE_H

#include "../Type.h"
#include "Node.h"
#include "Group.h"
#include "Geometry.h"


namespace VX
{
	namespace SG
	{
		class DomainBCM;
		class ColorMap;

		///
		/// 距離場スライス情報の構築, 管理クラス
		///
		class DistanceFieldSlice : public Node
		{
		public:
			/// コンストラクタ
			///
			/// @param[in] axis スライス面の垂直軸
			/// @param[in] colorMap 描画に使用するカラーマップ
			///
			explicit DistanceFieldSlice(u32 axis, ColorMap* colorMap);

			/// デストラクタ
			~DistanceFieldSlice();

			/// 指定する位置で距離場スライスを再計算する.
			///
			/// @param[in] pos 新しいスライス位置. 軸上の座標値で指定する
			/// @param[in] sg シーングラフ
			/// @param[in] boundaryOnly true の場合は輪郭線のみを更新し, 距離場を再計算しない
			///
			void UpdatePosition(f32 pos, const Group* sg, b8 boundaryOnly);

			/// 現在の距離場スライス位置
			f32 GetPosition() const;

			/// 輪郭線の形状
			const Geometry* GetBoundaryGeometry() const { return m_boundary; }

			/// 距離場スライス面の形状
			const Geometry* GetFieldGeometry() const { return m_field; }

			/// 関連付けられたカラーマップ
			const ColorMap* GetColorMap() const { return m_colorMap; }

			b8 GetSelection() const { return false; }
			void SetSelection(b8 select, b8 flagonly) {}

		protected:
			virtual void GetClippingBBOX(Math::vec3& min, Math::vec3& max) const = 0;

		private:
			const u32 m_axis; ///< スライス面の垂直軸

			f32 m_position; ///< スライス面の軸上座標
			NodeRefPtr<Geometry> m_boundary; ///< 輪郭線形状
			NodeRefPtr<Geometry> m_field;    ///< 距離場面形状
			NodeRefPtr<ColorMap> m_colorMap; ///< 関連付けられたカラーマップ
		};


	} // namespace SG
} // namespace VX




#endif // INCLUDE_VXSCENEGRAPH_DISTANCE_FIELD_SLICE_H