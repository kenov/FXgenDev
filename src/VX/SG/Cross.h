/*
 * VX/SG/Cross.h
 *
 * SceneGraphLibrary
 *
 */

#ifndef INCLUDE_VXSCENEGRAPH_CROSS_H
#define INCLUDE_VXSCENEGRAPH_CROSS_H

#include "../Type.h"
#include "../Math.h"

#include "Node.h"

namespace VX
{
	namespace SG
	{
		class Cross : public Geometry
		{
		public:
			Cross(const NODETYPE ntype = NODETYPE_CROSS) : Geometry(ntype)
			{
				SetDrawMode(MODE_LINE);
				SetUseVertexLineColor(true);
				m_x = m_y = m_z = 0;
				m_size = 1.0;
				make();

			}

			virtual ~Cross()
			{
			}
			
			virtual b8 IntersectFrustum(const Math::vec4* frustum, const Math::matrix& mat, b8 selection = true)
			{
				return false;
			}
			
			b8 GetSelection() const { return false; };
			void SetSelection(b8 select, b8 flagonly = false){};
			
			void Set(f32 x, f32 y, f32 z, f32 s)
			{
				m_x = x;
				m_y = y;
				m_z = z;
				m_size = s;
				make();
				EnableNeedUpdate();
			}
			
		private:
			f32 m_x,m_y,m_z;
			f32 m_size;
			b8 make()
			{
				Alloc(6, 6);
				using namespace VX::Math;
				Geometry::VertexFormat* vtx = GetVertex();
				
				vtx[0].pos = vec3(-m_size + m_x,          m_y,        m_z); vtx[0].col = 0xFF0000FF;
				vtx[1].pos = vec3( m_size + m_x,          m_y,        m_z); vtx[1].col = 0xFF0000FF;
				vtx[2].pos = vec3(          m_x,-m_size + m_y,        m_z); vtx[2].col = 0xFF00FF00;
				vtx[3].pos = vec3(          m_x, m_size + m_y,        m_z); vtx[3].col = 0xFF00FF00;
				vtx[4].pos = vec3(          m_x,          m_y,-m_size + m_z); vtx[4].col = 0xFFFF0000;
				vtx[5].pos = vec3(          m_x,          m_y, m_size + m_z); vtx[5].col = 0xFFFF0000;
				VXLogD("Cross(%f,%f,%f) - %f\n",m_x,m_y,m_z,m_size);
				Index* idx = GetIndex();
				
				idx[0]  = 0; idx[1]  = 1;
				idx[2]  = 2; idx[3]  = 3;
				idx[4]  = 4; idx[5]  = 5;
				
				CalcBounds();
				return true;
			}
		};
		
	} // namespace SG
} // namespace VX

#endif // INCLUDE_VXSCENEGRAPH_CROSS_H

