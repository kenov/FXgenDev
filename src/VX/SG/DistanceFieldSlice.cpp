/*
 * VX/SG/DistanceFieldSlice.cpp
 *
 * Distance field slice
 *
 */

#include "DistanceFieldSlice.h"
#include "DomainBCM.h"
#include "GeometryBVH.h"
#include "VoxGeometryUtil.h"

// some files should be included before Util.h
#include "Transform.h"
#include "MeshCartesian.h"
#include "MeshBCM.h"
#include "DomainCartesian.h"
#include "VoxelCartesianG.h"

#include "Util.h"


#if defined(_OPENMP)
#define USE_OPENMP
#include <omp.h>
#endif


using namespace VX::Math;

namespace
{
	const int UDIV = 128;
	const int VDIV = 128;
	const u32 BOUNDARY_COLOR = 0xFF3F55FF;

	void fillPlaneIndices(u32 udiv, u32 vdiv, VX::SG::Index* indices)
	{
		VX::SG::Index* idx = indices;
		for (u32 v = 0; v < vdiv; ++v)
		{
			for (u32 u = 0; u < udiv; ++u)
			{
				const u32 base = (udiv + 1) * v + u;
				*idx++ = base;
				*idx++ = base + 1;
				*idx++ = base + udiv+1;
				*idx++ = base + udiv+1;
				*idx++ = base + 1;
				*idx++ = base + udiv+1 + 1;
			}
		}
	}

	void fillPlanePositionNormals(
		u32 udiv,
		u32 vdiv,
		const vec3& origin,
		const vec3& udir,
		const vec3& vdir,
		VX::SG::Geometry::VertexFormat* vertices)
	{
		VX::SG::Geometry::VertexFormat* vtx = vertices;
		const vec3 normal = normalize(cross(udir, vdir));
		for (u32 v = 0; v <= vdiv; ++v)
		{
			for (u32 u = 0; u <= udiv; ++u)
			{
				vtx->pos = origin + udir * (u / (f32)udiv) + vdir * (v / (f32)vdiv);
				vtx->normal = normal;
				vtx++;
			}
		}
	}

	struct Gather
	{
		std::vector<const VX::SG::GeometryBVH*>& geometries;
		explicit Gather(std::vector<const VX::SG::GeometryBVH*>& geometries) : geometries(geometries) {}

		inline void operator() (const VX::SG::Geometry* g) const
		{
			geometries.push_back(static_cast<const VX::SG::GeometryBVH*>(g));
		}
	};

	void getSceneGeometries(const VX::SG::Node* root, std::vector<const VX::SG::GeometryBVH*>& geometries)
	{

		geometries.clear();
		geometries.reserve(16);
		Gather gather(geometries);
		VX::SG::VisitAllGeometry(root, gather, false);
	}
}

namespace VX {
namespace SG {

////////////////////////////////////////////////////////////////////////////////////////////////////

DistanceFieldSlice::DistanceFieldSlice(u32 axis, ColorMap* colorMap)
	: Node(NODETYPE_DISTANCE_FIELD_SLICE), m_axis(axis)
{
	const char* names[] = { "Slice X", "Slice Y", "Slice Z" };
	SetName(names[m_axis]);

	m_boundary = vxnew Geometry();
	m_boundary->SetDrawMode(Geometry::MODE_LINE);
	m_boundary->SetUseVertexLineColor(true);
	m_boundary->Alloc(4, 8);

	m_field = vxnew Geometry();
	m_field->SetDrawMode(Geometry::MODE_POLYGON);
	m_field->SetUseVertexLineColor(true);
	m_field->Alloc((UDIV+1)*(VDIV+1), UDIV*VDIV*6 , Geometry::ALLOC_WITHOUT_IDBUFFER);

	if (!colorMap)
	{
		ColorMap* cm = vxnew ColorMap();
		ColorMap::Jet(*cm, true);
		cm->EnableNeedUpdate();
		m_field->SetColorMap(cm);
		m_colorMap = cm;
	}
	else
	{
		m_field->SetColorMap(colorMap);
		m_colorMap = colorMap;
	}
}

DistanceFieldSlice::~DistanceFieldSlice()
{
}

void DistanceFieldSlice::UpdatePosition(f32 pos, const Group* sg, b8 boundaryOnly)
{
	if (!sg) return;
	std::vector<const GeometryBVH*> geoms;
	getSceneGeometries(sg, geoms);

	vec3 min, max;
	GetClippingBBOX(min, max);
	const vec3 extent = max - min;

	vec3 origin = min;
	vec3 udir;
	vec3 vdir;
	switch (m_axis)
	{
	case 0: // X
		udir = vec3(0, extent.y, 0);
		vdir = vec3(0, 0, extent.z);
		origin.x = pos;
		break;
	case 1: // Y
		udir = vec3(extent.x, 0, 0);
		vdir = vec3(0, 0, extent.z);
		origin.y = pos;
		break;
	default:
	case 2: // Z
		udir = vec3(0, extent.y, 0);
		vdir = vec3(extent.x, 0, 0);
		origin.z = pos;
	}

	// boundary
	{
		BoundarySetter(m_boundary->GetVertex(), m_boundary->GetIndex(), m_axis, origin, extent, BOUNDARY_COLOR);
		m_boundary->EnableNeedUpdate();
	}
	if (boundaryOnly)
	{
		return;
	}

	// field geometry
	VX::SG::Geometry::VertexFormat* vbegin = m_field->GetVertex();
	fillPlaneIndices(UDIV, VDIV, m_field->GetIndex());
	fillPlanePositionNormals(UDIV, VDIV, origin, udir, vdir, vbegin);

	// generate distance field
#if defined(USE_OPENMP)
	#pragma omp parallel
#endif // USE_OPENMP
	{
		using std::min;
		const f32 cutoff = Pow(m_field->GetColorMap()->GetRangeMax(), 2) * 1.2f;
		bool checkLastClosest = false;
		vec3 lastClosest;
		
#if defined(USE_OPENMP)
		#pragma omp for
#endif // USE_OPENMP
		for (int v = 0; v <= VDIV; ++v)
		{
			for (int u = 0; u <= UDIV; ++u)
			{
				VX::SG::Geometry::VertexFormat* vtx = vbegin + v * (UDIV+1) + u;

				f32 dist = cutoff;
				if (checkLastClosest)
				{
					dist = min(cutoff, dot(vtx->pos - lastClosest, vtx->pos - lastClosest));
				}

				for (std::vector<const GeometryBVH*>::const_iterator g = geoms.begin(); g != geoms.end(); ++g)
				{
					f32 d = (*g)->DistanceSqFromPoint(vtx->pos, dist, lastClosest);
					dist = min(d, dist);
				}

				*(vtx->u()) = std::sqrt(dist);

				if (dist < cutoff)
				{
					checkLastClosest = true;
				}
			}
		}
	}

	m_field->EnableNeedUpdate();
	m_position = pos;
}


f32 DistanceFieldSlice::GetPosition() const
{
	return m_position;
}


} // namespace SG
} // namespace VX