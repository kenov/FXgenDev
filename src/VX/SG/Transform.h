/*
 * VX/SG/Transform.h
 *
 * SceneGraph Library
 *
 */

#ifndef INCLUDE_VXSCENEGRAPH_TRANSFORM_H
#define INCLUDE_VXSCENEGRAPH_TRANSFORM_H

#include "../Type.h"
#include "Node.h"

namespace VX
{
	namespace SG
	{
			
		class Transform : public Group
		{
		public:
			Transform() : Group(NODETYPE_TRANSFORM)
			{
				m_mat = Math::Identity();
			}
			~Transform(){}
		
			void SetMatrix(const VX::Math::matrix& mat)
			{
				m_mat = mat;
			}
			const VX::Math::matrix& GetMatrix() const
			{
				return m_mat;
			}
		private:
			VX::Math::matrix m_mat;
		};
		
	} // namespace SG
		
} // namespace VX

#endif // INCLUDE_VXSCENEGRAPH_TRANSFORM_H
