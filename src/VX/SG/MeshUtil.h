/*
 * VX/SG/MeshUtil.h
 *
 * Utility
 */

#ifndef INCLUDE_VXSCENEGRAPH_MESH_UTIL_H
#define INCLUDE_VXSCENEGRAPH_MESH_UTIL_H

#include "../../VOX/BitVoxel.h"
#include "../RLE.h"
#include "../Type.h"
#include "../Math.h"

namespace VX
{
	namespace SG
	{
		// GetCellID from bitVoxel
		inline unsigned char GetCellID( const VOX::bitVoxelCell* bitVoxel, const unsigned int bitWidth, 
		                         const VX::Math::idx3& size, const VX::Math::idx3& pos )
		{
			const unsigned char vox_per_cell = static_cast<unsigned char>((sizeof(VOX::bitVoxelCell) * 8) / bitWidth);

			unsigned char mask = 0;
			for(unsigned int i = 0; i < bitWidth; i++) mask += (1 << i);

			size_t loc = pos.x + (pos.y + pos.z * size.y) * size.x;

			size_t       cellIdx =  loc / vox_per_cell;
			unsigned int bitIdx  = (loc % vox_per_cell) * bitWidth;

			return (bitVoxel[cellIdx] >> bitIdx) & mask;
		}

		// GetCellID from bitVoxel
		inline unsigned int GetBCflg( const VOX::bitVoxelCell* bitVoxel,
		                         const VX::Math::idx3& size, const VX::Math::idx3& pos )
		{
			size_t cellIdx = pos.x + (pos.y + pos.z * size.y) * size.x;
			return bitVoxel[cellIdx];
		}
		
		// CellID Block
		class Block
		{
		public:
			Block(unsigned char* rleBuf, const size_t rleSize, const size_t bitVoxelSize)
			 : m_rleBuf(rleBuf), m_rleSize(rleSize), m_bitVoxelSize(bitVoxelSize)
			{

			}

			~Block() {
				delete [] m_rleBuf; // !! Do not use vxnew !!
			}

			// Get bitVoxel (from RLE)
			VOX::bitVoxelCell* GetBitVoxel() const {
				return rleDecode<VOX::bitVoxelCell, u8>(m_rleBuf, m_rleSize, m_bitVoxelSize * sizeof(VOX::bitVoxelCell));
			}

			const u8*     GetRleBuf()       const { return m_rleBuf; }
			const size_t  GetRleSize()      const { return m_rleSize; }
			const size_t  GetBitVoxelSize() const { return m_bitVoxelSize; }

		private:
			u8*     m_rleBuf;
			size_t  m_rleSize;
			size_t  m_bitVoxelSize;
		};

	} // namepsace SG

} // namespace VX

#endif // INCLUDE_VXSCENEGRAPH_MESH_UTIL_H

