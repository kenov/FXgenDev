#include "VoxelCartesianL_SelectionHelper.h"


#include "VoxelCartesianL.h"
#include "HitVoxelCartesianL.h"


namespace VX {
namespace SG {


//====================== VoxelCartesianL_SelectionHelper ===========================
VoxelCartesianL_SelectionHelper::VoxelCartesianL_SelectionHelper(VoxelCartesianL* local):m_local(local)
{

	assert(local!=NULL);
	createRegionScopeGeometry();
}


VoxelCartesianL_SelectionHelper::VoxelCartesianL_SelectionHelper():m_local(NULL)
{
	assert(false);
}

VoxelCartesianL_SelectionHelper::~VoxelCartesianL_SelectionHelper()
{

}

void VoxelCartesianL_SelectionHelper::createRegionScopeGeometry()
{

			
	const VX::Math::vec3 origin = m_local->GetOrigin(); //VX::Math::vec3(0.0f, 0.0f, 0.0f);//global's value
	const VX::Math::vec3 region = m_local->GetRegion(); //VX::Math::vec3(1.0f, 1.0f, 1.0f);//global's value
			
	m_minval = origin;
	m_maxval = origin + region;
	m_clip[0] = m_minval;
	m_clip[1] = m_maxval;
	m_clipdir = VX::Math::vec3(1.0f,0.0f,0.0f);
	m_numVoxels[0] = m_numVoxels[1] = m_numVoxels[2] = 1;
	m_numDivs[0]   = m_numDivs[1]   = m_numDivs[2]   = 1;

	m_exportPolicy = EXP_VOXEL_PITCH;

	m_selecting = false;
				
	m_pressingClipHandle = SELECT_NONE;
				
	const unsigned int active = 1;
				
	m_subdomain = vxnew u8[m_numDivs[0] * m_numDivs[1] * m_numDivs[2]];
	memset(m_subdomain, active, sizeof(u8) * m_numDivs[0] * m_numDivs[1] * m_numDivs[2]);
	m_selection = vxnew u8[m_numDivs[0] * m_numDivs[1] * m_numDivs[2]];
	memset(m_selection, 0, sizeof(u8) * m_numDivs[0] * m_numDivs[1] * m_numDivs[2]);

	const VX::Math::vec4 line1_normal(150/255.0f, 150/255.0f, 150/255.0f, 1.0f);
	const VX::Math::vec4 line1_active(152/255.0f, 74 /255.0f, 40 /255.0f, 1.0f);
	const VX::Math::vec4 line2_normal(150/255.0f, 150/255.0f, 150/255.0f, 1.0f);
	const VX::Math::vec4 line2_active(192/255.0f, 90 /255.0f, 42 /255.0f, 1.0f);

	m_line = vxnew Geometry();
	m_line->SetUseVertexLineColor(true);
	m_line->SetDrawMode(Geometry::MODE_LINE);
	m_line->SetLineActiveColor(line1_active);
	m_line->SetLineNormalColor(line1_normal);
				
	m_box = vxnew Geometry();
	m_box->SetUseVertexLineColor(true);
	m_box->SetDrawMode(Geometry::MODE_POLYGON);
	m_box->SetLineActiveColor(line2_active);
	m_box->SetLineNormalColor(line2_normal);

				
	m_clipgeo = vxnew Geometry();
	m_clipgeo->SetUseVertexLineColor(true);
	m_clipgeo->SetDrawMode(Geometry::MODE_LINE);
				
	UpdateGeometry();

}

#define CLIPBOXSIZE 0.01f

b8 VoxelCartesianL_SelectionHelper::IntersectFrustum(const Math::vec3* orgdir, std::vector<VX::SG::HitVoxelCartesianL>& hdoms)
{
	if (m_pressingClipHandle)
	{
		const VX::Math::vec3 od = m_oldorgdir[1] * m_oldhit + m_oldorgdir[0];
		const VX::Math::vec3 nd = orgdir[1] * m_oldhit + orgdir[0];
		const VX::Math::vec3 md = nd - od;
		if (m_pressingClipHandle == SELECT_MIN)
			m_clip[0] += md;
		else if (m_pressingClipHandle == SELECT_MAX)
			m_clip[1] += md;
					
		m_clip[0] = clipedMinMaxValue(m_clip[0]);
		m_clip[1] = clipedMinMaxValue(m_clip[1]);
		m_oldorgdir[0] = orgdir[0];
		m_oldorgdir[1] = orgdir[1];
		UpdateGeometry();
		return true;
	}

	f32 hmin = 1.0E+16f;
				
	// Clipbox
	f32 offset = length(m_clip[1] - m_clip[0]) * CLIPBOXSIZE;
	VX::Math::vec3 cliphandle[] = {
		m_clip[1]+VX::Math::vec3(-offset,-offset,-offset),m_clip[1],
		m_clip[0],m_clip[0]+VX::Math::vec3( offset, offset, offset),
	};
	for (s32 i = 0; i < 2; ++i) {
		if (testAABB(orgdir, cliphandle[1+2*i], cliphandle[2*i], hmin)){
			m_pressingClipHandle = (i == 0 ? SELECT_MAX : SELECT_MIN);
			m_oldhit = hmin;
			m_oldorgdir[0] = orgdir[0];
			m_oldorgdir[1] = orgdir[1];
			return true;
		}
	}
									
	b8 hit = false;
	const VX::SG::Geometry::VertexFormat* v = m_line->GetVertex();
				
	const s32 lXNum = m_numDivs[0] + 1;
	const s32 lYNum = m_numDivs[1] + 1;
	for (s32 z = 0; z < m_numDivs[2]; ++z) {
		for (s32 y = 0; y < m_numDivs[1]; ++y) {
			for (s32 x = 0; x < m_numDivs[0]; ++x) {
				const VX::Math::vec3 aabb_min = v[x + y * lXNum + z * lXNum * lYNum].pos;
				const VX::Math::vec3 aabb_max = v[x+1 + (y+1) * lXNum + (z+1) * lXNum * lYNum].pos;
				if (isClip(aabb_max) || isClip(aabb_min))
					continue;
							
				b8 r = testAABB(orgdir, aabb_max, aabb_min, hmin);
				hit |= r;
				if (r) {
					VX::SG::HitVoxelCartesianL dom(getLocal(), x + y * m_numDivs[0] + z * m_numDivs[0] * m_numDivs[1], hmin);
					hdoms.push_back(dom);
				}
			}
		}
	}
					
	return hit;
}

VoxelCartesianL* VoxelCartesianL_SelectionHelper::getLocal()
{
	assert(m_local!=NULL);
	return m_local;
}

b8 VoxelCartesianL_SelectionHelper::IntersectFrustum(const Math::vec4* frustum, const Math::matrix& mat, b8 selection)
{
	VX::SG::Geometry::VertexFormat* v = m_line->GetVertex();
	
	b8 hit = false;
	const s32 frustum_num = 4;
	const u8 sflag = selection ? 1 : 0;
	const s32 lXNum = m_numDivs[0] + 1;
	const s32 lYNum = m_numDivs[1] + 1;
	// if you feel slow, you can optimize this.
	for (s32 z = 0; z < m_numDivs[2]; ++z) {
		for (s32 y = 0; y < m_numDivs[1]; ++y) {
			for (s32 x = 0; x < m_numDivs[0]; ++x) {
				s32 inner = 0;
				const VX::Math::vec4 tv[] = {
					VX::Math::vec4(v[x   +  y    * lXNum +  z    * lXNum * lYNum].pos, 1.0),
					VX::Math::vec4(v[x+1 +  y    * lXNum +  z    * lXNum * lYNum].pos, 1.0),
					VX::Math::vec4(v[x+1 + (y+1) * lXNum +  z    * lXNum * lYNum].pos, 1.0),
					VX::Math::vec4(v[x   + (y+1) * lXNum +  z    * lXNum * lYNum].pos, 1.0),
					VX::Math::vec4(v[x   +  y    * lXNum + (z+1) * lXNum * lYNum].pos, 1.0),
					VX::Math::vec4(v[x   + (y+1) * lXNum + (z+1) * lXNum * lYNum].pos, 1.0),
					VX::Math::vec4(v[x+1 +  y    * lXNum + (z+1) * lXNum * lYNum].pos, 1.0),
					VX::Math::vec4(v[x+1 + (y+1) * lXNum + (z+1) * lXNum * lYNum].pos, 1.0)
				};
				for (s32 p = 0; p < 8; ++p){
					if (isClip(tv[p].xyz()))
						break;
								
					const VX::Math::vec4 tvp = mat * tv[p];
					for (s32 f = 0; f < frustum_num; ++f)
					{
						const f32 d = dot(frustum[f], tvp);
						if (d <= 0)
							++inner;
					}
				}
							
				if (inner == frustum_num * 8)
				{
					m_selection[x + y * m_numDivs[0] + z * m_numDivs[0] * m_numDivs[1]] = sflag;
					hit = true;
				}
			}
		}
	}

	if (hit) {
		UpdateGeometry();
		m_line->EnableNeedUpdate();
		if (selection)
			SetSelection(true, true);
		else
			checkSelection();
	}
	return hit;
}

void VoxelCartesianL_SelectionHelper::checkSelection()
{
	const s32 n = m_numDivs[0] * m_numDivs[1] * m_numDivs[2];
	for (s32 i = 0; i < n; ++i) {
		if (m_selection[i]){
			m_selecting = true;
			return;
		}
	}
	m_selecting = false;
}
			

b8 VoxelCartesianL_SelectionHelper::EndPick()
{
	m_pressingClipHandle = SELECT_NONE;
	return true;
}

VX::Math::vec3 VoxelCartesianL_SelectionHelper::clipedMinMaxValue(const VX::Math::vec3& clip)
{
	VX::Math::vec3 c = clip;
	c.x = (c.x < m_minval.x ? m_minval.x : c.x);
	c.y = (c.y < m_minval.y ? m_minval.y : c.y);
	c.z = (c.z < m_minval.z ? m_minval.z : c.z);
	c.x = (c.x > m_maxval.x ? m_maxval.x : c.x);
	c.y = (c.y > m_maxval.y ? m_maxval.y : c.y);
	c.z = (c.z > m_maxval.z ? m_maxval.z : c.z);
	return c;
}

b8 VoxelCartesianL_SelectionHelper::UpdateGeometry()
{
	makeLines();
	makeBox();
	updateClipGeometry();
	return true;
}			

void VoxelCartesianL_SelectionHelper::updateClipGeometry()
{
	m_clipgeo->Alloc(6*6, 6*6);// 6Line
	Geometry::VertexFormat* vtx = m_clipgeo->GetVertex();
	Index* idx = m_clipgeo->GetIndex();
	for (s32 i = 0; i < 6; ++i)
		makeClipBox(&vtx[i*6], &idx[i*6], i);
				
	m_clipgeo->CalcBounds();
	m_clipgeo->EnableNeedUpdate();
}


b8 VoxelCartesianL_SelectionHelper::testAABB(const VX::Math::vec3* orgdir, const VX::Math::vec3& aabb_max, const VX::Math::vec3& aabb_min, f32& hitmin) const
{
	const VX::Math::vec3 invDir(1.0f / orgdir[1].x, 1.0f / orgdir[1].y, 1.0f / orgdir[1].z);
	u32 dirIsNeg[3] = { invDir.x < 0, invDir.y < 0, invDir.z < 0 };
	const VX::Math::vec3 bb_minmax[] = {aabb_min, aabb_max};
	return intersect(bb_minmax, orgdir[0], invDir, dirIsNeg, hitmin);
}

b8 VoxelCartesianL_SelectionHelper::isClip(const VX::Math::vec3& pos) const
{
	if (pos.x < m_clip[0].x || pos.x > m_clip[1].x
	||  pos.y < m_clip[0].y || pos.y > m_clip[1].y
	||  pos.z < m_clip[0].z || pos.z > m_clip[1].z)
	{
		return true;
	}
	return false;
}


void VoxelCartesianL_SelectionHelper::makeLines()
{
	using namespace VX::Math;
	const s32 lXNum = m_numDivs[0] + 1;
	const s32 lYNum = m_numDivs[1] + 1;
	const s32 lZNum = m_numDivs[2] + 1;
	const u32 lineNum = lXNum * lYNum * m_numDivs[2] + lXNum * m_numDivs[1] * lZNum + m_numDivs[0] * lYNum * lZNum;
	const u32 pointNum = lXNum * lYNum * lZNum;
				
	m_line->Alloc(pointNum * 3, lineNum * 2);
	const u32 activeOffset   = 0;
	const u32 inactiveOffset = pointNum;
	const u32 selectOffset   = pointNum * 2;

	Geometry::VertexFormat* vtx = m_line->GetVertex();
				
	const vec3 maxval, minval;
				
	// Vertex Table
	float *vtxTbl[3] = {0, 0, 0};
	for(int i = 0; i < 3; i++){
		vtxTbl[i] = vxnew float[m_numDivs[i] + 1];
		float pitch = (m_maxval[i] - m_minval[i]) / m_numVoxels[i];
		s32   nbase = m_numVoxels[i] / m_numDivs[i];
		s32   amari = m_numVoxels[i] % m_numDivs[i];
					
		vtxTbl[i][0]             = m_minval[i];
		vtxTbl[i][m_numDivs[i]]  = m_maxval[i];
					
		for(s32 j = 1; j < m_numDivs[i]; j++){
			if( amari > j ){
				vtxTbl[i][j] = m_minval[i] + (nbase + 1) * j * pitch;
			}else{
				vtxTbl[i][j] = m_minval[i] + ((nbase + 1 ) * amari + nbase * (j - amari)) * pitch;
			}
		}
	}
				
	// Vertex
	for (s32 z = 0; z < lZNum; ++z) {
		for (s32 y = 0; y < lYNum; ++y) {
			for (s32 x = 0; x < lXNum; ++x) {
				vtx[x + y * lXNum + z*lXNum*lYNum                 ].pos = vec3( vtxTbl[0][x], vtxTbl[1][y], vtxTbl[2][z] );
				vtx[x + y * lXNum + z*lXNum*lYNum                 ].col = m_lineColor;
				vtx[x + y * lXNum + z*lXNum*lYNum + inactiveOffset].pos = vec3( vtxTbl[0][x], vtxTbl[1][y], vtxTbl[2][z] );
				vtx[x + y * lXNum + z*lXNum*lYNum + inactiveOffset].col = m_lineInactiveColor;
				vtx[x + y * lXNum + z*lXNum*lYNum + selectOffset  ].pos = vec3( vtxTbl[0][x], vtxTbl[1][y], vtxTbl[2][z] );
				vtx[x + y * lXNum + z*lXNum*lYNum + selectOffset  ].col = m_lineSelectColor;
			}
		}
	}
				
	for(int i = 0; i < 3; i++){
		vxdeleteArray(vtxTbl[i]);
	}
				
	// Index
	u32 offset = 0;
	Index* idx = m_line->GetIndex();
	memset(idx, 0, sizeof(Index)*lineNum * 2);
	s32 maxx = static_cast<s32>(m_numDivs[0]-1);
	s32 maxy = static_cast<s32>(m_numDivs[1]-1);
	s32 maxz = static_cast<s32>(m_numDivs[2]-1);
	for (s32 z = 0; z < lZNum; ++z) {
		for (s32 y = 0; y < lYNum; ++y) {
			for (s32 x = 0; x < static_cast<s32>(m_numDivs[0]); ++x) {
				const s32 lineidx = offset + (x + y * m_numDivs[0] + z * m_numDivs[0] * lYNum) * 2;
				using namespace VX::Math;
				const s32 sid[] = {
					x + max(y-1,0   ) * m_numDivs[0] + max(z-1, 0   )* m_numDivs[0] * m_numDivs[1],
					x + max(y-1,0   ) * m_numDivs[0] + min(z  , maxz)* m_numDivs[0] * m_numDivs[1],
					x + min(y  ,maxy) * m_numDivs[0] + max(z-1, 0   )* m_numDivs[0] * m_numDivs[1],
					x + min(y  ,maxy) * m_numDivs[0] + min(z  , maxz)* m_numDivs[0] * m_numDivs[1]
				};
				const s32 sel[] = { m_selection[sid[0]], m_selection[sid[1]], m_selection[sid[2]], m_selection[sid[3]] };
				const s32 dom[] = { m_subdomain[sid[0]], m_subdomain[sid[1]], m_subdomain[sid[2]], m_subdomain[sid[3]] }; // acitve:1, inactive:0
				const s32 actoffset = ((dom[0] & dom[1] & dom[2] & dom[3]) == 1 ? activeOffset : inactiveOffset);
				const s32 seloffset = (sel[0] | sel[1] | sel[2] | sel[3] ? selectOffset : actoffset);
				idx[lineidx    ] = x   + y * lXNum + z * lXNum * lYNum + seloffset;
				idx[lineidx + 1] = x+1 + y * lXNum + z * lXNum * lYNum + seloffset;
				if (isClip(vtx[idx[lineidx]].pos) || isClip(vtx[idx[lineidx+1]].pos) ) {
					idx[lineidx] = 0;
					idx[lineidx+1] = 0;
				}
			}
		}
	}
	offset = m_numDivs[0] * lYNum * lZNum * 2;
	for (s32 z = 0; z < lZNum; ++z) {
		for (s32 x = 0; x < lXNum; ++x) {
			for (s32 y = 0; y < static_cast<s32>(m_numDivs[1]); ++y) {
				const s32 lineidx = offset + (y + x * m_numDivs[1] + z * m_numDivs[1] * lXNum) * 2;
				using namespace VX::Math;
				const s32 sid[] = {
					max(x-1,0   ) + y * m_numDivs[0] + max(z-1, 0   )* m_numDivs[0] * m_numDivs[1],
					max(x-1,0   ) + y * m_numDivs[0] + min(z  , maxz)* m_numDivs[0] * m_numDivs[1],
					min(x  ,maxx) + y * m_numDivs[0] + max(z-1, 0   )* m_numDivs[0] * m_numDivs[1],
					min(x  ,maxx) + y * m_numDivs[0] + min(z  , maxz)* m_numDivs[0] * m_numDivs[1]
				};
				const s32 sel[] = { m_selection[sid[0]], m_selection[sid[1]], m_selection[sid[2]], m_selection[sid[3]] };
				const s32 dom[] = { m_subdomain[sid[0]], m_subdomain[sid[1]], m_subdomain[sid[2]], m_subdomain[sid[3]] }; // acitve:1, inactive:0
				const s32 actoffset = ((dom[0] & dom[1] & dom[2] & dom[3]) == 1 ? activeOffset : inactiveOffset);
				const s32 seloffset = (sel[0] | sel[1] | sel[2] | sel[3] ? selectOffset : actoffset);
				idx[lineidx    ] = x +  y    * lXNum + z * lXNum * lYNum + seloffset;
				idx[lineidx + 1] = x + (y+1) * lXNum + z * lXNum * lYNum + seloffset;
				if (isClip(vtx[idx[lineidx]].pos) || isClip(vtx[idx[lineidx+1]].pos) ) {
					idx[lineidx] = 0;
					idx[lineidx+1] = 0;
				}
			}
		}
	}
	offset += lXNum * m_numDivs[1] * lZNum * 2;
	for (s32 x = 0; x < lXNum; ++x) {
		for (s32 y = 0; y < lYNum; ++y) {
			for (s32 z = 0; z < static_cast<s32>(m_numDivs[2]); ++z) {
				const s32 lineidx = offset + (z + x * m_numDivs[2] + y * m_numDivs[2] * lXNum) * 2;
				using namespace VX::Math;
				const s32 sid[] = {
					max(x-1,0   ) + max(y-1, 0   ) * m_numDivs[0] + z * m_numDivs[0] * m_numDivs[1],
					max(x-1,0   ) + min(y  , maxy) * m_numDivs[0] + z * m_numDivs[0] * m_numDivs[1],
					min(x  ,maxx) + max(y-1, 0   ) * m_numDivs[0] + z * m_numDivs[0] * m_numDivs[1],
					min(x  ,maxx) + min(y  , maxy) * m_numDivs[0] + z * m_numDivs[0] * m_numDivs[1]
				};
				const s32 sel[] = { m_selection[sid[0]], m_selection[sid[1]], m_selection[sid[2]], m_selection[sid[3]] };
				const s32 dom[] = { m_subdomain[sid[0]], m_subdomain[sid[1]], m_subdomain[sid[2]], m_subdomain[sid[3]] }; // acitve:1, inactive:0
				const s32 actoffset = ((dom[0] & dom[1] & dom[2] & dom[3]) == 1 ? activeOffset : inactiveOffset);
				const s32 seloffset = (sel[0] | sel[1] | sel[2] | sel[3] ? selectOffset : actoffset);
				idx[lineidx    ] = x + y * lXNum +  z    * lXNum * lYNum + seloffset;
				idx[lineidx + 1] = x + y * lXNum + (z+1) * lXNum * lYNum + seloffset;
				if (isClip(vtx[idx[lineidx]].pos) || isClip(vtx[idx[lineidx+1]].pos) ) {
					idx[lineidx] = 0;
					idx[lineidx+1] = 0;
				}
			}
		}
	}
	offset += lXNum * lYNum * m_numDivs[2] * 2;
	assert(offset == lineNum * 2);

	m_line->CalcBounds();				
	m_line->EnableNeedUpdate();

}


class faceCounter
{
public:
	faceCounter()    { m_cnt = 0; }
	void operator()(u32 x, u32 y, u32 z, u32 dir){ m_cnt++;   }
	u32 GetTriangleIndexCount(){ return m_cnt * 6; }
private:
	u32 m_cnt;
};

void VoxelCartesianL_SelectionHelper::makeBox()
{
	using namespace VX::Math;
	const s32 lXNum = m_numDivs[0] + 1;
	const s32 lYNum = m_numDivs[1] + 1;
	const s32 lZNum = m_numDivs[2] + 1;
	const u32 pointNum = lXNum * lYNum * lZNum;

	const u8 active = 1;
				
	faceCounter fc;
	voxelIterator(fc, m_subdomain,active);
	voxelIterator(fc, m_selection,1);
	u32 idxCount = fc.GetTriangleIndexCount();
	m_box->Alloc(pointNum*2, idxCount);
				
	// copy vtx
	Geometry::VertexFormat* boxvtx = m_box->GetVertex();
	Geometry::VertexFormat* linevtx = m_line->GetVertex();
	memcpy(boxvtx, linevtx, pointNum * sizeof(Geometry::VertexFormat));
	for (u32 v = 0; v < pointNum; ++v){
		boxvtx[v].col = 0x00FFFFFF;//��I�� ������	
		//boxvtx[v].col = 0x66FFFFFF;//��I�� ������
	}
	memcpy(&boxvtx[pointNum], linevtx, pointNum * sizeof(Geometry::VertexFormat));
	for (u32 v = 0; v < pointNum; ++v){
		boxvtx[v+pointNum].col = 0x333333FF;//�I���@������
		//boxvtx[v+pointNum].col = 0x663333FF;//�I���@������
	}
	const vec3 maxval, minval;
				
	// Index
	Index* idx = m_box->GetIndex();
	memset(idx, 0, sizeof(Index)*idxCount);
	faceCreator fcr(idx, m_numDivs);
	fcr.SetVertexOffset(pointNum);
	voxelIterator(fcr,m_selection,1);
	fcr.SetVertexOffset(0);
	voxelIterator(fcr,m_subdomain,active);
				
	m_box->CalcBounds();
	m_box->EnableNeedUpdate();
}

void VoxelCartesianL_SelectionHelper::makeClipBox(VX::SG::Geometry::VertexFormat* vtx, VX::SG::Index* idx, int axis)
{
	using namespace VX::Math;
	f32 offset = length(m_clip[1] - m_clip[0]) * CLIPBOXSIZE;
	f32 sig = 1.0f;
	s32 clp = 0;
	if (axis >= 3) {
		sig = -1.0f;
		clp = 1;
	}
	offset *= sig;
				
	const float dd = offset * 0.05f;
	vtx[0].pos = m_clip[clp] + vec3(-dd  , -dd    , -dd   ); vtx[0].col = 0xFF0000FF; vtx[0].normal = vec3(1.0f,1.0f,1.0f);
	vtx[1].pos = m_clip[clp] + vec3(offset, -dd   , -dd   ); vtx[1].col = 0xFF0000FF; vtx[1].normal = vec3(1.0f,1.0f,1.0f);
	vtx[2].pos = m_clip[clp] + vec3(-dd   , -dd   , -dd   ); vtx[2].col = 0xFF00FF00; vtx[2].normal = vec3(1.0f,1.0f,1.0f);
	vtx[3].pos = m_clip[clp] + vec3(-dd   , offset, -dd   ); vtx[3].col = 0xFF00FF00; vtx[3].normal = vec3(1.0f,1.0f,1.0f);
	vtx[4].pos = m_clip[clp] + vec3(-dd   , -dd   , -dd   ); vtx[4].col = 0xFFFF0000; vtx[4].normal = vec3(1.0f,1.0f,1.0f);
	vtx[5].pos = m_clip[clp] + vec3(-dd   , -dd   , offset); vtx[5].col = 0xFFFF0000; vtx[5].normal = vec3(1.0f,1.0f,1.0f);
	const Index base = axis * 6;
	idx[0] = base  ;
	idx[1] = base+1;
	idx[2] = base+2;
	idx[3] = base+3;
	idx[4] = base+4;
	idx[5] = base+5;
}

bool VoxelCartesianL_SelectionHelper::intersect(const VX::Math::vec3* bb_minmax, const VX::Math::vec3& org,
								const VX::Math::vec3& invDir, const u32 dirIsNeg[3], f32& hitmin) 
{
	// Check for ray intersection against $x$ and $y$ slabs
	float tmin =  (bb_minmax[    dirIsNeg[0]].x - org.x) * invDir.x;
	float tmax =  (bb_minmax[1 - dirIsNeg[0]].x - org.x) * invDir.x;
	const float tymin = (bb_minmax[    dirIsNeg[1]].y - org.y) * invDir.y;
	const float tymax = (bb_minmax[1 - dirIsNeg[1]].y - org.y) * invDir.y;
	if ((tmin > tymax) || (tymin > tmax))
		return false;
	if (tymin > tmin) tmin = tymin;
	if (tymax < tmax) tmax = tymax;
				
	// Check for ray intersection against $z$ slab
	const float tzmin = (bb_minmax[    dirIsNeg[2]].z - org.z) * invDir.z;
	const float tzmax = (bb_minmax[1 - dirIsNeg[2]].z - org.z) * invDir.z;
	if ((tmin > tzmax) || (tzmin > tmax))
		return false;
	if (tzmin > tmin)
		tmin = tzmin;
	if (tzmax < tmax)
		tmax = tzmax;
	b8 r = (tmin < hitmin) && (tmax > 0);
	hitmin = tmin;
	return r;
}



const VX::SG::Geometry* VoxelCartesianL_SelectionHelper::GetGeometry() const
{
	return m_line;
}
			
const VX::SG::Geometry* VoxelCartesianL_SelectionHelper::GetClipGeometry() const
{
	return m_clipgeo;
}
const VX::SG::Geometry* VoxelCartesianL_SelectionHelper::GetBoxGeometry() const
{
	return m_box;
}

b8 VoxelCartesianL_SelectionHelper::GetSelection() const 
{ 
	return m_selecting; 
}

void VoxelCartesianL_SelectionHelper::SetSelection(b8 select, b8 flagonly)
{
	m_selecting = select;
	if (flagonly)
		return;
	const u8 sflag = (select ? 1 : 0);
	const s32 n = m_numDivs[0] * m_numDivs[1] * m_numDivs[2];
	for (s32 i = 0; i < n; ++i)
		m_selection[i] = sflag;

	UpdateGeometry();
}

void VoxelCartesianL_SelectionHelper::SelectSubdomain(u32 idx, b8 select)
{
	if (idx >= static_cast<u32>(m_numDivs[0] * m_numDivs[1] * m_numDivs[2]))
		return;
				
	const u8 state = (select ? 1 : 0);
	if (m_selection[idx] == state)
		return;
				
	m_selection[idx] = state;
	UpdateGeometry();
	if (select)
		SetSelection(true, true);
	else
		checkSelection();
}

}} // end of namespace SG::VX



