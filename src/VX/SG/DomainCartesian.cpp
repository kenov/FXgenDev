/*
 * VX/SG/Domain_Cartesian.cpp
 *
 * SceneGraphLibrary
 *
 */


#include "../Type.h"
#include "../Math.h"
#include "Node.h"
#include "DomainCartesian.h"

#include <string>

namespace VX {
namespace SG {
	const static u32 boundaryColor = 0xFF3F55FF;
	const static u32 lineColor     = 0xFF050505;

DomainCartesianSlice::DomainCartesianSlice( const u32 axis, const DomainCartesian* domain )
 : DomainSlice(NODETYPE_BBOX_DOMAIN_SLICE), m_domain(domain), m_axis(axis), m_selecting(false)
{
	const char* sliceName[3] = {"Slice X", "Slice Y", "Slice Z"};
	SetName(sliceName[m_axis]);

	VX::Math::vec3 minval;
	VX::Math::vec3 maxval;
	m_domain->GetBBoxMinMax(minval, maxval);
	
	VX::Math::idx3 vox;
	domain->GetVoxelCount(vox);
	m_rootUVW = VX::Math::idx3(
	    axis == 0 ? vox.y : vox.x,
		axis == 2 ? vox.y : vox.z,
		axis == 0 ? vox.x : axis == 1 ? vox.y : vox.z );
	
	m_maxBlocks = vox.x * vox.y * vox.z;

	m_boundary = vxnew Geometry();
	m_boundary->SetUseVertexLineColor(true);
	m_boundary->SetDrawMode(Geometry::MODE_LINE);
	m_boundary->Alloc(4, 8);
	
	m_block = vxnew Geometry();
	m_block->SetUseVertexLineColor(true);
	m_block->SetDrawMode(Geometry::MODE_LINE);
	m_block->Alloc( 4 * m_rootUVW[0] * m_rootUVW[1], 8 * m_maxBlocks);

	m_face = vxnew Geometry();
	m_face->SetUseVertexLineColor(true);
	m_face->SetDrawMode(Geometry::MODE_POLYGON);
	m_face->Alloc(4, 6);

	f32 pos =  domain->GetOrigin()[m_axis] + domain->GetRegion()[m_axis] * 0.5f;
	
	//minval.v[m_axis] + (maxval.v[m_axis] - minval.v[m_axis]) / vox.v[m_axis];
	
	//SetPosition(pos, 0);
}

DomainCartesianSlice::~DomainCartesianSlice()
{

}

f32 DomainCartesianSlice::GetPosition() const
{
	return m_boundary->GetVertex()[0].pos[m_axis];
}

void DomainCartesianSlice::SetPosition( const f32 pos, const u32 index_coord, const b8 boundaryOnly )
{
	using namespace VX::Math;

	const vec3 gorg = m_domain->GetOrigin();
	const vec3 grgn = m_domain->GetRegion();
	const vec3 pitch = m_domain->GetPitch();

	if( pos < gorg[m_axis] || pos > (gorg[m_axis] + grgn[m_axis]) )
	{
		return;
	}

	VX::Math::idx3 vox;
	m_domain->GetVoxelCount(vox);

	m_rootUVW = VX::Math::idx3(
	    m_axis == 0 ? vox.y : vox.x,
		m_axis == 2 ? vox.y : vox.z,
		m_axis == 0 ? vox.x : m_axis == 1 ? vox.y : vox.z );

	if (m_maxBlocks != static_cast<u32>(vox.x * vox.y * vox.z)) {
		m_maxBlocks = vox.x * vox.y * vox.z;
		m_block = vxnew Geometry();
		m_block->SetUseVertexLineColor(true);
		m_block->SetDrawMode(Geometry::MODE_LINE);
		m_block->Alloc( 4 * m_maxBlocks, 8 * m_maxBlocks);
	}

	u32 posIdx = m_domain->GetIdxPosition(m_axis, pos);	
	//VXLogD("posIdx: %d gorg: %f grgn: %f pitch: %f pos: %f\n", posIdx, gorg[m_axis], grgn[m_axis], pitch[m_axis], pos);

	float position = pos;
	if(index_coord == 0){
		position = gorg[m_axis] + (static_cast<f32>(posIdx) + .5f) * pitch[m_axis];
	}

	// Boundary and Face
	{
		vec3 org = gorg;
		org[m_axis] = position;
		BoundarySetter(m_boundary->GetVertex(), m_boundary->GetIndex(), m_axis, org, grgn, boundaryColor, 0);
		m_boundary->EnableNeedUpdate();
		if( boundaryOnly ){
			return;
		}

		FaceSetter(m_face->GetVertex(), m_face->GetIndex(), m_axis, org, grgn, boundaryColor, 0);
		m_face->EnableNeedUpdate();
	}

	//VXLogD("NumBlocks[%d] : %ld m_maxBlocks : %ld\n", m_axis, leafNodes.size(), m_maxBlocks);
	
	Geometry::VertexFormat* bvtx = m_block->GetVertex();
	Index *bidx = m_block->GetIndex();

	m_block->SetIndexCount( static_cast<u32>(m_rootUVW[0] * m_rootUVW[1] * 8) );

	for (s32 v = 0; v < m_rootUVW[1]; v++) {
		for (s32 u = 0; u < m_rootUVW[0]; u++) {
			vec3 rgn = pitch;
			vec3 org = vec3(gorg.x + pitch.x * u,
							gorg.y + pitch.y * (m_axis == 0 ? u : v),
							gorg.z + pitch.z * v);

			org[m_axis] = position;
			s32 inednt = u + m_rootUVW[0] * v;
			BoundarySetter(&bvtx[inednt * 4], &bidx[inednt * 8], m_axis, org, rgn, lineColor, (u32)(inednt * 4));

		}
	}

	m_block->EnableNeedUpdate();
}

void DomainCartesian::makeLines()
{
	using namespace VX::Math;
	const s32 lXNum = m_numDivs[0] + 1;
	const s32 lYNum = m_numDivs[1] + 1;
	const s32 lZNum = m_numDivs[2] + 1;
	const u32 lineNum = lXNum * lYNum * m_numDivs[2] + lXNum * m_numDivs[1] * lZNum + m_numDivs[0] * lYNum * lZNum;
	const u32 pointNum = lXNum * lYNum * lZNum;
				
	m_line->Alloc(pointNum * 3, lineNum * 2);
	const u32 activeOffset   = 0;
	const u32 inactiveOffset = pointNum;
	const u32 selectOffset   = pointNum * 2;

	Geometry::VertexFormat* vtx = m_line->GetVertex();
				
	const vec3 maxval, minval;
				
	// Vertex Table
	float *vtxTbl[3] = {0, 0, 0};
	for(int i = 0; i < 3; i++){
		vtxTbl[i] = vxnew float[m_numDivs[i] + 1];
		float pitch = (m_maxval[i] - m_minval[i]) / m_numVoxels[i];
		s32   nbase = m_numVoxels[i] / m_numDivs[i];
		s32   amari = m_numVoxels[i] % m_numDivs[i];
					
		vtxTbl[i][0]             = m_minval[i];
		vtxTbl[i][m_numDivs[i]]  = m_maxval[i];
					
		for(s32 j = 1; j < m_numDivs[i]; j++){
			if( amari > j ){
				vtxTbl[i][j] = m_minval[i] + (nbase + 1) * j * pitch;
			}else{
				vtxTbl[i][j] = m_minval[i] + ((nbase + 1 ) * amari + nbase * (j - amari)) * pitch;
			}
		}
	}
				
	// Vertex
	for (s32 z = 0; z < lZNum; ++z) {
		for (s32 y = 0; y < lYNum; ++y) {
			for (s32 x = 0; x < lXNum; ++x) {
				vtx[x + y * lXNum + z*lXNum*lYNum                 ].pos = vec3( vtxTbl[0][x], vtxTbl[1][y], vtxTbl[2][z] );
				vtx[x + y * lXNum + z*lXNum*lYNum                 ].col = m_lineColor;
				vtx[x + y * lXNum + z*lXNum*lYNum + inactiveOffset].pos = vec3( vtxTbl[0][x], vtxTbl[1][y], vtxTbl[2][z] );
				vtx[x + y * lXNum + z*lXNum*lYNum + inactiveOffset].col = m_lineInactiveColor;
				vtx[x + y * lXNum + z*lXNum*lYNum + selectOffset  ].pos = vec3( vtxTbl[0][x], vtxTbl[1][y], vtxTbl[2][z] );
				vtx[x + y * lXNum + z*lXNum*lYNum + selectOffset  ].col = m_lineSelectColor;
			}
		}
	}
				
	for(int i = 0; i < 3; i++){
		vxdeleteArray(vtxTbl[i]);
	}
				
	// Index
	u32 offset = 0;
	Index* idx = m_line->GetIndex();
	memset(idx, 0, sizeof(Index)*lineNum * 2);
	s32 maxx = static_cast<s32>(m_numDivs[0]-1);
	s32 maxy = static_cast<s32>(m_numDivs[1]-1);
	s32 maxz = static_cast<s32>(m_numDivs[2]-1);
	for (s32 z = 0; z < lZNum; ++z) {
		for (s32 y = 0; y < lYNum; ++y) {
			for (s32 x = 0; x < static_cast<s32>(m_numDivs[0]); ++x) {
				const s32 lineidx = offset + (x + y * m_numDivs[0] + z * m_numDivs[0] * lYNum) * 2;
				using namespace VX::Math;
				const s32 sid[] = {
					x + max(y-1,0   ) * m_numDivs[0] + max(z-1, 0   )* m_numDivs[0] * m_numDivs[1],
					x + max(y-1,0   ) * m_numDivs[0] + min(z  , maxz)* m_numDivs[0] * m_numDivs[1],
					x + min(y  ,maxy) * m_numDivs[0] + max(z-1, 0   )* m_numDivs[0] * m_numDivs[1],
					x + min(y  ,maxy) * m_numDivs[0] + min(z  , maxz)* m_numDivs[0] * m_numDivs[1]
				};
				const s32 sel[] = { m_selection[sid[0]], m_selection[sid[1]], m_selection[sid[2]], m_selection[sid[3]] };
				const s32 dom[] = { m_subdomain[sid[0]], m_subdomain[sid[1]], m_subdomain[sid[2]], m_subdomain[sid[3]] }; // acitve:1, inactive:0
				const s32 actoffset = ((dom[0] & dom[1] & dom[2] & dom[3]) == 1 ? activeOffset : inactiveOffset);
				const s32 seloffset = (sel[0] | sel[1] | sel[2] | sel[3] ? selectOffset : actoffset);
				idx[lineidx    ] = x   + y * lXNum + z * lXNum * lYNum + seloffset;
				idx[lineidx + 1] = x+1 + y * lXNum + z * lXNum * lYNum + seloffset;
				if (isClip(vtx[idx[lineidx]].pos) || isClip(vtx[idx[lineidx+1]].pos) ) {
					idx[lineidx] = 0;
					idx[lineidx+1] = 0;
				}
			}
		}
	}
	offset = m_numDivs[0] * lYNum * lZNum * 2;
	for (s32 z = 0; z < lZNum; ++z) {
		for (s32 x = 0; x < lXNum; ++x) {
			for (s32 y = 0; y < static_cast<s32>(m_numDivs[1]); ++y) {
				const s32 lineidx = offset + (y + x * m_numDivs[1] + z * m_numDivs[1] * lXNum) * 2;
				using namespace VX::Math;
				const s32 sid[] = {
					max(x-1,0   ) + y * m_numDivs[0] + max(z-1, 0   )* m_numDivs[0] * m_numDivs[1],
					max(x-1,0   ) + y * m_numDivs[0] + min(z  , maxz)* m_numDivs[0] * m_numDivs[1],
					min(x  ,maxx) + y * m_numDivs[0] + max(z-1, 0   )* m_numDivs[0] * m_numDivs[1],
					min(x  ,maxx) + y * m_numDivs[0] + min(z  , maxz)* m_numDivs[0] * m_numDivs[1]
				};
				const s32 sel[] = { m_selection[sid[0]], m_selection[sid[1]], m_selection[sid[2]], m_selection[sid[3]] };
				const s32 dom[] = { m_subdomain[sid[0]], m_subdomain[sid[1]], m_subdomain[sid[2]], m_subdomain[sid[3]] }; // acitve:1, inactive:0
				const s32 actoffset = ((dom[0] & dom[1] & dom[2] & dom[3]) == 1 ? activeOffset : inactiveOffset);
				const s32 seloffset = (sel[0] | sel[1] | sel[2] | sel[3] ? selectOffset : actoffset);
				idx[lineidx    ] = x +  y    * lXNum + z * lXNum * lYNum + seloffset;
				idx[lineidx + 1] = x + (y+1) * lXNum + z * lXNum * lYNum + seloffset;
				if (isClip(vtx[idx[lineidx]].pos) || isClip(vtx[idx[lineidx+1]].pos) ) {
					idx[lineidx] = 0;
					idx[lineidx+1] = 0;
				}
			}
		}
	}
	offset += lXNum * m_numDivs[1] * lZNum * 2;
	for (s32 x = 0; x < lXNum; ++x) {
		for (s32 y = 0; y < lYNum; ++y) {
			for (s32 z = 0; z < static_cast<s32>(m_numDivs[2]); ++z) {
				const s32 lineidx = offset + (z + x * m_numDivs[2] + y * m_numDivs[2] * lXNum) * 2;
				using namespace VX::Math;
				const s32 sid[] = {
					max(x-1,0   ) + max(y-1, 0   ) * m_numDivs[0] + z * m_numDivs[0] * m_numDivs[1],
					max(x-1,0   ) + min(y  , maxy) * m_numDivs[0] + z * m_numDivs[0] * m_numDivs[1],
					min(x  ,maxx) + max(y-1, 0   ) * m_numDivs[0] + z * m_numDivs[0] * m_numDivs[1],
					min(x  ,maxx) + min(y  , maxy) * m_numDivs[0] + z * m_numDivs[0] * m_numDivs[1]
				};
				const s32 sel[] = { m_selection[sid[0]], m_selection[sid[1]], m_selection[sid[2]], m_selection[sid[3]] };
				const s32 dom[] = { m_subdomain[sid[0]], m_subdomain[sid[1]], m_subdomain[sid[2]], m_subdomain[sid[3]] }; // acitve:1, inactive:0
				const s32 actoffset = ((dom[0] & dom[1] & dom[2] & dom[3]) == 1 ? activeOffset : inactiveOffset);
				const s32 seloffset = (sel[0] | sel[1] | sel[2] | sel[3] ? selectOffset : actoffset);
				idx[lineidx    ] = x + y * lXNum +  z    * lXNum * lYNum + seloffset;
				idx[lineidx + 1] = x + y * lXNum + (z+1) * lXNum * lYNum + seloffset;
				if (isClip(vtx[idx[lineidx]].pos) || isClip(vtx[idx[lineidx+1]].pos) ) {
					idx[lineidx] = 0;
					idx[lineidx+1] = 0;
				}
			}
		}
	}
	offset += lXNum * lYNum * m_numDivs[2] * 2;
	assert(offset == lineNum * 2);

	m_line->CalcBounds();				
	m_line->EnableNeedUpdate();

}


} // namespace SG
} // namespace VX

