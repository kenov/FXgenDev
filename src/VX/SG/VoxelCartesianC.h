/*
 * VX/SG/CellIDVoxelCartesian.h
 *
 * SceneGraphLibrary
 *
 */

#ifndef INCLUDE_VXSCENEGRAPH_VoxelCartesianC_H
#define INCLUDE_VXSCENEGRAPH_VoxelCartesianC_H

#include "../Type.h"
#include "../Math.h"

#include "Node.h"
#include "Group.h"
#include "Geometry.h"

#include "MeshUtil.h"

#include <string>
#include <vector>

namespace VX
{
	namespace SG
	{
		class VoxelCartesianSliceC;
		class VoxelCartesianBlock;
		class VoxelCartesianC : public Group
		{
		public:
			
			VoxelCartesianC( const VX::Math::vec3& origin,
			               const VX::Math::vec3& region,
						   const VX::Math::idx3& voxSize,
						   const VX::Math::idx3& divs  );

			~VoxelCartesianC();

	
			const std::string& GetBcfDir() const;
			const VX::Math::idx3& GetNumVoxels() const;
			const VX::Math::idx3& GetNumDivs() const;
			const VX::Math::vec3& GetOrigin() const;
			const VX::Math::vec3& GetRegion() const;
			const VX::Math::vec3& GetMin() const;	
			const VX::Math::vec3 GetMax() const;		
			const VX::Math::vec3& GetPitch() const;

			void SetSlicePosition(const u32 axis, const f32 pos, const u32 index_coord, const b8 boundaryOnly);
			f32 GetSlicePosition(const u32 axis) const ;
			// 座標値からインデックス番号を取得する
			u32 GetIdxPosition(const u32 axis, const f32 coord_pos) const;
			// インデックス番号から座標値を取得する
			f32 GetCoordPosition(const u32 axis, const u32 idx_pos) const;
			b8 GetBBoxMinMax(VX::Math::vec3& minval, VX::Math::vec3& maxval) const;

		private:
			void setDispName();
			//void AddRegionScopeGeometry(const VX::Math::vec3& origin, const VX::Math::vec3& region);

		private:
			VX::Math::vec3 m_org;
			VX::Math::vec3 m_rgn;
			VX::Math::vec3 m_pitch;
			VX::Math::idx3 m_voxSize;
			VX::Math::idx3 m_divs;

			NodeRefPtr<VoxelCartesianSliceC> m_slices[3];

			u64 m_numUseBlockMemory;
		};

	} // namespace SG

} // namespace VX

#endif // INCLUDE_VXSCENEGRAPH_VoxelCartesianC_H

