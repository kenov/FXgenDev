#include "MeshCartesian.h"

#include "../Type.h"
#include "../Math.h"

#include "Node.h"
#include "Group.h"
#include "Geometry.h"
#include "Texture.h"
#include "VoxGeometryUtil.h"

#include "MeshUtil.h"

#include <string>
#include <vector>

#include <limits.h>

namespace {
	const static u32 boundaryColor = 0xFF3F55FF;
	const static u32 lineColor     = 0xFF050505;

	bool IsCross(const VX::Math::idx2& hIdxA, const VX::Math::idx2& szA,
	             const VX::Math::idx2& hIdxB, const VX::Math::idx2& szB)
	{
		for(int i = 0; i < 2; i++){
			if( (hIdxA[i] + szA[i]-1) < hIdxB[i] )
				return false;
		}
		for(int i = 0; i < 2; i++){
			if( (hIdxB[i] + szB[i]-1) < hIdxA[i] )
				return false;
		}
		return true;
	}

	
	void CalcDivision(const int wholeSize, const int limitSize, std::vector<int>& sizeList)
	{
		int lsize = INT_MAX;
		int divCount = 0;
		while( lsize > limitSize ){
			divCount++;
			lsize = wholeSize / divCount + ( wholeSize % divCount != 0 ? 1 : 0 );
		}

		int rem = wholeSize % divCount;
		sizeList.clear();
		sizeList.resize(divCount);
		for(int i = 0; i < divCount; i++){
			sizeList[i] = wholeSize / divCount + (i < rem ? 1 : 0);
		}
	}

} // namespace 


namespace VX {
namespace SG {

//====================== MeshCartesianSlice ===========================
MeshCartesianSlice::MeshCartesianSlice(const u32 axis, const MeshCartesian* mesh )
  : Node(NODETYPE_MESH_CARTESIAN_SLICE), m_mesh(mesh), m_axis(axis)
{	
	const char* sliceName[3] = {"Slice X", "Slice Y", "Slice Z"};
	SetName(sliceName[m_axis]);
	
	const VX::Math::idx3 voxsize = mesh->GetNumVoxels();
	m_uvw[0] = axis == 0 ? voxsize[1] : voxsize[0];
	m_uvw[1] = axis == 2 ? voxsize[1] : voxsize[2];
	m_uvw[2] = axis == 0 ? voxsize[0] : axis == 1 ? voxsize[1] : voxsize[2];
	
	using namespace VX::SG;
	using namespace VX::Math;

	// Boundary
	m_boundary = vxnew Geometry();
	m_boundary->SetUseVertexLineColor(true);
	m_boundary->SetDrawMode(Geometry::MODE_LINE);
	m_boundary->Alloc(4, 8);

	// Grid
	m_grid = vxnew Geometry();
	m_grid->SetUseVertexLineColor(true);
	m_grid->SetDrawMode(Geometry::MODE_LINE);
	u32 gridStride = (m_uvw[0]-1) * 2 + (m_uvw[1]-1) * 2;
	m_grid->Alloc(gridStride, gridStride);

	// BlockBoundary
	const VX::Math::idx3 numDiv3 = mesh->GetNumDivs();
	m_div = VX::Math::idx2( axis == 0 ? numDiv3[1] : numDiv3[0], axis == 2 ? numDiv3[1] : numDiv3[2] );
	m_block = vxnew Geometry();
	m_block->SetUseVertexLineColor(true);
	m_block->SetDrawMode(Geometry::MODE_LINE);
	m_block->Alloc( 4 * (m_div.x * m_div.y), 8 * (m_div.x * m_div.y));

	std::vector< int > uSizeList;
	std::vector< int > vSizeList;

	//const int textureLimit = 32; // TODO Texture::MAX_SIZE
	//const int textureLimit = 512; // TODO Texture::MAX_SIZE
	const int textureLimit = Texture::MAX_SIZE;
	CalcDivision(m_uvw.x, textureLimit, uSizeList);
	CalcDivision(m_uvw.y, textureLimit, vSizeList);

	m_subFaces.resize(uSizeList.size() * vSizeList.size());
	idx2 hIdxCounter(0, 0);
	int cnt = 0;

	for(std::vector<int>::iterator vit = vSizeList.begin(); vit != vSizeList.end(); vit++){
		for(std::vector<int>::iterator uit = uSizeList.begin(); uit != uSizeList.end(); uit++){
			Texture* tex = vxnew Texture(idx2(*uit, *vit));
			m_subFaces[cnt].face = vxnew Geometry();
			m_subFaces[cnt].face->SetDrawMode(Geometry::MODE_POLYGON);
			m_subFaces[cnt].face->SetTexture(tex);
			m_subFaces[cnt].face->Alloc(4, 6);
			
			m_subFaces[cnt].headIndex = hIdxCounter;
			m_subFaces[cnt].size = idx2(*uit, *vit);
			hIdxCounter.x += *uit;
			cnt++;

		}
		hIdxCounter.y += *vit;
		hIdxCounter.x = 0;
	}
	/*
	VXLogD("Create subface[%d]\n", m_axis);
	for(std::vector<SubFace>::iterator it = m_subFaces.begin(); it != m_subFaces.end(); ++it){
		VXLogD("hIdx(%3d %3d) size(%3d %3d)\n", it->headIndex.x, it->headIndex.y, it->size.x, it->size.y);
	}
	*/

	vec3 org = mesh->GetOrigin();
	vec3 rgn = mesh->GetRegion();
	org[m_axis] += rgn[m_axis] * 0.5f;

	SetPosition(org[m_axis], 0);
}

MeshCartesianSlice::~MeshCartesianSlice()
{

}

f32 MeshCartesianSlice::GetPosition() const
{
	return m_boundary->GetVertex()[0].pos[m_axis];
}
void MeshCartesianSlice::SetPosition( const f32 pos, const u32 index_coord, const b8 boundaryOnly )
{
	using namespace VX::Math;
	
	const vec3 gorg = m_mesh->GetOrigin();
	const vec3 grgn = m_mesh->GetRegion();
	const vec3 pitch = m_mesh->GetPitch();
	const idx3 voxSize = m_mesh->GetNumVoxels();
	
	if( pos < gorg[m_axis] || pos > (gorg[m_axis] + grgn[m_axis]) )
	{
		return;
	}

	u32 posIdx = m_mesh->GetIdxPosition(m_axis, pos);
	//VXLogD("posIdx : %ld gorg :%f grgn :%f pitch :%f pos :%f\n", posIdx, gorg[m_axis], grgn[m_axis], pitch[m_axis], pos);

	float position = pos;
	if(index_coord == 0){
		position = gorg[m_axis] + static_cast<f32>(posIdx) * pitch[m_axis] + pitch[m_axis] * .5f;
	}
	
	// Boundary Set
	{
		vec3 org = gorg;
		org[m_axis] = position;
		BoundarySetter(m_boundary->GetVertex(), m_boundary->GetIndex(), m_axis, org, grgn, boundaryColor);
		m_boundary->EnableNeedUpdate();
	}

	
	if( boundaryOnly ) {
		return;
	}

	int cnt = 0;

	std::vector<const MeshCartesian::CartesianBlock*> blockList;
	m_mesh->GetBlocksOnSlice(m_axis, posIdx, blockList);
	//VXLogD("m_axis[%2d] numBlocks : %ld\n", m_axis, blockList.size());

	//u32 bw = m_mesh->GetBitWidth();
	for(std::vector<SubFace>::iterator it = m_subFaces.begin(); it != m_subFaces.end(); it++)
	{
		Texture* tex = it->face->GetTexture();
		u32* img = reinterpret_cast<u32*>(tex->GetImagePtr());
		idx2 texsize = tex->GetSize();

		memset(img, 0, sizeof(u32) * texsize.x * texsize.y);
		
		const idx2 &sbSize = it->size;
		const idx2 &sbHIdx = it->headIndex;
		const idx2 sbTIdx  = (sbHIdx + sbSize) - 1;
		//VXLogD("SubFace : %d\n", cnt);
		
		u32 idxU = m_axis == 0 ? 1 : 0;
		u32 idxV = m_axis == 2 ? 1 : 2;

		//#pragma omp parallel for
		for(s32 i = 0; i < static_cast<s32>(blockList.size()); i++){
			const idx3 bSize3 = blockList[i]->GetSize();
			const idx3 bHIdx3 = blockList[i]->GetHeadIdx();
			const idx3 bTIdx3 = (bHIdx3 + bSize3) - 1;
			idx2 bHIdx(bHIdx3[idxU], bHIdx3[idxV]);
			idx2 bSize(bSize3[idxU], bSize3[idxV]);
			idx2 bTIdx(bTIdx3[idxU], bTIdx3[idxV]);

			
			if( !IsCross(sbHIdx, sbSize, bHIdx, bSize) )
			{
				continue;
			}

			VOX::bitVoxelCell* bitVoxel = blockList[i]->GetBitVoxel();
			idx2 bStIdx( (sbHIdx.x > bHIdx.x ? (sbHIdx.x - bHIdx.x) : 0),
			             (sbHIdx.y > bHIdx.y ? (sbHIdx.y - bHIdx.y) : 0) );

			idx2 bEdIdx( (sbTIdx.x < bTIdx.x ? (bSize.x - (bTIdx.x - sbTIdx.x)-1) : (bSize.x-1)),
				         (sbTIdx.y < bTIdx.y ? (bSize.y - (bTIdx.y - sbTIdx.y)-1) : (bSize.y-1)) );

			idx2 sbOffset( (sbHIdx.x > bHIdx.x ? 0 : (bHIdx.x - sbHIdx.x)),
			               (sbHIdx.y > bHIdx.y ? 0 : (bHIdx.y - sbHIdx.y)) );
			
			const u8 bw = blockList[i]->GetBitWidth();
			for(int v = bStIdx.y; v <= bEdIdx.y; v++){
				for(int u = bStIdx.x; u <= bEdIdx.x; u++){
					idx2 sbLoc( (sbOffset.x + (u - bStIdx.x)), (sbOffset.y + (v - bStIdx.y)) );
					idx3 bPos;
					bPos[m_axis] = posIdx-bHIdx3[m_axis];
					bPos[idxU] = u;
					bPos[idxV] = v;
					u32 id = GetCellID(bitVoxel, bw, bSize3, bPos);
					int r = 0;
					int g = id;
					int b = 0;
					img[sbLoc.x + sbLoc.y * texsize.x] = (b << 0) | (g << 8) | (r << 16) | (255 << 24);
				}
			}
			delete [] bitVoxel; // !! Do not use vxnew !!
		}

		cnt++;

		tex->EnableNeedUpdate();

		vec3 rgn = vec3 (
			m_axis == 0 ? 0.0f : pitch.x *  it->size.x,
			m_axis == 1 ? 0.0f : pitch.y * (m_axis == 0 ? it->size.x : it->size.y),
			m_axis == 2 ? 0.0f : pitch.z *  it->size.y );

		vec3 offset = vec3 (
			m_axis == 0 ? 0.0f : pitch.x *  it->headIndex.x,
			m_axis == 1 ? 0.0f : pitch.y * (m_axis == 0 ? it->headIndex.x : it->headIndex.y),
			m_axis == 2 ? 0.0f : pitch.z *  it->headIndex.y );

		vec3 org = gorg + offset;
		org[m_axis] = position;

		FaceTexSetter(it->face->GetVertex(), it->face->GetIndex(), m_axis, org, rgn, vec2(0.0, 0.0), vec2(1.0, 1.0));
		it->face->EnableNeedUpdate();
	}

	// Update Grid
	{
		vec3 org = gorg;
		org[m_axis] = position;
		GridSetter(m_grid->GetVertex(), m_grid->GetIndex(), m_axis, org, grgn, idx2(m_uvw[0], m_uvw[1]), lineColor);
		m_grid->EnableNeedUpdate();
	}

	// Update Block Boundary
	{
		vec3 org = gorg;
		org[m_axis] = position;
		u32 idxU = m_axis == 0 ? 1 : 0;
		u32 idxV = m_axis == 2 ? 1 : 2;
		for(u32 i = 0; i < static_cast<s32>(blockList.size()); i++){
			const idx3 bSize3 = blockList[i]->GetSize();
			const idx3 bHIdx3 = blockList[i]->GetHeadIdx();

			org[idxU] = gorg[idxU] + (bHIdx3[idxU] * pitch[idxU]);
			org[idxV] = gorg[idxV] + (bHIdx3[idxV] * pitch[idxV]);
			vec3 rgn(0, 0, 0);
			rgn[idxU] = bSize3[idxU] * pitch[idxU];
			rgn[idxV] = bSize3[idxV] * pitch[idxV];
			BoundarySetter(&m_block->GetVertex()[i * 4], &m_block->GetIndex()[i * 8], m_axis, org, rgn, lineColor, (i * 4));
		}
		m_block->EnableNeedUpdate();
	}
}


//====================== MeshCartesian ===========================
MeshCartesian::MeshCartesian( const VX::Math::vec3& origin,
                              const VX::Math::vec3& region,
							  const VX::Math::idx3& voxSize,
							  const VX::Math::idx3& divs,
							  const std::vector<CartesianBlock*>& blocks )
	: m_org(origin),
	  m_rgn(region),
	  m_voxSize(voxSize),
	  m_divs(divs),
      Group(NODETYPE_MESH_CARTESIAN)
	
{
	m_pitch = VX::Math::vec3( m_rgn.x / float(m_voxSize.x),
	                          m_rgn.y / float(m_voxSize.y),
							  m_rgn.z / float(m_voxSize.z) );

	m_blocks.resize(blocks.size());

	m_numUseBlockMemory = 0;
	for(size_t i = 0; i < blocks.size(); i++){
		m_blocks[i] = blocks[i];
		m_numUseBlockMemory += m_blocks[i]->GetRleSize();
	}
	//Memory::AddExternalSize(m_numUseBlockMemory); // TODO

	for(int i = 0; i < 3; i++){
		MeshCartesianSlice* slice = vxnew MeshCartesianSlice(i, this);
		this->AddChild(slice);
		m_slices[i] = slice;
	}

}

MeshCartesian::~MeshCartesian()
{
	for(std::vector<CartesianBlock*>::iterator it = m_blocks.begin(); it != m_blocks.end(); ++it){
		vxdelete(*it);
	}
	//Memory::SubExternalSize(m_numUseBlockMemory); // TODO
}

void MeshCartesian::GetBlocksOnSlice(const u32 axis, const size_t idx, std::vector<const CartesianBlock*> &blockList) const
{
	blockList.clear();

	for(std::vector<CartesianBlock*>::const_iterator it = m_blocks.begin(); it != m_blocks.end(); ++it)
	{
		const VX::Math::idx3 hIdx = (*it)->GetHeadIdx();
		const VX::Math::idx3 size = (*it)->GetSize();
		if( hIdx[axis] <= idx && idx < (hIdx[axis] + size[axis]))
		{
			blockList.push_back(*it);
		}
	}
}

} // namespace SG
} // namespace VX



