#include "VoxelCartesianL.h"

#include "../Type.h"
#include "../Math.h"

#include "Node.h"
#include "Group.h"
#include "Geometry.h"
#include "Texture.h"
#include "VoxGeometryUtil.h"
#include "VoxelCartesianBlock.h"
#include "VoxelCartesianSliceL.h"
#include "VoxelCartesianL_SelectionHelper.h"
#include "HitVoxelCartesianL.h"
#include "VoxelCartesianC.h"

#include "MeshUtil.h"
#include "../../FileIO/VoxelCartesianBlockLoader.h"

#include <string>
#include <vector>

#include <stdio.h>
#include <limits.h>

namespace {
	//order is ABGR 
	const static u32 boundaryColor			= 0xFF00FF00;
	const static u32 select_boundaryColor   = 0xFF0000FF;

} // namespace 

namespace VX {
namespace SG {

//BCflagがサブドメインに設定されてない識別子　-1(ランク番号) , 0 〜　は意味がある数字のためそれ以外を設定
const int VoxelCartesianL::BCFLAG_NONE = -2;

//====================== VoxelCartesianL ===========================
VoxelCartesianL::VoxelCartesianL( 
							  const u32& id,
							  const VX::Math::vec3& origin,
                              const VX::Math::vec3& region,
							  const VX::Math::idx3& voxSize,
							  const VX::Math::idx3& divs,
							  const VX::Math::idx3& headGIdx,
							  const std::string& block_CellID_file,
							  const std::string& block_BCflagID_file,
							  const s32& cellID,
							  const s32& bCflagID,
							  const bool& header_isNeedSwap,
							  const u32& header_gc,
							  const u32& digit_rank,
							  const u32& digit_grid
							  )
	: m_id(id),
	m_org(origin),
	  m_rgn(region),
	  m_voxSize(voxSize),
	  m_divs(divs),
	  m_headIdx_G(headGIdx),
	  m_block_CellID_file(block_CellID_file),
	  m_block_BCflagID_file(block_BCflagID_file),
	  m_CellID(cellID),
	  m_BCflagID(bCflagID),
	  m_load(false),
	  m_block_CellID(NULL),
	  m_block_BCflagID(NULL),
	  m_header_isNeedSwap(header_isNeedSwap),
	  m_header_gc(header_gc),
	  m_headIdx_L(1,1,1),
	  m_digit_rank(digit_rank),
	  m_digit_grid(digit_grid),
      Group(NODETYPE_VOXEL_CARTESIAN_L)
{

	m_pitch = VX::Math::vec3( m_rgn.x / float(m_voxSize.x),
	                          m_rgn.y / float(m_voxSize.y),
							  m_rgn.z / float(m_voxSize.z) );



	//Memory::AddExternalSize(m_numUseBlockMemory); // TODO fuchi



	for(int i = 0; i < 3; i++){
		VoxelCartesianSliceL* slice = vxnew VoxelCartesianSliceL(i, this);
		this->AddChild(slice);
		m_slices[i] = slice;
	}

	// add region scope
	// selection helper
	m_SelectionHelper = vxnew VoxelCartesianL_SelectionHelper(this); 

	// add BCflag node
	AddBCflagNode();

	// set display name
	setDispName();


}


VoxelCartesianL::~VoxelCartesianL()
{
	vxdelete(m_block_CellID);
	vxdelete(m_block_BCflagID);
	//Memory::SubExternalSize(m_numUseBlockMemory); // TODO

	vxdelete(m_SelectionHelper);
}


s32 VoxelCartesianL::GetConst_CellID()const
{
	return m_CellID;
}

s32 VoxelCartesianL::GetConst_BCflagID()const
{
	return m_BCflagID;
}


void VoxelCartesianL::LoadBlockFiles(std::string& errMsg)
{

	std::string errMsg1;
	std::string errMsg2;
	//CellID
	if(m_block_CellID==NULL){
		// load 
		m_block_CellID = VoxelCartesianBlockLoader::Load( VoxelCartesianBlockLoader::MODE_CELLID,
															m_block_CellID_file,
															m_voxSize,
															m_header_isNeedSwap,
															m_header_gc,
															errMsg1);
		if( m_block_CellID == NULL){
			VXLogE("CellID Read File Error (%s) [%s:%d] \n", m_block_CellID_file.c_str(), __FILE__, __LINE__);
		}
	}else{
		VXLogD("CellID Already Exist.[%s:%d] \n",  __FILE__, __LINE__);
	}



	//BCflag
	if(m_block_BCflagID==NULL){
		m_block_BCflagID = VoxelCartesianBlockLoader::Load( VoxelCartesianBlockLoader::MODE_BCFLAG,
															m_block_BCflagID_file,
															m_voxSize,
															m_header_isNeedSwap,
															m_header_gc,
															errMsg2);
		if( m_block_BCflagID == NULL){
			VXLogE("BCflagID Read File Error (%s) [%s:%d] \n", m_block_BCflagID_file.c_str(), __FILE__, __LINE__);
		}
	}else{
		VXLogD("BCflagID Already Exist.[%s:%d] \n",  __FILE__, __LINE__);
	}

	errMsg = errMsg1 + errMsg2;
}

void VoxelCartesianL::UnLoadBlockFiles()
{

	if(m_block_CellID!=NULL){
		VoxelCartesianBlockLoader::UnLoad( m_block_CellID);
		m_block_CellID = NULL;
	}

	if(m_block_BCflagID!=NULL){
		VoxelCartesianBlockLoader::UnLoad( m_block_BCflagID);
		m_block_BCflagID = NULL;
		//GLSLデータのジオメトリもクリアする
		m_BCflag->UnLoad();
	}
}

size_t VoxelCartesianL::GetByteSize()
{
	size_t total = 0;
	if(m_block_CellID!=NULL){
		total += m_block_CellID->GetbitVoxelSize();
	}
	if(m_block_BCflagID!=NULL){
		total += m_block_BCflagID->GetbitVoxelSize();
	}
	return total;
}

std::string VoxelCartesianL::GetNameOnGridCell() const
{
	char name[1000] ;
	sprintf(name, "ID=%d,headIdx(%d,%d,%d),Origin(%f,%f,%f),Region(%f,%f,%f),Voxel(%d,%d,%d)",m_id,
			m_headIdx_G[0],m_headIdx_G[1],m_headIdx_G[2],
			m_org[0],m_org[1],m_org[2],
			m_rgn[0],m_rgn[1],m_rgn[2],
			m_voxSize[0],m_voxSize[1],m_voxSize[2]
		);
	std::string ret = name; 
	return ret;
}

void VoxelCartesianL::setDispName()
{

	char name[1000] ;
	memset( name, 0, sizeof(name) );

	char fmt[1000] ;
	memset( fmt, 0, sizeof(fmt) );
	
	

	sprintf(fmt, "ID=%%0%dd,head(%%0%dd,%%0%dd,%%0%dd)",m_digit_rank,m_digit_grid,m_digit_grid,m_digit_grid);

	sprintf(name, fmt,m_id,
			m_headIdx_G[0],m_headIdx_G[1],m_headIdx_G[2]);
		
	std::string wk(name);
	SetName(wk);
}




const VoxelCartesianBlock* VoxelCartesianL::GetBlock_CellID() const
{
	return m_block_CellID;
}

const VoxelCartesianBlock* VoxelCartesianL::GetBlock_BCflagID() const
{
	return m_block_BCflagID;
}

void VoxelCartesianL::GetBlocksOnSlice(const u32 axis, const size_t idx, std::vector<const VoxelCartesianBlock*> &blockList) const
{
	blockList.clear();

	//idxはLocalの世界の中でのボクセルのインデックスのため
	//相対なので(1,1,1)固定
	//const VX::Math::idx3 hIdx = m_block_CellID->GetHeadIdx();
	const VX::Math::idx3 hIdx = GetHeadIdx_L();

	//const VX::Math::idx3 size = m_block_CellID->GetSize();
	const VX::Math::idx3 size = m_voxSize;
	if( hIdx[axis] <= idx && idx < (hIdx[axis] + size[axis]))
	{
		if(m_block_CellID!=NULL){
			blockList.push_back(m_block_CellID);
		}
	}
}


const VX::Math::idx3& VoxelCartesianL::GetHeadIdx_G() const
{
	return m_headIdx_G;
}

const VX::Math::idx3& VoxelCartesianL::GetHeadIdx_L() const
{
	return m_headIdx_L;
}


const VX::Math::idx3& VoxelCartesianL::GetNumVoxels() const
{
	return m_voxSize;
}

const VX::Math::idx3& VoxelCartesianL::GetNumDivs() const
{
	return m_divs;
}

const VX::Math::vec3& VoxelCartesianL::GetOrigin() const
{
	return m_org;
}

const VX::Math::vec3& VoxelCartesianL::GetRegion() const
{
	return m_rgn;
}

const VX::Math::vec3& VoxelCartesianL::GetMin() const 
{
	return m_org;
}
			
const VX::Math::vec3 VoxelCartesianL::GetMax() const
{
	return (m_org + m_rgn);
}
			
const VX::Math::vec3& VoxelCartesianL::GetPitch() const 
{
	return m_pitch;
}

void VoxelCartesianL::SetSlicePosition(const u32 axis, const f32 pos, const u32 index_coord, const b8 boundaryOnly)
{
	m_slices[axis]->SetPosition(pos, index_coord, boundaryOnly);
}

/**
*	@brief 断面を強制的に再描画する
*/
void VoxelCartesianL::RepaintSlicePlane()
{
	const u32 index_coord = 1;//座標値
	const b8 boundaryOnly = false;
	for(int i=0;i<3;i++){
		f32 pos = GetSlicePosition(i);
		SetSlicePosition(i,pos,index_coord,boundaryOnly);
	}
}

f32 VoxelCartesianL::GetSlicePosition(const u32 axis) const 
{
	return m_slices[axis]->GetPosition();
}

// 座標値からインデックス番号を取得する
u32 VoxelCartesianL::GetIdxPosition(const u32 axis, const f32 coord_pos) const
{
	u32 posIdx = static_cast<u32>((coord_pos - m_org[axis]) / m_pitch[axis]);
	posIdx = static_cast<s32>(posIdx) >= m_voxSize[axis] ? m_voxSize[axis]-1 : posIdx;
	return posIdx;
}

// インデックス番号から座標値を取得する
f32 VoxelCartesianL::GetCoordPosition(const u32 axis, const u32 idx_pos) const
{
	f32 posCoord = m_org[axis] + m_pitch[axis] * idx_pos;
	return posCoord;
}

u32	VoxelCartesianL::Getid() const
{
	return m_id;
}

b8 VoxelCartesianL::IsLoad()const
{
	return m_load;
}

void VoxelCartesianL::SetLoad(const b8& b,std::string& errMsg)
{
	m_load = b;
	if(m_load){
		load_voxdata(errMsg);
	}else{
		clear_voxdata();
	}
}

void VoxelCartesianL::load_voxdata(std::string& errMsg)
{
	//外部ファイルを読む
	LoadBlockFiles(errMsg);
}

void VoxelCartesianL::clear_voxdata()
{
	//外部ファイルを読む
	UnLoadBlockFiles();
}

/**
*	@brief 指定したbcflag値でデータ構築
*			GUIで可視化を選択されたフラグ
*/
void VoxelCartesianL::ChangeShowBCFlag(const std::vector<u8>& BCflags)
{
	// GUIのチェックボックスの状態を電波して処理する
	bool visible = GetVisible();
	m_BCflag->ChangeShowBCFlag(	m_load , visible , BCflags);
}

VoxelCartesianBCflag* VoxelCartesianL::GetBCflagNode()
{
	return m_BCflag; 
}
const VoxelCartesianBCflag* VoxelCartesianL::GetBCflagNode()const
{
	return m_BCflag;
}

b8 VoxelCartesianL::GetBBoxMinMax(VX::Math::vec3& minval, VX::Math::vec3& maxval) const
{
	minval = m_org;
	maxval = minval + m_rgn;
	return true;
}



void VoxelCartesianL::AddBCflagNode()
{

	VX::SG::VoxelCartesianBCflag* f = vxnew VX::SG::VoxelCartesianBCflag(this);
	this->AddChild(f);
	m_BCflag = f;

}


/*
void VoxelCartesianL::setBoundaryGeometryColor(const u32& color)
{

	VX::SG::Geometry::VertexFormat* vtx = m_boundary_geo->GetVertex();
	vtx[0].col = color;
	vtx[1].col = color;
	vtx[2].col = color;
	vtx[3].col = color;
	vtx[4].col = color;
	vtx[5].col = color;
	vtx[6].col = color;
	vtx[7].col = color;

	m_boundary_geo->EnableNeedUpdate();
}
*/
void VoxelCartesianL::Select(b8 bSelect)
{
	SelectSubdomain(0,bSelect);
}
void VoxelCartesianL::SelectSubdomain(u32 idx, b8 select)
{
	m_SelectionHelper->SelectSubdomain(idx,select);
}

b8 VoxelCartesianL::IntersectFrustum(const Math::vec3* orgdir, std::vector<VX::SG::HitVoxelCartesianL>& hdoms)
{
	return m_SelectionHelper->IntersectFrustum(orgdir,hdoms);
}

b8 VoxelCartesianL::IntersectFrustum(const Math::vec4* frustum, const Math::matrix& mat, b8 selection)
{
	return m_SelectionHelper->IntersectFrustum(frustum, mat, selection);
}

b8 VoxelCartesianL::EndPick()
{
	return m_SelectionHelper->EndPick();
}

const VX::SG::Geometry* VoxelCartesianL::GetGeometry() const
{
	return m_SelectionHelper->GetGeometry();
}

const VX::SG::Geometry* VoxelCartesianL::GetClipGeometry() const
{
	return m_SelectionHelper->GetClipGeometry();
}

const VX::SG::Geometry* VoxelCartesianL::GetBoxGeometry() const
{
	return m_SelectionHelper->GetBoxGeometry();
}
b8 VoxelCartesianL::GetSelection() const
{
	return m_SelectionHelper->GetSelection();
}

void VoxelCartesianL::SetSelection(b8 select, b8 flagonly)
{
	m_SelectionHelper->SetSelection(select,flagonly);

	if(select == false && flagonly == false){
		//syncronize to clear selection event , only when clear event from group
		m_BCflag->SetSelection(select,flagonly);
	}
}

b8 VoxelCartesianL::IsVisibleByClippint( const VX::Math::vec3& slicePos)const
{


	// origin と 断面を見て
	if( (m_org.x + m_rgn.x <= slicePos.x) ||
		(m_org.y + m_rgn.y <= slicePos.y) ||
		(m_org.z + m_rgn.z <= slicePos.z) ){
		return false;
	}else{
		return true;
	}
}
    

/**
*	@brief 
*/
void VoxelCartesianL::ScanBCflg(  VoxelCartesianL_Scan& vals)
{
    GetBCflagNode()->Scan(vals);
}	

const VoxelCartesianL_Scan& VoxelCartesianL::GetScanResult_bcf()const
{
	return GetBCflagNode()->GetScanResult();
}
const VoxelCartesianL_Scan& VoxelCartesianL::GetScanResult_cel()const
{
	return m_cel_scan_result;
}    
/**
*	@brief セルＩＤをスキャンする
*/
void VoxelCartesianL::ScanCellID(  VoxelCartesianL_Scan& result)
{
	//スキャン結果
	m_cel_scan_result.Clear();

    if(m_block_CellID==NULL){
        return;
    }
    VOX::bitVoxelCell* bitVoxel = m_block_CellID->GetBitVoxel();
    if(bitVoxel==NULL){
        return;
    }
    const u8 bw = m_block_CellID->GetBitWidth();
    const VX::Math::idx3 bSize3 = GetNumVoxels();
    VX::Math::idx3 bPos;
    for( s32 z=0; z< bSize3[2]; z++ ){
        for( s32 y=0; y< bSize3[1]; y++ ){
            for( s32 x=0; x< bSize3[0]; x++ ){
                bPos.x = x;
                bPos.y = y;
                bPos.z = z;
                u8 v = GetCellID(bitVoxel, bw, bSize3, bPos);
                if(v==0){
                    continue;
                }
				//結果を自分に保存
				m_cel_scan_result.SetEnable(v);                    
            }
        }
    }
        
    delete [] bitVoxel; // !! Do not use vxnew !!

	//結果を呼び出し元に反映
	for( int i=0; i< m_cel_scan_result.GetSize();i++){
		if(m_cel_scan_result.IsEnable(i)){
			result.SetEnable(i);
		}
	}

}
    
}} // end of namespace SG::VX



