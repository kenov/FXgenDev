/*
 * VX/SG/Medium.h
 *
 * SceneGraph Library
 *
 */

#ifndef INCLUDE_VXSCENEGRAPH_MEDIUM_H
#define INCLUDE_VXSCENEGRAPH_MEDIUM_H

#include "../Type.h"
#include "../Math.h"
#include "Node.h"

#include <string>

namespace VX
{
	namespace SG
	{
		class Medium : public Node
		{
		public:
			Medium(const std::string& label = "deflabel",
					u32 id = 0,
					VX::Math::vec4 color = VX::Math::vec4(1,1,1,1),
					const std::string& medType = "Solid")
			: Node(NODETYPE_MEDIUM)
			{
				m_label = label;
				m_color = color;
				m_id    = id;
				m_mediumtype = medType;
			}
			~Medium(){}
			
			void SetLabel(const std::string& label)
			{
				m_label = label;
			}
			
			const std::string GetAbsoluteName() const
			{
				return m_label + std::string(" [") + m_mediumtype + std::string("]");
				//return m_label;
			}

			const std::string& GetLabel() const
			{
				return m_label;
			}
			
			const VX::Math::vec4& GetColor() const
			{
				return m_color;
			}
			
			void SetColor(const VX::Math::vec4& color)
			{
				m_color = color;
			}
			
			void SetID(u32 id)
			{
				m_id = id;
			}
			
			u32 GetID() const
			{
				return m_id;
			}
			
			const std::string& GetMediumType() const
			{
				return m_mediumtype;
			}

			void SetMediumType(const std::string& medType)
			{
				m_mediumtype = medType;
			}

			b8 GetSelection() const { return false; };
			void SetSelection(b8 select, b8 flagonly = false){};
			
			
		private:
			std::string    m_label;
			u32            m_id;
			VX::Math::vec4 m_color;
			std::string    m_mediumtype;
		};
		
	} // namespace SG
		
} // namespace VX

#endif // INCLUDE_VXSCENEGRAPH_MEDIUM_H
