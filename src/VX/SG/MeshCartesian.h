/*
 * VX/SG/CellIDMeshCartesian.h
 *
 * SceneGraphLibrary
 *
 */

#ifndef INCLUDE_VXSCENEGRAPH_MESH_CARTESIAN_H
#define INCLUDE_VXSCENEGRAPH_MESH_CARTESIAN_H

#include "../Type.h"
#include "../Math.h"

#include "Node.h"
#include "Group.h"
#include "Geometry.h"

#include "MeshUtil.h"

#include <string>
#include <vector>

namespace VX
{
	namespace SG
	{
		class MeshCartesian;
		class MeshCartesianSlice;
		

		class MeshCartesianSlice : public Node
		{
		public:
			MeshCartesianSlice( const u32 axis, const MeshCartesian* mesh );
			~MeshCartesianSlice();

			int GetNumFaceGeometry() const
			{
				return static_cast<int>(m_subFaces.size());
			}

			const Geometry* GetFaceGeometry( int i ) const
			{
				return m_subFaces[i].face;
			}

			const Geometry* GetBoundaryGeometry() const
			{
				return m_boundary;
			}

			const Geometry* GetGridGeometry() const 
			{
				return m_grid;
			}

			const Geometry* GetBlockGeometry() const
			{
				return m_block;
			}

			void SetPosition(const f32 pos, const u32 index_coord, const b8 boundaryOnly = false);
			f32 GetPosition() const;
			

			b8 GetSelection() const { return false; }
			void SetSelection(b8 select, b8 flagonly = false) { }

		private:
			const MeshCartesian*  m_mesh;
			const u32             m_axis;

			VX::Math::idx3 m_uvw;
			VX::Math::idx2 m_div;

			NodeRefPtr<Geometry> m_block;
			NodeRefPtr<Geometry> m_grid;
			NodeRefPtr<Geometry> m_boundary;

			struct SubFace
			{
				NodeRefPtr<Geometry> face;
				VX::Math::idx2 headIndex;
				VX::Math::idx2 size;
			};
			std::vector<SubFace> m_subFaces;

		};

		class MeshCartesian : public Group
		{
		public:
			class CartesianBlock
			{
			public:
				/// @note bitVoxelSize = VOX::GetbitVoxelSize( ... )
				CartesianBlock(const VX::Math::idx3 &size, const VX::Math::idx3 &headIdx, 
				               u8* rleBuf, const size_t rleSize, const size_t bitVoxelSize, const u8 bitWidth)
					: m_size(size), m_headIdx(headIdx), m_bitWidth(bitWidth)
				{
					m_block = vxnew Block(rleBuf, rleSize, bitVoxelSize);
				}

				~CartesianBlock(){
					vxdelete(m_block);
				}


				VOX::bitVoxelCell* GetBitVoxel() const
				{
					return m_block->GetBitVoxel();
				}
				
				const VX::Math::idx3& GetSize()    const { return m_size;     }
				const VX::Math::idx3& GetHeadIdx() const { return m_headIdx;  }
				const u8 GetBitWidth()             const { return m_bitWidth; }

				const unsigned char* GetRleBuf() const { return m_block->GetRleBuf(); }
				const size_t GetRleSize() const { return m_block->GetRleSize(); }
				const size_t GetbitVoxelSize() const { return m_block->GetBitVoxelSize(); }
				
			private:
				Block            *m_block;
				VX::Math::idx3    m_size;
				VX::Math::idx3    m_headIdx;
				u8                m_bitWidth;
			};
			
			MeshCartesian( const VX::Math::vec3& origin,
			               const VX::Math::vec3& region,
						   const VX::Math::idx3& voxSize,
						   const VX::Math::idx3& divs,
						   const std::vector<CartesianBlock*>& blocks );

			~MeshCartesian();


			const VX::Math::idx3& GetNumVoxels() const
			{
				return m_voxSize;
			}

			const VX::Math::idx3& GetNumDivs() const
			{
				return m_divs;
			}

			const VX::Math::vec3& GetOrigin() const
			{
				return m_org;
			}

			const VX::Math::vec3& GetRegion() const
			{
				return m_rgn;
			}

			const VX::Math::vec3& GetMin() const 
			{
				return m_org;
			}
			
			const VX::Math::vec3 GetMax() const
			{
				return (m_org + m_rgn);
			}
			
			const VX::Math::vec3& GetPitch() const 
			{
				return m_pitch;
			}

			const std::vector<CartesianBlock*>& GetBlocks() const
			{
				return m_blocks;
			}
			
			void GetBlocksOnSlice(const u32 axis, const size_t idx, std::vector<const CartesianBlock*> &blockList) const;

			void SetSlicePosition(const u32 axis, const f32 pos, const u32 index_coord, const b8 boundaryOnly)
			{
				m_slices[axis]->SetPosition(pos, index_coord, boundaryOnly);
			}

			f32 GetSlicePosition(const u32 axis) const 
			{
				return m_slices[axis]->GetPosition();
			}

			// 座標値からインデックス番号を取得する
			u32 GetIdxPosition(const u32 axis, const f32 coord_pos) const
			{
				u32 posIdx = static_cast<u32>((coord_pos - m_org[axis]) / m_pitch[axis]);
				posIdx = static_cast<s32>(posIdx) >= m_voxSize[axis] ? m_voxSize[axis]-1 : posIdx;
				return posIdx;
			}

			// インデックス番号から座標値を取得する
			f32 GetCoordPosition(const u32 axis, const u32 idx_pos) const
			{
				f32 posCoord = m_org[axis] + m_pitch[axis] * idx_pos;
				return posCoord;
			}

		private:
			VX::Math::vec3 m_org;
			VX::Math::vec3 m_rgn;
			VX::Math::vec3 m_pitch;
			VX::Math::idx3 m_voxSize;
			VX::Math::idx3 m_divs;

			std::vector<CartesianBlock*> m_blocks;
			NodeRefPtr<MeshCartesianSlice> m_slices[3];

			u64 m_numUseBlockMemory;
		};

	} // namespace SG

} // namespace VX

#endif // INCLUDE_VXSCENEGRAPH_MESH_CARTESIAN_H

