/*
 * VX/SG/Group.cpp
 *
 * Group
 *
 */

#include "Group.h"


namespace VX {
namespace SG {


void Group::clearLocalBCOuterBCMedium()
{
	const int n = GetChildCount();
	for (int i = n - 1; i >= 0; --i) {
		VX::SG::Node* node = GetChild(i);
		if (node->GetType() == VX::SG::NODETYPE_LOCALBC
		||  node->GetType() == VX::SG::NODETYPE_OUTERBC
		||  node->GetType() == VX::SG::NODETYPE_MEDIUM)
			RemoveChild(node);
	}
}

} // namespace SG
} // namespace VX



