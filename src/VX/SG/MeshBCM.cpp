#include "MeshBCM.h"

#include "../Type.h"
#include "../Math.h"

#include "Node.h"
#include "Group.h"
#include "Geometry.h"
#include "Texture.h"
#include "VoxGeometryUtil.h"

#include "../../VOX/BCMOctree.h"
#include "../../VOX/RootGrid.h"
#include "../../VOX/Partition.h"
#include "../..//VOX/VOXUtil.h"

#include "MeshUtil.h"

#include <string>
#include <vector>


namespace {
	const static u32 boundaryColor = 0xFF3F55FF;
	const static u32 lineColor     = 0xFF050505;

	inline void GetPedigreeBBox(const VX::Math::vec3& rootOrigin, const VX::Math::vec3& rootRegion, const VOX::Pedigree& p,
	                     VX::Math::vec3& origin, VX::Math::vec3& region)
	{
		s32 lv = p.getLevel();
		VX::Math::vec3 ploc(static_cast<f32>(p.getX()), static_cast<f32>(p.getY()), static_cast<f32>(p.getZ()));

		region = VX::Math::vec3( rootRegion.x / (1 << lv), rootRegion.y / (1 << lv), rootRegion.z / (1 << lv) );
		origin = VX::Math::vec3( rootOrigin.x + region.x * ploc.x,
		                         rootOrigin.y + region.y * ploc.y,
								 rootOrigin.z + region.z * ploc.z );
	}


} // namespace 


namespace VX {
namespace SG {

MeshBCMSlice::MeshBCMSlice( const u32 axis, const MeshBCM* mesh )
  : Node(NODETYPE_MESH_BCM_SLICE), m_mesh(mesh), m_axis(axis)
{
	const char* sliceName[3] = { "Slice X", "Slice Y", "Slice Z" };
	SetName(sliceName[m_axis]);
	
	using namespace VX::SG;
	using namespace VX::Math;
	
	const VOX::BCMOctree* octree   = mesh->GetOctree();
	const VOX::RootGrid*  rootGrdi = octree->getRootGrid();

	const VX::Math::idx3 blockSize = mesh->GetBlockSize();

	const VX::Math::idx2 blockUV( m_axis == 0 ? blockSize[1] : blockSize[0], 
	                              m_axis == 2 ? blockSize[1] : blockSize[2] );

	const u32 maxLevel = mesh->GetMaxOctreeLevel();
	m_maxBlocks = static_cast<u32>(SearchBCMMaxBlockCount( m_axis, octree, maxLevel ));

	// Boundary
	m_boundary = vxnew Geometry();
	m_boundary->SetUseVertexLineColor(true);
	m_boundary->SetDrawMode(Geometry::MODE_LINE);
	m_boundary->Alloc(4, 8);
	m_boundary->SetName(std::string(sliceName[m_axis]) + std::string(" boundary"));

	// Block
	m_block = vxnew Geometry();
	m_block->SetUseVertexLineColor(true);
	m_block->SetDrawMode(Geometry::MODE_LINE);
	m_block->Alloc(m_maxBlocks * 4, m_maxBlocks * 8);
	m_block->SetName(std::string(sliceName[m_axis]) + std::string(" block"));

	// Grid
	u32 nv = ((blockUV.x -1) * 2 + (blockUV.y - 1) * 2) * m_maxBlocks;
	m_grid = vxnew Geometry();
	m_grid->SetUseVertexLineColor(true);
	m_grid->SetDrawMode(Geometry::MODE_LINE);
	m_grid->Alloc(nv, nv);
	m_grid->SetName(std::string(sliceName[m_axis]) + std::string(" grid"));

	// Face
	Texture* tex = CreateTexture(m_maxBlocks, blockUV);	
	m_face = vxnew Geometry();
	m_face->SetDrawMode(Geometry::MODE_POLYGON);
	m_face->SetTexture(tex);
	m_face->Alloc(m_maxBlocks * 4, m_maxBlocks * 6);
	m_face->SetName(std::string(sliceName[m_axis]) + std::string(" face"));

	f32 pos = mesh->GetOrigin()[m_axis] + mesh->GetRegion()[m_axis] * .5f;
	SetPosition(pos, 0);
}

MeshBCMSlice::~MeshBCMSlice( )
{

}

f32 MeshBCMSlice::GetPosition() const
{
	return m_boundary->GetVertex()[0].pos[m_axis];
}

void MeshBCMSlice::SetPosition( const f32 pos, const u32 index_coord, const b8 boundaryOnly )
{
	using namespace VX::Math;

	const vec3 gorg  = m_mesh->GetOrigin();    // GlobalOrigin
	const vec3 grgn  = m_mesh->GetRegion();    // GlobalRegion
	const vec3 pitch = m_mesh->GetPitch();     // GlobalPitch
	const idx3 bsz   = m_mesh->GetBlockSize(); // BlockSize

	if( pos < gorg[m_axis] || pos > (gorg[m_axis] + grgn[m_axis]) )
	{
		return;
	}

	const VOX::BCMOctree* octree   = m_mesh->GetOctree();
	const VOX::RootGrid*  rootGrid = octree->getRootGrid();

	const u32 maxLevel = m_mesh->GetMaxOctreeLevel();

	const idx3 rootDims( rootGrid->getSizeX(), rootGrid->getSizeY(), rootGrid->getSizeZ() );
	const u32  numIdx = rootDims[m_axis] * ( 1 << maxLevel ) * bsz[m_axis];

	const VX::Math::idx2 buv( (m_axis == 0 ? bsz[1] : bsz[0]), (m_axis == 2 ? bsz[1] : bsz[2]) );

	u32 posIdx = m_mesh->GetIdxPosition(m_axis, pos);

	float position = pos;
	if(index_coord == 0){
		position = gorg[m_axis] + static_cast<f32>(posIdx) * pitch[m_axis] + pitch[m_axis] * .5f;
	}

	// Boundary
	{
		vec3 org = gorg;
		org[m_axis] = position;
		BoundarySetter(m_boundary->GetVertex(), m_boundary->GetIndex(), m_axis, org, grgn, boundaryColor, 0);
		m_boundary->EnableNeedUpdate();
	}
	if( boundaryOnly )
	{
		return;
	}
	
	const u32 numBlocksWR = 1 << maxLevel;              // Max Number of Blocks within RootGrid
	const u32 numCellsWR  = numBlocksWR * bsz[m_axis];  // Max Number of Cells within RootGrid;
	const u32 rPos = posIdx / numCellsWR;               // RootGrid Position
	const u32 lPos = posIdx % numCellsWR;               // Cell Position whithin RootGrid
	const u32 nPos = lPos / bsz[m_axis];                // Node Position within RootGrid
	
	//VXLogD("[axis:%d] numBlocksWR : %6d numCellsWR : %6d rPos : %4d lPos : %4d nPos : %4d\n", m_axis, numBlocksWR, numCellsWR, rPos, lPos, nPos);

	std::vector<const VOX::Node*> leafNodes;
	leafNodes.reserve(m_maxBlocks);
	// Collect leafNodes which crossed SlicePositoin from Octree
	VX::Math::idx2 rootUV( (m_axis == 0 ? rootDims[1] : rootDims[0]), (m_axis == 2 ? rootDims[1] : rootDims[2]) );
	for(s32 v = 0; v < rootUV.y; v++) {
		for(s32 u = 0; u < rootUV.x; u++) {
			u32 x = m_axis == 0 ? rPos : u;
			u32 y = m_axis == 1 ? rPos : m_axis == 0 ? u : v;
			u32 z = m_axis == 2 ? rPos : v;
			const VOX::Node* root = octree->getRootNode(rootGrid->index2rootID(x, y, z));
			GetBCMNodesOnPlane(m_axis, root, nPos, maxLevel, leafNodes);
		}
	}

	// size of rootGrid in coordinate space
	vec3 rootRgn( grgn.x / (f32)(rootDims.x), grgn.y / (f32)(rootDims.y), grgn.z / (f32)(rootDims.z) );

	Geometry::VertexFormat *bvtx = m_block->GetVertex();
	Geometry::VertexFormat *gvtx = m_grid->GetVertex();
	Geometry::VertexFormat *fvtx = m_face->GetVertex();
	Index *bidx = m_block->GetIndex();
	Index *gidx = m_grid->GetIndex();
	Index *fidx = m_face->GetIndex();

	u32 gridStride = ((buv.x -1) * 2 + (buv.y - 1) * 2);
	m_block->SetIndexCount( static_cast<u32>(leafNodes.size()) * 8 );
	m_grid->SetIndexCount( static_cast<u32>(leafNodes.size()) * gridStride );
	m_face->SetIndexCount( static_cast<u32>(leafNodes.size()) * 6 );
	
	Texture* tex = m_face->GetTexture();
	u32* img = reinterpret_cast<u32*>(tex->GetImagePtr());
	const idx2 texsize = tex->GetSize();

	idx2 subTexsize( texsize.x, ((u32)(leafNodes.size()) * buv.x / texsize.x + 1) * buv.y );

	// Set Geometry for each block which crossed slice plane
	for(s64 i = 0; i < (s64)(leafNodes.size()); i++){
		const VOX::Pedigree p = leafNodes[i]->getPedigree();
		
		// position of rootGrid in index space
		idx3 rootIdx( rootGrid->rootID2indexX(p.getRootID()),
		              rootGrid->rootID2indexY(p.getRootID()),
					  rootGrid->rootID2indexZ(p.getRootID()) );

		// position of rootGrid's origin  in coordinate space
		vec3 rootOrg( gorg.x + (rootRgn.x * static_cast<f32>(rootIdx.x)),
		              gorg.y + (rootRgn.y * static_cast<f32>(rootIdx.y)),
					  gorg.z + (rootRgn.z * static_cast<f32>(rootIdx.z)) );

		// position of block in coordinate space
		vec3 org, rgn;
		GetPedigreeBBox(rootOrg, rootRgn, p, org, rgn);
		org[m_axis] = position;

		// Set Geometry to block[i] boundary
		BoundarySetter(&bvtx[i * 4], &bidx[i * 8], m_axis, org, rgn, lineColor, (u32)(i * 4));
		// Set Geometry to block[i] grid
		GridSetter(&gvtx[i * gridStride], &gidx[i * gridStride], m_axis, org, rgn, buv, lineColor, (u32)(i * gridStride));

		// Get bitVoxel of block[i]
		s32 NodeID = leafNodes[i]->getBlockID();
		VOX::bitVoxelCell* bitVoxel = m_mesh->GetBlock(NodeID)->GetBitVoxel();
		
		// Texcoord
		const idx2 texIdx( (((u32)(i) * buv.x) % texsize.x), (((u32)(i) * buv.x) / texsize.x * buv.y) );
		const vec2 texcoordLB( static_cast<f32>(texIdx.x)         / static_cast<f32>(texsize.x),
		                       static_cast<f32>(texIdx.y)         / static_cast<f32>(texsize.y));
		const vec2 texcoordRT( static_cast<f32>(texIdx.x + buv.x) / static_cast<f32>(texsize.x),
		                       static_cast<f32>(texIdx.y + buv.y) / static_cast<f32>(texsize.y));
		
		// Set Geometry (and Texcoord) to block[i] face
		FaceTexSetter(&fvtx[i * 4], &fidx[i * 6], m_axis, org, rgn, texcoordLB, texcoordRT, (u32)(i * 4));
		u32 lv = p.getLevel();
		idx3 oPos( p.getX(), p.getY(), p.getZ() );
		u32  bPos = ( lPos - (oPos[m_axis] << (maxLevel - lv)) * bsz[m_axis]) >> (maxLevel - lv);
		u32 uidx = m_axis == 0 ? 1 : 0;
		u32 vidx = m_axis == 2 ? 1 : 2;

		for(s32 v = 0; v < buv.y; v++){
			for(s32 u = 0; u < buv.x; u++){
				idx3 cPos;
				cPos[uidx] = u;
				cPos[vidx] = v;
				cPos[m_axis ] = bPos;
				u32 id = GetCellID(bitVoxel, m_mesh->GetBitWidth(), bsz, cPos);

				size_t loc = (u + texIdx.x) + (v + texIdx.y) * subTexsize.x;
				u32 r = 0;
				u32 g = id;
				u32 b = 0;
				img[loc] = (b << 0) | (g << 8) | (r << 16) | (255 << 24); 
			}
		}
		delete [] bitVoxel; // !! Do not use vxnew !!
	}

	tex->EnableNeedUpdate();

	m_block->EnableNeedUpdate();
	m_grid->EnableNeedUpdate();
	m_face->EnableNeedUpdate();
}

Texture* MeshBCMSlice::CreateTexture( const u32 maxBlocks, const VX::Math::idx2& blockUV )
{
	const u32 textureLimit = Texture::MAX_SIZE;

	VX::Math::idx2 texsize;
	if( maxBlocks * blockUV.x <= textureLimit )
	{
		texsize.x = maxBlocks * blockUV.x;
		texsize.y = blockUV.y;
	}
	else
	{
		b8 status = false;
		for(u32 i = 2; i <= (textureLimit / blockUV.y); i++){
			u32 nbu = maxBlocks / i + (maxBlocks % i == 0 ? 0 : 1);
			if( nbu * blockUV.x < textureLimit )
			{
				texsize.x = nbu * blockUV.x;
				texsize.y = i   * blockUV.y;
				status = true;
				break;
			}
		}
		if( !status )
		{
			VXLogE("cannot create texture [%s:%d]\n", __FILE__, __LINE__);
			return NULL;
		}
	}
	Texture* tex = vxnew Texture(texsize);
	return tex;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
MeshBCM::MeshBCM( const VX::Math::vec3& origin,   const VX::Math::vec3& region,
                  const VX::Math::idx3& rootDims, const VX::Math::idx3& blockSize,
				  u8 bitWidth,
				  const std::vector<VOX::Pedigree>& pedigrees,
				  const std::vector<Block*>& blocks )
	: m_org(origin), m_rgn(region), m_rootDims(rootDims), m_blockSize(blockSize), m_bitWidth(bitWidth), Group(NODETYPE_MESH_BCM)
{
	m_maxLevel = 0;
	for(std::vector<VOX::Pedigree>::const_iterator it = pedigrees.begin(); it != pedigrees.end(); ++it)
	{
		m_maxLevel = (m_maxLevel < it->getLevel() ? it->getLevel() : m_maxLevel);
	}

	if( m_maxLevel > 16 ){
		return;
	}

	VOX::RootGrid* rootGrid = new VOX::RootGrid(rootDims.x, rootDims.y, rootDims.z); // !! DO NOT USE vxnew to create RootGrid  !!
	m_octree = new VOX::BCMOctree(rootGrid, pedigrees);                              // !! DO NOT USE vxnew to create BCMOctree !!
	
	VX::Math::vec3 rootPitch(m_rgn.x / static_cast<f32>(m_rootDims.x),
	                         m_rgn.y / static_cast<f32>(m_rootDims.y),
							 m_rgn.z / static_cast<f32>(m_rootDims.z) );

	VX::Math::vec3 nodePitch(rootPitch.x / static_cast<f32>( 1 << m_maxLevel ),
	                         rootPitch.y / static_cast<f32>( 1 << m_maxLevel ),
							 rootPitch.z / static_cast<f32>( 1 << m_maxLevel ) );

	m_pitch = VX::Math::vec3( nodePitch.x / static_cast<f32>(m_blockSize.x),
	                          nodePitch.y / static_cast<f32>(m_blockSize.y),
							  nodePitch.z / static_cast<f32>(m_blockSize.z) );
	
	m_blocks.resize(blocks.size());
	m_numUseBlockMemory = 0;
	for(size_t i = 0; i < blocks.size(); i++){
		m_blocks[i] = blocks[i];
		m_numUseBlockMemory += m_blocks[i]->GetRleSize(); // TODO
	}

	for(int i = 0; i < 3; i++){
		MeshBCMSlice* slice = vxnew MeshBCMSlice(i, this);
		this->AddChild(slice);
		m_slices[i] = slice;
	}

	VOX::BCMNodeCounter counter;
	for(int i = 0; i < rootGrid->getSize(); i++){
		const VOX::Node* root = m_octree->getRootNode(i);
		VisitAllBCMNode(root, counter);
	}
	m_numBCMOctreeNode = counter.GetNumNodes();

	Memory::AddExternalSize(m_numBCMOctreeNode * sizeof(VOX::Node) + m_numUseBlockMemory); // TODO

}

MeshBCM::~MeshBCM( )
{
	delete m_octree;
	
	VXLogD("Delete Blocks (MeshBCM) [Start]\n");
	//#pragma omp parallel for
	for(s64 i = 0; i < static_cast<s64>(m_blocks.size()); i++){
		delete m_blocks[i]; // !! Do not use vxnew !!
	}
	VXLogD("Delete Blocks (MeshBCM) [End]\n");

	Memory::SubExternalSize(m_numBCMOctreeNode * sizeof(VOX::Node) + m_numUseBlockMemory); // TODO
}


} // namespace VX
} // namespace SG


