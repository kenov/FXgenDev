/*
 * VX/SG/Transform.h
 *
 * SceneGraph Library
 *
 */

#ifndef INCLUDE_VXSCENEGRAPH_NODETYPE_H
#define INCLUDE_VXSCENEGRAPH_NODETYPE_H

#include "../Type.h"
#include "Node.h"

namespace VX
{
	namespace SG
	{
		enum NODETYPE
		{
			NODETYPE_UNKNOWN,
			NODETYPE_GROUP,
			NODETYPE_GEOMETRY,
			NODETYPE_MATERIAL,
			NODETYPE_TRANSFORM,
			NODETYPE_BBOX,
			NODETYPE_BBOX_DOMAIN,
			NODETYPE_BBOX_DOMAIN_SLICE_GROUP,
			NODETYPE_BBOX_DOMAIN_SLICE,
			NODETYPE_BBOX_DOMAIN_CARTESIAN_GROUP,
			NODETYPE_BBOX_DOMAIN_CARTESIAN,
			NODETYPE_BBOX_DOMAIN_BCM,
			NODETYPE_BBOX_DOMAIN_BCM_GROUP,
			NODETYPE_BBOX_DOMAIN_BCM_SUB_GROUP,
			NODETYPE_BBOX_DOMAIN_BCM_GRID,
			NODETYPE_LOCALBC,
			NODETYPE_OUTERBC,
			NODETYPE_MEDIUM,
			NODETYPE_MEDIUM_GROUP,
			NODETYPE_LOCALBCCLASS,
			NODETYPE_OUTERBCCLASS,
			NODETYPE_CROSS,
			NODETYPE_TEXTURE,
			NODETYPE_MESH_CARTESIAN,
			NODETYPE_MESH_CARTESIAN_SLICE,
			NODETYPE_MESH_BCM,
			NODETYPE_MESH_BCM_SLICE,
			NODETYPE_COLORMAP,
			NODETYPE_DISTANCE_FIELD_SLICE,
			NODETYPE_GUI_SETTINGS,
			NODETYPE_VOXEL_CARTESIAN_G,
			NODETYPE_VOXEL_CARTESIAN_SLICE_G,
			NODETYPE_VOXEL_CARTESIAN_L,
			NODETYPE_VOXEL_CARTESIAN_SLICE_L,
			NODETYPE_VOXEL_CARTESIAN_C,
			NODETYPE_VOXEL_CARTESIAN_SLICE_C,
			NODETYPE_VOXEL_CARTESIAN_BCFLAG,
		};
		
	} // namespace SG
		
} // namespace VX

#endif // INCLUDE_VXSCENEGRAPH_NODETYPE_H
