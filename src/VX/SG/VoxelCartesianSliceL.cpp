#include "VoxelCartesianSliceL.h"

#include "VoxelCartesianL.h"

#include "../Type.h"
#include "../Math.h"

#include "Node.h"
#include "Group.h"
#include "Geometry.h"
#include "Texture.h"
#include "VoxGeometryUtil.h"

#include "VoxelCartesianUtil.h"
#include "VoxelCartesianBlock.h"

#include "MeshUtil.h"

#include <string>
#include <vector>

#include <limits.h>

namespace {
	const static u32 boundaryColor = 0xFF3F55FF;
	const static u32 lineColor     = 0xFF050505;

} // namespace 

namespace VX {
namespace SG {

VoxelCartesianSliceL::VoxelCartesianSliceL(const u32 axis, const VoxelCartesianL* mesh )
  : Node(NODETYPE_VOXEL_CARTESIAN_SLICE_L), m_mesh(mesh), m_axis(axis)
{	
	assert(mesh!=NULL);
	assert(axis==0 || axis==1 || axis==2);

	const char* sliceName[3] = {"Slice X", "Slice Y", "Slice Z"};
	SetName(sliceName[m_axis]);
	
	const VX::Math::idx3 voxsize = mesh->GetNumVoxels();
	m_uvw[0] = axis == 0 ? voxsize[1] : voxsize[0];
	m_uvw[1] = axis == 2 ? voxsize[1] : voxsize[2];
	m_uvw[2] = axis == 0 ? voxsize[0] : axis == 1 ? voxsize[1] : voxsize[2];
	
	using namespace VX::SG;
	using namespace VX::Math;

	// Boundary
	m_boundary = vxnew Geometry();
	m_boundary->SetUseVertexLineColor(true);
	m_boundary->SetDrawMode(Geometry::MODE_LINE);
	m_boundary->Alloc(4, 8);

	// Grid
	m_grid = vxnew Geometry();
	m_grid->SetUseVertexLineColor(true);
	m_grid->SetDrawMode(Geometry::MODE_LINE);
	u32 gridStride = (m_uvw[0]-1) * 2 + (m_uvw[1]-1) * 2;
	assert(m_uvw[0]>0);
	assert(m_uvw[1]>0);
	assert(gridStride>0);

	m_grid->Alloc(gridStride, gridStride);

	// BlockBoundary
	const VX::Math::idx3 numDiv3 = mesh->GetNumDivs();
	m_div = VX::Math::idx2( axis == 0 ? numDiv3[1] : numDiv3[0], axis == 2 ? numDiv3[1] : numDiv3[2] );
	assert(m_div.x>0);
	assert(m_div.y>0);

	m_block = vxnew Geometry();
	m_block->SetUseVertexLineColor(true);
	m_block->SetDrawMode(Geometry::MODE_LINE);
	m_block->Alloc( 4 * (m_div.x * m_div.y), 8 * (m_div.x * m_div.y));

	std::vector< int > uSizeList;
	std::vector< int > vSizeList;

	//const int textureLimit = 32; // TODO Texture::MAX_SIZE
	//const int textureLimit = 512; // TODO Texture::MAX_SIZE
	const int textureLimit = Texture::MAX_SIZE;
	CalcDivision(m_uvw.x, textureLimit, uSizeList);
	CalcDivision(m_uvw.y, textureLimit, vSizeList);

	m_subFaces.resize(uSizeList.size() * vSizeList.size());
	idx2 hIdxCounter(0, 0);
	int cnt = 0;

	for(std::vector<int>::iterator vit = vSizeList.begin(); vit != vSizeList.end(); vit++){
		for(std::vector<int>::iterator uit = uSizeList.begin(); uit != uSizeList.end(); uit++){
			Texture* tex = vxnew Texture(idx2(*uit, *vit));
			m_subFaces[cnt].face = vxnew Geometry();
			m_subFaces[cnt].face->SetDrawMode(Geometry::MODE_POLYGON);
			m_subFaces[cnt].face->SetTexture(tex);
			m_subFaces[cnt].face->Alloc(4, 6);
			
			m_subFaces[cnt].headIndex = hIdxCounter;
			m_subFaces[cnt].size = idx2(*uit, *vit);
			hIdxCounter.x += *uit;
			cnt++;

		}
		hIdxCounter.y += *vit;
		hIdxCounter.x = 0;
	}
	/*
	VXLogD("Create subface[%d]\n", m_axis);
	for(std::vector<SubFace>::iterator it = m_subFaces.begin(); it != m_subFaces.end(); ++it){
		VXLogD("hIdx(%3d %3d) size(%3d %3d)\n", it->headIndex.x, it->headIndex.y, it->size.x, it->size.y);
	}
	*/

	vec3 org = mesh->GetOrigin();
	vec3 rgn = mesh->GetRegion();
	org[m_axis] += rgn[m_axis] * 0.5f;

	SetPosition(org[m_axis], 0);
}

VoxelCartesianSliceL::~VoxelCartesianSliceL()
{

}

f32 VoxelCartesianSliceL::GetPosition() const
{
	return m_boundary->GetVertex()[0].pos[m_axis];
}

bool VoxelCartesianSliceL::isConstCellIDValueDef(s32& cellid)
{
	cellid = 0;
	cellid = m_mesh->GetConst_CellID();
	if(cellid !=-1){
		return true;
	}
	return false;
}



void VoxelCartesianSliceL::SetPosition( const f32 pos, const u32 index_coord, const b8 boundaryOnly )
{
	using namespace VX::Math;
	
	const vec3 gorg = m_mesh->GetOrigin();
	const vec3 grgn = m_mesh->GetRegion();
	const vec3 pitch = m_mesh->GetPitch();
	const idx3 voxSize = m_mesh->GetNumVoxels();
	
	if( pos < gorg[m_axis] || pos > (gorg[m_axis] + grgn[m_axis]) )
	{
		return;
	}

	u32 pos_Idx_zeroStart = m_mesh->GetIdxPosition(m_axis, pos);
	// below posIndex start index is 1.
	static const u32 OFFSET = 1;
	u32 posIdx = pos_Idx_zeroStart + OFFSET;



	//VXLogD("posIdx : %ld gorg :%f grgn :%f pitch :%f pos :%f\n", posIdx, gorg[m_axis], grgn[m_axis], pitch[m_axis], pos);

	float position = pos;
	if(index_coord == 0){
		position = gorg[m_axis] + static_cast<f32>(pos_Idx_zeroStart) * pitch[m_axis] + pitch[m_axis] * .5f;
	}
	
	// Boundary Set
	{
		vec3 org = gorg;
		org[m_axis] = position;
		BoundarySetter(m_boundary->GetVertex(), m_boundary->GetIndex(), m_axis, org, grgn, boundaryColor);
		m_boundary->EnableNeedUpdate();
	}

	
	if( boundaryOnly ) {
		return;
	}

	int cnt = 0;

	std::vector<const VoxelCartesianBlock*> blockList;
	m_mesh->GetBlocksOnSlice(m_axis, posIdx, blockList);
	
	//VXLogD("m_axis[%2d] numBlocks : %ld\n", m_axis, blockList.size());
	assert( blockList.size()<=1);

	// cellIDが固定値であれば blockデータが無いので個別対応
	s32 const_cellId = 0;
	bool bShowConst = false;
	if( m_mesh->IsLoad() && isConstCellIDValueDef(const_cellId) && blockList.size()==0){
		//ダミーを入れる
		bShowConst = true;
		const VoxelCartesianBlock* dumy = NULL;
		blockList.push_back(dumy);
	}

	//u32 bw = m_mesh->GetBitWidth();
	for(std::vector<SubFace>::iterator it = m_subFaces.begin(); it != m_subFaces.end(); it++)
	{
		Texture* tex = it->face->GetTexture();
		u32* img = reinterpret_cast<u32*>(tex->GetImagePtr());
		idx2 texsize = tex->GetSize();

		memset(img, 0, sizeof(u32) * texsize.x * texsize.y);
		
		const idx2 &sbSize = it->size;
		const idx2 &sbHIdx = it->headIndex;
		const idx2 sbTIdx  = (sbHIdx + sbSize) - 1;
		//VXLogD("SubFace : %d\n", cnt);
		
		u32 idxU = m_axis == 0 ? 1 : 0;
		u32 idxV = m_axis == 2 ? 1 : 2;

		//#pragma omp parallel for
		for(s32 i = 0; i < static_cast<s32>(blockList.size()); i++){
			//const idx3 bSize3 = blockList[i]->GetSize();
			//const idx3 bHIdx3 = blockList[i]->GetHeadIdx();

			//相対なので変更
			const idx3 bSize3 = m_mesh->GetNumVoxels();
			const idx3 bHIdx3 = m_mesh->GetHeadIdx_L();

			const idx3 bTIdx3 = (bHIdx3 + bSize3) - 1;
			idx2 bHIdx(bHIdx3[idxU], bHIdx3[idxV]);
			idx2 bSize(bSize3[idxU], bSize3[idxV]);
			idx2 bTIdx(bTIdx3[idxU], bTIdx3[idxV]);

			
			if( !IsCross(sbHIdx, sbSize, bHIdx, bSize) )
			{
				continue;
			}

			
			idx2 bStIdx( (sbHIdx.x > bHIdx.x ? (sbHIdx.x - bHIdx.x) : 0),
			             (sbHIdx.y > bHIdx.y ? (sbHIdx.y - bHIdx.y) : 0) );

			//idx2 bEdIdx( (sbTIdx.x < bTIdx.x ? (bSize.x - (bTIdx.x - sbTIdx.x)-1) : (bSize.x-1)),
			//	         (sbTIdx.y < bTIdx.y ? (bSize.y - (bTIdx.y - sbTIdx.y)-1) : (bSize.y-1)) );
			idx2 bEdIdx( bSize.x-OFFSET, bSize.y-OFFSET );//zero start


			idx2 sbOffset( (sbHIdx.x > bHIdx.x ? 0 : (bHIdx.x - sbHIdx.x)),
			               (sbHIdx.y > bHIdx.y ? 0 : (bHIdx.y - sbHIdx.y)) );
			
			if( bShowConst){

				//固定値を設定
				assert(const_cellId!=-1);
				for(int v = bStIdx.y; v <= bEdIdx.y; v++){
					for(int u = bStIdx.x; u <= bEdIdx.x; u++){
						//idx2 sbLoc( (sbOffset.x + (u - bStIdx.x)), (sbOffset.y + (v - bStIdx.y)) );
						idx2 sbLoc( ((sbOffset.x- OFFSET) + (u - bStIdx.x)), ((sbOffset.y- OFFSET) + (v - bStIdx.y)) );
							
						idx3 bPos;
						bPos[m_axis] = posIdx-bHIdx3[m_axis];
						bPos[idxU] = u;
						bPos[idxV] = v;
						u32 id = const_cellId;
						int r = 0;
						int g = id;
						int b = 0;
						img[sbLoc.x + sbLoc.y * texsize.x] = (b << 0) | (g << 8) | (r << 16) | (255 << 24);
					}
				}
			}else{

				VOX::bitVoxelCell* bitVoxel = blockList[i]->GetBitVoxel();
				
				if(bitVoxel!=NULL){
					const u8 bw = blockList[i]->GetBitWidth();
	
					for(int v = bStIdx.y; v <= bEdIdx.y; v++){
						for(int u = bStIdx.x; u <= bEdIdx.x; u++){
							//idx2 sbLoc( (sbOffset.x + (u - bStIdx.x)), (sbOffset.y + (v - bStIdx.y)) );
							idx2 sbLoc( ((sbOffset.x- OFFSET) + (u - bStIdx.x)), ((sbOffset.y- OFFSET) + (v - bStIdx.y)) );
							
							idx3 bPos;
							bPos[m_axis] = posIdx-bHIdx3[m_axis];
							bPos[idxU] = u;
							bPos[idxV] = v;
							u32 id = GetCellID(bitVoxel, bw, bSize3, bPos);
							int r = 0;
							int g = id;
							int b = 0;
							img[sbLoc.x + sbLoc.y * texsize.x] = (b << 0) | (g << 8) | (r << 16) | (255 << 24);
						}
					}
					delete [] bitVoxel; // !! Do not use vxnew !!
				}else{
					assert(false);
				}

			}
		}

		cnt++;

		tex->EnableNeedUpdate();

		vec3 rgn = vec3 (
			m_axis == 0 ? 0.0f : pitch.x *  it->size.x,
			m_axis == 1 ? 0.0f : pitch.y * (m_axis == 0 ? it->size.x : it->size.y),
			m_axis == 2 ? 0.0f : pitch.z *  it->size.y );

		vec3 offset = vec3 (
			m_axis == 0 ? 0.0f : pitch.x *  it->headIndex.x,
			m_axis == 1 ? 0.0f : pitch.y * (m_axis == 0 ? it->headIndex.x : it->headIndex.y),
			m_axis == 2 ? 0.0f : pitch.z *  it->headIndex.y );
		//check
		assert(offset.x ==0 && offset.y == 0.0f && offset.z == 0.0f);

		vec3 org = gorg + offset;
		org[m_axis] = position;

		FaceTexSetter(it->face->GetVertex(), it->face->GetIndex(), m_axis, org, rgn, vec2(0.0, 0.0), vec2(1.0, 1.0));
		it->face->EnableNeedUpdate();
	}

	// Update Grid
	{
		vec3 org = gorg;
		org[m_axis] = position;
		GridSetter(m_grid->GetVertex(), m_grid->GetIndex(), m_axis, org, grgn, idx2(m_uvw[0], m_uvw[1]), lineColor);
		m_grid->EnableNeedUpdate();
	}

	// Update Block Boundary
	{
		vec3 org = gorg;
		org[m_axis] = position;
		u32 idxU = m_axis == 0 ? 1 : 0;
		u32 idxV = m_axis == 2 ? 1 : 2;
		for(u32 i = 0; i < static_cast<s32>(blockList.size()); i++){
			//const idx3 bSize3 = blockList[i]->GetSize();
			//const idx3 bHIdx3 = blockList[i]->GetHeadIdx();
			//相対なので以下
			const idx3 bSize3 = m_mesh->GetNumVoxels();
			const idx3 bHIdx3 = m_mesh->GetHeadIdx_L();

			org[idxU] = gorg[idxU] + (bHIdx3[idxU] * pitch[idxU]);
			org[idxV] = gorg[idxV] + (bHIdx3[idxV] * pitch[idxV]);
			vec3 rgn(0, 0, 0);
			rgn[idxU] = bSize3[idxU] * pitch[idxU];
			rgn[idxV] = bSize3[idxV] * pitch[idxV];
			BoundarySetter(&m_block->GetVertex()[i * 4], &m_block->GetIndex()[i * 8], m_axis, org, rgn, lineColor, (i * 4));
		}
		m_block->EnableNeedUpdate();
	}
}


} // namespace SG
} // namespace VX



