/*
 * VX/SG/CellIDVoxelCartesian.h
 *
 * SceneGraphLibrary
 *
 */

#ifndef INCLUDE_VoxelCartesianSliceC_H
#define INCLUDE_VoxelCartesianSliceC_H

#include "../Type.h"
#include "../Math.h"

#include "Node.h"
#include "Group.h"
#include "Geometry.h"

#include "MeshUtil.h"

#include <string>
#include <vector>

namespace VX
{
	namespace SG
	{
		class VoxelCartesianC;

		class VoxelCartesianSliceC : public Node
		{
		public:
			VoxelCartesianSliceC( const u32 axis, const VoxelCartesianC* mesh );
			~VoxelCartesianSliceC();

			int GetNumFaceGeometry() const
			{
				return static_cast<int>(m_subFaces.size());
			}

			const Geometry* GetFaceGeometry( int i ) const
			{
				return m_subFaces[i].face;
			}

			const Geometry* GetBoundaryGeometry() const
			{
				return m_boundary;
			}

			const Geometry* GetGridGeometry() const 
			{
				return m_grid;
			}

			const Geometry* GetBlockGeometry() const
			{
				return m_block;
			}

			void SetPosition(const f32 pos, const u32 index_coord, const b8 boundaryOnly = false);
			f32 GetPosition() const;
			

			b8 GetSelection() const { return false; }
			void SetSelection(b8 select, b8 flagonly = false) { }

		private:
			const VoxelCartesianC*  m_mesh;
			const u32             m_axis;

			VX::Math::idx3 m_uvw;
			VX::Math::idx2 m_div;

			NodeRefPtr<Geometry> m_block;
			NodeRefPtr<Geometry> m_grid;
			NodeRefPtr<Geometry> m_boundary;

			struct SubFace
			{
				NodeRefPtr<Geometry> face;
				VX::Math::idx2 headIndex;
				VX::Math::idx2 size;
			};
			std::vector<SubFace> m_subFaces;

		};

	} // namespace SG

} // namespace VX

#endif // INCLUDE_VXSCENEGRAPH_MESH_CARTESIAN_H

