/*
 * VX/SG/LocalBC.h
 *
 * SceneGraph Library
 *
 */

#ifndef INCLUDE_VXSCENEGRAPH_LOCALBC_H
#define INCLUDE_VXSCENEGRAPH_LOCALBC_H

#include "../Type.h"
#include "../Math.h"
#include "Node.h"

#include <string>

namespace VX
{
	namespace SG
	{
		class LocalBCClass;
		
		class LocalBC : public Node
		{
		public:
			LocalBC(const std::string& alias = "defname",
					const LocalBCClass* bcclass = 0,
					u32 id = 0,
					VX::Math::vec4 color = VX::Math::vec4(1,1,1,1),
					const std::string& medium = "")
			: Node(NODETYPE_LOCALBC)
			{
				m_alias = alias;
				m_color = color;
				m_id    = id;
				m_class = bcclass;
				m_medium = medium;
			}
			~LocalBC(){}
			
			void SetAlias(const std::string& alias)
			{
				m_alias = alias;
			}
			
			const std::string& GetAlias() const
			{
				return m_alias;
			}

			void SetMedium(const std::string& medium)
			{
				m_medium = medium;
			}
			
			const std::string& GetMedium() const
			{
				return m_medium;
			}

			const VX::Math::vec4& GetColor() const
			{
				return m_color;
			}
			
			void SetColor(const VX::Math::vec4& color)
			{
				m_color = color;
			}
			
			void SetClass(VX::SG::LocalBCClass* bcclass)
			{
				m_class = bcclass;
			}
			
			const VX::SG::LocalBCClass* GetClass() const
			{
				return m_class;
			}
			
			void SetID(u32 id)
			{
				m_id = id;
			}
			
			u32 GetID() const
			{
				return m_id;
			}
			
			b8 GetSelection() const { return false; };
			void SetSelection(b8 select, b8 flagonly = false){};
			
		private:
			std::string          m_alias;
			const LocalBCClass*  m_class;

			// this id is to relate with m_alias . m_alias is unique.
			u32                  m_id;
			VX::Math::vec4       m_color;
			std::string			 m_medium;
		};
		
	} // namespace SG
		
} // namespace VX

#endif // INCLUDE_VXSCENEGRAPH_LOCALBC_H
