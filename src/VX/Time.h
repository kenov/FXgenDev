//
//  VX/Time.h
//

#ifndef INCLUDE_VXTIME_H
#define INCLUDE_VXTIME_H

#include "Type.h"

#if VX_PLATFORM_WINDOWS
#include <windows.h>
#else // other platform
#include <sys/time.h>
#endif

#define USE_LOWPRECISION

namespace VX {
#if VX_PLATFORM_WINDOWS

#ifdef USE_LOWPRECISION	
inline f64 GetTimeCount()
{
    return static_cast<f64>(GetTickCount()) * 1e-3;
}

#else

inline f64 GetTimeCount()
{
	// TODO: use performance counter...
	return 0;
}
#endif // VX_PLATFORM_WINDOWS


// other OS
#else

inline f64 GetTimeCount()
{
    timeval timet;
    gettimeofday(&timet,0);
    return timet.tv_sec + static_cast<f64>(timet.tv_usec) * 1e-6;
}

#endif
} // namespace VX

#endif // INCLUDE_VXTIME_H

