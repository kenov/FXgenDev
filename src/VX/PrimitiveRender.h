#ifndef INCLUDE_VX_PRIMITIVE_RENDER_H
#define INCLUDE_VX_PRIMITIVE_RENDER_H

#include "../VX/Type.h"
#include "../VX/Math.h"
#include <vector>
#include <map>

namespace VX {
	class Graphics;
	class TextureObject;
	class ProgramObject;

	class PrimitiveRender
	{
	public:
		enum MODE{
			MODE_UNKNOWN        = 0,
			MODE_POINTS         = 1,
			MODE_LINES          = 2,
			MODE_LINE_STRIP     = 3,
			MODE_TRIANGLES      = 4,
			MODE_TRIANGLE_STRIP = 5,
			MODE_FIRST_FLAG     = 128
		};
		
		PrimitiveRender(Graphics* mg);
		~PrimitiveRender();
		
		void Clear();
		void SetTexture(TextureObject* tex);
		void Begin(MODE mode);
		void End();
		void Vertex(const Math::vec3& v);
		void Color(const Math::vec4& col);
		void Normal(const Math::vec3& n);
		void Texcoord(const Math::vec2& uv);
		
		void Rect(const Math::vec3& start, const Math::vec3& end);
		
		void SetWorldViewMat(const Math::matrix4x4& mat){
			m_viewMat = mat;
		}
		void SetProjMat(const Math::matrix4x4& mat){
			m_projMat = mat;
		}
		
		void DrawAll();
		
	private:
		
		// temp
		u32 m_col;
		Math::vec3 m_nor;
		Math::vec2 m_uv;
		TextureObject* m_tex;
		u32 m_mode;
		b8 m_needupdate;
		
		Graphics* g;

		class CVertex {
		public:
			CVertex(){}
			CVertex(const Math::vec3& pos_, const u32 col_) : pos(pos_), col(col_){}
			Math::vec3 pos;
			u32 col;
		};
		class CNVertex {
		public:
			CNVertex(){}
			CNVertex(const Math::vec3& pos_, const u32 col_, const Math::vec3& nor_) : pos(pos_), col(col_), nor(nor_){}
			Math::vec3 pos;
			u32 col;
			Math::vec3 nor;
		};
		class UVVertex {
		public:
			UVVertex(){};
			UVVertex(const Math::vec3& pos_, const u32 col_, const Math::vec2& uv_) : pos(pos_), col(col_), uv(uv_) {}
			Math::vec3 pos;
			u32 col;
			Math::vec2 uv;
		};

		std::vector<CVertex> 	                m_points;
		std::vector<CVertex>                    m_lines[2];  // LINES and LINE_STRIP
		std::vector<CNVertex>                   m_tris[2];   // TRIANGLES and TRIANGLE_STRIP
		std::map<void*, std::vector<UVVertex> > m_tris_uv[2];// TRIANGLES and TRIANGLE_STRIP

		ProgramObject* m_prg;
		ProgramObject* m_prgnor;
		ProgramObject* m_prgtex;
		
		u32 m_vb[3];// CVertex and CNVertex and UVVertex
		
		Math::matrix4x4 m_viewMat;
		Math::matrix4x4 m_projMat;
	};
	
} // namespace VX

#endif // INCLUDE_VX_PRIMITIVE_RENDER_H
