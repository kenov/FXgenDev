/*
 *
 * SGPickerHelper.h
 * 
 */


#ifndef __VX_SG_SGPickerHelper_H__
#define __VX_SG_SGPickerHelper_H__

#include "Type.h"
#include "Math.h"
#include "MatrixStack.h"

namespace VX {


	namespace SG{
		class Node;
		class HitGeometry;
		class Geometry;
		class VoxelCartesianG;
	}

	class SGPickerHelper
	{
	public:
		SGPickerHelper();
		~SGPickerHelper();
	
		std::string getTriPickInformation(const VX::SG::Node* root , const VX::SG::HitGeometry* hg);
		std::string getVoxelPickInformation(const VX::SG::VoxelCartesianG* g , const VX::SG::HitGeometry* hg);

	private:

		void getPath(const VX::SG::Node* self,const VX::SG::Geometry* geo ,  const std::string& dir , std::string& outPath  );


	};
	
} // namespace VX

#endif // __VX_SG_RENDER_H__

