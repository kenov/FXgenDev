/*
 *  VX.h
 *
 * Basic VX lib header
 * 
 */

#ifndef INCLUDE_VX_H
#define INCLUDE_VX_H

#include "Memory.h"
#include "Type.h"

#if 1
#include "Log.h"
#else
#define VXLogE printf
#define VXLogW printf
#define VXLogI printf
#define VXLogD printf
#endif

#endif // INCLUDE_VX_H

