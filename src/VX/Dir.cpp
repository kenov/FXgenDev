#include "VX.h"

#include <string>
#include <vector>
#include <algorithm>

#if VX_PLATFORM_WINDOWS
#include <windows.h>

#else // Macosx / Linux
#include <sys/types.h>
#include <sys/stat.h> 
#include <unistd.h> 
#include <dirent.h>

#endif

#include "Dir.h"

#include <string.h>

namespace  {
	std::string parseFilename(const s8* fpath, const s8* splitChar)
	{
		std::string f(fpath);
		size_t p = f.rfind(splitChar);
		if (p != std::string::npos)
			f.erase(f.begin(), f.begin() + p + 1);
		return f;
	}
}
namespace VX {
	
Dir::Dir(const s8* dirname)
{
	m_opened = false;
	m_dirpath = std::string(dirname);
	
	
#if VX_PLATFORM_WINDOWS
	m_dirname = parseFilename(dirname,  "\\");
	WIN32_FIND_DATAA fd;
	HANDLE h = FindFirstFileExA((std::string(dirname) + "\\*").c_str(),
						FindExInfoStandard, &fd,
						FindExSearchNameMatch, NULL,0);
	if ( INVALID_HANDLE_VALUE == h )
        return;

    do { 
        if ( fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY )
		{
			std::string fname(fd.cFileName);
			if (fname != "." && fname != "..")
				m_dirs.push_back(fd.cFileName);
		}
		else
		{
			m_files.push_back(fd.cFileName);
		}
    } while (FindNextFileA( h, &fd ));
    FindClose( h );
	
#else
	m_dirname = parseFilename(dirname,  "/");
	DIR* pDir = opendir(dirname);	
	if (!pDir)
		return;
	
	dirent* pEnt = readdir(pDir);
    while (pEnt)
	{
        if ( strcmp( pEnt->d_name, "." ) &&
             strcmp( pEnt->d_name, ".." ) )
		{
			std::string wPathName = std::string(dirname) + std::string("/") + std::string(pEnt->d_name);
			struct stat wStat;
            if ( stat( wPathName.c_str(), &wStat ) )
                break;
            
            if ( S_ISDIR( wStat.st_mode ) )
				m_dirs.push_back(pEnt->d_name);
            else
                m_files.push_back(pEnt->d_name);
        }
		pEnt = readdir(pDir);
    }
	closedir(pDir);
#endif
	m_opened = true;
}
	
b8 Dir::IsOpened() const
{
	return m_opened;
}
	
u32 Dir::GetFileCount() const
{
	return static_cast<u32>(m_files.size());
}
	
u32 Dir::GetDirCount() const
{
	return static_cast<u32>(m_dirs.size());
}
	
const std::string& Dir::GetFile(u32 i) const
{
	static std::string nullstring;
	if (i >= m_files.size())
		return nullstring;
	
	return m_files[i];
}
	
const std::string& Dir::GetDir(u32 i) const
{
	static std::string nullstring;
	if (i >= m_dirs.size())
		return nullstring;
	
	return m_dirs[i];
}

std::string Dir::GetFilePath(const s8* filename) const
{
#if VX_PLATFORM_WINDOWS
	return m_dirpath + "\\" + filename;
#else
	return m_dirpath + "/" + filename;
#endif
}
	
const std::string& Dir::GetPath() const
{
	return m_dirpath;
}

const std::string& Dir::GetName() const
{
	return m_dirname;
}

b8 Dir::CreateDir(const s8* dirname)
{
	std::string dirpath = GetFilePath(dirname);
	std::vector<std::string>::iterator it = std::find(m_files.begin(), m_files.end(), dirname);
	if (it != m_files.end())
		return false;
	it = std::find(m_dirs.begin(), m_dirs.end(), dirname);
	if (it != m_dirs.end())
		return true;
	
#if VX_PLATFORM_WINDOWS
	if (!CreateDirectoryA(dirpath.c_str(), NULL))
		return false;
#else
	const mode_t mode = S_IRWXU | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH;
	if ( mkdir(dirpath.c_str(), mode) != 0 )
		return false;
#endif
	return true;
}
	
} // namespace VX
