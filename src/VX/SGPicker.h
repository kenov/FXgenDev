/*
 *
 * SGPicker.h
 * 
 */


#ifndef __VX_SG_PICKER_H__
#define __VX_SG_PICKER_H__

#include "Type.h"
#include "Math.h"
#include "MatrixStack.h"
#include "SGPickerElm.h"

namespace VX {
	class MatrixStack;
	namespace SG{
		class Node;
		class HitGeometry;
		class HitDomain;
		class HitVoxelCartesianL;
	}

class SGPicker
{
public:
	SGPicker();
	~SGPicker();
	
	void SetViewMat(const VX::Math::matrix4x4& mat);
	void SetProjMat(const VX::Math::matrix4x4& mat);
	void Resize(const u32 width, const u32 height);
	
	enum TYPE_TRAVERSAL {
		TYPE_NONE = 0,
		TYPE_GEOMETRY = 1,
		TYPE_DOMAIN = 2,
		TYPE_ALL = 3
	};
	b8 Pick(VX::SG::Node* node, f32 win_x, f32 win_y, b8 selection = true, TYPE_TRAVERSAL type = TYPE_ALL);
	b8 Pick(VX::SG::Node* ndoe, f32 win_sx, f32 win_sy, f32 win_ex, f32 win_ey, b8 selection = true, TYPE_TRAVERSAL type = TYPE_ALL);
	b8 EndPick(VX::SG::Node* node, f32 win_x, f32 win_y, TYPE_TRAVERSAL type = TYPE_ALL);

	const VX::SGPickerElm& GetPickInformation() const;

private:
	class Frustum
	{
	public:
		VX::Math::vec4 plane[4];
	};

	b8 pick(VX::SG::Node* node, const VX::Math::vec4* frustum, b8 selection, TYPE_TRAVERSAL type);

	b8 pick(VX::SG::Node* node, const VX::Math::vec3* orgdir, std::vector<VX::SG::HitGeometry>& hits,
			std::vector<VX::SG::HitDomain>& hdoms, 
			std::vector<VX::SG::HitVoxelCartesianL>& ldoms, 
			TYPE_TRAVERSAL type);
	
	b8 endPick(VX::SG::Node* node, const VX::Math::vec3* orgdir, TYPE_TRAVERSAL type);


	VX::Math::matrix4x4 m_view;
	VX::Math::matrix4x4 m_proj;
	
	MatrixStack* m_stack;
	
	u32 m_width;
	u32 m_height;
	//pick user info of tri
	SGPickerElm m_pickElm;
};
	
} // namespace VX

#endif // __VX_SG_RENDER_H__

