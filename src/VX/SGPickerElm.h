/*
 *
 * SGPicker.h
 * 
 */


#ifndef __VX_SG_PICKERELM_H__
#define __VX_SG_PICKERELM_H__

#include "Type.h"
#include "Math.h"
#include "SG/GeometryBVH.h"
#include "SG/HitVoxelCartesianL.h"

namespace VX {


class SGPickerElm
{

public:
	SGPickerElm(){
		Clear();
	}
	virtual ~SGPickerElm(){}

	//	コピーコンストラクタ
	SGPickerElm( const SGPickerElm& obj )
	{
		assignCopy(obj);
	}

	SGPickerElm& operator=(const SGPickerElm& obj)
	{
		if(this == &obj){
			return *this;   //自己代入
		}
		assignCopy(obj);
		return(*this);
	}	
	
	const VX::SG::HitGeometry& GetGeometry(bool& isOK)const{
		isOK = bSetGeo;
		return mGeo;
	}

	const VX::SG::HitVoxelCartesianL& GetLocal(bool& isOK)const{
		isOK = bSetLocal;
		return mLocal;
	}

	void SetGeo(VX::SG::HitGeometry& geo){
		mGeo	= geo;
		bSetGeo = true;
	}
	void SetLocal(VX::SG::HitVoxelCartesianL& local){
		mLocal  = local;
		bSetLocal = true;
	}

	void Clear(){
		bSetGeo=false;
		bSetLocal=false;
	}

private:
	void assignCopy(const SGPickerElm& obj)
	{
		mGeo	= obj.mGeo;
		mLocal	= obj.mLocal;
	}
	bool bSetGeo;
	bool bSetLocal;
	VX::SG::HitGeometry			mGeo;
	VX::SG::HitVoxelCartesianL	mLocal;

};
	
} // namespace VX

#endif // __VX_SG_RENDER_H__

