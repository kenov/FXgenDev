/*
 *
 * SGRender.h
 * 
 */

#include "VX.h"
#include "Math.h"
#include "MatrixStack.h"
#include "SG/SceneGraph.h"
#include "RenderDevice.h"
#include "Graphics.h"

#include "SGRender.h"

#include "ShaderProgramObject.h"

#include <vector>
#include <set>

using namespace VX::Math;

namespace {
	static const char* cnvVShader = SHADER_INLINE(
												  uniform mat4 proj;
												  uniform mat4 view;
												  uniform mat4 world;
												  uniform mat4 projview;
												  attribute vec3 pos;
												  attribute vec3 nor;
												  attribute vec4 col;
												  varying vec4 color;
												  varying vec3 normal;
												  uniform vec4 plane;
												  void main(void)
												  {
													  normal = normalize((projview * vec4(nor,0)).xyz);
													  color = col;
													  vec4 wpos = world * vec4(pos,1.0);
													  gl_Position = projview * wpos;
												  }
												  );

	static const char* cnvFShader = SHADER_INLINE(
												  uniform float showpoly;
												  uniform float idmode;
												  uniform float alpha;
												  uniform vec4 normalcol;
												  uniform vec4 activecol;
												  uniform vec4 activediaphacol;
												  uniform vec4 coltable[32];
												  varying vec4 color;
												  varying vec3 normal;
												  uniform vec4 plane;
												  uniform float vcolmode;
												  int cast_int(float f){
                                                      return int(f + 0.5);// for nvidia cast hack
												  }

												  void main(void)
												  {
													  vec3 L = normalize(vec3(0.2,0.5,-1.0));
													  float sh = (1.0 - dot(normalize(normal),L)) * 3.141592;//-PI - PI
													  float g  = cos(sh) * 0.40 + 0.70;
													  int t = cast_int(color.r);
													  int idx = cast_int(255.0 * color.g);
													  //vec4 gcol = vec4(color.rgb * g, color.a);
													  vec4 scol[4];
													  //scol[0] = vec4(normalcol.rgb * g, normalcol.a);
													  scol[0] = vec4(coltable[idx].rgb * g, coltable[idx].a * alpha) * showpoly;
													  scol[1] = activediaphacol;
													  scol[2] = vec4(coltable[idx].rgb * g, coltable[idx].a) * showpoly;
													  scol[3] = activecol;
													  //scol[4] = diaphacol;
/*
													  //fuchi
									
											          //fuchi 半透明
													  vec4 c2 = vec4(0.5,0.5,0.5,0.25);
													  // http://en.wikibooks.org/wiki/GLSL_Programming/GLUT/Transparency
													  vec4 result = c2;//vec4(1.0) * gl_FragColor + vec4(1.0 - gl_FragColor.a) * c2;
							
													  scol[3] = result;
*/
													  float vcm = float(cast_int(vcolmode));
													  vec4 fcol = scol[cast_int(idmode) * 2 + t] * (1.0 - vcm) + vcm * color;
													  //vec4 fcol = scol[0] * (1.0 - vcm) + vcm * color;
													  //vec4 fcol = scol[const_int(idmode) * 2 + t] * (1.0 - vcm) + vcm * gcol;
                                                      
                                                      if (fcol.a < 0.01) discard;
                                                      // AMD graphic is necessary to set gl_FragColor even if discard status.
                                                      gl_FragColor = fcol;
												  }
												  );

	static const char* lcnvVShader = SHADER_INLINE(
												   uniform mat4 proj;
												   uniform mat4 view;
												   uniform mat4 world;
												   uniform mat4 projview;
												   attribute vec3 pos;
												   attribute vec3 nor;
												   attribute vec4 col;
												   varying vec4 color;
												   uniform vec4 plane;
												   void main(void)
												   {
													   color  = col;
													   vec4 wpos = world * vec4(pos,1.0);
													   gl_Position = projview * wpos;
												   }
												   );
	
	static const char* lcnvFShader = SHADER_INLINE(
												   uniform vec4 normalcol;
												   uniform vec4 activecol;
												   uniform float vcolmode;
												   varying vec4 color;
												   uniform vec4 plane;
												   int cast_int(float f){
												       return int(f + 0.5);// for nvidia cast hack
												   }
												   void main(void)
												   {
													   vec4 cols[3];
													   cols[0] = normalcol;
													   cols[1] = activecol;
													   cols[2] = color;
													   int t = cast_int(color.r);
													   int vmode = cast_int(vcolmode);
													   gl_FragColor = cols[t * (1 - vmode) + 2 * vmode];
													   gl_FragDepth = gl_FragCoord.z - 0.0001;
												   }
												   );

	static const char* tcnvVShader = SHADER_INLINE(
												   uniform mat4 proj;
												   uniform mat4 view;
												   uniform mat4 world;
												   uniform mat4 projview;
												   attribute vec3 pos;
												   attribute vec3 nor;
												   attribute vec2 uv;
												   varying vec3 normal;
												   varying vec2 texcoord;
												   uniform vec4 plane;
												   void main(void)
												   {
												       normal   = normalize((projview * vec4(nor,0)).xyz);
													   texcoord = uv;
												       vec4 wpos = world * vec4(pos,1.0);
												       gl_Position = projview * wpos;
												   }
												   );

	static const char* tcnvFShader = SHADER_INLINE(
												   uniform sampler2D tex;
												   uniform vec4 coltable[32];
												   varying vec3 normal;
												   varying vec2 texcoord;
												   uniform vec4 plane;
												   int cast_int(float f){
												       return int(f + 0.5);// for nvidia cast hack
												   }

												   void main(void)
												   {
												       vec3 L = normalize(vec3(0.2,0.5,-1.0));
												       float sh = (1.0 - dot(normalize(normal),L)) * 3.141592;//-PI - PI
												       float g  = cos(sh) * 0.40 + 0.70;

													   vec4 color = texture2D(tex, texcoord);
												       vec4 scol[4];
												       
													   //テクスチャの色はここで指定する。32のmudiumのカラーマップ
													   int idx = cast_int(255.0 * color.g);
												 	   
													   vec4 fcol = vec4(coltable[idx].rgb * g, coltable[idx].a);
													   
													   //ロードしていない断面はグレーにしないでレンダリングしない
													   if( idx == 0){
														   fcol.a = 0.00;
													   }
												       if (fcol.a < 0.01) discard;
                                                       // AMD graphic is necessary to set gl_FragColor even if discard status.
												       gl_FragColor = fcol;
												   }
												   );
	
	; // color mapping shader

	static const char* colormapVShader = SHADER_INLINE(
		uniform mat4 proj;
		uniform mat4 view;
		uniform mat4 world;
		uniform mat4 projview;
		uniform vec4 range; // x: range min, y: range max
		attribute vec3 pos;
		attribute vec3 nor;
		attribute vec2 uv;
		varying vec3 normal;
		varying float svalue;
		void main()
		{
			vec4 wpos = world * vec4(pos, 1.0);
			gl_Position = projview * wpos;
			normal = normalize((projview * vec4(nor, 0.0)).xyz);
			svalue = (uv.x - range.x) / (range.y - range.x);
		}
	);

	static const char* colormapFShader = SHADER_INLINE(
		uniform sampler2D colormap;
		varying vec3 normal;
		varying float svalue;
		
		// http://lolengine.net/blog/2013/07/27/rgb-to-hsv-in-glsl
		vec3 hsv2rgb(vec3 c)
		{
			vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
			vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
			return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
		}

		void main()
		{
			// lighting coefficient
			vec3 L = normalize(vec3(0.2,0.5,-1.0));
			float sh = (1.0 - dot(normalize(normal),L)) * 3.141592;//-PI - PI
			float g  = cos(sh) * 0.40 + 0.70;

			//float h = clamp(svalue, 0.0, 1.0) * 0.667;
			//float s = 1.0;
			//float v = 1.0;
			//float4 col = vec4(hsv2rgb(vec3(h, s, v)), 1.0);

			float u = clamp(svalue, 0.01, 0.99); // avoid border sampling 
			vec4 col = texture2D(colormap, vec2(u, 0.5));
			gl_FragColor = vec4(col.xyz * g, col.w);
		}
	);
	

} // namespace

namespace VX {
	
class SGRender::DrawQueue
{
private:
	class cachedNodeBuffer
	{
	public:
		cachedNodeBuffer(const SG::Node* node_, u32 bufferId_)
		{
			node     = node_;
			bufferId = bufferId_;
		}
		b8 operator<(const cachedNodeBuffer& t) const
		{
			return node < t.node;
		}
		b8 operator==(const cachedNodeBuffer& t) const
		{
			return node == t.node;
		}
		
		const SG::Node* node;
		u32 bufferId;
	};
	
public:
	class DrawQueueItem
	{
	public:
		enum DrawMode {
			DRAW_NOTHING         = 0,
			DRAW_POINTS          = 1,
			DRAW_LINES           = 2,
			DRAW_POLYGON         = 4,
		};
		DrawQueueItem(const SG::Geometry* geo, const matrix& m, u32 drawMode, b8 idmode = false, b8 showpoly = true, b8 trans = false)
		{
			m_geo    = geo;
			m_matrix = m;
			m_drawMode = drawMode;
			m_showpoly = showpoly;
			m_idmode = idmode;
			m_trans = trans;
		}
		~DrawQueueItem()
		{
		}
		
		b8 IsDrawPoints() const
		{
			return (m_drawMode & DRAW_POINTS ? true : false);
		}
		b8 IsDrawLines() const
		{
			return (m_drawMode & DRAW_LINES ? true : false);
		}
		b8 IsDrawPolygon() const
		{
			return (m_drawMode & DRAW_POLYGON ? true : false);
		}
		b8 IsShowPoly() const
		{
			return m_showpoly;
		}
		b8 IsIdMode() const
		{
			return m_idmode;
		}
		b8 IsTrans() const
		{
			return m_trans;
		}
		const SG::Node* GetNode() const
		{
			return m_geo;
		}
		const Math::matrix& GetMatrix() const
		{
			return m_matrix;
		}
	private:
		const SG::Geometry* m_geo;
		matrix m_matrix;
		b8     m_showpoly;
		b32    m_drawMode;
		b8     m_idmode;
		b8     m_trans;
	};
	
	DrawQueue(RenderDevice* rd, ProgramObject* triprg, ProgramObject* textriprg, ProgramObject* lineprg, ProgramObject* transtriprg, ProgramObject* colormapprg)
	{
		assert(rd);
		m_render = rd;

		m_triprg      = triprg;
		m_textriprg   = textriprg;
		m_lineprg     = lineprg;
		m_transtriprg = transtriprg;
		m_colormapprg = colormapprg;
	}
	~DrawQueue()
	{
		ClearCache();
	}
	
	void Add(const DrawQueueItem& item)
	{
		m_que.push_back(item);
	}
	
	u32 GetQueueCount() const
	{
		return static_cast<u32>(m_que.size());
	}
	
	void RenderAll(b8 drawDiapha)
	{
		std::vector<DrawQueueItem>::const_iterator it, eit = m_que.end();
		for (it = m_que.begin(); it != eit; ++it)
		{
			render(*it, drawDiapha);
		}
		if(drawDiapha) {
			m_que.clear();
		}
	}
	
	void ClearCache()
	{
		std::set<cachedNodeBuffer>::iterator it, eit = m_cached_sg.end();
		for (it = m_cached_sg.begin(); it != eit; ++it)
		{
			m_render->DeleteBuffer(it->bufferId);
			m_render->DeleteTexture(it->bufferId);
		}
		m_cached_sg.clear();
	}
	
private:
	
	RenderDevice* m_render;
	std::vector<DrawQueueItem> m_que;
	std::set<cachedNodeBuffer> m_cached_sg;
	
	VX::ProgramObject* m_triprg;
	VX::ProgramObject* m_textriprg;
	VX::ProgramObject* m_lineprg;
	VX::ProgramObject* m_transtriprg;
	VX::ProgramObject* m_colormapprg;
	
	u32 getCachedBufferID(const SG::Node* node) const 
	{
		std::set<cachedNodeBuffer>::const_iterator it = m_cached_sg.find(cachedNodeBuffer(node, 0));
		if (it != m_cached_sg.end())
			return it->bufferId;
		else
			return 0;
	}
	void cachedBufferID(const SG::Node* node, u32 bufferID)
	{
		m_cached_sg.insert(cachedNodeBuffer(node, bufferID));
	}
	void eraseBufferID(const SG::Node* node, u32 bufferID)
	{
		m_cached_sg.erase(cachedNodeBuffer(node, bufferID));
	}
	
	void render(const DrawQueueItem& item, b8 DrawDiapha)
	{
		const SG::Node* node = item.GetNode();
		assert(node!=NULL);
		const SG::Geometry* geo = static_cast<const SG::Geometry*>(node);
		const SG::Texture*  tex = static_cast<const SG::Texture*>(geo->GetTexture());
		const SG::ColorMap* colorMap = geo->GetColorMap();

		assert(geo!=NULL);

		if (DrawDiapha && !(geo->GetDiapha() || geo->GetDiaphaDraw())){
			return;
		} else if (!DrawDiapha && (geo->GetDiapha() || geo->GetDiaphaDraw())) {
			return;
		}
		
		u32 bufId = getCachedBufferID(node);
		//printf("render(SG:0x%X - ID:%u %d,%d)\n",geo, bufId, geo->GetVertexCount(), geo->GetIndexCount());
		const u32* idbuffer = 0;
		if (!geo->GetUseVertexLineColor())
			idbuffer = geo->GetIDBuffer();

		if (!bufId) {
			u32 bufferId = m_render->WriteBuffer(geo->GetVertex(), geo->GetVertexCount(), geo->GetIndex(), geo->GetIndexCount(), idbuffer);
			cachedBufferID(node, bufferId);
			bufId = bufferId;
		}
		else if (geo->GetNeedUpdate())
		{
			const_cast<SG::Geometry*>(geo)->DisableNeedUpdate();
			if (bufId == 1)
				bufId = bufId;
			u32 bufferId = m_render->WriteBuffer(geo->GetVertex(), geo->GetVertexCount(), geo->GetIndex(), geo->GetIndexCount(), idbuffer, bufId);
			cachedBufferID(node, bufferId);
			bufId = bufferId;
		}
		
		u32 texId = 0;
		if( tex ){
			texId = getCachedBufferID(tex);
			if(!texId) {
				u32 textureId = m_render->WriteTexture(tex->GetTexImage(), tex->GetSize() /* TODO format */);
				cachedBufferID(tex, textureId);
				texId = textureId;
			}
			else if (tex->GetNeedUpdate())
			{
				const_cast<SG::Texture*>(tex)->DisableNeedUpdate();
				u32 textureId = m_render->WriteTexture(tex->GetTexImage(), tex->GetSize(), /* TODO format */ texId );
				cachedBufferID(tex, textureId);
				texId = textureId;
			}
		}

		u32 colorMapId = 0;
		if (colorMap)
		{
			colorMapId = getCachedBufferID(colorMap);
			if (colorMapId == 0)
			{
				Math::idx2 size(colorMap->GetColorTableSize(), 1);
				std::vector<u8> buf(size.x * sizeof(u32));
				colorMap->WriteBufferBGRA32(buf.data(), (u32)buf.size());
				colorMapId = m_render->WriteTexture(buf.data(), size);
				cachedBufferID(colorMap, colorMapId);
			}
			else if (colorMap->GetNeedUpdate())
			{
				const_cast<SG::ColorMap*>(colorMap)->DisableNeedUpdate();
				Math::idx2 size(colorMap->GetColorTableSize(), 1);
				std::vector<u8> buf(size.x * sizeof(u32));
				colorMap->WriteBufferBGRA32(buf.data(), (u32)buf.size());
				colorMapId = m_render->WriteTexture(buf.data(), size, colorMapId);
				cachedBufferID(colorMap, colorMapId);
			}
		}
		
		if (item.IsDrawPolygon()) {
			if( tex )
			{
				m_render->QBindShader(m_textriprg);
				m_render->QInt("tex", 0);
				m_render->QMatrix("world", item.GetMatrix());
				m_render->QRenderTriangle(bufId, texId);
				m_render->QUnbindShader();
			}
			else if (colorMap)
			{
				const SG::ColorMap* cm = geo->GetColorMap();
				m_render->QBindShader(m_colormapprg);
				m_render->QInt("colormap", 0); // bind to slot 0
				m_render->QMatrix("world", item.GetMatrix());
				m_render->QVec4("range", vec4(cm->GetRangeMin(), cm->GetRangeMax(), 0, 0));
				m_render->QRenderTriangle(bufId, colorMapId);
				m_render->QUnbindShader();
			}
			else
			{
				if (item.IsTrans())
					m_render->QBindShader(m_transtriprg);
				else
            	    m_render->QBindShader(m_triprg);

				m_render->QFloat("showpoly", (item.IsShowPoly() ? 1.0f : 0.0f));
				m_render->QFloat("vcolmode", geo->GetUseVertexLineColor() ? 1.0f : 0.0f);
				m_render->QFloat("idmode", (item.IsIdMode() ? 1.0f : 0.0f));
				m_render->QFloat("alpha", geo->GetAlpha());
				m_render->QVec4("normalcol", geo->GetPolyNormalColor());
				m_render->QVec4("activecol", geo->GetPolyActiveColor());
				m_render->QVec4("activediaphacol", geo->GetPolyActiveDiaphaColor());
				m_render->QMatrix("world", item.GetMatrix());
				m_render->QRenderTriangle(bufId);
				m_render->QUnbindShader();

				if (item.IsDrawLines()) {
					m_render->QBindShader(m_lineprg);
					m_render->QVec4("normalcol", geo->GetLineNormalColor());
					m_render->QVec4("activecol", geo->GetLineActiveColor());
					m_render->QFloat("vcolmode", geo->GetUseVertexLineColor() ? 1.0f : 0.0f);
					m_render->QMatrix("world", item.GetMatrix());
					m_render->QRenderTriangleLine(bufId);
					m_render->QUnbindShader();
				}
			}
		} else if (item.IsDrawLines()) {
			m_render->QBindShader(m_lineprg);
			m_render->QVec4("normalcol", geo->GetLineNormalColor());
			m_render->QVec4("activecol", geo->GetLineActiveColor());
			m_render->QFloat("vcolmode", geo->GetUseVertexLineColor() ? 1.0f : 0.0f);
			m_render->QMatrix("world", item.GetMatrix());
			m_render->QRenderLines(bufId);
			m_render->QUnbindShader();
		} else if (item.IsDrawPoints()) {
			assert(0);//TODO impl
			m_render->QRenderPoints(bufId);
		}
	}

};
	
// -----------------------------------------------------------------------------
	
SGRender::SGRender(VX::Graphics* vg)
{
	g            = vg;
	m_render     = vxnew RenderDevice(g);
	m_stack      = vxnew MatrixStack();
	m_enableWire = false;
	m_enablePolygon = true;
	m_enableDomainWire = true;
	m_enableMeshGrid = false;

	const s32 prgnum = 5;
	ProgramObject* pg[prgnum];
	for (s32 i = 0; i < prgnum; ++i)
		pg [i] = vxnew ProgramObject(g);
	std::sort(&pg[0],&pg[prgnum]);
	m_triprg      = pg[0];
	m_textriprg   = pg[1];
	m_lineprg     = pg[2];
	m_transtriprg = pg[3];
	m_colormapprg = pg[4];

	ShaderObject vs(g), fs(g), lvs(g), lfs(g), tvs(g), tfs(g), cmvs(g), cmfs(g);
	vs.LoadFromMemory (cnvVShader,  ShaderObject::VERTEX_SHADER);
	fs.LoadFromMemory (cnvFShader,  ShaderObject::FRAGMENT_SHADER);
	lvs.LoadFromMemory(lcnvVShader, ShaderObject::VERTEX_SHADER);
	lfs.LoadFromMemory(lcnvFShader, ShaderObject::FRAGMENT_SHADER);
	tvs.LoadFromMemory(tcnvVShader, ShaderObject::VERTEX_SHADER);
	tfs.LoadFromMemory(tcnvFShader, ShaderObject::FRAGMENT_SHADER);
	cmvs.LoadFromMemory(colormapVShader, ShaderObject::VERTEX_SHADER);
	cmfs.LoadFromMemory(colormapFShader, ShaderObject::FRAGMENT_SHADER);
	m_triprg->Link(vs, fs);
	m_textriprg->Link(tvs, tfs);
	m_lineprg->Link(lvs, lfs);
	m_transtriprg->Link(vs, fs);
	m_colormapprg->Link(cmvs, cmfs);

	m_que = vxnew DrawQueue(m_render, m_triprg, m_textriprg, m_lineprg, m_transtriprg, m_colormapprg);
	
	//デフォルトの色
	const VX::Math::vec4 defColor(120/255.0f,120/255.0f,120/255.0f, 1.0f);
	for (s32 i = 0; i < MAX_IDCOLOR_NUM; ++i)
		m_colors[i] = defColor;
}
	
SGRender::~SGRender()
{
	vxdelete( m_que   );
	vxdelete( m_stack );
	
	vxdelete( m_triprg      );
	vxdelete( m_textriprg   );
	vxdelete( m_transtriprg );
	vxdelete( m_lineprg     );
	vxdelete( m_render      );
	vxdelete( m_colormapprg );
}

void SGRender::ClearCache()
{
	m_que->ClearCache();
}
	 
void SGRender::Draw(const VX::SG::Node* node)
{
	m_stack->Clear();
	drawNode(node);
}

void SGRender::RenderAll()
{
	//f64 stm = VX::GetTimeCount();
	m_render->QBindShader(m_transtriprg);
	m_render->QVec4Array("coltable", m_colors, MAX_IDCOLOR_NUM);
	m_render->QMatrix("projview", m_proj * m_view);
	m_render->QVec4("plane", VX::Math::vec4(1,0,0,0));
	m_render->QUnbindShader();

	m_render->QBindShader(m_triprg);
	m_render->QVec4Array("coltable", m_colors, MAX_IDCOLOR_NUM);
	m_render->QMatrix("projview", m_proj * m_view);
	m_render->QVec4("plane", VX::Math::vec4(1,0,0,0));
	m_render->QUnbindShader();

	m_render->QBindShader(m_textriprg);
	m_render->QVec4Array("coltable", m_colors, MAX_IDCOLOR_NUM);
	m_render->QMatrix("projview", m_proj * m_view);
	m_render->QVec4("plane", VX::Math::vec4(1,0,0,0));
	m_render->QUnbindShader();

	m_render->QBindShader(m_lineprg);
	m_render->QMatrix("projview", m_proj * m_view);
	m_render->QVec4("plane", VX::Math::vec4(1,0,0,0));
	m_render->QUnbindShader();

	m_render->QBindShader(m_colormapprg);
	m_render->QMatrix("projview", m_proj * m_view);
	m_render->QUnbindShader();

	m_que->RenderAll(false);
	g->DepthMask(VG_TRUE);
	m_render->FlashCommands();
	g->DepthMask(VG_FALSE);
	m_que->RenderAll(true);
	m_render->FlashCommands();
	g->DepthMask(VG_TRUE);

	//f64 ftm = VX::GetTimeCount();
//	VXLogD("### Time %lf\n", ftm - stm);
}
	
void SGRender::SetIDColorTable(int i, const VX::Math::vec4& col)
{
	if (i >= MAX_IDCOLOR_NUM)
		return;
	
	m_colors[i] = col;
}

VX::Math::vec4 SGRender::GetIDColorTable(int i)
{
	
	VX::Math::vec4 ret(0,0,0,0);

	if (i >= MAX_IDCOLOR_NUM){
		assert(false);
		return ret;
	}
	
	ret = m_colors[i] ;
	return ret;
}

void SGRender::drawGeometry(const VX::SG::Geometry* geo)
{
	if( geo->GetIndexCount() == 0 ){
		return;
	}
	//const u32 drawmode = DrawQueue::DrawQueueItem::DRAW_POLYGON|(m_enableWire ? DrawQueue::DrawQueueItem::DRAW_LINES : 0);
	const u32 drawmode = geo->GetDrawMode() | (m_enableWire ? DrawQueue::DrawQueueItem::DRAW_LINES : 0);
	if(geo->GetDiapha() || geo->GetDiaphaDraw()) {
		m_que->Add(DrawQueue::DrawQueueItem(geo, m_stack->GetMatrix(), drawmode, false, m_enablePolygon));
	} else {
		m_que->Add(DrawQueue::DrawQueueItem(geo, m_stack->GetMatrix(), drawmode, true, m_enablePolygon));
	}
}
	
void SGRender::drawDomain(const VX::SG::DomainCartesian* bbox)
{
	if (m_enableDomainWire) {
		const u32 drawmode = bbox->GetGeometry()->GetDrawMode();
		m_que->Add(DrawQueue::DrawQueueItem(bbox->GetGeometry(), m_stack->GetMatrix(), drawmode));
	}
	const u32 cdrawmode = bbox->GetClipGeometry()->GetDrawMode();
	m_que->Add(DrawQueue::DrawQueueItem(bbox->GetClipGeometry(), m_stack->GetMatrix(), cdrawmode));
	const u32 bdrawmode = bbox->GetBoxGeometry()->GetDrawMode();
	m_que->Add(DrawQueue::DrawQueueItem(bbox->GetBoxGeometry(), m_stack->GetMatrix(), bdrawmode, false, true, true));
}



void SGRender::drawVoxelCartesianL(const VX::SG::VoxelCartesianL* node)
{

	//中身の断面スライスの子供を表示
	if(node->IsLoad()){
		const VX::SG::Group* grp = static_cast<const VX::SG::Group*>(node);
		s32 n = grp->GetChildCount();
		for (s32 i = 0; i < n; i++) {
			//ロードしていればレンダリングするXYZ断面、BCFlagのポリゴン
			drawNode(grp->GetChild(i));
		}
	}
	//セレクション
	//depthの理由で最後に枠を表示
	if (m_enableDomainWire) {
		const u32 drawmode = node->GetGeometry()->GetDrawMode();
		m_que->Add(DrawQueue::DrawQueueItem(node->GetGeometry(), m_stack->GetMatrix(), drawmode));
	}
	const u32 cdrawmode = node->GetClipGeometry()->GetDrawMode();
	m_que->Add(DrawQueue::DrawQueueItem(node->GetClipGeometry(), m_stack->GetMatrix(), cdrawmode));
	const u32 bdrawmode = node->GetBoxGeometry()->GetDrawMode();
	m_que->Add(DrawQueue::DrawQueueItem(node->GetBoxGeometry(), m_stack->GetMatrix(), bdrawmode, false, true, true));

}

void SGRender::drawVoxelCartesianG(const VX::SG::VoxelCartesianG* node)
{

	const VX::SG::VoxelCartesianC* clipNode	= node->GetClipNode();
	const VX::SG::Group*		globalGroup = node->GetGlobalGroup();
	const VX::SG::Group*		localGroup	= node->GetLocalGroup();
	const std::vector<VX::SG::NodeRefPtr<VX::SG::VoxelCartesianL> >& locals	= node->GetLocals();

	bool isEnableClipping = clipNode->GetVisible();

	if(isEnableClipping){
		drawNode(clipNode);
	}
	//スライスポジション
	VX::Math::vec3 slicePos;
	slicePos.x = clipNode->GetSlicePosition(0);
	slicePos.y = clipNode->GetSlicePosition(1);
	slicePos.z = clipNode->GetSlicePosition(2);

	//グローバル断面
	if(globalGroup->GetVisible()){
		int size = globalGroup->GetChildCount();
		assert(size==3);
		for(int i=0;i<3;i++){
			const VX::SG::VoxelCartesianSliceG* globalCross = static_cast<const VX::SG::VoxelCartesianSliceG*>( globalGroup->GetChild(i) );
			if( !isEnableClipping || globalCross->IsVisibleByClippint(slicePos[i]) ){
				drawNode(globalCross);
			}
		}
	}
	if(localGroup->GetVisible()){
		int size = (int)locals.size();
		for(int i=0;i<size;i++){
			const VX::SG::VoxelCartesianL* local = locals.at(i);
			if(!isEnableClipping || local->IsVisibleByClippint(slicePos)){
				drawNode(local);
			}
		}
	}
}

void SGRender::drawVoxelCartesianC(const VX::SG::VoxelCartesianC* node)
{
	const VX::SG::Group* grp = static_cast<const VX::SG::Group*>(node);
	s32 n = grp->GetChildCount();
	for (s32 i = 0; i < n; i++) {
		drawNode(grp->GetChild(i));
	}
}

void SGRender::drawVoxelCartesianSliceG(const VX::SG::VoxelCartesianSliceG* slice)
{
	for(int i = 0; i < slice->GetNumFaceGeometry(); i++){
		const VX::SG::Geometry* geo = slice->GetFaceGeometry(i);
		const u32 drawmode = geo->GetDrawMode();
		m_que->Add(DrawQueue::DrawQueueItem(geo, m_stack->GetMatrix(), drawmode, true));
	}

	const u32 boundaryDrawmode = slice->GetBoundaryGeometry()->GetDrawMode();
	m_que->Add(DrawQueue::DrawQueueItem(slice->GetBoundaryGeometry(), m_stack->GetMatrix(), boundaryDrawmode));
	
	const VX::SG::Geometry* grid = slice->GetGridGeometry();
	if(m_enableMeshGrid){
		const u32 gridDrawmode = grid->GetDrawMode();
		m_que->Add(DrawQueue::DrawQueueItem(grid, m_stack->GetMatrix(), gridDrawmode));
	}

	const u32 blockDrawmode = slice->GetGridGeometry()->GetDrawMode();
	m_que->Add(DrawQueue::DrawQueueItem(slice->GetBlockGeometry(), m_stack->GetMatrix(), blockDrawmode));
}

void SGRender::drawVoxelCartesianSliceC(const VX::SG::VoxelCartesianSliceC* slice)
{
	for(int i = 0; i < slice->GetNumFaceGeometry(); i++){
		const VX::SG::Geometry* geo = slice->GetFaceGeometry(i);
		const u32 drawmode = geo->GetDrawMode();
		m_que->Add(DrawQueue::DrawQueueItem(geo, m_stack->GetMatrix(), drawmode, true));
	}

	const u32 boundaryDrawmode = slice->GetBoundaryGeometry()->GetDrawMode();
	m_que->Add(DrawQueue::DrawQueueItem(slice->GetBoundaryGeometry(), m_stack->GetMatrix(), boundaryDrawmode));
	
	const VX::SG::Geometry* grid = slice->GetGridGeometry();
	if(m_enableMeshGrid){
		const u32 gridDrawmode = grid->GetDrawMode();
		m_que->Add(DrawQueue::DrawQueueItem(grid, m_stack->GetMatrix(), gridDrawmode));
	}

	const u32 blockDrawmode = slice->GetGridGeometry()->GetDrawMode();
	m_que->Add(DrawQueue::DrawQueueItem(slice->GetBlockGeometry(), m_stack->GetMatrix(), blockDrawmode));
}


void SGRender::drawVoxelCartesianBCflag(const VX::SG::VoxelCartesianBCflag* flg)
{
	const VX::SG::GeometryBVH* geo = flg->GetGridGeometry();
	if(geo==NULL){
		return;
	}
	if( geo->GetIndexCount() == 0 ){
		return;
	}

	const u32 drawmode = geo->GetDrawMode();
	
	assert( DrawQueue::DrawQueueItem::DRAW_POLYGON == drawmode );
	
	m_que->Add(DrawQueue::DrawQueueItem(geo, m_stack->GetMatrix(), drawmode, true, m_enablePolygon));

}

void SGRender::drawVoxelCartesianSliceL(const VX::SG::VoxelCartesianSliceL* slice)
{
	for(int i = 0; i < slice->GetNumFaceGeometry(); i++){
		const VX::SG::Geometry* geo = slice->GetFaceGeometry(i);
		const u32 drawmode = geo->GetDrawMode();
		m_que->Add(DrawQueue::DrawQueueItem(geo, m_stack->GetMatrix(), drawmode, true));
	}

	const u32 boundaryDrawmode = slice->GetBoundaryGeometry()->GetDrawMode();
	m_que->Add(DrawQueue::DrawQueueItem(slice->GetBoundaryGeometry(), m_stack->GetMatrix(), boundaryDrawmode));
	
	const VX::SG::Geometry* grid = slice->GetGridGeometry();
	if(m_enableMeshGrid){
		const u32 gridDrawmode = grid->GetDrawMode();
		m_que->Add(DrawQueue::DrawQueueItem(grid, m_stack->GetMatrix(), gridDrawmode));
	}

	const u32 blockDrawmode = slice->GetGridGeometry()->GetDrawMode();
	m_que->Add(DrawQueue::DrawQueueItem(slice->GetBlockGeometry(), m_stack->GetMatrix(), blockDrawmode));
}
void SGRender::drawMeshCartesianSlice(const VX::SG::MeshCartesianSlice* slice)
{
	for(int i = 0; i < slice->GetNumFaceGeometry(); i++){
		const VX::SG::Geometry* geo = slice->GetFaceGeometry(i);
		const u32 drawmode = geo->GetDrawMode();
		m_que->Add(DrawQueue::DrawQueueItem(geo, m_stack->GetMatrix(), drawmode, true));
	}

	const u32 boundaryDrawmode = slice->GetBoundaryGeometry()->GetDrawMode();
	m_que->Add(DrawQueue::DrawQueueItem(slice->GetBoundaryGeometry(), m_stack->GetMatrix(), boundaryDrawmode));
	
	const VX::SG::Geometry* grid = slice->GetGridGeometry();
	if(m_enableMeshGrid){
		const u32 gridDrawmode = grid->GetDrawMode();
		m_que->Add(DrawQueue::DrawQueueItem(grid, m_stack->GetMatrix(), gridDrawmode));
	}

	const u32 blockDrawmode = slice->GetGridGeometry()->GetDrawMode();
	m_que->Add(DrawQueue::DrawQueueItem(slice->GetBlockGeometry(), m_stack->GetMatrix(), blockDrawmode));
}

void SGRender::drawMeshBCMSlice(const VX::SG::MeshBCMSlice* slice)
{
	const VX::SG::Geometry* geo;
	geo = slice->GetBoundaryGeometry();
	m_que->Add(DrawQueue::DrawQueueItem(geo, m_stack->GetMatrix(), geo->GetDrawMode()));
	
	geo = slice->GetFaceGeometry();
	m_que->Add(DrawQueue::DrawQueueItem(geo, m_stack->GetMatrix(), geo->GetDrawMode()));

	geo = slice->GetBlockGeometry();
	m_que->Add(DrawQueue::DrawQueueItem(geo, m_stack->GetMatrix(), geo->GetDrawMode()));

	if(m_enableMeshGrid){
		geo = slice->GetGridGeometry();
		m_que->Add(DrawQueue::DrawQueueItem(geo, m_stack->GetMatrix(), geo->GetDrawMode()));
	}

}

void SGRender::drawDistanceFieldSlice(const VX::SG::DistanceFieldSlice* slice)
{
	const VX::SG::Geometry* geo = slice->GetBoundaryGeometry();
	m_que->Add(DrawQueue::DrawQueueItem(geo, m_stack->GetMatrix(), geo->GetDrawMode()));

	geo = slice->GetFieldGeometry();
	m_que->Add(DrawQueue::DrawQueueItem(geo, m_stack->GetMatrix(), geo->GetDrawMode()));
}

void SGRender::drawBBox(const VX::SG::BBox* bbox)
{
	const u32 drawmode = bbox->GetDrawMode();
	m_que->Add(DrawQueue::DrawQueueItem(bbox, m_stack->GetMatrix(), drawmode));
}

void SGRender::drawNode(const VX::SG::Node* node)
{
	if (!node || !node->GetVisible())
		return;
	
	using namespace VX::SG;
	const NODETYPE t = node->GetType();
	if (t == NODETYPE_GROUP							|| 
		t == NODETYPE_MESH_CARTESIAN				|| 
		t == NODETYPE_MESH_BCM						|| 
		t == NODETYPE_BBOX_DOMAIN_SLICE_GROUP		|| 
		t == NODETYPE_BBOX_DOMAIN_CARTESIAN_GROUP	|| 
		t == NODETYPE_BBOX_DOMAIN_BCM_GROUP			|| 
		t == NODETYPE_BBOX_DOMAIN_BCM_SUB_GROUP) {
		const Group* grp = static_cast<const Group*>(node);
		s32 n = grp->GetChildCount();
		for (s32 i = 0; i < n; i++) {
			const NODETYPE type = grp->GetChild(i)->GetType();
			if (type == NODETYPE_GEOMETRY) {
				const Geometry* geo = static_cast<const Geometry*>(grp->GetChild(i));
				if (!geo->GetDiapha() && !geo->GetDiaphaDraw()) {
					drawNode(grp->GetChild(i));
				}
			} else {
				drawNode(grp->GetChild(i));
			}
		}
		for (s32 i = 0; i < n; i++) {
			const NODETYPE type = grp->GetChild(i)->GetType();
			if (type == NODETYPE_GEOMETRY) {
				const Geometry* geo = static_cast<const Geometry*>(grp->GetChild(i));
				if (geo->GetDiapha() || geo->GetDiaphaDraw()) {
					drawNode(grp->GetChild(i));
				}
			}
		}
	} else if (t == NODETYPE_GEOMETRY	|| 
		t == NODETYPE_CROSS				|| 
		t == NODETYPE_BBOX_DOMAIN_BCM_GRID) {
		const Geometry* geo = static_cast<const Geometry*>(node);
		drawGeometry(geo);
	} else if (t == NODETYPE_BBOX) {
		const BBox* bbox = static_cast<const BBox*>(node);
		drawBBox(bbox);
	} else if (t == NODETYPE_BBOX_DOMAIN_CARTESIAN) {
		const DomainCartesian* domain = static_cast<const DomainCartesian*>(node);
		drawDomain(domain);
	} else if (t == NODETYPE_BBOX_DOMAIN_SLICE){
		const DomainSlice* slice = static_cast<const DomainSlice*>(node);
		drawGeometry(slice->GetBlockGeometry());
		drawGeometry(slice->GetBoundaryGeometry());
		drawGeometry(slice->GetFaceGeometry());
	} else if (t == NODETYPE_VOXEL_CARTESIAN_G){
		const VoxelCartesianG* domain = static_cast<const VoxelCartesianG*>(node);
		drawVoxelCartesianG(domain);
	} else if (t == NODETYPE_VOXEL_CARTESIAN_L){
		const VoxelCartesianL* domain = static_cast<const VoxelCartesianL*>(node);
		drawVoxelCartesianL(domain);
	} else if (t == NODETYPE_VOXEL_CARTESIAN_C){
		const VoxelCartesianC* domain = static_cast<const VoxelCartesianC*>(node);
		drawVoxelCartesianC(domain);
	} else if (t == NODETYPE_VOXEL_CARTESIAN_SLICE_G){
		const VoxelCartesianSliceG* slice = static_cast<const VoxelCartesianSliceG*>(node);
		drawVoxelCartesianSliceG(slice);

	} else if (t == NODETYPE_VOXEL_CARTESIAN_SLICE_L){
		const VoxelCartesianSliceL* slice = static_cast<const VoxelCartesianSliceL*>(node);
		drawVoxelCartesianSliceL(slice);

	} else if (t == NODETYPE_VOXEL_CARTESIAN_BCFLAG){
		
		const VoxelCartesianBCflag* f = static_cast<const VoxelCartesianBCflag*>(node);
		drawVoxelCartesianBCflag(f);

	} else if (t == NODETYPE_VOXEL_CARTESIAN_SLICE_C){
		const VoxelCartesianSliceC* slice = static_cast<const VoxelCartesianSliceC*>(node);
		drawVoxelCartesianSliceC(slice);

	} else if (t == NODETYPE_MESH_CARTESIAN_SLICE){
		const MeshCartesianSlice* slice = static_cast<const MeshCartesianSlice*>(node);
		drawMeshCartesianSlice(slice);
	} else if (t == NODETYPE_MESH_BCM_SLICE){
		const MeshBCMSlice* slice = static_cast<const MeshBCMSlice*>(node);
		drawMeshBCMSlice(slice);
	} else if (t == NODETYPE_DISTANCE_FIELD_SLICE) {
		const DistanceFieldSlice* slice = static_cast<const DistanceFieldSlice*>(node);
		drawDistanceFieldSlice(slice);
	} else if (t == NODETYPE_TRANSFORM) {
		const Transform* trs = static_cast<const Transform*>(node);
		const matrix& m = trs->GetMatrix();
		b8 push = false;
		if (m != Identity())
			push = true;
			
		if (push)
			m_stack->Push(m);
		s32 n = trs->GetChildCount();
		for (s32 i = 0; i < n; i++) {
			drawNode(trs->GetChild(i));
		}
		if (push)
			m_stack->Pop();
	}
}

void SGRender::SetWorldViewMat(const VX::Math::matrix4x4& mat)
{
	m_view = mat;
}
	
void SGRender::SetProjMat(const VX::Math::matrix4x4& mat)
{
	m_proj = mat;
}
	
void SGRender::ShowWire(b8 enable)
{
	m_enableWire = enable;
}
	
b8 SGRender::IsShowWire()const
{
	return m_enableWire;
}

void SGRender::ShowDomainWire(b8 enable)
{
	m_enableDomainWire = enable;
}

b8 SGRender::IsShowDomainWire()const
{
	return m_enableDomainWire;
}

void SGRender::ShowMeshGrid(b8 enable)
{
	m_enableMeshGrid = enable;
}

b8 SGRender::IsShowMeshGrid() const
{
	return m_enableMeshGrid;
}

void SGRender::ShowPolygon(b8 enable)
{
	m_enablePolygon = enable;
}

b8 SGRender::IsShowPolygon()const
{
	return m_enablePolygon;
}


} // namespace VX

