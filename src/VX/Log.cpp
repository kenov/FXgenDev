

#include "Log.h"
#include <stdio.h>
#include <stdarg.h>
#include <string>

#ifdef _WIN32
#include <windows.h>
#endif

enum LOG_LEVEL
{
	LOG_ERROR = 0,
	LOG_WARN,
	LOG_INFO,
	LOG_DEBUG
};


void VXLog( enum LOG_LEVEL level, const std::string& msg, bool prefix = true )
{
	static const char *log_header[4] = {
		"[ERR]", "[WRN]", "[INF]", "[DBG]"
	};
	
	std::string logstr;
	if(prefix){
		logstr += log_header[level];
	}
	logstr += std::string(" ") + msg;

#ifdef _WIN32
	OutputDebugStringA(logstr.c_str());
#else
	printf("%s", logstr.c_str());	
#endif
}

void VXLogE (const char *format, ...)
{
	char buf[1024];
	va_list arg;
	va_start(arg, format);
	vsprintf(buf, format, arg);
	va_end(arg);
	
	VXLog(LOG_ERROR, buf);
}

void VXLogW (const char *format, ...)
{
	char buf[1024];
	va_list arg;
	va_start(arg, format);
	vsprintf(buf, format, arg);
	va_end(arg);
	
	VXLog(LOG_WARN, buf);
}

void VXLogI (const char *format, ...)
{
	char buf[1024];
	va_list arg;
	va_start(arg, format);
	vsprintf(buf, format, arg);
	va_end(arg);
	
	VXLog(LOG_INFO, buf);
}

void VXLogD (const char *format, ...)
{
#ifdef _DEBUG
	char buf[1024];
	va_list arg;
	va_start(arg, format);
	vsprintf(buf, format, arg);
	va_end(arg);
	
	VXLog(LOG_DEBUG, buf);
#endif
}

void VXPrint (const char *format, ...)
{
#ifdef _DEBUG
	char buf[1024];
	va_list arg;
	va_start(arg, format);
	vsprintf(buf, format, arg);
	va_end(arg);
	
	VXLog(LOG_DEBUG, buf, false);
#endif
}
