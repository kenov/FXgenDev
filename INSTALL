/*
 * FXgen
 * Mesh generator for FrontFlow solvers
 *
 *
 * Copyright (c) 2011-2013 Institute of Industrial Science, The University of Tokyo. 
 * All rights reserved.
 *
 * Copyright (c) 2012-2013 Advanced Institute for Computational Science, RIKEN. 
 * All rights reserved.
 *
 */


SOFTWARE REQUIREMENT
====================

- wXwidgets
- M+ OUTLINE FONTS


INSTALL FROM BINARY
===================

1. Linux
Decompress archived file.

$ tar xvfz FXgen_1_1_0.tar.gz

Application is installed in expanded directory FXgen_1_1_0/bin/FXgen.
Add search path /bin to your .bashrc


2. Mac OSX
Archived dmg file is provided. Mount FXgen_1_1_0.dmg and copy FXgen.app and doc/ to your application folder.


3. Windows
A zip file FXgen_1_1_0.zip is provided. After decompress the file, you'll find bin/ and doc/ directory.
Copy both of them to your arbitorary directory.


HOW TO BUILD
============

1. with configure

$ ./configure [options]
$ make
$ make install


Configure options:

 --prefix=INSTALL_DIR
    Specify a directory to be installed. The default directory is /usr/local/FFVC

 CC=C_COMPILER
    Specify a C compiler.

 CFLAGS=C_OPTIONS
    Specify compiler options.

 CXX=CXX_COMPILER
    Specify a C++ compiler, e.g., g++, icpc, xlc++ or others.

 CXXFLAGS=CXX_OPTIONS
    Specify compiler options.


Here is examples.


## for OpenMPI compiled by INTEL compiler, wrapper compiler, openmp.

  $ ./configure \
      CC=mpicc \
      CFLAGS=-O3 \
      CXX=mpicxx \
      CXXFLAGS="-O3 -openmp -par-report=3 -vec-report=2" 


## for GNU compiler

  $ ./configure \
      --prefix=/usr/local/FFV/FFVC \
      CC=mpicc \
      CFLAGS=-O3 \
      CXX=mpicxx \
      CXXFLAGS="-O3 -fopenmp"



2. Hand compile

If a provided configure does not work, use Makefile_hand to build this library. At first,
edit a make_setting file to tell your machine environment.


