#
# Makefile for fxgen
#

LINUXDIR = ./linuxproj
PARALLEL = 1

# Linux Settings
PROGRAM = FXgen
RM = rm -rf
all : $(PROGRAM_LINUX)
	cd ./unbomer; make; cd ..
	if [ ! -d $(LINUXDIR) ] ;then mkdir $(LINUXDIR) ; fi
	if [ -d $(LINUXDIR) ] ;then \
		cd ./linuxproj; \
		cp -u ../unbomer/unbomall .; \
		cp -ru ../src/* .; \
		./unbomall `find ./ -name *.cpp -o -name *.h` \
		cd ..;\
	fi
	if [ -d $(LINUXDIR) ] ;then \
		make -j $(PARALLEL) -f Makefile_Linux \
	; fi

clean:
	if [ -d $(LINUXDIR) ] ; then rm -rf $(LINUXDIR) ; fi
