#include <cstdio>
#include <fstream>
#include <vector>

int main(int argc, const char *argv[]) {
	if (argc != 2) {
		std::puts("unbomer <input filename>");
		return -1;
	}

	std::ifstream fin(argv[1], std::ios::binary);
	if (!fin) {
		//std::printf("%s is not a text file\n", argv[1]);
		//return -1;
		return 0;
	}

	std::vector<unsigned char> buf;

	unsigned char c = 0;
	while (fin.read((char *)&c, 1)) buf.push_back(c);
	fin.close();

	if (!buf.empty() && buf[0] == 0xEF && buf[1] == 0xBB && buf[2] == 0xBF) {
		std::remove(argv[1]);
		std::ofstream fout(argv[1], std::ios::binary);
		fout.write((char *)&buf[3], (std::streamsize)(buf.size()-3));
		fout.close();
		std::printf("%s delete bom\n", argv[1]);

	} else {
		//std::remove(argv[1]);
		//std::ofstream fout(argv[1], std::ios::binary);
		//fout.write((char *)&buf[0], (std::streamsize)(buf.size()));
		//fout.close();
		//std::printf("do not have bom\n", argv[1]);
	}
}
