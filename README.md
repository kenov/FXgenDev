FXgen
=====

Mesh generation for FrontFlow solvers


Copyright (c) 2011-2015 Institute of Industrial Science, The University of Tokyo. 
All rights reserved.

Copyright (c) 2012-2015 Advanced Institute for Computational Science, RIKEN. 
All rights reserved.



OUTLINE
=======

FXgen is a mesh generation GUI application for Cartesian and hierarchical Cartesian data structure. This application generate not a real grid system but parameters to generate a material grid system on parallel computers.


