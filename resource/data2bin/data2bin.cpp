/*
	DATA2Header Exporter coded by kioku(Cyber K 2005
	2005/1/29
*/

#include <stdio.h>
#include <string>
using namespace std;

int main(int argc, char* argv[])
{
	FILE* fp;
	char filename[1024];
	printf("DATA to Binary_HeaderFile Converter for Intro64K\n(c) Cyber K 2004 (coded by kioku)\n");
	if(argc==1){
		printf("Input filename which you want to convert:");
		scanf("%s",filename);
	}else if(argc==2){
		strcpy(filename, argv[1]);
	}

	printf("Open...%s\n",filename);
	if((fp=fopen(filename,"rb"))==NULL){
		printf("Can't open %s !\n",filename);	
	}else{
		string fname = filename;
		string ofname = filename;
		if(fname.rfind("\\")!=-1){
			fname.erase(fname.begin(),fname.begin()+fname.rfind("\\")+1);
		}
		fname.erase(fname.begin()+fname.find("."),fname.end());

		if(ofname.rfind("\\")!=-1){
			ofname.erase(ofname.begin()+ofname.rfind("\\")+1,ofname.end());
		}else{
			ofname="";
		}
		string out_fname = ofname+fname+".h";
		FILE* of = fopen(out_fname.c_str(),"wt");
		unsigned char buf;

		fprintf(of,"//Output by DATA2Header Exporter coded by kioku(Cyber K 2005\n\n");
		fprintf(of,"const unsigned char %s[] = {\n\t",fname.c_str());
		int bcnt=0;
		while(1){
			bcnt++;
			fread(&buf,1,1,fp);
			fprintf(of,"0x%02X",buf);
			if(feof(fp)) break;
			
			fprintf(of,", ");
			if(bcnt>=16){
				fprintf(of,"\n\t");
				bcnt=0;
			}
		}
		fprintf(of,"\n};"); 
		fclose(of);
		fclose(fp);
	}
	return 0;
}